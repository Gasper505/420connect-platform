package com.fourtwenty.core.services.mgmt.impl;

import com.fourtwenty.core.domain.models.product.Inventory;
import com.fourtwenty.core.domain.models.product.InventoryTransferKit;
import com.fourtwenty.core.domain.models.product.InventoryTransferLog;
import com.fourtwenty.core.domain.models.product.ProductBatch;
import com.fourtwenty.core.domain.repositories.dispensary.InventoryRepository;
import com.fourtwenty.core.domain.repositories.dispensary.InventoryTransferKitRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductBatchRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.mgmt.InventoryTransferKitService;
import com.fourtwenty.core.util.DateUtil;
import com.google.common.collect.Lists;
import com.google.inject.Provider;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.HashSet;

public class InventoryTransferKitServiceImpl extends AbstractAuthServiceImpl implements InventoryTransferKitService {

    private static final String INVENTORY_KIT = "Inventory transfer kit";
    private static final String INVENTORY_KIT_NOT_FOUND = "Inventory transfer kit not found";
    private static final String TRANSFER_REQUEST_NOT_FOUND = "Inventory transfer kit not found";
    private static final String INVENTORY_KIT_NAME_EXIST = "Inventory transfer kit name already exists";

    @Inject
    private InventoryTransferKitRepository kitRepository;
    @Inject
    private InventoryRepository inventoryRepository;
    @Inject
    private ProductBatchRepository productBatchRepository;

    @Inject
    public InventoryTransferKitServiceImpl(Provider<ConnectAuthToken> token) {
        super(token);
    }

    /**
     * This method get list of inventory
     * @param start: start
     * @param limit: limit
     */
    @Override
    public SearchResult<InventoryTransferKit> getInventoryTransferKits(int start, int limit) {
        if (limit == 0) {
            limit = Integer.MAX_VALUE;
        }

        return kitRepository.findItems(token.getCompanyId(), token.getShopId(), start, limit);
    }

    /**
     * This method returns instance of InventoryTransferKit
     * @param id: id of transfer kit
     */
    @Override
    public InventoryTransferKit getInventoryTransferKitById(String id) {

        InventoryTransferKit transferKit = kitRepository.get(token.getCompanyId(), id);
        if (transferKit == null) {
            throw new BlazeInvalidArgException(INVENTORY_KIT, INVENTORY_KIT_NOT_FOUND);
        }

        return transferKit;
    }

    /**
     * Create inventory transfer kit
     * @param request: information required to create inventory kit
     */
    @Override
    public InventoryTransferKit createInventoryTransferKit(InventoryTransferKit request) {

        InventoryTransferKit kitByName = kitRepository.getInventoryKitByName(token.getCompanyId(), token.getShopId(), request.getKitName());
        if (kitByName != null) {
            throw new BlazeInvalidArgException(INVENTORY_KIT, INVENTORY_KIT_NAME_EXIST);
        }

        HashMap<String, Inventory> inventoryHashMap = inventoryRepository.listAsMap(token.getCompanyId());

        this.validateInventoryTransferKit(inventoryHashMap, request);

        String fromShopId = StringUtils.isBlank(request.getFromShopId()) ? token.getShopId() : request.getFromShopId();
        String toShopId = StringUtils.isBlank(request.getToShopId()) ? token.getShopId() : request.getToShopId();

        InventoryTransferKit kit = new InventoryTransferKit();
        kit.prepare(token.getCompanyId());
        kit.setShopId(token.getShopId());
        kit.setFromShopId(fromShopId);
        kit.setToShopId(toShopId);
        kit.setFromInventoryId(request.getFromInventoryId());
        kit.setToInventoryId(request.getToInventoryId());

        HashSet<ObjectId> batchIds = new HashSet<>();
        for (InventoryTransferLog log : request.getTransferLogs()) {
            if (StringUtils.isNotBlank(log.getToBatchId()) && ObjectId.isValid(log.getToBatchId())) {
                batchIds.add(new ObjectId(log.getToBatchId()));
            }
            if (StringUtils.isNotBlank(log.getFromBatchId()) && ObjectId.isValid(log.getFromBatchId())) {
                batchIds.add(new ObjectId(log.getFromBatchId()));
            }
        }
        HashMap<String, ProductBatch> productBatchHashMap = productBatchRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(batchIds));
        for (InventoryTransferLog log : request.getTransferLogs()) {
            ProductBatch fromProductBatch = productBatchHashMap.get(log.getFromBatchId());
            if (fromProductBatch != null) {
                String batchInfo = String.format("%s | %s", DateUtil.toDateFormatted(fromProductBatch.getPurchasedDate()), fromProductBatch.getSku());
                log.setFromBatchInfo(batchInfo);
                log.setFromProductBatch(fromProductBatch);
            }

            if (StringUtils.isNotBlank(log.getToBatchId()) && ObjectId.isValid(log.getToBatchId())) {
                ProductBatch toProductBatch = productBatchHashMap.get(log.getToBatchId());
                if (toProductBatch != null) {
                    log.setToProductBatch(toProductBatch);
                }
            }

            log.setPrevTransferAmt(log.getTransferAmount());
        }

        kit.setTransferByBatch(request.isTransferByBatch());
        kit.setTransferLogs(request.getTransferLogs());
        kit.setKitName(request.getKitName());

        return kitRepository.save(kit);
    }

    /**
     * Update inventory transfer kit
     * @param id: id of inventory transfer kit
     * @param request: information required to update inventory kit
     */
    @Override
    public InventoryTransferKit updateInventoryTransferKit(String id, InventoryTransferKit request) {
        InventoryTransferKit dbKit = kitRepository.get(token.getCompanyId(), id);

        if (dbKit == null) {
            throw new BlazeInvalidArgException(INVENTORY_KIT, INVENTORY_KIT_NOT_FOUND);
        }

        InventoryTransferKit kitByName = kitRepository.getInventoryKitByName(token.getCompanyId(), token.getShopId(), request.getKitName());
        if (kitByName != null && !kitByName.getId().equalsIgnoreCase(id)) {
            throw new BlazeInvalidArgException(INVENTORY_KIT, INVENTORY_KIT_NAME_EXIST);
        }

        String fromShopId = StringUtils.isBlank(request.getFromShopId()) ? token.getShopId() : request.getFromShopId();
        String toShopId = StringUtils.isBlank(request.getToShopId()) ? token.getShopId() : request.getToShopId();

        HashMap<String, Inventory> inventoryHashMap = inventoryRepository.listAsMap(token.getCompanyId());

        this.validateInventoryTransferKit(inventoryHashMap, request);

        dbKit.setKitName(request.getKitName());
        dbKit.setFromShopId(fromShopId);
        dbKit.setToShopId(toShopId);
        dbKit.setFromInventoryId(request.getFromInventoryId());
        dbKit.setToInventoryId(request.getToInventoryId());
        dbKit.setTransferByBatch(request.isTransferByBatch());
        dbKit.setTransferLogs(request.getTransferLogs());

        return kitRepository.update(token.getCompanyId(), dbKit.getId(), dbKit);
    }

    private void validateInventoryTransferKit(HashMap<String, Inventory> inventoryHashMap, InventoryTransferKit request) {
        if (request.getTransferLogs().size() == 0) {
            throw new BlazeInvalidArgException(INVENTORY_KIT, TRANSFER_REQUEST_NOT_FOUND);
        }

        if (StringUtils.isNotBlank(request.getFromInventoryId()) && !inventoryHashMap.containsKey(request.getFromInventoryId())) {
            throw new BlazeInvalidArgException(INVENTORY_KIT, "Invalid 'from' inventory.");
        }
        if (StringUtils.isNotBlank(request.getToInventoryId()) && !inventoryHashMap.containsKey(request.getToInventoryId())) {
            throw new BlazeInvalidArgException(INVENTORY_KIT, "Invalid 'to' inventory.");
        }
        if (StringUtils.isNotBlank(request.getFromInventoryId()) && StringUtils.isNotBlank(request.getToInventoryId()) && request.getFromInventoryId().equalsIgnoreCase(request.getToInventoryId())) {
            throw new BlazeInvalidArgException(INVENTORY_KIT, "Cannot transfer within the same inventory.");
        }

        for (InventoryTransferLog log : request.getTransferLogs()) {
            if (!ObjectId.isValid(log.getProductId())) {
                throw new BlazeInvalidArgException(INVENTORY_KIT, log.getProductId() + " is an invalid id.");
            } else if (log.getTransferAmount().doubleValue() <= 0) {
                throw new BlazeInvalidArgException(INVENTORY_KIT, "Transfer amount must be greater than 0.");
            }
        }
    }

    /**
     * Delete inventory transfer kit
     * @param id: id of inventory transfer kit
     */
    @Override
    public void deleteInventoryTransferKit(String id) {
        InventoryTransferKit transferKit = kitRepository.get(token.getCompanyId(), id);
        if (transferKit == null) {
            throw new BlazeInvalidArgException(INVENTORY_KIT, INVENTORY_KIT_NOT_FOUND);
        }
        kitRepository.removeById(token.getCompanyId(), id);
    }
}
