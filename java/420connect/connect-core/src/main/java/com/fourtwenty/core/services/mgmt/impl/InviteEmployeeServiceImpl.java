package com.fourtwenty.core.services.mgmt.impl;

import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.domain.models.company.*;
import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.exceptions.BlazeOperationException;
import com.fourtwenty.core.exceptions.BlazeQuickPinExistException;
import com.fourtwenty.core.managed.AmazonServiceManager;
import com.fourtwenty.core.rest.dispensary.requests.employees.EmployeeInviteRequest;
import com.fourtwenty.core.rest.dispensary.requests.employees.EmployeeInviteTokenRequest;
import com.fourtwenty.core.rest.dispensary.requests.employees.RegisterInviteeRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.security.tokens.EmployeeInviteToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.mgmt.InviteEmployeeService;
import com.fourtwenty.core.util.SecurityUtil;
import com.google.common.collect.Lists;
import com.google.inject.Provider;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.validator.routines.EmailValidator;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.*;

/**
 * Created by decipher on 22/11/17 12:04 PM
 * Raja (Software Engineer)
 * rajad@decipherzone.com
 * Decipher Zone Softwares
 * www.decipherzone.com
 */
public class InviteEmployeeServiceImpl extends AbstractAuthServiceImpl implements InviteEmployeeService {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmployeeServiceImpl.class);

    private static final String INVITE_EMAIL_ERROR = "Error while generating invitation email";
    private static final String INVITE_TOKEN_INVALID = "Either your token is expired or it is invalid";
    private static final String INVITE_TOKEN = "Invitation Token";
    private static final String EMPLOYEE = "Employee";
    private static final String EMPLOYEE_PIN = "Employee Pin";
    private static final String EMPLOYEE_PIN_ALREADY_USED = "This pin is in used.";
    private static final String EMPLOYEE_PIN_NUMERIC = "Employee Pin should be numeric.";
    private static final String EMPLOYEE_ALREADY_REGISTERED = "Employee registered already";
    private static final String EMPLOYEE_DOEST_NOT_EXIST = "Employee does not exist";
    private static final String DRIVER_DELIVERY = "Delivery Driver";
    private static final String DRIVER_DELIVERY_NOT_COMPLETE = "Delivery Driver details are not complete";
    private static final String INVITE_TOKEN_NULL = "Invite token is empty";
    private static final String DUPLICATE_EMAIL = "DuplicateEmail";
    private static final String ANOTHER_EMPLOYEE_EXIST_WITH_EMAIL = "Another employee exists with this email.";

    private InviteEmployeeRepository inviteEmployeeRepository;
    private EmployeeRepository employeeRepository;
    private CompanyFeaturesRepository companyFeaturesRepository;
    private SecurityUtil securityUtil;
    private RoleRepository roleRepository;
    private CompanyRepository companyRepository;
    private ShopRepository shopRepository;
    private AmazonServiceManager amazonServiceManager;
    private ConnectConfiguration connectConfiguration;

    @Inject
    public InviteEmployeeServiceImpl(Provider<ConnectAuthToken> token, InviteEmployeeRepository inviteEmployeeRepository,
                                     EmployeeRepository employeeRepository, CompanyFeaturesRepository companyFeaturesRepository, SecurityUtil securityUtil, RoleRepository roleRepository, CompanyRepository companyRepository, ShopRepository shopRepository, AmazonServiceManager amazonServiceManager, ConnectConfiguration connectConfiguration) {
        super(token);
        this.inviteEmployeeRepository = inviteEmployeeRepository;
        this.employeeRepository = employeeRepository;
        this.companyFeaturesRepository = companyFeaturesRepository;
        this.securityUtil = securityUtil;
        this.roleRepository = roleRepository;
        this.companyRepository = companyRepository;
        this.shopRepository = shopRepository;
        this.amazonServiceManager = amazonServiceManager;
        this.connectConfiguration = connectConfiguration;
    }

    /**
     * This method send invitation to invited employee
     *
     * @param request
     */
    @Override
    public InviteEmployee inviteEmployee(EmployeeInviteRequest request) {
        String email = request.getEmail().toLowerCase().trim();

        CompanyFeatures companyFeatures = companyFeaturesRepository.getCompanyFeature(token.getCompanyId());
        long count = employeeRepository.count(token.getCompanyId());

        if (count >= companyFeatures.getMaxEmployees()) {
            throw new BlazeQuickPinExistException("EmployeeCount", "Maximum employees allowed reached.");
        }

        // Check for invalid email
        if (!EmailValidator.getInstance().isValid(email)) {
            throw new BlazeQuickPinExistException("InvalidEmail", "Please specify a valid email.");
        }

        Employee employee = employeeRepository.getEmployeeByEmail(email);
        if (employee != null) {
            throw new BlazeQuickPinExistException(DUPLICATE_EMAIL, ANOTHER_EMPLOYEE_EXIST_WITH_EMAIL);
        }

        InviteEmployee inviteEmployee = inviteEmployeeRepository.getEmployeeByEmail(email);
        if (inviteEmployee != null) {
            throw new BlazeQuickPinExistException(DUPLICATE_EMAIL, ANOTHER_EMPLOYEE_EXIST_WITH_EMAIL);
        }

        inviteEmployee = new InviteEmployee();
        inviteEmployee.prepare(token.getCompanyId());
        inviteEmployee.setFirstName(request.getFirstName());
        inviteEmployee.setLastName(request.getLastName());
        inviteEmployee.setEmail(email);
        inviteEmployee.setPin(securityUtil.generatePin());

        Address address = new Address();
        address.setAddress(request.getAddress());
        address.setCity(request.getCity());
        address.setState(request.getState());
        address.setZipCode(request.getZipcode());
        address.prepare(token.getCompanyId());
        inviteEmployee.setAddress(address);

        if (CollectionUtils.isEmpty(request.getShops())) {
            request.setShops(Lists.newArrayList(token.getShopId()));
        }

        inviteEmployee.setShops(request.getShops());
        Role role;
        if (StringUtils.isNotEmpty(request.getRoleId())) {
            role = roleRepository.get(token.getCompanyId(), request.getRoleId());
            if (role == null) {
                throw new BlazeInvalidArgException("Role", "Role does not exist.");
            }
            inviteEmployee.setRoleId(role.getId());
        } else {
            throw new BlazeInvalidArgException("RoleId", "Role id is missing.");
        }

        inviteEmployee.setPassword(null);
        inviteEmployee = inviteEmployeeRepository.save(inviteEmployee);
        inviteEmployee.setPin(null);

        inviteEmployee.setRole(role);


        Company company = companyRepository.getById(token.getCompanyId());
        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());
        String body = getInviteEmailBody(company, shop, inviteEmployee);

        amazonServiceManager.sendEmail("support@blaze.me", inviteEmployee.getEmail(), connectConfiguration.getAppName() + " - Invitation", body, null, null, "Blaze Support");

        return inviteEmployee;
    }

    @Override
    public InviteEmployee registerInvitee(RegisterInviteeRequest request) {
        EmployeeInviteToken inviteToken = securityUtil.decryptEmployeeInviteToken(request.getToken());

        if (inviteToken.isValid()) {
            InviteEmployee dbInviteEmployee = inviteEmployeeRepository.get(inviteToken.getCompanyId(), inviteToken.getEmployeeId());

            if (dbInviteEmployee == null) {
                throw new BlazeInvalidArgException(EMPLOYEE, EMPLOYEE_DOEST_NOT_EXIST);
            }

            if (!StringUtils.isBlank(dbInviteEmployee.getPassword())) {
                throw new BlazeInvalidArgException(EMPLOYEE, EMPLOYEE_ALREADY_REGISTERED);
            }

            if (StringUtils.isNotEmpty(request.getPassword())) {
                dbInviteEmployee.setPassword(securityUtil.encryptPassword(request.getPassword()));
            }

            if (!StringUtils.isNumeric(request.getPin())) {
                throw new BlazeInvalidArgException(EMPLOYEE_PIN, EMPLOYEE_PIN_NUMERIC);
            }

            Employee anotherEmployee = employeeRepository.getEmployeeByQuickPin(inviteToken.getCompanyId(), request.getPin());
            if (anotherEmployee != null && !anotherEmployee.getId().equalsIgnoreCase(dbInviteEmployee.getId())) {
                throw new BlazeInvalidArgException(EMPLOYEE_PIN, EMPLOYEE_PIN_ALREADY_USED);
            }

            Employee employeeByEmail = employeeRepository.getEmployeeByEmail(dbInviteEmployee.getEmail());
            if (employeeByEmail != null) {
                throw new BlazeQuickPinExistException(DUPLICATE_EMAIL, ANOTHER_EMPLOYEE_EXIST_WITH_EMAIL);
            }

            Shop shop = shopRepository.getById(inviteToken.getShopId());

            dbInviteEmployee.setPin(request.getPin());

            Role role = roleRepository.get(inviteToken.getCompanyId(), dbInviteEmployee.getRoleId());
            if (role.getName().equalsIgnoreCase("Delivery Driver")) {
                if (StringUtils.isBlank(request.getDlExpirationDate())
                        && StringUtils.isBlank(request.getDriversLicense())
                        && StringUtils.isBlank(request.getVehicleMake())) {
                    throw new BlazeInvalidArgException(DRIVER_DELIVERY, DRIVER_DELIVERY_NOT_COMPLETE);
                }

                dbInviteEmployee.setDlExpirationDate(request.getDlExpirationDate());
                dbInviteEmployee.setDriversLicense(request.getDriversLicense());
                dbInviteEmployee.setVehicleMake(request.getVehicleMake());
            }

            Employee employee = new Employee();
            try {
                BeanUtils.copyProperties(employee, dbInviteEmployee);
                //Set employee id null because InviteEmployee's id also copied from above action
                employee.setId(null);
                employee.prepare(inviteToken.getCompanyId());
                if (CollectionUtils.isEmpty(employee.getShops()) && shop != null) {
                    employee.setShops(Lists.newArrayList(shop.getId()));
                }

            } catch (Exception e) {
                LOGGER.error("Error while creating employee from invite employee " + e);
                throw new BlazeInvalidArgException(EMPLOYEE, "Error while creating employee from invite employee");
            }

            LinkedHashSet<CompanyFeatures.AppTarget> appAccessList = new LinkedHashSet<>();
            appAccessList.add(CompanyFeatures.AppTarget.AuthenticationApp);
            if ("Admin".equalsIgnoreCase(role.getName())) {
                appAccessList.addAll(Lists.newArrayList(CompanyFeatures.AppTarget.values()));
            } else {
                List<ObjectId> shopIds = new ArrayList<>();
                for (String shopId : employee.getShops()) {
                    if (shopId != null && ObjectId.isValid(shopId)) {
                        shopIds.add(new ObjectId(shopId));
                    }
                }
                // Add access configs for all related shops
                Iterable<Shop> shops = shopRepository.findItemsIn(inviteToken.getCompanyId(),shopIds);
                for (Shop cShop : shops) {
                    appAccessList.add(cShop.getAppTarget());
                }
            }
            employee.setAppAccessList(appAccessList);

            Employee savedEmployee = employeeRepository.save(employee);
            if (savedEmployee != null) {
                dbInviteEmployee.setCompanyId(inviteToken.getCompanyId());
                dbInviteEmployee.setRegistered(true);
                dbInviteEmployee.setEmployeeId(savedEmployee.getId());
                dbInviteEmployee.setRegistrationDate(DateTime.now().getMillis());
                return inviteEmployeeRepository.update(request.getCompanyId(), dbInviteEmployee.getId(), dbInviteEmployee);
            } else {
                throw new BlazeInvalidArgException(EMPLOYEE, "Error while creating employee from invite employee");
            }
        }

        throw new BlazeInvalidArgException(INVITE_TOKEN, INVITE_TOKEN_INVALID);
    }

    @Override
    public InviteEmployee getInviteeInfo(EmployeeInviteTokenRequest token) {
        if (StringUtils.isBlank(token.getToken()))
            throw new BlazeInvalidArgException(INVITE_TOKEN, INVITE_TOKEN_NULL);

        EmployeeInviteToken employeeInviteToken = securityUtil.decryptEmployeeInviteToken(token.getToken());

        if (!Objects.isNull(employeeInviteToken) && employeeInviteToken.isValid()) {
            InviteEmployee dbInviteEmployee = inviteEmployeeRepository.get(employeeInviteToken.getCompanyId(), employeeInviteToken.getEmployeeId());

            if (dbInviteEmployee == null) {
                throw new BlazeInvalidArgException(EMPLOYEE, EMPLOYEE_DOEST_NOT_EXIST);
            }

            if (!StringUtils.isBlank(dbInviteEmployee.getPassword())) {
                throw new BlazeInvalidArgException(EMPLOYEE, EMPLOYEE_ALREADY_REGISTERED);
            }

            Role role = roleRepository.get(employeeInviteToken.getCompanyId(), dbInviteEmployee.getRoleId());
            dbInviteEmployee.setRoleId(role.getId());
            dbInviteEmployee.setRole(role);
            dbInviteEmployee.setPin(null);
            dbInviteEmployee.setPassword(null);
            return dbInviteEmployee;
        }

        throw new BlazeInvalidArgException(INVITE_TOKEN, INVITE_TOKEN_INVALID);
    }

    @Override
    public SearchResult<InviteEmployee> getInviteEmployeeList(int start, int limit) {

        if (limit <= 0 || limit > 200) {
            limit = 200;
        }
        SearchResult<InviteEmployee> result;
        String projection = getPermissionProjection();
        result = inviteEmployeeRepository.findItems(token.getCompanyId(), start, limit, projection);

        processEmployeesResult(result);
        return result;
    }

    private void processEmployeesResult(SearchResult<InviteEmployee> result) {
        HashMap<String, Role> roleHashMap = roleRepository.listAsMap(token.getCompanyId());
        for (Employee employee : result.getValues()) {
            Role role = roleHashMap.get(employee.getRoleId());
            employee.setRole(role);
        }
    }

    private String getPermissionProjection() {
        String userId = token.getActiveTopUser().getUserId();
        Employee employee = employeeRepository.get(token.getCompanyId(), userId);
        Role role = roleRepository.get(token.getCompanyId(), employee.getRoleId());
        String projection = "{password:0,pin:0}";
        if (role != null) {
            boolean isManager = role.getPermissions().contains(Role.Permission.WebEmployeeManage);
            if (isManager) {
                projection = "{password:0}";
            }
        }
        return projection;
    }

    @Override
    public InviteEmployee getInviteEmployeeById(String employeeId) {
        InviteEmployee inviteEmployee = inviteEmployeeRepository.get(token.getCompanyId(), employeeId);
        if (inviteEmployee == null) {
            throw new BlazeInvalidArgException("Invite Employee", "Invite employee not found");
        }
        return inviteEmployee;
    }

    private String getInviteEmailBody(Company company, Shop shop, InviteEmployee employee) {

        String body;
        try {
            InputStream inputStream = PurchaseOrderServiceImpl.class.getResourceAsStream("/employee_invite.html");
            StringWriter writer = new StringWriter();
            try {
                IOUtils.copy(inputStream, writer, "UTF-8");
            } catch (IOException e) {
                LOGGER.error("Error in emailAccountingBody : " + e);
            }
            body = writer.toString();
            body = body.replaceAll("==preferredEmailColor==", company.getPreferredEmailColor());
            body = body.replaceAll("==shopName==", shop.getName());
            body = body.replaceAll("==employeeName==", employee.getFirstName() + " " + employee.getLastName());
            body = body.replaceAll("==employeeRole==", employee.getRole().getName());

            EmployeeInviteToken token = new EmployeeInviteToken();
            token.setCompanyId(company.getId());
            token.setInitDate(DateTime.now().getMillis());
            token.setExpirationDate(DateTime.now().plusDays(1).getMillis());
            token.setEmployeeId(employee.getId());
            token.setShopId(shop.getId());

            String employeeInviteToken = URLEncoder.encode(securityUtil.createEmployeeInviteToken(token), "UTF-8");

            body = body.replaceAll("==actionUrl==", connectConfiguration.getAppWebsiteURL() + "/employeesignup?token=" + employeeInviteToken);
        } catch (Exception e) {
            LOGGER.error(INVITE_EMAIL_ERROR, e);
            throw new BlazeOperationException(INVITE_EMAIL_ERROR, e);
        }

        return body;
    }

    @Override
    public void resendInvite(String inviteEmployeeId) {
        InviteEmployee inviteEmployee = inviteEmployeeRepository.get(token.getCompanyId(), inviteEmployeeId);
        if (inviteEmployee == null)
            throw new BlazeInvalidArgException("Invite Employee", "Invite Employee not found");

        Role role;
        if (StringUtils.isNotEmpty(inviteEmployee.getRoleId())) {
            role = roleRepository.get(token.getCompanyId(), inviteEmployee.getRoleId());
            if (role == null) {
                throw new BlazeInvalidArgException("Role", "Role does not exist.");
            }
            inviteEmployee.setRoleId(role.getId());
        } else {
            throw new BlazeInvalidArgException("RoleId", "Role id is missing.");
        }

        inviteEmployee.setRole(role);
        Company company = companyRepository.getById(token.getCompanyId());
        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());
        String body = getInviteEmailBody(company, shop, inviteEmployee);
        amazonServiceManager.sendEmail("support@blaze.me", inviteEmployee.getEmail(), connectConfiguration.getAppName() + " - Invitation", body, null, null, "Blaze Support");


    }
}
