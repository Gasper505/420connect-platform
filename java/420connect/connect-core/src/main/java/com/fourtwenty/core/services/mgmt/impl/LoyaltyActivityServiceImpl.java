package com.fourtwenty.core.services.mgmt.impl;

import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.loyalty.LoyaltyActivityLog;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.CompanyRepository;
import com.fourtwenty.core.domain.repositories.dispensary.MemberRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.domain.repositories.loyalty.LoyaltyUsageLogRepository;
import com.fourtwenty.core.services.mgmt.LoyaltyActivityService;
import com.fourtwenty.core.util.NumberUtils;
import com.google.inject.Inject;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;

import java.math.BigDecimal;

/**
 * Created by mdo on 8/8/17.
 */
public class LoyaltyActivityServiceImpl implements LoyaltyActivityService {
    private static final Log LOG = LogFactory.getLog(LoyaltyActivityServiceImpl.class);


    @Inject
    LoyaltyUsageLogRepository usageLogRepository;
    @Inject
    CompanyRepository companyRepository;
    @Inject
    MemberRepository memberRepository;
    @Inject
    TransactionRepository transactionRepository;


    @Override
    public LoyaltyActivityLog addLoyaltyPoints(String companyId, Transaction transaction) {
        try {
            Company company = companyRepository.getById(companyId);
            if (company == null) {
                LOG.error("RewardActivity - company does not exist.");
                return null;
            }
            if (transaction == null) {
                LOG.error("RewardActivity - transaction is null.");
                return null;
            }
            Member member = memberRepository.get(companyId, transaction.getMemberId());
            if (member == null) {
                LOG.error("RewardActivity - member is null.");
                return null;
            }
            if (!member.isEnableLoyalty()) {
                LOG.error("RewardActivity - member does not have loyalty reward enabled.");
                return null;
            }

            if (company.isEnableLoyalty()) {

                boolean hasEarnedPrevious = usageLogRepository.hasEarnedForTransaction(companyId, transaction.getShopId(), transaction.getMemberId(), transaction.getTransNo());

                if (hasEarnedPrevious) {
                    LOG.error("RewardActivity - member has previously earned from this transaction: " + transaction.getTransNo());
                    return null;
                }
                BigDecimal conversion = company.getDollarToPointRatio();
                double pointsEarned = 0;
                if (transaction.getCart() != null && company.getDollarToPointRatio().doubleValue() > 0) {
                    BigDecimal salesToConvert = new BigDecimal(0);

                    if (company.getLoyaltyAccrueOpt() == Company.LoyaltyAccrueOption.Subtotal) {
                        salesToConvert = transaction.getCart().getSubTotal();
                    } else if (company.getLoyaltyAccrueOpt() == Company.LoyaltyAccrueOption.SubtotalWithDiscount) {
                        salesToConvert = transaction.getCart().getSubTotalDiscount();
                    } else if (company.getLoyaltyAccrueOpt() == Company.LoyaltyAccrueOption.FinalTotal) {
                        salesToConvert = transaction.getCart().getTotal();
                    }
                    pointsEarned = salesToConvert.doubleValue() * (1 / conversion.doubleValue());

                }
                pointsEarned = NumberUtils.round(pointsEarned, 2);

                if (pointsEarned > 0) {

                    LoyaltyActivityLog log = new LoyaltyActivityLog();
                    log.prepare(companyId);
                    log.setShopId(transaction.getShopId());
                    log.setActivityDate(DateTime.now().getMillis());
                    log.setActivityType(LoyaltyActivityLog.LoyaltyActivityType.Accrue);
                    log.setAmount(new BigDecimal(pointsEarned));
                    log.setEmployeeId(transaction.getAssignedEmployeeId());
                    log.setMemberId(transaction.getMemberId());
                    log.setTransNo(transaction.getTransNo());
                    usageLogRepository.save(log);

                    LOG.info(String.format("Member %s earned %.1f for trans: %s", transaction.getMemberId(), pointsEarned, transaction.getTransNo()));
                    memberRepository.addLoyaltyPoints(companyId, transaction.getMemberId(), pointsEarned);
                    memberRepository.addLifetimePoints(companyId, transaction.getMemberId(), pointsEarned);

                    return log;
                }

            } else {
                LOG.error("RewardActivity - company does not have paymentcard enabled.");
            }

        } catch (Exception e) {
            LOG.error("Error adding paymentcard activity", e);
        }
        return null;
    }

    @Override
    public LoyaltyActivityLog refundLoyaltyPoints(String companyId, Transaction transaction) {
        try {
            Company company = companyRepository.getById(companyId);
            if (company == null) {
                LOG.error("RewardActivity - company does not exist.");
                return null;
            }
            if (transaction == null) {
                LOG.error("RewardActivity - transaction is null.");
                return null;
            }
            Member member = memberRepository.get(companyId, transaction.getMemberId());
            if (member == null) {
                LOG.error("RewardActivity - member is null.");
                return null;
            }
            if (!member.isEnableLoyalty()) {
                LOG.error("RewardActivity - member does not have paymentcard enabled.");
                return null;
            }

            if (company.isEnableLoyalty()) {
                // Calculate how much was earned previously
                BigDecimal previousPoints = transaction.getPointsEarned();

                // Calculate how much member earned after refund

                // subtract the amt earned
                BigDecimal conversion = company.getDollarToPointRatio();
                double pointsEarned = 0;
                if (transaction.getCart() != null && company.getDollarToPointRatio().doubleValue() > 0) {
                    BigDecimal salesToConvert = transaction.getCart().getSubTotal();
                    pointsEarned = salesToConvert.doubleValue() * (1 / conversion.doubleValue());

                }
                pointsEarned = NumberUtils.round(pointsEarned, 1);

                double deducted = previousPoints.doubleValue() - pointsEarned;
                deducted = NumberUtils.round(deducted, 1);

                LoyaltyActivityLog log = new LoyaltyActivityLog();
                log.prepare(companyId);
                log.setShopId(transaction.getShopId());
                log.setActivityDate(DateTime.now().getMillis());
                log.setActivityType(LoyaltyActivityLog.LoyaltyActivityType.Accrue);
                log.setAmount(new BigDecimal(-deducted));
                log.setEmployeeId(transaction.getAssignedEmployeeId());
                log.setMemberId(transaction.getMemberId());
                log.setTransNo(transaction.getTransNo());
                usageLogRepository.save(log);

                transactionRepository.updatePointsEarned(transaction.getCompanyId(), transaction.getId(), new BigDecimal(pointsEarned));

                LOG.info(String.format("Member %s deducted %.1f for trans: %s", transaction.getMemberId(), deducted, transaction.getTransNo()));
                memberRepository.addLoyaltyPoints(companyId, transaction.getMemberId(), -deducted);
                memberRepository.addLifetimePoints(companyId, transaction.getMemberId(), -deducted);

                return log;

            }
        } catch (Exception e) {

        }
        return null;
    }

    @Override
    public boolean isUsed(String companyId, String memberId, String rewardId) {
        return usageLogRepository.hasUsedForMemberReward(companyId, memberId, rewardId);
    }
}
