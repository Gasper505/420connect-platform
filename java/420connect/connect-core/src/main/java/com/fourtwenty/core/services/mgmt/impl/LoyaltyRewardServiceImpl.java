package com.fourtwenty.core.services.mgmt.impl;

import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.generic.CompanyUniqueSequence;
import com.fourtwenty.core.domain.models.loyalty.DiscountVersion;
import com.fourtwenty.core.domain.models.loyalty.LoyaltyActivityLog;
import com.fourtwenty.core.domain.models.loyalty.LoyaltyReward;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.repositories.dispensary.CompanyUniqueSequenceRepository;
import com.fourtwenty.core.domain.repositories.dispensary.EmployeeRepository;
import com.fourtwenty.core.domain.repositories.dispensary.MemberRepository;
import com.fourtwenty.core.domain.repositories.loyalty.DiscountVersionRepository;
import com.fourtwenty.core.domain.repositories.loyalty.LoyaltyRewardRepository;
import com.fourtwenty.core.domain.repositories.loyalty.LoyaltyUsageLogRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.rest.dispensary.requests.promotions.RewardAddRequest;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.company.LoyaltyActivityLogResult;
import com.fourtwenty.core.rest.dispensary.results.company.LoyaltyMemberReward;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.mgmt.LoyaltyRewardService;
import com.fourtwenty.core.util.TextUtil;
import com.github.slugify.Slugify;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.google.inject.Provider;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;

/**
 * Created by mdo on 8/3/17.
 */
public class LoyaltyRewardServiceImpl extends AbstractAuthServiceImpl implements LoyaltyRewardService {
    @Inject
    LoyaltyRewardRepository rewardRepository;
    @Inject
    LoyaltyUsageLogRepository usageLogRepository;
    @Inject
    MemberRepository memberRepository;
    @Inject
    CompanyUniqueSequenceRepository companyUniqueSequenceRepository;
    @Inject
    EmployeeRepository employeeRepository;
    @Inject
    DiscountVersionRepository discountVersionRepository;


    @Inject
    public LoyaltyRewardServiceImpl(Provider<ConnectAuthToken> tokenProvider) {
        super(tokenProvider);
    }

    @Override
    public LoyaltyReward addReward(RewardAddRequest request) {
        // Make sure only one name exists
        String slug = (new Slugify()).slugify(request.getName().trim());
        boolean exists = rewardRepository.existsWithSlug(token.getCompanyId(), token.getShopId(), slug);
        if (exists) {
            throw new BlazeInvalidArgException("Reward", "A reward with similar name exists.");
        }

        CompanyUniqueSequence companyUniqueSequence = companyUniqueSequenceRepository.getNewIdentifier(token.getCompanyId(), "LoyaltyReward");
        LoyaltyReward loyaltyReward = new LoyaltyReward();
        loyaltyReward.prepare(token.getCompanyId());
        loyaltyReward.setShopId(token.getShopId());
        loyaltyReward.setRewardNo(companyUniqueSequence.getCount() + "");
        loyaltyReward.setSlug(slug);
        loyaltyReward.setName(request.getName().trim());
        loyaltyReward.setActive(true);
        loyaltyReward.setPoints(request.getPoints());
        loyaltyReward.setPublished(false);

        LoyaltyReward dbLoyaltyReward = rewardRepository.save(loyaltyReward);

        //Save new reward in discount version collection
        DiscountVersion discountVersion = new DiscountVersion(dbLoyaltyReward);
        discountVersionRepository.save(discountVersion);

        return loyaltyReward;
    }

    @Override
    public LoyaltyReward getRewardById(String rewardId) {
        return rewardRepository.get(token.getCompanyId(), rewardId);
    }

    @Override
    public SearchResult<LoyaltyReward> getAllRewards(int start, int limit) {
        if (limit >= 200 || limit <= 0) {
            limit = 200;
        }
        SearchResult<LoyaltyReward> rewardSearchResult = rewardRepository.findItems(token.getCompanyId(), token.getShopId(), start, limit);

        return rewardSearchResult;
    }

    @Override
    public LoyaltyReward updateReward(String rewardId, LoyaltyReward reward) {
        LoyaltyReward dbReward = rewardRepository.get(token.getCompanyId(), rewardId);
        if (dbReward == null) {
            throw new BlazeInvalidArgException("Reward", "Loyalty reward does not exists.");
        }
        // check if slug changed
        String slug = (new Slugify()).slugify(reward.getName().trim());
        if (!dbReward.getSlug().equalsIgnoreCase(slug)) {
            // slug has changed
            boolean exists = rewardRepository.existsWithSlug(token.getCompanyId(), token.getShopId(), slug);
            if (exists) {
                throw new BlazeInvalidArgException("Reward", "A reward with similar name exists.");
            }
        }
        dbReward.setSlug(slug);
        dbReward.setName(reward.getName().trim());
        dbReward.setActive(reward.isActive());
        dbReward.setPoints(reward.getPoints());
        dbReward.setCategoryIds(reward.getCategoryIds());
        dbReward.setPromotionId(reward.getPromotionId());
        dbReward.setDetail(reward.getDetail());
        dbReward.setDiscountAmt(reward.getDiscountAmt());
        dbReward.setDiscountType(reward.getDiscountType());
        dbReward.setDiscountUnitType(reward.getDiscountUnitType());
        dbReward.setProductIds(reward.getProductIds());
        dbReward.setVendorIds(reward.getVendorIds());
        dbReward.setProductTags(reward.getProductTags());

        dbReward.setStartDate(reward.getStartDate());
        dbReward.setEndDate(reward.getEndDate());
        if (reward.getRewardTarget() != null) {
            dbReward.setRewardTarget(reward.getRewardTarget());
        } else {
            dbReward.setRewardTarget(LoyaltyReward.LoyaltyRewardTarget.WholeCart);
        }

        if (dbReward.getDiscountType() == OrderItem.DiscountType.Percentage) {
            if (dbReward.getDiscountAmt().doubleValue() < 0 || dbReward.getDiscountAmt().doubleValue() > 100) {
                throw new BlazeInvalidArgException("Reward", "Percentage discounts should be between 0 and 100.");
            }
        }

        rewardRepository.update(token.getCompanyId(), dbReward.getId(), dbReward);

        //Save new reward in discount version collection
        DiscountVersion discountVersion = new DiscountVersion(dbReward);
        discountVersionRepository.save(discountVersion);

        return dbReward;
    }

    @Override
    public void deleteReward(String rewardId) {
        // Rename Reward before deleting
        LoyaltyReward reward = rewardRepository.get(token.getCompanyId(), rewardId);
        if (reward == null) {
            throw new BlazeInvalidArgException("Reward", "Loyalty reward does not exists.");
        }
        reward.setName(reward.getName() + "--deleted--" + DateTime.now().getMillis());
        reward.setSlug(reward.getSlug() + "--deleted--" + DateTime.now().getMillis());
        reward.setActive(false);
        reward.setDeleted(true);
        reward.setPublished(false);
        rewardRepository.update(rewardId, reward);
    }

    @Override
    public LoyaltyReward publishReward(String rewardId) {
        LoyaltyReward dbReward = rewardRepository.get(token.getCompanyId(), rewardId);
        if (dbReward == null) {
            throw new BlazeInvalidArgException("Reward", "Loyalty reward does not exists.");
        }
        if (dbReward.isPublished()) {
            return dbReward;
        }

        if (dbReward.isActive() == false) {
            throw new BlazeInvalidArgException("Reward", "Please set reward to active before publishing.");
        }

        dbReward.setPublished(true);
        dbReward.setPublishedDate(DateTime.now().getMillis());
        dbReward.setUnpublishedDate(null);
        rewardRepository.update(token.getCompanyId(), dbReward.getId(), dbReward);
        return dbReward;
    }

    @Override
    public LoyaltyReward unpublishReward(String rewardId) {
        LoyaltyReward dbReward = rewardRepository.get(token.getCompanyId(), rewardId);
        if (dbReward == null) {
            throw new BlazeInvalidArgException("Reward", "Loyalty reward does not exists.");
        }
        if (!dbReward.isPublished()) {
            return dbReward;
        }
        dbReward.setPublished(false);
        dbReward.setPublishedDate(null);
        dbReward.setUnpublishedDate(DateTime.now().getMillis());

        rewardRepository.update(token.getCompanyId(), dbReward.getId(), dbReward);
        return dbReward;
    }

    @Override
    public SearchResult<LoyaltyMemberReward> getRewardsForMembers(String memberId) {
        Member member = memberRepository.get(token.getCompanyId(), memberId);
        return getRewardsForMembers(token.getCompanyId(), token.getShopId(), member);
    }

    @Override
    public SearchResult<LoyaltyMemberReward> getRewardsForMembers(String companyId, String shopId, Member member) {
        Iterable<LoyaltyReward> rewards = rewardRepository.getPublishedRewards(companyId, shopId);

        SearchResult<LoyaltyMemberReward> memberRewards = new SearchResult<>();

        for (LoyaltyReward reward : rewards) {
            LoyaltyMemberReward memberReward = new LoyaltyMemberReward();

            memberReward.setRewardName(reward.getName());
            memberReward.setRewardId(reward.getId());
            memberReward.setRewardDescription(reward.getDetail());
            memberReward.setAvailableDate(reward.getStartDate());
            memberReward.setPointsRequired(reward.getPoints());
            memberReward.setRewardState(LoyaltyMemberReward.MemberRewardState.Locked);
            if (member != null) {
                memberReward.setMemberId(member.getId());

                BigDecimal memberPoints = member.getLoyaltyPoints();
                if (reward.getPoints() <= memberPoints.doubleValue()) {
                    memberReward.setRewardState(LoyaltyMemberReward.MemberRewardState.Available);
                }
            }

            memberRewards.getValues().add(memberReward);


            // Configure discount
            String discountInfo = "";
            if (reward.getDiscountType() == OrderItem.DiscountType.Cash) {
                discountInfo = TextUtil.toCurrency(reward.getDiscountAmt().doubleValue(), null) + " off";
            } else {
                // handle percentage%
                discountInfo = String.format("%.2f%% off %s", reward.getDiscountAmt().doubleValue(), reward.getDiscountUnitType().weightName);
            }

            memberReward.setDiscountInfo(discountInfo);
        }
        memberRewards.setTotal((long) memberRewards.getValues().size());

        return memberRewards;
    }

    @Override
    public DateSearchResult<LoyaltyActivityLog> getLoyaltyUsageReward(long afterDate, long beforeDate) {
        if (beforeDate <= 0) {
            beforeDate = DateTime.now().getMillis();
        }

        return usageLogRepository.getUsageLogsForDate(token.getCompanyId(), token.getShopId(), afterDate, beforeDate);
    }

    @Override
    public DateSearchResult<LoyaltyReward> getRewardsByDate(long afterDate, long beforeDate) {
        if (beforeDate <= 0) {
            beforeDate = DateTime.now().getMillis();
        }
        return rewardRepository.findItemsWithDate(token.getCompanyId(), token.getShopId(), afterDate, beforeDate);
    }

    @Override
    public SearchResult<LoyaltyActivityLogResult> getLoyaltyUsageReward(LoyaltyActivityLog.LoyaltyActivityType activityType, int start, int limit) {
        if (limit >= 500 || limit <= 0) {
            limit = 500;
        }
        SearchResult<LoyaltyActivityLogResult> results = usageLogRepository.getUsageLogsForType(token.getCompanyId(), token.getShopId(), activityType, start, limit, LoyaltyActivityLogResult.class);

        assignDependencies(results.getValues());
        return results;
    }

    private void assignDependencies(List<LoyaltyActivityLogResult> items) {
        LinkedHashSet<ObjectId> memberIds = new LinkedHashSet<>();
        LinkedHashSet<ObjectId> employeeIds = new LinkedHashSet<>();

        for (LoyaltyActivityLogResult log : items) {
            if (log.getMemberId() != null && ObjectId.isValid(log.getMemberId())) {
                memberIds.add(new ObjectId(log.getMemberId()));
            }
            if (log.getEmployeeId() != null && ObjectId.isValid(log.getEmployeeId())) {
                employeeIds.add(new ObjectId(log.getEmployeeId()));
            }
        }

        HashMap<String, Member> memberHashMap = memberRepository.findItemsInAsMap(token.getCompanyId(), Lists.newArrayList(memberIds));
        HashMap<String, Employee> employeeHashMap = employeeRepository.findItemsInAsMap(token.getCompanyId(), Lists.newArrayList(employeeIds));
        for (LoyaltyActivityLogResult log : items) {
            String memberName = "";
            String employeeName = "";
            Member member = memberHashMap.get(log.getMemberId());
            if (member != null) {
                memberName = member.getFirstName() + " " + member.getLastName();
            }
            log.setMemberName(memberName);

            Employee emp = employeeHashMap.get(log.getEmployeeId());
            if (emp != null) {
                employeeName = emp.getFirstName() + " " + emp.getLastName();
            }
            log.setEmployeeName(employeeName);
        }

    }
}
