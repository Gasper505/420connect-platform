package com.fourtwenty.core.services.mgmt.impl;

import com.esotericsoftware.kryo.Kryo;
import com.fourtwenty.core.domain.models.App;
import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.models.company.CompanyFeatures;
import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.marketing.MarketingJob;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.rest.dispensary.requests.marketing.MarketingJobAddRequest;
import com.fourtwenty.core.rest.dispensary.requests.marketing.MarketingJobMemberCountRequest;
import com.fourtwenty.core.rest.dispensary.results.MarketingMemberCountResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.common.AssetStreamResult;
import com.fourtwenty.core.rest.marketing.MarketingJobResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.mgmt.MarketingJobService;
import com.fourtwenty.core.services.thirdparty.AmazonS3Service;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.core.util.NumberUtils;
import com.google.common.collect.Lists;
import com.google.inject.Provider;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.math.BigDecimal;
import java.util.*;

/**
 * Created by mdo on 7/6/17.
 */
public class MarketingJobServiceImpl extends AbstractAuthServiceImpl implements MarketingJobService {

    private static final Logger LOG = LoggerFactory.getLogger(MarketingJobServiceImpl.class);

    @Inject
    MarketingJobRepository marketingJobRepository;
    @Inject
    EmployeeRepository employeeRepository;
    @Inject
    MemberRepository memberRepository;
    @Inject
    AppRepository appRepository;
    @Inject
    CompanyFeaturesRepository companyFeaturesRepository;
    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private ProductRepository productRepository;
    @Inject
    private AmazonS3Service amazonS3Service;

    @Inject
    public MarketingJobServiceImpl(Provider<ConnectAuthToken> tokenProvider) {
        super(tokenProvider);
    }

    @Override
    public SearchResult<MarketingJobResult> getCompletedJobs(int start, int limit) {
        if (limit > 200) {
            limit = 200;
        }

        SearchResult<MarketingJobResult> completedJobs = marketingJobRepository.findMarketingJobsByStatus(token.getCompanyId(),
                token.getShopId(),
                Lists.newArrayList(MarketingJob.MarketingJobStatus.Completed,
                        MarketingJob.MarketingJobStatus.InProcess,
                        MarketingJob.MarketingJobStatus.Failed),
                start, limit,
                MarketingJobResult.class);

        applyDependencies(completedJobs, false);
        return completedJobs;
    }


    @Override
    public SearchResult<MarketingJobResult> getPendingJobs(int start, int limit) {
        if (limit > 200) {
            limit = 200;
        }

        SearchResult<MarketingJobResult> pendingJobs = marketingJobRepository.findMarketingJobsByStatus(token.getCompanyId(),
                token.getShopId(),
                Lists.newArrayList(MarketingJob.MarketingJobStatus.Pending),
                start, limit,
                MarketingJobResult.class);

        applyDependencies(pendingJobs, true);
        return pendingJobs;
    }

    private long getMembersCount(final MarketingJob marketingJob) {

        if (marketingJob.getRecipientType() == MarketingJob.MarketingRecipientType.AllMembers) {
            // find all member for this company
            return memberRepository.countActiveTextOptIn(marketingJob.getCompanyId(), Member.MembershipStatus.Active);
        } else if (marketingJob.getRecipientType() == MarketingJob.MarketingRecipientType.InactiveMembers) {

            DateTime daysOld = DateTime.now().withTimeAtStartOfDay().minusDays(marketingJob.getInactiveDays());
            return memberRepository.getInactiveMembersWithDaysCount(marketingJob.getCompanyId(), daysOld.getMillis());

        } else if (marketingJob.getRecipientType() == MarketingJob.MarketingRecipientType.MemberGroup) {

            return memberRepository.getMemberByMemberGroupsCountTextOptIn(marketingJob.getCompanyId(),
                    marketingJob.getMemberGroupIds());
        }

        return 0;
    }

    private void applyDependencies(SearchResult<MarketingJobResult> jobResultSearchResult, boolean pending) {
        App app = appRepository.getAppByName(App.BLAZE_APP_NAME);

        HashMap<String, Employee> employeeHashMap = employeeRepository.listAllAsMap(token.getCompanyId());
        for (MarketingJobResult result : jobResultSearchResult.getValues()) {
            result.setEmployee(employeeHashMap.get(result.getReqEmployeeId()));

            if (pending) {
                long pendingCount = getMembersCount(result);
                double cost = 0;
                if (app != null) {
                    if (result.getJobType() == MarketingJob.MarketingJobType.SMS) {
                        cost = pendingCount * app.getSmsCost().doubleValue() / 100;
                    } else {
                        cost = pendingCount * app.getEmailCost().doubleValue() / 100;
                    }
                }
                result.setPendingCost(new BigDecimal(cost));
                result.setPendingCount((int) pendingCount);
            }
        }
    }


    @Override
    public MarketingJob createMarketingJob(MarketingJobAddRequest addRequest) {

        if (addRequest.getRecipientType() == MarketingJob.MarketingRecipientType.SpecificMembers && addRequest.getMemberAsset() == null) {
            throw new BlazeInvalidArgException("Marketing Job", "Asset cannot be blank.");

        }

        MarketingJob marketingJob = new MarketingJob();
        marketingJob.prepare(token.getCompanyId());
        marketingJob.setShopId(token.getShopId());
        marketingJob.setMemberGroupIds(addRequest.getMemberGroupIds());
        marketingJob.setJobType(addRequest.getJobType());
        marketingJob.setRecipientType(addRequest.getRecipientType());
        marketingJob.setReqSendDate(addRequest.getReqSendDate());
        marketingJob.setReqEmployeeId(token.getActiveTopUser().getUserId());
        marketingJob.setMessage(addRequest.getMessage());
        marketingJob.setStatus(MarketingJob.MarketingJobStatus.Pending);
        marketingJob.setActive(true);
        marketingJob.setInactiveDays(addRequest.getInactiveDays());
        marketingJob.setCategoryId(addRequest.getCategoryId());
        marketingJob.setStartDate(addRequest.getStartDate());
        marketingJob.setEndDate(addRequest.getEndDate());
        marketingJob.setVendorId(addRequest.getVendorId());
        marketingJob.setNoOfVisits(addRequest.getNoOfVisits());
        marketingJob.setAmountDollars(addRequest.getAmountDollars());
        marketingJob.setType(addRequest.getType());
        marketingJob.setDayBefore(addRequest.getDayBefore());
        marketingJob.setEnable(addRequest.isEnable());
        marketingJob.setMemberTags(addRequest.getMemberTags());

        if (marketingJob.getReqSendDate() == null || marketingJob.getReqSendDate() == 0) {
            marketingJob.setReqSendDate(DateTime.now().plusMinutes(10).getMillis());
        } else if (marketingJob.getReqSendDate() != null) {
            if (marketingJob.getReqSendDate().longValue() < DateTime.now().plusMinutes(1).getMillis()) {
                marketingJob.setReqSendDate(DateTime.now().plusMinutes(10).getMillis());
            }
        }

        if (addRequest.getRecipientType() == MarketingJob.MarketingRecipientType.SpecificMembers) {
            CompanyAsset memberAsset = addRequest.getMemberAsset();
            prepareUploadMemberCsv(memberAsset);
            marketingJob.setMemberAsset(memberAsset);
        }

        // get current credits

        CompanyFeatures companyFeatures = companyFeaturesRepository.getCompanyFeature(token.getCompanyId());
        if (companyFeatures == null) {
            throw new BlazeInvalidArgException("CompanyFeatures", "Not enough credits.");
        }
        int smsAailable = companyFeatures.getSmsMarketingCredits();


        // get all pending jobs
        Iterable<MarketingJob> pendingJobs = marketingJobRepository.listAllPendingJobsByStatus(token.getCompanyId(), token.getShopId(), MarketingJob.MarketingJobStatus.Pending);
        List<MarketingJob> marketingJobs = Lists.newArrayList(pendingJobs);
        marketingJobs.add(marketingJob);


        long membersToText = getMembersTextOptInCount(marketingJobs);
        if (smsAailable < membersToText) {
            throw new BlazeInvalidArgException("CompanyFeatures", "Not enough credits for all pending jobs with this job included.");
        }

        // Save the job
        marketingJobRepository.save(marketingJob);

        //Request a send
        return marketingJob;
    }

    private long getMembersTextOptInCount(final List<MarketingJob> marketingJobs) {
        long memberGroupsCount = 0;

        for (MarketingJob marketingJob : marketingJobs) {
            memberGroupsCount += getMemberCountForJob(marketingJob.getCompanyId(), marketingJob.getRecipientType(), marketingJob.getInactiveDays(), marketingJob.getMemberGroupIds(), marketingJob.getShopId(),
                    marketingJob.getStartDate(), marketingJob.getEndDate(), marketingJob.getAmountDollars(), marketingJob.getVendorId(), marketingJob.getCategoryId(), marketingJob.getNoOfVisits(), marketingJob.getDayBefore(), marketingJob.getMemberAsset(), marketingJob.getMemberTags());
        }

        return memberGroupsCount;
    }

    private long getMemberCountForJob(String companyId, MarketingJob.MarketingRecipientType recipientType, int inactiveDays, List<String> memberGroupIds, String shopId, long startDate, long endDate, BigDecimal amountDollars, String vendorId, String categoryId, int noOfVisits, int dayBefore, CompanyAsset memberAsset, Set<String> memberTags) {
        long memberCount = 0;

        switch (recipientType) {
            case AllMembers: {
                memberCount = memberRepository.countWithOptionsTextOptIn(companyId,
                        Member.MembershipStatus.Active,
                        "{firstName:1,lastName:1,primaryPhone:1,email:1,textOptIn:1,emailOptIn:1}");
                break;
            }
            case InactiveMembers: {
                DateTime daysOld = DateTime.now().withTimeAtStartOfDay().minusDays(inactiveDays);
                memberCount += memberRepository.countInactiveMembersWithDaysTextOptIn(companyId, daysOld.getMillis());
                break;
            }
            case MemberGroup: {
                memberCount = memberRepository.getMemberByMemberGroupsCountTextOptIn(companyId, memberGroupIds);
                break;
            }
            case MembersSpentXDollarsInYDateRange: {
                Iterable<Transaction> transactions = transactionRepository.getCompletedTransactionsForDateRange(companyId, shopId, startDate, endDate);
                Map<String, BigDecimal> membersTotalAmountMap = new HashMap<>();

                Set<ObjectId> memberIds = new HashSet<>();
                double amt = NumberUtils.round(amountDollars, 2);
                for (Transaction transaction : transactions) {

                    BigDecimal total = membersTotalAmountMap.getOrDefault(transaction.getMemberId(), new BigDecimal(0));
                    total = total.add(transaction.getCart().getTotal());
                    membersTotalAmountMap.put(transaction.getMemberId(), total);

                    if (NumberUtils.round(total, 2) >= amt) {
                        memberIds.add(new ObjectId(transaction.getMemberId()));
                    }
                }

                memberCount = memberRepository.countByTextOptIn(token.getCompanyId(), Lists.newArrayList(memberIds));
                break;
            }
            case MembersHadTransactionsBetweenDateRange: {
                Iterable<Transaction> transactions = transactionRepository.getCompletedTransactionsForDateRange(companyId, shopId, startDate, endDate);

                Set<ObjectId> memberIds = new HashSet<>();
                for (Transaction transaction : transactions) {
                    memberIds.add(new ObjectId(transaction.getMemberId()));
                }

                memberCount = memberRepository.countByTextOptIn(token.getCompanyId(), Lists.newArrayList(memberIds));
                break;
            }
            case MembersBoughtProductsFromVendorXInYDateRange: {
                Iterable<Transaction> transactions = transactionRepository.getCompletedTransactionsForDateRange(companyId, shopId, startDate, endDate);

                Set<ObjectId> memberIds = new HashSet<>();
                HashMap<String, Product> productHashMap = productRepository.listAllAsMap(companyId, shopId);

                for (Transaction transaction : transactions) {
                    for (OrderItem orderItem : transaction.getCart().getItems()) {
                        Product product = productHashMap.get(orderItem.getProductId());

                        if (product != null && product.getVendorId().equals(vendorId)) {
                            memberIds.add(new ObjectId(transaction.getMemberId()));
                        }
                    }
                }

                memberCount = memberRepository.countByTextOptIn(token.getCompanyId(), Lists.newArrayList(memberIds));
                break;
            }
            case MembersBoughtXCategoryInLastYDateRange: {
                System.out.println("1: " + DateTime.now());
                Iterable<Transaction> transactions = transactionRepository.getCompletedTransactionsForDateRange(companyId, shopId, startDate, endDate);

                System.out.println("2: " + DateTime.now());
                //Set<ObjectId> productIds = new HashSet<>();
                HashMap<String, Product> productHashMap = productRepository.listAllAsMap(companyId, shopId);

                System.out.println("3: " + DateTime.now());
                Set<ObjectId> memberIds = new HashSet<>();
                for (Transaction transaction : transactions) {
                    for (OrderItem orderItem : transaction.getCart().getItems()) {
                        Product product = productHashMap.get(orderItem.getProductId());
                        if (product != null && product.getCategoryId().equals(categoryId)) {
                            memberIds.add(new ObjectId(transaction.getMemberId()));
                        }
                    }
                }
                System.out.println("4: " + DateTime.now());

                memberCount = memberRepository.countByTextOptIn(token.getCompanyId(), Lists.newArrayList(memberIds));
                System.out.println("5: " + DateTime.now());
                break;
            }
            case MembersVisitedXTimesInYDateRange: {
                Iterable<Transaction> transactions = transactionRepository.getCompletedTransactionsForDateRange(companyId, shopId, startDate, endDate);

                Map<String, Integer> memberVisits = new HashMap<>();

                Set<ObjectId> memberIds = new HashSet<>();
                for (Transaction transaction : transactions) {

                    Integer visits = memberVisits.getOrDefault(transaction.getMemberId(), 0);
                    visits++;
                    memberVisits.put(transaction.getMemberId(), visits);

                    if (visits >= noOfVisits) {
                        memberIds.add(new ObjectId(transaction.getMemberId()));
                    }

                }

                memberCount = memberRepository.countByTextOptIn(token.getCompanyId(), Lists.newArrayList(memberIds));
                break;
            }
            case AutomatedBirthdayText: {
                DateTime now = DateTime.now();
                Iterable<Member> members = memberRepository.listWithOptionsTextOptIn(companyId,
                        Member.MembershipStatus.Active,
                        "{firstName:1,lastName:1,primaryPhone:1,email:1,textOptIn:1,emailOptIn:1,dob:1}");

                int count = 0;
                for (Member member : members) {
                    if (member.getDob() != null) {
                        int days = DateUtil.getDaysBetweenTwoDates(now.getMillis(), member.getDob());
                        if (days == dayBefore) {
                            count++;
                        }
                    }
                }
                memberCount = count;
                break;
            }
            case AutomatedRecommendationExpireText: {
                DateTime now = DateTime.now();
                Iterable<Member> members = memberRepository.listWithOptionsTextOptIn(companyId,
                        Member.MembershipStatus.Active,
                        "{firstName:1,lastName:1,primaryPhone:1,email:1,textOptIn:1,emailOptIn:1,recommendations:1}");

                int count = 0;
                for (Member member : members) {
                    if (!member.getRecommendations().isEmpty() && member.getRecommendations().get(0).getExpirationDate() != null) {
                        if (member.getRecommendations().get(0).getExpirationDate() < 0) {
                            continue;
                        }
                        int days = DateUtil.getDaysBetweenTwoDates(now.getMillis(), member.getRecommendations().get(0).getExpirationDate());
                        if (days == dayBefore) {
                            count++;
                        }
                    }
                }

                memberCount = count;
                break;
            }

            case SpecificMembers: {
                if (memberAsset != null) {
                    List<ObjectId> memberIds = prepareUploadMemberCsv(memberAsset);
                    memberCount = memberRepository.countByTextOptIn(companyId, memberIds);
                }
                break;
            }

            case MembersWithSpecificTags: {
                if (!CollectionUtils.isEmpty(memberTags)) {
                    memberCount = memberRepository.countMembersTextOptInByTags(companyId, Lists.newArrayList(memberTags));
                }
                break;
            }

        }

        return memberCount;
    }

    @Override
    public MarketingJob duplicateMarketingJob(String marketingJobId) {
        MarketingJob oldJob = marketingJobRepository.get(token.getCompanyId(), marketingJobId);
        if (oldJob == null) {
            throw new BlazeInvalidArgException("MarketingJob", "Target marketing job does not exist.");
        }
        Kryo kryo = new Kryo();
        MarketingJob newJob = kryo.copy(oldJob);
        newJob.setId(null);
        newJob.prepare(token.getCompanyId());
        newJob.setReqSendDate(null);
        newJob.setActive(true);
        newJob.setStatus(MarketingJob.MarketingJobStatus.Pending);
        newJob.setInactiveDays(oldJob.getInactiveDays());
        marketingJobRepository.save(newJob);

        // Need to be saved

        return newJob;
    }

    @Override
    public MarketingJob updateMarketingJob(String marketingJobId, MarketingJob marketingJob) {
        MarketingJob dbJob = marketingJobRepository.get(token.getCompanyId(), marketingJobId);
        if (dbJob == null) {
            throw new BlazeInvalidArgException("MarketingJob", "Marketing Job does not exist.");
        }

        if (marketingJob.getRecipientType() == MarketingJob.MarketingRecipientType.SpecificMembers && marketingJob.getMemberAsset() == null) {
            throw new BlazeInvalidArgException("MarketingJob", "Asset canot be blank.");
        }
        dbJob.setMemberGroupIds(marketingJob.getMemberGroupIds());
        dbJob.setJobType(marketingJob.getJobType());
        dbJob.setRecipientType(marketingJob.getRecipientType());
        dbJob.setReqSendDate(marketingJob.getReqSendDate());
        dbJob.setMessage(marketingJob.getMessage());
        dbJob.setInactiveDays(marketingJob.getInactiveDays());
        dbJob.setCategoryId(marketingJob.getCategoryId());
        dbJob.setStartDate(marketingJob.getStartDate());
        dbJob.setEndDate(marketingJob.getEndDate());
        dbJob.setEnable(marketingJob.isEnable());
        dbJob.setDayBefore(marketingJob.getDayBefore());
        dbJob.setAmountDollars(marketingJob.getAmountDollars());
        dbJob.setNoOfVisits(marketingJob.getNoOfVisits());
        dbJob.setVendorId(marketingJob.getVendorId());
        dbJob.setMemberTags(marketingJob.getMemberTags());

        if (dbJob.getReqSendDate() != null && dbJob.getReqSendDate() == 0) {
            dbJob.setReqSendDate(DateTime.now().getMillis());
        }

        if (marketingJob.getRecipientType() == MarketingJob.MarketingRecipientType.SpecificMembers) {
            CompanyAsset memberAsset = marketingJob.getMemberAsset();
            prepareUploadMemberCsv(memberAsset);
            dbJob.setMemberAsset(memberAsset);
        }

        marketingJobRepository.update(token.getCompanyId(), dbJob.getId(), dbJob);
        return dbJob;
    }

    @Override
    public void removeMarketingJob(String marketingJobId) {
        MarketingJob dbJob = marketingJobRepository.get(token.getCompanyId(), marketingJobId);
        if (dbJob == null) {
            throw new BlazeInvalidArgException("MarketingJob", "Marketing Job does not exist.");
        }
        if (dbJob.getStatus() == MarketingJob.MarketingJobStatus.Completed
                || dbJob.getStatus() == MarketingJob.MarketingJobStatus.Failed) {
            throw new BlazeInvalidArgException("MarketingJob", "Deletion of a completed or failed job is not allowed.");
        }
        marketingJobRepository.removeByIdSetState(token.getCompanyId(), marketingJobId);
    }

    /**
     * This method gets number of count of member for marketing job
     *
     * @param request
     */
    @Override
    public MarketingMemberCountResult getMemberCountForMarketingJob(MarketingJobMemberCountRequest request) {
        MarketingMemberCountResult countResult = new MarketingMemberCountResult();
        long count = this.getMemberCountForJob(token.getCompanyId(), request.getRecipientType(), request.getInactiveDays(), request.getMemberGroupIds(), token.getShopId(), request.getStartDate(),
                request.getEndDate(), request.getAmountDollars(), request.getVendorId(), request.getCategoryId(), request.getNoOfVisits(), request.getDayBefore(), request.getMemberAsset(), request.getMemberTags());
        countResult.setCount(count);
        return countResult;
    }

    private List<ObjectId> prepareUploadMemberCsv(CompanyAsset asset) {
        AssetStreamResult assetStreamResult = amazonS3Service.downloadFile(asset.getKey(), asset.isSecured());
        if (assetStreamResult == null || assetStreamResult.getStream() == null) {
            LOG.error("File not found at amazon s3");
            throw new BlazeInvalidArgException("Marketing Job", "File does not exist.");
        }
        CSVParser parser = null;
        Set<ObjectId> memberIds = new HashSet<>();
        try {
            Reader in = new InputStreamReader(assetStreamResult.getStream());
            CSVFormat format = CSVFormat.EXCEL.withHeader().withSkipHeaderRecord(true);
            parser = new CSVParser(in, format);
            for (CSVRecord r : parser) {
                String entity = r.get("Member Id");
                if (StringUtils.isNotBlank(entity) && ObjectId.isValid(entity)) {
                    memberIds.add(new ObjectId(entity));
                } else {
                    throw new BlazeInvalidArgException("Marketing Job", "Member Id is not valid");
                }
            }
        } catch (IOException e) {
            //add log Exception in member csv read
            return Lists.newArrayList(memberIds);
        }

        if (!memberIds.isEmpty()) {
            long count = memberRepository.countItemsIn(token.getCompanyId(), Lists.newArrayList(memberIds));
            if (count < memberIds.size()) {
                LOG.info("Member found in CSV : " + memberIds.size() + " and found in collection : " + count);
            }
        }

        return Lists.newArrayList(memberIds);
    }
}
