package com.fourtwenty.core.services.mgmt.impl;

import com.fourtwenty.core.domain.models.customer.MedicalCondition;
import com.fourtwenty.core.domain.repositories.dispensary.MedicalConditionRepository;
import com.fourtwenty.core.rest.dispensary.results.ListResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.mgmt.MedicalConditionService;
import com.google.inject.Provider;

import javax.inject.Inject;

/**
 * Created by mdo on 11/9/15.
 */
public class MedicalConditionServiceImpl extends AbstractAuthServiceImpl implements MedicalConditionService {
    @Inject
    MedicalConditionRepository medicalConditionRepository;

    @Inject
    public MedicalConditionServiceImpl(Provider<ConnectAuthToken> token) {
        super(token);
    }

    @Override
    public ListResult<MedicalCondition> getAllMedicalConditions() {
        ListResult<MedicalCondition> result = new ListResult<>();
        result.setValues(medicalConditionRepository.list());
        return result;
    }
}
