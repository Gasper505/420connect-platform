package com.fourtwenty.core.services.mgmt.impl;

import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.customer.MemberActivity;
import com.fourtwenty.core.domain.repositories.dispensary.MemberActivityRepository;
import com.fourtwenty.core.domain.repositories.dispensary.MemberRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.services.mgmt.MemberActivityService;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;

public class MemberActivityServiceImpl implements MemberActivityService {

    private static final String MEMBER_ACTIVITY = "Member Activity";
    private static final String MEMBER_NOT_BLANK = "Member does not found";
    private static final String MESSAGE_NOT_BLANK = "Member cannot be blank";

    @Inject
    private MemberRepository memberRepository;
    @Inject
    private MemberActivityRepository memberActivityRepository;


    @Override
    public MemberActivity addMemberActivity(String companyId, String shopId, String employeeId, String message, String memberId) {
        if (StringUtils.isBlank(message)) {
            throw new BlazeInvalidArgException(MEMBER_ACTIVITY, MESSAGE_NOT_BLANK);
        }
        if (StringUtils.isBlank(memberId)) {
            throw new BlazeInvalidArgException(MEMBER_ACTIVITY, MEMBER_NOT_BLANK);
        }
        Member member = memberRepository.get(companyId, memberId);
        if (member == null) {
            throw new BlazeInvalidArgException(MEMBER_ACTIVITY, MEMBER_NOT_BLANK);
        }
        MemberActivity activity = new MemberActivity();
        activity.prepare(companyId);
        activity.setShopId(shopId);
        activity.setEmployeeId(employeeId);
        activity.setLog(message);
        activity.setTargetId(memberId);

        return memberActivityRepository.save(activity);

    }

    @Override
    public SearchResult<MemberActivity> getMemberActivities(String companyId, String shopId, String memberId, int start, int limit) {
        if (limit == 0) {
            limit = 200;
        }
        return memberActivityRepository.getMemberActivities(companyId, shopId, memberId, start, limit);
    }
}
