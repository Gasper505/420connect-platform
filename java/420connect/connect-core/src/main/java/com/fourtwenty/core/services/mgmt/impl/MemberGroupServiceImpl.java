package com.fourtwenty.core.services.mgmt.impl;

import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.domain.models.company.MemberGroup;
import com.fourtwenty.core.domain.repositories.dispensary.MemberGroupRepository;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.mgmt.MemberGroupService;
import com.google.inject.Inject;
import com.google.inject.Provider;

/**
 * Created by mdo on 7/11/16.
 */
public class MemberGroupServiceImpl extends AbstractAuthServiceImpl implements MemberGroupService {
    @Inject
    MemberGroupRepository memberGroupRepository;

    @Inject
    public MemberGroupServiceImpl(Provider<ConnectAuthToken> token) {
        super(token);
    }


    @Override
    public DateSearchResult<MemberGroup> getMemberGroupsByDate(long afterDate, long beforeDate) {
        if (token.getMembersShareOption() == Company.CompanyMembersShareOption.Shared) {
            return memberGroupRepository.findItemsWithDate(token.getCompanyId(), afterDate, beforeDate);
        } else {
            return memberGroupRepository.findItemsWithDate(token.getCompanyId(), token.getShopId(), afterDate, beforeDate);
        }
    }
}
