package com.fourtwenty.core.services.mgmt.impl;

import com.fourtwenty.core.domain.models.company.*;
import com.fourtwenty.core.domain.models.consumer.ConsumerAsset;
import com.fourtwenty.core.domain.models.consumer.ConsumerUser;
import com.fourtwenty.core.domain.models.customer.*;
import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.models.generic.Note;
import com.fourtwenty.core.domain.models.global.StateCannabisLimit;
import com.fourtwenty.core.domain.models.partner.PartnerWebHook;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.models.views.MemberLimitedView;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.domain.repositories.store.ConsumerUserRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.managed.ElasticSearchManager;
import com.fourtwenty.core.managed.PartnerWebHookManager;
import com.fourtwenty.core.rest.dispensary.requests.company.MembershipAddRequest;
import com.fourtwenty.core.rest.dispensary.requests.company.MembershipBulkUpdateRequest;
import com.fourtwenty.core.rest.dispensary.requests.company.MembershipUpdateRequest;
import com.fourtwenty.core.rest.dispensary.requests.employees.DriverLicenseRequest;
import com.fourtwenty.core.rest.dispensary.requests.promotions.MemberLoyaltyAdjustRequest;
import com.fourtwenty.core.rest.dispensary.results.CannabisResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.company.InactiveMemberResult;
import com.fourtwenty.core.rest.dispensary.results.company.MemberResult;
import com.fourtwenty.core.rest.store.webhooks.MemberData;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.common.AuditLogService;
import com.fourtwenty.core.services.common.RealtimeService;
import com.fourtwenty.core.services.global.CannabisLimitService;
import com.fourtwenty.core.services.mgmt.DriverLicenseLogService;
import com.fourtwenty.core.services.mgmt.MemberActivityService;
import com.fourtwenty.core.services.mgmt.MemberService;
import com.fourtwenty.core.services.mgmt.RoleService;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.core.util.EntityFieldUtil;
import com.fourtwenty.core.util.SecurityUtil;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.inject.Provider;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by mdo on 8/26/15.
 */
public class MemberServiceImpl extends AbstractAuthServiceImpl implements MemberService {

    private static final Logger LOG = LoggerFactory.getLogger(MemberService.class);

    private static final String CARE_GIVER = "Care Giver";
    private static final String ERROR_CARE_GIVER = "Error while saving care giver";
    private static final String CARE_GIVER_NOT_FOUND = "Care giver not found";
    private static final String MEMBER = "Member";
    private static final String MEMBER_DRIVING_LICENSE = "User with this driving license already exist.";
    private static final String MEMBER_OWN_DRIVING_LICENSE = "Current user already have this driving license.";

    @Inject
    MemberRepository memberRepository;
    @Inject
    MemberGroupRepository memberGroupRepository;
    @Inject
    CompanyAssetRepository companyAssetRepository;
    @Inject
    DoctorRepository doctorRepository;
    @Inject
    EmployeeRepository employeeRepository;
    @Inject
    RealtimeService realtimeService;
    @Inject
    ContractRepository contractRepository;
    @Inject
    RoleService roleService;
    @Inject
    SecurityUtil securityUtil;
    @Inject
    TransactionRepository transactionRepository;
    @Inject
    private CareGiverRepository careGiverRepository;
    @Inject
    private ElasticSearchManager elasticSearchManager;
    @Inject
    private ShopRepository shopRepository;
    @Inject
    private CannabisLimitService cannabisLimitService;
    @Inject
    private PartnerWebHookManager partnerWebHookManager;
    @Inject
    private MemberActivityService memberActivityService;

    @Inject
    private DriverLicenseLogService driverLicenseLogService;
    @Inject
    AuditLogService auditLogService;

    @Inject
    private ConsumerUserRepository consumerUserRepository;

    @Inject
    public MemberServiceImpl(Provider<ConnectAuthToken> token) {
        super(token);
    }

    @Override
    public MemberResult getMembership(String membershipId, boolean addCannabisLimits, boolean addActivities) {
        if (membershipId == null) {
            throw new BlazeInvalidArgException("MembershipId", "Member Id cannot be null");
        }
        MemberResult member = memberRepository.get(token.getCompanyId(), membershipId, MemberResult.class);

        if (member != null) {
            if (member.getAddress() == null) {

                Address address = new Address();
                address.prepare(token.getCompanyId());
                member.setAddress(address);
            }

            SearchResult<Member> result = new SearchResult<>();
            result.getValues().add(member);
            prepareDataForView(result, false);

            //Care giver
            if (member.getCareGivers() != null && member.getCareGiverList().size() > 0) {
                HashMap<String, CareGiver> careGiverMap = careGiverRepository.listAllAsMap(token.getCompanyId());

                List<CareGiver> careGiverList = new ArrayList<>();
                for (String careGiverId : member.getCareGivers()) {
                    CareGiver careGiver = careGiverMap.get(careGiverId);
                    careGiverList.add(careGiver);
                }
                member.setCareGiverList(careGiverList);
            }

            Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());
            if (shop == null) {
                throw new BlazeInvalidArgException("Shop", "Shop does not found");
            }


            if (shop.isEnableCannabisLimit() && addCannabisLimits) {
                StateCannabisLimit stateCannabisLimit = null;
                if (StringUtils.isNotBlank(shop.getAddress().getState())) {
                    stateCannabisLimit = cannabisLimitService.getCannabisLimitByConsumerTypeAndState(token.getCompanyId(),shop.getAddress().getState(), member.getConsumerType());
                    if (stateCannabisLimit != null) {
                        member.setStateCannabisLimit(stateCannabisLimit);
                    }
                }

                HashMap<Product.CannabisType, BigDecimal> userCurrentCannabisLimit = cannabisLimitService.userCurrentCannabisLimit(token.getCompanyId(), shop, member, null, stateCannabisLimit, null, null, false);
                if (userCurrentCannabisLimit.size() == 0 && userCurrentCannabisLimit.isEmpty()) {
                    HashMap<Product.CannabisType, BigDecimal> cannabisTypeMap = new HashMap<>();
                    cannabisTypeMap.put(Product.CannabisType.CONCENTRATE, new BigDecimal(0));
                    cannabisTypeMap.put(Product.CannabisType.NON_CONCENTRATE, new BigDecimal(0));
                    cannabisTypeMap.put(Product.CannabisType.PLANT, new BigDecimal(0));
                    cannabisTypeMap.put(Product.CannabisType.DRY_LEAF, new BigDecimal(0));
                    cannabisTypeMap.put(Product.CannabisType.OIL, new BigDecimal(0));
                    cannabisTypeMap.put(Product.CannabisType.EXTRACT, new BigDecimal(0));
                    cannabisTypeMap.put(Product.CannabisType.DRY_FLOWER, new BigDecimal(0));
                    cannabisTypeMap.put(Product.CannabisType.EDIBLE, new BigDecimal(0));
                    cannabisTypeMap.put(Product.CannabisType.KIEF, new BigDecimal(0));
                    cannabisTypeMap.put(Product.CannabisType.LIQUID, new BigDecimal(0));
                    cannabisTypeMap.put(Product.CannabisType.SUPPOSITORY, new BigDecimal(0));
                    cannabisTypeMap.put(Product.CannabisType.TINCTURE, new BigDecimal(0));
                    cannabisTypeMap.put(Product.CannabisType.TOPICAL, new BigDecimal(0));
                    prepareMemberDataForCannabis(member, cannabisTypeMap);

                } else {
                    prepareMemberDataForCannabis(member, userCurrentCannabisLimit);
                }
            }

            if (addActivities) {
                SearchResult<MemberActivity> memberLogs = memberActivityService.getMemberActivities(token.getCompanyId(), token.getShopId(), member.getId(), 0, Integer.MAX_VALUE);
                member.setMemberActivities((memberLogs != null) ? memberLogs.getValues() : new ArrayList<>());
            } else {
                member.setMemberActivities(new ArrayList<>());
            }
            long count = transactionRepository.getActiveTransactionCount(token.getShopId(), member.getId());
            member.setInQueue((count > 0));
            member.setActiveTransaction(count);
        }
        return member;
    }

    private MemberResult prepareMemberDataForCannabis(MemberResult member, HashMap<Product.CannabisType, BigDecimal> userCurrentCannabisLimit) {
        List<CannabisResult> cannabisResults = new ArrayList<>();
        HashMap<Product.CannabisType, BigDecimal> userLimit = new HashMap<>();
        for (Product.CannabisType cannabisType : userCurrentCannabisLimit.keySet()) {
            CannabisResult cannabisResult = new CannabisResult();
            BigDecimal currentUsage = userCurrentCannabisLimit.get(cannabisType);
            String cannabis = null;
            Product.CannabisType cannabisDetail = null;
            if (cannabisType.getType() == 0) {
                cannabis = "Concentrate";
                cannabisDetail = Product.CannabisType.CONCENTRATE;
            } else if (cannabisType.getType() == 1) {
                cannabis = "Non-Concentrate";
                cannabisDetail = Product.CannabisType.NON_CONCENTRATE;
            } else if (cannabisType.getType() == 2) {
                cannabis = "Plant";
                cannabisDetail = Product.CannabisType.PLANT;
            } else {
                continue;
            }
            cannabisResult.setBlazeCannabisType(cannabisType);
            cannabisResult.setCannabisType(cannabis);
            cannabisResult.setCurrentUsage(currentUsage);
            cannabisResults.add(cannabisResult);
            if (userLimit.containsKey(cannabisDetail)) {
                BigDecimal updateQuantity = userLimit.get(cannabisDetail).add(currentUsage);
                userLimit.put(cannabisDetail, updateQuantity);
            } else {
                userLimit.put(cannabisDetail, currentUsage);
            }
        }
        member.setCurrentCannabisLimit(userLimit);
        member.setCannabisResult(cannabisResults);
        return member;
    }

    @Override
    public SearchResult<MemberLimitedView> getMembershipsForActiveShop(String term, int start, int limit) {
        if (start < 0) start = 0;
        if (limit <= 0 || limit > 50000) {
            limit = 50000;
        }

        SearchResult<Member> memberSearchResult;

        if (!roleService.canAccessRestricted(token.getCompanyId(), token.getShopId(), token.getActiveTopUser().getUserId())) {
            Iterable<Transaction> transactions = transactionRepository.getAllActiveTransactionsForAssignedEmployee(token.getCompanyId(), token.getShopId(), token.getActiveTopUser().getUserId());

            List<ObjectId> memberIds = new ArrayList<>();
            for (Transaction transaction : transactions) {
                if (StringUtils.isNotBlank(transaction.getMemberId()) && ObjectId.isValid(transaction.getMemberId())) {
                    memberIds.add(new ObjectId(transaction.getMemberId()));
                }
            }
            memberSearchResult = memberRepository.findItemsWithSort(token.getCompanyId(), memberIds, "{firstName:1}", start, limit);

        } else {
            if (token.getMembersShareOption() == Company.CompanyMembersShareOption.Shared) {
                if (StringUtils.isBlank(term)) {
                    memberSearchResult = memberRepository.findItems(token.getCompanyId(), start, limit);
                } else {
                    memberSearchResult = memberRepository.searchMemberships(token.getCompanyId(), term, start, limit);
                }
            } else {
                if (StringUtils.isBlank(term)) {
                    memberSearchResult = memberRepository.findItems(token.getCompanyId(), token.getShopId(), start, limit);
                } else {
                    memberSearchResult = memberRepository.searchMembershipsWithShop(token.getCompanyId(), token.getShopId(), term, start, limit);
                }
            }
        }

        SearchResult<MemberLimitedView> result = prepareDataForView(memberSearchResult, true);
        result.getValues().sort(new Comparator<MemberLimitedView>() {
            @Override
            public int compare(MemberLimitedView o1, MemberLimitedView o2) {
                return o1.getFirstName().compareTo(o2.getFirstName());
            }
        });
        return result;
    }

    @Override
    public SearchResult<MemberLimitedView> getMembersForDoctorId(String doctorId, String term, int start, int limit) {
        if (start < 0) start = 0;
        if (limit <= 0 || limit > 500) {
            limit = 200;
        }

        SearchResult<Member> memberSearchResult;

        if (token.getMembersShareOption() == Company.CompanyMembersShareOption.Shared) {
            if (StringUtils.isBlank(term)) {
                memberSearchResult = memberRepository.getMembersWithDoctorId(token.getCompanyId(), doctorId, start, limit);
            } else {
                memberSearchResult = memberRepository.getMembersWithDoctorIdWithShop(token.getCompanyId(), doctorId, term, start, limit);
            }
        } else {
            if (StringUtils.isBlank(term)) {
                memberSearchResult = memberRepository.getMembersWithDoctorIdWithShop(token.getCompanyId(), token.getShopId(), doctorId, start, limit);
            } else {
                memberSearchResult = memberRepository.getMembersWithDoctorIdWithShop(token.getCompanyId(), token.getShopId(), doctorId, term, start, limit);
            }
        }


        SearchResult<MemberLimitedView> result = prepareDataForView(memberSearchResult, true);

        return result;
    }

    @Override
    public SearchResult<MemberLimitedView> prepareDataForView(SearchResult<Member> memberSearchResult, boolean limitView) {
        HashMap<String, Doctor> doctorHashMap = doctorRepository.listAllAsMap(token.getCompanyId());
        List<Contract> contracts = contractRepository.getActiveContracts(token.getCompanyId(), token.getShopId());
        HashMap<String, MemberGroup> memberGroupHashMap = memberGroupRepository.listAsMap(token.getCompanyId());

        Contract activeContract = null;
        if (contracts.size() > 0) {
            activeContract = contracts.get(0);
        }
        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());
        if (shop == null) {
            throw new BlazeInvalidArgException("Shop", "Shop does not found");
        }
        long currentDate = DateTime.now().getMillis();
        long in30DaysDate = DateTime.now().plusDays(30).getMillis();
        SearchResult<MemberLimitedView> limitedViewSearchResult = new SearchResult<MemberLimitedView>();
        for (Member member : memberSearchResult.getValues()) {
            boolean isRecExpired = true;
            boolean isDLExpired = true;
            boolean isAgreementExpired = false;
            long recExpiryLeft = 0;
            if (member.getAddress() != null) {
                member.getAddress().clean();
            }

            for (Identification identification : member.getIdentifications()) {
                if (identification.getExpirationDate() != null && identification.getExpirationDate() >= currentDate) {
                    isDLExpired = false;
                }
            }

            String licenseNumber = "";
            if (member.getIdentifications().isEmpty()) {
                member.getExpStatuses().add("DL Missing");
            } else if (!member.getIdentifications().isEmpty()) {
                Identification lastId = member.getIdentifications().get(member.getIdentifications().size() - 1);
                if (Objects.isNull(lastId.getExpirationDate())) {
                    member.getExpStatuses().add("DL Expiration date missing.");
                } else if (lastId.getExpirationDate() < in30DaysDate) {
                    member.getExpStatuses().add("DL Expires: " + DateFormatUtils.format(lastId.getExpirationDate(), "MM/dd/yyyy"));
                }
                licenseNumber = lastId.getLicenseNumber();
            }


            for (Recommendation recommendation : member.getRecommendations()) {
                recommendation.setDoctor(doctorHashMap.get(recommendation.getDoctorId()));

                if (recommendation.getExpirationDate() != null && recommendation.getExpirationDate() >= currentDate) {
                    isRecExpired = false;
                    recExpiryLeft = DateUtil.getStandardDaysBetweenTwoDates(currentDate, recommendation.getExpirationDate());
                }
            }

            if (member.isMedical() && member.getRecommendations().isEmpty()) {
                member.getExpStatuses().add("Recommendation: Missing");
            } else if (member.isMedical() && !Objects.isNull(member.getRecommendations()) && !member.getRecommendations().isEmpty()) {
                Recommendation rec = member.getRecommendations().get(member.getRecommendations().size() - 1);

                if (Objects.isNull(rec.getExpirationDate())) {
                    member.getExpStatuses().add("Rec Expiration date missing.");
                } else if (rec.getExpirationDate() < in30DaysDate) {
                    member.getExpStatuses().add("Rec Expires: " + DateFormatUtils.format(rec.getExpirationDate(), "MM/dd/yyyy"));
                }

            }

            // active and required
            String agreeExp = "";
            if (activeContract != null && activeContract.isRequired()) {
                // ignore
                if (member.getContracts().size() > 0) {
                    agreeExp = "New Required Agreement";
                    isAgreementExpired = true;
                    for (SignedContract signedContract : member.getContracts()) {
                        if (signedContract != null && signedContract.getContractId() != null) {
                            if (signedContract.getContractId().equalsIgnoreCase(activeContract.getId())) {
                                // agreement match.
                                isAgreementExpired = false;
                                agreeExp = "";
                                break;
                            }
                        }
                    }
                } else {
                    isAgreementExpired = true;
                    agreeExp = "Agreement Missing";
                }
            }

            if (StringUtils.isNotEmpty(agreeExp)) {
                member.getExpStatuses().add(agreeExp);
            }


            member.setDlExpired(isDLExpired);
            member.setRecommendationExpired(isRecExpired);
            member.setAgreementExpired(isAgreementExpired);
            member.setRecommendationExpiryLeft(recExpiryLeft);

            if (limitView) {
                MemberLimitedView memberLimitedView = new MemberLimitedView();
                memberLimitedView.setId(member.getId());
                memberLimitedView.setCompanyId(member.getCompanyId());
                memberLimitedView.setCreated(member.getCreated());
                memberLimitedView.setModified(member.getModified());
                memberLimitedView.setFirstName(member.getFirstName());
                memberLimitedView.setLastName(member.getLastName());
                memberLimitedView.setPrimaryPhone(member.getPrimaryPhone());
                memberLimitedView.setExpStatuses(member.getExpStatuses());
                memberLimitedView.setEmail(member.getEmail());
                memberLimitedView.setLoyaltyPoints(member.getLoyaltyPoints());
                memberLimitedView.setMedical(member.isMedical());
                //memberLimitedView.setIdentifications(member.getIdentifications());
                //memberLimitedView.setRecommendations(member.getRecommendations());
                memberLimitedView.setStartDate(member.getStartDate());
                memberLimitedView.setLicenseNumber(licenseNumber);
                memberLimitedView.setStatus(member.getStatus());
                memberLimitedView.setBan(member.isBanPatient());
                memberLimitedView.setRecommendationExpired(isRecExpired);
                memberLimitedView.setRecommendationExpiryLeft(recExpiryLeft);

                MemberGroup memberGroup = memberGroupHashMap.get(member.getMemberGroupId());
                if (memberGroup != null) {
                    memberLimitedView.setMemberGroupName(memberGroup.getName());
                }

                limitedViewSearchResult.getValues().add(memberLimitedView);
            }
        }

        limitedViewSearchResult.setTotal((long) limitedViewSearchResult.getValues().size());

        return limitedViewSearchResult;
    }


    @Override
    public Member addMembership(MembershipAddRequest addRequest) {
        // let's add the member

        if (StringUtils.isNotEmpty(addRequest.getEmail())) {
            long count = 0;
            if (token.getMembersShareOption() == Company.CompanyMembersShareOption.Shared) {
                count = memberRepository.countMemberByEmail(token.getCompanyId(), addRequest.getEmail().toLowerCase());
            } else {
                count = memberRepository.countMemberByEmail(token.getCompanyId(), token.getShopId(), addRequest.getEmail().toLowerCase());
            }
            if (count > 0) {
                throw new BlazeInvalidArgException("MembershipAddRequest", "Member with this email already exist for this shop.");
            }

            if (checkConsumerUserExistence(addRequest.getEmail(), "")) {
                throw new BlazeInvalidArgException("New Member", "User with this email already exist.");
            }
        }

        Member member = addOrUpdateMember(null, addRequest, null);

        callWebHook(member, false);

        return member;
    }
    @Override
    public Member addAnonymousMember(String ticketNumber, ConsumerType consumerType) {
        Member newMember = new Member();
        newMember.prepare(token.getCompanyId());
        newMember.setConsumerType(consumerType);
        newMember.setFirstName("Anon-" + ticketNumber);
        newMember.setLastName(UUID.randomUUID().toString());

        memberRepository.save(newMember);
        return newMember;
    }

    @Override
    public Member addOrUpdateMember(Member member, MembershipAddRequest addRequest, SignedContract signedContract) {
        boolean shouldUpdate = false;

        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());
        if (shop == null) {
            throw new BlazeInvalidArgException("Shop", "Shop does not found");
        }

        if (member == null) {
            member = new Member();
            member.setId(addRequest.getId());
            if (StringUtils.isNotBlank(addRequest.getEmail())) {
                member.setEmail(addRequest.getEmail().toLowerCase().trim());
            }
            member.setStartDate(DateTime.now().getMillis());
            // This is an add
            member.setLoyaltyPoints(addRequest.getLoyaltyPoints());
            member.setLifetimePoints(addRequest.getLoyaltyPoints());
        } else {
            shouldUpdate = true;
        }
        if (member.getStartDate() == null || member.getStartDate() == 0) {
            member.setStartDate(DateTime.now().getMillis());
        }

        if (shop.isCheckDuplicateDl()) {
            this.checkMemberDrivingLicense(member, addRequest.getIdentifications());
        }
        HashMap<String, MemberGroup> memberGroupHashMap = new HashMap<>();
        Iterable<MemberGroup> groups = memberGroupRepository.list(token.getCompanyId());
        MemberGroup defaultGroup = null;
        for (MemberGroup memberGroup : groups) {
            memberGroupHashMap.put(memberGroup.getId(), memberGroup);
            if (memberGroup.isDefaultGroup()) {
                defaultGroup = memberGroup;
            }
        }

        if (member.getAddresses() != null) {
            for (Address address : member.getAddresses()) {

                address.prepare(token.getCompanyId());
            }
        }

        MemberGroup memberGroup = memberGroupHashMap.get(addRequest.getMemberGroupId());
        if (memberGroup != null) {
            member.setMemberGroupId(memberGroup.getId());
            member.setMemberGroup(memberGroup);
        } else if (defaultGroup != null) {
            member.setMemberGroupId(defaultGroup.getId());
            member.setMemberGroup(defaultGroup);
        }

        if (member.getMemberGroup() == null) {
            throw new BlazeInvalidArgException("MemberGroup", "Invalid membership level");
        }

        //set fields
        member.setCompanyId(token.getCompanyId());
        member.setShopId(token.getShopId());
        member.setEmail(addRequest.getEmail());
        member.setFirstName(addRequest.getFirstName());
        member.setLastName(addRequest.getLastName());
        member.setMiddleName(addRequest.getMiddleName());
        member.setAddress(addRequest.getAddress());
        member.setAddresses(addRequest.getAddresses());
        member.setDob(addRequest.getDob());
        member.setAnonymous(addRequest.isAnonymous());
        if (StringUtils.isNotBlank(addRequest.getMarketingSource())) {
            LinkedHashSet<String> marketingSources = shop.getMarketingSources();
            if (marketingSources == null) {
                marketingSources = new LinkedHashSet<>();
            }
            if (!marketingSources.contains(addRequest.getMarketingSource())) {
                marketingSources.add(addRequest.getMarketingSource());
                shopRepository.updateMarketingSources(token.getCompanyId(), shop.getId(), marketingSources);
            }
        }
        member.setMarketingSource(addRequest.getMarketingSource());
        member.setEnableLoyalty(addRequest.isEnableLoyalty());
        if (member.getAddress() != null && member.getAddress().getCompanyId() != null) {
            member.getAddress().resetPrepare(token.getCompanyId());

        }

        if (StringUtils.isNotEmpty(member.getEmail())) {
            member.setEmail(member.getEmail().toLowerCase());
        }

        if (shop.getAddress() != null && StringUtils.isNotBlank(shop.getAddress().getCountry())) {
            if (member.getAddress() == null || StringUtils.isBlank(member.getAddress().getCountry())) {
                member.getAddress().setCountry(shop.getAddress().getCountry());
            }
            if (member.getAddresses() != null) {
                for (Address address : member.getAddresses()) {
                    if (address == null || StringUtils.isBlank(address.getCountry())) {
                        address.setCountry(shop.getAddress().getCountry());
                    }
                }
            }
        }


        member.setPrimaryPhone(addRequest.getPrimaryPhone());
        member.setTextOptIn(addRequest.isTextOptIn());
        member.setEmailOptIn(addRequest.isEmailOptIn());
        member.setMedical(addRequest.isMedical());
        member.setSex(addRequest.getSex());
        member.setStartDate(addRequest.getStartDate());
        member.setConsumerType(addRequest.getConsumerType());
        if (member.getStartDate() == 0) {
            member.setStartDate(DateTime.now().getMillis());
        }

        String search = String.format("%s %s %s %s %s %s %s",
                EntityFieldUtil.value(addRequest.getFirstName()),
                EntityFieldUtil.value(addRequest.getMiddleName()),
                EntityFieldUtil.value(addRequest.getLastName()),
                EntityFieldUtil.value((addRequest.getAddress() == null) ? "" : addRequest.getAddress().getAddress()),
                EntityFieldUtil.value(addRequest.getEmail()),
                EntityFieldUtil.value(addRequest.getPrimaryPhone()),
                EntityFieldUtil.value(member.getMemberGroup().getName()));
        member.setSearchText(search.toLowerCase());

        List<String> careGivers = addRequest.getCareGivers();
        HashMap<String, CareGiver> careGiverHashMap = careGiverRepository.listAllAsMap(token.getCompanyId());
        List<String> cleanedGivers = new ArrayList<>();
        if (careGivers != null) {
            for (String careGiver : careGivers) {
                CareGiver careGiverDetails = careGiverHashMap.get(careGiver);
                if (careGiverDetails != null) {
                    //throw new BlazeInvalidArgException("CareGiver", "Care Giver is not found");
                    cleanedGivers.add(careGiverDetails.getId());
                }
            }
        }
        member.setCareGivers(cleanedGivers);
        member.setTags(addRequest.getTags());


        // Identifications and recommendations
        member.setIdentifications(addRequest.getIdentifications());
        member.setRecommendations(addRequest.getRecommendations());

        member.setRecommendationExpired(getRecommendationExpired(member));

        member.setStatus(addRequest.getStatus());

        if (signedContract != null) {
            member.getContracts().add(signedContract);
        }

        member.setStatus(addRequest.getStatus());
        member.setEnabledCareGiver(addRequest.isEnabledCareGiver());

        prepareDataToSave(member);
        //save entity
        StringBuilder logMessage = new StringBuilder();
        if (shouldUpdate) {

            memberRepository.update(token.getCompanyId(), member.getId(), member);
            if (StringUtils.isNotBlank(member.getConsumerUserId())) {
                this.updateCorrespondingConsumerUser(member);
            }
            logMessage.append("Member updated by - ");
        } else {
            memberRepository.save(member);
            logMessage.append("Member created by - ");
        }

        //update member tags in shop
        if (addRequest.getTags() != null && addRequest.getTags().size() > 0) {
            Set<String> shopMembersTag = shop.getMembersTag();
            for (String requestTag : addRequest.getTags()) {
                if (!shopMembersTag.contains(requestTag)) {
                    shopMembersTag.add(requestTag);
                }
            }
            shopRepository.updateMemberTags(token.getCompanyId(), token.getShopId(), shopMembersTag);

        }

        elasticSearchManager.createOrUpdateIndexedDocument(member);

        logMessage.append(token.getActiveTopUser().getFirstName())
                .append((StringUtils.isNotBlank(token.getActiveTopUser().getLastName())) ? token.getActiveTopUser().getLastName() : "");

        memberActivityService.addMemberActivity(token.getCompanyId(), token.getShopId(), token.getActiveTopUser().getUserId(), logMessage.toString(), member.getId());

        realtimeService.sendRealTimeEvent(token.getShopId().toString(), RealtimeService.RealtimeEventType.MembersUpdateEvent, null);
        return member;
    }

    private void updateCorrespondingConsumerUser(Member member) {
        ConsumerUser consumerUser = consumerUserRepository.get(token.getCompanyId(), member.getConsumerUserId());

        if (consumerUser == null) {
            return;
        }

        consumerUser.prepare(token.getCompanyId());
        consumerUser.setEmail(member.getEmail());
        consumerUser.setFirstName(member.getFirstName());
        consumerUser.setLastName(member.getLastName());
        consumerUser.setMiddleName(member.getMiddleName());
        consumerUser.setDob(member.getDob());
        consumerUser.setPrimaryPhone(member.getPrimaryPhone());
        consumerUser.setTextOptIn(member.isTextOptIn());
        consumerUser.setEmailOptIn(member.isEmailOptIn());
        consumerUser.setMedical(member.isMedical());
        consumerUser.setConsumerType(member.getConsumerType());
        consumerUser.setMarketingSource(member.getMarketingSource());
        consumerUser.setSex(member.getSex());

        Address address = member.getAddress();
        if (address != null) {
            address.prepare(token.getCompanyId());
            consumerUser.setAddress(address);
        }

        this.updateConsumerUserInfo(consumerUser, member);

        consumerUserRepository.update(token.getCompanyId(), consumerUser.getId(), consumerUser);
    }

    @Override
    public void updateConsumerUserInfo(ConsumerUser consumerUser, Member member) {
        if (member.getIdentifications() != null && member.getIdentifications().size() > 0) {
            Identification identification = member.getIdentifications().get(0);

            consumerUser.setDlExpiration(identification.getExpirationDate());
            consumerUser.setDlNo(identification.getLicenseNumber());
            consumerUser.setDlState(identification.getState());

            if (identification.getFrontPhoto() != null && StringUtils.isNotBlank(identification.getFrontPhoto().getKey())) {
                CompanyAsset companyAsset = companyAssetRepository.getAssetByKey(token.getCompanyId(), identification.getFrontPhoto().getKey());

                if (companyAsset != null) {
                    ConsumerAsset consumerAsset = companyAsset.toConsumerAsset();
                    consumerUser.setDlPhoto(consumerAsset);
                }
            }
        }

        if (member.getRecommendations() != null && !member.getRecommendations().isEmpty() && member.getRecommendations().get(0) != null
                && member.getRecommendations().get(0).getFrontPhoto() != null) {
            Recommendation recommendation = member.getRecommendations().get(0);

            CompanyAsset companyAsset = companyAssetRepository.getAssetByKey(token.getCompanyId(), recommendation.getFrontPhoto().getKey());

            if (companyAsset != null) {
                ConsumerAsset consumerAsset = companyAsset.toConsumerAsset();
                consumerUser.setRecPhoto(consumerAsset);
            }

            consumerUser.setRecNo(recommendation.getRecommendationNumber());
            consumerUser.setRecExpiration(recommendation.getExpirationDate());
            consumerUser.setRecIssueDate(recommendation.getIssueDate());
            consumerUser.setVerificationPhone(recommendation.getVerifyPhoneNumber());
            consumerUser.setVerificationWebsite(recommendation.getVerifyWebsite());

            Doctor doctor = doctorRepository.get(token.getCompanyId(), recommendation.getDoctorId());
            if (doctor != null) {
                consumerUser.setDoctorFirstName(doctor.getFirstName());
                consumerUser.setDoctorLastName(doctor.getLastName());
                consumerUser.setDoctorLicense(doctor.getLicense());
            }
        }
    }

    private void prepareDataToSave(Member member) {
        // set start Date
        if (member.getStartDate() == null || member.getStartDate() == 0) {
            member.setStartDate(DateTime.now().getMillis());
        }
        if (member.getAddress() != null) {
            member.getAddress().prepare();
        }

        if (member.getIdentifications() != null) {
            for (Identification identification : member.getIdentifications()) {
                identification.prepare(token.getCompanyId());
                if (identification.getFrontPhoto() != null) {
                    CompanyAsset backAsset = companyAssetRepository.getById(identification.getFrontPhoto().getId());
                    identification.setFrontPhoto(backAsset);
                }

                // Refresh assets just in case
                List<CompanyAsset> assets = new ArrayList<>();
                for (CompanyAsset asset : identification.getAssets()) {
                    if (asset == null) {
                        continue;
                    }
                    CompanyAsset backAsset = companyAssetRepository.getById(asset.getId());
                    assets.add(backAsset);

                    if (identification.getFrontPhoto() == null) {
                        identification.setFrontPhoto(backAsset);
                    }
                }
                if (assets.size() == 0 && identification.getFrontPhoto() != null) {
                    assets.add(identification.getFrontPhoto());
                } else if (assets.size() > 0) {
                    identification.setFrontPhoto(assets.get(0));
                }
                identification.setAssets(assets);
            }
        }
        if (member.getRecommendations() != null) {
            for (Recommendation recommendation : member.getRecommendations()) {
                recommendation.prepare(token.getCompanyId());
                if (recommendation.getFrontPhoto() != null && recommendation.getFrontPhoto().getId() != null) {
                    CompanyAsset backAsset = companyAssetRepository.getById(recommendation.getFrontPhoto().getId());
                    recommendation.setFrontPhoto(backAsset);
                }
                // Refresh assets just in case
                List<CompanyAsset> assets = new ArrayList<>();
                for (CompanyAsset asset : recommendation.getAssets()) {
                    if (asset == null) {
                        continue;
                    }
                    CompanyAsset backAsset = companyAssetRepository.getById(asset.getId());
                    assets.add(backAsset);

                    if (recommendation.getFrontPhoto() == null) {
                        recommendation.setFrontPhoto(backAsset);
                    }
                }

                if (assets.size() == 0 && recommendation.getFrontPhoto() != null) {
                    assets.add(recommendation.getFrontPhoto());
                } else if (assets.size() > 0) {
                    recommendation.setFrontPhoto(assets.get(0));
                }
                recommendation.setAssets(assets);

                if (StringUtils.isNotBlank(recommendation.getDoctorId()) && ObjectId.isValid(recommendation.getDoctorId())) {
                    Doctor dbDoctor = doctorRepository.get(token.getCompanyId(), recommendation.getDoctorId());
                    if (dbDoctor != null) {
                        recommendation.setDoctorId(dbDoctor.getId());
                        recommendation.setDoctor(dbDoctor);
                    }
                } else if (recommendation.getDoctor() != null) {
                    Doctor doctor = recommendation.getDoctor();
                    Doctor dbDoctor = null; //doctorRepository.getDoctorByLicense(token.getCompanyId(), doctor.getLicense());

                    if (doctor.getId() != null) {
                        dbDoctor = doctorRepository.get(token.getCompanyId(), doctor.getId());
                    } else if (StringUtils.isNotEmpty(doctor.getLicense())) {
                        dbDoctor = doctorRepository.getDoctorByLicense(token.getCompanyId(), doctor.getLicense());
                    }

                    if (dbDoctor == null) {
                        doctor.setId(ObjectId.get().toString());
                        doctor.setActive(true);
                        doctorRepository.save(doctor);
                    } else {
                        if (!dbDoctor.complete()) {
                            dbDoctor.setFirstName(doctor.getFirstName());
                            dbDoctor.setLastName(doctor.getLastName());
                            dbDoctor.setLicense(doctor.getLicense());
                            dbDoctor.setWebsite(doctor.getWebsite());
                            dbDoctor.setPhoneNumber(doctor.getPhoneNumber());
                            doctor.setActive(true);
                            doctorRepository.update(token.getCompanyId(), dbDoctor.getId(), dbDoctor);
                        }
                        doctor = dbDoctor;
                    }
                    recommendation.setDoctorId(doctor.getId());
                    recommendation.setDoctor(doctor);
                }
            }
        }


        if (member.getNotes() != null) {
            for (Note note : member.getNotes()) {
                note.prepare();
                if (note.getWriterId() == null) {
                    note.setWriterId(token.getActiveTopUser().getUserId());
                    note.setWriterName(token.getActiveTopUser().getFirstName() + " " + token.getActiveTopUser().getLastName());
                }
            }
        }

        if (member.getContracts() != null) {
            for (SignedContract signedContract : member.getContracts()) {
                signedContract.prepare(token.getCompanyId());
                if (signedContract.getMembershipId() == null) {
                    signedContract.setShopId(token.getShopId());
                    signedContract.setMembershipId(member.getId());
                    signedContract.setSignedDate(DateTime.now().getMillis());
                }
            }
        }
    }

    @Override
    public Member updateLoyaltyPoint(String memberId, MemberLoyaltyAdjustRequest request) {

        Member dbMember = memberRepository.get(token.getCompanyId(), memberId);
        if (dbMember == null) {
            throw new BlazeInvalidArgException("Member", "Member doesn't exist with this id");
        }

        Employee employee = employeeRepository.get(token.getCompanyId(), token.getActiveTopUser().getUserId());
        if (employee == null) {
            throw new BlazeInvalidArgException("Employee", "Session is invalid.");
        }


        boolean hasSettingsPerm = roleService.hasPermission(token.getCompanyId(), employee.getRoleId(), Role.Permission.WebSettingsManage);
        if (!hasSettingsPerm) {
            throw new BlazeInvalidArgException("Permission", "You must be an admin to edit member paymentcard points.");
        }
        dbMember.setLoyaltyPoints(request.getLoyaltyPoints());
        memberRepository.update(token.getCompanyId(), dbMember.getId(), dbMember);
        realtimeService.sendRealTimeEvent(token.getShopId().toString(), RealtimeService.RealtimeEventType.MembersUpdateEvent, null);

        StringBuilder logMessage = new StringBuilder();
        logMessage.append("Member paymentcard points updated by - ");
        logMessage.append(token.getActiveTopUser().getFirstName())
                .append((StringUtils.isNotBlank(token.getActiveTopUser().getLastName())) ? token.getActiveTopUser().getLastName() : "");

        memberActivityService.addMemberActivity(token.getCompanyId(), token.getShopId(), token.getActiveTopUser().getUserId(), logMessage.toString(), dbMember.getId());

        return dbMember;
    }

    @Override
    public Member updateMembership(String memberId, MembershipUpdateRequest member) {
        if (memberId == null || !ObjectId.isValid(memberId)) {
            throw new BlazeInvalidArgException("MemberId", "Invalid member id");
        }

        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());
        if (shop == null) {
            throw new BlazeInvalidArgException("Shop", "Shop does not found");
        }



        Member dbMember = memberRepository.get(token.getCompanyId(), memberId);
        if (dbMember == null) {
            throw new BlazeInvalidArgException("Member", "Member doesn't exist with this id");
        }
        if (member.getEmail() != null && !member.getEmail().equalsIgnoreCase(dbMember.getEmail())) {
            long memberCount = memberRepository.countMemberByEmail(token.getCompanyId(), member.getEmail());

            if (memberCount > 0) {
                throw new BlazeInvalidArgException("New Member", "Member with this email already exist.");
            }
            if (checkConsumerUserExistence(member.getEmail(), member.getConsumerUserId())) {
                throw new BlazeInvalidArgException("New Member", "User with this email already exist.");
            }
        }

        if (shop.isCheckDuplicateDl()) {
            this.checkMemberDrivingLicense(dbMember, member.getIdentifications());
        }
        boolean verifyUpdate = false;
        boolean recVerifyUpdate = false;
        if (CollectionUtils.isNotEmpty(member.getRecommendations())) {
            Recommendation rec = member.getRecommendations().get(member.getRecommendations().size() - 1);
            if (rec != null && CollectionUtils.isNotEmpty(dbMember.getRecommendations())) {
                for (Recommendation recommendation : dbMember.getRecommendations()) {
                    if (StringUtils.isNotBlank(rec.getRecommendationNumber()) && rec.getRecommendationNumber().equalsIgnoreCase(recommendation.getRecommendationNumber())) {
                        recVerifyUpdate = rec.isVerified();
                        break;
                    } else if (rec.isVerified()) {
                        recVerifyUpdate = true;
                        break;
                    }
                }
            } else if (rec != null) {
                recVerifyUpdate = rec.isVerified();
            }
        }

        if (CollectionUtils.isNotEmpty(member.getIdentifications())) {
            Identification identification = member.getIdentifications().get(member.getIdentifications().size() - 1);
            if (identification != null && CollectionUtils.isNotEmpty(dbMember.getIdentifications())) {
                for (Identification dbIdentification : dbMember.getIdentifications()) {
                    if (StringUtils.isNotBlank(identification.getLicenseNumber()) && identification.getLicenseNumber().equalsIgnoreCase(dbIdentification.getLicenseNumber())) {
                        verifyUpdate = identification.isVerified();
                        break;
                    } else if (identification.isVerified()) {
                        verifyUpdate = true;
                        break;
                    }
                }
            } else if (identification != null) {
                verifyUpdate = identification.isVerified();
            }
        }


        member.setId(memberId);
        if (member.getNotes() != null) {
            for (Note note : member.getNotes()) {
                if (note.getId() == null) {
                    note.prepare();
                    note.setWriterId(token.getActiveTopUser().getUserId());
                    note.setWriterName(token.getActiveTopUser().getFirstName() + " " + token.getActiveTopUser().getLastName());
                }
            }
        }

        prepareDataToSave(member);


        HashMap<String, MemberGroup> memberGroupHashMap = new HashMap<>();
        Iterable<MemberGroup> groups = memberGroupRepository.list(token.getCompanyId());
        MemberGroup defaultGroup = null;
        for (MemberGroup memberGroup : groups) {
            memberGroupHashMap.put(memberGroup.getId(), memberGroup);
            if (memberGroup.isDefaultGroup()) {
                defaultGroup = memberGroup;
            }
        }


        MemberGroup memberGroup = memberGroupHashMap.get(member.getMemberGroupId());
        if (memberGroup != null) {
            dbMember.setMemberGroupId(memberGroup.getId());
            dbMember.setMemberGroup(memberGroup);
        } else if (defaultGroup != null) {
            dbMember.setMemberGroupId(defaultGroup.getId());
            dbMember.setMemberGroup(defaultGroup);
        }

        if (dbMember.getAddresses() != null) {
            for (Address address : dbMember.getAddresses()) {
                address.prepare(token.getCompanyId());
            }

        }
        boolean isAddressUpdate = (dbMember.getAddress() != null && member.getAddress() != null) && !dbMember.getAddress().getAddressString().equalsIgnoreCase(member.getAddress().getAddressString());

        dbMember.setFirstName(member.getFirstName());
        dbMember.setLastName(member.getLastName());
        dbMember.setMiddleName(member.getMiddleName());
        dbMember.setAddress(member.getAddress());
        dbMember.setNotes(member.getNotes());
        dbMember.setStatus(member.getStatus());
        if (member.getContracts() != null || member.getContracts().size() > 0) {
            dbMember.setContracts(member.getContracts());
        }
        dbMember.setIdentifications(member.getIdentifications());
        dbMember.setPreferences(member.getPreferences());
        dbMember.setRecommendations(member.getRecommendations());
        dbMember.setDob(member.getDob());
        dbMember.setEmail(member.getEmail());
        dbMember.setEmailOptIn(member.isEmailOptIn());
        dbMember.setTextOptIn(member.isTextOptIn());
        dbMember.setMedical(member.isMedical());
        dbMember.setPrimaryPhone(member.getPrimaryPhone());
        dbMember.setSex(member.getSex());
        dbMember.setStartDate(member.getStartDate());
        dbMember.setAnonymous(member.isAnonymous());

        if (StringUtils.isNotBlank(member.getMarketingSource())) {
            LinkedHashSet<String> marketingSources = shop.getMarketingSources();
            if (marketingSources == null) {
                marketingSources = new LinkedHashSet<>();
            }
            if (!marketingSources.contains(member.getMarketingSource())) {
                marketingSources.add(member.getMarketingSource());
                shopRepository.updateMarketingSources(token.getCompanyId(), token.getShopId(), marketingSources);
            }
        }
        dbMember.setMarketingSource(member.getMarketingSource());
        dbMember.setEnableLoyalty(member.isEnableLoyalty());
        dbMember.setEnabledCareGiver(member.isEnabledCareGiver());
        dbMember.setAddresses(member.getAddresses());
        dbMember.setTags(member.getTags());

        List<String> careGivers = member.getCareGivers();
        HashMap<String, CareGiver> careGiverHashMap = careGiverRepository.listAllAsMap(token.getCompanyId());
        if (careGivers != null) {
            for (String careGiver : careGivers) {
                CareGiver careGiverDetails = careGiverHashMap.get(careGiver);
                if (careGiverDetails == null) {
                    throw new BlazeInvalidArgException("CareGiver", "Care Giver does not exit.");
                }
            }
        } else {
            careGivers = new ArrayList<>();
        }
        dbMember.setCareGivers(careGivers);

        dbMember.setConsumerType(member.getConsumerType());

        if (StringUtils.isNotEmpty(member.getEmail())) {
            dbMember.setEmail(member.getEmail().toLowerCase());
        }
        String memberGroupName = "";
        if (dbMember.getMemberGroup() != null) {
            memberGroupName = dbMember.getMemberGroup().getName();
        }

        String search = String.format("%s %s %s %s %s %s %s",
                EntityFieldUtil.value(dbMember.getFirstName()),
                EntityFieldUtil.value(dbMember.getMiddleName()),
                EntityFieldUtil.value(dbMember.getLastName()),
                EntityFieldUtil.value((dbMember.getAddress() == null) ? "" : dbMember.getAddress().getAddress()),
                EntityFieldUtil.value(dbMember.getEmail()),
                EntityFieldUtil.value(dbMember.getPrimaryPhone()),
                EntityFieldUtil.value(memberGroupName));

        dbMember.setSearchText(search.toLowerCase());

        if (member.getLoyaltyPoints() == null) {
            member.setLoyaltyPoints(new BigDecimal(0));
        }

        if (dbMember.getLoyaltyPoints().compareTo(member.getLoyaltyPoints()) != 0) {
            // There's a different in points
            // Make sure only Admin can do this
            Employee employee = employeeRepository.get(token.getCompanyId(), token.getActiveTopUser().getUserId());
            if (employee == null) {
                throw new BlazeInvalidArgException("Employee", "Session is invalid.");
            }


            boolean hasSettingsPerm = roleService.hasPermission(token.getCompanyId(), employee.getRoleId(), Role.Permission.WebSettingsManage);
            if (hasSettingsPerm && member.getLoyaltyPoints() != null && member.getLoyaltyPoints().doubleValue() > 0) {
                //throw new BlazeInvalidArgException("Permission", "You must be an admin to edit member paymentcard points.");
                dbMember.setLoyaltyPoints(member.getLoyaltyPoints());
            }
        }


        if (dbMember.getAddress() == null) {
            Address address = new Address();
            address.prepare(token.getCompanyId());
            dbMember.setAddress(address);
        }

        dbMember.setRecommendationExpired(getRecommendationExpired(member));

        elasticSearchManager.createOrUpdateIndexedDocument(dbMember);
        Member m = memberRepository.update(token.getCompanyId(), memberId, dbMember);

        //update member tags in shop
        if (member.getTags() != null && member.getTags().size() > 0) {
            Set<String> shopMembersTag = member.getTags();
            for (String requestTag : member.getTags()) {
                if (!shopMembersTag.contains(requestTag)) {
                    shopMembersTag.add(requestTag);
                }
            }
            shopRepository.updateMemberTags(token.getCompanyId(), token.getShopId(), shopMembersTag);
        }

        if (StringUtils.isNotBlank(m.getConsumerUserId())) {
            this.updateCorrespondingConsumerUser(m);
        }

        List<String> removeTags = new ArrayList<>();
        List<String> updateTags = new ArrayList<>();
        if (recVerifyUpdate) {
            updateTags.add(Recommendation.VERIFY_TAG);
        } else {
            removeTags.add(Recommendation.VERIFY_TAG);
        }
        if (verifyUpdate) {
            updateTags.add(Identification.VERIFY_TAG);
        }  else {
            removeTags.add(Identification.VERIFY_TAG);
        }
        if (!updateTags.isEmpty() || !removeTags.isEmpty()) {
            transactionRepository.updateActiveOrderTags(token.getCompanyId(), Lists.newArrayList(dbMember.getId()), updateTags, removeTags);
        }
        realtimeService.sendRealTimeEvent(token.getShopId().toString(), RealtimeService.RealtimeEventType.MembersUpdateEvent, null);

        callWebHook(dbMember, true);

        StringBuilder logMessage = new StringBuilder();
        logMessage.append("Member updated by - ");
        logMessage.append(token.getActiveTopUser().getFirstName())
                .append((StringUtils.isNotBlank(token.getActiveTopUser().getLastName())) ? token.getActiveTopUser().getLastName() : "");

        memberActivityService.addMemberActivity(token.getCompanyId(), token.getShopId(), token.getActiveTopUser().getUserId(), logMessage.toString(), member.getId());

        if (isAddressUpdate) {
            transactionRepository.updateAllDeliveryAddress(token.getCompanyId(), dbMember.getId(), dbMember.getAddress());
        }

        return m;
    }

    @Override
    public void removeMembership(String memberId) {

        Member member = memberRepository.get(token.getCompanyId(), memberId);
        if (member == null) {
            throw new BlazeInvalidArgException("Member", "Member not found");
        }

        long count = transactionRepository.getActiveTransactionCount(token.getShopId(), member.getId());

        if (count > 0) {
            throw new BlazeInvalidArgException("Member", "Member has active transaction(s), So please complete/remove transaction(s) first.");
        }

        checkEmployeeAccessbility();

        memberRepository.removeByIdSetState(token.getCompanyId(), memberId);
        elasticSearchManager.deleteIndexedDocument(memberId, Member.class);
        StringBuilder logMessage = new StringBuilder();
        logMessage.append("Member deleted by - ");
        logMessage.append(token.getActiveTopUser().getFirstName())
                .append((StringUtils.isNotBlank(token.getActiveTopUser().getLastName())) ? token.getActiveTopUser().getLastName() : "");

        memberActivityService.addMemberActivity(token.getCompanyId(), token.getShopId(), token.getActiveTopUser().getUserId(), logMessage.toString(), memberId);

    }


    @Override
    public Member sanitize(MembershipAddRequest addRequest, HashMap<String, Doctor> doctorMap) throws BlazeInvalidArgException {
        Member member = new Member();
        //set fields
        member.setCompanyId(token.getCompanyId());
        member.setShopId(token.getShopId());
        member.setStatus(Member.MembershipStatus.Active);
        member.setFirstName(addRequest.getFirstName());
        member.setLastName(addRequest.getLastName());
        member.setMiddleName(addRequest.getMiddleName());
        member.setAddress(addRequest.getAddress());
        member.setDob(addRequest.getDob());
        member.setEmail(addRequest.getEmail());

        if (StringUtils.isNotEmpty(member.getEmail())) {
            member.setEmail(member.getEmail().toLowerCase());
        }

        if (member.getAddress() != null) {
            member.getAddress().prepare();
        }

        member.setPrimaryPhone(addRequest.getPrimaryPhone());
        member.setTextOptIn(addRequest.isTextOptIn());
        member.setEmailOptIn(addRequest.isEmailOptIn());
        member.setMedical(addRequest.isMedical());
        member.setSex(addRequest.getSex());
        member.setLoyaltyPoints(addRequest.getLoyaltyPoints());
        member.setLifetimePoints(addRequest.getLoyaltyPoints());
        member.setEnableLoyalty(addRequest.isEnableLoyalty());
        member.setMarketingSource(addRequest.getMarketingSource());

        if (member.isMedical()) {
            member.setConsumerType(ConsumerType.MedicinalThirdParty);
        } else {
            member.setConsumerType(ConsumerType.AdultUse);
        }

        String memberGroup = member.getMemberGroup() != null ? member.getMemberGroup().getName() : "";
        String search = String.format("%s %s %s %s %s %s %s",
                EntityFieldUtil.value(addRequest.getFirstName()),
                EntityFieldUtil.value(addRequest.getMiddleName()),
                EntityFieldUtil.value(addRequest.getLastName()),
                EntityFieldUtil.value((addRequest.getAddress() == null) ? "" : addRequest.getAddress().getAddress()),
                EntityFieldUtil.value(addRequest.getEmail()),
                EntityFieldUtil.value(addRequest.getPrimaryPhone()),
                EntityFieldUtil.value(memberGroup));
        member.setSearchText(search.toLowerCase());


        // Identifications and recommendations
        member.setIdentifications(addRequest.getIdentifications());
        this.checkMemberDrivingLicense(null, addRequest.getIdentifications());
        if (member.getIdentifications() != null) {
            for (Identification identification : member.getIdentifications()) {
                identification.prepare(token.getCompanyId());
                if (identification.getFrontPhoto() != null && StringUtils.isNotEmpty(identification.getFrontPhoto().getId())) {
                    CompanyAsset backAsset = companyAssetRepository.getById(identification.getFrontPhoto().getId());
                    identification.setFrontPhoto(backAsset);
                }
            }
        }
        member.setRecommendations(addRequest.getRecommendations());

        if (member.getRecommendations() != null) {
            for (Recommendation recommendation : member.getRecommendations()) {
                recommendation.prepare(token.getCompanyId());
                if (recommendation.getFrontPhoto() != null && StringUtils.isNotEmpty(recommendation.getFrontPhoto().getId())) {
                    CompanyAsset backAsset = companyAssetRepository.getById(recommendation.getFrontPhoto().getId());
                    recommendation.setFrontPhoto(backAsset);
                }
            }
        }
        //save entity
        return member;
    }

    @Override
    public HashMap<String, String> getMemberNameMap(String companyId) {
        HashMap<String, String> memberMap = new HashMap<>();
        List<Member> memberList = memberRepository.findItems(companyId, 0, Integer.MAX_VALUE).getValues();
        for (Member m : memberList) {
            memberMap.put(m.getId(), m.getFirstName() + " " + m.getLastName());
        }
        return memberMap;
    }

    @Override
    public InactiveMemberResult getInactiveMembers(int daysInactive) {

        DateTime daysOld = DateTime.now().withTimeAtStartOfDay().minusDays(daysInactive);
        long count = memberRepository.getInactiveMembersWithDaysCount(token.getCompanyId(), daysOld.getMillis());

        InactiveMemberResult result = new InactiveMemberResult();
        result.setDaysInactive(daysInactive);
        result.setInactiveMembers((int) count);
        return result;
    }

    @Override
    public void bulkUpdateMembers(MembershipBulkUpdateRequest membershipBulkUpdateRequest) {
        List<String> memberIds = membershipBulkUpdateRequest.getMemberIds();
        if (membershipBulkUpdateRequest.isUpdateAll()) {
            memberIds = memberRepository.getAllMemberIds(token.getCompanyId());
        } else if (memberIds == null || memberIds.isEmpty()) {
            throw new BlazeInvalidArgException(MEMBER, "Member Id cannot be blank");
        }
        List<ObjectId> membersList = new ArrayList<>();
        for (String memberId : memberIds) {
            membersList.add(new ObjectId(memberId));
        }

        switch (membershipBulkUpdateRequest.getMembershipBulkOperationType()) {
            case SMS_OPT_IN:
                memberRepository.bulkUpdateTextOptIn(token.getCompanyId(), membersList, membershipBulkUpdateRequest.getSmsOptIn());
                break;

            case EMAIL_OPT_IN:
                memberRepository.bulkUpdateEmailOptIn(token.getCompanyId(), membersList, membershipBulkUpdateRequest.getEmailOptIn());
                break;

            case STATUS:
                memberRepository.bulkUpdateMemberStatus(token.getCompanyId(), membersList, membershipBulkUpdateRequest.getMembershipStatus());
                break;

            case MEMBERSHIP_GROUP:
                MemberGroup memberGroup = memberGroupRepository.get(token.getCompanyId(), membershipBulkUpdateRequest.getMemberGroupId());
                if (!Objects.isNull(memberGroup)) {
                    memberRepository.bulkUpdateMemberGroup(token.getCompanyId(), membersList, memberGroup);
                } else {
                    throw new BlazeInvalidArgException("Bulk members update", "Invalid member group or invalid member group for company");
                }
                break;

            case MEMBERSHIP_TYPE:
                boolean medicinal = false;
                if (membershipBulkUpdateRequest.getConsumerType().equals(ConsumerType.AdultUse)) {
                    membershipBulkUpdateRequest.setMedical(false);
                } else {
                    medicinal = true;
                    membershipBulkUpdateRequest.setMedical(true);
                }
                memberRepository.bulkUpdateMemberType(token.getCompanyId(), membersList, medicinal, membershipBulkUpdateRequest.getConsumerType());
                break;

            case VERIFIED:
                memberRepository.bulkUpdateMemberVerified(token.getCompanyId(), membersList, membershipBulkUpdateRequest.getVerified());
                List<String> addTags = new ArrayList<>();
                List<String> removeTags = new ArrayList<>();
                if (membershipBulkUpdateRequest.getVerified()) {
                    addTags.add(Recommendation.VERIFY_TAG);
                } else {
                    removeTags.add(Recommendation.VERIFY_TAG);
                }
                transactionRepository.updateActiveOrderTags(token.getCompanyId(), memberIds, addTags, removeTags);
                break;

            case MARKETING_SOURCE:
                memberRepository.bulkUpdateMemberMarketingSource(token.getCompanyId(), membersList, membershipBulkUpdateRequest.getMarketingSource());
                break;

            case ADD_NOTE:
                Note note = new Note();
                note.prepare();
                note.setWriterId(token.getActiveTopUser().getUserId());
                note.setWriterName(token.getActiveTopUser().getFirstName() + " " + token.getActiveTopUser().getLastName());
                note.setMessage(membershipBulkUpdateRequest.getNote());

                memberRepository.bulkAddNoteToMembers(token.getCompanyId(), membersList, note);
                break;

            case DELETE:
                checkEmployeeAccessbility();
                memberRepository.bulkDeleteMembers(token.getCompanyId(), membersList);
                break;

            case LOYALTY:
                memberRepository.bulkUpdateMemberLoyalty(token.getCompanyId(), membersList, membershipBulkUpdateRequest.getEnableLoyalty());
                break;
            case ID_VERIFIED:
                memberRepository.bulkUpdateMemberIdVerified(token.getCompanyId(), membersList, membershipBulkUpdateRequest.getVerified());
                addTags = new ArrayList<>();
                removeTags = new ArrayList<>();
                if (membershipBulkUpdateRequest.getVerified()) {
                    addTags.add(Identification.VERIFY_TAG);
                } else {
                    removeTags.add(Identification.VERIFY_TAG);
                }
                transactionRepository.updateActiveOrderTags(token.getCompanyId(), memberIds, addTags, removeTags);
                break;
        }


        // ES Update
        if (membershipBulkUpdateRequest.getMembershipBulkOperationType() ==
                MembershipBulkUpdateRequest.MembershipBulkOperationType.DELETE) {


            elasticSearchManager.deleteIndexedDocumentsFor(memberIds, Member.class);
        } else {

            int batches = 2000;
            for (long i = 0; i < batches; i++) {
                List<ObjectId> batchMemberIds = membersList
                        .stream()
                        .skip(i * batches)
                        .limit(batches)
                        .collect(Collectors.toList());


                List<Member> members = Lists.newArrayList(memberRepository.getMembers(token.getCompanyId(), batchMemberIds));
                elasticSearchManager.createOrUpdateIndexedDocuments(members, 50);

            }
            //List<Member> members = Lists.newArrayList(memberRepository.getMembers(token.getCompanyId(), membersList));
            //elasticSearchManager.createOrUpdateIndexedDocuments(members, 50);
        }


        callWebHookForBulkUpdate(membersList);

        auditLogService.addAuditLog(this.token, "/api/v1/mgmt/members/bulkupdate", "Management - Members",
                String.format("Members - Bulk: %s", membershipBulkUpdateRequest.getMembershipBulkOperationType()));
    }

    @Override
    public SearchResult<CareGiver> getCareGivers(int start, int limit) {
        if (start < 0) start = 0;
        if (limit <= 0 || limit > 500) {
            limit = 500;
        }
        if (token.getMembersShareOption() == Company.CompanyMembersShareOption.Shared) {
            return careGiverRepository.findItems(token.getCompanyId(), start, limit);
        } else {
            return careGiverRepository.findItems(token.getCompanyId(), token.getShopId(), start, limit);
        }
    }

    /**
     * Get care giver by id
     *
     * @param careGiverId : care giver id
     */
    @Override
    public CareGiver getCareGiverById(String careGiverId) {
        CareGiver careGiver = careGiverRepository.get(token.getCompanyId(), careGiverId);
        if (careGiver == null) {
            throw new BlazeInvalidArgException(CARE_GIVER, CARE_GIVER_NOT_FOUND);
        }
        return careGiver;
    }

    /**
     * Add Care Giver
     *
     * @param careGiver : care giver object
     * @return careGiver
     */
    @Override
    public CareGiver addCareGiver(CareGiver careGiver) {
        careGiver.prepare(token.getCompanyId());
        careGiver.setShopId(token.getShopId());
        return careGiverRepository.save(careGiver);
    }

    /**
     * Get member of care giver by id
     *
     * @param careGiverId
     */
    @Override
    public List<Member> getMembersOfCareGiver(String careGiverId) {
        return memberRepository.getMembersByCareGiver(token.getCompanyId(), careGiverId);
    }

    /**
     * Update care giver
     *
     * @param careGiverId : care giver id
     * @param careGiver   : care giver's object
     */
    @Override
    public CareGiver updateCareGiver(String careGiverId, CareGiver careGiver) {
        CareGiver dbCareGiver = careGiverRepository.get(token.getCompanyId(), careGiverId);

        if (dbCareGiver == null) {
            throw new BlazeInvalidArgException(CARE_GIVER, CARE_GIVER_NOT_FOUND);
        }

        return careGiverRepository.update(token.getCompanyId(), careGiverId, careGiver);
    }

    @Override
    public void deleteCareGiver(String careGiverId) {
        CareGiver dbCareGiver = careGiverRepository.get(token.getCompanyId(), careGiverId);

        if (dbCareGiver == null) {
            throw new BlazeInvalidArgException(CARE_GIVER, CARE_GIVER_NOT_FOUND);
        }

        careGiverRepository.removeById(token.getCompanyId(), careGiverId);
    }

    /**
     * This method is used for update member ban for patient
     *
     * @param memberId      : memberId
     * @param statusRequest : statusRequest
     * @return member
     */
    @Override
    public Member updateMemberBanStatus(String memberId, StatusRequest statusRequest) {
        Member dbMember = memberRepository.get(token.getCompanyId(), memberId);
        if (dbMember == null) {
            throw new BlazeInvalidArgException("Member", "Member not found");
        }
        dbMember.setBanPatient(statusRequest.isStatus());

        callWebHook(dbMember, true);

        dbMember = memberRepository.update(token.getCompanyId(), dbMember.getId(), dbMember);
        elasticSearchManager.createOrUpdateIndexedDocument(dbMember);

        StringBuilder logMessage = new StringBuilder();
        logMessage.append("Member banned by - ");
        logMessage.append(token.getActiveTopUser().getFirstName())
                .append((StringUtils.isNotBlank(token.getActiveTopUser().getLastName())) ? token.getActiveTopUser().getLastName() : "");

        memberActivityService.addMemberActivity(token.getCompanyId(), token.getShopId(), token.getActiveTopUser().getUserId(), logMessage.toString(), dbMember.getId());

        return dbMember;
    }

    /**
     * This is private method for web hook call
     *
     * @param member   : details of member
     * @param isUpdate : false (if new member is added) and true (if member update)
     */
    private void callWebHook(Member member, boolean isUpdate) {
        if (member == null) {
            return;
        }

        try {
            final MemberData memberData = new MemberData();

            StringBuilder name = new StringBuilder();
            name.append(StringUtils.isNotBlank(member.getFirstName()) ? member.getFirstName() : "").append(" ");
            name.append(StringUtils.isNotBlank(member.getMiddleName()) ? member.getMiddleName() : "").append(" ");
            name.append(StringUtils.isNotBlank(member.getLastName()) ? member.getLastName() : "");
            memberData.setId(member.getId());
            memberData.setCreated(member.getCreated());
            memberData.setModified(member.getModified());
            memberData.setMemberName(name.toString());
            memberData.setFirstName(member.getFirstName());
            memberData.setLastName(member.getLastName());
            memberData.setPhoneNumber(member.getPrimaryPhone());
            memberData.setEmail(member.getEmail());
            memberData.setSex(member.getSex());
            memberData.setAddress(member.getAddress());
            memberData.setDob(member.getDob());
            memberData.setMemberGroupId(member.getMemberGroupId());
            memberData.setStatus(member.getStatus());
            memberData.setConsumerType(member.getConsumerType());
            memberData.setTextOptIn(member.isTextOptIn());
            memberData.setEmailOptIn(member.isEmailOptIn());

            PartnerWebHook.PartnerWebHookType webHookType = (isUpdate) ? PartnerWebHook.PartnerWebHookType.UPDATE_MEMBER : PartnerWebHook.PartnerWebHookType.NEW_MEMBER;
            partnerWebHookManager.memberWebHook(token.getCompanyId(), token.getShopId(), memberData, webHookType);

        } catch (Exception e) {
            LOG.error(((isUpdate) ? "Update" : "Create") + " member web hook failed for shopId :" + token.getShopId() + ",company id:" + token.getCompanyId());
        }

    }

    /**
     * This is private method used in case of bulk update members.
     *
     * @param memberIds
     */
    private void callWebHookForBulkUpdate(List<ObjectId> memberIds) {
        HashMap<String, Member> memberHashMap = memberRepository.findItemsInAsMap(memberIds);
        for (ObjectId memberId : memberIds) {
            Member member = memberHashMap.get(memberId);
            callWebHook(member, true);
        }
    }

    /**
     * Method for parsing driver license detail.
     *
     * @param request
     * @return
     */
    @Override
    public DriverLicense parseDriverLicense(DriverLicenseRequest request) {
        DriverLicense driverLicense = new DriverLicense();

        if (request == null || StringUtils.isBlank(request.getLicenseDetail())) {
            throw new BlazeInvalidArgException("Driver License", "Driver license detail cannot be blank");
        }

        String myData = request.getLicenseDetail();
        String rawData = StringUtils.replaceAll(myData, "IDDAQ", "\nDAQ");
        rawData = StringUtils.replaceAll(rawData, "DLDAQ", "\nDAQ");
        String[] components = rawData.split("\n");

        List<String> elementNames = new ArrayList<>();
        elementNames.add("DCA");     //vehicleClass
        elementNames.add("DCB");     //restriction code
        elementNames.add("DCD");     //jEndorsementCode
        elementNames.add("DBA");     //expirationDate
        elementNames.add("DCS");     //lastName
        elementNames.add("DDE");     //lastNameTruncation
        elementNames.add("DAC");     //firstName
        elementNames.add("DDF");     //firstNameTruncation
        elementNames.add("DAD");     //middleName
        elementNames.add("DDG");     //middleNameTruncation
        elementNames.add("DBD");     //issueDate
        elementNames.add("DBB");     //birthDate
        elementNames.add("DBC");     //gender
        elementNames.add("DAY");     //eyeColor
        elementNames.add("DAU");     //heightInches
        elementNames.add("DAG");     //streetAddress
        elementNames.add("DAI");     //city
        elementNames.add("DAJ");     //state
        elementNames.add("DAK");     //postalCode
        elementNames.add("DAQ");     //driverLicenseNumber
        elementNames.add("DAW");     //weightPounds
        elementNames.add("DCF");     //uniqueDocumentId
        elementNames.add("DCG");     //country
        elementNames.add("DCU");
        elementNames.add("DCK");     //inventoryControlNumber
        elementNames.add("DDA");     //complianceType
        elementNames.add("DDB");     //revisionDate
        elementNames.add("DDD");     //isTemporaryDocument
        elementNames.add("DDJ");
        elementNames.add("DDK");     //isOrganDonor

        if (components != null && components.length == 1) {
            // try splitting using the keys
            //String regex = "DCAC|DCB|DCD|DBA|DCS|DAC|DAD|DBD|DBB|DBC|DAY|DAU|DAG|DAI|DAJ|DAK|DAQ|DCF|DCG|DDE|DDF|DDG|DAZ|DAH|DCI|DCJ|DCK|DBN|DBG|DBS|DCU|DCE|DCL|DCM|DCN|DCO|DCP|DCQ|DCR|DDA|DDB|DDC|DDD|DAW|DAX|DDH|DDI|DDJ|DDK|DDL";

            String tempData = rawData;
            for (String elementName : elementNames) {
                tempData = tempData.replaceAll(elementName, "\n" + elementName);
            }

            components = tempData.split("\n");
        }


        HashMap<String, String> data = new HashMap<>();

        for (String component : components) {
            for (String elementName : elementNames) {
                if (component.startsWith(elementName)) {
                    data.put(elementName, component.replaceAll(elementName, ""));
                }
            }
        }

        driverLicense.prepare(token.getCompanyId());
        driverLicense.setShopId(token.getShopId());

        if (data.isEmpty()) {
            driverLicenseLogService.saveLicenseLog(rawData);
            throw new BlazeInvalidArgException("Driver License", "Invalid license details.");
        } else if (!data.containsKey("DAQ")) {
            driverLicenseLogService.saveLicenseLog(rawData);
            throw new BlazeInvalidArgException("Driver License", "Cannot find license.");
        }

        driverLicense.setExpDate(data.get("DBA"));
        driverLicense.setFamilyName((StringUtils.isBlank(data.get("DCS"))) ? "" : StringUtils.capitalize(data.get("DCS"))); //capital
        driverLicense.setFirstName((StringUtils.isBlank(data.get("DAC"))) ? "" : StringUtils.capitalize(data.get("DAC"))); //capital
        driverLicense.setMiddleName(StringUtils.isBlank(data.get("DAD")) ? "" : StringUtils.capitalize(data.get("DAD"))); //capital
        driverLicense.setIssueDate(data.get("DBD"));
        driverLicense.setDob(data.get("DBB"));
        driverLicense.setSex(data.get("DBC"));
        driverLicense.setEyeColor(data.get("DAY"));
        driverLicense.setHeight(data.get("DAU"));
        driverLicense.setStreet((StringUtils.isBlank(data.get("DAG"))) ? "" : StringUtils.capitalize(data.get("DAG"))); //capital
        driverLicense.setCity((StringUtils.isBlank(data.get("DAI"))) ? "" : StringUtils.capitalize(data.get("DAI"))); //capital
        driverLicense.setState(data.get("DAJ"));
        driverLicense.setCountry(data.get("DCG"));
        driverLicense.setPostalCode(data.get("DAK"));
        driverLicense.setLicense(data.get("DAQ"));
        driverLicense.setWeight(data.get("DAW"));
        driverLicense.setNameSuffix((StringUtils.isBlank(data.get("DCU"))) ? "" : StringUtils.capitalize(data.get("DCU")));

        driverLicense.setMember(memberRepository.getMemberWithDriversLicense(token.getCompanyId(), driverLicense.getLicense()));
        return driverLicense;
    }

    /**
     * Private method to update recommendation expired.
     *
     * @param member
     * @return
     */
    private boolean getRecommendationExpired(Member member) {
        boolean isRecExpired = true;
        long recExpiryLeft = 0;
        if (member.getRecommendations() != null && !member.getRecommendations().isEmpty()) {
            long currentDate = DateTime.now().getMillis();
            for (Recommendation recommendation : member.getRecommendations()) {
                if (recommendation.getExpirationDate() != null && recommendation.getExpirationDate() >= currentDate) {
                    isRecExpired = false;
                    recExpiryLeft = DateUtil.getStandardDaysBetweenTwoDates(currentDate, recommendation.getExpirationDate());
                    break;
                }
            }
        }
        member.setRecommendationExpiryLeft(recExpiryLeft);
        return isRecExpired;
    }

    @Override
    public List<String> getAllMemberTags() {
        return memberRepository.getAllMemberTags(token.getCompanyId());
    }

    /** private method to check if current logged in employee is admin or manager to delete member.
     *
     */
    private void checkEmployeeAccessbility() {
        Role role = null;
        Employee employee = employeeRepository.get(token.getCompanyId(), token.getActiveTopUser().getUserId());
        if (employee != null) {
            role = roleService.getRoleById(employee.getRoleId());
        }

        if (role == null ||
                !(role.getName().equalsIgnoreCase("Manager") ||
                        role.getName().equalsIgnoreCase("Admin"))) {
            throw new BlazeInvalidArgException("Member", "Only Admin or Manager are authorized to delete members.");
        }
    }

    @Override
    public Member getMemberByEmail(String email) {
        Iterable<Member> members = memberRepository.getMemberByEmail(token.getCompanyId(), email);
        for (Member m : members) {
            return m;
        }
        return null;
    }

    @Override
    public SearchResult<Member> getMembersByLicenceNo(String licenceNumber) {
        ArrayList<Member> members = memberRepository.getMembersByLicenceNo(token.getCompanyId(), licenceNumber, "{modified:-1}");
        SearchResult<Member> result = new SearchResult<>();
        if (members != null) {
            result.setValues(members);
            result.setLimit(members.size());
            result.setSkip(0);
            result.setTotal((long) members.size());
        }
        return result;
    }

    /**
     * Private method to check whether email already exists or not.
     * @param email          : email
     * @param consumerUserId : consumerUserId
     * @return return false if not exists else true
     */
    private boolean checkConsumerUserExistence(String email, String consumerUserId) {
        ConsumerUser consumerUser = consumerUserRepository.getConsumerByEmail(email, ConsumerUser.class);
        return !(consumerUser == null || consumerUser.getId().equals(consumerUserId));
    }

    /**
     * This method checks member driving license, if already exist It will show exception.
     *
     * @param  identifications : identifications @param  member : member
     */
    private void checkMemberDrivingLicense(Member member , List<Identification> identifications) {
        List<String> licenseNumber = new ArrayList<>();
        List<String> newLicense = new ArrayList<>();
        HashMap<String, Integer> identificationMap = new HashMap<>();
        if (member.getIdentifications() == null) {
            member.setIdentifications(new ArrayList<Identification>());
        }
        for (Identification identification : member.getIdentifications()) {
            licenseNumber.add(identification.getLicenseNumber().trim());
        }
        if (identifications != null && identifications.size() > 0) {
            for (Identification identification : identifications) {
                if (identificationMap.containsKey(identification.getLicenseNumber().trim())) {
                    identificationMap.replace(identification.getLicenseNumber().trim(), identificationMap.get(identification.getLicenseNumber().trim()) + 1);
                } else {
                    identificationMap.put(identification.getLicenseNumber().trim(), 1);
                }
                if (StringUtils.isBlank(identification.getLicenseNumber()) || licenseNumber.contains(identification.getLicenseNumber().trim())) {
                    continue;
                }
                newLicense.add(identification.getLicenseNumber().trim());
            }
            for (Map.Entry<String, Integer> entry : identificationMap.entrySet()) {
                if (entry.getValue() > 1) {
                    throw new BlazeInvalidArgException(MEMBER, MEMBER_OWN_DRIVING_LICENSE);
                }
            }
            Iterable<Member> members = memberRepository.getMembersByDrivingLicense(token.getCompanyId(), newLicense);
            if (Iterables.size(members) > 0) {
                        throw new BlazeInvalidArgException(MEMBER, MEMBER_DRIVING_LICENSE);
            }
        }
    }
}
