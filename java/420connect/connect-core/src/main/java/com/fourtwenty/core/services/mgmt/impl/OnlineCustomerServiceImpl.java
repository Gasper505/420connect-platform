package com.fourtwenty.core.services.mgmt.impl;

import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.consumer.ConsumerUser;
import com.fourtwenty.core.domain.models.store.ConsumerCart;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.domain.repositories.store.ConsumerCartRepository;
import com.fourtwenty.core.domain.repositories.store.ConsumerUserRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.rest.dispensary.requests.consumeruser.request.OnlineConsumerUserRequest;
import com.fourtwenty.core.rest.dispensary.requests.queues.DeclineRequest;
import com.fourtwenty.core.rest.dispensary.results.OnlineUserResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.mgmt.OnlineCustomerService;
import com.fourtwenty.core.services.mgmt.QueuePOSService;
import com.fourtwenty.core.util.DateUtil;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.google.inject.Provider;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

public class OnlineCustomerServiceImpl extends AbstractAuthServiceImpl implements OnlineCustomerService {

    private static final String CONSUMER_USER = "Consumer User";
    private static final String CONSUMER_USER_NOT_FOUND = "Consumer user is not found";

    @Inject
    private ShopRepository shopRepository;
    @Inject
    private ConsumerUserRepository consumerUserRepository;
    @Inject
    private ConsumerCartRepository consumerCartRepository;
    @Inject
    private QueuePOSService queuePOSService;

    @Inject
    public OnlineCustomerServiceImpl(Provider<ConnectAuthToken> token) {
        super(token);
    }

    /**
     * This method get consumer user by id
     *
     * @param userId : consumer user id
     */
    @Override
    public ConsumerUser getConsumerUserById(String userId) {
        ConsumerUser consumerUser = consumerUserRepository.get(token.getCompanyId(), userId);

        if (consumerUser == null) {
            throw new BlazeInvalidArgException(CONSUMER_USER, CONSUMER_USER_NOT_FOUND);
        }

        return consumerUser;
    }

    @Override
    public SearchResult<OnlineUserResult> findConsumerUser(int start, int limit, OnlineConsumerUserRequest.OnlineUserQueryType queryType, String marketingSource, String term) {

        if (start < 0) {
            start = 0;
        }

        if (limit <= 0 || limit > 200) {
            limit = 200;
        }

        SearchResult<ConsumerUser> consumerUserList = null;

        if (queryType == null || OnlineConsumerUserRequest.OnlineUserQueryType.TODAY == queryType) {

            DateTime endDateTime = DateUtil.nowUTC().plusDays(1).minusSeconds(1);
            DateTime startDateTime = DateUtil.nowUTC().withTimeAtStartOfDay();

            Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());
            String timeZone = token.getRequestTimeZone();
            if (timeZone == null && shop != null) {
                timeZone = shop.getTimeZone();
            }

            int timeZoneOffset = DateUtil.getTimezoneOffsetInMinutes(timeZone);

            long timeZoneStartDateMillis = startDateTime.minusMinutes(timeZoneOffset).getMillis();
            long timeZoneEndDateMillis = endDateTime.minusMinutes(timeZoneOffset).getMillis();

            if (StringUtils.isNotBlank(marketingSource)) {
                consumerUserList = consumerUserRepository.findCreatedConsumerByDateWithMarketingSource(token.getCompanyId(), start, limit, timeZoneStartDateMillis, timeZoneEndDateMillis, marketingSource, term);
            } else {
                consumerUserList = consumerUserRepository.findCreatedConsumerByDate(token.getCompanyId(), start, limit, timeZoneStartDateMillis, timeZoneEndDateMillis, term);
            }

        } else if (OnlineConsumerUserRequest.OnlineUserQueryType.PENDING == queryType) {

            SearchResult<ConsumerCart> consumerCarts = consumerCartRepository.getPlacedConsumerCartsByStatus(token.getCompanyId(),
                    token.getShopId(), Lists.newArrayList(ConsumerCart.ConsumerCartStatus.Placed), 0, Integer.MAX_VALUE, ConsumerCart.class);
            consumerUserList = this.prepareConsumersByQueryType(start, limit, consumerCarts, marketingSource, queryType, term);

        } else if (OnlineConsumerUserRequest.OnlineUserQueryType.ACCEPTED == queryType) {

            if (StringUtils.isNotBlank(marketingSource)) {
                consumerUserList = consumerUserRepository.findConsumersByAcceptanceWithMarketingSource(token.getCompanyId(), start, limit, marketingSource, Boolean.TRUE, "{modified:-1}", term);
            } else {
                consumerUserList = consumerUserRepository.findConsumersByAcceptance(token.getCompanyId(), start, limit, Boolean.TRUE, "{modified:-1}", term);
            }

        } else if (OnlineConsumerUserRequest.OnlineUserQueryType.REJECTED == queryType) {
            SearchResult<ConsumerCart> consumerCarts = consumerCartRepository.getPlacedConsumerCartsByStatus(token.getCompanyId(),
                    token.getShopId(), Lists.newArrayList(ConsumerCart.ConsumerCartStatus.Placed), 0, Integer.MAX_VALUE, ConsumerCart.class);
            List<ObjectId> consumerUserIds = new ArrayList<>();
            for (ConsumerCart cart : consumerCarts.getValues()) {
                if (StringUtils.isNotBlank(cart.getConsumerId()) && ObjectId.isValid(cart.getConsumerId())) {
                    consumerUserIds.add(new ObjectId(cart.getConsumerId()));
                }
            }
            if (StringUtils.isNotBlank(marketingSource)) {
                consumerUserList = consumerUserRepository.findRejectedConsumersWithMarketingSource(token.getCompanyId(), start, limit, marketingSource, Boolean.FALSE, "{modified:-1}", consumerUserIds, term);
            } else {
                consumerUserList = consumerUserRepository.findRejectedConsumers(token.getCompanyId(), start, limit, Boolean.FALSE, "{modified:-1}", consumerUserIds, term);
            }
            List<ConsumerUser> consumerUsers = new ArrayList<>();
            for (ConsumerUser consumerUser : consumerUserList.getValues()) {

                if (consumerUser.getRejectedDate() == null) {

                    consumerUser.setRejectedDate(consumerUser.getModified());
                }
                consumerUsers.add(consumerUser);
            }
            consumerUserList.setValues(consumerUsers);
        }
        return this.prepareConsumerUser(consumerUserList);
    }

    private SearchResult<ConsumerUser> prepareConsumersByQueryType(int start, int limit, SearchResult<ConsumerCart> consumerCarts, String marketingSource, OnlineConsumerUserRequest.OnlineUserQueryType queryType, String term) {
        SearchResult<ConsumerUser> consumerUserList = new SearchResult<>();

        if (consumerCarts.getValues() == null || consumerCarts.getValues().size() == 0) {
            return consumerUserList;
        }

        List<ObjectId> consumerUserIds = new ArrayList<>();
        for (ConsumerCart cart : consumerCarts.getValues()) {
            if (StringUtils.isNotBlank(cart.getConsumerId()) && ObjectId.isValid(cart.getConsumerId())) {
                consumerUserIds.add(new ObjectId(cart.getConsumerId()));
            }
        }

        consumerUserList = StringUtils.isBlank(marketingSource) ?
                            consumerUserRepository.findConsumersByIdAndAcceptance(token.getCompanyId(), start, limit, consumerUserIds, false, "{modified:-1}", term) :
                            consumerUserRepository.findConsumersByIdAndAcceptanceWithMarketingSource(token.getCompanyId(), start, limit, consumerUserIds, marketingSource, false, "{modified:-1}", term);
        return consumerUserList;
    }


    private SearchResult<OnlineUserResult> prepareConsumerUser(SearchResult<ConsumerUser> consumerUserList) {
        SearchResult<OnlineUserResult> result = new SearchResult<>();
        if (consumerUserList == null || consumerUserList.getValues() == null || consumerUserList.getValues().size() == 0) {
            return result;
        }

        List<OnlineUserResult> userResults = new ArrayList<>();
        OnlineUserResult userResult;
        for (ConsumerUser user : consumerUserList.getValues()) {
            userResult = new OnlineUserResult();
            userResult.setId(user.getId());
            userResult.setFirstName(user.getFirstName());
            userResult.setLastName(user.getLastName());
            userResult.setConsumerType(user.getConsumerType());
            userResult.setPhoneNumber(user.getPrimaryPhone());
            userResult.setEmail(user.getEmail());
            userResult.setRegisteredDate(user.getCreated());
            userResult.setMember(user.isAccepted());
            userResult.setMemberId(user.getMemberId());
            userResult.setMarketingSource(user.getMarketingSource());
            userResult.setAcceptedDate(user.getAcceptedDate());
            userResult.setRejectedDate(user.getRejectedDate());
            userResults.add(userResult);
        }

        result.setValues(userResults);
        result.setTotal(consumerUserList.getTotal());
        result.setSkip(consumerUserList.getSkip());
        result.setLimit(consumerUserList.getLimit());
        return result;
    }

    @Override
    public ConsumerUser acceptConsumerUser(String userId, ConsumerUser consumerUser) {
        ConsumerUser dbConsumerUser = consumerUserRepository.get(token.getCompanyId(), userId);
        if (dbConsumerUser == null) {
            throw new BlazeInvalidArgException(CONSUMER_USER, CONSUMER_USER_NOT_FOUND);
        }
        if (dbConsumerUser.isAccepted()) {
            throw new BlazeInvalidArgException(CONSUMER_USER, "Consumer User is already accepted.");
        }

        queuePOSService.acceptOrUpdateConsumer(dbConsumerUser, consumerUser, dbConsumerUser.getMemberId());

        return dbConsumerUser;
    }

    @Override
    public ConsumerUser declineConsumerUser(String userId, DeclineRequest request) {
        ConsumerUser dbConsumerUser = consumerUserRepository.get(token.getCompanyId(), userId);
        if (dbConsumerUser == null) {
            throw new BlazeInvalidArgException(CONSUMER_USER, CONSUMER_USER_NOT_FOUND);
        }

        dbConsumerUser.setAccepted(Boolean.FALSE);
        dbConsumerUser.setRejectedDate(DateTime.now().getMillis());
        dbConsumerUser.setAcceptedDate(null);
        dbConsumerUser.setReason(request.getReason());
        return consumerUserRepository.update(dbConsumerUser.getId(), dbConsumerUser);
    }
}
