package com.fourtwenty.core.services.mgmt.impl;

import com.fourtwenty.core.domain.models.company.Organization;
import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.repositories.dispensary.OrganizationRepository;
import com.fourtwenty.core.domain.repositories.dispensary.EmployeeRepository;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.mgmt.OrganizationService;
import com.google.inject.Provider;

import javax.inject.Inject;
import java.util.List;

public class OrganizationServiceImpl extends AbstractAuthServiceImpl implements OrganizationService {
    @Inject
    OrganizationRepository organizationRepository;
    @Inject
    EmployeeRepository employeeRepository;
   
    @Inject
    public OrganizationServiceImpl(Provider<ConnectAuthToken> token) {
        super(token);
    }

    @Override
    public List<Organization> getOrganizations() {
        return organizationRepository.getOrganizations();
    }

    @Override
    public Organization addOrganization(Organization organization) {
        return organizationRepository.save(organization);
    }

    @Override
    public List<Organization> getCurrentEmployeeOrganizations() {
        Employee employee = employeeRepository.get(token.getCompanyId(), token.getActiveTopUser().getUserId());
        return organizationRepository.getOrganizationsIn(employee.getOrganizationIds());
    }
}


