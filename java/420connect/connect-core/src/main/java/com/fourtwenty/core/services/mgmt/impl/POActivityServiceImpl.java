package com.fourtwenty.core.services.mgmt.impl;

import com.fourtwenty.core.domain.models.purchaseorder.POActivity;
import com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrder;
import com.fourtwenty.core.domain.repositories.dispensary.POActivityRepository;
import com.fourtwenty.core.domain.repositories.dispensary.PurchaseOrderRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.rest.comments.ActivityCommentRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.mgmt.POActivityService;
import com.google.inject.Inject;
import com.google.inject.Provider;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;

/**
 * Created by decipher on 4/10/17 5:38 PM
 * Abhishek Samuel  (Software Engineer)
 * abhishke.decipher@decipherzone.com
 * Decipher Zone Softwares
 * www.decipherzone.com
 */
public class POActivityServiceImpl extends AuthenticationServiceImpl implements POActivityService {

    private static final String ACTIVITY_LOG = "Purchase Order Activity Log";
    private static final String PO_NOT_FOUND = "Purchase Order does not found";

    private POActivityRepository poActivityRepository;
    private PurchaseOrderRepository purchaseOrderRepository;

    @Inject
    public POActivityServiceImpl(Provider<ConnectAuthToken> token, POActivityRepository poActivityRepository, PurchaseOrderRepository purchaseOrderRepository) {
        super(token);
        this.poActivityRepository = poActivityRepository;
        this.purchaseOrderRepository = purchaseOrderRepository;
    }

    /**
     * This method save po activity
     *
     * @param purchaseOrderId : id of purchase order
     * @param employeeId      : employee id
     * @param log             : log of activity
     */
    @Override
    public POActivity addPOActivity(String purchaseOrderId, String employeeId, String log) {
        if (StringUtils.isNotBlank(purchaseOrderId)) {
            POActivity poActivity = new POActivity();
            poActivity.prepare(token.getCompanyId());
            poActivity.setShopId(token.getShopId());
            poActivity.setEmployeeId(employeeId);
            poActivity.setLog(log);
            poActivity.setPurchaseOrderId(purchaseOrderId);
            poActivity.setActivityType(POActivity.ActivityType.NORMAL_LOG);
            return poActivityRepository.save(poActivity);
        }

        return null;
    }

    /**
     * This method gets list of POActivity
     *
     * @param start           : start of search
     * @param limit           : limit of search
     * @param purchaseOrderId : purchase order id from which PO activity needs to be find
     * @return List of PO activity by purchase order id (PO id)
     */
    @Override
    public SearchResult<POActivity> getAllPOActivityList(int start, int limit, String purchaseOrderId) {
        if (StringUtils.isBlank(purchaseOrderId)) {
            throw new BlazeInvalidArgException(ACTIVITY_LOG, "Purchase order cannot be blank");
        }
        if (limit == 0) {
            limit = Integer.MAX_VALUE;
        }
        return poActivityRepository.findItemsByPurchaseOrderId(token.getCompanyId(), token.getShopId(), start, limit, "{created:1}", purchaseOrderId);
    }

    /**
     * Overridden method is use for adding comments for po as activity log.
     *
     * @param request : request to add activity
     * @return POActivity
     */
    @Override
    public POActivity addActivityComment(ActivityCommentRequest request) {

        if (request == null || StringUtils.isBlank(request.getComment())) {
            throw new BlazeInvalidArgException(ACTIVITY_LOG, "Comment cannot be empty");
        }
        if (StringUtils.isBlank(request.getTargetId()) || !ObjectId.isValid(request.getTargetId())) {
            throw new BlazeInvalidArgException(ACTIVITY_LOG, PO_NOT_FOUND);
        }
        PurchaseOrder purchaseOrder = purchaseOrderRepository.getPOById(token.getCompanyId(), request.getTargetId());
        if (purchaseOrder == null) {
            new BlazeInvalidArgException(ACTIVITY_LOG, PO_NOT_FOUND);
        }

        POActivity poActivity = new POActivity();
        poActivity.prepare(token.getCompanyId());
        poActivity.setShopId(token.getShopId());
        poActivity.setEmployeeId(token.getActiveTopUser().getUserId());
        poActivity.setPurchaseOrderId(request.getTargetId());
        poActivity.setLog(request.getComment());
        poActivity.setActivityType(POActivity.ActivityType.COMMENT);

        return poActivityRepository.save(poActivity);


    }
}
