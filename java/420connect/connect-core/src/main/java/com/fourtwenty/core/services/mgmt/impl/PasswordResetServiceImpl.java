package com.fourtwenty.core.services.mgmt.impl;

import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.company.PasswordReset;
import com.fourtwenty.core.domain.repositories.dispensary.EmployeeRepository;
import com.fourtwenty.core.domain.repositories.dispensary.PasswordResetRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.managed.AmazonServiceManager;
import com.fourtwenty.core.rest.dispensary.requests.auth.PasswordResetRequest;
import com.fourtwenty.core.rest.dispensary.requests.auth.PasswordResetUpdateRequest;
import com.fourtwenty.core.services.mgmt.PasswordResetService;
import com.fourtwenty.core.util.SecurityUtil;
import com.google.inject.Inject;
import org.apache.commons.io.IOUtils;
import org.joda.time.DateTime;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.regex.Pattern;

/**
 * Created by mdo on 8/3/16.
 */
public class PasswordResetServiceImpl implements PasswordResetService {
    @Inject
    EmployeeRepository employeeRepository;
    @Inject
    SecurityUtil securityUtil;
    @Inject
    PasswordResetRepository passwordResetRepository;
    @javax.inject.Inject
    AmazonServiceManager amazonServiceManager;
    @Inject
    ConnectConfiguration connectConfiguration;

    @Override
    public PasswordReset requestPasswordReset(PasswordResetRequest resetRequest) {
        Employee employee = employeeRepository.getEmployeeByEmail(resetRequest.getEmail().toLowerCase());
        if (employee == null) {
            throw new BlazeInvalidArgException("Employee", "Unknown email: " + resetRequest.getEmail());
        }
        PasswordReset passwordReset = new PasswordReset();
        passwordReset.setEmployeeId(employee.getId());
        passwordReset.setCompanyId(employee.getCompanyId());
        passwordReset.setResetCode(securityUtil.getNextResetCode());
        passwordReset.setExpirationDate(DateTime.now().plusDays(2).getMillis());
        passwordReset.setExpired(false);

        // set all previous password reset to expired
        passwordResetRepository.setPasswordExpired(employee.getCompanyId(), employee.getId());
        passwordResetRepository.save(passwordReset);

        String body = getPasswordEmailBody(employee, passwordReset);
        amazonServiceManager.sendEmail("support@blaze.me", employee.getEmail(), connectConfiguration.getAppName() + " Email Reset", body, null, null, "Blaze Support");
        return passwordReset;
    }

    @Override
    public void updatePassword(PasswordResetUpdateRequest resetUpdateRequest) {
        PasswordReset passwordReset = passwordResetRepository.getPasswordReset(resetUpdateRequest.getResetCode());
        if (passwordReset == null) {
            throw new BlazeInvalidArgException("PasswordReset", "Invalid password reset.");
        }
        if (DateTime.now().isAfter(passwordReset.getExpirationDate())) {
            throw new BlazeInvalidArgException("PasswordReset", "This password reset code has expired.");
        }

        if (passwordReset.isExpired()) {
            throw new BlazeInvalidArgException("PasswordReset", "This password reset code has expired.");
        }
        Employee employee = employeeRepository.get(passwordReset.getCompanyId(), passwordReset.getEmployeeId());
        employee.setPassword(securityUtil.encryptPassword(resetUpdateRequest.getPassword()));
        employeeRepository.update(employee.getCompanyId(), employee.getId(), employee);

        // set all previous password reset to expired
        passwordResetRepository.setPasswordExpired(employee.getCompanyId(), employee.getId());
    }

    private String getPasswordEmailBody(Employee employee, PasswordReset passwordReset) {
        InputStream inputStream = ShopServiceImpl.class.getResourceAsStream("/passwordreset.html");
        StringWriter writer = new StringWriter();
        try {
            IOUtils.copy(inputStream, writer, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        String theString = writer.toString();

        theString = theString.replaceAll(Pattern.quote("{{name}}"), employee.getFirstName());
        theString = theString.replaceAll(Pattern.quote("{{action_url}}"), connectConfiguration.getAppWebsiteURL() + "/password/recovery?code=" + passwordReset.getResetCode());


        return theString;
    }
}
