package com.fourtwenty.core.services.mgmt.impl;

import com.fourtwenty.core.domain.models.generic.Note;
import com.fourtwenty.core.domain.models.product.ProductBatch;
import com.fourtwenty.core.domain.models.testsample.TestResult;
import com.fourtwenty.core.domain.models.testsample.TestSample;
import com.fourtwenty.core.domain.repositories.dispensary.ProductBatchRepository;
import com.fourtwenty.core.domain.repositories.testsample.TestSampleRepository;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.global.CompanyUniqueSequenceService;
import com.fourtwenty.core.services.mgmt.PoTestSampleService;
import com.fourtwenty.core.services.testsample.request.TestSampleAddRequest;
import com.google.inject.Inject;
import com.google.inject.Provider;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PoTestSampleServiceImpl extends AbstractAuthServiceImpl implements PoTestSampleService {

    private static final String TEST_SAMPLE = "Test Sample";

    @Inject
    private TestSampleRepository testSampleRepository;
    @Inject
    private ProductBatchRepository productBatchRepository;
    @Inject
    private CompanyUniqueSequenceService companyUniqueSequenceService;

    @Inject
    public PoTestSampleServiceImpl(Provider<ConnectAuthToken> token) {
        super(token);
    }

    @Override
    public List<TestSample> addTestSamples(List<TestSampleAddRequest> requests) {
        List<TestSample> testSamples = new ArrayList<>();
        if (requests == null || requests.isEmpty()) {
            return testSamples;
        }

        List<ObjectId> batchIds = new ArrayList<>();

        for (TestSampleAddRequest request : requests) {
            batchIds.add(new ObjectId(request.getBatchId()));
        }

        HashMap<String, ProductBatch> productBatchHashMap = productBatchRepository.listAsMap(token.getCompanyId(), batchIds);

        for (TestSampleAddRequest request : requests) {
            ProductBatch productBatch = productBatchHashMap.get(request.getBatchId());

            if (productBatch == null) {
                continue;
            }

            TestSample testSample = new TestSample();
            testSample.prepare(token.getCompanyId());
            testSample.setShopId(token.getShopId());
            testSample.setBatchId(request.getBatchId());
            testSample.setSample(request.getSample());
            long sampleNo = getNextSequence();
            testSample.setSampleNumber(sampleNo);
            testSample.setDateSent((request.getDateSent() == null) ? DateTime.now().getMillis() : request.getDateSent());
            testSample.setDateTested(request.getDateTested());
            testSample.setStatus((request.getStatus() != null) ? request.getStatus() : TestSample.SampleStatus.IN_TESTING);
            testSample.setTestResult(request.getTestResult());
            updateTestResult(testSample);
            testSample.setTestingCompanyId(request.getTestingCompanyId());
            testSample.setAttachments(request.getAttachments());
            testSample.setTestId(getTestSampleNo(productBatch));
            testSample.setReferenceNo(request.getReferenceNo());

            testSamples.add(testSample);
        }
        return testSampleRepository.save(testSamples);
    }

    /**
     * This private method is used to update test result.
     *
     * @param testSample
     */
    private void updateTestResult(TestSample testSample) {
        if (testSample.getTestResult() != null) {
            TestResult testResult = testSample.getTestResult();
            if (testResult.getNote() != null) {
                for (Note note : testResult.getNote()) {
                    note.prepare();
                    note.setWriterId(token.getActiveTopUser().getUserId());
                    note.setWriterName(token.getActiveTopUser().getFirstName() + " " + token.getActiveTopUser().getLastName());
                }
            }
        }
    }

    /**
     * This private method is used to get a sequence for sample number.
     *
     * @return
     */
    private long getNextSequence() {
        return companyUniqueSequenceService.getNewIdentifier(token.getCompanyId(), TEST_SAMPLE, testSampleRepository.count(token.getCompanyId()));
    }

    private String getTestSampleNo(ProductBatch productBatch) {
        long count = testSampleRepository.countItemsByProductBatch(token.getCompanyId(), token.getShopId(), productBatch.getId());
        return productBatch.getSku() + "-" + (++count) + "-TS";
    }

}
