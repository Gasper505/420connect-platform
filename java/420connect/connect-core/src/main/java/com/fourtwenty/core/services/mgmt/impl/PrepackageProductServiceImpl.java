package com.fourtwenty.core.services.mgmt.impl;

import com.fourtwenty.core.domain.models.company.BarcodeItem;
import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.models.transaction.PrepackageProductItemQueuedTransaction;
import com.fourtwenty.core.domain.models.transaction.ProductBatchQueuedTransaction;
import com.fourtwenty.core.domain.models.transaction.QueuedTransaction;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.rest.dispensary.requests.inventory.PrepackageAddRequest;
import com.fourtwenty.core.rest.dispensary.requests.inventory.PrepackageItemAddRequest;
import com.fourtwenty.core.rest.dispensary.requests.inventory.PrepackageItemReduceRequest;
import com.fourtwenty.core.rest.dispensary.requests.inventory.PrepackageItemUpdateStatusRequest;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.PrepackageGroupResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.PrepackageLineItemInfo;
import com.fourtwenty.core.rest.dispensary.results.inventory.PrepackageProductItemView;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.common.BackgroundJobService;
import com.fourtwenty.core.services.common.RealtimeService;
import com.fourtwenty.core.services.mgmt.BarcodeService;
import com.fourtwenty.core.services.mgmt.PrepackageProductService;
import com.google.common.collect.Lists;
import com.google.inject.Provider;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.*;

/**
 * Created by mdo on 3/6/17.
 */
public class PrepackageProductServiceImpl extends AbstractAuthServiceImpl implements PrepackageProductService {

    @Inject
    PrepackageProductItemRepository prepackageProductRepository;
    @Inject
    ProductBatchRepository batchRepository;
    @Inject
    QueuedTransactionRepository queuedTransactionRepository;
    @Inject
    ProductRepository productRepository;
    @Inject
    InventoryRepository inventoryRepository;
    @Inject
    RealtimeService realtimeService;
    @Inject
    PrepackageRepository prepackageRepository;
    @Inject
    BarcodeService barcodeService;
    @Inject
    ProductWeightToleranceRepository weightToleranceRepository;
    @Inject
    ProductPrepackageQuantityRepository prepackageQuantityRepository;
    @Inject
    private BackgroundJobService backgroundJobService;
    @Inject
    private BatchQuantityRepository batchQuantityRepository;

    @Inject
    public PrepackageProductServiceImpl(Provider<ConnectAuthToken> tokenProvider) {
        super(tokenProvider);
    }

    @Override
    public DateSearchResult<Prepackage> getPrepackagesWithDate(long afterDate, long beforeDate) {
        return prepackageRepository.findItemsWithDate(token.getCompanyId(), token.getShopId(), afterDate, beforeDate);
    }

    @Override
    public DateSearchResult<PrepackageProductItem> getPrepackagesWithDateItems(long afterDate, long beforeDate) {
        return prepackageProductRepository.findItemsWithDate(token.getCompanyId(), token.getShopId(), afterDate, beforeDate);
    }

    @Override
    public DateSearchResult<ProductPrepackageQuantity> getPrepackageQuantitiesWithDate(long afterDate, long beforeDate) {
        return prepackageQuantityRepository.findItemsWithDate(token.getCompanyId(), token.getShopId(), afterDate, beforeDate);
    }

    @Override
    public SearchResult<PrepackageProductItem> getPrepackages(String productId) {
        return prepackageProductRepository.findItems(token.getCompanyId(), token.getShopId(), 0, Integer.MAX_VALUE);
    }

    @Override
    public SearchResult<PrepackageGroupResult> getPrepackagesGroup(String productId, String targetShopId) {
        if (StringUtils.isBlank(productId)) {
            throw new BlazeInvalidArgException("Prepackage", "ProductId is required.");
        }

        if (StringUtils.isBlank(targetShopId)) {
            targetShopId = token.getShopId();
        }

        Product product = productRepository.get(token.getCompanyId(), productId);
        if (product == null) {
            throw new BlazeInvalidArgException("Prepackage", "Product does not exist.");
        }
        SearchResult<PrepackageGroupResult> result = prepackageRepository.getPrepackages(token.getCompanyId(), targetShopId, productId, PrepackageGroupResult.class);

        HashMap<String, ProductWeightTolerance> toleranceHashMap = weightToleranceRepository.listAllAsMap(token.getCompanyId());

        HashMap<String, ProductBatch> productBatchHashMap = batchRepository.getBatchesForProductsMap(token.getCompanyId(),
                targetShopId,
                Lists.newArrayList(productId));
        // Put this in a hash
        HashMap<String, PrepackageGroupResult> map = new HashMap<>();
        for (PrepackageGroupResult p : result.getValues()) {
            map.put(p.getId(), p);

            if (!p.isCustomWeight()) {
                ProductWeightTolerance tolerance = toleranceHashMap.get(p.getToleranceId());
                if (tolerance != null) {
                    p.setName(tolerance.getName());
                    p.setUnitValue(tolerance.getUnitValue());

                    for (ProductPriceRange pr : product.getPriceRanges()) {
                        if (pr.getWeightToleranceId().equalsIgnoreCase(tolerance.getId())) {
                            p.setPrice(pr.getPrice());
                        }
                    }
                }
            }
        }


        Iterable<PrepackageProductItemView> prepackageProductItems = prepackageProductRepository.getPrepackagesForProduct(token.getCompanyId(),
                targetShopId, productId, PrepackageProductItemView.class);

        Map<String, List<ProductPrepackageQuantity>> quantitiesMap = convertQuantitiesListToMap(prepackageQuantityRepository.getQuantitiesForProduct(token.getCompanyId(), targetShopId, productId));

        // group the packages
        for (PrepackageProductItemView prepackageItem : prepackageProductItems) {
            PrepackageGroupResult group = map.get(prepackageItem.getPrepackageId());
            if (group == null) {
                continue;
            }
            if (group.getPrepackages() == null) {
                group.setPrepackages(new ArrayList<PrepackageProductItemView>());
            }
            int total = group.getTotalQuantity();

            List<ProductPrepackageQuantity> quantities = quantitiesMap.get(prepackageItem.getId());
            if (quantities != null) {

                for (ProductPrepackageQuantity quantity : quantities) {
                    total += quantity.getQuantity();

                    int itemTotal = prepackageItem.getQuantity() + quantity.getQuantity();
                    prepackageItem.setQuantity(itemTotal);
                }
            }
            prepackageItem.setQuantities(quantities);
            prepackageItem.setBatch(productBatchHashMap.get(prepackageItem.getBatchId()));
            group.setTotalQuantity(total);
            group.getPrepackages().add(prepackageItem);

        }

        return result;
    }

    @Override
    public PrepackageProductItem addPrepackageLineItem(final String prepackageId, PrepackageItemAddRequest prepackageItemAddRequest) {
        ProductBatch productBatch = batchRepository.get(token.getCompanyId(), prepackageItemAddRequest.getBatchId());
        if (productBatch == null) {
            throw new BlazeInvalidArgException("PrepackageProductItem", "Batch does not exist.");
        }
        Prepackage prepackage = prepackageRepository.get(token.getCompanyId(), prepackageId);
        if (prepackage == null) {
            throw new BlazeInvalidArgException("PrepackageProductItem", "Parent prepackage does not exist.");
        }

        Product product = productRepository.get(token.getCompanyId(), productBatch.getProductId());
        if (product == null) {
            throw new BlazeInvalidArgException("PrepackageProductItem", "Product does not exist.");
        }
        if (!product.isActive()) {
            throw new BlazeInvalidArgException("PrepackageProductItem", "Please enable the product.");
        }

        BigDecimal unitValue = prepackage.getUnitValue();
        if (!prepackage.isCustomWeight()) {
            ProductWeightTolerance weightTolerance = weightToleranceRepository.get(token.getCompanyId(), prepackage.getToleranceId());
            if (weightTolerance != null) {
                unitValue = weightTolerance.getUnitValue();
            }
        }

        // Check to see if there are enough quantity
        BigDecimal totalValue = unitValue.multiply(new BigDecimal(prepackageItemAddRequest.getQuantity()));
        Inventory safeInventory = inventoryRepository.getInventory(token.getCompanyId(), token.getShopId(), Inventory.SAFE);
        if (safeInventory == null) {
            throw new BlazeInvalidArgException("PrepackageProductItem", "SAFE Inventory does not exist.");
        }
        // Get SAFE quantity
        BatchQuantity safeQuantity = batchQuantityRepository.getBatchQuantityForInventory(token.getCompanyId(), token.getShopId(), product.getId(), safeInventory.getId(), productBatch.getId());

        if (safeQuantity == null) {
            throw new BlazeInvalidArgException("PrepackageProductItem", "No quantity in Safe inventory.");
        }

        if (totalValue.doubleValue() > safeQuantity.getQuantity().doubleValue()) {
            throw new BlazeInvalidArgException("PrepackageProductItem", "Not enough quantity in Safe inventory to create packages. Please specify a lower amount.");
        }

        // Create Prepackages
        // Check if there's an existing prepackge, if so, add the new amount
        // If not, create a new ones
        PrepackageProductItem prepackageProductItem = prepackageProductRepository.getPrepackageProduct(token.getCompanyId(),
                token.getShopId(),
                product.getId(),
                prepackage.getId(),
                productBatch.getId());

        boolean isNew = false;
        if (prepackageProductItem == null) {
            // create a new one
            isNew = true;
            prepackageProductItem = new PrepackageProductItem();
            prepackageProductItem.prepare(token.getCompanyId());
            prepackageProductItem.setShopId(token.getShopId());
            prepackageProductItem.setProductId(product.getId());
            prepackageProductItem.setBatchSKU(productBatch.getSku());
            prepackageProductItem.setBatchId(productBatch.getId());
            prepackageProductItem.setPrepackageId(prepackage.getId());
            prepackageProductItem.setActive(prepackageItemAddRequest.isActive());

        }

        if (StringUtils.isBlank(prepackageProductItem.getSku())) {
            // Update sku if needed
            BarcodeItem item = barcodeService.createBarcodeItemIfNeeded(product.getCompanyId(),product.getShopId(), product.getId(),
                    BarcodeItem.BarcodeEntityType.Prepackage,
                    prepackageProductItem.getId(),
                    prepackageProductItem.getSku(), null, false);
            if (item != null) {
                prepackageProductItem.setSku(item.getBarcode());
            }
        }


        if (isNew) {
            // Save as new
            prepackageProductRepository.save(prepackageProductItem);
        } else {
            // update
            prepackageProductRepository.update(token.getCompanyId(), prepackageProductItem.getId(), prepackageProductItem);
        }

        this.createTransactionJobForPrepackageLineItem(prepackageProductItem, safeInventory.getId(),
                new BigDecimal(prepackageItemAddRequest.getQuantity()), totalValue, ProductBatchQueuedTransaction.OperationType.Add, null);

        return prepackageProductItem;
    }

    private void createTransactionJobForPrepackageLineItem(PrepackageProductItem prepackageProductItem, String inventoryId, BigDecimal prepackageQuantity,
                                                           BigDecimal productQuantity, ProductBatchQueuedTransaction.OperationType operationType, String prepackageId) {

        PrepackageProductItemQueuedTransaction queuedTransaction = new PrepackageProductItemQueuedTransaction();
        queuedTransaction.setShopId(token.getShopId());
        queuedTransaction.setCompanyId(token.getCompanyId());
        queuedTransaction.setId(ObjectId.get().toString());
        queuedTransaction.setCart(null);
        queuedTransaction.setPendingStatus(Transaction.TransactionStatus.InProgress);
        queuedTransaction.setStatus(QueuedTransaction.QueueStatus.Pending);
        queuedTransaction.setQueuedTransType(QueuedTransaction.QueuedTransactionType.Prepackage);
        queuedTransaction.setSellerId(token.getActiveTopUser().getUserId());
        queuedTransaction.setMemberId(null);
        queuedTransaction.setTerminalId(null);
        if (prepackageProductItem != null) {
            queuedTransaction.setTransactionId(prepackageProductItem.getId());
        }
        queuedTransaction.setRequestTime(DateTime.now().getMillis());
        queuedTransaction.setInventoryId(inventoryId);
        queuedTransaction.setPrepackageItemQuantity(prepackageQuantity);
        queuedTransaction.setProductQuantity(productQuantity);
        queuedTransaction.setOperationType(operationType);
        queuedTransaction.setPrepackageId(prepackageId);

        queuedTransaction.setReassignTransfer(Boolean.FALSE);
        queuedTransaction.setNewTransferInventoryId(null);

        queuedTransactionRepository.save(queuedTransaction);

        backgroundJobService.addTransactionJob(token.getCompanyId(), token.getShopId(), queuedTransaction.getId());
    }

    @Override
    public PrepackageProductItem reducePrepackageLineItem(String prepackageItemId, PrepackageItemReduceRequest reduceRequest) {
        // Check if there are current pending queued items
        /*long count = queuedTransactionRepository.getCountStatus(token.getCompanyId(),token.getShopId(), QueuedTransaction.QueueStatus.Pending);
        if (count > 0) {
            throw new BlazeInvalidArgException("PrepackageProductItem", "There are active pending transactions. Please wait until all transactions are completed.");
        }*/

        PrepackageProductItem prepackageProductItem = prepackageProductRepository.get(token.getCompanyId(), prepackageItemId);

        if (prepackageProductItem == null) {
            throw new BlazeInvalidArgException("PrepackageProductItem", "PrepackageProductItem does not exist for these parameters.");
        }


        ProductBatch productBatch = batchRepository.get(token.getCompanyId(), prepackageProductItem.getBatchId());
        if (productBatch == null) {
            throw new BlazeInvalidArgException("PrepackageProductItem", "Batch does not exist.");
        }

        Product product = productRepository.get(token.getCompanyId(), productBatch.getProductId());
        if (product == null) {
            throw new BlazeInvalidArgException("PrepackageProductItem", "Product does not exist.");
        }
        if (!product.isActive()) {
            throw new BlazeInvalidArgException("PrepackageProductItem", "Please enable the product.");
        }

        // Check to see if there are enough quantity
        Inventory targetInventory = null;

        if (StringUtils.isNotEmpty(reduceRequest.getInventoryId()) && ObjectId.isValid(reduceRequest.getInventoryId())) {
            targetInventory = inventoryRepository.get(token.getCompanyId(), reduceRequest.getInventoryId());
        } else {
            targetInventory = inventoryRepository.getInventory(token.getCompanyId(), token.getShopId(), Inventory.SAFE);
        }
        if (targetInventory == null) {
            throw new BlazeInvalidArgException("PrepackageProductItem", "Target Inventory does not exist.");
        }

        ProductPrepackageQuantity prepackageQuantity = prepackageQuantityRepository.getQuantity(token.getCompanyId(), token.getShopId(),
                targetInventory.getId(), prepackageProductItem.getId());
        if (prepackageQuantity == null || reduceRequest.getReduceBy() > prepackageQuantity.getQuantity()) {
            throw new BlazeInvalidArgException("PrepackageProductItem", "Reduce by request should be lesser or equal to the total available packages.");
        }

        Prepackage dbPrepackage = prepackageRepository.get(token.getCompanyId(), prepackageProductItem.getPrepackageId());
        if (dbPrepackage == null) {
            throw new BlazeInvalidArgException("PrepackageProductItem", "Parent prepackage does not exist.");
        }


        BigDecimal unitValue = dbPrepackage.getUnitValue();
        if (!dbPrepackage.isCustomWeight()) {
            ProductWeightTolerance weightTolerance = weightToleranceRepository.get(token.getCompanyId(), dbPrepackage.getToleranceId());
            if (weightTolerance != null) {
                unitValue = weightTolerance.getUnitValue();
            }
        }

        double productQuantity = reduceRequest.getReduceBy() * unitValue.doubleValue();

        this.createTransactionJobForPrepackageLineItem(prepackageProductItem, targetInventory.getId(), new BigDecimal(reduceRequest.getReduceBy()), new BigDecimal(productQuantity), ProductBatchQueuedTransaction.OperationType.ReducePrepackage, null);

        return prepackageProductItem;
    }

    @Override
    public PrepackageProductItem deletePrepackageLineItem(String prepackageLineItemId) {
        // Check if there are current pending queued items
        /*long count = queuedTransactionRepository.getCountStatus(token.getCompanyId(),token.getShopId(), QueuedTransaction.QueueStatus.Pending);
        if (count > 0) {
            throw new BlazeInvalidArgException("PrepackageProductItem", "There are active pending transactions. Please wait until all transactions are completed.");
        }*/

        PrepackageProductItem prepackageProductItem = prepackageProductRepository.get(token.getCompanyId(), prepackageLineItemId);

        if (prepackageProductItem == null) {
            throw new BlazeInvalidArgException("PrepackageProductItem", "PrepackageProductItem does not exist for these parameters.");
        }

        if (prepackageProductItem.isDeleted()) {
            throw new BlazeInvalidArgException("PrepackageProductItem", "PrepackageProductItem is already deleted.");
        }

        ProductBatch productBatch = batchRepository.get(token.getCompanyId(), prepackageProductItem.getBatchId());
        if (productBatch == null) {
            throw new BlazeInvalidArgException("PrepackageProductItem", "Batch does not exist.");
        }

        Product product = productRepository.get(token.getCompanyId(), productBatch.getProductId());
        if (product == null) {
            throw new BlazeInvalidArgException("PrepackageProductItem", "Product does not exist.");
        }

        Prepackage dbPrepackage = prepackageRepository.get(token.getCompanyId(), prepackageProductItem.getPrepackageId());
        if (dbPrepackage == null) {
            throw new BlazeInvalidArgException("PrepackageProductItem", "Parent prepackage does not exist.");
        }


        createTransactionJobForPrepackageLineItem(prepackageProductItem, null, null, null, ProductBatchQueuedTransaction.OperationType.DeletePrepackageItem, null);

        prepackageProductItem.setDeleted(Boolean.TRUE);

        return prepackageProductItem;
    }

    @Override
    public PrepackageProductItem updatePrepackageItemStatus(String prepackageLineItemId, PrepackageItemUpdateStatusRequest request) {
        PrepackageProductItem dbPrepackageItem = prepackageProductRepository.get(token.getCompanyId(), prepackageLineItemId);

        if (dbPrepackageItem == null) {
            throw new BlazeInvalidArgException("PrepackageProductItem", "PrepackageProductItem does not exist for these parameters.");
        }

        dbPrepackageItem.setActive(request.isActive());

        prepackageProductRepository.update(token.getCompanyId(), dbPrepackageItem.getId(), dbPrepackageItem);

        // Notify that inventory has been updated
        realtimeService.sendRealTimeEvent(token.getShopId().toString(),
                RealtimeService.RealtimeEventType.InventoryUpdateEvent, null);

        return dbPrepackageItem;
    }

    @Override
    public Prepackage addPrepackage(PrepackageAddRequest addRequest) {
        ProductWeightTolerance weightTolerance = weightToleranceRepository.get(token.getCompanyId(), addRequest.getToleranceId());
        String packageName = addRequest.getName();
        if (!addRequest.isCustomWeight() && weightTolerance != null) {
            packageName = weightTolerance.getName();
        }


        Prepackage oldPackage = prepackageRepository.getPrepackage(token.getCompanyId(), token.getShopId(), addRequest.getProductId(), packageName);
        if (oldPackage != null) {
            throw new BlazeInvalidArgException("Prepackage", "Another prepackage exist with this name.");
        }

        Product product = productRepository.get(token.getCompanyId(), addRequest.getProductId());
        if (product == null) {
            throw new BlazeInvalidArgException("Prepackage", "Please specify a valid product.");
        }

        Prepackage prepackage = new Prepackage();
        prepackage.prepare(token.getCompanyId());
        prepackage.setShopId(token.getShopId());
        prepackage.setCustomWeight(addRequest.isCustomWeight());
        if (!addRequest.isCustomWeight()) {
            prepackage.setToleranceId(addRequest.getToleranceId());
        }
        prepackage.setProductId(addRequest.getProductId());
        prepackage.setName(packageName);
        prepackage.setUnitValue(addRequest.getUnitValue());
        prepackage.setPrice(addRequest.getPrice());
        prepackage.setActive(addRequest.isActive());
        prepackage.setSku(addRequest.getSku());

        BarcodeItem item = barcodeService.createBarcodeItemIfNeeded(prepackage.getCompanyId(),prepackage.getShopId(), product.getId(),
                BarcodeItem.BarcodeEntityType.Prepackage,
                prepackage.getId(), addRequest.getSku(), null, false);
        if (item != null) {
            prepackage.setSku(item.getBarcode());
        }

        prepackageRepository.save(prepackage);
        return prepackage;
    }

    @Override
    public Prepackage updatePrepackage(String prepackageId, Prepackage prepackage) {
        Prepackage dbPrepackage = prepackageRepository.get(token.getCompanyId(), prepackageId);
        if (dbPrepackage == null) {
            throw new BlazeInvalidArgException("Prepackage", "The prepackage group does not exist.");
        }

        Product product = productRepository.get(token.getCompanyId(), prepackage.getProductId());
        if (product == null) {
            throw new BlazeInvalidArgException("Prepackage", "Please specify a valid product.");
        }

        String packageName = prepackage.getName();

        if (!prepackage.isCustomWeight()) {
            ProductWeightTolerance weightTolerance = weightToleranceRepository.get(token.getCompanyId(), prepackage.getToleranceId());
            if (weightTolerance != null) {
                packageName = weightTolerance.getName();
            }
        }

        // Check if name was updated
        if (StringUtils.isNotBlank(packageName)) {
            Prepackage otherPackage = prepackageRepository.getPrepackage(token.getCompanyId(),
                    token.getShopId(),
                    dbPrepackage.getProductId(), packageName);

            if (otherPackage != null && !otherPackage.getId().equalsIgnoreCase(dbPrepackage.getId())) {
                throw new BlazeInvalidArgException("Prepackage", "Another prepackage exist with this name.");
            }
        }

        dbPrepackage.setCustomWeight(prepackage.isCustomWeight());
        if (!prepackage.isCustomWeight()) {
            dbPrepackage.setToleranceId(prepackage.getToleranceId());
        }
        dbPrepackage.setActive(prepackage.isActive());
        dbPrepackage.setUnitValue(prepackage.getUnitValue());
        dbPrepackage.setName(packageName);
        dbPrepackage.setPrice(prepackage.getPrice());

        BarcodeItem item = barcodeService.createBarcodeItemIfNeeded(prepackage.getCompanyId(),prepackage.getShopId(), product.getId(),
                BarcodeItem.BarcodeEntityType.Prepackage,
                prepackage.getId(), prepackage.getSku(), dbPrepackage.getSku(), false);
        if (item != null) {
            dbPrepackage.setSku(item.getBarcode());
        }

        prepackageRepository.update(token.getCompanyId(), dbPrepackage.getId(), dbPrepackage);
        return dbPrepackage;
    }

    @Override
    public void deletePrepackage(String prepackageId) {
        Prepackage dbPrepackage = prepackageRepository.get(token.getCompanyId(), prepackageId);
        if (dbPrepackage == null) {
            throw new BlazeInvalidArgException("Prepackage", "The prepackage group does not exist.");
        }

        // Check to see if there are enough quantity
        Inventory safeInventory = inventoryRepository.getInventory(token.getCompanyId(), token.getShopId(), Inventory.SAFE);
        if (safeInventory == null) {
            throw new BlazeInvalidArgException("PrepackageProductItem", "SAFE Inventory does not exist.");
        }
        Product product = productRepository.get(token.getCompanyId(), dbPrepackage.getProductId());
        if (product == null) {
            throw new BlazeInvalidArgException("PrepackageProductItem", "Product does not exist.");
        }

        createTransactionJobForPrepackageLineItem(null, null, null, null, ProductBatchQueuedTransaction.OperationType.DeletePrepackage, prepackageId);

        prepackageRepository.removeById(token.getCompanyId(), dbPrepackage.getId(), dbPrepackage.getName());

    }

    @Override
    public Map<String, SearchResult<PrepackageGroupResult>> getPrepackagesByCategoryId(String categoryId) {
        List<String> categoryIds = new ArrayList<>();
        categoryIds.add(categoryId);
        Iterable<Product> productsForCategories = productRepository.getProductsForCategories(token.getCompanyId(), categoryIds);

        List<String> productIds = new ArrayList<>();
        Map<String, Product> productMap = new HashMap<>();
        for (Product product : productsForCategories) {
            productIds.add(product.getId());
            productMap.put(product.getId(), product);
        }

        Iterable<PrepackageGroupResult> prepackages = prepackageRepository.getPrepackagesForProducts(token.getCompanyId(), token.getShopId(), productIds, PrepackageGroupResult.class);

        HashMap<String, ProductWeightTolerance> toleranceHashMap = weightToleranceRepository.listAllAsMap(token.getCompanyId());

        HashMap<String, PrepackageGroupResult> prepackageGroupResultMap = new HashMap<>();

        for (PrepackageGroupResult prepackageGroupResult : prepackages) {
            prepackageGroupResultMap.put(prepackageGroupResult.getId(), prepackageGroupResult);

            if (!prepackageGroupResult.isCustomWeight()) {
                ProductWeightTolerance tolerance = toleranceHashMap.get(prepackageGroupResult.getToleranceId());

                if (tolerance != null) {
                    prepackageGroupResult.setName(tolerance.getName());
                    prepackageGroupResult.setUnitValue(tolerance.getUnitValue());

                    Product product = productMap.get(prepackageGroupResult.getProductId());
                    for (ProductPriceRange pr : product.getPriceRanges()) {
                        if (pr.getWeightToleranceId().equalsIgnoreCase(tolerance.getId())) {
                            prepackageGroupResult.setPrice(pr.getPrice());
                        }
                    }
                }
            }
        }

        Iterable<PrepackageProductItemView> prepackageProductItems = prepackageProductRepository.getPrepackagesForProductIds(token.getCompanyId(), token.getShopId(), productIds, PrepackageProductItemView.class);
        Map<String, List<ProductPrepackageQuantity>> prepackageQuantitiesMap = convertQuantitiesListToMap(prepackageQuantityRepository.getQuantitiesForProducts(token.getCompanyId(), token.getShopId(), productIds));
        HashMap<String, ProductBatch> productBatchHashMap = batchRepository.getBatchesForProductsMap(token.getCompanyId(), token.getShopId(), productIds);

        Map<String, SearchResult<PrepackageGroupResult>> resultMap = new HashMap<>();


        // group the packages
        for (PrepackageProductItemView prepackageItem : prepackageProductItems) {
            PrepackageGroupResult group = prepackageGroupResultMap.get(prepackageItem.getPrepackageId());
            if (group == null) {
                continue;
            }
            if (group.getPrepackages() == null) {
                group.setPrepackages(new ArrayList<>());
            }
            int total = group.getTotalQuantity();

            List<ProductPrepackageQuantity> quantities = prepackageQuantitiesMap.get(prepackageItem.getId());
            if (quantities != null) {

                for (ProductPrepackageQuantity quantity : quantities) {
                    total += quantity.getQuantity();

                    int itemTotal = prepackageItem.getQuantity() + quantity.getQuantity();
                    prepackageItem.setQuantity(itemTotal);
                }
            }
            prepackageItem.setQuantities(quantities);
            prepackageItem.setBatch(productBatchHashMap.get(prepackageItem.getBatchId()));
            group.setTotalQuantity(total);
            group.getPrepackages().add(prepackageItem);


            SearchResult<PrepackageGroupResult> searchResult = new SearchResult<>();
            searchResult.setValues(Lists.newArrayList(group));
            searchResult.setSkip(0);
            searchResult.setLimit(searchResult.getValues().size());
            searchResult.setTotal((long) searchResult.getValues().size());


            resultMap.put(prepackageItem.getProductId(), searchResult);
        }

        return resultMap
                ;
    }

    private Map<String, List<ProductPrepackageQuantity>> convertQuantitiesListToMap(Iterable<ProductPrepackageQuantity> quantities) {
        Map<String, List<ProductPrepackageQuantity>> quantitiesMap = new HashMap<>();
        for (ProductPrepackageQuantity quantity : quantities) {
            List<ProductPrepackageQuantity> items = quantitiesMap.computeIfAbsent(quantity.getPrepackageItemId(), k -> new ArrayList<>());
            items.add(quantity);
        }
        return quantitiesMap;
    }

    /**
     * This method gets information for prepackage line items ids
     *
     * @param ids : prepackage line items ids
     */
    @Override
    public Map<String, PrepackageLineItemInfo> getPrepackageLineItemInfoById(String ids) {

        List<String> prepackageItemsIds = new ArrayList<String>(Arrays.asList(ids.split(",")));

        Map<String, PrepackageLineItemInfo> prepackageInfoMap = new HashMap<>();

        if (prepackageItemsIds.size() == 0) {
            throw new BlazeInvalidArgException("Prepackage product item", "Please provide valid prepackage product items itds");
        }

        List<ObjectId> prepackageLineItemIds = new ArrayList<>();
        for (String id : prepackageItemsIds) {
            if (StringUtils.isNotBlank(id) && ObjectId.isValid(id)) {
                prepackageLineItemIds.add(new ObjectId(id));
            }
        }

        Iterable<PrepackageLineItemInfo> prepackageProductItems = prepackageProductRepository.list(token.getCompanyId(), prepackageLineItemIds, PrepackageLineItemInfo.class);

        Set<ObjectId> prepackageIds = new HashSet<>();
        Set<ObjectId> batchIds = new HashSet<>();
        for (PrepackageProductItem item : prepackageProductItems) {
            if (StringUtils.isNotBlank(item.getPrepackageId()) && ObjectId.isValid(item.getPrepackageId())) {
                prepackageIds.add(new ObjectId(item.getPrepackageId()));
            }
            if (StringUtils.isNotBlank(item.getBatchId()) && ObjectId.isValid(item.getBatchId())) {
                batchIds.add(new ObjectId(item.getBatchId()));
            }
        }

        HashMap<String, Prepackage> prepackageMap = prepackageRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(prepackageIds));
        HashMap<String, ProductBatch> batchMap = batchRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(batchIds));

        for (PrepackageLineItemInfo item : prepackageProductItems) {
            item.setPrepackage(prepackageMap.get(item.getPrepackageId()));
            item.setProductBatch(batchMap.get(item.getBatchId()));

            prepackageInfoMap.put(item.getId(), item);
        }

        return prepackageInfoMap;
    }

}
