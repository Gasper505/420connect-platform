package com.fourtwenty.core.services.mgmt.impl;

import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.repositories.dispensary.PricingTemplateRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductWeightToleranceRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.ProductPriceBreakResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.mgmt.PricingTemplateService;
import com.fourtwenty.core.services.mgmt.ProductService;
import com.google.inject.Inject;
import com.google.inject.Provider;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class PricingTemplateServiceImpl extends AbstractAuthServiceImpl implements PricingTemplateService {

    private static final String PRICING_TEMPLATE = "Pricing template";
    private static final String TEMPLATE_NAME_NOT_FOUND = "Add pricing template name not found";
    private static final String TEMPLATE_NAME_EXIST = "Pricing template name already exist";
    private static final String PRICING_NOT_FOUND = "Tier pricing not found";
    private static final String PRICING_TEMPLATE_NOT_FOUND = "Pricing template not found";
    private static final String WEIGHT_TOLERANCE_NOT_FOUND = "Weight tolerance not found";
    private static final String WEIGHT_TOLERANCE_NOT_ENABLED = "Weight tolerance is not enabled";
    private static final String UNIT_TYPE_SAME_ERROR = "Unite type of template can't be updated";
    private static final String WEIGHT_PER_UNIT_TYPE_SAME_ERROR = "Weight per unit type of template can't be updated";

    @Inject
    private PricingTemplateRepository pricingTemplateRepository;
    @Inject
    private ProductWeightToleranceRepository weightToleranceRepository;
    @Inject
    private ProductService productService;

    @Inject
    public PricingTemplateServiceImpl(Provider<ConnectAuthToken> token) {
        super(token);
    }

    /**
     * Add price template
     *
     * @param pricingTemplate : object containing price template information
     */
    @Override
    public PricingTemplate createPricingTemplate(PricingTemplate pricingTemplate) {

        if (StringUtils.isBlank(pricingTemplate.getName())) {
            throw new BlazeInvalidArgException(PRICING_TEMPLATE, TEMPLATE_NAME_NOT_FOUND);
        }

        PricingTemplate templateByName = pricingTemplateRepository.getTemplateByName(token.getCompanyId(), token.getShopId(), pricingTemplate.getName());

        if (templateByName != null) {
            throw new BlazeInvalidArgException(PRICING_TEMPLATE, TEMPLATE_NAME_EXIST);
        }

        if (ProductCategory.UnitType.grams == pricingTemplate.getUnitType() && pricingTemplate.getPriceRanges() == null) {
            throw new BlazeInvalidArgException(PRICING_TEMPLATE, PRICING_NOT_FOUND);
        } else if (ProductCategory.UnitType.units == pricingTemplate.getUnitType() && pricingTemplate.getPriceBreaks() == null) {
            throw new BlazeInvalidArgException(PRICING_TEMPLATE, PRICING_NOT_FOUND);
        }

        pricingTemplate.prepare(token.getCompanyId());
        pricingTemplate.setShopId(token.getShopId());

        if (pricingTemplate.getPriceBreaks() != null) {

            for (ProductPriceBreak priceBreak : pricingTemplate.getPriceBreaks()) {
                priceBreak.prepare(token.getCompanyId());
                priceBreak.setId(ObjectId.get().toString());
            }

            pricingTemplate.getPriceBreaks().removeIf(priceBreak -> ProductPriceBreak.PriceBreakType.None == priceBreak.getPriceBreakType());
        }

        this.preparePriceRange(pricingTemplate);

        if (ProductCategory.UnitType.grams == pricingTemplate.getUnitType()) {
            pricingTemplate.setPriceBreaks(null);
        } else {
            pricingTemplate.setPriceRanges(null);
        }

        return pricingTemplateRepository.save(pricingTemplate);
    }

    /**
     * Update pricing template
     *
     * @param pricingTemplateId : template id
     * @param pricingTemplate   : updated information for pricing template
     */
    @Override
    public PricingTemplate updatePricingTemplate(String pricingTemplateId, PricingTemplate pricingTemplate) {

        PricingTemplate dbPricingTemplate = pricingTemplateRepository.get(token.getCompanyId(), pricingTemplateId);

        if (dbPricingTemplate == null) {
            throw new BlazeInvalidArgException(PRICING_TEMPLATE, PRICING_TEMPLATE_NOT_FOUND);
        }

        if (!dbPricingTemplate.getUnitType().equals(pricingTemplate.getUnitType())) {
            throw new BlazeInvalidArgException(PRICING_TEMPLATE, UNIT_TYPE_SAME_ERROR);
        }

        if (!dbPricingTemplate.getWeightPerUnit().equals(pricingTemplate.getWeightPerUnit())) {
            throw new BlazeInvalidArgException(PRICING_TEMPLATE, WEIGHT_PER_UNIT_TYPE_SAME_ERROR);
        }

        PricingTemplate templateByName = pricingTemplateRepository.getTemplateByName(token.getCompanyId(), token.getShopId(), pricingTemplate.getName());

        if (templateByName != null && !templateByName.getId().equalsIgnoreCase(pricingTemplateId)) {
            throw new BlazeInvalidArgException(PRICING_TEMPLATE, TEMPLATE_NAME_EXIST);
        }

        if (ProductCategory.UnitType.grams == pricingTemplate.getUnitType() && pricingTemplate.getPriceRanges() == null) {
            throw new BlazeInvalidArgException(PRICING_TEMPLATE, PRICING_NOT_FOUND);
        } else if (ProductCategory.UnitType.units == pricingTemplate.getUnitType() && pricingTemplate.getPriceBreaks() == null) {
            throw new BlazeInvalidArgException(PRICING_TEMPLATE, PRICING_NOT_FOUND);
        }


        if (ProductCategory.UnitType.grams == pricingTemplate.getUnitType()) {
            // handle price ranges
            pricingTemplate.setPriceBreaks(new ArrayList<ProductPriceBreak>());
            this.preparePriceRange(pricingTemplate);
        } else {
            // handle price breaks
            pricingTemplate.setPriceRanges(new ArrayList<ProductPriceRange>());
            if (pricingTemplate.getPriceBreaks() != null) {
                for (ProductPriceBreak priceBreak : pricingTemplate.getPriceBreaks()) {
                    priceBreak.prepare(token.getCompanyId());
                    if (StringUtils.isBlank(priceBreak.getId())) {
                        priceBreak.setId(ObjectId.get().toString());
                    }
                }

                pricingTemplate.getPriceBreaks().removeIf(priceBreak -> ProductPriceBreak.PriceBreakType.None == priceBreak.getPriceBreakType());
            }
        }

        pricingTemplateRepository.update(token.getCompanyId(), pricingTemplateId, pricingTemplate);

        productService.updateMemberGroupPrices(pricingTemplate);
        productService.applyPricingTemplateUpdate(pricingTemplate);
        return pricingTemplate;
    }

    /**
     * This method prepares price range for pricing template
     *
     * @param pricingTemplate : pricingTemplate object
     */
    private void preparePriceRange(PricingTemplate pricingTemplate) {
        if (pricingTemplate.getPriceRanges() != null && pricingTemplate.getPriceRanges().size() > 0) {
            Iterable<ProductWeightTolerance> weightTolerances = weightToleranceRepository.listSort(token.getCompanyId(), "{startWeight:1}");

            for (ProductPriceRange priceRange : pricingTemplate.getPriceRanges()) {
                for (ProductWeightTolerance tolerance : weightTolerances) {
                    if (StringUtils.isBlank(priceRange.getWeightToleranceId())) {
                        throw new BlazeInvalidArgException(PRICING_TEMPLATE, WEIGHT_TOLERANCE_NOT_FOUND);
                    }

                    if (tolerance.getId().equalsIgnoreCase(priceRange.getWeightToleranceId())) {
                        if (!tolerance.isEnabled()) {
                            throw new BlazeInvalidArgException(PRICING_TEMPLATE, WEIGHT_TOLERANCE_NOT_ENABLED);
                        }

                        priceRange.setWeightTolerance(tolerance);
                        priceRange.setPriority(tolerance.getPriority());
                        priceRange.setId(ObjectId.get().toString());
                        break;
                    }
                }
            }
        }
    }

    /**
     * Get pricing template not found
     *
     * @param pricingTemplateId : get template by id
     */
    @Override
    public PricingTemplate getPricingTemplateById(String pricingTemplateId) {

        PricingTemplate template = pricingTemplateRepository.get(token.getCompanyId(), pricingTemplateId);

        if (template == null) {
            throw new BlazeInvalidArgException(PRICING_TEMPLATE, PRICING_TEMPLATE_NOT_FOUND);
        }

        return template;
    }

    /**
     * Delete price template id
     *
     * @param pricingTemplateId : price template id
     */
    @Override
    public void deletePricingTemplateById(String pricingTemplateId) {

        PricingTemplate template = pricingTemplateRepository.get(token.getCompanyId(), pricingTemplateId);

        if (template == null) {
            throw new BlazeInvalidArgException(PRICING_TEMPLATE, PRICING_TEMPLATE_NOT_FOUND);
        }

        pricingTemplateRepository.removeById(token.getCompanyId(), pricingTemplateId);
        productService.removePricingTemplate(token.getCompanyId(), token.getShopId(), template);
    }

    /**
     * Fetch list of pricing templates by shop
     *
     * @param start : start
     * @param limit : limit
     */
    @Override
    public SearchResult<PricingTemplate> getPricingTemplates(int start, int limit) {
        if (limit == 0) {
            limit = Integer.MAX_VALUE;
        }

        return pricingTemplateRepository.findItems(token.getCompanyId(), token.getShopId(), start, limit);
    }


    @Override
    public ProductPriceBreakResult getPriceBreaksForUnitType(String weightPerUnit) {
        ProductPriceBreakResult result = new ProductPriceBreakResult();

        Product.WeightPerUnit weight = Product.WeightPerUnit.EACH;
        try {
            weight = Product.WeightPerUnit.valueOf(weightPerUnit.toUpperCase());
        } catch (Exception e) {
            throw new BlazeInvalidArgException("WeightPerUnit", "Invalid weight per unit");
        }

        result.setPriceBreaks(getProductPriceBreaks(true, weight));

        return result;
    }

    private List<ProductPriceBreak> getProductPriceBreaks(boolean isDefault, Product.WeightPerUnit weightPerUnit) {

        final ProductPriceBreak.PriceBreakType[] priceBreakTypes = ProductPriceBreak.PriceBreakType.values();

        List<ProductPriceBreak> priceBreaks = new ArrayList<>();
        for (ProductPriceBreak.PriceBreakType type : priceBreakTypes) {
            if (type == ProductPriceBreak.PriceBreakType.None) {
                continue;
            }

            if (type == ProductPriceBreak.PriceBreakType.HalfGramUnit) {
                if (weightPerUnit == Product.WeightPerUnit.FULL_GRAM
                        || weightPerUnit == Product.WeightPerUnit.EACH
                        || weightPerUnit == Product.WeightPerUnit.EIGHTH
                        || weightPerUnit == Product.WeightPerUnit.FOURTH
                        || weightPerUnit == Product.WeightPerUnit.CUSTOM_GRAMS) {
                    continue;
                }
            }

            // find the price break by type
            ProductPriceBreak priceBreak = null;
            // if none is found, create a new one
            if (priceBreak == null) {
                // Add a new one
                priceBreak = new ProductPriceBreak();
                priceBreak.prepare(token.getCompanyId());
                priceBreak.setPriceBreakType(type);
                priceBreak.setActive(false);


                // add price break to group
                priceBreaks.add(priceBreak);
            }

            // specify name and quantity
            if (weightPerUnit == Product.WeightPerUnit.EACH
                    || weightPerUnit == Product.WeightPerUnit.EIGHTH
                    || weightPerUnit == Product.WeightPerUnit.FOURTH
                    || weightPerUnit == Product.WeightPerUnit.CUSTOM_GRAMS) {
                priceBreak.setName(type.eachName);
                priceBreak.setQuantity(type.fullGramValue);
            } else if (weightPerUnit == Product.WeightPerUnit.FULL_GRAM) {
                priceBreak.setQuantity(type.fullGramValue);
                priceBreak.setName(type.gramName);
            } else {
                priceBreak.setName(type.gramName);
                priceBreak.setQuantity(type.halfGramValue);
            }
            priceBreak.setPrice(new BigDecimal(0));

            // default cost
            if (isDefault) {
                if (weightPerUnit == Product.WeightPerUnit.EACH
                        || weightPerUnit == Product.WeightPerUnit.FULL_GRAM
                        || weightPerUnit == Product.WeightPerUnit.EIGHTH
                        || weightPerUnit == Product.WeightPerUnit.FOURTH
                        || weightPerUnit == Product.WeightPerUnit.CUSTOM_GRAMS) {
                    if (priceBreak.getPriceBreakType() == ProductPriceBreak.PriceBreakType.OneGramUnit) {
                        priceBreak.setActive(true);
                    }
                } else {
                    if (priceBreak.getPriceBreakType() == ProductPriceBreak.PriceBreakType.HalfGramUnit) {
                        priceBreak.setActive(true);
                    }
                }
            }

        }

        // Sort price breaks
        priceBreaks.sort(new Comparator<ProductPriceBreak>() {
            @Override
            public int compare(ProductPriceBreak o1, ProductPriceBreak o2) {
                return ((Integer) o1.getQuantity()).compareTo((Integer) o2.getQuantity());
            }
        });

        return priceBreaks;
    }
}
