package com.fourtwenty.core.services.mgmt.impl;

import com.amazonaws.util.CollectionUtils;
import com.fourtwenty.core.domain.models.company.BarcodeItem;
import com.fourtwenty.core.domain.models.company.CompanyFeatures;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrder;
import com.fourtwenty.core.domain.models.testsample.TestSample;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.domain.repositories.testsample.TestSampleRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.importer.model.CustomProductBatchResult;
import com.fourtwenty.core.importer.model.ProductBatchResult;
import com.fourtwenty.core.reporting.model.reportmodels.ProductBatchByInventory;
import com.fourtwenty.core.rest.dispensary.requests.inventory.BulkProductBatchUpdateRequest;
import com.fourtwenty.core.rest.dispensary.requests.inventory.ProductBatchScannedRequest;
import com.fourtwenty.core.rest.dispensary.requests.inventory.ProductBatchStatusRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.common.RealtimeService;
import com.fourtwenty.core.services.inventory.BatchQuantityService;
import com.fourtwenty.core.services.mgmt.*;
import com.fourtwenty.core.util.DateUtil;
import com.google.inject.Provider;
import org.apache.commons.lang3.StringUtils;
import org.assertj.core.util.Lists;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.*;

public class ProductBatchServiceImpl extends AbstractAuthServiceImpl implements ProductBatchService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductBatchServiceImpl.class);
    private static final String BATCH_NOT_FOUND = "Product Batch does not found";
    private static final String BARCODE_NOT_FOUND = "Bar code does not found";
    private static final String PRODUCT_BATCH = "Product Batch";
    private static final String PRODUCT = "Product";
    private static final String PRODUCT_NOT_FOUND = "Product does not found";

    private ProductBatchRepository productBatchRepository;
    private BrandRepository brandRepository;
    private VendorRepository vendorRepository;
    private ProductRepository productRepository;
    private InventoryService inventoryService;
    private BarcodeService barcodeService;
    private InventoryRepository inventoryRepository;
    private ProductChangeLogRepository productChangeLogRepository;
    private RealtimeService realtimeService;
    private BatchQuantityService batchQuantityService;
    private PurchaseOrderRepository purchaseOrderRepository;
    private TestSampleRepository testSampleRepository;
    private InventoryTransferHistoryService transferHistoryService;
    private BatchActivityLogService batchActivityLogService;
    private BatchQuantityRepository batchQuantityRepository;
    private ProductService productService;
    @Inject
    private DerivedProductBatchLogRepository derivedProductBatchLogRepository;
    @Inject
    private ShopRepository shopRepository;

    @Inject
    ProductCategoryRepository productCategoryRepository;
    @Inject
    public ProductBatchServiceImpl(Provider<ConnectAuthToken> token,
                                   ProductBatchRepository productBatchRepository,
                                   BrandRepository brandRepository,
                                   VendorRepository vendorRepository,
                                   ProductRepository productRepository,
                                   InventoryService inventoryService,
                                   BarcodeService barcodeService,
                                   InventoryRepository inventoryRepository,
                                   ProductChangeLogRepository productChangeLogRepository,
                                   RealtimeService realtimeService,
                                   BatchQuantityService batchQuantityService,
                                   PurchaseOrderRepository purchaseOrderRepository,
                                   TestSampleRepository testSampleRepository,
                                   InventoryTransferHistoryService transferHistoryService,
                                   BatchActivityLogService batchActivityLogService,
                                   BatchQuantityRepository batchQuantityRepository,
                                   ProductService productService) {
        super(token);
        this.productBatchRepository = productBatchRepository;
        this.brandRepository = brandRepository;
        this.vendorRepository = vendorRepository;
        this.productRepository = productRepository;
        this.inventoryService = inventoryService;
        this.barcodeService = barcodeService;
        this.inventoryRepository = inventoryRepository;
        this.productChangeLogRepository = productChangeLogRepository;
        this.realtimeService = realtimeService;
        this.batchQuantityService = batchQuantityService;
        this.purchaseOrderRepository = purchaseOrderRepository;
        this.testSampleRepository = testSampleRepository;
        this.transferHistoryService = transferHistoryService;
        this.batchActivityLogService = batchActivityLogService;
        this.batchQuantityRepository = batchQuantityRepository;
        this.productService = productService;
    }

    /**
     * This method is used to get product batch with productBatchId.
     *
     * @param productBatchId
     * @return
     */
    @Override
    public ProductBatchResult getProductBatch(String productBatchId) {
        ProductBatchResult productBatchResult = productBatchRepository.get(token.getCompanyId(), productBatchId, ProductBatchResult.class);

        if (productBatchResult == null) {
            throw new BlazeInvalidArgException(PRODUCT_BATCH, BATCH_NOT_FOUND);
        }
        prepareProductBatchResult(Lists.newArrayList(productBatchResult));
        return productBatchResult;
    }

    /**
     * This public method is used to prepare product batch for result.
     *
     * @param productBatchResults
     */
    @Override
    public void prepareProductBatchResult(List<ProductBatchResult> productBatchResults) {
        Brand brand;
        Vendor vendor;
        Product product;
        PurchaseOrder purchaseOrder;

        List<ObjectId> poIds = new ArrayList<>();
        List<ObjectId> brandIds = new ArrayList<>();
        List<ObjectId> productIds = new ArrayList<>();
        List<ObjectId> vendorIds = new ArrayList<>();
        List<String> batchIds = new ArrayList<>();
        List<ObjectId> derivedLogId = new ArrayList<>();

        for (ProductBatchResult productBatchResult : productBatchResults) {
            if (StringUtils.isNotBlank(productBatchResult.getBrandId()) && ObjectId.isValid(productBatchResult.getBrandId())) {
                brandIds.add(new ObjectId(productBatchResult.getBrandId()));
            }
            if (StringUtils.isNotBlank(productBatchResult.getProductId()) && ObjectId.isValid(productBatchResult.getProductId())) {
                productIds.add(new ObjectId(productBatchResult.getProductId()));
            }
            if (StringUtils.isNotBlank(productBatchResult.getVendorId()) && ObjectId.isValid(productBatchResult.getVendorId())) {
                vendorIds.add(new ObjectId(productBatchResult.getVendorId()));
            }
            if (StringUtils.isNotBlank(productBatchResult.getPurchaseOrderId()) && ObjectId.isValid(productBatchResult.getPurchaseOrderId())) {
                poIds.add(new ObjectId(productBatchResult.getPurchaseOrderId()));
            }
            batchIds.add(productBatchResult.getId());
            if (StringUtils.isNotBlank(productBatchResult.getDerivedLogId()) && ObjectId.isValid(productBatchResult.getDerivedLogId())) {
                derivedLogId.add(new ObjectId(productBatchResult.getDerivedLogId()));
            }
        }

        HashMap<String, Brand> brandMap = brandRepository.listAsMap(token.getCompanyId(), brandIds);
        HashMap<String, Product> productHashMap = productRepository.listAsMap(token.getCompanyId(), productIds);
        HashMap<String, Vendor> vendorHashMap = vendorRepository.listAsMap(token.getCompanyId(), vendorIds);
        HashMap<String, PurchaseOrder> purchaseOrderHashMap = purchaseOrderRepository.listAsMap(token.getCompanyId(), poIds);
        HashMap<String, List<BatchActivityLog>> batchActivityLogHashMap = batchActivityLogService.findItemsByBatchId(token.getCompanyId(), token.getShopId(), batchIds);
        HashMap<String, DerivedProductBatchLog> derivedLogMap = derivedProductBatchLogRepository.listAsMap(token.getCompanyId(), derivedLogId);
        HashMap<String, List<TestSample>> testResultHashMap = testSampleRepository.getTestSampleByBatchAsMap(token.getCompanyId(), token.getShopId(), batchIds);

        for (ProductBatchResult productBatchResult : productBatchResults) {
            brand = brandMap.get(productBatchResult.getBrandId());
            product = productHashMap.get(productBatchResult.getProductId());
            vendor = vendorHashMap.get(productBatchResult.getVendorId());
            purchaseOrder = purchaseOrderHashMap.get(productBatchResult.getPurchaseOrderId());
            List<BatchActivityLog> batchLogs = batchActivityLogHashMap.get(productBatchResult.getId());
            DerivedProductBatchLog derivedLog = derivedLogMap.get(productBatchResult.getDerivedLogId());

            if (brand != null) {
                productBatchResult.setBrandName((StringUtils.isBlank(brand.getName()) ? "" : brand.getName()));
            }

            if (product != null) {
                productBatchResult.setProductName((StringUtils.isBlank(product.getName()) ? "" : product.getName()));
                productBatchResult.setProductType(product.getProductSaleType().toString());
            }

            if (vendor != null) {
                productBatchResult.setVendorName((StringUtils.isBlank(vendor.getName()) ? "" : vendor.getName()));
            }

            if (purchaseOrder != null) {
                productBatchResult.setPoNumber(purchaseOrder.getPoNumber());
                productBatchResult.setPurchaseOrderId(purchaseOrder.getId());
            }

            if (batchLogs != null && !batchLogs.isEmpty()) {
                batchLogs.sort(new Comparator<BatchActivityLog>() {
                    @Override
                    public int compare(BatchActivityLog log1, BatchActivityLog log2) {
                        DateTime d1 = DateUtil.parseLongToDate(log1.getModified());
                        DateTime d2 = DateUtil.parseLongToDate(log2.getModified());
                        return d2.compareTo(d1);
                    }
                });
                productBatchResult.setBatchActivityLog(batchLogs);
            }
            HashMap<String, Inventory> inventoryMap = inventoryRepository.listAsMap(token.getCompanyId(), token.getShopId());
            List<ProductBatchByInventory> quantities = batchQuantityRepository.getBatchQuantitiesByInventory(token.getCompanyId(), token.getShopId(), productBatchResult.getId(), productBatchResult.getProductId());
            Map<String, List<BatchQuantity>> quantityByInventoryMap = new HashMap<>();
            if (!CollectionUtils.isNullOrEmpty(quantities)) {
                for (ProductBatchByInventory quantity : quantities) {
                    quantityByInventoryMap.putIfAbsent(quantity.getInventoryId(), new ArrayList<>());
                    quantityByInventoryMap.get(quantity.getInventoryId()).addAll(quantity.getBatchQuantity());
                }
            }
            Map<String, BigDecimal> batchQuantityMap = new HashMap<>();
            for (Map.Entry<String, Inventory> entry : inventoryMap.entrySet()) {
                if (entry.getValue() != null && entry.getValue().isActive()) {
                    List<BatchQuantity> batchQuantities = quantityByInventoryMap.get(entry.getKey());

                    double quantity = 0d;
                    //Calculate batch quantity
                    if (!CollectionUtils.isNullOrEmpty(batchQuantities)) {
                        for (BatchQuantity batchQuantity : batchQuantities) {
                            quantity += batchQuantity.getQuantity().doubleValue();
                        }
                    }

                    batchQuantityMap.putIfAbsent(entry.getKey(), new BigDecimal(0));
                    BigDecimal totalQuantity = new BigDecimal(batchQuantityMap.get(entry.getKey()).doubleValue() + quantity);
                    batchQuantityMap.put(entry.getKey(), totalQuantity);
                }
            }
            productBatchResult.setBatchQuantityMap(batchQuantityMap);
            productBatchResult.setDerivedBatchLog(derivedLog);
            productBatchResult.setTestSamples(testResultHashMap.get(productBatchResult.getId()));

        }
    }


    /**
     * This method is used to get all product batches of shop.
     *
     * @param start
     * @param limit
     * @return
     */
    @Override
    public SearchResult<ProductBatchResult> getAllProductBatches(int start, int limit, String searchTerm, ProductBatch.BatchFilter filter) {
        if (limit == 0) {
            limit = Integer.MAX_VALUE;
        }
        SearchResult<ProductBatchResult> searchResult = productBatchRepository.getAllProductBatches(token.getCompanyId(), token.getShopId(), "{modified:-1}", start, limit);

        if (searchResult.getValues().size() > 0) {
            prepareProductBatchResult(searchResult.getValues());
        }
        return searchResult;


    }

    /**
     * This method is used to get all archived product batch.
     *
     * @param start
     * @param limit
     * @return
     */
    @Override
    public SearchResult<ProductBatchResult> getAllArchivedProductBatches(int start, int limit) {
        if (limit == 0) {
            limit = Integer.MAX_VALUE;
        }
        return productBatchRepository.getAllArchivedProductBatches(token.getCompanyId(), token.getShopId(), "{modified:-1}", start, limit);
    }

    /**
     * This method is used to update product batch status
     *
     * @param productBatchId
     * @param request
     * @return
     */
    @Override
    public ProductBatch updateProductBatchStatus(String productBatchId, ProductBatchStatusRequest request) {
        ProductBatch dbProductBatch = null;
        ProductBatch productBatch = productBatchRepository.get(token.getCompanyId(), productBatchId);
        if (productBatch == null) {
            throw new BlazeInvalidArgException(PRODUCT_BATCH, BATCH_NOT_FOUND);
        }
        if (productBatch.getStatus() == request.getStatus()) {
            return productBatch;
        }
        boolean statusChange = false;
        if (request.getStatus().equals(ProductBatch.BatchStatus.IN_TESTING)) {
            productBatch.setStatus(request.getStatus());
            statusChange = true;
        } else if (request.getStatus().equals(ProductBatch.BatchStatus.READY_FOR_SALE)) {
            batchMoveToSale(productBatch, request);
            productBatch.setStatus(request.getStatus());
            // Notify that inventory has been updated
            realtimeService.sendRealTimeEvent(token.getShopId().toString(),
                    RealtimeService.RealtimeEventType.InventoryUpdateEvent, null);

            statusChange = true;

        } else if (request.getStatus().equals(ProductBatch.BatchStatus.RECEIVED)) {
            productBatch.setStatus(request.getStatus());
            statusChange = true;
        }

        if (statusChange) {
            dbProductBatch = productBatchRepository.update(token.getCompanyId(), productBatchId, productBatch);
            batchActivityLogService.addBatchActivityLog(dbProductBatch.getId(), token.getActiveTopUser().getUserId(), dbProductBatch.getSku() + " Batch status changed to " + dbProductBatch.getStatus());
        }
        return dbProductBatch;
    }

    /**
     * This method is used to get All product batch by state
     *
     * @param start
     * @param limit
     * @param state
     * @return
     */
    @Override
    public SearchResult<ProductBatchResult> getAllProductBatchesByState(int start, int limit, boolean state) {
        if (limit == 0) {
            limit = Integer.MAX_VALUE;
        }
        SearchResult<ProductBatchResult> searchResult = productBatchRepository.getAllProductBatchesByState(token.getCompanyId(), token.getShopId(), state, "{modified:-1}", start, limit);

        if (searchResult.getValues().size() > 0) {
            prepareProductBatchResult(searchResult.getValues());
        }
        return searchResult;
    }

    /**
     * This method is used to get all product batch status
     *
     * @param start
     * @param limit
     * @param status
     * @return
     */
    @Override
    public SearchResult<ProductBatchResult> getAllProductBatchesByStatus(int start, int limit, ProductBatch.BatchStatus status, String searchTerm, ProductBatch.BatchFilter filter) {
        if (limit == 0) {
            limit = Integer.MAX_VALUE;
        }

        Shop shop = shopRepository.get(token.getCompanyId(),token.getShopId());
        if (shop == null) {
            throw new BlazeInvalidArgException("Shop", "Invalid shop.");
        }

        SearchResult<ProductBatchResult> searchResult;
        if (shop.getAppTarget() == CompanyFeatures.AppTarget.Retail) {

            // get all batches
            searchResult = productBatchRepository.findItems(token.getCompanyId(), token.getShopId(), "{modified:-1}", start, limit,  ProductBatchResult.class);

        } else {
            status = status == null ? ProductBatch.BatchStatus.RECEIVED : status;

            if (StringUtils.isNotBlank(searchTerm)) {
                searchResult = productBatchRepository.getAllProductBatchesByTermWithStatus(token.getCompanyId(), token.getShopId(), status, "{modified:-1}", start, limit, searchTerm, ProductBatchResult.class);
            } else {
                searchResult = productBatchRepository.getAllProductBatchesByStatus(token.getCompanyId(), token.getShopId(), status, "{modified:-1}", start, limit, ProductBatchResult.class);

            }
        }


        if (searchResult.getValues().size() > 0) {
            prepareProductBatchResult(searchResult.getValues());
        }
        filter = filter == null ? ProductBatch.BatchFilter.RECEIVE_DATE : filter;


        if (ProductBatch.BatchFilter.BRAND_NAME == filter) {
            Collections.sort(searchResult.getValues(), new Comparator<ProductBatchResult>() {
                public int compare(ProductBatchResult one, ProductBatchResult other) {
                    if (one.getBrandName() != null && other.getBrandName() != null) {
                        int val = one.getBrandName().compareTo(other.getBrandName());
                        return val;
                    } else {
                        return 0;
                    }

                }
            });
            return searchResult;
        } else if (ProductBatch.BatchFilter.CATEGORY == filter) {
            Collections.sort(searchResult.getValues(), new Comparator<ProductBatchResult>() {
                public int compare(ProductBatchResult one, ProductBatchResult other) {
                    if (one.getProductType() != null && other.getProductType() != null) {
                        int val = one.getProductType().compareTo(other.getProductType());
                        return val;
                    } else {
                        return 0;
                    }

                }
            });
            return searchResult;

        } else if (ProductBatch.BatchFilter.PO_NUMBER == filter) {
            Collections.sort(searchResult.getValues(), new Comparator<ProductBatchResult>() {
                public int compare(ProductBatchResult one, ProductBatchResult other) {
                    if (one.getPoNumber() != null && other.getPoNumber() != null) {
                        int val = one.getPoNumber().compareTo(other.getPoNumber());
                        return val;
                    } else {
                        return 0;
                    }

                }
            });
            return searchResult;

        } else if (ProductBatch.BatchFilter.PRODUCT_NAME == filter) {
            Collections.sort(searchResult.getValues(), new Comparator<ProductBatchResult>() {
                public int compare(ProductBatchResult one, ProductBatchResult other) {
                    if (one.getProductName() != null && other.getProductName() != null) {
                        int val = one.getProductName().compareTo(other.getProductName());
                        return val;
                    } else {
                        return 0;
                    }

                }
            });
            return searchResult;

        } else if (ProductBatch.BatchFilter.QUANTITY_RECEIVED == filter) {
            Collections.sort(searchResult.getValues(), new Comparator<ProductBatchResult>() {
                public int compare(ProductBatchResult one, ProductBatchResult other) {
                    if (one.getQuantity() != null && other.getQuantity() != null) {
                        int val = one.getQuantity().compareTo(other.getQuantity());
                        return val;
                    } else {
                        return 0;
                    }

                }
            });
            return searchResult;

        } else if (ProductBatch.BatchFilter.BATCH_ID == filter) {
            Collections.sort(searchResult.getValues(), new Comparator<ProductBatchResult>() {
                public int compare(ProductBatchResult one, ProductBatchResult other) {
                    if (one.getSku() != null && other.getSku() != null) {
                        int val = one.getSku().compareTo(other.getSku());
                        return val;
                    } else {
                        return 0;
                    }

                }
            });
            return searchResult;

        } else if (ProductBatch.BatchFilter.STATUS == filter) {
            Collections.sort(searchResult.getValues(), new Comparator<ProductBatchResult>() {
                public int compare(ProductBatchResult one, ProductBatchResult other) {
                    if (one.getStatus() != null && other.getStatus() != null) {
                        int val = one.getStatus().compareTo(other.getStatus());
                        return val;
                    } else {
                        return 0;
                    }

                }
            });
            return searchResult;

        } else if (ProductBatch.BatchFilter.VENDOR_NAME == filter) {
            Collections.sort(searchResult.getValues(), new Comparator<ProductBatchResult>() {
                public int compare(ProductBatchResult one, ProductBatchResult other) {
                    if (one.getVendorName() != null && other.getVendorName() != null) {
                        int val = one.getVendorName().compareTo(other.getVendorName());
                        return val;
                    } else {
                        return 0;
                    }

                }
            });
            return searchResult;

        } else {
            return searchResult;
        }
    }

    /**
     * This method is used to get all scanned product batches.
     *
     * @param request
     * @param start
     * @param limit
     * @return
     */
    @Override
    public SearchResult<ProductBatchResult> getAllScannedProductBatch(ProductBatchScannedRequest request, int start, int limit) {

        BarcodeItem barcodeItem = barcodeService.getBarcodeItemByBarcode(token.getCompanyId(), token.getShopId(), request.getEntityType(), request.getBarcode());
        if (barcodeItem == null) {
            throw new BlazeInvalidArgException(PRODUCT_BATCH, BARCODE_NOT_FOUND);
        }
        if (limit == 0) {
            limit = Integer.MAX_VALUE;
        }
        SearchResult<ProductBatchResult> searchResult = productBatchRepository.getAllScannedProductBatch(token.getCompanyId(), token.getShopId(), request.getMertcId(), barcodeItem.getProductId(), start, limit, "{modified:-1}");

        if (searchResult.getValues().size() > 0) {
            prepareProductBatchResult(searchResult.getValues());
        }

        return searchResult;
    }

    /**
     * This method is used to get all deleted product batch
     *
     * @param start
     * @param limit
     * @return
     */
    @Override
    public SearchResult<ProductBatchResult> getAllDeletedProductBatch(int start, int limit) {
        if (limit == 0) {
            limit = Integer.MAX_VALUE;
        }
        SearchResult<ProductBatchResult> searchResult = productBatchRepository.getAllDeletedProductBatch(token.getCompanyId(), token.getShopId(), "{modified:-1}", start, limit);
        if (searchResult.getValues().size() > 0) {
            prepareProductBatchResult(searchResult.getValues());
        }

        return searchResult;
    }

    /**
     * This method is used to bulk update product batch
     *
     * @param request
     * @return
     */
    @Override
    public void bulkProductBatchUpdate(BulkProductBatchUpdateRequest request) {
        List<String> batchIds = request.getProductBatchIds();
        if (batchIds == null || batchIds.isEmpty()) {
            throw new BlazeInvalidArgException(PRODUCT_BATCH, "Please select Batches");
        }
        if (request.getOperationType() == null) {
            throw new BlazeInvalidArgException(PRODUCT_BATCH, "Please select an action type");
        }
        List<ObjectId> objectIds = new ArrayList<>();
        for (String id : request.getProductBatchIds()) {
            objectIds.add(new ObjectId(id));
        }
        String logMessage = "";
        switch (request.getOperationType()) {
            case STATUS:
                logMessage = " status updated to " + request.getBatchStatus();
                if (request.getBatchStatus() == null) {
                    throw new BlazeInvalidArgException(PRODUCT_BATCH, "Please select a status");
                }

                if (request.getBatchStatus().equals(ProductBatch.BatchStatus.READY_FOR_SALE)) {
                    HashMap<String, ProductBatch> productBatchMap = productBatchRepository.listAsMap(token.getCompanyId(), objectIds);
                    for (String batchId : request.getProductBatchIds()) {
                        ProductBatch productBatch = productBatchMap.get(batchId);
                        if (productBatch == null)
                            continue;
                        if (productBatch.getStatus() == ProductBatch.BatchStatus.READY_FOR_SALE) {
                            continue;
                        }
                        Product product = productRepository.get(token.getCompanyId(), productBatch.getProductId());
                        if (product != null) {
                            Inventory quarantineInventory = inventoryRepository.getInventory(token.getCompanyId(), token.getShopId(), Inventory.QUARANTINE);

                            Inventory safeInventory = inventoryRepository.getInventory(token.getCompanyId(), token.getShopId(), Inventory.SAFE);

                            createBatchTransferRequest(productBatch, quarantineInventory, safeInventory, product);

                            productBatch.setStatus(request.getBatchStatus());
                            productBatchRepository.update(token.getCompanyId(), batchId, productBatch);

                        }
                    }
                } else {
                    productBatchRepository.bulkProductBatchStatusUpdate(token.getCompanyId(), token.getShopId(), objectIds, request.getBatchStatus());
                }
                break;
            case DELETE_BATCHES:
                logMessage = " deleted.";
                productBatchRepository.bulkProductBatchDelete(token.getCompanyId(), token.getShopId(), objectIds);
                break;
            case UPDATE_BATCHES_TO_ARCHIVE:
                logMessage = " batch archived";
                productBatchRepository.bulkProductBatchArchive(token.getCompanyId(), token.getShopId(), objectIds, request.isArchive());
                break;
            case VOID_STATUS:
                logMessage = " status updated to void.";
                productBatchRepository.bulkProductBatchVoidStatusUpdate(token.getCompanyId(), token.getShopId(), objectIds, request.isVoidStatus());
        }
        batchActivityLogService.addBatchActivityLog(objectIds, token.getActiveTopUser().getUserId(), logMessage);


    }

    /**
     * This method is used to update product batch void status
     *
     * @param productBatchId
     * @param voidStatus
     * @return
     */
    @Override
    public ProductBatch updateProductBatchVoidStatus(String productBatchId, boolean voidStatus) {
        ProductBatch productBatch = null;
        if (StringUtils.isBlank(productBatchId)) {
            throw new BlazeInvalidArgException(PRODUCT_BATCH, BATCH_NOT_FOUND);
        }

        ProductBatch dbProductBatch = productBatchRepository.get(token.getCompanyId(), productBatchId);

        if (dbProductBatch == null) {
            throw new BlazeInvalidArgException(PRODUCT_BATCH, BATCH_NOT_FOUND);
        }

        if (!dbProductBatch.getStatus().equals(ProductBatch.BatchStatus.READY_FOR_SALE)) {
            dbProductBatch.setVoidStatus(voidStatus);
            productBatch = productBatchRepository.update(token.getCompanyId(), productBatchId, dbProductBatch);
            batchActivityLogService.addBatchActivityLog(productBatchId, token.getActiveTopUser().getUserId(), dbProductBatch.getSku() + " batch status updated to void.");
        } else {
            throw new BlazeInvalidArgException("ProductBatch", "Product batch is already in Ready For Sale state");
        }

        return productBatch;
    }

    /**
     * This method is used to get all product batches by void status
     *
     * @param start      : start
     * @param limit      : limit
     * @param voidStatus : status for void
     */
    @Override
    public SearchResult<ProductBatchResult> getAllProductBatchesByVoidStatus(int start, int limit, boolean voidStatus) {
        if (limit == 0) {
            limit = Integer.MAX_VALUE;
        }
        SearchResult<ProductBatchResult> searchResult = productBatchRepository.getAllProductBatchesByVoidStatus(token.getCompanyId(), token.getShopId(), voidStatus, "{modified:-1}", start, limit);
        if (searchResult.getValues().size() > 0) {
            prepareProductBatchResult(searchResult.getValues());
        }

        return searchResult;
    }

    private void createBatchTransferRequest(ProductBatch productBatch, Inventory quarantineInventory, Inventory safeInventory, Product product) {
        if (quarantineInventory == null) {
            return;
        }
        if (safeInventory == null) {
            return;
        }
        ProductQuantity quarantineQuantity = null;
        for (ProductQuantity q : product.getQuantities()) {
            if (q.getInventoryId().equalsIgnoreCase(quarantineInventory.getId())) {
                quarantineQuantity = q;
                break;
            }
        }
        if (quarantineQuantity == null) {
            return;
        }

        InventoryTransferHistory transferRequest = new InventoryTransferHistory();
        transferRequest.setFromInventoryId(quarantineInventory.getId());
        transferRequest.setToInventoryId(safeInventory.getId());
        transferRequest.setFromShopId(token.getShopId());
        transferRequest.setToShopId(token.getShopId());
        transferRequest.setCompleteTransfer(true);
        transferRequest.setTransferByBatch(true);
        InventoryTransferLog transferLog = new InventoryTransferLog();
        transferLog.setProductId(product.getId());
        transferLog.setFromBatchId(productBatch.getId());
        transferLog.setToBatchId(productBatch.getId());

        BatchQuantity batchQuantity = batchQuantityRepository.getBatchQuantityForInventory(token.getCompanyId(), token.getShopId(), productBatch.getProductId(), quarantineInventory.getId(), productBatch.getId());
        BigDecimal quantity = (batchQuantity != null) ? batchQuantity.getQuantity() : new BigDecimal(0);
        /*double testQuantities =0.0;
        SearchResult<TestSample> sampleSearchResult = testSampleRepository.getAllTestSamplesByBatchId ( token.getCompanyId (),productBatch.getId (),"{modified:-1}",0,Integer.MAX_VALUE );
        if(!CollectionUtils.isNullOrEmpty ( sampleSearchResult.getValues () )){
            for(TestSample testsample : sampleSearchResult.getValues ()){
                testQuantities = testQuantities + testsample.getSample ().doubleValue ();
            }
        }
        quantity = quantity.subtract ( new BigDecimal ( testQuantities ) );*/
        transferLog.setTransferAmount(quantity);
        LinkedHashSet<InventoryTransferLog> transferLogs = new LinkedHashSet<>();
        transferLogs.add(transferLog);
        transferRequest.setTransferLogs(transferLogs);

        try {
            if (quantity.doubleValue() <= 0) {
                LOGGER.error("Transfer amount is less than or equal to zero");
                return;
            } else {
                transferHistoryService.bulkTransferInventoryByBatches(transferRequest);
            }
        } catch (Exception e) {
            LOGGER.error("Error in inventory transfer : " + e.getMessage());
            throw new BlazeInvalidArgException(PRODUCT_BATCH, "Product Batch could not move to ready for sale due to :" + e.getMessage());
        }
    }

    @Override
    public void batchMoveToSale(ProductBatch productBatch, ProductBatchStatusRequest request) {
        Inventory quarantineInventory = inventoryRepository.getInventory(token.getCompanyId(), token.getShopId(), Inventory.QUARANTINE);

        Inventory safeInventory = inventoryRepository.getInventory(token.getCompanyId(), token.getShopId(), Inventory.SAFE);

        Product product = productRepository.get(token.getCompanyId(), productBatch.getProductId());
        if (product == null) {
            throw new BlazeInvalidArgException(PRODUCT, PRODUCT_NOT_FOUND);
        }

        createBatchTransferRequest(productBatch, quarantineInventory, safeInventory, product);
    }

    @Override
    public CustomProductBatchResult getProductBatchWithProduct(String productBatchId) {

        ProductBatchResult productBatchResult = this.getProductBatch(productBatchId);
        Product product = productService.getProductById(productBatchResult.getProductId());
        if (product == null) {
            throw new BlazeInvalidArgException(PRODUCT, PRODUCT_NOT_FOUND);
        }
        ProductCategory productCategory = productCategoryRepository.get(token.getCompanyId(), product.getCategoryId());
        return new CustomProductBatchResult(productBatchResult, product, productCategory);
    }

    @Override
    public SearchResult<CustomProductBatchResult> getAllProductBatchesWithProduct(int start, int limit, String searchTerm, ProductBatch.BatchFilter filter) {

        SearchResult<CustomProductBatchResult> allProductBatchesWithProduct = new SearchResult<>();
        this.prepareCustomProductBatchResult(this.getAllProductBatches(start, limit, searchTerm, filter), allProductBatchesWithProduct);
        return allProductBatchesWithProduct;
    }

    private void prepareCustomProductBatchResult(SearchResult<ProductBatchResult> allProductBatches, SearchResult<CustomProductBatchResult> allProductBatchesWithProduct) {

        List<CustomProductBatchResult> customProductBatchResultList = new ArrayList<>();
        for (ProductBatchResult batchResult : allProductBatches.getValues()) {
            customProductBatchResultList.add(getProductBatchWithProduct(batchResult.getId()));
        }
        allProductBatchesWithProduct.setValues(customProductBatchResultList);
    }

    public Map<String, ProductBatch> getBatchesByBatchesId(List<String> batchIdList){
        return productBatchRepository.getBatchesByBatchesId(batchIdList);
    }
}
