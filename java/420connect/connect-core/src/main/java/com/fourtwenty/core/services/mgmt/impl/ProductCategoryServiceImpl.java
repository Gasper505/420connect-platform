package com.fourtwenty.core.services.mgmt.impl;

import com.fourtwenty.core.domain.models.product.MemberGroupPrices;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.domain.repositories.dispensary.MemberGroupPricesRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductCategoryRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.managed.BackgroundTaskManager;
import com.fourtwenty.core.rest.dispensary.requests.inventory.ProductCategoryAddRequest;
import com.fourtwenty.core.rest.dispensary.requests.inventory.ProductCategoryOrderItem;
import com.fourtwenty.core.rest.dispensary.requests.inventory.ProductCategoryUpdateOrderRequest;
import com.fourtwenty.core.rest.dispensary.results.ListResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.common.RealtimeService;
import com.fourtwenty.core.services.mgmt.ProductCategoryService;
import com.fourtwenty.core.services.thirdparty.WeedmapService;
import com.fourtwenty.core.tasks.MigrateProductsToPublicTask;
import com.google.common.collect.Lists;
import com.google.inject.Provider;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

/**
 * Created by mdo on 10/14/15.
 */
public class ProductCategoryServiceImpl extends AbstractAuthServiceImpl implements ProductCategoryService {
    @Inject
    ProductCategoryRepository productCategoryRepository;
    @Inject
    ProductRepository productRepository;
    @Inject
    RealtimeService realtimeService;
    @Inject
    MigrateProductsToPublicTask migrateProductsToPublicTask;
    @javax.inject.Inject
    BackgroundTaskManager backgroundTaskManager;
    @Inject
    private MemberGroupPricesRepository memberGroupPricesRepository;
    @Inject
    private WeedmapService weedmapService;

    @Inject
    public ProductCategoryServiceImpl(Provider<ConnectAuthToken> token) {
        super(token);
    }

    @Override
    public ListResult<ProductCategory> getProductCategories(String shopId) {
        if (StringUtils.isBlank(shopId)) {
            shopId = token.getShopId();
        }

        Iterable<ProductCategory> list = productCategoryRepository.listByShop(token.getCompanyId(), shopId, "{priority:1,name:1}");
        ListResult<ProductCategory> values = new ListResult<>(Lists.newArrayList(list));

        values.getValues().sort(new Comparator<ProductCategory>() {
            @Override
            public int compare(ProductCategory o1, ProductCategory o2) {
                Integer p0 = o1.getPriority();
                Integer p1 = o2.getPriority();
                if (o1.getPriority() == o2.getPriority()) {
                    return o1.getName().compareTo(o2.getName());
                }
                return p0.compareTo(p1);
            }
        });

        return values;
    }

    @Override
    public SearchResult<ProductCategory> getProductCategories(long afterDate, long beforeDate) {
        if (afterDate < 0) afterDate = 0;
        if (beforeDate <= 0) beforeDate = DateTime.now().getMillis();
        return productCategoryRepository.findItemsWithDate(token.getCompanyId(), token.getShopId(), afterDate, beforeDate);
    }

    @Override
    public ProductCategory createProductCategory(ProductCategoryAddRequest request) {
        String name = request.getName().trim();
        // make sure there's only one
        ProductCategory other = productCategoryRepository.getCategory(token.getCompanyId(), token.getShopId(), name);
        if (other != null) {
            throw new BlazeInvalidArgException("Category", "Another category with this same name exists.");
        }

        ProductCategory productCategory = new ProductCategory();
        productCategory.setName(name);
        productCategory.setCannabis(request.isCannabis());
        productCategory.setCompanyId(token.getCompanyId());
        productCategory.setShopId(token.getShopId());
        productCategory.setUnitType(request.getUnitType());
        productCategory.setActive(request.isActive());
        productCategory.setPriority(request.getPriority());
        productCategory.setLowThreshold(request.getLowThreshold());
        productCategory.setWmCategory(request.getWmCategory());

        if (token.getGrowAppType().equals(ConnectAuthToken.GrowAppType.Operations)) {
            if (StringUtils.isBlank(request.getExternalId())) {
                throw new BlazeInvalidArgException("ExternalId", "External Id is missing");
            }
            productCategory.setExternalId(request.getExternalId());
        }

        productCategoryRepository.save(productCategory);
        realtimeService.sendRealTimeEvent(token.getShopId().toString(),
                RealtimeService.RealtimeEventType.InventoryUpdateEvent, null);

        return productCategory;
    }

    @Override
    public ProductCategory updateProductCategory(String productCategoryId, ProductCategory category) {

        ProductCategory dbCategory = productCategoryRepository.get(token.getCompanyId(), productCategoryId);
        if (dbCategory == null) {
            throw new BlazeInvalidArgException("Category", "Category does not exist.");
        }

        String name = category.getName().trim();

        ProductCategory other = productCategoryRepository.getCategory(token.getCompanyId(), token.getShopId(), name);
        if (other != null && !other.getId().equalsIgnoreCase(dbCategory.getId())) {
            // Another category with this name exists
            throw new BlazeInvalidArgException("Category", "Another category with this same name exists.");
        }

        boolean stateChange = (dbCategory.isActive() != category.isActive());
        List<ObjectId> productIds = null;
        List<ObjectId> memberGroupPriceIds = null;
        if (!dbCategory.getUnitType().equals(category.getUnitType())) {
            productIds = new ArrayList<>();
            List<String> allProducts = new ArrayList<>();
            Iterable<Product> productsForCategory = productRepository.getProductsForCategory(token.getCompanyId(), productCategoryId);

            for (Product product : productsForCategory) {
                allProducts.add(product.getId());
                if (StringUtils.isNotBlank(product.getPricingTemplateId())) {
                    productIds.add(new ObjectId(product.getId()));
                }
            }

            if (allProducts.size() > 0) {
                memberGroupPriceIds = new ArrayList<>();
                Iterable<MemberGroupPrices> memberGroupPrices = memberGroupPricesRepository.getPricesForProducts(token.getCompanyId(), token.getShopId(), allProducts);

                for (MemberGroupPrices memberGroupPrice : memberGroupPrices) {
                    if (StringUtils.isNotBlank(memberGroupPrice.getPricingTemplateId())) {
                        memberGroupPriceIds.add(new ObjectId(memberGroupPrice.getId()));
                    }
                }
            }
        }

        dbCategory.setName(name);
        dbCategory.setUnitType(category.getUnitType());
        dbCategory.setCannabis(category.isCannabis());
        dbCategory.setPhoto(category.getPhoto());
        dbCategory.setActive(category.isActive());
        dbCategory.setPriority(category.getPriority());
        dbCategory.setLowThreshold(category.getLowThreshold());
        dbCategory.setCannabisType(category.getCannabisType());
        dbCategory.setComplianceId(category.getComplianceId());
        dbCategory.setWmCategory(category.getWmCategory());


        if (productIds != null && productIds.size() > 0) {
            productRepository.resetPricingTemplateByProductCategory(token.getCompanyId(), token.getShopId(), productCategoryId, productIds);
        }

        if (memberGroupPriceIds != null && memberGroupPriceIds.size() > 0) {
            memberGroupPricesRepository.updatePricingTemplate(token.getCompanyId(), token.getShopId(), memberGroupPriceIds, null);
        }

        if (token.getGrowAppType().equals(ConnectAuthToken.GrowAppType.Operations)) {
            if (StringUtils.isBlank(category.getExternalId())) {
                throw new BlazeInvalidArgException("ExternalId", "External Id is missing");
            }
            dbCategory.setExternalId(category.getExternalId());
        }
        productCategoryRepository.update(token.getCompanyId(), productCategoryId, dbCategory);
        productRepository.updateCachedCategory(token.getCompanyId(), token.getShopId(), dbCategory.getId(), dbCategory);
        realtimeService.sendRealTimeEvent(token.getShopId().toString(),
                RealtimeService.RealtimeEventType.InventoryUpdateEvent, null);
        if (category.isActive() == false) {
            // only delete if it's inactive
            weedmapService.removeProductFromWeedMap(Lists.newArrayList(dbCategory.getId()), dbCategory.isActive(), new ArrayList<>());
        }
        return dbCategory;
    }

    @Override
    public ListResult<ProductCategory> updateCategoryOrders(ProductCategoryUpdateOrderRequest request) {
        HashMap<String, ProductCategory> categoryHashMap = productCategoryRepository.listAsMap(token.getCompanyId(), token.getShopId());
        for (ProductCategoryOrderItem orderItem : request.getCategoryOrders()) {
            ProductCategory category = categoryHashMap.get(orderItem.getCategoryId());
            if (category != null) {
                category.setPriority(orderItem.getPriority());
                productCategoryRepository.update(token.getCompanyId(), category.getId(), category);
            }
        }
        ListResult<ProductCategory> categoryListResult = new ListResult<>();
        List<ProductCategory> categoryList = Lists.newArrayList(categoryHashMap.values());
        categoryList.sort(new Comparator<ProductCategory>() {
            @Override
            public int compare(ProductCategory o1, ProductCategory o2) {
                Integer p0 = o1.getPriority();
                Integer p1 = o2.getPriority();
                if (o1.getPriority() == o2.getPriority()) {
                    return o1.getName().compareTo(o2.getName());
                }
                return p0.compareTo(p1);
            }
        });
        categoryListResult.setValues(categoryList);
        return categoryListResult;
    }

    @Override
    public void deleteProductCategory(String productCategoryId) {
        // Make sure no products are using this category
        long productCount = productRepository.countProductsForCategory(token.getCompanyId(), productCategoryId);
        if (productCount > 0) {
            throw new BlazeInvalidArgException("ProductCategory.Delete", "There are currently products assigned to this category.");
        }
        productCategoryRepository.removeByIdSetState(token.getCompanyId(), productCategoryId);

        // For auto sync of products at weedmap of given product category id
        weedmapService.removeProductFromWeedMap(Lists.newArrayList(productCategoryId), false, new ArrayList<>());
    }

    @Override
    public ProductCategory getProductCategoryById(String productCategoryId) {
        ProductCategory dbCategory = productCategoryRepository.get(token.getCompanyId(), productCategoryId);
        if (dbCategory == null) {
            throw new BlazeInvalidArgException("Category", "Category does not exist.");
        }
        return dbCategory;
    }
}

