package com.fourtwenty.core.services.mgmt.impl;

import com.amazonaws.util.CollectionUtils;
import com.esotericsoftware.kryo.Kryo;
import com.fourtwenty.core.domain.models.company.*;
import com.fourtwenty.core.domain.models.customer.MedicalCondition;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.models.generic.Note;
import com.fourtwenty.core.domain.models.partner.PartnerWebHook;
import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.models.purchaseorder.POProductRequest;
import com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrder;
import com.fourtwenty.core.domain.models.thirdparty.WmProductMapping;
import com.fourtwenty.core.domain.models.thirdparty.WmTagGroups;
import com.fourtwenty.core.domain.models.transaction.*;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.domain.repositories.thirdparty.WmMappingRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.managed.BackgroundTaskManager;
import com.fourtwenty.core.managed.ElasticSearchManager;
import com.fourtwenty.core.managed.PartnerWebHookManager;
import com.fourtwenty.core.rest.dispensary.requests.inventory.*;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.*;
import com.fourtwenty.core.rest.store.webhooks.ProductData;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.common.AuditLogService;
import com.fourtwenty.core.services.common.BackgroundJobService;
import com.fourtwenty.core.services.common.RealtimeService;
import com.fourtwenty.core.services.mgmt.*;
import com.fourtwenty.core.services.thirdparty.WeedmapService;
import com.fourtwenty.core.tasks.MigrateProductsToPublicTask;
import com.fourtwenty.core.thirdparty.weedmap.models.ThirdPartyProduct;
import com.google.common.collect.Lists;
import com.google.inject.Provider;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.*;
import java.util.function.Predicate;

/**
 * Created by Stephen Schmidt on 10/6/2015.
 */
public class ProductServiceImpl extends AbstractAuthServiceImpl implements ProductService {

    private static final Logger LOG = LoggerFactory.getLogger(ProductServiceImpl.class);

    private static final String BULK_PRODUCT_UPDATE = "Bulk Product Update";
    private static final int PRODUCT_RESULT_LIMIT = 10000;
    private static final String PRODUCT = "Product";
    private static final String VENDOR = "Vendor";
    private static final String VENDOR_NOT_FOUND = "Vendor is not found";
    private static final String BRAND_NOT_FOUND = "Brand is not found";
    private static final String PRODUCT_NAME_EXISTS = "Another Product with this same name exists";
    private static final String PRICING_TEMPLATE_NOT_FOUND = "Pricing template not found or inactive";
    private static final String NOT_VALID_BUNDLE_PRODUCT = "Please select valid product for creating bundle product";
    private static final String BUNDLE_PRODUCT_USE = "Bundle type product can't be used in creation of bundle product";
    private static final String CAN_NOT_DELETE_QUANTITY_AVAILABLE = "Cannot delete products with available inventory. You can clear inventory by reporting a loss.";
    private static final String SECONDARY_VENDOR_NOT_FOUND = "Secondary vendor does not exists.";
    private static final String TOLERANCE_NOT_FOUND = "Product weight tolerance does not exists.";

    @Inject
    ProductRepository productRepository;
    @Inject
    MedicalConditionRepository medicalConditionRepository;
    @Inject
    CompanyAssetRepository companyAssetRepository;
    @Inject
    VendorRepository vendorRepository;
    @Inject
    BrandRepository brandRepository;
    @Inject
    ProductCategoryRepository categoryRepository;
    @Inject
    ProductWeightToleranceRepository productWeightToleranceRepository;
    @Inject
    ProductCategoryRepository productCategoryRepository;
    @Inject
    RealtimeService realtimeService;
    @Inject
    BarcodeService barcodeService;
    @Inject
    InventoryService inventoryService;
    @Inject
    ProductBatchRepository batchRepository;
    @Inject
    QueuedTransactionRepository queuedTransactionRepository;
    @Inject
    InventoryRepository inventoryRepository;
    @Inject
    ShopRepository shopRepository;
    @Inject
    PrepackageRepository prepackageRepository;
    @Inject
    MemberGroupPricesRepository groupPricesRepository;
    @Inject
    MemberGroupRepository memberGroupRepository;
    @Inject
    MemberRepository memberRepository;
    @Inject
    MigrateProductsToPublicTask migrateProductsToPublicTask;
    @Inject
    TerminalRepository terminalRepository;
    @javax.inject.Inject
    BackgroundTaskManager backgroundTaskManager;
    @Inject
    ProductPrepackageQuantityRepository productPrepackageQuantityRepository;
    @Inject
    ProductWeightToleranceRepository weightToleranceRepository;
    @Inject
    private ElasticSearchManager elasticSearchManager;
    @Inject
    private InventoryTransferHistoryRepository inventoryTransferHistoryRepository;
    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private PrepackageProductItemRepository prepackageProductItemRepository;
    @Inject
    private PurchaseOrderRepository purchaseOrderRepository;
    @Inject
    private PartnerWebHookManager partnerWebHookManager;
    @Inject
    private ProductService productService;
    @Inject
    private InventoryActionRepository inventoryActionRepository;
    @Inject
    private BackgroundJobService backgroundJobService;
    @Inject
    private PricingTemplateRepository pricingTemplateRepository;
    @Inject
    AuditLogService auditLogService;
    @Inject
    private WeedmapService weedmapService;
    @Inject
    private BatchQuantityRepository batchQuantityRepository;
    @Inject
    private WmMappingRepository wmMappingRepository;
    @Inject
    private VendorService vendorService;
    @Inject
    private ProductVersionRepository productVersionRepository;
    @Inject
    private StorewideSaleService storewideSaleService;

    @Inject
    public ProductServiceImpl(Provider<ConnectAuthToken> token) {
        super(token);
    }

    @Override
    public Product addProduct(ProductAddRequest addRequest) {

        Vendor vendor = null;
        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());
        if (shop == null) {
            throw new BlazeInvalidArgException("Shop", "Shop does not found");
        }
        if (shop.getAppTarget() == CompanyFeatures.AppTarget.Grow && StringUtils.isBlank(addRequest.getVendorId())) {
            vendor = vendorService.getDefaultVendor(token.getCompanyId());
            addRequest.setVendorId(vendor.getId());
        } else if ((Product.ProductType.REGULAR == addRequest.getProductType()) ||
                (Product.ProductType.DERIVED == addRequest.getProductType())) {
            // Check if vendor exist
            vendor = vendorRepository.get(token.getCompanyId(), addRequest.getVendorId());
            if (vendor == null) {
                throw new BlazeInvalidArgException("Vendor", "Vendor not exist");
            }
        }

        if (StringUtils.isNotBlank(addRequest.getToleranceId())) {
            ProductWeightTolerance dbWeightTolerance = weightToleranceRepository.get(token.getCompanyId(), addRequest.getToleranceId());
            if (dbWeightTolerance == null) {
                throw new BlazeInvalidArgException(PRODUCT, TOLERANCE_NOT_FOUND);
            }
        }

        if (!CollectionUtils.isNullOrEmpty(addRequest.getSecondaryVendors())) {
            List<ObjectId> vendorObjIds = new ArrayList<>();
            for (String vendorId : addRequest.getSecondaryVendors()) {
                if (StringUtils.isNotBlank(vendorId) && ObjectId.isValid(vendorId)) {
                    vendorObjIds.add(new ObjectId(vendorId));
                }
            }
            HashMap<String, Vendor> secondaryVendorsMap = vendorRepository.listAsMap(token.getCompanyId(), vendorObjIds);
            if (secondaryVendorsMap.isEmpty()) {
                throw new BlazeInvalidArgException(VENDOR, SECONDARY_VENDOR_NOT_FOUND);
            }
            addRequest.setSecondaryVendors(secondaryVendorsMap.keySet());
        }

        long result = 0;
        if (StringUtils.isNotBlank(addRequest.getName())) {
            result = productRepository.getProductsByName(token.getCompanyId(), token.getShopId(), addRequest.getName(), addRequest.getVendorId(), addRequest.getCategoryId(), addRequest.getFlowerType(), addRequest.getCannabisType());
        }
        if (result > 0) {
            throw new BlazeInvalidArgException(PRODUCT, PRODUCT_NAME_EXISTS);
        }

        Product product = new Product();
        product.prepare(token.getCompanyId());
        product.setVendorId(addRequest.getVendorId());
        product.setName(addRequest.getName());
        product.setCategoryId(addRequest.getCategoryId());
        product.setShopId(token.getShopId());
        product.setUnitPrice(addRequest.getUnitPrice());
        product.setSku(addRequest.getSku());
        product.setDescription(addRequest.getDescription());
        product.setPriceRanges(addRequest.getPriceRanges());
        product.setFlowerType(addRequest.getFlowerType());
        product.setGenetics(addRequest.getGenetics());
        product.setEnableMixMatch(addRequest.isEnableMixMatch());
        product.setLowThreshold(addRequest.getLowThreshold());
        product.setLowInventoryNotification(addRequest.isLowInventoryNotification());
        product.setShowInWidget(addRequest.isShowInWidget());
        product.setDiscountable(true);
        product.setMedicinal(addRequest.isMedicinal());
        product.setByGram(addRequest.isByGram());
        product.setByPrepackage(addRequest.isByPrepackage());
        product.setProductSaleType(addRequest.getProductSaleType());
        product.setTags(addRequest.getTags());
        product.setEnableExciseTax(addRequest.isEnableExciseTax());
        product.setCannabisType(addRequest.getCannabisType());
        product.setActive(addRequest.isActive());
        product.setPriceIncludesExcise(addRequest.isPriceIncludesExcise());
        product.setPriceIncludesALExcise(addRequest.isPriceIncludesALExcise());
        product.setAutomaticReOrder(addRequest.isAutomaticReOrder());
        product.setReOrderLevel(addRequest.getReOrderLevel());
        product.setProductType(addRequest.getProductType());
        product.setSecondaryVendors(addRequest.getSecondaryVendors());
        product.setWeightPerUnit(addRequest.getWeightPerUnit() == null ? Product.WeightPerUnit.EACH : addRequest.getWeightPerUnit());
        product.setToleranceId(addRequest.getToleranceId());

        if (token.getGrowAppType().equals(ConnectAuthToken.GrowAppType.Operations)) {
            if (StringUtils.isBlank(addRequest.getExternalId())) {
                throw new BlazeInvalidArgException("ExternalId", "External Id is missing");
            }
            product.setExternalId(addRequest.getExternalId());
        }
        product.setProducerMfg(addRequest.getProducerMfg());
        product.setProducerLicense(addRequest.getProducerLicense());
        product.setProducerAddress(addRequest.getProducerAddress());
        if (product.getProducerAddress() != null) {
            product.getProducerAddress().prepare(token.getCompanyId());
        }
        product.setSecondaryVendors(addRequest.getSecondaryVendors());

        if (vendor != null) {
            if (Vendor.ArmsLengthType.ARMS_LENGTH == vendor.getArmsLengthType()) {
                product.setPriceIncludesALExcise(shop.isProductPriceIncludeExciseTax());
                product.setPriceIncludesExcise(Boolean.FALSE);
            } else if (Vendor.ArmsLengthType.NON_ARMS_LENGTH == vendor.getArmsLengthType()) {
                product.setPriceIncludesExcise(shop.isProductPriceIncludeExciseTax());
                product.setPriceIncludesALExcise(Boolean.FALSE);
            }
        }

        if (StringUtils.isNotBlank(addRequest.getBrandId())) {

            if (vendor.getBrands().contains(addRequest.getBrandId())) {
                product.setBrandId(addRequest.getBrandId());
            } else {
                throw new BlazeInvalidArgException("Brand", BRAND_NOT_FOUND);
            }

        }
        if (StringUtils.isNotBlank(addRequest.getSku())) {
            Product productBySku = productRepository.findProductBySku(token.getCompanyId(), token.getShopId(), addRequest.getSku());
            if (productBySku != null) {
                throw new BlazeInvalidArgException("Barcode",
                        String.format("SKU '%s' is currently used by another product .", addRequest.getSku()));
            }
        }
        //link the id of the vendor in our DB to this product
        ProductCategory category = productCategoryRepository.get(token.getCompanyId(), product.getCategoryId());
        if ((Product.ProductType.REGULAR == addRequest.getProductType() && category == null) ||
                (Product.ProductType.DERIVED == addRequest.getProductType() && StringUtils.isNotBlank(product.getCategoryId()) && category == null)) {
            throw new BlazeInvalidArgException("Product", "Product Category does not exists");
        }
        product.setCategory(category);

        BarcodeItem item = barcodeService.createBarcodeItemIfNeeded(product.getCompanyId(), product.getShopId(), product.getId(),
                BarcodeItem.BarcodeEntityType.Product, product.getId(), product.getSku(), null, false);
        if (item != null) {
            product.setSku(item.getBarcode());
        }

        product.setBundleItems(addRequest.getBundleItems());
        this.validateBundleProduct(product);

        if (category != null) {
            if (category.getUnitType() == ProductCategory.UnitType.grams) {
                List<ProductWeightTolerance> tolerances = this.getWeightTolerances();
                // Recreate the tolerance
                List<ProductPriceRange> productPriceRanges = new ArrayList<>();
                for (ProductWeightTolerance tolerance : tolerances) {
                    if (tolerance.isEnabled()) {
                        ProductPriceRange priceRange = new ProductPriceRange();
                        productPriceRanges.add(priceRange);

                        priceRange.setWeightToleranceId(tolerance.getId());
                        priceRange.setWeightTolerance(tolerance);
                        priceRange.setPriority(tolerance.getPriority());
                        priceRange.setId(product.getId() + "_" + priceRange.getWeightToleranceId());
                    }
                }


                int priority = 0;
                for (ProductPriceRange productPriceRange : product.getPriceRanges()) {
                    for (ProductPriceRange newPriceRange : productPriceRanges) {
                        if (newPriceRange.getWeightToleranceId().equalsIgnoreCase(productPriceRange.getWeightToleranceId())) {
                            newPriceRange.setPrice(productPriceRange.getPrice());
                            break;
                        }
                    }
                    priority++;
                }

                product.setPriceRanges(productPriceRanges);
                product.setPriceBreaks(new ArrayList<ProductPriceBreak>());
            } else {
                // This is the unit category, use price breaks instead of price ranges
                if (product.getPriceBreaks() != null) {
                    for (ProductPriceBreak priceBreak : product.getPriceBreaks()) {
                        priceBreak.resetPrepare(token.getCompanyId());
                        if (product.getWeightPerUnit() == Product.WeightPerUnit.EACH
                                || product.getWeightPerUnit() == Product.WeightPerUnit.FULL_GRAM
                                || product.getWeightPerUnit() == Product.WeightPerUnit.EIGHTH
                                || product.getWeightPerUnit() == Product.WeightPerUnit.FOURTH) {
                            if (priceBreak.getPriceBreakType() == ProductPriceBreak.PriceBreakType.OneGramUnit) {
                                priceBreak.setActive(true);
                            }
                        } else {
                            if (priceBreak.getPriceBreakType() == ProductPriceBreak.PriceBreakType.HalfGramUnit) {
                                priceBreak.setActive(true);
                            }
                        }
                    }
                }

                populatePriceBreaks(product, category, product.getPriceBreaks(), true, null, false, Boolean.FALSE);

                product.setPriceBreaks(product.getPriceBreaks());
                product.setPriceRanges(new ArrayList<ProductPriceRange>());
            }
        }

        // Get the asset
        for (CompanyAsset asset : addRequest.getAssets()) {
            asset.prepare(token.getCompanyId());
        }

        product.setAssets(addRequest.getAssets());


        //add the new product
        product.setMedicalConditions(updateMedicalConditions(addRequest.getMedicalConditions()));

        product.setCustomWeight(addRequest.getCustomWeight());
        product.setCustomGramType(addRequest.getCustomGramType());
        product.setSellable(addRequest.isSellable());
        product.setWmThreshold(addRequest.getWmThreshold());

        Product dbProduct = productRepository.save(product);

        //update product tags in shop
        LinkedHashSet<String> shopProductsTag = shop.getProductsTag();
        for (String requestTag : addRequest.getTags()) {
            if (!shopProductsTag.contains(requestTag)) {
                shopProductsTag.add(requestTag);
            }
        }
        shop.setProductsTag(shopProductsTag);
        shopRepository.update(token.getCompanyId(), token.getShopId(), shop);

        dbProduct.setVendor(vendor);

        //Save the updated product in product versions collection
        ProductVersion productVersion = new ProductVersion();
        productVersion.setProduct(dbProduct);
        productVersionRepository.save(productVersion);

        //Apply active storewide sale discount
        dbProduct = storewideSaleService.applyActiveStorewideSaleForProduct(dbProduct);

        elasticSearchManager.createOrUpdateIndexedDocument(new ProductCustomResult(product));
        realtimeService.sendRealTimeEvent(token.getShopId().toString(),
                RealtimeService.RealtimeEventType.InventoryUpdateEvent, null);
        callWebHook(dbProduct, true);
        return dbProduct;
    }

    private void validateBundleProduct(Product product) {
        if (Product.ProductType.BUNDLE == product.getProductType()) {
            if (product.getBundleItems() != null && product.getBundleItems().size() > 0) {
                List<ObjectId> productIds = new ArrayList<>();
                for (BundleItem bundleItem : product.getBundleItems()) {
                    if (StringUtils.isNotBlank(bundleItem.getProductId()) && ObjectId.isValid(bundleItem.getProductId())) {
                        productIds.add(new ObjectId(bundleItem.getProductId()));
                    } else {
                        throw new BlazeInvalidArgException(PRODUCT, NOT_VALID_BUNDLE_PRODUCT);
                    }
                }

                HashMap<String, Product> productMap = productRepository.listAsMap(token.getCompanyId(), productIds);

                for (BundleItem bundleItem : product.getBundleItems()) {
                    Product parentProduct = productMap.get(bundleItem.getProductId());
                    if (parentProduct == null) {
                        throw new BlazeInvalidArgException(PRODUCT, NOT_VALID_BUNDLE_PRODUCT);
                    }

                }
            }
        }
    }

    @Override
    public Product sanitize(ProductAddRequest importRequest, HashMap<String, Vendor> vendorHashMap) throws BlazeInvalidArgException {
        if (importRequest == null
                || StringUtils.isBlank(importRequest.getName())
                || importRequest.getVendorImportId() == null) {
            throw new BlazeInvalidArgException("ProductAddRequest", "Invalid arguments, importId: " +
                    importRequest.getImportId() + " - missing name, flowertype, Category name or vendorID or Product Brand");
        }
        Vendor v = null;
        if (vendorHashMap != null) {
            v = vendorHashMap.get(importRequest.getVendorImportId());
        }
        Product product = new Product();
        product.setName(importRequest.getName());
        product.setShopId(token.getShopId());
        product.setCompanyId(token.getCompanyId());
        product.setVendorId(v != null ? v.getId() : null);
        product.setUnitPrice(importRequest.getUnitPrice());
        product.setDescription(importRequest.getDescription());
        product.setPriceRanges(importRequest.getPriceRanges());
        product.setFlowerType(importRequest.getFlowerType());
        product.setGenetics(importRequest.getGenetics());
        product.setImportId(product.getName() + product.getFlowerType() + product.getVendorId());
        product.setSku(importRequest.getSku());
        product.setCannabisType(importRequest.getCannabisType());
        if (importRequest.getBrandId() != null) {
            product.setBrandId(importRequest.getBrandId());
        }

        if (StringUtils.isBlank(product.getVendorId()) && v != null) {
            product.setVendorId(v.getId());
        }

        // generate sku if missing one
        if (StringUtils.isBlank(product.getSku())) {
            String uuid = UUID.randomUUID().toString();
            product.setSku((uuid.substring(uuid.length() - 6, uuid.length()) + RandomStringUtils.randomAlphanumeric(8)).toUpperCase());
        }

        if (v != null) {
            product.setVendorId(v.getId());
        }

        return product;

    }

    @Override
    public Product updateProduct(String productId, Product product) {

        Product dbProduct = productRepository.get(token.getCompanyId(), productId);
        if (dbProduct == null) {
            throw new BlazeInvalidArgException("Product", "Product does not exist with request id");
        }

        ProductCategory category = productCategoryRepository.get(token.getCompanyId(), product.getCategoryId());
        if ((Product.ProductType.REGULAR == product.getProductType() && category == null) ||
                (Product.ProductType.DERIVED == product.getProductType() && StringUtils.isNotBlank(product.getCategoryId()) && category == null)) {
            throw new BlazeInvalidArgException("Category", "Category does not exist.");
        }
        Vendor vendor = null;
        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());
        if (shop.getAppTarget() == CompanyFeatures.AppTarget.Grow && StringUtils.isBlank(product.getVendorId())) {
            vendor = vendorService.getDefaultVendor(token.getCompanyId());
            product.setVendorId(vendor.getId());
        } else if ((Product.ProductType.REGULAR == product.getProductType()) ||
                (Product.ProductType.DERIVED == product.getProductType())) {
            // Check if vendor exist
            vendor = vendorRepository.get(token.getCompanyId(), product.getVendorId());
            if (vendor == null) {
                throw new BlazeInvalidArgException("Vendor", "Vendor does not exist");
            }

        }

        if (StringUtils.isNotBlank(product.getToleranceId())) {
            ProductWeightTolerance dbWeightTolerance = weightToleranceRepository.get(token.getCompanyId(), product.getToleranceId());
            if (dbWeightTolerance == null) {
                throw new BlazeInvalidArgException(PRODUCT, TOLERANCE_NOT_FOUND);
            }
        }

        if (product.getNotes() != null) {
            for (Note note : product.getNotes()) {
                if (note.getId() == null) {
                    note.setId(ObjectId.get().toString());
                    note.setWriterId(token.getActiveTopUser().getUserId());
                    note.setWriterName(token.getActiveTopUser().getFirstName() + " " + token.getActiveTopUser().getLastName());
                }
            }
        }
        Product.WeightPerUnit oldWeight = dbProduct.getWeightPerUnit();
        if (StringUtils.isNotBlank(dbProduct.getVendorId())
                && StringUtils.isBlank(product.getVendorId())) {
            throw new BlazeInvalidArgException("Product", "VendorId should not be empty.");
        }
        boolean isCategoryUpdate = (!dbProduct.getCategoryId().equals(product.getCategoryId()));
        String oldCategory = dbProduct.getCategoryId();
        List<String> productIds = new ArrayList<>();
        productIds.add(productId);
        if (!product.isActive()) {
            SearchResult<Transaction> transactionByProduct = transactionRepository.getTransactionByProduct(token.getCompanyId(), token.getShopId(), "modified:-1", productIds);
            if (transactionByProduct.getValues().size() > 0) {
                String noDetails = "";
                StringBuilder transactionString = new StringBuilder();
                transactionString.append(noDetails);
                for (Transaction transaction : transactionByProduct.getValues()) {
                    transactionString.append(" " + transaction.getTransNo() + ",");
                }

                String transactionSubstring = transactionString.substring(0, transactionString.length() - 1);
                throw new BlazeInvalidArgException("Transaction", "This product is being used in transaction (" + transactionSubstring + "). So can not deactivate it.");
            }
        }

        long result = 0;
        if (StringUtils.isNotBlank(product.getName()) && !product.getName().equalsIgnoreCase(dbProduct.getName())) {
            result = productRepository.getProductsByName(token.getCompanyId(), token.getShopId(), product.getName(), product.getVendorId(), product.getCategoryId(), product.getFlowerType(), product.getCannabisType());
        }
        if (result > 0) {
            throw new BlazeInvalidArgException(PRODUCT, PRODUCT_NAME_EXISTS);
        }

        PricingTemplate template = pricingTemplateRepository.get(token.getCompanyId(), product.getPricingTemplateId());
        PricingTemplate dbTemplate = pricingTemplateRepository.get(token.getCompanyId(), dbProduct.getPricingTemplateId());


        if (!dbProduct.getWeightPerUnit().equals(product.getWeightPerUnit()) || (dbTemplate != null && !category.getUnitType().equals(dbTemplate.getUnitType()))
                || dbProduct.getUnitPrice().doubleValue() != product.getUnitPrice().doubleValue()) {

            dbProduct.setPricingTemplateId(StringUtils.EMPTY);

            Iterable<MemberGroupPrices> memberGroupPrices = groupPricesRepository.getPricesForProduct(token.getCompanyId(), token.getShopId(), productId, MemberGroupPrices.class);

            List<ObjectId> groupPriceId = new ArrayList<>();
            for (MemberGroupPrices memberGroupPrice : memberGroupPrices) {
                groupPriceId.add(new ObjectId(memberGroupPrice.getId()));
            }

            groupPricesRepository.updatePricingTemplate(token.getCompanyId(), token.getShopId(), groupPriceId, null);

            if (template != null && ((category.getUnitType().equals(ProductCategory.UnitType.units) && template.getWeightPerUnit().equals(product.getWeightPerUnit()))
                    || ((category.getUnitType().equals(ProductCategory.UnitType.grams) && StringUtils.isNotBlank(product.getPricingTemplateId()))))) {
                dbProduct.setPricingTemplateId(product.getPricingTemplateId());
            }

        } else {
            dbProduct.setPricingTemplateId(product.getPricingTemplateId());
        }

        boolean shouldReset = false;
        if (product.getWeightPerUnit() == Product.WeightPerUnit.HALF_GRAM) {
            shouldReset = product.getWeightPerUnit() != dbProduct.getWeightPerUnit();
        }

        dbProduct.setActive(product.isActive());
        dbProduct.setCategoryId(product.getCategoryId());
        dbProduct.setCategory(category);
        dbProduct.setName(product.getName());
        dbProduct.setAssets(product.getAssets());
        dbProduct.setDescription(product.getDescription());
        dbProduct.setFlowerType(product.getFlowerType());
        dbProduct.setGenetics(product.getGenetics());
        dbProduct.setMedicalConditions(product.getMedicalConditions());
        dbProduct.setNotes(product.getNotes());
        dbProduct.setUnitPrice(product.getUnitPrice());
        dbProduct.setVendorId(product.getVendorId());
        dbProduct.setEnableMixMatch(product.isEnableMixMatch());
        dbProduct.setEnableWeedmap(product.isEnableWeedmap());
        dbProduct.setWeightPerUnit(product.getWeightPerUnit());
        dbProduct.setTaxOrder(product.getTaxOrder());
        dbProduct.setTaxType(product.getTaxType());
        dbProduct.setCustomTaxInfo(product.getCustomTaxInfo());
        dbProduct.setLowThreshold(product.getLowThreshold());
        dbProduct.setLowInventoryNotification(product.isLowInventoryNotification());
        dbProduct.setShowInWidget(product.isShowInWidget());
        dbProduct.setDiscountable(product.isDiscountable());
        dbProduct.setThc(product.getThc());
        dbProduct.setCbd(product.getCbd());
        dbProduct.setCbda(product.getCbda());
        dbProduct.setCbn(product.getCbn());
        dbProduct.setThca(product.getThca());
        dbProduct.setMedicinal(product.isMedicinal());
        dbProduct.setByGram(product.isByGram());
        dbProduct.setByPrepackage(product.isByPrepackage());
        dbProduct.setProductSaleType(product.getProductSaleType());
        dbProduct.setTags(product.getTags());
        dbProduct.setEnableExciseTax(product.isEnableExciseTax());
        dbProduct.setAutomaticReOrder(product.isAutomaticReOrder());
        dbProduct.setReOrderLevel(product.getReOrderLevel());
        dbProduct.setProductType(product.getProductType());
        dbProduct.setWeightPerUnit(product.getWeightPerUnit() == null ? dbProduct.getWeightPerUnit() : product.getWeightPerUnit());
        dbProduct.setToleranceId(product.getToleranceId());

        if (token.getGrowAppType().equals(ConnectAuthToken.GrowAppType.Operations)) {
            if (StringUtils.isBlank(product.getExternalId())) {
                throw new BlazeInvalidArgException("ExternalId", "External Id is missing");
            }
            dbProduct.setExternalId(product.getExternalId());
        }
        dbProduct.setProducerMfg(product.getProducerMfg());
        dbProduct.setProducerLicense(product.getProducerLicense());
        dbProduct.setProducerAddress(product.getProducerAddress());
        if (dbProduct.getProducerAddress() != null) {
            dbProduct.getProducerAddress().prepare(token.getCompanyId());
        } else {
            dbProduct.setProducerAddress(new Address());
        }

        vendor = vendorRepository.get(token.getCompanyId(), product.getVendorId());
        if ((Product.ProductType.REGULAR == product.getProductType() && vendor == null) ||
                (Product.ProductType.DERIVED == product.getProductType() && StringUtils.isNotBlank(product.getVendorId()) && vendor == null)) {
            throw new BlazeInvalidArgException(VENDOR, VENDOR_NOT_FOUND);
        }

        if (!CollectionUtils.isNullOrEmpty(product.getSecondaryVendors())) {
            List<ObjectId> vendorObjIds = new ArrayList<>();
            for (String vendorId : product.getSecondaryVendors()) {
                if (StringUtils.isNotBlank(vendorId) && ObjectId.isValid(vendorId)) {
                    vendorObjIds.add(new ObjectId(vendorId));
                }
            }
            HashMap<String, Vendor> secondaryVendorsMap = vendorRepository.listAsMap(token.getCompanyId(), vendorObjIds);
            if (secondaryVendorsMap.isEmpty()) {
                throw new BlazeInvalidArgException(VENDOR, SECONDARY_VENDOR_NOT_FOUND);
            }
            dbProduct.setSecondaryVendors(secondaryVendorsMap.keySet());
        } else {
            dbProduct.setSecondaryVendors(Collections.EMPTY_SET);
        }

        if (StringUtils.isNotBlank(product.getBrandId())) {
            if (vendor.getBrands().contains(product.getBrandId())) {
                dbProduct.setBrandId(product.getBrandId());
            } else {
                dbProduct.setBrandId("");
            }
        } else {
            dbProduct.setBrandId("");
        }

        dbProduct.setCannabisType(product.getCannabisType());
        dbProduct.setPriceIncludesExcise(product.isPriceIncludesExcise());
        dbProduct.setPotency(product.isPotency());
        dbProduct.setPriceIncludesALExcise(product.isPriceIncludesALExcise());

        if (product.getPotencyAmount() != null) {
            dbProduct.setPotencyAmount(product.getPotencyAmount());
        } else {
            //Null is not being set in database.
            PotencyMG potencyMG = new PotencyMG();
            dbProduct.setPotencyAmount(potencyMG);
        }

        if (dbProduct.getCustomTaxInfo() != null) {
            dbProduct.getCustomTaxInfo().prepare();
        }

        dbProduct.setBundleItems(product.getBundleItems());
        this.validateBundleProduct(dbProduct);

        dbProduct.setTaxTables(product.getTaxTables());
        if (dbProduct.getTaxTables() != null) {
            for (CompoundTaxTable taxTable : dbProduct.getTaxTables()) {
                taxTable.prepare(token.getCompanyId());
                taxTable.setShopId(token.getShopId());
                if (taxTable.getTaxType() != TaxInfo.TaxType.Exempt) {
                    taxTable.setTaxType(TaxInfo.TaxType.Custom);


                    if (taxTable.getCityTax() != null) {
                        taxTable.getCityTax().prepare(token.getCompanyId());
                        taxTable.getCityTax().setShopId(token.getShopId());
                    }

                    if (taxTable.getStateTax() != null) {
                        taxTable.getCountyTax().prepare(token.getCompanyId());
                        taxTable.getCountyTax().setShopId(token.getShopId());
                    }

                    if (taxTable.getCountyTax() != null) {
                        taxTable.getStateTax().prepare(token.getCompanyId());
                        taxTable.getStateTax().setShopId(token.getShopId());
                    }
                    if (taxTable.getFederalTax() != null) {
                        taxTable.getFederalTax().prepare(token.getCompanyId());
                        taxTable.getFederalTax().setShopId(token.getShopId());
                    }

                    taxTable.reset();
                }
            }
        }

        if (category != null) {
            if (StringUtils.isNotBlank(dbProduct.getPricingTemplateId())) {
                if (template == null) {
                    template = pricingTemplateRepository.get(token.getCompanyId(), dbProduct.getPricingTemplateId());
                }
                if (template == null) {
                    throw new BlazeInvalidArgException("Pricing template", "Pricing template not found");
                }
                if (!template.getUnitType().equals(category.getUnitType())) {
                    throw new BlazeInvalidArgException("Product", "Product category's unit type does not match with pricing template");
                }
                if (template.getUnitType().equals(ProductCategory.UnitType.units) && !template.getWeightPerUnit().equals(product.getWeightPerUnit())) {
                    throw new BlazeInvalidArgException("Pricing template", "Product's weight per unit type does not match with pricing template");
                }
                this.assignPricingTemplateToProduct(dbProduct, template, category);
            } else {
                //Update pricing
                if (category.getUnitType() == ProductCategory.UnitType.grams) {
                    List<ProductWeightTolerance> tolerances = this.getWeightTolerances();
                    // Recreate the tolerance
                    List<ProductPriceRange> productPriceRanges = new ArrayList<>();
                    for (ProductWeightTolerance tolerance : tolerances) {
                        if (tolerance.isEnabled()) {
                            ProductPriceRange priceRange = new ProductPriceRange();
                            productPriceRanges.add(priceRange);

                            priceRange.setWeightToleranceId(tolerance.getId());
                            priceRange.setWeightTolerance(tolerance);
                            priceRange.setPriority(tolerance.getPriority());
                            priceRange.setId(product.getId() + "_" + priceRange.getWeightToleranceId());
                        }
                    }


                    int priority = 0;
                    for (ProductPriceRange productPriceRange : product.getPriceRanges()) {
                        for (ProductPriceRange newPriceRange : productPriceRanges) {
                            if (newPriceRange.getWeightToleranceId().equalsIgnoreCase(productPriceRange.getWeightToleranceId())) {
                                newPriceRange.setPrice(productPriceRange.getPrice());
                                break;
                            }
                        }
                        priority++;
                    }

                    dbProduct.setPriceRanges(productPriceRanges);
                    dbProduct.setPriceBreaks(new ArrayList<ProductPriceBreak>());
                } else {
                    // This is the unit category, use price breaks instead of price ranges
                    if (product.getPriceBreaks() != null) {
                        for (ProductPriceBreak priceBreak : product.getPriceBreaks()) {
                            priceBreak.prepare(token.getCompanyId());
                            if (product.getWeightPerUnit() == Product.WeightPerUnit.EACH
                                    || product.getWeightPerUnit() == Product.WeightPerUnit.FULL_GRAM
                                    || product.getWeightPerUnit() == Product.WeightPerUnit.EIGHTH
                                    || product.getWeightPerUnit() == Product.WeightPerUnit.FOURTH) {
                                if (priceBreak.getPriceBreakType() == ProductPriceBreak.PriceBreakType.OneGramUnit) {
                                    priceBreak.setActive(true);
                                }
                            } else {
                                if (priceBreak.getPriceBreakType() == ProductPriceBreak.PriceBreakType.HalfGramUnit) {
                                    priceBreak.setActive(true);
                                }
                            }
                        }
                    }

                    populatePriceBreaks(dbProduct, category, product.getPriceBreaks(), true, null, shouldReset, false);

                    dbProduct.setPriceBreaks(product.getPriceBreaks());
                    dbProduct.setPriceRanges(new ArrayList<ProductPriceRange>());
                }
            }
        }

        // Update sku if needed
        BarcodeItem item = barcodeService.createBarcodeItemIfNeeded(dbProduct.getCompanyId(), dbProduct.getShopId(), dbProduct.getId(),
                BarcodeItem.BarcodeEntityType.Product, product.getId(),
                product.getSku(), // new sku
                dbProduct.getSku(), false);
        if (item != null) {
            dbProduct.setSku(item.getBarcode());
        }

        dbProduct.setCustomWeight(product.getCustomWeight());
        dbProduct.setCustomGramType(product.getCustomGramType());
        dbProduct.setSellable(product.isSellable());
        dbProduct.setWmThreshold(product.getWmThreshold());
        dbProduct = productRepository.update(token.getCompanyId(), productId, dbProduct);
        dbProduct.setVendor(vendor);

        elasticSearchManager.createOrUpdateIndexedDocument(new ProductCustomResult(dbProduct));

        //update product tags in shop


        LinkedHashSet<String> shopProductsTag = shop.getProductsTag();
        for (String requestTag : product.getTags()) {
            if (!shopProductsTag.contains(requestTag)) {
                shopProductsTag.add(requestTag);
            }
        }
        shop.setProductsTag(shopProductsTag);
        shopRepository.updateProductTags(token.getCompanyId(), token.getShopId(), shopProductsTag);

        //Save the updated product in product versions collection
        ProductVersion productVersion = new ProductVersion();
        productVersion.setProduct(dbProduct);
        productVersionRepository.save(productVersion);

        //Apply active storewide sale discount
        dbProduct = storewideSaleService.applyActiveStorewideSaleForProduct(dbProduct);

        realtimeService.sendRealTimeEvent(token.getShopId().toString(),
                RealtimeService.RealtimeEventType.InventoryUpdateEvent, null);
        callWebHook(dbProduct, false);
        if (!dbProduct.isEnableWeedmap() || !dbProduct.isActive() || dbProduct.isDeleted()) {
            weedmapService.removeProductFromWeedMap(Lists.newArrayList(oldCategory), false, Lists.newArrayList(dbProduct));
        }
        return dbProduct;

    }

    private void assignPricingTemplateToProduct(Product product, PricingTemplate template, ProductCategory category) {

        if (!template.isActive()) {
            throw new BlazeInvalidArgException("Pricing Template", "Pricing template is not active.");
        }

        product.setPriceBreaks(template.getPriceBreaks());
        product.setPriceRanges(template.getPriceRanges());

        if (product.getPriceRanges() == null) {
            product.setPriceRanges(new ArrayList<>());
        }
        if (product.getPriceBreaks() == null) {
            product.setPriceBreaks(new ArrayList<>());
        }
        if (!CollectionUtils.isNullOrEmpty(product.getPriceRanges())) {
            for (ProductPriceRange priceRange : product.getPriceRanges()) {
                priceRange.setId(product.getId() + "_" + priceRange.getWeightToleranceId());
            }
        }

        if (!CollectionUtils.isNullOrEmpty(product.getPriceBreaks())) {
            if (template != null) {
                for (ProductPriceBreak poriceBreak : product.getPriceBreaks()) {
                    poriceBreak.resetPrepare(product.getCompanyId());
                }
            }
            product.getPriceBreaks().removeIf(priceBreak -> ProductPriceBreak.PriceBreakType.None == priceBreak.getPriceBreakType());

            populatePriceBreaks(product, category, product.getPriceBreaks(), Boolean.TRUE, null, Boolean.FALSE, Boolean.TRUE);
        }
    }

    @Override
    public ProductResult getProductById(String productId) {
        ProductResult product = productRepository.get(token.getCompanyId(), productId, ProductResult.class);
        if (product != null) {

            // Lazy way
            SearchResult<Product> results = new SearchResult<>();
            results.getValues().add(product);
            assignedProductDependencies(product, token.getShopId());

            ProductCategory productCategory = productCategoryRepository.get(token.getCompanyId(), product.getCategoryId());

            if (productCategory != null) {
                // Populate the priceRanges
                if (productCategory.getUnitType() == ProductCategory.UnitType.grams) {
                    List<ProductPriceRange> ranges = populatePriceRanges(product, product.getPriceRanges(), "default", null);
                    product.setPriceRanges(ranges);
                    product.setPriceBreaks(new ArrayList<ProductPriceBreak>());
                } else {
                    List<ProductPriceBreak> productPriceBreaks = populatePriceBreaks(product, productCategory, product.getPriceBreaks(), true, null, false, Boolean.FALSE);
                    product.setPriceBreaks(productPriceBreaks);
                    product.setPriceRanges(new ArrayList<ProductPriceRange>());
                }
            }
            Brand brand = brandRepository.get(token.getCompanyId(), product.getBrandId());
            if (brand != null) {
                product.setBrandName(brand.getName());
                product.setBrand(brand);
            }


            if (product.getTaxTables() == null
                    || product.getTaxTables().size() == 0) {
                Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());
                if (shop != null) {
                    if (shop.isUseComplexTax()) {
                        List<CompoundTaxTable> taxes = new ArrayList<>();
                        for (CompoundTaxTable taxTable : shop.getTaxTables()) {
                            taxTable.setId(null);
                            taxTable.prepare(token.getCompanyId());
                            taxes.add(taxTable);
                            taxTable.setTaxType(TaxInfo.TaxType.Custom);
                            taxTable.setActive(false);

                            taxTable.setCityTax(new CompoundTaxRate());
                            taxTable.setCountyTax(new CompoundTaxRate());
                            taxTable.setStateTax(new CompoundTaxRate());
                            taxTable.setFederalTax(new CompoundTaxRate());

                            taxTable.getCityTax().prepare(token.getCompanyId());
                            taxTable.getCityTax().setShopId(token.getShopId());

                            taxTable.getCountyTax().prepare(token.getCompanyId());
                            taxTable.getCountyTax().setShopId(token.getShopId());

                            taxTable.getStateTax().prepare(token.getCompanyId());
                            taxTable.getStateTax().setShopId(token.getShopId());

                            taxTable.getFederalTax().prepare(token.getCompanyId());
                            taxTable.getFederalTax().setShopId(token.getShopId());
                            taxTable.reset();
                        }
                        product.setTaxTables(taxes);
                    }
                }
            }

            if (Product.ProductType.BUNDLE == product.getProductType()) {
                List<BundleItemResult> itemResults = new ArrayList<>();

                List<ObjectId> bundleProductIds = new ArrayList<>();
                for (BundleItem item : product.getBundleItems()) {
                    bundleProductIds.add(new ObjectId(item.getProductId()));
                }

                HashMap<String, Product> productMap = productRepository.listAsMap(token.getCompanyId(), bundleProductIds);
                for (BundleItem item : product.getBundleItems()) {
                    Product bundleProduct = productMap.get(item.getProductId());
                    BundleItemResult result = new BundleItemResult();
                    result.setProductName(bundleProduct.getName());
                    result.setProductId(item.getProductId());
                    result.setQuantity(item.getQuantity());

                    itemResults.add(result);
                }
                product.setBundleItemResults(itemResults);
                product.setBundleItems(null); //setting null as it will be redundant data
            }

            if (!CollectionUtils.isNullOrEmpty(product.getSecondaryVendors())) {
                List<ObjectId> vendorObjIds = new ArrayList<>();
                for (String vendorId : product.getSecondaryVendors()) {
                    if (StringUtils.isNotBlank(vendorId) && ObjectId.isValid(vendorId)) {
                        vendorObjIds.add(new ObjectId(vendorId));
                    }
                }
                HashMap<String, Vendor> secondaryVendorsMap = vendorRepository.listAsMap(token.getCompanyId(), vendorObjIds);
                product.setSecondaryVendorResult(Lists.newArrayList(secondaryVendorsMap.values()));
            }

            WmProductMapping wmProductMapping = wmMappingRepository.getMappingForProduct(token.getCompanyId(), token.getShopId(), product.getId());
            if (wmProductMapping != null) {
                ThirdPartyProduct thirdPartyProduct = wmProductMapping.getThirdPartyProduct();
                if (thirdPartyProduct != null) {
                    product.setThirdPartyProductName(thirdPartyProduct.getName());
                    product.setThirdPartyBrandName(thirdPartyProduct.getThirdPartyBrandName());
                    product.setThirdPartyBrandId(thirdPartyProduct.getThirdPartyBrandId());
                    product.setThirdPartyProductId(thirdPartyProduct.getSourceProductId());
                }
                product.setWmTags(wmProductMapping.getWmTags());
                product.setProductTagGroups(wmProductMapping.getProductTagGroups());
            }

        }

        return product;
    }

    public SearchResult<ProductLimitedResult> getProductListLimited() {
        return productRepository.findItems(token.getCompanyId(), token.getShopId(), 0, PRODUCT_RESULT_LIMIT, ProductLimitedResult.class);
    }

    @Override
    public SearchResult<ProductResult> searchProductsByCategoryId(String shopId, String categoryId, String productId,
                                                                  int start, int limit, boolean quantity, String term, String targetInventoryId, FilterType status, String batchSku) {

        if (limit <= 0 || limit > PRODUCT_RESULT_LIMIT)
            limit = PRODUCT_RESULT_LIMIT;

        if (StringUtils.isBlank(shopId)) {
            shopId = token.getShopId();
        }

        if (!token.getShopId().equalsIgnoreCase(shopId)) {
            ProductCategory currentCategory = productCategoryRepository.get(token.getCompanyId(), categoryId);
            if (currentCategory != null) {
                ProductCategory category = productCategoryRepository.getCategory(token.getCompanyId(), shopId, currentCategory.getName());
                if (category != null) {
                    categoryId = category.getId();
                }
            }
        }

        SearchResult<ProductResult> results = new SearchResult<>();
        if (StringUtils.isNotEmpty(productId)) {
            ProductResult product = productRepository.get(token.getCompanyId(), productId, ProductResult.class);
            if (product != null) {
                results.getValues().add(product);
            }
        } else if (StringUtils.isEmpty(categoryId) && StringUtils.isNoneEmpty(term)) {
            if (status == null || status == FilterType.All) {
                results = productRepository.findProductsByPattern(token.getCompanyId(), shopId, "{name:1}", term, start, limit, ProductResult.class);
            } else {
                results = productRepository.findProductsByPatternAndState(token.getCompanyId(), shopId, "{name:1}", term, start, limit, ProductResult.class, (status == FilterType.Active));
            }
        } else if (StringUtils.isNotEmpty(categoryId) && StringUtils.isNoneEmpty(term)) {
            if (status == null || status == FilterType.All) {
                results = productRepository.findProductsByCategoryIdAndTerm(token.getCompanyId(), shopId, "{name:1}", categoryId, term, start, limit, ProductResult.class);
            } else {
                results = productRepository.findProductsByCategoryIdAndTermAndState(token.getCompanyId(), shopId, "{name:1}", categoryId, term, start, limit, ProductResult.class, (status == FilterType.Active));
            }
        } else if (StringUtils.isNotEmpty(categoryId)) {
            if (quantity) {

                Terminal terminal = terminalRepository.get(token.getCompanyId(), token.getTerminalId());
                String inventoryId = terminal.getAssignedInventoryId();

                if (StringUtils.isNotBlank(targetInventoryId)) {
                    inventoryId = targetInventoryId;
                }

                HashMap<String, Integer> quantityProductMap =
                        productPrepackageQuantityRepository.getQuantitiesForInventoryWithQuantityProductMap(token.getCompanyId(), shopId, inventoryId);


                if (status == null || status == FilterType.All) {
                    results = productRepository.findProductsByCategoryId(token.getCompanyId(), shopId, categoryId, "{name:1}", start, limit, ProductResult.class);
                } else {
                    results = productRepository.findProductsByCategoryIdAndState(token.getCompanyId(), shopId, categoryId, "{name:1}", start, limit, ProductResult.class, (status == FilterType.Active));
                }
                List<ProductResult> productResults = results.getValues();
                List<ProductResult> productResultList = new ArrayList<>();


                for (ProductResult productResult : productResults) {
                    List<ProductQuantity> productQuantities = productResult.getQuantities();
                    List<ProductQuantity> productQuantityList = new ArrayList<>();
                    for (ProductQuantity productQuantity : productQuantities) {
                        if (productQuantity.getQuantity().compareTo(BigDecimal.ZERO) != 0 && productQuantity.getInventoryId().equals(inventoryId)) {
                            productQuantityList.add(productQuantity);
                        }
                    }

                    Integer prepackageAmt = quantityProductMap.get(productResult.getId());

                    // if quantity exists and prepackageAmt is greater than 0
                    if (productQuantityList.size() > 0 || (prepackageAmt != null && prepackageAmt > 0)) {
                        productResult.setQuantities(productQuantityList);
                        productResultList.add(productResult);
                    }

                }
                results.setValues(productResultList);
            } else {
                if (status == null || status == FilterType.All) {
                    results = productRepository.findProductsByCategoryId(token.getCompanyId(), shopId, categoryId, "{name:1}", start, limit, ProductResult.class);
                } else {
                    results = productRepository.findProductsByCategoryIdAndState(token.getCompanyId(), shopId, categoryId, "{name:1}", start, limit, ProductResult.class, (status == FilterType.Active));
                }
            }
        } else if (StringUtils.isNotEmpty(batchSku)) {
            Product product = getProductByBatchSKU(batchSku);
            ProductResult productResult = productRepository.get(token.getCompanyId(), product.getId(), ProductResult.class);
            if (productResult != null) {
                results.getValues().add(productResult);
            }
        } else {
            if (quantity) {
                Terminal terminal = terminalRepository.get(token.getCompanyId(), token.getTerminalId());
                String inventoryId = terminal.getAssignedInventoryId();

                if (StringUtils.isNotBlank(targetInventoryId)) {
                    inventoryId = targetInventoryId;
                }

                HashMap<String, Integer> quantityProductMap =
                        productPrepackageQuantityRepository.getQuantitiesForInventoryWithQuantityProductMap(token.getCompanyId(), shopId, inventoryId);

                if (status == null || status == FilterType.All) {
                    results = productRepository.findItems(token.getCompanyId(), shopId, "{name:1}", start, limit, ProductResult.class);
                } else {
                    results = productRepository.findItemsByState(token.getCompanyId(), shopId, "{name:1}", start, limit, ProductResult.class, (status == FilterType.Active));
                }

                List<ProductResult> productResults = results.getValues();
                List<ProductResult> productResultList = new ArrayList<>();

                for (ProductResult productResult : productResults) {
                    List<ProductQuantity> quantities = productResult.getQuantities();
                    List<ProductQuantity> productQuantityList = new ArrayList<>();
                    for (ProductQuantity productQuantity : quantities) {
                        if (productQuantity.getQuantity().compareTo(BigDecimal.ZERO) != 0 && productQuantity.getInventoryId().equals(inventoryId)) {
                            productQuantityList.add(productQuantity);
                        }
                    }

                    Integer prepackageAmt = quantityProductMap.get(productResult.getId());

                    // if quantity exists and prepackageAmt is greater than 0
                    if (productQuantityList.size() > 0 || (prepackageAmt != null && prepackageAmt > 0)) {
                        productResult.setQuantities(productQuantityList);

                        productResultList.add(productResult);
                    }

                }
                results.setValues(productResultList);

            } else {
                if (status == null || status == FilterType.All) {
                    results = productRepository.findItems(token.getCompanyId(), shopId, "{name:1}", start, limit, ProductResult.class);
                } else {
                    results = productRepository.findItemsByState(token.getCompanyId(), shopId, "{name:1}", start, limit, ProductResult.class, (status == FilterType.Active));
                }
            }
        }

        assignedProductDependencies(results, shopId);
        return results;
    }

    private SearchResult<ProductResult> assignedProductDependencies(SearchResult<ProductResult> products, String shopId) {
        List<ObjectId> vendorIds = new ArrayList<>();
        List<ObjectId> brandIds = new ArrayList<>();
        List<String> productIds = new ArrayList<>();
        List<ObjectId> tagGroupIds = new ArrayList<>();

        for (Product p : products.getValues()) {
            productIds.add(p.getId());
            if (p.getVendorId() != null && ObjectId.isValid(p.getVendorId())) {
                vendorIds.add(new ObjectId(p.getVendorId()));
            }
            if (StringUtils.isNotBlank(p.getBrandId()) && ObjectId.isValid(p.getBrandId())) {
                brandIds.add(new ObjectId(p.getBrandId()));
            }
            if (!CollectionUtils.isNullOrEmpty(p.getSecondaryVendors())) {
                for (String vendorId : p.getSecondaryVendors()) {
                    if (StringUtils.isNotBlank(vendorId) && ObjectId.isValid(vendorId)) {
                        vendorIds.add(new ObjectId(vendorId));
                    }
                }
            }
        }

        HashMap<String, Vendor> vendorHashMap = vendorRepository.findItemsInAsMap(token.getCompanyId(), vendorIds);
        HashMap<String, Brand> brandHashMap = brandRepository.findItemsInAsMap(token.getCompanyId(), brandIds);
        HashMap<String, ProductCategory> productCategoryHashMap = productCategoryRepository.listAllAsMap(token.getCompanyId(), shopId);
        HashMap<String, WmProductMapping> wmProductMappingMap = wmMappingRepository.getMappingForProducts(token.getCompanyId(), token.getShopId(), productIds);

        for (ProductResult p : products.getValues()) {
            p.setVendor(vendorHashMap.get(p.getVendorId()));
            p.setCategory(productCategoryHashMap.get(p.getCategoryId()));

            BigDecimal total = new BigDecimal(0);
            for (ProductQuantity pq : p.getQuantities()) {
                total = total.add(pq.getQuantity());
            }
            p.setQuantityAvailable(total);

            Brand brand = brandHashMap.get(p.getBrandId());
            if (brand != null) {
                p.setBrandName(brand.getName());
            }
            if (!CollectionUtils.isNullOrEmpty(p.getSecondaryVendors())) {
                for (String vendorId : p.getSecondaryVendors()) {
                    if (p.getSecondaryVendorResult() == null) {
                        p.setSecondaryVendorResult(new ArrayList<>());
                    }
                    p.getSecondaryVendorResult().add(vendorHashMap.get(vendorId));
                }
            }
            WmProductMapping wmProductMapping = wmProductMappingMap.get(p.getId());
            if (wmProductMapping != null) {
                ThirdPartyProduct thirdPartyProduct = wmProductMapping.getThirdPartyProduct();
                if (thirdPartyProduct != null) {
                    p.setThirdPartyBrandId(thirdPartyProduct.getThirdPartyBrandId());
                    p.setThirdPartyProductId(thirdPartyProduct.getSourceProductId());
                    p.setThirdPartyProductName(thirdPartyProduct.getName());
                    p.setThirdPartyBrandName(thirdPartyProduct.getThirdPartyBrandName());
                }
                p.setWmTags(wmProductMapping.getWmTags());
                p.setProductTagGroups(wmProductMapping.getProductTagGroups());
            }
        }

        return products;
    }

    private void assignedProductDependencies(Product product, String shopId) {
        List<ObjectId> vendorIds = new ArrayList<>();
        if (product.getVendorId() != null && ObjectId.isValid(product.getVendorId())) {
            vendorIds.add(new ObjectId(product.getVendorId()));
        }

        HashMap<String, Vendor> vendorHashMap = vendorRepository.findItemsInAsMap(token.getCompanyId(), vendorIds);
        HashMap<String, ProductCategory> productCategoryHashMap = productCategoryRepository.listAllAsMap(token.getCompanyId(), shopId);
        product.setVendor(vendorHashMap.get(product.getVendorId()));
        product.setCategory(productCategoryHashMap.get(product.getCategoryId()));
    }

    @Override
    public void deleteProduct(String productId) {
        // Make inventory isn't available
        Product dbProduct = productRepository.get(token.getCompanyId(), productId);
        if (dbProduct == null) {
            throw new BlazeInvalidArgException("Product.id", "Invalid product id");
        }

        List<String> productIds = new ArrayList<>();
        productIds.add(productId);
        SearchResult<PurchaseOrder> allPurchaseOrder = purchaseOrderRepository.getAllPurchaseOrderByProductId(token.getCompanyId(), token.getShopId(), "{modified:-1}", productIds);
        if (allPurchaseOrder.getValues().size() > 0) {
            String purchaseDetails = "";
            StringBuilder purchaseString = new StringBuilder();
            purchaseString.append(purchaseDetails);
            for (PurchaseOrder purchaseOrder : allPurchaseOrder.getValues()) {
                purchaseString.append(" " + purchaseOrder.getPoNumber() + ",");
            }

            String purchaseSubString = purchaseString.substring(0, purchaseString.length() - 1);
            throw new BlazeInvalidArgException("Product", "This product is being used in purchase order (" + purchaseSubString + "). So can not deactivate it.");
        }


        //boolean hasQuantity = false;
        for (ProductQuantity quantity : dbProduct.getQuantities()) {
            if (quantity.getQuantity().doubleValue() > 0) {
                throw new BlazeInvalidArgException(PRODUCT, CAN_NOT_DELETE_QUANTITY_AVAILABLE);
            }
        }

        //TODO:
        // Delete any prepackages

        // Delete anY prepackage items and prepackage quantity
        elasticSearchManager.deleteIndexedDocument(productId, Product.class);
        barcodeService.deleteBarcodesForProduct(dbProduct.getId());
        productRepository.removeByIdSetState(token.getCompanyId(), productId);
        realtimeService.sendRealTimeEvent(token.getShopId().toString(),
                RealtimeService.RealtimeEventType.InventoryUpdateEvent, null);

    }

    @Override
    public DateSearchResult<Product> getProducts(long afterDate, long beforeDate) {
        return productRepository.findItemsWithDate(token.getCompanyId(), token.getShopId(), afterDate, beforeDate);
    }

    private List<MedicalCondition> updateMedicalConditions(List<MedicalCondition> medicalConditions) {
        List<MedicalCondition> currentMeds = medicalConditionRepository.list();
        HashMap<String, MedicalCondition> medMap = new HashMap<>();
        for (MedicalCondition current : currentMeds) {
            medMap.put(current.getName(), current);
        }

        Set<MedicalCondition> values = new LinkedHashSet<>();
        for (MedicalCondition condition : medicalConditions) {
            MedicalCondition prev = medMap.get(condition.getName());
            if (prev != null) {
                values.add(prev);
            } else {
                condition.setId(null);
                medicalConditionRepository.save(condition);
                values.add(condition);
                medMap.put(condition.getName(), condition);
            }
        }
        realtimeService.sendRealTimeEvent(token.getShopId().toString(),
                RealtimeService.RealtimeEventType.InventoryUpdateEvent, null);
        return Lists.newArrayList(values);
    }

    @Override
    public List<ProductWeightTolerance> getWeightTolerances() {
        Iterable<ProductWeightTolerance> list = productWeightToleranceRepository.listSort(token.getCompanyId(), "{startWeight:1}");
        return Lists.newArrayList(list);
    }

    @Override
    public HashMap<String, String> getProductNameMap(String companyId, String shopId) {
        HashMap<String, String> productMap = new HashMap<>();
        List<Product> list = productRepository.findItems(companyId, shopId, 0, Integer.MAX_VALUE).getValues();
        for (Product p : list) {
            productMap.put(p.getId(), p.getName());
        }
        return productMap;
    }

    @Override
    public SearchResult<Product> getProductsByIds(List<String> productIds) {
        List<ObjectId> ids = new ArrayList<>();
        for (String pid : productIds) {
            if (pid != null && ObjectId.isValid(pid)) {
                ids.add(new ObjectId(pid));
            }
        }

        Iterable<Product> products = productRepository.findProductsByIds(token.getCompanyId(), token.getShopId(), ids);

        SearchResult<Product> result = new SearchResult<>();
        result.setValues(Lists.newArrayList(products));
        result.setSkip(0);
        result.setLimit(result.getValues().size());
        result.setTotal((long) result.getValues().size());
        return result;
    }

    @Override
    public Product getProductByBatchSKU(String batchSKU) {
        ProductBatch batch = batchRepository.getBatchBySKU(token.getCompanyId(), token.getShopId(), batchSKU);
        if (batch == null) {
            throw new BlazeInvalidArgException("ProductBatch", "Product batch does not exists");
        }
        return productRepository.get(token.getCompanyId(), batch.getProductId());
    }

    @Override
    public SearchResult<Product> getProductsByVendorId(String vendorId, boolean active) {
        return productRepository.getProductsByVendorId(token.getCompanyId(), token.getShopId(), vendorId, active, Product.class);
    }

    @Override
    public Product convertToProduct(String productId, InventoryConvertToProductRequest request) {
        // Check if there are current pending queued items
        /*long count = queuedTransactionRepository.getCountStatus(token.getCompanyId(),token.getShopId(), QueuedTransaction.QueueStatus.Pending);
        if (count > 0) {
            throw new BlazeInvalidArgException("ProductConvert", "There are active pending transactions. Please wait until all transactions are completed.");
        }*/

        Product dbProduct = productRepository.get(token.getCompanyId(), productId);
        if (dbProduct == null) {
            throw new BlazeInvalidArgException("ProductConvert", "Product does not exist.");
        }
        Vendor vendor = vendorRepository.get(token.getCompanyId(), dbProduct.getVendorId());

        ProductBatch oldBatch = batchRepository.get(token.getCompanyId(), request.getBatchId());
        if (oldBatch == null) {
            throw new BlazeInvalidArgException("ProductConvert", "Batch does not exist.");
        }
        Inventory safeInventory = inventoryRepository.getInventory(token.getCompanyId(), token.getShopId(), Inventory.SAFE);
        if (safeInventory == null) {
            throw new BlazeInvalidArgException("ProductConvert", "Safe Inventory does not exist.");
        }

        Product targetProduct = null;
        if (request.isExisting()) {
            targetProduct = productRepository.get(token.getCompanyId(), request.getTargetProductId());
            if (targetProduct == null) {
                throw new BlazeInvalidArgException("ProductConvert", "Target product does not exist.");
            }


        } else {
            ProductCategory category = categoryRepository.get(token.getCompanyId(), request.getTargetCategoryId());
            if (category == null) {
                throw new BlazeInvalidArgException("ProductConvert", "Target category does not exist.");
            }

            if (StringUtils.isBlank(request.getTargetProductName())) {
                throw new BlazeInvalidArgException("ProductConvert", "Target name must not be blank.");
            }
            // create new batch
            targetProduct = new Product();
            targetProduct.prepare(token.getCompanyId());
            targetProduct.setVendorId(dbProduct.getVendorId());
            targetProduct.setCategoryId(category.getId());

            targetProduct.setName(request.getTargetProductName());
            targetProduct.setCategoryId(category.getId());
            targetProduct.setShopId(token.getShopId());
            targetProduct.setUnitPrice(new BigDecimal(0));
            targetProduct.setDescription("");
            targetProduct.setPriceRanges(new ArrayList<ProductPriceRange>());
            targetProduct.setFlowerType(dbProduct.getFlowerType());
            targetProduct.setGenetics(dbProduct.getGenetics());
            targetProduct.setEnableMixMatch(false);
            targetProduct.setTaxOrder(dbProduct.getTaxOrder());
            targetProduct.setTaxType(dbProduct.getTaxType());
            targetProduct.setCustomTaxInfo(dbProduct.getCustomTaxInfo());

            targetProduct.setCustomWeight(dbProduct.getCustomWeight());
            targetProduct.setCustomGramType(dbProduct.getCustomGramType());
            targetProduct.setSecondaryVendors(dbProduct.getSecondaryVendors());
            targetProduct.setWeightPerUnit(dbProduct.getWeightPerUnit());
            targetProduct.setToleranceId(dbProduct.getToleranceId());

            targetProduct.setProducerMfg(dbProduct.getProducerMfg());
            targetProduct.setProducerLicense(dbProduct.getProducerLicense());
            targetProduct.setProducerAddress(dbProduct.getProducerAddress());
            if (targetProduct.getProducerAddress() != null) {
                targetProduct.getProducerAddress().prepare(token.getCompanyId());
            }
            targetProduct.setSecondaryVendors(dbProduct.getSecondaryVendors());

            if (category.getUnitType() == ProductCategory.UnitType.grams) {
                List<ProductWeightTolerance> tolerances = this.getWeightTolerances();
                // Recreate the tolerance
                List<ProductPriceRange> productPriceRanges = new ArrayList<>();
                for (ProductWeightTolerance tolerance : tolerances) {
                    if (tolerance.isEnabled()) {
                        ProductPriceRange priceRange = new ProductPriceRange();
                        productPriceRanges.add(priceRange);

                        priceRange.setWeightToleranceId(tolerance.getId());
                        priceRange.setWeightTolerance(tolerance);
                        priceRange.setPriority(tolerance.getPriority());
                        priceRange.setId(targetProduct.getId() + "_" + priceRange.getWeightToleranceId());
                    }
                }


                int priority = 0;
                for (ProductPriceRange productPriceRange : targetProduct.getPriceRanges()) {
                    for (ProductPriceRange newPriceRange : productPriceRanges) {
                        if (newPriceRange.getWeightToleranceId().equalsIgnoreCase(productPriceRange.getWeightToleranceId())) {
                            newPriceRange.setPrice(productPriceRange.getPrice());
                            break;
                        }
                    }
                    priority++;
                }

                targetProduct.setPriceRanges(productPriceRanges);
                targetProduct.setPriceBreaks(new ArrayList<ProductPriceBreak>());
            } else {
                // This is the unit category, use price breaks instead of price ranges
                if (targetProduct.getPriceBreaks() != null) {
                    for (ProductPriceBreak priceBreak : targetProduct.getPriceBreaks()) {
                        priceBreak.resetPrepare(token.getCompanyId());
                        if (targetProduct.getWeightPerUnit() == Product.WeightPerUnit.EACH
                                || targetProduct.getWeightPerUnit() == Product.WeightPerUnit.FULL_GRAM
                                || targetProduct.getWeightPerUnit() == Product.WeightPerUnit.EIGHTH
                                || targetProduct.getWeightPerUnit() == Product.WeightPerUnit.FOURTH) {
                            if (priceBreak.getPriceBreakType() == ProductPriceBreak.PriceBreakType.OneGramUnit) {
                                priceBreak.setActive(true);
                            }
                        } else {
                            if (priceBreak.getPriceBreakType() == ProductPriceBreak.PriceBreakType.HalfGramUnit) {
                                priceBreak.setActive(true);
                            }
                        }
                    }
                }
                populatePriceBreaks(targetProduct, category, targetProduct.getPriceBreaks(), true, null, false, Boolean.FALSE);

                targetProduct.setPriceBreaks(targetProduct.getPriceBreaks());
                targetProduct.setPriceRanges(new ArrayList<ProductPriceRange>());
            }
            //link the id of the vendor in our DB to this product

            BarcodeItem item = barcodeService.createBarcodeItemIfNeeded(targetProduct.getCompanyId(), targetProduct.getShopId(), targetProduct.getId(),
                    BarcodeItem.BarcodeEntityType.Product,
                    targetProduct.getId(), targetProduct.getSku(), targetProduct.getSku(), false);
            if (item != null) {
                targetProduct.setSku(item.getBarcode());
            }

            productRepository.save(targetProduct);

        }

        // Deduct from old product
        double totalToDeduct = request.getQuantity().doubleValue() * (request.getUnitValue().doubleValue() != 0 ? request.getUnitValue().doubleValue() : 1.0);
        for (ProductQuantity productQuantity : dbProduct.getQuantities()) {
            if (productQuantity.getInventoryId().equalsIgnoreCase(safeInventory.getId())) {

                double currentQuantity = productQuantity.getQuantity().doubleValue();
                if (currentQuantity < totalToDeduct) {
                    throw new BlazeInvalidArgException("Inventory", "Not enough quantity to convert.");
                }

                double newAmount = currentQuantity - totalToDeduct;

                // Deduct
                if (newAmount < 0) {
                    newAmount = 0;
                }
                productQuantity.setQuantity(new BigDecimal(newAmount));

                break;
            }
        }
        this.createQueuedTransactionJobForConvertToProduct(dbProduct, safeInventory, totalToDeduct);
        // Update old product
//        productRepository.update(token.getCompanyId(),dbProduct.getId(),dbProduct);

        // Add new batch
        BatchAddRequest batchAddRequest = new BatchAddRequest();
        batchAddRequest.setActive(true);
        batchAddRequest.setCbd(oldBatch.getCbd());
        batchAddRequest.setCbda(oldBatch.getCbda());
        batchAddRequest.setCbn(oldBatch.getCbn());
        batchAddRequest.setProductId(targetProduct.getId());
        batchAddRequest.setThc(oldBatch.getThc());
        batchAddRequest.setThca(oldBatch.getThca());
        batchAddRequest.setPurchasedDate(DateTime.now().getMillis());
        batchAddRequest.setQuantity(request.getQuantity());
        batchAddRequest.setCost(new BigDecimal(0));
        batchAddRequest.setSellBy(oldBatch.getSellBy());
        batchAddRequest.setVendorId(StringUtils.isBlank(request.getVendorId()) ? dbProduct.getVendorId() : request.getVendorId());

        // Add batch for target product
        inventoryService.addBatch(batchAddRequest);

        realtimeService.sendRealTimeEvent(token.getShopId().toString(),
                RealtimeService.RealtimeEventType.InventoryUpdateEvent, null);
        return dbProduct;
    }

    private void createQueuedTransactionJobForConvertToProduct(Product product, Inventory inventory, double quantity) {
        ConvertToProductQueuedTransaction queuedTransaction = new ConvertToProductQueuedTransaction();
        queuedTransaction.setShopId(token.getShopId());
        queuedTransaction.setCompanyId(token.getCompanyId());
        queuedTransaction.setId(ObjectId.get().toString());
        queuedTransaction.setCart(null);
        queuedTransaction.setPendingStatus(Transaction.TransactionStatus.InProgress);
        queuedTransaction.setStatus(QueuedTransaction.QueueStatus.Pending);
        queuedTransaction.setQueuedTransType(QueuedTransaction.QueuedTransactionType.Product);
        queuedTransaction.setSellerId(token.getActiveTopUser().getUserId());
        queuedTransaction.setMemberId(null);
        queuedTransaction.setTerminalId(null);
        queuedTransaction.setTransactionId(product.getId());
        queuedTransaction.setRequestTime(DateTime.now().getMillis());
        queuedTransaction.setInventoryId(inventory.getId());
        queuedTransaction.setQuantity(new BigDecimal(quantity));
        queuedTransaction.setOperationType(ProductBatchQueuedTransaction.OperationType.ConvertToProduct);

        queuedTransactionRepository.save(queuedTransaction);

        backgroundJobService.addTransactionJob(token.getCompanyId(), token.getShopId(), queuedTransaction.getId());
    }


    @Override
    public SearchResult<Product> copyProductsToShop(CopyProductRequest copyRequest) {

        Shop otherShop = shopRepository.get(token.getCompanyId(), copyRequest.getToShopId());
        if (otherShop == null) {
            throw new BlazeInvalidArgException("ProductCopy", "Shop does not exist with specified id.");
        }

        List<ObjectId> productIds = new ArrayList<>();
        for (String pid : copyRequest.getProductIds()) {
            if (ObjectId.isValid(pid)) {
                productIds.add(new ObjectId(pid));
            }
        }

        Iterable<Product> products = productRepository.findProductsByIds(token.getCompanyId(), token.getShopId(), productIds);


        // Find current set of products from other shop
        Iterable<Product> otherShopProducts = productRepository.listByShop(token.getCompanyId(), otherShop.getId());
        HashMap<String, Product> otherProductHashMap = new HashMap<>();
        // Create product map with name_vendorId as key
        for (Product product : otherShopProducts) {
            String key = product.getCompanyLinkId();
            otherProductHashMap.put(key, product);
        }

        HashMap<String, ProductCategory> currentCategories = productCategoryRepository.listAllAsMap(token.getCompanyId(), token.getShopId());
        Iterable<ProductCategory> otherCategories = productCategoryRepository.listAllByShop(token.getCompanyId(), copyRequest.getToShopId());
        HashMap<String, ProductCategory> otherCategoryMap = new HashMap<>();
        for (ProductCategory category : otherCategories) {
            otherCategoryMap.put(category.getName().toLowerCase(), category);
        }


        // Retrieve prepackages
        Iterable<Prepackage> prepackages = prepackageRepository.listByShop(token.getCompanyId(), token.getShopId());
        HashMap<String, List<Prepackage>> prepackageListMap = new HashMap<>();
        for (Prepackage prepackage : prepackages) {
            List<Prepackage> prepackageList = prepackageListMap.get(prepackage.getProductId());
            if (prepackageList == null) {
                prepackageList = new ArrayList<>();
                prepackageListMap.put(prepackage.getProductId(), prepackageList);
            }
            prepackageList.add(prepackage);
        }


        List<Prepackage> prepackagesToAdd = new ArrayList<>();
        List<ProductCategory> categoriesToAdd = new ArrayList<>();
        List<Product> productsToAdd = new ArrayList<>();
        List<Product> productsToUpdate = new ArrayList<>();

        Kryo kryo = new Kryo();
        long currentDate = DateTime.now().getMillis();
        for (Product product : products) {
            String key = product.getCompanyLinkId();
            if (otherProductHashMap.containsKey(key)) {
                // this product exist on another shop, skip or put it for update
                productsToUpdate.add(otherProductHashMap.get(key));
                // Ignore
            } else {
                Product newProduct = kryo.copy(product);
                // New product
                long now = DateTime.now().getMillis();
                newProduct.setShopId(otherShop.getId());
                newProduct.setId(ObjectId.get().toString());
                newProduct.setCreated(now);

                newProduct.setCustomWeight(product.getCustomWeight());
                newProduct.setCustomGramType(product.getCustomGramType());


                //1. Change categoryId -- use matching category name
                ProductCategory category = currentCategories.get(newProduct.getCategoryId());
                ProductCategory otherCategory = otherCategoryMap.get(category.getName().toLowerCase());
                if (otherCategory == null) {
                    otherCategory = kryo.copy(category); // Make a copy
                    otherCategory.setId(ObjectId.get().toString());
                    otherCategory.setShopId(otherShop.getId());
                    otherCategory.setModified(currentDate);
                    otherCategory.setCreated(currentDate);
                    otherCategory.setActive(true);
                    otherCategory.setDeleted(false);
                    categoriesToAdd.add(otherCategory);
                    otherCategoryMap.put(otherCategory.getName().toLowerCase(), otherCategory);
                }
                newProduct.setCategoryId(otherCategory.getId());
                newProduct.setCategory(otherCategory);

                //2. Change Photos
                // Keep photos/assets the same

                //3. Set quantities to 0
                newProduct.setQuantities(new ArrayList<ProductQuantity>());

                //4. Reset preconditions shopId
                for (MedicalCondition condition : newProduct.getMedicalConditions()) {
                    condition.setId(ObjectId.get().toString());
                    condition.setModified(currentDate);
                    condition.setCreated(currentDate);
                }

                //5. Reset price ranges
                for (ProductPriceRange productPriceRange : newProduct.getPriceRanges()) {
                    productPriceRange.setId(String.format("%s_%s", newProduct.getId(), productPriceRange.getWeightToleranceId()));
                }

                //6. Reset price breaks
                for (ProductPriceBreak priceBreak : newProduct.getPriceBreaks()) {
                    priceBreak.setId(ObjectId.get().toString());
                    priceBreak.setModified(currentDate);
                    priceBreak.setCreated(currentDate);
                }

                //7. Clear notes
                newProduct.setNotes(new ArrayList<Note>());

                //8. Transfer prepackages over if any. Do not transfer prepackage quantities.
                List<Prepackage> productPrepackages = prepackageListMap.get(product.getId());
                if (productPrepackages != null) {
                    for (Prepackage prepackage : productPrepackages) {
                        Prepackage newPrepackage = kryo.copy(prepackage);
                        newPrepackage.setShopId(otherShop.getId());
                        newPrepackage.setProductId(newProduct.getId());
                        newPrepackage.setId(ObjectId.get().toString());
                        newPrepackage.setModified(currentDate);
                        newPrepackage.setCreated(currentDate);
                        prepackagesToAdd.add(newPrepackage);
                    }
                }


                //9. Use same sku

                //10. Keep vendor the same as vendor is now a company wide thing


                productsToAdd.add(newProduct);

                // store this tmeporary
                otherProductHashMap.put(key, newProduct);
            }
        }
        for (Product newProduct : productsToAdd) {
            BarcodeItem item = barcodeService.createBarcodeItemIfNeeded(newProduct.getCompanyId(), newProduct.getShopId(), newProduct.getId(),
                    BarcodeItem.BarcodeEntityType.Product, newProduct.getId(), newProduct.getSku(), null, false);
            if (item != null) {
                newProduct.setSku(item.getBarcode());
            }
        }

        // Now add things over
        productRepository.save(productsToAdd);
        productCategoryRepository.save(categoriesToAdd);
        prepackageRepository.save(prepackagesToAdd);

        realtimeService.sendRealTimeEvent(token.getShopId().toString(),
                RealtimeService.RealtimeEventType.ProductsUpdateEvent, null);
        SearchResult<Product> result = new SearchResult<>();
        result.setValues(productsToAdd);
        result.setTotal((long) productsToAdd.size());
        callWebHookForCopyProduct(productsToAdd);
        return result;
    }


    //MEMBER GROUP PRICES
    @Override
    public SearchResult<MemberGroupPricesResult> getMemberGroupPrices(String productId) {
        Product product = productRepository.get(token.getCompanyId(), productId);
        if (product == null) {
            throw new BlazeInvalidArgException("GroupPrices", "Product does not exist.");
        }

        Iterable<MemberGroupPricesResult> prices = groupPricesRepository.getPricesForProduct(token.getCompanyId(),
                token.getShopId(),
                productId, MemberGroupPricesResult.class);

        Iterable<MemberGroup> memberGroups = memberGroupRepository.list(token.getCompanyId());

        ProductCategory category = productCategoryRepository.get(token.getCompanyId(), product.getCategoryId());

        SearchResult<MemberGroupPricesResult> pricesResultSearchResult = new SearchResult<>();
        for (MemberGroup memberGroup : memberGroups) {
            MemberGroupPricesResult groupPrice = null;

            // Find the groupPrices -- should be fast as there shouldn't be too many prices
            for (MemberGroupPricesResult pricesResult : prices) {
                if (pricesResult.getMemberGroupId().equalsIgnoreCase(memberGroup.getId())) {
                    groupPrice = pricesResult;

                    break;
                }
            }

            if (groupPrice == null) {
                groupPrice = new MemberGroupPricesResult();
                groupPrice.prepare(token.getCompanyId());
                groupPrice.setShopId(token.getShopId());
                groupPrice.setEnabled(false);
                groupPrice.setProductId(productId);
                groupPrice.setMemberGroupId(memberGroup.getId());
            }


            if (category.getUnitType() == ProductCategory.UnitType.units) {
                // Fill in the priceBreaks or priceRanges
                List<ProductPriceBreak> priceBreaks = populatePriceBreaks(product, category, groupPrice.getPriceBreaks(), false, null, false, Boolean.FALSE);
                groupPrice.setPriceBreaks(priceBreaks);
                groupPrice.setPriceRanges(new ArrayList<ProductPriceRange>());
            } else {
                // Populate the priceRanges
                List<ProductPriceRange> priceRanges = populatePriceRanges(product, groupPrice.getPriceRanges(), groupPrice.getMemberGroupId(), null);
                groupPrice.setPriceRanges(priceRanges);
                groupPrice.setPriceBreaks(new ArrayList<ProductPriceBreak>());
            }

            groupPrice.setMemberGroup(memberGroup);
            pricesResultSearchResult.getValues().add(groupPrice);
        }


        // Add any missing member groups
        return pricesResultSearchResult;
    }

    @Override
    public DateSearchResult<MemberGroupPrices> getMemberGroupPrices(long afterDate, long beforeDate) {
        return groupPricesRepository.findItemsWithDate(token.getCompanyId(), afterDate, beforeDate);
    }

    @Override
    public MemberGroupPricesResult getGroupPricesForMember(String productId, String memberId) {
        Product product = productRepository.get(token.getCompanyId(), productId);
        if (product == null) {
            throw new BlazeInvalidArgException("GroupPrices", "Product does not exist.");
        }

        Member member = memberRepository.get(token.getCompanyId(), memberId);
        if (member == null) {
            throw new BlazeInvalidArgException("Member", "Member does not exist.");
        }
        MemberGroup memberGroup = memberGroupRepository.get(token.getCompanyId(), member.getMemberGroupId());
        MemberGroupPricesResult memberGroupPrices = groupPricesRepository.getPricesForProductGroup(token.getCompanyId(),
                token.getShopId(),
                product.getId(),
                member.getMemberGroupId(),
                MemberGroupPricesResult.class);
        if (memberGroupPrices == null) {
            memberGroupPrices = new MemberGroupPricesResult();
            memberGroupPrices.setEnabled(false);
        }

        boolean isDefault = false;
        if (memberGroupPrices.isEnabled() == false) {
            memberGroupPrices.setPriceBreaks(product.getPriceBreaks());
            memberGroupPrices.setPriceRanges(product.getPriceRanges());
            isDefault = true;
        }

        ProductCategory productCategory = productCategoryRepository.get(token.getCompanyId(), product.getCategoryId());
        if (productCategory == null) {
            throw new BlazeInvalidArgException("Member", String.format("Product '%s' is missing a category."));
        }
        if (productCategory.getUnitType() == ProductCategory.UnitType.grams) {
            List<ProductPriceRange> priceRanges = populatePriceRanges(product, memberGroupPrices.getPriceRanges(), memberGroupPrices.getMemberGroupId(), null);
            memberGroupPrices.setPriceRanges(priceRanges);
            memberGroupPrices.setPriceBreaks(new ArrayList<ProductPriceBreak>());
        } else {
            List<ProductPriceBreak> priceBreaks = populatePriceBreaks(product, productCategory, memberGroupPrices.getPriceBreaks(), isDefault, null, false, false);
            memberGroupPrices.setPriceBreaks(priceBreaks);
            memberGroupPrices.setPriceRanges(new ArrayList<ProductPriceRange>());
        }

        memberGroupPrices.setMemberGroup(memberGroup);
        return memberGroupPrices;
    }

    @Override
    public List<ProductPriceRange> populatePriceRanges(final Product product, final List<ProductPriceRange> priceRanges, String memberGroupId, ArrayList<ProductWeightTolerance> weightTolerancesList) {
        List<ProductWeightTolerance> tolerances = this.getWeightTolerances();
        if (weightTolerancesList != null) {
            tolerances = weightTolerancesList;
        }

        // Recreate the tolerance
        List<ProductPriceRange> productPriceRanges = new ArrayList<>();
        for (ProductWeightTolerance tolerance : tolerances) {
            if (tolerance.isEnabled()) {
                ProductPriceRange priceRange = new ProductPriceRange();
                productPriceRanges.add(priceRange);

                priceRange.setWeightToleranceId(tolerance.getId());
                priceRange.setWeightTolerance(tolerance);
                priceRange.setPriority(tolerance.getPriority());
                priceRange.setId(product.getId() + "_" + priceRange.getWeightToleranceId() + "_" + memberGroupId);
            }
        }


        int priority = 0;
        // fill in the price
        for (ProductPriceRange productPriceRange : priceRanges) {
            for (ProductPriceRange newPriceRange : productPriceRanges) {
                if (newPriceRange.getWeightToleranceId().equalsIgnoreCase(productPriceRange.getWeightToleranceId())) {
                    newPriceRange.setPrice(productPriceRange.getPrice());
                    break;
                }
            }
            priority++;
        }
        return productPriceRanges;
    }

    @Override
    public List<ProductPriceBreak> populatePriceBreaks(final Product product,
                                                       ProductCategory category,
                                                       List<ProductPriceBreak> priceBreaks,
                                                       boolean isDefault,
                                                       ArrayList<ProductWeightTolerance> weightTolerancesList,
                                                       boolean reset, Boolean isPricingTemplate) {

        // Fill in the priceBreaks or priceRanges
        if (category.getUnitType() == ProductCategory.UnitType.units) {

            final ProductPriceBreak.PriceBreakType[] priceBreakTypes = ProductPriceBreak.PriceBreakType.values();

            // Remove bad price breaks
            priceBreaks.removeIf(new Predicate<ProductPriceBreak>() {
                @Override
                public boolean test(ProductPriceBreak productPriceBreak) {

                    return (product.getWeightPerUnit() == Product.WeightPerUnit.EACH ||
                            product.getWeightPerUnit() == Product.WeightPerUnit.FULL_GRAM ||
                            product.getWeightPerUnit() == Product.WeightPerUnit.EIGHTH
                            || product.getWeightPerUnit() == Product.WeightPerUnit.FOURTH
                            || product.getWeightPerUnit() == Product.WeightPerUnit.CUSTOM_GRAMS)
                            && productPriceBreak.getPriceBreakType() == ProductPriceBreak.PriceBreakType.HalfGramUnit;
                }
            });

            if (reset) {
                for (ProductPriceBreak productPriceBreak : priceBreaks) {
                    productPriceBreak.setPrice(new BigDecimal(0));
                    productPriceBreak.setActive(false);
                }
            }

            for (ProductPriceBreak.PriceBreakType type : priceBreakTypes) {
                if (type == ProductPriceBreak.PriceBreakType.None) {
                    continue;
                }

                if (type == ProductPriceBreak.PriceBreakType.HalfGramUnit) {
                    if (product.getWeightPerUnit() == Product.WeightPerUnit.FULL_GRAM
                            || product.getWeightPerUnit() == Product.WeightPerUnit.EACH ||
                            product.getWeightPerUnit() == Product.WeightPerUnit.EIGHTH
                            || product.getWeightPerUnit() == Product.WeightPerUnit.FOURTH
                            || product.getWeightPerUnit() == Product.WeightPerUnit.CUSTOM_GRAMS) {
                        continue;
                    }
                }

                // find the price break by type
                ProductPriceBreak priceBreak = null;
                for (ProductPriceBreak productPriceBreak : priceBreaks) {
                    if (productPriceBreak.getPriceBreakType() == type) {
                        priceBreak = productPriceBreak;
                        break;
                    }
                }
                // if none is found, create a new one
                if (priceBreak == null) {
                    // Add a new one
                    priceBreak = new ProductPriceBreak();
                    priceBreak.prepare(token.getCompanyId());
                    priceBreak.setPriceBreakType(type);
                    priceBreak.setActive(false);


                    // add price break to group
                    priceBreaks.add(priceBreak);
                }

                // specify name and quantity
                if (product.getWeightPerUnit() == Product.WeightPerUnit.EACH ||
                        product.getWeightPerUnit() == Product.WeightPerUnit.EIGHTH
                        || product.getWeightPerUnit() == Product.WeightPerUnit.FOURTH
                        || product.getWeightPerUnit() == Product.WeightPerUnit.CUSTOM_GRAMS) {
                    priceBreak.setName(type.eachName);
                    priceBreak.setQuantity(type.fullGramValue);
                } else if (product.getWeightPerUnit() == Product.WeightPerUnit.FULL_GRAM) {
                    priceBreak.setQuantity(type.fullGramValue);
                    priceBreak.setName(type.gramName);
                } else {
                    priceBreak.setName(type.gramName);
                    priceBreak.setQuantity(type.halfGramValue);
                }

                // default cost
                if (isDefault) {
                    if (product.getWeightPerUnit() == Product.WeightPerUnit.EACH
                            || product.getWeightPerUnit() == Product.WeightPerUnit.FULL_GRAM ||
                            product.getWeightPerUnit() == Product.WeightPerUnit.EIGHTH
                            || product.getWeightPerUnit() == Product.WeightPerUnit.FOURTH
                            || product.getWeightPerUnit() == Product.WeightPerUnit.CUSTOM_GRAMS) {
                        if (priceBreak.getPriceBreakType() == ProductPriceBreak.PriceBreakType.OneGramUnit) {
                            if (isPricingTemplate) {
                                product.setUnitPrice(priceBreak.getPrice() == null ? BigDecimal.ZERO : priceBreak.getPrice());
                            } else {
                                priceBreak.setPrice(product.getUnitPrice());
                            }
                            priceBreak.setActive(true);
                        }
                    } else {
                        if (priceBreak.getPriceBreakType() == ProductPriceBreak.PriceBreakType.HalfGramUnit) {
                            if (isPricingTemplate) {
                                product.setUnitPrice(priceBreak.getPrice() == null ? BigDecimal.ZERO : priceBreak.getPrice());
                            } else {
                                priceBreak.setPrice(product.getUnitPrice());
                            }
                            priceBreak.setActive(true);
                        }
                    }
                }

                if (reset) {
                    priceBreak.setPrice(product.getUnitPrice().multiply(new BigDecimal(priceBreak.getQuantity())));
                }
            }

            // Sort price breaks
            priceBreaks.sort(new Comparator<ProductPriceBreak>() {
                @Override
                public int compare(ProductPriceBreak o1, ProductPriceBreak o2) {
                    return ((Integer) o1.getQuantity()).compareTo((Integer) o2.getQuantity());
                }
            });
        }

        return priceBreaks;
    }

    @Override
    public MemberGroupPricesResult addMemberGroupPrices(String productId, MemberGroupPricesAddRequest addRequest) {
        MemberGroupPrices prices = groupPricesRepository.getPricesForProductGroup(
                token.getCompanyId(),
                token.getShopId(),
                productId,
                addRequest.getMemberGroupId(), MemberGroupPrices.class);
        if (prices != null) {
            throw new BlazeInvalidArgException("GroupPrices", "Group prices already exist for this product and member group combination.");
        }

        boolean exists = productRepository.exist(addRequest.getProductId());
        if (!exists) {
            throw new BlazeInvalidArgException("GroupPrices", "Product does not exist.");
        }

        MemberGroup memberGroup = memberGroupRepository.get(token.getCompanyId(), addRequest.getMemberGroupId());
        if (memberGroup == null) {
            throw new BlazeInvalidArgException("GroupPrices", "Unknown member group.");
        }

        MemberGroupPricesResult pricesResult = new MemberGroupPricesResult();
        pricesResult.prepare(token.getCompanyId());
        pricesResult.setShopId(token.getShopId());
        pricesResult.setMemberGroupId(addRequest.getMemberGroupId());
        pricesResult.setUnitPrice(addRequest.getUnitPrice());
        pricesResult.setProductId(productId);
        pricesResult.setPriceBreaks(addRequest.getPriceBreaks());
        pricesResult.setPriceRanges(addRequest.getPriceRanges());

        groupPricesRepository.save((MemberGroupPrices) pricesResult);

        pricesResult.setMemberGroup(memberGroup);
        realtimeService.sendRealTimeEvent(token.getShopId().toString(),
                RealtimeService.RealtimeEventType.ProductsUpdateEvent, null);
        return pricesResult;
    }

    @Override
    public MemberGroupPricesResult updatePrices(String productId, String pricesId, MemberGroupPrices groupPrices) {

        Product product = productRepository.get(token.getCompanyId(), productId);
        if (product == null) {
            throw new BlazeInvalidArgException("Product", "Product not found");
        }

        if (StringUtils.isBlank(groupPrices.getMemberGroupId())) {
            throw new BlazeInvalidArgException("MemberGroup", "Member Group Id does not exist.");
        }
        MemberGroup memberGroup = memberGroupRepository.get(token.getCompanyId(), groupPrices.getMemberGroupId());

        MemberGroupPricesResult dbPrices = groupPricesRepository.get(token.getCompanyId(), pricesId, MemberGroupPricesResult.class);

        if (memberGroup == null) {
            throw new BlazeInvalidArgException("MemberGroup", "MemberGroup does not exist.");
        }

        if (dbPrices != null) {
            dbPrices.setUnitPrice(groupPrices.getUnitPrice());
            dbPrices.setPriceBreaks(groupPrices.getPriceBreaks());
            dbPrices.setPriceRanges(groupPrices.getPriceRanges());
            dbPrices.setEnabled(groupPrices.isEnabled());
            dbPrices.setPricingTemplateId(groupPrices.getPricingTemplateId());

            if (StringUtils.isNotBlank(dbPrices.getPricingTemplateId())) {
                this.assignPricingTemplate(product, dbPrices.getPricingTemplateId(), dbPrices);
            }

            groupPricesRepository.update(token.getCompanyId(), dbPrices.getId(), (MemberGroupPrices) dbPrices);
        } else {
            dbPrices = new MemberGroupPricesResult();
            dbPrices.setId(groupPrices.getId());
            dbPrices.setCreated(groupPrices.getCreated());
            dbPrices.setModified(groupPrices.getModified());
            dbPrices.prepare(token.getCompanyId());
            dbPrices.setShopId(token.getShopId());
            dbPrices.setMemberGroupId(memberGroup.getId());
            dbPrices.setProductId(productId);
            dbPrices.setPricingTemplateId(groupPrices.getPricingTemplateId());

            if (StringUtils.isNotBlank(dbPrices.getPricingTemplateId())) {
                this.assignPricingTemplate(product, dbPrices.getPricingTemplateId(), dbPrices);
            }

            dbPrices.setEnabled(groupPrices.isEnabled());
            groupPricesRepository.save(groupPrices);
        }


        dbPrices.setMemberGroup(memberGroup);

        realtimeService.sendRealTimeEvent(token.getShopId().toString(),
                RealtimeService.RealtimeEventType.ProductsUpdateEvent, null);
        return dbPrices;
    }

    /**
     * This method assign price range/break template
     *
     * @param product           : product object
     * @param pricingTemplateId : pricing template id
     * @param dbPrices          : MemberGroupPrices object
     */
    private void assignPricingTemplate(Product product, String pricingTemplateId, MemberGroupPricesResult dbPrices) {
        PricingTemplate template = pricingTemplateRepository.get(token.getCompanyId(), pricingTemplateId);
        ProductCategory productCategory = productCategoryRepository.get(token.getCompanyId(), product.getCategoryId());

        if (template == null) {
            throw new BlazeInvalidArgException("Pricing template", "Pricing template not found");
        }
        if (!template.isActive()) {
            throw new BlazeInvalidArgException("Pricing Template", "Pricing template is not active.");
        }

        if (!template.getUnitType().equals(productCategory.getUnitType())) {
            throw new BlazeInvalidArgException("Product", "Product category's unit type does not match with pricing template");
        }

        if (!CollectionUtils.isNullOrEmpty(template.getPriceBreaks())) {
            template.getPriceBreaks().removeIf(priceBreak -> ProductPriceBreak.PriceBreakType.None == priceBreak.getPriceBreakType());
        }

        dbPrices.setPriceBreaks(template.getPriceBreaks());
        dbPrices.setPriceRanges(template.getPriceRanges());

        //As price range id was random while creation so updating here with product id + tolerance id
        if (!CollectionUtils.isNullOrEmpty(dbPrices.getPriceRanges())) {
            for (ProductPriceRange priceRange : dbPrices.getPriceRanges()) {
                priceRange.setId(product.getId() + "_" + priceRange.getWeightToleranceId());
            }
        }

        if (!CollectionUtils.isNullOrEmpty(dbPrices.getPriceBreaks())) {
            populatePriceBreaks(product, productCategory, dbPrices.getPriceBreaks(), Boolean.TRUE, null, Boolean.FALSE, Boolean.FALSE);
        }

    }

    @Override
    public void deleteMemberGroupPrices(String productId, String groupPricesId) {
        groupPricesRepository.removeById(token.getCompanyId(), groupPricesId);
    }

    /**
     * This method gets products with custom result on basis of fields pro
     *
     * @param shopId     : shop id
     * @param categoryId : category id
     * @param start      : start
     * @param limit      : limit
     * @param term       : search term
     */
    @Override
    public SearchResult<ProductCustomResult> searchAllProducts(String shopId, String categoryId, int start, int limit, String term, FilterType status) {
        SearchResult<ProductCustomResult> productSearchResult = new SearchResult<>();
        if (limit <= 0 || limit > PRODUCT_RESULT_LIMIT)
            limit = PRODUCT_RESULT_LIMIT;

        if (StringUtils.isBlank(shopId)) {
            shopId = token.getShopId();
        }

        if (!token.getShopId().equalsIgnoreCase(shopId)) {
            ProductCategory currentCategory = productCategoryRepository.get(token.getCompanyId(), categoryId);
            if (currentCategory != null) {
                ProductCategory category = productCategoryRepository.getCategory(token.getCompanyId(), shopId, currentCategory.getName());
                if (category != null) {
                    categoryId = category.getId();
                }
            }
        }
        SearchResult<Product> searchResult;
        if (StringUtils.isNoneEmpty(term)) {
            if (status == null || status == FilterType.All) {
                searchResult = productRepository.findProductsByPattern(token.getCompanyId(), shopId, "{name:1}", term, start, limit, Product.class);
            } else {
                searchResult = productRepository.findProductsByPatternAndState(token.getCompanyId(), shopId, "{name:1}", term, start, limit, Product.class, (status == FilterType.Active));
            }
        } else if (StringUtils.isNotEmpty(categoryId)) {
            if (status == null || status == FilterType.All) {
                searchResult = productRepository.findProductsByCategoryId(token.getCompanyId(), shopId, categoryId, "{name:1}", start, limit, Product.class);
            } else {
                searchResult = productRepository.findProductsByCategoryIdAndState(token.getCompanyId(), shopId, categoryId, "{name:1}", start, limit, Product.class, (status == FilterType.Active));
            }
        } else {
            if (status == null || status == FilterType.All) {
                searchResult = productRepository.findItems(token.getCompanyId(), shopId, "{name:1}", start, limit, Product.class);
            } else {
                searchResult = productRepository.findItemsByState(token.getCompanyId(), shopId, "{name:1}", start, limit, Product.class, (status == FilterType.Active));
            }
        }

        final HashMap<String, Inventory> inventoryHashMap = inventoryRepository.listAllAsMap(token.getCompanyId(), token.getShopId());

        if (searchResult != null && searchResult.getValues() != null) {
            prepareCustomProductResult(searchResult, productSearchResult, shopId, inventoryHashMap);
        }

        return productSearchResult;
    }

    public void prepareCustomProductResult(SearchResult<Product> searchResult, SearchResult<ProductCustomResult> productSearchResult, String shopId, HashMap<String, Inventory> inventoryHashMap) {
        productSearchResult.setLimit(searchResult.getLimit());
        productSearchResult.setSkip(searchResult.getSkip());
        productSearchResult.setTotal(searchResult.getTotal());

        HashMap<String, Vendor> vendorHashMap = vendorRepository.listAllAsMap(token.getCompanyId());
        HashMap<String, ProductCategory> productCategoryMap = productCategoryRepository.listAllAsMap(token.getCompanyId(), shopId);
        HashMap<String, Brand> brandHashMap = brandRepository.listAsMap(token.getCompanyId());

        List<ProductCustomResult> customResults = new ArrayList<>();
        ProductCustomResult customResult;
        for (Product product : searchResult.getValues()) {
            customResult = new ProductCustomResult();

            customResult.setId(product.getId());
            customResult.setName(product.getName());
            customResult.setAssets(product.getAssets());
            customResult.setUnitPrice(product.getUnitPrice());
            customResult.setFlowerType(product.getFlowerType());
            customResult.setActive(product.isActive());
            customResult.setSku(product.getSku());
            customResult.setShowCostInWidget(product.isShowInWidget());

            BigDecimal total = new BigDecimal(0);
            for (ProductQuantity pq : product.getQuantities()) {
                if (inventoryHashMap.containsKey(pq.getInventoryId()) && inventoryHashMap.get(pq.getInventoryId()).isActive())
                    total = total.add(pq.getQuantity());
            }
            customResult.setQuantityAvailable(total);
            ProductCategory productCategory = productCategoryMap.get(product.getCategoryId());
            if (productCategory != null) {
                customResult.setUnitType(productCategory.getUnitType());
                customResult.setProductCategoryPhoto(productCategory.getPhoto());
            }

            Vendor vendor = vendorHashMap.get(product.getVendorId());
            if (vendor != null) {
                customResult.setVendorName(vendor.getName());
                if (Vendor.ArmsLengthType.NON_ARMS_LENGTH.equals(vendor.getArmsLengthType())) {
                    customResult.setArmsLengthType("Non-Arms Length");
                } else {
                    customResult.setArmsLengthType("Arms Length");
                }
            }

            Brand brand = brandHashMap.get(product.getBrandId());
            customResult.setBrandName((brand == null) ? "" : brand.getName());

            customResults.add(customResult);
        }

        productSearchResult.setValues(customResults);
    }

    @Override
    public void bulkProductUpdates(ProductBulkUpdateRequest request) {
        List<String> productIds = request.getProductIds();
        if (productIds == null || productIds.isEmpty()) {
            throw new BlazeInvalidArgException(BULK_PRODUCT_UPDATE, "Please select products");
        }

        if (request.getOperationType() == null) {
            throw new BlazeInvalidArgException(BULK_PRODUCT_UPDATE, "Please selection type of action");
        }

        List<ObjectId> productObjectIdList = new ArrayList<>();
        for (String productId : productIds) {
            productObjectIdList.add(new ObjectId(productId));
        }
        HashMap<String, Product> productMap = null;
        boolean isWeedMapSync = false;
        switch (request.getOperationType()) {
            case STATUS:
                if (!request.isActive()) {
                    checkProductUseInTransaction(productIds, "deactivate");
                }
                productRepository.bulkUpdateStatus(token.getCompanyId(), token.getShopId(), productObjectIdList, request.isActive());
                isWeedMapSync = !request.isActive();
                break;
            case COPY_PRODUCTS:
                if (request.getProductIds().isEmpty())
                    throw new BlazeInvalidArgException(BULK_PRODUCT_UPDATE, "Please select products to copy");
                else if (StringUtils.isBlank(request.getToShopId()))
                    throw new BlazeInvalidArgException(BULK_PRODUCT_UPDATE, "Please select a shop to copy products");

                CopyProductRequest copyProductRequest = new CopyProductRequest();
                copyProductRequest.setToShopId(request.getToShopId());
                copyProductRequest.setProductIds(request.getProductIds());
                this.copyProductsToShop(copyProductRequest);
                break;
            case VENDOR:
                if (StringUtils.isBlank(request.getVendorId()))
                    throw new BlazeInvalidArgException(BULK_PRODUCT_UPDATE, "Please select a vendor");

                productRepository.bulkUpdateVendor(token.getCompanyId(), token.getShopId(), productObjectIdList, request.getVendorId());
                break;
            case MIX_MATCH:
                productRepository.bulkUpdateMixMatch(token.getCompanyId(), token.getShopId(), productObjectIdList, request.isEnableMixMatch());
                break;
            case DELETE_PRODUCTS:
                if (request.getProductIds().isEmpty())
                    throw new BlazeInvalidArgException(BULK_PRODUCT_UPDATE, "Please select products to delete");

                checkProductUseInTransaction(productIds, "delete");
                checkProductUseInPurchaseOrder(productIds, "delete");

                productMap = productRepository.findProductsByIdsAsMap(token.getCompanyId(), token.getShopId(), productObjectIdList);

                checkAvailableInventory(productMap);
                productRepository.bulkDeleteProduct(token.getCompanyId(), token.getShopId(), productObjectIdList);
                //Delete bar code
                barcodeService.deleteBarCodesForProducts(token.getCompanyId(), token.getShopId(), productIds);
                isWeedMapSync = true;
                break;
            case WEEDMAP_STATUS:
                productRepository.bulkUpdateWeedMapStatus(token.getCompanyId(), token.getShopId(), productObjectIdList, request.isEnableWeedmap());
                isWeedMapSync = !request.isEnableWeedmap();
                break;
            case TAX:
                if (request.getTaxType().equals(TaxInfo.TaxType.Custom) && Objects.isNull(request.getCustomTaxInfo()))
                    throw new BlazeInvalidArgException(BULK_PRODUCT_UPDATE, "Please enter custom tax information");

                productRepository.bulkUpdateTaxInfo(token.getCompanyId(), token.getShopId(), productObjectIdList, request.getTaxType(), request.getTaxOrder(), request.getCustomTaxInfo());
                break;
            case LOW_INVENTORY_NOTIFICATION:
                productRepository.bulkUpdateLowInventoryNotification(token.getCompanyId(), token.getShopId(), productObjectIdList, request.isLowInventoryNotification(), request.getLowThreshold());
                break;
            case PRICE:

                //If product category type is in gram then update price range otherwise update unit price
                if (request.isCategoryInGrams()) {
                    productMap = productRepository.listAsMap(token.getCompanyId(), productObjectIdList);
                    HashMap<String, ProductCategory> productCategoryMap = productCategoryRepository.listAsMap(token.getCompanyId(), token.getShopId());

                    HashMap<String, ProductWeightTolerance> activeTolerancesMap = productWeightToleranceRepository.getActiveTolerancesAsMap(token.getCompanyId());
                    for (String productId : productIds) {
                        Product product = productMap.get(productId);

                        if (product == null) {
                            throw new BlazeInvalidArgException("Product", "Product not found");
                        }

                        ProductCategory category = productCategoryMap.get(product.getCategoryId());
                        if (category == null) {
                            throw new BlazeInvalidArgException("Category", "Category does not exist.");
                        }

                        if (product != null && !request.getProductBulkUpdatePriceWeightPrice().isEmpty()) {

                            List<ProductBulkUpdatePriceWeightPrice> bulkUpdatePrice = request.getProductBulkUpdatePriceWeightPrice();
                            //Update pricing
                            if (category.getUnitType() == ProductCategory.UnitType.grams) {
                                // Recreate the tolerance
                                List<ProductPriceRange> productPriceRanges = new ArrayList<>();
                                for (ProductBulkUpdatePriceWeightPrice priceWeightPrice : bulkUpdatePrice) {
                                    ProductWeightTolerance tolerance = activeTolerancesMap.get(priceWeightPrice.getWeightToleranceId());
                                    if (tolerance != null) {
                                        ProductPriceRange priceRange = new ProductPriceRange();
                                        productPriceRanges.add(priceRange);

                                        priceRange.setWeightToleranceId(tolerance.getId());
                                        priceRange.setWeightTolerance(tolerance);
                                        priceRange.setPriority(tolerance.getPriority());
                                        priceRange.setId(product.getId() + "_" + priceRange.getWeightToleranceId());
                                        priceRange.setPrice(priceWeightPrice.getPrice());
                                    }
                                }
                                // sort the ranges
                                productPriceRanges.sort(new Comparator<ProductPriceRange>() {
                                    @Override
                                    public int compare(ProductPriceRange o1, ProductPriceRange o2) {
                                        return ((Integer) o1.getPriority()).compareTo(o2.getPriority());
                                    }
                                });

                                product.setPriceRanges(productPriceRanges);
                                product.setPriceBreaks(new ArrayList<ProductPriceBreak>());
                                productRepository.updateProductPriceRanges(token.getCompanyId(), product.getId(), productPriceRanges);
                            }
                        }
                    }
                } else {

                    productRepository.bulkUpdateUnitPrice(token.getCompanyId(), token.getShopId(), productObjectIdList, request.getUnitPrice());
                    Iterable<Product> products = productRepository.list(token.getCompanyId(), productObjectIdList);
                    HashMap<String, ProductCategory> productCategoryMap = productCategoryRepository.listAsMap(token.getCompanyId(), token.getShopId());

                    for (Product product : products) {
                        ProductCategory category = productCategoryMap.get(product.getCategoryId());
                        if (category == null) {
                            throw new BlazeInvalidArgException("Category", "Category does not exist.");
                        }

                        if (product != null) {
                            // Re-generate the list of price breaks if possible
                            List<ProductPriceBreak> productPriceBreaks = populatePriceBreaks(product, category, product.getPriceBreaks(), true, null, false, Boolean.FALSE);
                            productRepository.updateProductPriceBreaks(token.getCompanyId(), product.getId(), productPriceBreaks);
                        }

                    }
                }
                break;
            case UPDATE_CATEGORY:
                if (request.getCategoryId().isEmpty()) {
                    throw new BlazeInvalidArgException("Update Category", "Please select category to update");
                }
                ProductCategory category = productCategoryRepository.get(token.getCompanyId(), request.getCategoryId());
                if (category == null) {
                    throw new BlazeInvalidArgException("Update Category", "Please choose a valid category for the shop.");
                }

                productMap = productRepository.findProductsByIdsAsMap(token.getCompanyId(), token.getShopId(), productObjectIdList);

                productRepository.bulkUpdateCategory(token.getCompanyId(), token.getShopId(), productObjectIdList, category);
                isWeedMapSync = true;
                break;
            case PRODUCT_SALE_TYPE:
                if (request.getProductSaleType() == null)
                    throw new BlazeInvalidArgException("Product Sale Type", "Product sale type is not found");
                productRepository.bulkOperationType(token.getCompanyId(), token.getShopId(), productObjectIdList, request.getProductSaleType());
                break;
            case UPDATE_TAG:
                if (request.getTags() == null || request.getTags().isEmpty()) {
                    throw new BlazeInvalidArgException("Update Product Tag", "Please enter product tags");
                }
                productRepository.bulkUpdateProductTag(token.getCompanyId(), token.getShopId(), productObjectIdList, request.getTags());
                break;
            case UPDATE_COMPLEX_TAX:
                List<CompoundTaxTable> taxTableList = request.getCompoundTaxTables();
                if (taxTableList == null || request.getCompoundTaxTables().size() == 0) {
                    throw new BlazeInvalidArgException("Update Compound tax", "Please enter compound taxes");
                }

                for (String productId : productIds) {
                    for (CompoundTaxTable taxTable : taxTableList) {
                        taxTable.resetPrepare(token.getCompanyId());
                        taxTable.setShopId(token.getShopId());
                        if (taxTable.getTaxType() != TaxInfo.TaxType.Exempt) {
                            taxTable.setTaxType(TaxInfo.TaxType.Custom);

                            if (taxTable.getCityTax() != null) {
                                taxTable.getCityTax().resetPrepare(token.getCompanyId());
                                taxTable.getCityTax().setShopId(token.getShopId());
                            }

                            if (taxTable.getStateTax() != null) {
                                taxTable.getCountyTax().resetPrepare(token.getCompanyId());
                                taxTable.getCountyTax().setShopId(token.getShopId());
                            }

                            if (taxTable.getCountyTax() != null) {
                                taxTable.getStateTax().resetPrepare(token.getCompanyId());
                                taxTable.getStateTax().setShopId(token.getShopId());
                            }
                            if (taxTable.getFederalTax() != null) {
                                taxTable.getFederalTax().resetPrepare(token.getCompanyId());
                                taxTable.getFederalTax().setShopId(token.getShopId());
                            }
                        }
                    }

                }
                // set default tax type to request taxType.
                productRepository.bulkUpdateProductTaxtable(token.getCompanyId(), token.getShopId(), productObjectIdList, taxTableList, request.getTaxType());

                break;
            case CANNABIS_TYPE:
                productRepository.bulkUpdateCannabisType(token.getCompanyId(), token.getShopId(), productObjectIdList, request.getCannabisType());
                break;
            case ENABLE_EXCISE_TAX:
                productRepository.bulkUpdatePriceIncludesExcise(token.getCompanyId(), token.getShopId(), productObjectIdList, request.getPriceIncludesExcise());
                break;
            case SHOW_IN_WIDGET:
                productRepository.bulkUpdateShowInWidget(token.getCompanyId(), token.getShopId(), productObjectIdList, request.isShowInWidget());
                break;
            case UPDATE_BRAND:
                if (StringUtils.isNotBlank(request.getBrandId())) {
                    Brand brand = brandRepository.getById(request.getBrandId());
                    if (brand == null) {
                        throw new BlazeInvalidArgException(BULK_PRODUCT_UPDATE, "Brand does not found");
                    }
                }
                productRepository.bulkUpdateBrandInProduct(token.getCompanyId(), token.getShopId(), productObjectIdList, request.getBrandId());
                break;
            case FLOWER_TYPE:
                if (StringUtils.isBlank(request.getFlowerType())) {
                    throw new BlazeInvalidArgException(BULK_PRODUCT_UPDATE, "Flower type does not found");
                }
                productRepository.bulkUpdateFlowerTypeInProduct(token.getCompanyId(), token.getShopId(), productObjectIdList, request.getFlowerType());
                break;
            case WEIGHT_PER_UNIT:
                if (request.getWeightPerUnit() == Product.WeightPerUnit.CUSTOM_GRAMS && request.getCustomWeight().doubleValue() <= 0) {
                    throw new BlazeInvalidArgException(BULK_PRODUCT_UPDATE, "Custom weight cannot be 0.");
                }
                productRepository.bulkUpdateWeightPerUnit(token.getCompanyId(), token.getShopId(), productObjectIdList, request.getWeightPerUnit(), request.getCustomWeight(), request.getCustomGramType());
                break;
            case PRICING_TEMPLATE:
                if (StringUtils.isBlank(request.getPricingTemplateId())) {
                    throw new BlazeInvalidArgException(BULK_PRODUCT_UPDATE, PRICING_TEMPLATE_NOT_FOUND);
                }

                PricingTemplate pricingTemplate = pricingTemplateRepository.get(token.getCompanyId(), request.getPricingTemplateId());
                if (pricingTemplate == null || !pricingTemplate.isActive()) {
                    throw new BlazeInvalidArgException(BULK_PRODUCT_UPDATE, PRICING_TEMPLATE_NOT_FOUND);
                }

                productMap = productRepository.listAsMap(token.getCompanyId(), productObjectIdList);
                HashMap<String, ProductCategory> productCategoryMap = productCategoryRepository.listAsMap(token.getCompanyId(), token.getShopId());

                for (Map.Entry<String, Product> entry : productMap.entrySet()) {
                    Product product = entry.getValue();
                    ProductCategory productCategory = productCategoryMap.get(product.getCategoryId());
                    if (productCategory == null) {
                        throw new BlazeInvalidArgException("Product Category", "Invalid category");
                    }

                    if (!pricingTemplate.getUnitType().equals(productCategory.getUnitType())) {
                        throw new BlazeInvalidArgException(BULK_PRODUCT_UPDATE, "Product category's unit type does not match with pricing template");
                    }
                    if (pricingTemplate.getUnitType() == ProductCategory.UnitType.units
                            && pricingTemplate.getWeightPerUnit() != product.getWeightPerUnit()) {
                        throw new BlazeInvalidArgException(BULK_PRODUCT_UPDATE, "Product's weight per unit does not match with template weight per unit");
                    }
                }

                for (Map.Entry<String, Product> entry : productMap.entrySet()) {
                    Product product = entry.getValue();
                    ProductCategory productCategory = productCategoryMap.get(product.getCategoryId());
                    if (productCategory == null) {
                        continue;
                    }

                    this.assignPricingTemplateToProduct(product, pricingTemplate, productCategory);

                    BigDecimal unitPrice = product.getUnitPrice();
                    if (unitPrice == null) {
                        unitPrice = BigDecimal.ZERO;
                    }
                    productRepository.updateProductPricing(token.getCompanyId(), product.getId(), product.getPriceRanges(), product.getPriceBreaks(), pricingTemplate.getId(), unitPrice);
                }

                break;
        }
        callWebHookForBulkProduct(productObjectIdList);

        /*if (isWeedMapSync) {
            if (request.getOperationType() != ProductBulkUpdateRequest.ProductsUpdateOperationType.UPDATE_CATEGORY) {
                productMap = productRepository.findProductsByIdsAsMap(token.getCompanyId(), token.getShopId(), productObjectIdList);
            }
            Set<String> categoryIds = new HashSet<>();
            for (Product product : productMap.values()) {
                categoryIds.add(product.getCategoryId());
            }
            //weedmapService.removeProductFromWeedMap(Lists.newArrayList(categoryIds), false, Lists.newArrayList(productMap.values()));
        }*/
        auditLogService.addAuditLog(this.token, "/api/v1/mgmt/product/bulkupdate", "Management - Products", String.format("Product - Bulk: %s", request.getOperationType()));

    }

    private void checkAvailableInventory(HashMap<String, Product> productMap) {

        for (String productId : productMap.keySet()) {
            Product product = productMap.get(productId);
            if (product == null) {
                continue;
            }

            for (ProductQuantity quantity : product.getQuantities()) {
                if (quantity.getQuantity().doubleValue() > 0) {
                    throw new BlazeInvalidArgException(PRODUCT, CAN_NOT_DELETE_QUANTITY_AVAILABLE);
                }
            }
        }
    }

    @Override
    public Product createProductFromExisting(CreateProductFromExisting request) {

        Product oldProduct = productRepository.get(token.getCompanyId(), request.getProductId());
        if (oldProduct == null) {
            throw new BlazeInvalidArgException("Product", "Product does not found");
        }

        long result = 0;
        if (StringUtils.isNotBlank(request.getName())) {
            result = productRepository.getProductsByName(token.getCompanyId(), token.getShopId(), request.getName(), oldProduct.getVendorId(), oldProduct.getCategoryId(), oldProduct.getFlowerType(), oldProduct.getCannabisType());
        }
        if (result > 0) {
            throw new BlazeInvalidArgException(PRODUCT, PRODUCT_NAME_EXISTS);
        }

        Product product = new Product();

        product.prepare(token.getCompanyId());
        product.setShopId(token.getShopId());

        product.setCategoryId(oldProduct.getCategoryId());

        BarcodeItem item = barcodeService.createBarcodeItemIfNeeded(product.getCompanyId(), product.getShopId(), product.getId(),
                BarcodeItem.BarcodeEntityType.Product, product.getId(), product.getSku(), null, false);
        if (item != null) {
            product.setSku(item.getBarcode());
        }

        product.setVendorId(oldProduct.getVendorId());
        product.setProductSaleType(oldProduct.getProductSaleType());
        product.setName(request.getName());
        product.setDescription(oldProduct.getDescription());
        product.setFlowerType(oldProduct.getFlowerType());
        product.setUnitPrice(oldProduct.getUnitPrice());
        product.setWeightPerUnit(oldProduct.getWeightPerUnit());
        product.setThc(oldProduct.getThc());
        product.setCbd(oldProduct.getCbd());
        product.setCbda(oldProduct.getCbda());
        product.setCbn(oldProduct.getCbn());
        product.setThca(oldProduct.getThca());
        product.setActive(oldProduct.isActive());
        product.setGenetics(oldProduct.getGenetics());
        product.setPotencyAmount(oldProduct.getPotencyAmount());
        product.setPotency(oldProduct.isPotency());

        List<CompanyAsset> assets = oldProduct.getAssets();
        if (assets != null) {
            for (CompanyAsset asset : assets) {
                asset.prepare(token.getCompanyId());
            }
        }

        product.setAssets(assets);
        product.setCategory(oldProduct.getCategory());
        product.setEnableMixMatch(oldProduct.isEnableMixMatch());
        product.setEnableWeedmap(oldProduct.isEnableWeedmap());
        product.setShowInWidget(oldProduct.isShowInWidget());
        product.setTaxType(oldProduct.getTaxType());
        product.setTaxOrder(oldProduct.getTaxOrder());

        TaxInfo customTaxInfo = oldProduct.getCustomTaxInfo();
        if (customTaxInfo != null)
            customTaxInfo.prepare();

        product.setCustomTaxInfo(customTaxInfo);
        product.setDiscountable(oldProduct.isDiscountable());
        product.setLowThreshold(oldProduct.getLowThreshold());
        product.setLowInventoryNotification(oldProduct.isLowInventoryNotification());
        product.setMedicinal(oldProduct.isMedicinal());
        product.setByGram(oldProduct.isByGram());
        product.setByPrepackage(oldProduct.isByPrepackage());
        product.setTaxTables(oldProduct.getTaxTables());
        product.setTags(oldProduct.getTags());
        product.setCustomTaxInfo(oldProduct.getCustomTaxInfo());
        product.setEnableExciseTax(oldProduct.isEnableExciseTax());
        product.setCannabisType(oldProduct.getCannabisType());
        product.setPriceIncludesExcise(oldProduct.isPriceIncludesExcise());
        product.setPriceIncludesALExcise(oldProduct.isPriceIncludesALExcise());
        product.setAutomaticReOrder(oldProduct.isAutomaticReOrder());
        product.setReOrderLevel(oldProduct.getReOrderLevel());
        product.setBrandId(oldProduct.getBrandId());

        List<MedicalCondition> medicalConditions = oldProduct.getMedicalConditions();
        if (medicalConditions != null) {
            for (MedicalCondition medicalCondition : medicalConditions) {
                medicalCondition.prepare();
            }
        }

        product.setMedicalConditions(medicalConditions);

        ProductCategory category = productCategoryRepository.get(token.getCompanyId(), product.getCategoryId());

        if (category.getUnitType() == ProductCategory.UnitType.grams) {
            List<ProductWeightTolerance> tolerances = this.getWeightTolerances();
            // Recreate the tolerance
            List<ProductPriceRange> productPriceRanges = new ArrayList<>();
            for (ProductWeightTolerance tolerance : tolerances) {
                if (tolerance.isEnabled()) {
                    ProductPriceRange priceRange = new ProductPriceRange();
                    productPriceRanges.add(priceRange);

                    priceRange.setWeightToleranceId(tolerance.getId());
                    priceRange.setWeightTolerance(tolerance);
                    priceRange.setPriority(tolerance.getPriority());
                    priceRange.setId(product.getId() + "_" + priceRange.getWeightToleranceId());
                }
            }


            int priority = 0;
            for (ProductPriceRange productPriceRange : oldProduct.getPriceRanges()) {
                for (ProductPriceRange newPriceRange : productPriceRanges) {
                    if (newPriceRange.getWeightToleranceId().equalsIgnoreCase(productPriceRange.getWeightToleranceId())) {
                        newPriceRange.setPrice(productPriceRange.getPrice());
                        break;
                    }
                }
                priority++;
            }

            product.setPriceRanges(productPriceRanges);
            product.setPriceBreaks(new ArrayList<ProductPriceBreak>());
        } else {
            // This is the unit category, use price breaks instead of price ranges
            if (oldProduct.getPriceBreaks() != null) {
                for (ProductPriceBreak priceBreak : oldProduct.getPriceBreaks()) {
                    priceBreak.resetPrepare(token.getCompanyId());
                    if (oldProduct.getWeightPerUnit() == Product.WeightPerUnit.EACH
                            || oldProduct.getWeightPerUnit() == Product.WeightPerUnit.FULL_GRAM
                            || oldProduct.getWeightPerUnit() == Product.WeightPerUnit.EIGHTH
                            || oldProduct.getWeightPerUnit() == Product.WeightPerUnit.FOURTH) {
                        if (priceBreak.getPriceBreakType() == ProductPriceBreak.PriceBreakType.OneGramUnit) {
                            priceBreak.setActive(true);
                        }
                    } else {
                        if (priceBreak.getPriceBreakType() == ProductPriceBreak.PriceBreakType.HalfGramUnit) {
                            priceBreak.setActive(true);
                        }
                    }
                }
            }
            populatePriceBreaks(product, category, oldProduct.getPriceBreaks(), true, null, false, Boolean.FALSE);

            product.setPriceBreaks(oldProduct.getPriceBreaks());
            product.setPriceRanges(new ArrayList<ProductPriceRange>());
        }

        product.setCustomWeight(oldProduct.getCustomWeight());
        product.setCustomGramType(oldProduct.getCustomGramType());
        product.setSecondaryVendors(oldProduct.getSecondaryVendors());
        product.setWeightPerUnit(oldProduct.getWeightPerUnit());
        product.setToleranceId(oldProduct.getToleranceId());

        product.setProducerMfg(oldProduct.getProducerMfg());
        product.setProducerLicense(oldProduct.getProducerLicense());
        product.setProducerAddress(oldProduct.getProducerAddress());
        if (product.getProducerAddress() != null) {
            product.getProducerAddress().prepare(token.getCompanyId());
        }
        product.setSecondaryVendors(oldProduct.getSecondaryVendors());

        elasticSearchManager.createOrUpdateIndexedDocument(new ProductCustomResult(product));
        callWebHook(product, true);

        return productRepository.save(product);
    }

    @Override
    public ProductQuantityInfo getQuantityInfo(String productId) {
        Product dbProduct = productRepository.get(token.getCompanyId(), productId);
        if (dbProduct == null) {
            throw new BlazeInvalidArgException("Product", "Product not found");
        }

        ProductQuantityInfo productQuantityInfo = new ProductQuantityInfo();

        productQuantityInfo.setProductId(dbProduct.getId());
        if (dbProduct.getQuantities().size() > 0) {
            productQuantityInfo.setCurrentQuantity(dbProduct.getQuantities());
        }

        SearchResult<Prepackage> prepackages = prepackageRepository.getPrepackages(token.getCompanyId(), token.getShopId(), productId);
        List<Prepackage> prepackageList = prepackages.getValues();

        List<String> prepackageIds = new ArrayList<>();
        for (Prepackage prepackage : prepackageList) {
            prepackageIds.add(prepackage.getId());
        }

        Iterable<ProductPrepackageQuantity> quantitiesForPrepackage = productPrepackageQuantityRepository.getQuantitiesWithProductAndPrepackage(token.getCompanyId(), token.getShopId(), prepackageIds, productId);
        ArrayList<ProductPrepackageQuantity> productPrepackageQuantities = Lists.newArrayList(quantitiesForPrepackage);


        List<ProductQuantity> transferQuantity = new ArrayList<>();
        List<ProductPrepackageQuantityResult> prepackageTransferQuantity = new ArrayList<>();


        List<InventoryTransferHistory.TransferStatus> transferStatusList = new ArrayList<>();
        if (!transferStatusList.contains(InventoryTransferHistory.TransferStatus.PENDING)) {
            transferStatusList.add(InventoryTransferHistory.TransferStatus.PENDING);
        }

        SearchResult<InventoryTransferHistory> itemByStatus = inventoryTransferHistoryRepository.findItemByStatus(token.getCompanyId(), token.getShopId(), "{modified : -1}", 0, Integer.MAX_VALUE, transferStatusList, false, InventoryTransferHistory.class);
        Iterable<PrepackageProductItem> prepackagesForPrepackagesAndProduct = prepackageProductItemRepository.getPrepackagesForPrepackagesAndProduct(token.getCompanyId(), token.getShopId(), prepackageIds, productId);

        if (itemByStatus != null) {
            List<InventoryTransferHistory> inventoryTransferHistories = itemByStatus.getValues();
            for (InventoryTransferHistory inventoryTransferHistory : inventoryTransferHistories) {
                if (inventoryTransferHistory != null) {
                    LinkedHashSet<InventoryTransferLog> transferLogs = inventoryTransferHistory.getTransferLogs();
                    for (InventoryTransferLog transferLog : transferLogs) {
                        if (transferLog != null) {
                            if (transferLog.getProductId().equalsIgnoreCase(productId) && StringUtils.isBlank(transferLog.getPrepackageItemId())) {
                                ProductQuantity productQuantity = new ProductQuantity();
                                productQuantity.setInventoryId(inventoryTransferHistory.getFromInventoryId());
                                productQuantity.setQuantity(transferLog.getTransferAmount());
                                transferQuantity.add(productQuantity);
                            } else {
                                for (PrepackageProductItem prepackageProductItem : prepackagesForPrepackagesAndProduct) {
                                    if (StringUtils.isNotBlank(transferLog.getPrepackageItemId()) && transferLog.getPrepackageItemId().equalsIgnoreCase(prepackageProductItem.getId())) {

                                        ProductPrepackageQuantityResult productPrepackageQuantityResult = new ProductPrepackageQuantityResult();
                                        productPrepackageQuantityResult.setInventoryId(inventoryTransferHistory.getFromInventoryId());
                                        productPrepackageQuantityResult.setQuantity(transferLog.getTransferAmount());
                                        for (Prepackage prepackage : prepackageList) {
                                            if (prepackage.getId().equalsIgnoreCase(prepackageProductItem.getPrepackageId())) {
                                                productPrepackageQuantityResult.setName(prepackage.getName());
                                            }
                                        }
                                        prepackageTransferQuantity.add(productPrepackageQuantityResult);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        List<HoldQuantityInfo> holdQuantity = new ArrayList<>();
        List<ProductPrepackageQuantityResult> prepackageHoldQuantity = new ArrayList<>();

        Iterable<Transaction> transactionByStatus = transactionRepository.getAllActiveTransactions(token.getCompanyId(), token.getShopId());

        for (Transaction transaction : transactionByStatus) {
            if (transaction != null && transaction.getCart() != null && transaction.getCart().getItems() != null) {

                List<OrderItem> items = transaction.getCart().getItems();
                for (OrderItem item : items) {
                    if (item.getQuantityLogs() == null) {
                        continue;
                    }

                    // if not targeted product, continue
                    if (!item.getProductId().equalsIgnoreCase(productId)) {
                        continue;
                    }
                    HoldQuantityInfo productQuantity = new HoldQuantityInfo();
                    ProductPrepackageQuantityResult productPrepackageQuantityHoldResult = new ProductPrepackageQuantityResult();

                    for (QuantityLog changeLog : item.getQuantityLogs()) {

                        // blank prepackage item
                        if (StringUtils.isBlank(changeLog.getPrepackageItemId())) {
                            BigDecimal qty = productQuantity.getQuantity().add(changeLog.getQuantity());
                            productQuantity.setInventoryId(changeLog.getInventoryId());
                            productQuantity.setQuantity(qty);
                            productQuantity.setTransactionNo(transaction.getTransNo());
                        } else {
                            if (StringUtils.isNotBlank(changeLog.getPrepackageItemId())) {
                                for (PrepackageProductItem prepackageProductItem : prepackagesForPrepackagesAndProduct) {
                                    if (changeLog.getPrepackageItemId().equalsIgnoreCase(prepackageProductItem.getId())) {
                                        for (Prepackage prepackage : prepackageList) {
                                            if (prepackage.getId().equalsIgnoreCase(prepackageProductItem.getPrepackageId())) {
                                                productPrepackageQuantityHoldResult.setName(prepackage.getName());
                                                break;
                                            }
                                        }
                                        break;
                                    }
                                }
                            }
                            BigDecimal qty = productQuantity.getQuantity().add(changeLog.getQuantity());

                            productPrepackageQuantityHoldResult.setInventoryId(changeLog.getInventoryId());
                            productPrepackageQuantityHoldResult.setQuantity(qty);
                            productPrepackageQuantityHoldResult.setTransactionNo(transaction.getTransNo());
                        }
                    }


                    if (productQuantity.getQuantity().doubleValue() > 0) {
                        holdQuantity.add(productQuantity);
                    }
                    if (productPrepackageQuantityHoldResult.getQuantity().intValue() > 0) {
                        prepackageHoldQuantity.add(productPrepackageQuantityHoldResult);
                    }

                }
            }
        }

        List<ProductPrepackageQuantityResult> prepackageQuantities = new ArrayList<>();

        if (prepackageList.size() > 0) {
            for (Prepackage prepackage : prepackageList) {
                if (productPrepackageQuantities.size() > 0) {
                    for (ProductPrepackageQuantity productPrepackageQuantity : productPrepackageQuantities) {
                        if (productPrepackageQuantity.getPrepackageId().equalsIgnoreCase(prepackage.getId())) {
                            ProductPrepackageQuantityResult productPrepackageQuantityResult = new ProductPrepackageQuantityResult();
                            productPrepackageQuantityResult.setInventoryId(productPrepackageQuantity.getInventoryId());
                            productPrepackageQuantityResult.setQuantity(BigDecimal.valueOf(productPrepackageQuantity.getQuantity()));
                            productPrepackageQuantityResult.setName(prepackage.getName());

                            prepackageQuantities.add(productPrepackageQuantityResult);
                        }
                    }
                }
            }
        }


        List<ProductQuantity> hiddenQuantity = new ArrayList<>();
        Map<String, BigDecimal> productQuantitiesMap = new HashMap<>();

        if (dbProduct.getQuantities().size() > 0) {
            List<ObjectId> inventories = new ArrayList<>();
            for (ProductQuantity quantity : dbProduct.getQuantities()) {
                if (quantity != null && StringUtils.isNotBlank(quantity.getInventoryId())) {
                    inventories.add(new ObjectId(quantity.getInventoryId()));
                    productQuantitiesMap.put(quantity.getInventoryId(), quantity.getQuantity());
                }
            }

            Iterable<Inventory> inventoriesByStatus = inventoryRepository.getInventoriesByStatus(token.getCompanyId(), token.getShopId(), inventories, false);

            if (inventoriesByStatus != null) {
                for (Inventory byStatus : inventoriesByStatus) {
                    if (productQuantitiesMap.containsKey(byStatus.getId())) {
                        ProductQuantity productQuantity = new ProductQuantity();
                        productQuantity.setInventoryId(byStatus.getId());
                        productQuantity.setQuantity(productQuantitiesMap.get(byStatus.getId()));
                        hiddenQuantity.add(productQuantity);
                    }
                }
            }
        }

        productQuantityInfo.setTransferQuantity(transferQuantity);
        productQuantityInfo.setHoldQuantity(holdQuantity);
        productQuantityInfo.setProductPrepackageQuantityResult(prepackageQuantities);
        productQuantityInfo.setHiddenQuantity(hiddenQuantity);
        productQuantityInfo.setProductPrepackageTransferResult(prepackageTransferQuantity);
        productQuantityInfo.setProductPrepackageHoldResult(prepackageHoldQuantity);
        return productQuantityInfo;
    }

    /**
     * Get inventory action for transaction
     *
     * @param transactionId : transaction id
     * @return
     */
    public Map<String, List<InventoryOperation>> getInventoryAction(String transactionId) {
        List<InventoryOperation> allBatchInfo = new ArrayList<>();
        List<InventoryOperation> allPrepackageInfo = new ArrayList<>();
        List<InventoryOperation> previousActions = inventoryActionRepository.getActionsBySource(token.getCompanyId(), token.getShopId(), transactionId, InventoryOperation.SourceType.Transaction);
        Map<String, List<InventoryOperation>> preBatchQuantityInfoMap = new HashMap<>();
        Map<String, List<InventoryOperation>> prePrepackageQuantityInfoMap = new HashMap<>();
        for (InventoryOperation previousAction : previousActions) {
            BigDecimal quantity = previousAction.getQuantity();
            if (previousAction.getQuantity().doubleValue() < 0) {
                quantity = quantity.negate();
            }

            if (StringUtils.isNotBlank(previousAction.getPrepackageItemId())) {
                this.getInventoryActionForShowQuantity(previousAction, prePrepackageQuantityInfoMap, previousAction.getPrepackageItemId(), quantity, Boolean.TRUE);
            } else {
                this.getInventoryActionForShowQuantity(previousAction, preBatchQuantityInfoMap, previousAction.getBatchId(), quantity, Boolean.FALSE);
            }
        }

        for (Map.Entry<String, List<InventoryOperation>> entry : preBatchQuantityInfoMap.entrySet()) {
            for (InventoryOperation action : entry.getValue()) {
                allBatchInfo.add(action);
            }
        }

        for (Map.Entry<String, List<InventoryOperation>> entry : prePrepackageQuantityInfoMap.entrySet()) {
            for (InventoryOperation action : entry.getValue()) {
                allPrepackageInfo.add(action);
            }
        }

        Map<String, List<InventoryOperation>> actionDetails = new HashMap<>();
        actionDetails.put("Batch", allBatchInfo);
        actionDetails.put("Prepackage", allPrepackageInfo);

        return actionDetails;

    }

    /**
     * This method identifies inventory action that needs to Use for show quantity
     *
     * @param previousAction  : previous action
     * @param quantityInfoMap : list of all inventory actions
     * @param targetId        : batch or prepackage id
     * @param quantity        : inventory action's quantity
     * @param isPrepackage    : weather request is for prepackage or batch
     */
    private void getInventoryActionForShowQuantity(InventoryOperation previousAction, Map<String, List<InventoryOperation>> quantityInfoMap, String targetId, BigDecimal quantity, Boolean isPrepackage) {
        String groupId = this.getActionGroupId(previousAction.getProductId(), targetId, previousAction.getSourceChildId(), quantity);
        if (quantityInfoMap.containsKey(groupId)) {
            Iterator<InventoryOperation> actionIterator = quantityInfoMap.get(groupId).iterator();

            Boolean processed = Boolean.FALSE;
            while (actionIterator.hasNext()) {
                InventoryOperation action = actionIterator.next();

                BigDecimal actionQuantity = action.getQuantity();
                if (actionQuantity.doubleValue() < 0) {
                    actionQuantity = action.getQuantity().negate();
                }

                String actionGroupId;
                if (isPrepackage) {
                    actionGroupId = this.getActionGroupId(action.getProductId(), action.getPrepackageItemId(), action.getSourceChildId(), actionQuantity);
                } else {
                    actionGroupId = this.getActionGroupId(action.getProductId(), action.getBatchId(), action.getSourceChildId(), actionQuantity);
                }

                if (actionGroupId.equals(groupId) && quantityInfoMap.get(actionGroupId).size() != 0) {
                    actionIterator.remove();
                    processed = Boolean.TRUE;
                }
            }
            quantityInfoMap.put(groupId, Lists.newArrayList(actionIterator));

            if (!processed) {
                quantityInfoMap.get(groupId).add(previousAction);
            }

        } else {
            quantityInfoMap.putIfAbsent(groupId, new ArrayList<>());
            quantityInfoMap.get(groupId).add(previousAction);
        }
    }

    /**
     * Create action group id by grouping below given data
     *
     * @param productId     : product id
     * @param targetId      : batch or prepackage item id
     * @param sourceChildId : source child id (Order item id)
     * @param quantity      : quantity
     */
    private String getActionGroupId(String productId, String targetId, String sourceChildId, BigDecimal quantity) {
        return productId + "-" + targetId + "-" + sourceChildId + "-" + quantity;
    }


    @Override
    public DateSearchResult<Product> getProductsByDates(long afterDate, long beforeDate) {
        return productRepository.findItemsByDate(token.getCompanyId(), token.getShopId(), afterDate, beforeDate, "");

    }

    @Override
    public DateSearchResult<Brand> getBrandsByDates(long afterDate, long beforeDate) {
        return brandRepository.findItemsWithDate(token.getCompanyId(), afterDate, beforeDate);
    }

    private void checkProductUseInTransaction(List<String> productIds, String status) {
        SearchResult<Transaction> transactionByProduct = transactionRepository.getTransactionByProduct(token.getCompanyId(), token.getShopId(), "modified:-1", productIds);
        StringBuilder transactionString = new StringBuilder();
        LinkedHashSet<ObjectId> products = new LinkedHashSet<>();

        if (transactionByProduct != null) {
            for (Transaction transaction : transactionByProduct.getValues()) {
                Cart cart = transaction.getCart();
                if (cart != null && cart.getItems() != null) {
                    for (OrderItem orderItem : cart.getItems()) {
                        if (orderItem != null) {
                            for (String productId : productIds) {
                                if (orderItem.getProductId().equalsIgnoreCase(productId)) {
                                    products.add(new ObjectId(orderItem.getProductId()));
                                    transactionString.append(" ").append(transaction.getTransNo()).append(",");
                                }
                            }
                        }
                    }
                }
            }
        }

        ArrayList<ObjectId> productIdList = Lists.newArrayList(products);
        if (products.size() > 0) {
            StringBuilder productString = new StringBuilder();

            HashMap<String, Product> productHashMap = productRepository.listAsMap(token.getCompanyId(), productIdList);
            if (productHashMap != null) {
                for (String productId : productHashMap.keySet()) {
                    Product product = productHashMap.get(productId);
                    productString.append(" ").append(product.getName()).append(",");
                }
            }
            String transactionSubString = transactionString.substring(0, transactionString.length() - 1);
            String productSubString = productString.substring(0, productString.length() - 1);
            throw new BlazeInvalidArgException("Product", productSubString + " are being used in transaction (" + transactionSubString + " ). So can not " + status + " it.");
        }
    }

    @Override
    public SearchResult<ProductResult> getAllProductsByVendorBrand(String vendorId, int start, int limit) {
        limit = (limit == 0) ? Integer.MAX_VALUE : limit;
        Vendor vendor = vendorRepository.get(token.getCompanyId(), vendorId);
        LinkedHashSet<String> brandIds = new LinkedHashSet<>();
        if (vendor != null) {
            brandIds = vendor.getBrands();
        }

        SearchResult<ProductResult> searchResult = productRepository.getProductsByVendorBrand(token.getCompanyId(), token.getShopId(), vendorId, Lists.newArrayList(brandIds), start, limit);

        return assignedProductDependencies(searchResult, token.getShopId());

    }

    private void checkProductUseInPurchaseOrder(List<String> productIds, String status) {
        SearchResult<PurchaseOrder> allPurchaseOrder = purchaseOrderRepository.getAllPurchaseOrderByProductId(token.getCompanyId(), token.getShopId(), "{modified:-1}", productIds);
        StringBuilder purchaseString = new StringBuilder();
        LinkedHashSet<ObjectId> products = new LinkedHashSet<>();

        if (allPurchaseOrder != null) {
            for (PurchaseOrder purchaseOrder : allPurchaseOrder.getValues()) {
                List<POProductRequest> poProductRequestList = purchaseOrder.getPoProductRequestList();
                if (poProductRequestList.size() > 0) {
                    for (POProductRequest poProductRequest : poProductRequestList) {
                        if (poProductRequest != null) {
                            for (String productId : productIds) {
                                if (poProductRequest.getProductId().equalsIgnoreCase(productId)) {
                                    products.add(new ObjectId(poProductRequest.getProductId()));
                                    purchaseString.append(" ").append(purchaseOrder.getPoNumber()).append(",");
                                }
                            }
                        }
                    }
                }
            }
        }

        ArrayList<ObjectId> productIdList = Lists.newArrayList(products);
        if (products.size() > 0) {
            StringBuilder productString = new StringBuilder();

            HashMap<String, Product> productHashMap = productRepository.listAsMap(token.getCompanyId(), productIdList);
            if (productHashMap != null) {
                for (String productId : productHashMap.keySet()) {
                    Product product = productHashMap.get(productId);
                    productString.append(" ").append(product.getName()).append(",");
                }
            }
            String purchaseOrderSubString = purchaseString.substring(0, purchaseString.length() - 1);
            String productSubString = productString.substring(0, productString.length() - 1);
            throw new BlazeInvalidArgException("Product", productSubString + " are being used in purchase order (" + purchaseOrderSubString + " ). So can not " + status + " it.");
        }
    }

    /**
     * This is private method for call product update webhook.
     *
     * @param product
     */
    private void callWebHook(Product product, boolean isNew) {
        ProductData request = new ProductData();
        request.setId(product.getId());
        request.setName(product.getName());
        request.setSku(product.getSku());
        request.setFlowerType(product.getFlowerType());
        request.setVendorId(product.getVendorId());
        request.setProductSaleType(product.getProductSaleType());
        request.setBrandId(product.getBrandId());
        request.setCannabisType(product.getCannabisType());
        request.setCategoryId(product.getCategoryId());
        request.setWeightPerUnit(product.getWeightPerUnit());
        request.setPriceBreaks(product.getPriceBreaks());
        request.setPriceRanges(product.getPriceRanges());
        request.setQuantities(product.getQuantities());
        request.setNew(isNew);
        request.setByGram(product.isByGram());
        request.setActive(product.isActive());
        request.setAssets(product.getAssets());
        request.setBrand(product.getBrand());
        request.setByPrepackage(product.isByPrepackage());
        request.setCategory(product.getCategory());
        request.setCbd(product.getCbd());
        request.setCbda(product.getCbda());
        request.setCbn(product.getCbn());
        request.setCustomGramType(product.getCustomGramType());
        request.setCustomTaxInfo(product.getCustomTaxInfo());
        request.setCustomWeight(product.getCustomWeight());
        request.setDescription(product.getDescription());
        request.setDiscountable(product.isDiscountable());
        request.setEnableExciseTax(product.isEnableExciseTax());
        request.setEnableMixMatch(product.isEnableMixMatch());
        request.setEnableWeedmap(product.isEnableWeedmap());
        request.setGenetics(product.getGenetics());
        request.setImportId(product.getImportId());
        request.setLowInventoryNotification(product.isLowInventoryNotification());
        request.setLowThreshold(product.getLowThreshold());
        request.setMedicalConditions(product.getMedicalConditions());
        request.setMedicinal(product.isMedicinal());
        request.setNotes(product.getNotes());
        request.setPotency(product.isPotency());
        request.setPotencyAmount(product.getPotencyAmount());
        request.setPriceIncludesALExcise(product.isPriceIncludesALExcise());
        request.setPriceIncludesExcise(product.isPriceIncludesExcise());
        request.setQbDesktopItemRef(product.getQbDesktopItemRef());
        request.setQbItemRef(product.getQbItemRef());
        request.setTaxOrder(product.getTaxOrder());
        request.setShowInWidget(product.isShowInWidget());
        request.setTags(product.getTags());
        request.setTaxTables(product.getTaxTables());
        request.setTaxType(product.getTaxType());
        request.setThc(product.getThc());
        request.setThca(product.getThca());
        request.setUnitPrice(product.getUnitPrice());
        request.setUnitValue(product.getUnitValue());
        request.setVendor(product.getVendor());
        request.setCreated(product.getCreated());
        request.setCompanyId(product.getCompanyId());
        request.setDeleted(product.isDeleted());
        request.setDirty(product.isDirty());
        request.setShopId(product.getShopId());
        request.setModified(product.getModified());
        request.setUpdated(product.isUpdated());

        try {
            prepareProductForWebHook(request);
            partnerWebHookManager.updateProductWebHook(request.getCompanyId(), request.getShopId(), request, PartnerWebHook.PartnerWebHookType.UPDATE_PRODUCT);
        } catch (Exception e) {
            LOG.error("Product web hook failed for shop: " + token.getShopId() + ", company: " + token.getCompanyId());
        }

    }

    /**
     * Private method to call webhook for bulk update
     *
     * @param productIds
     */
    private void callWebHookForBulkProduct(List<ObjectId> productIds) {
        HashMap<String, Product> productHashMap = productRepository.findItemsInAsMap(token.getCompanyId(), token.getShopId(), productIds);

        for (String key : productHashMap.keySet()) {
            callWebHook(productHashMap.get(key), false);
        }
    }

    /**
     * Private method to call webhook for copy product.
     *
     * @param productsToAdd
     */
    private void callWebHookForCopyProduct(List<Product> productsToAdd) {

        for (Product product : productsToAdd) {
            callWebHook(product, true);
        }
    }

    private void prepareProductForWebHook(ProductData product) {
        HashMap<String, ProductWeightTolerance> toleranceHashMap = weightToleranceRepository.listAsMap(token.getCompanyId());
        Vendor vendor = vendorRepository.getById(product.getVendorId());
        ProductCategory productCategory = productCategoryRepository.getById(product.getCategoryId());
        Brand brand = brandRepository.getById(product.getBrandId());

        product.setVendor(vendor);
        product.setCategory(productCategory);
        product.setBrand(brand);

        ArrayList<ProductWeightTolerance> weightTolerancesList = new ArrayList<ProductWeightTolerance>(toleranceHashMap.values());

        if (productCategory != null) {
            if (productCategory.getUnitType() == ProductCategory.UnitType.grams && !CollectionUtils.isNullOrEmpty(product.getPriceBreaks())) {
                List<ProductPriceRange> ranges = productService.populatePriceRanges(product, product.getPriceRanges(), "default", weightTolerancesList);
                product.setPriceRanges(ranges);
                product.setPriceBreaks(new ArrayList<ProductPriceBreak>());
            } else if (productCategory.getUnitType() == ProductCategory.UnitType.units && !CollectionUtils.isNullOrEmpty(product.getPriceRanges())) {
                List<ProductPriceBreak> productPriceBreaks = productService.populatePriceBreaks(product, productCategory, product.getPriceBreaks(), true, weightTolerancesList, false, Boolean.FALSE);
                product.setPriceBreaks(productPriceBreaks);
                product.setPriceRanges(new ArrayList<ProductPriceRange>());
            }
        }
    }

    /**
     * Override method to update price range and price break according to product.
     *
     * @param pricingTemplate
     */
    @Override
    public void updateMemberGroupPrices(PricingTemplate pricingTemplate) {

        Iterable<MemberGroupPrices> memberGroupPrices = groupPricesRepository.getMemberGroupPricesByTemplate(token.getCompanyId(), token.getShopId(), pricingTemplate.getId());
        Set<ObjectId> productIds = new HashSet<>();

        HashMap<String, List<ProductPriceBreak>> priceBreakmap = new HashMap<>();
        HashMap<String, List<ProductPriceRange>> priceRangeMap = new HashMap<>();
        HashMap<String, List<ObjectId>> groupPriceIdMap = new HashMap<>();

        for (MemberGroupPrices memberGroupPrice : memberGroupPrices) {
            productIds.add(new ObjectId(memberGroupPrice.getProductId()));
            List<ObjectId> memberGroupIds = groupPriceIdMap.getOrDefault(groupPriceIdMap.get(memberGroupPrice.getProductId()), new ArrayList<>());
            memberGroupIds.add(new ObjectId(memberGroupPrice.getId()));
            groupPriceIdMap.put(memberGroupPrice.getProductId(), memberGroupIds);
        }

        HashMap<String, Product> productHashMap = productRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(productIds));
        HashMap<String, ProductCategory> categoryHashMap = productCategoryRepository.listAsMap(token.getCompanyId(), token.getShopId());

        for (MemberGroupPrices memberGroupPrice : memberGroupPrices) {
            Product product = productHashMap.get(memberGroupPrice.getProductId());
            if (product == null) {
                continue;
            }
            ProductCategory category = categoryHashMap.get(product.getCategoryId());
            if (category == null) {
                continue;
            }
            if (priceBreakmap.containsKey(product.getId()) && priceRangeMap.containsKey(product.getId())) {
                continue;
            }
            memberGroupPrice.setPriceBreaks(pricingTemplate.getPriceBreaks());
            memberGroupPrice.setPriceRanges(pricingTemplate.getPriceRanges());

            //As price range id was random while creation so updating here with product id + tolerance id
            if (!CollectionUtils.isNullOrEmpty(memberGroupPrice.getPriceRanges())) {
                for (ProductPriceRange priceRange : memberGroupPrice.getPriceRanges()) {
                    priceRange.setId(product.getId() + "_" + priceRange.getWeightToleranceId());
                }
            }

            if (!CollectionUtils.isNullOrEmpty(memberGroupPrice.getPriceBreaks())) {
                populatePriceBreaks(product, category, memberGroupPrice.getPriceBreaks(), Boolean.TRUE, null, Boolean.FALSE, Boolean.FALSE);
            }

            priceBreakmap.put(product.getId(), pricingTemplate.getPriceBreaks());
            priceRangeMap.put(product.getId(), pricingTemplate.getPriceRanges());

        }
        List<ObjectId> groupPrices = new ArrayList<>();
        List<ProductPriceRange> priceRange = new ArrayList<>();
        List<ProductPriceBreak> priceBreak = new ArrayList<>();

        for (String key : groupPriceIdMap.keySet()) {
            priceBreak = priceBreakmap.getOrDefault(priceBreakmap.get(key), new ArrayList<>());
            priceRange = priceRangeMap.getOrDefault(priceRangeMap.get(key), new ArrayList<>());
            groupPrices = groupPriceIdMap.getOrDefault(groupPriceIdMap.get(key), new ArrayList<>());
            groupPricesRepository.updatePricesForTemplate(token.getCompanyId(), token.getShopId(), groupPrices, priceBreak, priceRange);
        }
    }

    @Override
    public void removePricingTemplate(String companyId, String shopId, PricingTemplate pricingTemplate) {
        productRepository.removeProductPricing(companyId, shopId, pricingTemplate.getId());
        Iterable<MemberGroupPrices> memberGroupPrices = groupPricesRepository.getMemberGroupPricesByTemplate(token.getCompanyId(), token.getShopId(), pricingTemplate.getId());

        List<ObjectId> groupPriceId = new ArrayList<>();
        for (MemberGroupPrices memberGroupPrice : memberGroupPrices) {
            groupPriceId.add(new ObjectId(memberGroupPrice.getId()));
        }

        groupPricesRepository.updatePricingTemplate(token.getCompanyId(), token.getShopId(), groupPriceId, null);

    }

    /**
     * This method updates values of price break/range of product on update of pricing template
     *
     * @param pricingTemplate : pricing template
     */
    @Override
    public void applyPricingTemplateUpdate(PricingTemplate pricingTemplate) {
        Iterable<Product> products = productRepository.findProductsByPricingTemplateId(token.getCompanyId(), token.getShopId(), pricingTemplate.getId());
        HashMap<String, ProductCategory> categoryMap = categoryRepository.listAsMap(token.getCompanyId(), token.getShopId());

        for (Product product : products) {
            ProductCategory category = categoryMap.get(product.getCategoryId());

            if (category != null && ((category.getUnitType().equals(ProductCategory.UnitType.units) && pricingTemplate.getWeightPerUnit().equals(product.getWeightPerUnit()))
                    || ((category.getUnitType().equals(ProductCategory.UnitType.grams) && StringUtils.isNotBlank(product.getPricingTemplateId()))))) {
                this.assignPricingTemplateToProduct(product, pricingTemplate, category);
                productRepository.updateProductPrice(token.getCompanyId(), token.getShopId(), product);
            }
        }
    }

    /**
     * Private method to filter discovery tags and update in product.
     *
     * @param product          : product
     * @param tagGroupHashMap  : tagGroupHashMap
     * @param wmProductMapping : wmProductMapping
     */
    private void findDiscoveryTags(ProductResult product, HashMap<String, WmTagGroups> tagGroupHashMap, WmProductMapping wmProductMapping) {
        wmProductMapping.getProductTagGroups().forEach(productTagGroups -> {
            WmTagGroups tagGroup = tagGroupHashMap.get(productTagGroups.getWmTagGroup());
            if (tagGroup != null) {
                productTagGroups.setWmTagGroupName(tagGroup.getTagGroupName());
                WmTagGroups.WmDiscoveryTags discoveryTags = tagGroup.getDiscoveryTags().stream().filter(wmDiscoveryTags -> wmDiscoveryTags.getId().equalsIgnoreCase(wmProductMapping.getWmDiscoveryTag())).findFirst().orElse(null);
                if (discoveryTags != null) {
                    productTagGroups.setWmDiscoveryTagName(discoveryTags.getTagName());
                }
            }
        });
        product.setProductTagGroups(wmProductMapping.getProductTagGroups());
    }

    @Override
    public SearchResult<LimitedProductResult> searchLimitedProductsByCategoryId(String shopId, String categoryId, String productId, int start, int limit, boolean quantity, String term, String targetInventoryId, FilterType status, String batchSku) {
        if (limit <= 0 || limit > PRODUCT_RESULT_LIMIT)
            limit = PRODUCT_RESULT_LIMIT;

        if (StringUtils.isBlank(shopId)) {
            shopId = token.getShopId();
        }

        if (!token.getShopId().equalsIgnoreCase(shopId)) {
            ProductCategory currentCategory = productCategoryRepository.get(token.getCompanyId(), categoryId);
            if (currentCategory != null) {
                ProductCategory category = productCategoryRepository.getCategory(token.getCompanyId(), shopId, currentCategory.getName());
                if (category != null) {
                    categoryId = category.getId();
                }
            }
        }

        SearchResult<LimitedProductResult> results = new SearchResult<>();
        if (StringUtils.isNotEmpty(productId)) {
            LimitedProductResult product = productRepository.get(token.getCompanyId(), productId, LimitedProductResult.class);
            results.getValues().add(product);
        } else if (StringUtils.isBlank(categoryId) && StringUtils.isNotBlank(term)) {
            if (status == null || status == FilterType.All) {
                results = productRepository.findProductsByPattern(token.getCompanyId(), shopId, "{name:1}", term, start, limit, LimitedProductResult.class);
            } else {
                results = productRepository.findProductsByPatternAndState(token.getCompanyId(), shopId, "{name:1}", term, start, limit, LimitedProductResult.class, (status == FilterType.Active));
            }
        } else if (StringUtils.isNotBlank(categoryId) && StringUtils.isNotBlank(term)) {
            if (status == null || status == FilterType.All) {
                results = productRepository.findProductsByCategoryIdAndTerm(token.getCompanyId(), shopId, "{name:1}", categoryId, term, start, limit, LimitedProductResult.class);
            } else {
                results = productRepository.findProductsByCategoryIdAndTermAndState(token.getCompanyId(), shopId, "{name:1}", categoryId, term, start, limit, LimitedProductResult.class, (status == FilterType.Active));
            }
        } else if (StringUtils.isNotBlank(categoryId)) {
            if (status == null || status == FilterType.All) {
                results = productRepository.findProductsByCategoryId(token.getCompanyId(), shopId, categoryId, "{name:1}", start, limit, LimitedProductResult.class);
            } else {
                results = productRepository.findProductsByCategoryIdAndState(token.getCompanyId(), shopId, categoryId, "{name:1}", start, limit, LimitedProductResult.class, (status == FilterType.Active));
            }
            if (quantity) {
                List<LimitedProductResult> productResultList = new ArrayList<>();
                prepareQuantitiesForLimitedResult(shopId, targetInventoryId, results.getValues(), productResultList);
                results.setValues(productResultList);

            }
        } else if (StringUtils.isNotEmpty(batchSku)) {
            Product product = getProductByBatchSKU(batchSku);
            LimitedProductResult productResult = productRepository.get(token.getCompanyId(), product.getId(), LimitedProductResult.class);
            if (productResult != null) {
                results.getValues().add(productResult);
            }
        } else {
            if (status == null || status == FilterType.All) {
                results = productRepository.findItems(token.getCompanyId(), shopId, "{name:1}", start, limit, LimitedProductResult.class);
            } else {
                results = productRepository.findItemsByState(token.getCompanyId(), shopId, "{name:1}", start, limit, LimitedProductResult.class, (status == FilterType.Active));
            }
            if (quantity) {
                List<LimitedProductResult> productResults = results.getValues();
                List<LimitedProductResult> productResultList = new ArrayList<>();
                prepareQuantitiesForLimitedResult(shopId, targetInventoryId, productResults, productResultList);
                results.setValues(productResultList);

            }
        }
        List<ObjectId> categoryList = new ArrayList<>();
        List<ObjectId> brandList = new ArrayList<>();
        results.getValues().forEach(product -> {
            if (StringUtils.isNotBlank(product.getCategoryId()) && ObjectId.isValid(product.getCategoryId())) {
                categoryList.add(new ObjectId(product.getCategoryId()));
            }

            if (StringUtils.isNotBlank(product.getBrandId()) && ObjectId.isValid(product.getBrandId())) {
                brandList.add(new ObjectId(product.getBrandId()));
            }

        });
        HashMap<String, ProductCategory> productCategories = productCategoryRepository.listAsMap(token.getCompanyId(), categoryList);
        HashMap<String, Brand> productBrands = brandRepository.listAsMap(token.getCompanyId(), brandList);
        for (LimitedProductResult limitedProductResult : results.getValues()) {
            if (productBrands.containsKey(limitedProductResult.getCategoryId())) {
                limitedProductResult.setCategory(productCategories.get(limitedProductResult.getCategoryId()));
            }
            if (productBrands.containsKey(limitedProductResult.getBrandId())) {
                limitedProductResult.setBrandName(productBrands.get(limitedProductResult.getBrandId()).getName());
            }
        }
        return results;
    }

    private void prepareQuantitiesForLimitedResult(String shopId, String targetInventoryId, List<LimitedProductResult> productResults, List<LimitedProductResult> productResultList) {
        Terminal terminal = terminalRepository.get(token.getCompanyId(), token.getTerminalId());
        String inventoryId = targetInventoryId;
        if (terminal != null) {
            inventoryId = terminal.getAssignedInventoryId();
        }

        List<String> productIds = new ArrayList<>();
        productResults.forEach(limitedProductResult -> {
            productIds.add(limitedProductResult.getId());
        });

        HashMap<String, Integer> quantityProductMap = StringUtils.isNotBlank(inventoryId)
                ? productPrepackageQuantityRepository.getQuantitiesForInventoryWithQuantityProductMap(token.getCompanyId(), shopId, inventoryId) :
                productPrepackageQuantityRepository.getQuantitiesForProductsWithQuantityProductMap(token.getCompanyId(), shopId, Lists.newArrayList(productIds));


        for (LimitedProductResult productResult : productResults) {
            List<ProductQuantity> productQuantities = productResult.getQuantities();
            List<ProductQuantity> productQuantityList = new ArrayList<>();
            for (ProductQuantity productQuantity : productQuantities) {
                if (productQuantity.getQuantity().compareTo(BigDecimal.ZERO) != 0 && (StringUtils.isBlank(inventoryId) || productQuantity.getInventoryId().equals(inventoryId))) {
                    productQuantityList.add(productQuantity);
                }
            }

            Integer prepackageAmt = quantityProductMap.get(productResult.getId());

            // if quantity exists and prepackageAmt is greater than 0
            if (productQuantityList.size() > 0 || (prepackageAmt != null && prepackageAmt > 0)) {
                productResult.setQuantities(productQuantityList);
                productResultList.add(productResult);
            }

        }
    }
}

