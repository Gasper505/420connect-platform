package com.fourtwenty.core.services.mgmt.impl;

import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.loyalty.*;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.domain.models.product.ProductPriceRange;
import com.fourtwenty.core.domain.models.product.ProductWeightTolerance;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.domain.repositories.loyalty.LoyaltyRewardRepository;
import com.fourtwenty.core.domain.repositories.loyalty.LoyaltyUsageLogRepository;
import com.fourtwenty.core.engine.PromotionEngine;
import com.fourtwenty.core.engine.SuggestedPromotion;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.exceptions.BlazeOperationException;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.services.common.RealtimeService;
import com.fourtwenty.core.services.mgmt.PromotionProcessService;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.core.util.NumberUtils;
import com.fourtwenty.core.util.TextUtil;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import joptsimple.internal.Strings;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.joda.time.DateTimeZone;

import java.math.BigDecimal;
import java.util.*;

/**
 * Created by mdo on 12/15/17.
 */
public class PromotionProcessServiceImpl implements PromotionProcessService {
    private static final Log LOG = LogFactory.getLog(PromotionProcessServiceImpl.class);
    private static final String PROMOTION = "Promotion";

    @Inject
    PromotionRepository promotionRepository;
    @Inject
    PromoUsageRepository promoUsageRepository;
    @Inject
    TransactionRepository transactionRepository;
    @Inject
    ProductRepository productRepository;
    @Inject
    ShopRepository shopRepository;
    @Inject
    RealtimeService realtimeService;
    @Inject
    MemberRepository memberRepository;
    @Inject
    LoyaltyRewardRepository rewardRepository;
    @Inject
    LoyaltyUsageLogRepository loyaltyUsageLogRepository;
    @Inject
    PromotionEngine promotionEngine;


    @Override
    public Promotion findPromotionWithCode(final String companyId, final String shopId, String promoCode) {
        Promotion otherPromo = promotionRepository.getPromotionByPromoCode(companyId, shopId, promoCode);
        return otherPromo;
    }


    @Override
    public SearchResult<SuggestedPromotion> getSuggestedPromotions(final String companyId, final String shopId, Transaction transaction) {
        SearchResult<SuggestedPromotion> suggested = new SearchResult<>();
        if (transaction != null) {
            Member member = memberRepository.get(companyId, transaction.getMemberId());
            List<SuggestedPromotion> results = promotionEngine.getSuggestedPromotions(companyId, shopId, transaction.getCart(), member);
            suggested.setValues(results);
        }

        suggested.setTotal((long) suggested.getValues().size());
        return suggested;
    }

    // Algoritm to apply to transaction
    @Override
    public void applyPromo(final Shop shop,
                           final Transaction transaction,
                           final Cart workingCart,
                           final HashMap<String, Product> productHashMap,
                           final Transaction.TransactionStatus incomingStatus,
                           final HashMap<String, ProductCategory> productCategoryHashMap,
                           final HashMap<String, ProductWeightTolerance> weightToleranceHashMap,
                           final OrderedDiscountable discountable,
                           final Member member) {

        if (workingCart != null && workingCart.getPromotionReqs().size() == 0) {
            return;
        }

        Promotion promotion = null;
        LoyaltyReward reward = null;

        if (discountable instanceof Promotion) {
            promotion = (Promotion) discountable;
        } else if (discountable instanceof LoyaltyReward) {
            reward = (LoyaltyReward) discountable;
        }


        if (promotion == null && reward == null) {
            throw new BlazeInvalidArgException("Promotion", "Promotion/Reward does not exist.");
        }

        // Add promotions together
        if (promotion != null) {
            if (promotion.isRestrictMemberGroups()) {
                boolean isMemberIdExistsInMemberGroup = false;
                for (String s : promotion.getMemberGroupIds()) {
                    if (s.equals(member.getMemberGroupId())) {
                        isMemberIdExistsInMemberGroup = true;
                        break;
                    }
                }
                if (!isMemberIdExistsInMemberGroup) {
                    throw new BlazeOperationException("Member is not in the mandatory member groups.");
                }
            }

            applyAdditivePromotion(shop, transaction.getId(), workingCart, promotion, productHashMap,
                    incomingStatus, productCategoryHashMap, weightToleranceHashMap, member, transaction);
        } else if (reward != null) {

            // add reward discounts accordingly
            boolean success = applyAdditiveRewards(shop,
                    transaction.getId(),
                    transaction.getAssignedEmployeeId(),
                    transaction.getTransNo(),
                    workingCart,
                    reward,
                    productHashMap,
                    incomingStatus,
                    productCategoryHashMap,
                    weightToleranceHashMap, member);

            if (success) {
                double points = workingCart.getPointSpent().doubleValue();
                points += reward.getPoints();
                workingCart.setPointSpent(new BigDecimal(points));

                if (NumberUtils.round(points, 1) > NumberUtils.round(member.getLoyaltyPoints().doubleValue(), 1)) {
                    throw new BlazeInvalidArgException("Reward", "Member does not have enough points.");
                }
            }
        }
    }

    private boolean applyAdditiveRewards(final Shop shop,
                                         final String transactionId,
                                         final String assignedEmployeeId,
                                         final String transNo,
                                         Cart workingCart,
                                         final LoyaltyReward reward,
                                         final HashMap<String, Product> productHashMap,
                                         final Transaction.TransactionStatus incomingStatus,
                                         final HashMap<String, ProductCategory> productCategoryHashMap,
                                         final HashMap<String, ProductWeightTolerance> weightToleranceHashMap,
                                         final Member member) {
        // Check Criteria -- make sure all criteria are met when the reward isn't assigned yet.
        if (!reward.isAssigned()) {
            if (!reward.isPublished()) {
                throw new BlazeInvalidArgException("Reward", "Reward is not published.");
            }

            if (!reward.isActive()) {
                throw new BlazeInvalidArgException("Reward", "Reward is not active.");
            }
        }

        if (reward.getLoyaltyType() == LoyaltyReward.LoyaltyRewardType.Regular) {
            switch (reward.getRewardTarget()) {
                case AllSelectedProducts:
                    applyRewardAllSelectedProducts(reward, workingCart, productHashMap, productCategoryHashMap, weightToleranceHashMap);
                    break;
                case DeliveryFee:
                    applyRewardDeliveryDiscount(reward, workingCart, productHashMap, productCategoryHashMap, weightToleranceHashMap);
                    break;
                case OneFromProducts:
                    applyRewardOneFromSelectedProducts(reward, workingCart, productHashMap, productCategoryHashMap, weightToleranceHashMap);
                    break;
                case OneFromCategories:
                    applyRewardOneFromSelectedCategories(reward, workingCart, productHashMap, productCategoryHashMap, weightToleranceHashMap);
                    break;
                case OneFromVendors:
                    applyRewardOneFromSelectedVendors(reward, workingCart, productHashMap, productCategoryHashMap, weightToleranceHashMap);
                    break;
                case WholeCart:
                    applyRewardWholeCart(reward, workingCart);
                    break;
                case OneFromProductTags:
                    applyRewardOneFromSelectedProductTags(reward, workingCart, productHashMap, productCategoryHashMap, weightToleranceHashMap);
                    break;
            }
        }

        if (incomingStatus == Transaction.TransactionStatus.Completed) {
            // then create usage if necessary
            LoyaltyActivityLog log = new LoyaltyActivityLog();
            log.prepare(shop.getCompanyId());
            log.setShopId(shop.getId());
            log.setActivityDate(DateTime.now().getMillis());
            log.setActivityType(LoyaltyActivityLog.LoyaltyActivityType.Usage);
            log.setAmount(new BigDecimal(reward.getPoints()));
            log.setEmployeeId(assignedEmployeeId);
            log.setMemberId(member.getId());
            log.setTransNo(transNo);
            log.setRewardId(reward.getId());
            log.setRewardNo(reward.getRewardNo());
            loyaltyUsageLogRepository.save(log);

            // decrement points
            memberRepository.addLoyaltyPoints(shop.getCompanyId(), member.getId(), -reward.getPoints());

            realtimeService.sendRealTimeEvent(shop.getId(), RealtimeService.RealtimeEventType.RewardsUpdateEvent, null);
            realtimeService.sendRealTimeEvent(shop.getId(), RealtimeService.RealtimeEventType.MembersUpdateEvent, null);
        }

        return true;
    }


    private boolean applyRewardWholeCart(final LoyaltyReward reward, Cart workingCart) {

        double promoDiscount = 0;
        OrderItem.DiscountType discountType = reward.getDiscountType();

        if (discountType == OrderItem.DiscountType.Percentage) {
            double nonDiscountedCost = 0;
            for (OrderItem orderItem : workingCart.getItems()) {
                if (orderItem.getDiscount().doubleValue() <= 0) {
                    nonDiscountedCost += orderItem.getFinalPrice().doubleValue();
                }
            }
            promoDiscount = nonDiscountedCost * (reward.getDiscountAmt().doubleValue() / 100);
            promoDiscount = NumberUtils.round(promoDiscount, 2);
        } else {
            // cash type
            promoDiscount = reward.getDiscountAmt().doubleValue();
        }
        double currDiscount = workingCart.getDiscount().doubleValue();
        double newDiscount = currDiscount + promoDiscount;
        currDiscount = NumberUtils.round(newDiscount, 2);

        workingCart.setDiscount(new BigDecimal(currDiscount));
        workingCart.setDiscountType(OrderItem.DiscountType.Cash);

        // create log
        PromotionReqLog log = new PromotionReqLog();
        log.prepare(reward.getCompanyId());
        log.setRewardId(reward.getId());
        log.setAmount(reward.getDiscountAmt());
        log.setFinalizedDiscountId(reward.getFinalizedDiscountId());
        workingCart.getPromotionReqLogs().add(log);
        return true;
    }

    private boolean applyRewardDeliveryDiscount(final LoyaltyReward reward, Cart workingCart,
                                                final HashMap<String, Product> productHashMap,
                                                final HashMap<String, ProductCategory> productCategoryHashMap,
                                                final HashMap<String, ProductWeightTolerance> weightToleranceHashMap) {
        if (!workingCart.isEnableDeliveryFee()) {
            throw new BlazeInvalidArgException("Promotion", "Shop has no delivery fee.");
        }

        workingCart.setDeliveryDiscountType(reward.getDiscountType());
        workingCart.setDeliveryDiscount(reward.getDiscountAmt());
        return true;
    }

    private LinkedHashSet<OrderItem> filterRewardMatch(LoyaltyReward reward, LinkedHashSet<OrderItem> matchedItems) {

        if (reward.getDiscountType() == OrderItem.DiscountType.Cash) {
            // see if we can find a match with same unit price
            LinkedHashSet<OrderItem> filtered = new LinkedHashSet<>();
            for (OrderItem selectedItem : matchedItems) {
                int unitPrice = (int) (selectedItem.getUnitPrice().doubleValue() * 100);
                int finalPrice = (int) (selectedItem.getFinalPrice().doubleValue() * 100);
                int discountAmt = (int) (reward.getDiscountAmt().doubleValue() * 100);

                if (unitPrice == discountAmt && unitPrice <= finalPrice) {
                    filtered.add(selectedItem);
                    break;
                }
            }
            if (filtered.size() > 0) {
                return filtered;
            }
        }
        return matchedItems;
    }

    private void applyRewardOneFromSelectedVendors(final LoyaltyReward reward, Cart workingCart,
                                                   final HashMap<String, Product> productHashMap,
                                                   final HashMap<String, ProductCategory> productCategoryHashMap,
                                                   final HashMap<String, ProductWeightTolerance> weightToleranceHashMap) {


        List<OrderItem> orderItems = sortOrderItems(workingCart.getItems(),reward);

        LinkedHashSet<OrderItem> matchedItems = new LinkedHashSet<>();
        for (OrderItem orderItem : orderItems) {

            Product product = productHashMap.get(orderItem.getProductId());

            boolean found = false;
            if (product != null) {
                for (String vendorId : reward.getVendorIds()) {
                    if (vendorId != null && vendorId.equalsIgnoreCase(product.getVendorId())) {
                        found = true;
                        break;
                    }
                }
            }
            if (found) {
                if (orderItem.getStatus() == OrderItem.OrderItemStatus.Refunded) {
                    continue;
                }
                if (orderItem.getProductId().equalsIgnoreCase(product.getId())) {
                    matchedItems.add(orderItem);
                }
            }
        }

        matchedItems = filterRewardMatch(reward, matchedItems);

        for (OrderItem selectedItem : matchedItems) {
            // Apply the first one we find
            PromotionReqLog log = applyRewardDiscountForProduct(reward, selectedItem, productHashMap, productCategoryHashMap, weightToleranceHashMap);
            workingCart.getPromotionReqLogs().add(log);
            return;
        }


        throw new BlazeInvalidArgException("Rewards", "No products found for selected vendors for reward: " + reward.getName());
    }

    private void applyRewardOneFromSelectedCategories(final LoyaltyReward reward, Cart workingCart,
                                                      final HashMap<String, Product> productHashMap,
                                                      final HashMap<String, ProductCategory> productCategoryHashMap,
                                                      final HashMap<String, ProductWeightTolerance> weightToleranceHashMap) {

        List<OrderItem> orderItems = sortOrderItems(workingCart.getItems(),reward);
        LinkedHashSet<OrderItem> matchedItems = new LinkedHashSet<>();
        for (OrderItem orderItem : orderItems) {

            Product product = productHashMap.get(orderItem.getProductId());
            boolean found = false;
            if (product != null) {
                for (String categoryId : reward.getCategoryIds()) {
                    if (categoryId != null && categoryId.equalsIgnoreCase(product.getCategoryId())) {
                        found = true;
                        break;
                    }
                }
            }

            if (found) {
                if (orderItem.getStatus() == OrderItem.OrderItemStatus.Refunded) {
                    continue;
                }
                if (orderItem.getProductId().equalsIgnoreCase(product.getId())) {
                    matchedItems.add(orderItem);
                }
            }
        }
        matchedItems = filterRewardMatch(reward, matchedItems);

        for (OrderItem selectedItem : matchedItems) {
            // Apply the first one we find
            PromotionReqLog log = applyRewardDiscountForProduct(reward, selectedItem, productHashMap, productCategoryHashMap, weightToleranceHashMap);
            workingCart.getPromotionReqLogs().add(log);
            return;
        }
        throw new BlazeInvalidArgException("Rewards", "No products for selected categories for reward: " + reward.getName());
    }

    private void applyRewardOneFromSelectedProducts(final LoyaltyReward reward, Cart workingCart,
                                                    final HashMap<String, Product> productHashMap,
                                                    final HashMap<String, ProductCategory> productCategoryHashMap,
                                                    final HashMap<String, ProductWeightTolerance> weightToleranceHashMap) {
        List<OrderItem> orderItems = sortOrderItems(workingCart.getItems(),reward);
        LinkedHashSet<OrderItem> matchedItems = new LinkedHashSet<>();
        for (OrderItem orderItem : orderItems) {
            for (String productId : reward.getProductIds()) {
                if (productId != null) {
                    if (orderItem.getStatus() == OrderItem.OrderItemStatus.Refunded) {
                        continue;
                    }
                    if (orderItem.getProductId().equalsIgnoreCase(productId)) {
                        // Apply the first one we find
                        matchedItems.add(orderItem);
                        break;
                    }
                }
            }
        }
        matchedItems = filterRewardMatch(reward, matchedItems);

        for (OrderItem selectedItem : matchedItems) {
            // Apply the first one we find
            PromotionReqLog log = applyRewardDiscountForProduct(reward, selectedItem, productHashMap, productCategoryHashMap, weightToleranceHashMap);
            workingCart.getPromotionReqLogs().add(log);
            return;
        }

        throw new BlazeInvalidArgException("Rewards", "No products for selected products for reward: " + reward.getName());
    }

    private void applyRewardAllSelectedProducts(final LoyaltyReward reward, Cart workingCart,
                                                final HashMap<String, Product> productHashMap,
                                                final HashMap<String, ProductCategory> productCategoryHashMap,
                                                final HashMap<String, ProductWeightTolerance> weightToleranceHashMap) {
        boolean success = false;
        List<OrderItem> orderItems = sortOrderItems(workingCart.getItems(),reward);
        for (OrderItem orderItem : orderItems) {
            for (String productId : reward.getProductIds()) {
                if (orderItem.getStatus() == OrderItem.OrderItemStatus.Refunded) {
                    continue;
                }
                if (orderItem.getProductId().equalsIgnoreCase(productId)) {
                    PromotionReqLog log = applyRewardDiscountForProduct(reward, orderItem, productHashMap, productCategoryHashMap, weightToleranceHashMap);
                    workingCart.getPromotionReqLogs().add(log);
                    success = true;
                    break;
                }
            }
        }
        if (!success) {
            throw new BlazeInvalidArgException("Rewards", "No products for selected products for reward: " + reward.getName());
        }
    }

    public BigDecimal getRewardDiscountForProduct(final LoyaltyReward reward, OrderItem orderItem,
                                                  final HashMap<String, Product> productHashMap,
                                                  final HashMap<String, ProductCategory> productCategoryHashMap,
                                                  final HashMap<String, ProductWeightTolerance> weightToleranceHashMap){
        PromotionReqLog log = applyRewardDiscountForProduct(reward,orderItem, productHashMap,
                productCategoryHashMap, weightToleranceHashMap);
        return log.getAmount();
    }

    //Calculate the reward amount using the order item
    public BigDecimal getRewardDiscountAmountForProduct(final LoyaltyReward reward, OrderItem orderItem,
                                                  final HashMap<String, Product> productHashMap,
                                                  final HashMap<String, ProductCategory> productCategoryHashMap,
                                                  final HashMap<String, ProductWeightTolerance> weightToleranceHashMap){
        double discount = reward.getDiscountAmt().doubleValue();

        if (reward.getDiscountType() == OrderItem.DiscountType.Percentage) {
            Product product = productHashMap.get(orderItem.getProductId());
            ProductCategory category = productCategoryHashMap.get(product.getCategoryId());
            // convert to cash value
            if (category.getUnitType() == ProductCategory.UnitType.grams
                    && product.getPriceRanges().size() > 0
                    && reward.getDiscountUnitType() != ProductWeightTolerance.WeightKey.UNIT) {
                // Now calculate the discount
                ProductWeightTolerance weightTolerance = weightToleranceHashMap.get(reward.getDiscountUnitType().weightName);
                // Get the price Range
                if (weightTolerance != null) {
                    ProductPriceRange priceRange = null;

                    for (ProductPriceRange productPriceRange : product.getPriceRanges()) {
                        if (productPriceRange.getWeightToleranceId().equalsIgnoreCase(weightTolerance.getId())) {
                            priceRange = productPriceRange;
                            break;
                        }
                    }

                    if (priceRange != null) {
                        // Discount is the ratio of priceRange price

                        discount = priceRange.getPrice().doubleValue() * (reward.getDiscountAmt().doubleValue() / 100);
                        discount = NumberUtils.round(discount, 2);

                    }
                }

            } else {
                discount = orderItem.getUnitPrice().doubleValue() * ((reward.getDiscountAmt().doubleValue() / 100));
                discount = NumberUtils.round(discount, 2);
            }
        }

        return new BigDecimal(discount);

    }

    private PromotionReqLog applyRewardDiscountForProduct(final LoyaltyReward reward, OrderItem orderItem,
                                                          final HashMap<String, Product> productHashMap,
                                                          final HashMap<String, ProductCategory> productCategoryHashMap,
                                                          final HashMap<String, ProductWeightTolerance> weightToleranceHashMap) {
        double discount = reward.getDiscountAmt().doubleValue();

        if (reward.getDiscountType() == OrderItem.DiscountType.Percentage) {
            Product product = productHashMap.get(orderItem.getProductId());
            ProductCategory category = productCategoryHashMap.get(product.getCategoryId());
            // convert to cash value
            if (category.getUnitType() == ProductCategory.UnitType.grams
                    && product.getPriceRanges().size() > 0
                    && reward.getDiscountUnitType() != ProductWeightTolerance.WeightKey.UNIT) {
                // Now calculate the discount
                ProductWeightTolerance weightTolerance = weightToleranceHashMap.get(reward.getDiscountUnitType().weightName);
                // Get the price Range
                if (weightTolerance != null) {
                    ProductPriceRange priceRange = null;

                    for (ProductPriceRange productPriceRange : product.getPriceRanges()) {
                        if (productPriceRange.getWeightToleranceId().equalsIgnoreCase(weightTolerance.getId())) {
                            priceRange = productPriceRange;
                            break;
                        }
                    }

                    if (priceRange != null) {
                        // Discount is the ratio of priceRange price

                        discount = priceRange.getPrice().doubleValue() * (reward.getDiscountAmt().doubleValue() / 100);
                        discount = NumberUtils.round(discount, 2);

                    }
                }

            } else {
                discount = orderItem.getUnitPrice().doubleValue() * ((reward.getDiscountAmt().doubleValue() / 100));
                discount = NumberUtils.round(discount, 2);
            }
        }

        double currDiscount = orderItem.getDiscount().doubleValue();
        double newDiscount = currDiscount + discount;

        double finalCost = orderItem.getCost().doubleValue() - newDiscount;

        orderItem.setDiscount(new BigDecimal(newDiscount));
        orderItem.setDiscountType(OrderItem.DiscountType.Cash);

        if (finalCost < 0) {
            finalCost = 0;
            orderItem.setDiscount(orderItem.getCost());
        }


        orderItem.setFinalPrice(new BigDecimal(finalCost));

        orderItem.getPromotionReqs().add(new PromotionReq(null, reward.getId(),reward.getFinalizedDiscountId()));

        return createPromotionRewardLog(reward.getId(), reward.getFinalizedDiscountId(), reward.getCompanyId(), discount);
    }


    private boolean applyAdditivePromotion(final Shop shop,
                                           final String transactionId,
                                           Cart workingCart,
                                           final Promotion promotion,
                                           final HashMap<String, Product> productHashMap,
                                           final Transaction.TransactionStatus incomingStatus,
                                           final HashMap<String, ProductCategory> productCategoryHashMap,
                                           final HashMap<String, ProductWeightTolerance> weightToleranceHashMap,
                                           final Member member, final Transaction transaction) {

        //Check promotion stackability
        if (Promotion.PromotionType.Cart == promotion.getPromotionType() && !promotion.isStackable() && workingCart.getDiscount().doubleValue() > 0) {
            //throw new BlazeInvalidArgException(PROMOTION, "Promotion is stackable, so '"+ promotion.getName() +"' can not apply");
            return false;
        }


        // Check Criteria -- make sure all criteria are met when the promotion isn't assigned yet.
        if (!promotion.isAssigned()) {
            // Check expiration Date
            // Today
            long now = DateTime.now().getMillis();
            if (promotion.getStartDate() != null && promotion.getStartDate() != 0) {
                if (now < promotion.getStartDate()) {
                    removePromotion(workingCart, promotion);

                    throw new BlazeInvalidArgException("Promotion", String.format("Promo '%s' has not started.", promotion.getName()));
                }
            }

            if (promotion.getEndDate() != null && promotion.getEndDate() != 0) {
                if (now > promotion.getEndDate()) {
                    removePromotion(workingCart, promotion);
                    throw new BlazeInvalidArgException("Promotion", String.format("Promo '%s' has expired.", promotion.getName()));
                }
            }

            // Check whether promotion is enabled or not is done when it is added to the cart
            if (!promotion.isAssigned() && promotion.isActive() == false) {
                removePromotion(workingCart, promotion);
                throw new BlazeInvalidArgException("Promotion", String.format("Promo '%s' is no longer active.", promotion.getName()));
            }

            // Check promotion.available
            if (!promotion.isAssigned() && promotion.isEnableMaxAvailable()) {
                long maxUsed = promoUsageRepository.countForPromo(shop.getCompanyId(), shop.getId(), promotion.getId());
                if (promotion.getMaxAvailable() <= maxUsed) {
                    removePromotion(workingCart, promotion);
                    throw new BlazeInvalidArgException("Promotion", String.format("Promo '%s' is no longer available.", promotion.getName()));
                }
            }

            // Find current day based on timezone
            if (!isAvailableToday(promotion, shop)) {
                removePromotion(workingCart, promotion);
                throw new BlazeInvalidArgException("Promotion", String.format("Promo '%s' is not available today.", promotion.getName()));
            }

            // Check customer usage count. If exceeds, cannot apply promo
            if (promotion.isEnableLimitPerCustomer()) {
                int limitPerUser = promotion.getLimitPerCustomer();
                if (member != null) {
                    long usageCount = promoUsageRepository.countForMember(shop.getCompanyId(), shop.getId(), promotion.getId(), member.getId());
                    if (usageCount >= limitPerUser) {
                        removePromotion(workingCart, promotion);
                        throw new BlazeInvalidArgException("Promotion", String.format("Member has reached max allowed for promo '%s'.", promotion.getName()));
                    }
                }
            }

            // Check time of day
            if (promotion.isEnableDayDuration()) {
                long millis = DateUtil.timeAtStartWithTimeZone(shop == null ? null : shop.getTimeZone());
                LOG.info("Current day millis: " + millis);
                LOG.info("Day Start Time: " + promotion.getDayStartTime());
                LOG.info("Day End Time: " + promotion.getDayEndTime());
                if (promotion.getDayStartTime() != null) {
                    if (promotion.getDayStartTime() > millis) {
                        throw new BlazeInvalidArgException("Promotion", String.format("Promo '%s' is not active at this time.", promotion.getName()));
                    }
                }

                if (promotion.getDayEndTime() != null) {
                    if (promotion.getDayEndTime() < millis) {
                        throw new BlazeInvalidArgException("Promotion", String.format("Promo '%s' has ended for today.", promotion.getName()));
                    }
                }
            }
        }


        // Sort to make sure product and categories are last
        List<PromotionRule> sortedCriteria = Lists.newArrayList(promotion.getRules());

        sortedCriteria.sort(new Comparator<PromotionRule>() {
            @Override
            public int compare(PromotionRule o1, PromotionRule o2) {
                return o1.getRuleType().priority.compareTo(o2.getRuleType().priority);
            }
        });


        // now check criteria
        boolean cartPassed = true;
        int productsApplied = 0;

        List<OrderItem> matchedItems = new ArrayList<>();
        matchedItems.addAll(workingCart.getItems());
        matchedItems = sortOrderItems(matchedItems,promotion);

        for (PromotionRule criteria : sortedCriteria) {
            PromotionRule.PromotionRuleType type = criteria.getRuleType();

            switch (type) {
                case CartOrderMin:
                    cartPassed = checkPromoCartOrderMin(criteria, workingCart);
                    if (Transaction.TransactionType.Refund.equals(transaction.getTransType())) {
                        if (cartPassed == false) {
                            LOG.error("Cart subtotal did not meet promotion minimum amount of" + TextUtil.toCurrency(criteria.getMinAmt().doubleValue(), shop.getDefaultCountry()) + " for promo " + promotion.getName());
                        }
                    } else {
                        if (cartPassed == false) {
                            throw new BlazeInvalidArgException("Promotion", String.format("Cart subtotal did not meet promotion minimum amount of '%s' for promo '%s'.",
                                    TextUtil.toCurrency(criteria.getMinAmt().doubleValue(), shop.getDefaultCountry()),
                                    promotion.getName()));
                        }
                    }
                    break;
                case FirstTimeMember:
                    cartPassed = checkPromoFirstTimeMember(shop, criteria, workingCart, member);
                    if (cartPassed == false) {
                        throw new BlazeInvalidArgException("Promotion", String.format("Customer is not a first time member.", promotion.getName()));
                    }
                    break;
                case NumberMinOrders:
                    cartPassed = checkPromoMinOrdersPrevious(shop, criteria, workingCart, member);
                    if (cartPassed == false) {
                        throw new BlazeInvalidArgException("Promotion", String.format("Customer did not meet promotion minimum previous orders for promo '%s'.", promotion.getName()));
                    }
                    break;
                case Product:
                    matchedItems = checkPromoProduct(promotion, criteria, matchedItems);
                    break;
                case ProductCategory:
                    matchedItems = checkPromoProductCategory(promotion, criteria, matchedItems, productHashMap);
                    break;
                case ByVendor:
                    if (promotion.getPromotionType() == Promotion.PromotionType.Cart) {
                        cartPassed = checkPromoCartVendor(criteria, workingCart, productHashMap);
                        if (cartPassed == false) {
                            throw new BlazeInvalidArgException("Promotion", String.format("Promotion Criteria did not match for '%s'.", promotion.getName()));
                        }
                    } else if (promotion.getPromotionType() == Promotion.PromotionType.Product) {
                        matchedItems = checkPromoProductVendor(promotion, criteria, matchedItems, productHashMap);
                    } else if (promotion.getPromotionType() == Promotion.PromotionType.DeliveryFee) {
                        cartPassed = checkPromoCartVendorForDeliveryFee(criteria, workingCart, productHashMap);
                        if (cartPassed == false) {
                            throw new BlazeInvalidArgException("Promotion", String.format("Promotion Criteria did not match for '%s'.", promotion.getName()));
                        }
                    }
                    break;
                case LastVist:
                    cartPassed = checkPromoLastVisit(workingCart, member);
                    if (cartPassed == false) {
                        throw new BlazeInvalidArgException("Promotion", String.format("Promotion Criteria did not match for '%s'.", promotion.getName()));
                    }
                    break;
                case OneFromProducts:
                    matchedItems = checkPromoProduct(promotion, criteria, matchedItems);
                    break;
                case OneFromCategories:
                    matchedItems = checkPromoProductCategory(promotion, criteria, matchedItems, productHashMap);
                    break;
                case OneFromVendors:
                    matchedItems = checkPromoProductVendor(promotion, criteria, matchedItems, productHashMap);
                    break;
                case ProductTag:
                    matchedItems = checkPromotionForProductTag(promotion, criteria, matchedItems, productHashMap);
                    break;
                case OneFromProductTags:
                    matchedItems = checkPromotionForProductTag(promotion, criteria, matchedItems, productHashMap);
                    break;
                case ByBrand:
                    matchedItems = checkPromoBrand(promotion, criteria, matchedItems, productHashMap);
                    break;
                case OneFromBrands:
                    matchedItems = checkPromoBrand(promotion, criteria, matchedItems, productHashMap);
                    break;
                default:
                    break;
            }

            if (cartPassed == false) {
                break;
            }
        }

        // All cart passed
        if (cartPassed && promotion.getPromotionType() == Promotion.PromotionType.Cart) {
            double promoDiscount = promotion.getDiscountAmt().doubleValue();
            double currDiscount = workingCart.getDiscount().doubleValue();

            if (promotion.getDiscountType() == OrderItem.DiscountType.Percentage) {
                double nondiscountedCost = 0;
                for (OrderItem orderItem : workingCart.getItems()) {
                    nondiscountedCost += orderItem.getFinalPrice().doubleValue();
                }
                double curSubtotal = nondiscountedCost - currDiscount;
                currDiscount = NumberUtils.round(currDiscount, 2);
                promoDiscount = curSubtotal * (promotion.getDiscountAmt().doubleValue() / 100);
                promoDiscount = NumberUtils.round(promoDiscount, 2);

                BigDecimal maxCashValue = promotion.getMaxCashValue();
                if (maxCashValue != null && maxCashValue.doubleValue() > 0 && promoDiscount > maxCashValue.doubleValue()) {
                    promoDiscount = maxCashValue.doubleValue();
                }
            }

            double newDiscount = currDiscount + promoDiscount;
            currDiscount = NumberUtils.round(newDiscount, 2);

            workingCart.setDiscount(new BigDecimal(currDiscount)); //currDiscount);
            workingCart.setDiscountType(OrderItem.DiscountType.Cash);


            if (incomingStatus == Transaction.TransactionStatus.Completed) {
                createPromoUsage(shop, promotion, member.getId(), transactionId);
            }

            workingCart.getPromotionReqLogs().add(createPromotionLog(promotion, promoDiscount));

        } else if (promotion.getPromotionType() == Promotion.PromotionType.DeliveryFee) {
            if (!workingCart.isEnableDeliveryFee()) {
                throw new BlazeInvalidArgException("Promotion", "Shop has no delivery fee.");
            }
            workingCart.setDeliveryDiscount(promotion.getDiscountAmt());
            workingCart.setDeliveryDiscountType(promotion.getDiscountType());

            if (incomingStatus == Transaction.TransactionStatus.Completed) {
                createPromoUsage(shop, promotion, member.getId(), transactionId);
            }
        } else if (matchedItems.size() > 0) {
            double totalDiscounts = applyProductBasedPromotions(promotion, matchedItems, weightToleranceHashMap, workingCart, productHashMap, productCategoryHashMap);
            if (totalDiscounts > 0) {
                productsApplied += 1;
            }
            if (totalDiscounts > 0 && incomingStatus == Transaction.TransactionStatus.Completed) {
                createPromoUsage(shop, promotion, member.getId(), transactionId);
            }
            if (totalDiscounts > 0) {
                PromotionReqLog promotionReqLog = createPromotionLog(promotion, totalDiscounts);
                promotionReqLog.setPromotionType(Promotion.PromotionType.Product);
                workingCart.getPromotionReqLogs().add(promotionReqLog);
            }
        }


        // check product again
        if (promotion.getPromotionType() == Promotion.PromotionType.Product) {
            if (promotion.getRules().size() > 0 && productsApplied == 0) {
                removePromotion(workingCart, promotion);
                throw new BlazeInvalidArgException("Promotion", String.format("No products matched for promo '%s'.", promotion.getName()));
            }
        }
        return true;
    }

    private void removePromotion(Cart workingCart, Promotion promotion) {

        if (!promotion.getPromoCodes().isEmpty()
                && StringUtils.isNotBlank(workingCart.getPromoCode())
                && promotion.getPromoCodes().contains(workingCart.getPromoCode())) {
            workingCart.setPromoCode(Strings.EMPTY);
        }
        workingCart.getPromotionReqs().remove(promotion);
    }

    public PromotionReqLog createPromotionLog(Promotion promotion, double discountCashAmt) {
        PromotionReqLog log = new PromotionReqLog();
        log.prepare(promotion.getCompanyId());
        log.setPromotionId(promotion.getId());
        log.setAmount(new BigDecimal(discountCashAmt));
        log.setStackable(promotion.isStackable());
        log.setFinalizedDiscountId(promotion.getFinalizedDiscountId());
        return log;
    }

    @Override
    public PromotionReqLog createPromotionLog(String promotionId, String finalizedDiscountId, String companyId, double discountCashAmt) {
        PromotionReqLog log = new PromotionReqLog();
        log.prepare(companyId);
        log.setPromotionId(promotionId);
        log.setAmount(new BigDecimal(discountCashAmt));
        log.setFinalizedDiscountId(finalizedDiscountId);
        return log;
    }

    public PromotionReqLog createPromotionRewardLog(String rewardId,String finalizedDiscountId, String companyId, double discountCashAmt) {
        PromotionReqLog log = new PromotionReqLog();
        log.prepare(companyId);
        log.setRewardId(rewardId);
        log.setPromotionType(Promotion.PromotionType.Product);
        log.setAmount(new BigDecimal(discountCashAmt));
        log.setFinalizedDiscountId(finalizedDiscountId);
        return log;
    }

    private void createPromoUsage(final Shop shop, Promotion promotion, final String memberId, final String transId) {
        PromoUsage promoUsage = promoUsageRepository.getUserFor(shop.getCompanyId(), shop.getId(), promotion.getId(), memberId, transId);

        if (promoUsage == null) {
            promoUsage = new PromoUsage();
            promoUsage.prepare(shop.getCompanyId());
            promoUsage.setShopId(shop.getId());
            promoUsage.setPromoId(promotion.getId());
            promoUsage.setMemberId(memberId);
            promoUsage.setTransId(transId);
            promoUsageRepository.save(promoUsage);
        } else {
            int cCount = promoUsage.getCount();
            cCount++;
            promoUsage.setCount(cCount);
            promoUsageRepository.update(shop.getCompanyId(), promoUsage.getId(), promoUsage); // save promo usage
        }

        Iterable<PromoUsage> promoUsages = promoUsageRepository.listForPromo(shop.getCompanyId(), shop.getId(), promotion.getId());
        int currUsage = 0;
        for (PromoUsage pu : promoUsages) {
            currUsage += pu.getCount();
        }
        promotion.setUsageCount(currUsage);
        promotionRepository.update(shop.getCompanyId(), promotion.getId(), promotion);

        realtimeService.sendRealTimeEvent(shop.getId(), RealtimeService.RealtimeEventType.PromotionUpdateEvent, null);
    }

    private boolean checkPromoCartOrderMin(PromotionRule criteria, Cart workingCart) {
        if (criteria.getMinAmt() != null) {
            double amt = criteria.getMinAmt().intValue();

            // Check cost of cart without discounts
            double cost = 0.0;
            for (OrderItem orderItem : workingCart.getItems()) {
                if (orderItem.getStatus() == OrderItem.OrderItemStatus.Refunded) {
                    continue;
                }
                cost += orderItem.getCost().doubleValue();
            }
            return cost >= amt;
        }
        return true;
    }


    private boolean checkPromoLastVisit(Cart workingCart, Member member) {
        Long sixtyDays = (long) 60 * 60 * 24 * 60 * 1000; //60s * 60min * 24hrs * 60days * 1000ms

        if (member == null) {
            return true;
        }
        Long lastVisitDate = member.getLastVisitDate();
        Long diff = DateTime.now().getMillis() - lastVisitDate;
        if (diff > sixtyDays) {
            return true;
        }
        return false;
    }

    private boolean checkPromoFirstTimeMember(Shop shop, PromotionRule promotion, Cart workingCart, Member member) {
        if (member == null) {
            return true;
        }
        long totalVisits = transactionRepository.countTransactionsByMember(shop.getCompanyId(), shop.getId(), member.getId());
        return totalVisits == 0;
    }

    private boolean checkPromoMinOrdersPrevious(final Shop shop, PromotionRule promotion, Cart workingCart, Member member) {
        if (member == null) {
            return true;
        }
        long totalVisits = transactionRepository.countTransactionsByMember(shop.getCompanyId(), shop.getId(), member.getId());

        if (promotion.getMinAmt() != null) {
            long minOrders = promotion.getMinAmt().longValue();
            return minOrders <= totalVisits;
        }

        return true;

    }

    private boolean isAvailableToday(final Promotion promotion, final Shop shop) {
        if (shop != null) {
            String timeZone = shop.getTimeZone();
            int timeZoneOffset = DateUtil.getTimezoneOffsetInMinutes(timeZone);

            DateTime dateTime = DateTime.now().toDateTime(DateTimeZone.forID("UTC"));
            int dayOfWeek = dateTime.plusMinutes(timeZoneOffset).withTimeAtStartOfDay().getDayOfWeek();
            long millis = dateTime.plusMinutes(timeZoneOffset).withTimeAtStartOfDay().getMillis();
            LOG.info("Day of Week: " + dayOfWeek + "   millis: " + millis + "  timezoneOffset: " + timeZoneOffset + "  timezone" + timeZone);
            switch (dayOfWeek) {
                case DateTimeConstants.MONDAY:
                    return promotion.isMon();
                case DateTimeConstants.TUESDAY:
                    return promotion.isTues();
                case DateTimeConstants.WEDNESDAY:
                    return promotion.isWed();
                case DateTimeConstants.THURSDAY:
                    return promotion.isThur();
                case DateTimeConstants.FRIDAY:
                    return promotion.isFri();
                case DateTimeConstants.SATURDAY:
                    return promotion.isSat();
                case DateTimeConstants.SUNDAY:
                    return promotion.isSun();
            }
        }

        return true;
    }

    private boolean checkPromoCartVendor(PromotionRule criteria,
                                         Cart workingCart,
                                         final HashMap<String, Product> productHashMap) {
        for (OrderItem orderItem : workingCart.getItems()) {
            if (orderItem.getStatus() == OrderItem.OrderItemStatus.Refunded) {
                continue;
            }
            Product product = productHashMap.get(orderItem.getProductId());
            if (criteria.getVendorIds() != null) {
                for (String vendorId : criteria.getVendorIds()) {
                    if (vendorId.equals(product.getVendorId())) {
                        return true;
                    }
                }
            }
            if (!Objects.isNull(criteria.getVendorId()) && criteria.getVendorId().equals(product.getVendorId())) {
                return true;
            }


        }
        return false;
    }

    private boolean checkPromoCartVendorForDeliveryFee(PromotionRule criteria,
                                                       Cart workingCart,
                                                       final HashMap<String, Product> productHashMap) {
        for (OrderItem orderItem : workingCart.getItems()) {
            if (orderItem.getStatus() == OrderItem.OrderItemStatus.Refunded) {
                continue;
            }
            Product product = productHashMap.get(orderItem.getProductId());
            if (product == null) {
                throw new BlazeInvalidArgException("Product", "Product does not found");
            }

            if (criteria.getVendorIds() != null && criteria.getVendorIds().size() > 0) {
                for (String vendorId : criteria.getVendorIds()) {
                    if (vendorId.equalsIgnoreCase(product.getVendorId())) {
                        return true;
                    }
                }

            }

            if (!Objects.isNull(criteria.getVendorId()) && criteria.getVendorId().equals(product.getVendorId())) {
                return true;
            }
        }
        return false;
    }

    private List<OrderItem> filterMatch(Promotion promotion, List<OrderItem> matchedItems) {

        if (promotion.getDiscountType() == OrderItem.DiscountType.Cash) {
            // see if we can find a match with same unit price
            LinkedHashSet<OrderItem> filtered = new LinkedHashSet<>();
            for (OrderItem selectedItem : matchedItems) {

                boolean success = true;
                for (PromotionRule criteria : promotion.getRules()) {
                    success = checkProductQuantityRule(criteria, selectedItem);
                    if (!success) {
                        break;
                    }
                }

                if (success) {
                    int unitPrice = (int) (selectedItem.getUnitPrice().doubleValue() * 100);
                    int finalPrice = (int) (selectedItem.getFinalPrice().doubleValue() * 100);
                    int discountAmt = (int) (promotion.getDiscountAmt().doubleValue() * 100);

                    if (unitPrice == discountAmt && unitPrice <= finalPrice) {
                        filtered.add(selectedItem);
                    }
                }
            }
            if (filtered.size() > 0) {
                return Lists.newArrayList(filtered);
            }
        }
        return matchedItems;
    }

    private List<OrderItem> filterMatchByQuantity(Promotion promotion, PromotionRule criteria, List<OrderItem> matchedItems) {

        BigDecimal minQty = criteria.getMinAmt();
        List<OrderItem> filteredList = new ArrayList<>();

        BigDecimal availQty = new BigDecimal(0);
        for (OrderItem item : matchedItems) {
            if (item.getAvailableDiscountQty().compareTo(BigDecimal.ZERO) > 0) {
                filteredList.add(item);
                availQty = availQty.add(item.getAvailableDiscountQty());
                if (availQty.compareTo(minQty) >= 0) {
                    return filteredList;
                }
            }
        }

        // no available matches
        if (availQty.compareTo(minQty) < 0) {
            return new ArrayList<>();
        }

        return filteredList;
    }



    private List<OrderItem> checkPromoProductVendor(Promotion promotion, PromotionRule criteria,
                                                    final List<OrderItem> matchedItems,
                                                    final HashMap<String, Product> productHashMap) {
        List<OrderItem> matchedOrderItems = new ArrayList<>();
        List<OrderItem> orderItems = sortOrderItems(matchedItems,promotion);
        for (OrderItem orderItem : orderItems) {
            if (orderItem.getStatus() == OrderItem.OrderItemStatus.Refunded || orderItem.getAvailableDiscountQty().doubleValue() <= 0) {
                continue;
            }
            Product product = productHashMap.get(orderItem.getProductId());
            if (criteria.getVendorIds() != null && criteria.getVendorIds().size() > 0) {
                for (String vendorId : criteria.getVendorIds()) {
                    if (vendorId.equals(product.getVendorId())) {
                        // apply the product based promotions
                        matchedOrderItems.add(orderItem);
                    }
                }
            } else if (criteria.getVendorId().equals(product.getVendorId())) {
                // apply the product based promotions
                matchedOrderItems.add(orderItem);
            }

        }

        matchedOrderItems = filterMatch(promotion, matchedOrderItems);

        if (criteria.getRuleType() == PromotionRule.PromotionRuleType.OneFromVendors) {
            return filterMatchByQuantity(promotion,criteria,matchedOrderItems);
        }
        return matchedOrderItems;
    }

    private List<OrderItem> checkPromoProductCategory(Promotion promotion, PromotionRule criteria,
                                                      final List<OrderItem> matchedItems,
                                                      final HashMap<String, Product> productHashMap) {
        List<OrderItem> matchedOrderItems = new ArrayList<>();
        if (promotion.getPromotionType() == Promotion.PromotionType.Product) {
            for (OrderItem orderItem : matchedItems) {
                if (orderItem.getStatus() == OrderItem.OrderItemStatus.Refunded || orderItem.getAvailableDiscountQty().doubleValue() <= 0) {
                    continue;
                }
                Product product = productHashMap.get(orderItem.getProductId());
                if (product != null) {
                    if (criteria.getCategoryIds() != null && criteria.getCategoryIds().size() > 0) {
                        for (String categoryId : criteria.getCategoryIds()) {
                            if (categoryId.equalsIgnoreCase(product.getCategoryId())) {

                                matchedOrderItems.add(orderItem);
                            }
                        }
                    } else if (product.getCategoryId().equalsIgnoreCase(criteria.getCategoryId())) {
                        matchedOrderItems.add(orderItem);
                    }
                }
            }
        }

        matchedOrderItems = filterMatch(promotion, matchedOrderItems);

        if (criteria.getRuleType() == PromotionRule.PromotionRuleType.OneFromCategories) {
            return filterMatchByQuantity(promotion,criteria,matchedOrderItems);
        }
        return matchedOrderItems;
    }

    private List<OrderItem> checkPromoProduct(Promotion promotion, PromotionRule criteria,
                                              final List<OrderItem> matchedItems) {
        List<OrderItem> matchedOrderItems = new ArrayList<>();
        if (promotion.getPromotionType() == Promotion.PromotionType.Product) {
            for (OrderItem orderItem : matchedItems) {
                if (orderItem.getStatus() == OrderItem.OrderItemStatus.Refunded || orderItem.getAvailableDiscountQty().doubleValue() <= 0) {
                    continue;
                }
                if (criteria.getProductIds() != null && criteria.getProductIds().size() > 0) {
                    for (String productId : criteria.getProductIds()) {
                        if (orderItem.getProductId().equalsIgnoreCase(productId)) {
                            // check min and max
                            matchedOrderItems.add(orderItem);
                        }
                    }
                } else if (orderItem.getProductId().equalsIgnoreCase(criteria.getProductId())) {
                    // check min and max
                    matchedOrderItems.add(orderItem);
                }

            }
        }

        matchedOrderItems = filterMatch(promotion, matchedOrderItems);

        if (criteria.getRuleType() == PromotionRule.PromotionRuleType.OneFromProducts) {
            return filterMatchByQuantity(promotion,criteria,matchedOrderItems);
        }
        return matchedOrderItems;
    }


    private double applyProductBasedPromotions(final Promotion promotion,
                                               List<OrderItem> matchedOrderItems,
                                               final HashMap<String, ProductWeightTolerance> weightToleranceHashMap,
                                               final Cart workingCart,
                                               final HashMap<String, Product> productHashMap,
                                               final HashMap<String, ProductCategory> productCategoryHashMap) {


        double totalDiscounts = 0d;
        if (promotion.isEnableBOGO() && promotion.getDiscountType() == OrderItem.DiscountType.Percentage) {
            // BOGO AND IT'S PERCENTAGE
            double discount = promotion.getDiscountAmt().doubleValue();
            ProductWeightTolerance.WeightKey discountType = promotion.getDiscountUnitType();


            PromotionRule promotionRule = null;
            for (PromotionRule rule : promotion.getRules()) {
                // find the first promotion rule for product
                if (rule.getRuleType().isProductRule()){
                    promotionRule = rule;
                    break;
                }
            }

            // min
            BigDecimal requiredQty =  BigDecimal.ZERO;

            BigDecimal availableQty = BigDecimal.ZERO;
            for (OrderItem orderItem : matchedOrderItems) {
                availableQty = availableQty.add(orderItem.getAvailableDiscountQty());
            }
            if (availableQty.doubleValue() <= 0) {
                return totalDiscounts;
            }


            for (OrderItem orderItem : matchedOrderItems) {

                if (orderItem.getStatus() == OrderItem.OrderItemStatus.Refunded || orderItem.getAvailableDiscountQty().doubleValue() <= 0) {
                    continue;
                }
                BigDecimal origAvailQty = orderItem.getAvailableDiscountQty();

                // deduct accordingly
                if (availableQty.compareTo(BigDecimal.ZERO) >= 0
                        && requiredQty.compareTo(BigDecimal.ZERO) > 0) {
                    BigDecimal qtyLeft = orderItem.getAvailableDiscountQty().subtract(requiredQty);
                    if (qtyLeft.doubleValue() >0) {
                        orderItem.setAvailableDiscountQty(qtyLeft);
                        requiredQty = BigDecimal.ZERO;
                    } else {
                        orderItem.setAvailableDiscountQty(BigDecimal.ZERO);
                        requiredQty = qtyLeft.abs();
                        continue;
                    }
                }


                if (promotionRule != null) {
                    Product product = productHashMap.get(orderItem.getProductId());
                    if (product != null) {
                        ProductCategory category = productCategoryHashMap.get(product.getCategoryId());
                        if (category != null) {
                            DiscountResult discountResult = applyProductBasedBOGOPromotions(promotion, promotionRule, product, category, orderItem, weightToleranceHashMap, workingCart,availableQty);


                            double finalCost = orderItem.getCost().doubleValue() - discountResult.discountedAmt.doubleValue();

                            if (finalCost < 0) {
                                finalCost = 0;
                                orderItem.setDiscount(orderItem.getCost());
                                totalDiscounts += orderItem.getCost().doubleValue();
                            } else {
                                totalDiscounts += discountResult.discountedAmt.doubleValue();
                            }
                            orderItem.setFinalPrice(new BigDecimal(finalCost));

                            BigDecimal qtyLeft = orderItem.getAvailableDiscountQty().subtract(discountResult.discountedQty);
                            if (qtyLeft.doubleValue() >0) {
                                orderItem.setAvailableDiscountQty(qtyLeft);
                                requiredQty = BigDecimal.ZERO;
                                availableQty = qtyLeft; // assigned qty left
                            } else {
                                availableQty = availableQty.subtract(discountResult.discountedQty);
                                orderItem.setAvailableDiscountQty(BigDecimal.ZERO);
                                requiredQty = qtyLeft.abs();
                            }
                        }
                    }
                }

                // reset if it's stackable
                if (promotion.isStackable()) {
                    orderItem.setAvailableDiscountQty(origAvailQty);
                }
            }

        } else {
            // normal promotions
            // non-bogo, just apply this thing
            totalDiscounts = applyNormalProductDiscounts(promotion, matchedOrderItems, weightToleranceHashMap, workingCart, productHashMap, productCategoryHashMap);
        }

        return totalDiscounts;
    }


    private double applyNormalProductDiscounts(final Promotion promotion,
                                               List<OrderItem> matchedOrderItems,
                                               final HashMap<String, ProductWeightTolerance> weightToleranceHashMap,
                                               final Cart workingCart,
                                               final HashMap<String, Product> productHashMap,
                                               final HashMap<String, ProductCategory> productCategoryHashMap) {

        double totalDiscounts = 0d;
        // non-bogo, just apply this thing
        for (OrderItem orderItem : matchedOrderItems) {
            if (orderItem.getStatus() == OrderItem.OrderItemStatus.Refunded || orderItem.getAvailableDiscountQty().doubleValue() <= 0) {
                continue;
            }

            // make sure all rules are met
            double multiplier = 1;
            boolean success = true;
            boolean shouldMultiply = false;
            PromotionRule promotionRule = null;
            for (PromotionRule criteria : promotion.getRules()) {
                success = checkProductQuantityRule(criteria, orderItem);

                if (criteria.getRuleType().isMultiple()) {
                    promotionRule = criteria;
                    if (promotion.isScalable()) {
                        if (criteria.getMinAmt() != null) {
                            int minQty = (int) (criteria.getMinAmt().doubleValue() * 100); // multiply by 100
                            int qty = (int) (orderItem.getAvailableDiscountQty().doubleValue() * 100); // multiply by 100
                            if (minQty > 0 && qty > 0) {
                                multiplier = Math.floor(qty / minQty);
                            }
                            shouldMultiply = true;
                        }
                    } else if (promotion.getDiscountType() == OrderItem.DiscountType.Percentage) {

                        int minQty = (int) (criteria.getMinAmt().doubleValue() * 100); // multiply by 100
                        int qty = (int) (orderItem.getAvailableDiscountQty().doubleValue() * 100); // multiply by 100
                        if (minQty > 0 && qty > 0) {
                            multiplier = Math.floor(qty / minQty);
                        }
                        shouldMultiply = true;
                    }
                } else if (criteria.getRuleType().isOneOf()) {
                    promotionRule = criteria;
                    multiplier = 1;
                }
                if (!success) {
                    break;
                }
            }


            // All rules are met, now, let's calculate the discount
            if (success) {
                // Always cash
                orderItem.setDiscountType(OrderItem.DiscountType.Cash);
                double discount = promotion.getDiscountAmt().doubleValue();
                BigDecimal discountQty = BigDecimal.ZERO;

                // calculate cash value of this promotion
                if (promotion.getDiscountType() == OrderItem.DiscountType.Percentage) {
                    discount = orderItem.getFinalPrice().doubleValue() * (promotion.getDiscountAmt().doubleValue() / 100);
                    discount = NumberUtils.round(discount, 2);

                    BigDecimal maxCashValue = promotion.getMaxCashValue();
                    if (maxCashValue != null && maxCashValue.doubleValue() > 0 && discount > maxCashValue.doubleValue()) {
                        discount = maxCashValue.doubleValue();
                    }

                    // calculate discounts with multipliers
                    Product product = productHashMap.get(orderItem.getProductId());
                    ProductCategory category = productCategoryHashMap.get(product.getCategoryId());
                    // convert to cash value
                    if (category.getUnitType() == ProductCategory.UnitType.grams
                            && product.getPriceRanges().size() > 0
                            && promotion.getDiscountUnitType() != ProductWeightTolerance.WeightKey.UNIT) {
                        // Now calculate the discount
                        ProductWeightTolerance weightTolerance = weightToleranceHashMap.get(promotion.getDiscountUnitType().weightName);
                        // Get the price Range
                        if (weightTolerance != null) {
                            ProductPriceRange priceRange = null;

                            for (ProductPriceRange productPriceRange : product.getPriceRanges()) {
                                if (productPriceRange.getWeightToleranceId().equalsIgnoreCase(weightTolerance.getId())) {
                                    priceRange = productPriceRange;
                                    break;
                                }
                            }

                            if (priceRange != null) {
                                // Discount is the ratio of priceRange price
                                if (shouldMultiply && StringUtils.isNotBlank(orderItem.getPrepackageItemId())) {
                                    discount = multiplier * priceRange.getPrice().doubleValue() * (promotion.getDiscountAmt().doubleValue() / 100);
                                } else {
                                    discount = priceRange.getPrice().doubleValue() * (promotion.getDiscountAmt().doubleValue() / 100);
                                }
                                discount = NumberUtils.round(discount, 2);

                                if (StringUtils.isBlank(orderItem.getPrepackageItemId())) {
                                    discountQty = weightTolerance.getUnitValue();
                                } else {
                                    discountQty = new BigDecimal(1);
                                }
                            }
                        }

                    } else {
                        if (promotionRule != null && promotionRule.getRuleType().isOneOf()) {
                            // single product rule
                            // we only discount either the full unit price of stackable item

                            // if this item is less than original unit price
                            if (orderItem.getFinalPrice().doubleValue() < orderItem.getUnitPrice().doubleValue()) {
                                discount = orderItem.getFinalPrice().doubleValue() * ((promotion.getDiscountAmt().doubleValue() / 100));
                                discount = NumberUtils.round(discount, 2);
                            } else {
                                discount = orderItem.getUnitPrice().doubleValue() * ((promotion.getDiscountAmt().doubleValue() / 100));
                                discount = NumberUtils.round(discount, 2);
                            }
                            discountQty = new BigDecimal(1);
                        } else if (promotionRule != null && promotionRule.getRuleType().isMultiple()) {


                            if (orderItem.getQuantity().compareTo(orderItem.getAvailableDiscountQty()) == 0) {
                                discount = orderItem.getFinalPrice().doubleValue() * ((promotion.getDiscountAmt().doubleValue() / 100));
                                discount = NumberUtils.round(discount, 2);
                            } else {
                                discount = multiplier * orderItem.getUnitPrice().doubleValue() * ((promotion.getDiscountAmt().doubleValue() / 100));
                                discount = NumberUtils.round(discount, 2);
                            }

                            discountQty = BigDecimal.valueOf(multiplier);


                        } else {
                            if (orderItem.getFinalPrice().doubleValue() < orderItem.getUnitPrice().doubleValue()) {
                                discount = multiplier * orderItem.getFinalPrice().doubleValue() * ((promotion.getDiscountAmt().doubleValue() / 100));
                                discount = NumberUtils.round(discount, 2);
                            } else {
                                discount = multiplier * orderItem.getUnitPrice().doubleValue() * ((promotion.getDiscountAmt().doubleValue() / 100));
                                discount = NumberUtils.round(discount, 2);
                            }
                            discountQty = BigDecimal.valueOf(multiplier);
                        }
                    }
                } else {
                    if (promotion.isScalable() && shouldMultiply && multiplier > 0) {
                        discount = discount * multiplier;
                    }
                }
                if (discount <= 0) {
                    discountQty = BigDecimal.ZERO;
                }


                double currDiscount = orderItem.getDiscount().doubleValue();
                double newDiscount = currDiscount + discount;

                totalDiscounts += discount;

                orderItem.setDiscount(new BigDecimal(newDiscount));
                if (discount > 0) {
                    orderItem.getPromotionReqs().add(new PromotionReq(promotion.getId(), null, promotion.getFinalizedDiscountId()));
                }

                double finalCost = orderItem.getCost().doubleValue() - orderItem.getDiscount().doubleValue();
                if (finalCost < 0) {
                    finalCost = 0;
                    orderItem.setDiscount(orderItem.getCost());
                }
                orderItem.setFinalPrice(new BigDecimal(finalCost));



                orderItem.setDiscountedQty(orderItem.getDiscountedQty().add(discountQty));
                BigDecimal availableDiscQty = orderItem.getAvailableDiscountQty();
                orderItem.setAvailableDiscountQty(availableDiscQty.subtract(discountQty));

            }
        }

        return totalDiscounts;
    }


    private DiscountResult applyProductBasedBOGOPromotions(final Promotion promotion, final PromotionRule criteria,
                                                   final Product product, final ProductCategory category, OrderItem orderItem,
                                                   final HashMap<String, ProductWeightTolerance> weightToleranceHashMap,
                                                   final Cart workingCart,
                                                           final BigDecimal totalAvailableQty) {

        double discountsApplied = 0;
        // check min and max
        double minAmt = -1;
        double maxAmt = Double.MAX_VALUE;
        if (criteria.getMinAmt() != null) {
            minAmt = criteria.getMinAmt().doubleValue();
        }
        if (criteria.getMaxAmt() != null && criteria.getMaxAmt().doubleValue() > 0) {
            maxAmt = criteria.getMaxAmt().doubleValue();
        }


        // check if quantity matches with requirement
        BigDecimal quantity = new BigDecimal(0);
        quantity = totalAvailableQty;
        orderItem.setDiscountType(OrderItem.DiscountType.Cash);

        double discount = promotion.getDiscountAmt().doubleValue();
        BigDecimal appliedQty = criteria.getMinAmt();

        // we're in BOGO LAND
        if (minAmt > 0) {
            if (category.getUnitType() == ProductCategory.UnitType.grams
                    && product.getPriceRanges().size() > 0
                    && promotion.getDiscountUnitType() != ProductWeightTolerance.WeightKey.UNIT) {
                // Now calculate the discount
                ProductWeightTolerance weightTolerance = weightToleranceHashMap.get(promotion.getDiscountUnitType().weightName);
                // Get the price Range
                if (weightTolerance != null) {
                    ProductPriceRange priceRange = null;

                    for (ProductPriceRange productPriceRange : product.getPriceRanges()) {
                        if (productPriceRange.getWeightToleranceId().equalsIgnoreCase(weightTolerance.getId())) {
                            priceRange = productPriceRange;
                            break;
                        }
                    }

                    if (priceRange != null) {
                        // Discount is the ratio of priceRange price
                        double ratio = Math.floor(quantity.doubleValue() / minAmt);
                        if (criteria.getRuleType().isOneOf()) {
                            ratio = 1;
                        }
                        appliedQty = appliedQty.multiply(BigDecimal.valueOf(ratio));
                        discount = priceRange.getPrice().doubleValue() * ratio * (promotion.getDiscountAmt().doubleValue() / 100);
                        discount = NumberUtils.round(discount, 2);
                    }
                }

            } else {
                double ratio = Math.floor(quantity.doubleValue() / minAmt);
                if (criteria.getRuleType().isOneOf()) {
                    ratio = 1;
                }
                appliedQty = appliedQty.multiply(BigDecimal.valueOf(ratio));
                discount = orderItem.getUnitPrice().doubleValue() * (ratio * (promotion.getDiscountAmt().doubleValue() / 100));
                discount = NumberUtils.round(discount, 2);
            }


            if (Promotion.PromotionType.Product == promotion.getPromotionType() && !promotion.isStackable() && orderItem.getDiscount().doubleValue() > 0) {
                //throw new BlazeInvalidArgException(PROMOTION, "Promotion is stackable, so '"+ promotion.getName() +"' can not apply");
                discount = 0d;
            }

            // ADD NEW DISCOUNT TO OLD DISCOUNT
            double currDiscount = orderItem.getDiscount().doubleValue();
            double newDiscount = currDiscount + discount;
            discountsApplied = discount;

            orderItem.setDiscount(new BigDecimal(newDiscount));
            if (discount > 0) {
                orderItem.getPromotionReqs().add(new PromotionReq(promotion.getId(), null,promotion.getFinalizedDiscountId()));
            }

            double finalCost = orderItem.getCost().doubleValue() - orderItem.getDiscount().doubleValue();

            if (finalCost < 0) {
                finalCost = 0;
                orderItem.setDiscount(orderItem.getCost());
            }
            orderItem.setFinalPrice(new BigDecimal(finalCost));
        }

        DiscountResult result = new DiscountResult();
        result.discountedAmt = BigDecimal.valueOf(discountsApplied);
        result.discountedQty = appliedQty;
        return result;
    }

    private boolean checkProductQuantityRule(final PromotionRule criteria, OrderItem orderItem) {


        if (criteria.getRuleType().isProductRule()) {
            // check min and max
            double minAmt = -1;
            double maxAmt = Double.MAX_VALUE;
            if (criteria.getMinAmt() != null) {
                minAmt = criteria.getMinAmt().doubleValue();
            }
            if (criteria.getMaxAmt() != null && criteria.getMaxAmt().doubleValue() > 0) {
                maxAmt = criteria.getMaxAmt().doubleValue();
            }

            // check if quantity matches with requirement
            BigDecimal quantity = orderItem.getAvailableDiscountQty();

            boolean met = quantity.doubleValue() >= minAmt
                    && quantity.doubleValue() <= maxAmt;

            return met;
        }

        return true;
    }


    private List<OrderItem> sortOrderItems(List<OrderItem> orderItems, OrderedDiscountable discountable) {
        List<OrderItem> orderItems2 = Lists.newArrayList(orderItems);
        int order = discountable.applyLowestPriceFirst();
        orderItems2.sort(new Comparator<OrderItem>() {
            @Override
            public int compare(OrderItem o1, OrderItem o2) {
                if (o1.getFinalPrice().doubleValue() > 0 && o2.getFinalPrice().doubleValue() > 0) {
                    if (o1.getUnitPrice().compareTo(o1.getFinalPrice()) > 0) {
                        // unit price is greater than final price
                        return 1*order;
                    } else {
                        int c0 = (o2.getUnitPrice().compareTo(o1.getUnitPrice()));
                        if (c0 == 0) {

                            if (o1.hasSomeDiscounts() && !o2.hasSomeDiscounts()) {
                                return 1;
                            }

                            int c1 = (o2.getFinalPrice().compareTo(o1.getFinalPrice()));

                            if (c1 == 0) {
                                return o2.getCalcDiscount().compareTo(o1.getCalcDiscount())*order; // unless there are discounts
                            }
                            return c1; // show highest order items first
                        } else {
                            return c0*order;
                        }
                    }
                } else {
                    int c1 = (o2.getFinalPrice().compareTo(o1.getFinalPrice()));

                    if (c1 == 0) {
                        return o2.getCalcDiscount().compareTo(o1.getCalcDiscount())*order;
                    }
                    return c1*order;
                }

            }
        });
        return orderItems2;
    }

    private List<OrderItem> checkPromotionForProductTag(Promotion promotion, PromotionRule criteria,
                                                        List<OrderItem> orderItem, HashMap<String, Product> productHashMap) {

        List<OrderItem> orderItems = new ArrayList<>();

        if (promotion.getPromotionType().equals(Promotion.PromotionType.Product)) {
            for (OrderItem item : orderItem) {
                if (item.getStatus() == OrderItem.OrderItemStatus.Refunded || item.getAvailableDiscountQty().doubleValue() <= 0) {
                    continue;
                }

                Product product = productHashMap.get(item.getProductId());
                if (product != null && criteria.getProductTags() != null && criteria.getProductTags().size() > 0) {
                    for (String promotionTag : criteria.getProductTags()) {
                        if (product.getTags().contains(promotionTag)) {
                            orderItems.add(item);
                        }
                    }
                }
            }
        }

        orderItems = filterMatch(promotion, orderItems);

        if (criteria.getRuleType() == PromotionRule.PromotionRuleType.OneFromProductTags) {
            if (orderItems.size() > 0) {
                return filterMatchByQuantity(promotion,criteria,orderItems);

            }
        }
        return orderItems;
    }

    private void applyRewardOneFromSelectedProductTags(LoyaltyReward reward, Cart workingCart, HashMap<String, Product> productHashMap, HashMap<String, ProductCategory> productCategoryHashMap, HashMap<String, ProductWeightTolerance> weightToleranceHashMap) {
        List<OrderItem> orderItems = sortOrderItems(workingCart.getItems(),reward);
        LinkedHashSet<OrderItem> matchedItems = new LinkedHashSet<>();
        for (OrderItem orderItem : orderItems) {
            if (orderItem.getStatus() == OrderItem.OrderItemStatus.Refunded || orderItem.getAvailableDiscountQty().doubleValue() <= 0) {
                continue;
            }
            Product product = productHashMap.get(orderItem.getProductId());

            if (product != null && product.getTags() != null && reward.getProductTags() != null && reward.getProductTags().size() > 0) {
                for (String promotionTag : reward.getProductTags()) {
                    if (product.getTags().contains(promotionTag)) {
                        matchedItems.add(orderItem);
                        break;
                    }
                }
            }
        }
        matchedItems = filterRewardMatch(reward, matchedItems);
        for (OrderItem selectedItem : matchedItems) {
            // Apply the first one we find
            PromotionReqLog log = applyRewardDiscountForProduct(reward, selectedItem, productHashMap, productCategoryHashMap, weightToleranceHashMap);
            workingCart.getPromotionReqLogs().add(log);
            return;
        }
        throw new BlazeInvalidArgException("Rewards", "No products for selected products for reward: " + reward.getName());
    }

    private List<OrderItem> checkPromoBrand(Promotion promotion, PromotionRule criteria,
                                            final List<OrderItem> matchedItems, final HashMap<String, Product> productHashMap) {
        List<OrderItem> matchedOrderItems = new ArrayList<>();
        if (promotion.getPromotionType() == Promotion.PromotionType.Product) {
            for (OrderItem orderItem : matchedItems) {
                if (orderItem.getStatus() == OrderItem.OrderItemStatus.Refunded || orderItem.getAvailableDiscountQty().doubleValue() <= 0) {
                    continue;
                }
                Product product = productHashMap.get(orderItem.getProductId());
                if (criteria.getBrandIds() != null && criteria.getBrandIds().size() > 0) {

                    if (criteria.getBrandIds().contains(product.getBrandId())) {
                        matchedOrderItems.add(orderItem);
                    }
                }

            }
        }

        matchedOrderItems = filterMatch(promotion, matchedOrderItems);

        if (criteria.getRuleType() == PromotionRule.PromotionRuleType.OneFromBrands) {
            return filterMatchByQuantity(promotion,criteria,matchedOrderItems);
        }
        return matchedOrderItems;
    }



    class DiscountResult {
        BigDecimal discountedQty = new BigDecimal(0);
        BigDecimal discountedAmt = new BigDecimal(0);
    }
}


