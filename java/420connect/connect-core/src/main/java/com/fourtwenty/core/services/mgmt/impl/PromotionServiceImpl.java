package com.fourtwenty.core.services.mgmt.impl;

import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.loyalty.DiscountVersion;
import com.fourtwenty.core.domain.models.loyalty.Promotion;
import com.fourtwenty.core.domain.models.loyalty.PromotionRule;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.domain.repositories.loyalty.DiscountVersionRepository;
import com.fourtwenty.core.domain.repositories.loyalty.LoyaltyRewardRepository;
import com.fourtwenty.core.domain.repositories.loyalty.LoyaltyUsageLogRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.rest.dispensary.requests.promotions.PromotionAddRequest;
import com.fourtwenty.core.rest.dispensary.requests.promotions.PromotionRequest;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.promotion.PromotionResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.common.RealtimeService;
import com.fourtwenty.core.services.mgmt.PromotionService;
import com.fourtwenty.core.util.DateUtil;
import com.google.common.collect.Sets;
import com.google.inject.Inject;
import com.google.inject.Provider;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by mdo on 1/3/17.
 */
public class PromotionServiceImpl extends AbstractAuthServiceImpl implements PromotionService {
    private static final Log LOG = LogFactory.getLog(PromotionServiceImpl.class);
    @Inject
    PromotionRepository promotionRepository;
    @Inject
    PromoUsageRepository promoUsageRepository;
    @Inject
    TransactionRepository transactionRepository;
    @Inject
    ProductRepository productRepository;
    @Inject
    ShopRepository shopRepository;
    @Inject
    RealtimeService realtimeService;
    @Inject
    MemberRepository memberRepository;
    @Inject
    LoyaltyRewardRepository rewardRepository;
    @Inject
    LoyaltyUsageLogRepository loyaltyUsageLogRepository;
    @Inject
    DiscountVersionRepository discountVersionRepository;


    @Inject
    public PromotionServiceImpl(Provider<ConnectAuthToken> tokenProvider) {
        super(tokenProvider);
    }

    @Override
    public Promotion addPromotion(PromotionAddRequest request) {
        Promotion promotion = promotionRepository.getPromotionByName(token.getCompanyId(), token.getShopId(), request.getName());
        if (promotion != null) {
            throw new BlazeInvalidArgException("NAME", "Promotion is already exist with this name");
        }

        Promotion promo = new Promotion();
        promo.setName(request.getName());
        promo.setPromotionType(request.getPromotionType());
        promo.prepare(token.getCompanyId());
        promo.setShopId(token.getShopId());
        promo.setDiscountAmt(request.getDiscountAmt());
        promo.setSun(true);
        promo.setMon(true);
        promo.setTues(true);
        promo.setWed(true);
        promo.setThur(true);
        promo.setFri(true);
        promo.setSat(true);
        promo.setPromoSource(request.getPromoSource());
        promo.setActive(request.isActive());
        if (request.getDiscountType() != null) {
            promo.setDiscountType(request.getDiscountType());
        }
        promo.setStackable(request.isStackable());

        Promotion dbPromo = promotionRepository.save(promo);

        //Save new promotion in discount version collection
        DiscountVersion discountVersion = new DiscountVersion(dbPromo);
        discountVersionRepository.save(discountVersion);

        realtimeService.sendRealTimeEvent(token.getShopId().toString(), RealtimeService.RealtimeEventType.PromotionUpdateEvent, null);
        return promo;
    }

    @Override
    public Promotion findPromotionWithCode(String promoCode) {
        Promotion otherPromo = promotionRepository.getPromotionByPromoCode(token.getCompanyId(), token.getShopId(), promoCode);
        return otherPromo;
    }


    @Override
    public PromotionResult updatePromotion(String promotionId, PromotionRequest promotion) {
        PromotionResult dbPromo = promotionRepository.get(token.getCompanyId(), promotionId, PromotionResult.class);

        if (dbPromo == null) {
            throw new BlazeInvalidArgException("Promotion", "Promotion not found.");
        }

        if (!dbPromo.getName().equals(promotion.getName())) {
            Promotion promotionByName = promotionRepository.getPromotionByName(token.getCompanyId(), token.getShopId(), promotion.getName());
            if (promotionByName != null) {
                throw new BlazeInvalidArgException("NAME", "Promotion is already exist with this name");
            }
        }

        if (promotion.getPromoCodes() == null) {
            promotion.setPromoCodes(Sets.newHashSet());
        }

        // clean incoming promocodes
        HashSet<String> promoCodes = Sets.newHashSet();

        for (String promoCode : promotion.getPromoCodes()) {
            if (promoCode != null) {
                promoCodes.add(promoCode.trim());
            }
        }
        promotion.setPromoCodes(promoCodes);

        if (!promotion.getPromoCodes().isEmpty()) {
            List<Promotion> promotionList = promotionRepository.getPromotionByPromoCode(token.getCompanyId(), token.getShopId(), promotion.getPromoCodes())
                    .stream()
                    .filter(p -> !promotion.getId().equalsIgnoreCase(p.getId()))
                    .collect(Collectors.toList());
            if (!promotionList.isEmpty() && promotionList.stream().anyMatch(p -> !Collections.disjoint(promotion.getPromoCodes(), p.getPromoCodes()))) {
                throw new BlazeInvalidArgException("Promotion", "Promo code is in used by another promotion.");
            }
        }


        dbPromo.setName(promotion.getName());
        dbPromo.setActive(promotion.isActive());
        if (promotion.getStartDate() == null) {
            dbPromo.setStartDate(0L);
        } else {
            dbPromo.setStartDate(promotion.getStartDate());
        }
        if (promotion.getEndDate() == null) {
            dbPromo.setEndDate(0L);
        } else {
            dbPromo.setEndDate(promotion.getEndDate());
        }
        dbPromo.setPromoDesc(promotion.getPromoDesc());
        dbPromo.setConsumerFacing(promotion.isConsumerFacing());
        dbPromo.setDiscountAmt(promotion.getDiscountAmt());
        dbPromo.setDiscountType(promotion.getDiscountType());
        dbPromo.setPromoCodeRequired(promotion.isPromoCodeRequired());
        dbPromo.setPromoCodes(promotion.getPromoCodes());
        dbPromo.setRules(promotion.getRules());
        dbPromo.setMon(promotion.isMon());
        dbPromo.setTues(promotion.isTues());
        dbPromo.setWed(promotion.isWed());
        dbPromo.setThur(promotion.isThur());
        dbPromo.setFri(promotion.isFri());
        dbPromo.setSat(promotion.isSat());
        dbPromo.setSun(promotion.isSun());
        dbPromo.setPromotionType(promotion.getPromotionType());
        dbPromo.setDiscountUnitType(promotion.getDiscountUnitType());
        dbPromo.setStackable(promotion.isStackable());
        dbPromo.setLowestPriceFirst(promotion.isLowestPriceFirst());
        dbPromo.setEnableBOGO(promotion.isEnableBOGO());

        dbPromo.setEnableDayDuration(promotion.isEnableDayDuration());

        if (promotion.isEnableDayDuration()) {
            if (StringUtils.isNotBlank(promotion.getStartTime()) || StringUtils.isNotBlank(promotion.getEndTime())) {
                Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());
                if (shop == null) {
                    throw new BlazeInvalidArgException("Shop", "Shop not found");
                }
                DateTimeFormatter formatter = DateTimeFormat.forPattern("hh:mm a");

                if (StringUtils.isNotBlank(promotion.getStartTime())) {
                    DateTime startDateTime = formatter.parseDateTime(promotion.getStartTime());
                    dbPromo.setDayStartTime(DateUtil.timeAtStartWithTimeZone(shop.getTimeZone(), startDateTime).intValue());
                    dbPromo.setStartTime(promotion.getStartTime());
                }

                if (StringUtils.isNotBlank(promotion.getEndTime())) {
                    DateTime endDateTime = formatter.parseDateTime(promotion.getEndTime());
                    dbPromo.setDayEndTime(DateUtil.timeAtStartWithTimeZone(shop.getTimeZone(), endDateTime).intValue());
                    dbPromo.setEndTime(promotion.getEndTime());
                }

                LOG.info("Day Start Time: " + dbPromo.getDayStartTime() + " - " + dbPromo.getStartTime());
                LOG.info("Day End Time: " + dbPromo.getDayEndTime() + " - " + dbPromo.getEndTime());
            } else {
                throw new BlazeInvalidArgException("TimeFormat", "Time is not valid formant.");
            }
        } else {
            dbPromo.setDayStartTime(0);
            dbPromo.setDayEndTime(0);
            dbPromo.setStartTime("");
            dbPromo.setEndTime("");
        }

        dbPromo.setPromotionType(promotion.getPromotionType());
        dbPromo.setEnableMaxAvailable(promotion.isEnableMaxAvailable());
        dbPromo.setMaxAvailable(promotion.getMaxAvailable());
        dbPromo.setMaxCashValue(promotion.getMaxCashValue());

        dbPromo.setEnableLimitPerCustomer(promotion.isEnableLimitPerCustomer());
        dbPromo.setLimitPerCustomer(promotion.getLimitPerCustomer());

        dbPromo.setRestrictMemberGroups(promotion.isRestrictMemberGroups());
        dbPromo.setMemberGroupIds(promotion.getMemberGroupIds());
        dbPromo.setScalable(promotion.isScalable());

        if (dbPromo.getMemberGroupIds() == null) {
            dbPromo.setMemberGroupIds(new ArrayList<String>());
        }

        if (dbPromo.getRules() == null) {
            dbPromo.setRules(new ArrayList<PromotionRule>());
        }
        for (PromotionRule criteria : dbPromo.getRules()) {
            criteria.prepare(token.getCompanyId());
            //BACKWARD COMPATIBILITY
            if (criteria.getProductIds() != null && StringUtils.isNotBlank(criteria.getProductId())) {
                criteria.getProductIds().add(criteria.getProductId());
            }

            if (criteria.getCategoryIds() != null && StringUtils.isNotBlank(criteria.getCategoryId())) {
                criteria.getCategoryIds().add(criteria.getCategoryId());
            }

            if (criteria.getVendorIds() != null && StringUtils.isNotBlank(criteria.getVendorId())) {
                criteria.getVendorIds().add(criteria.getVendorId());
            }
        }

        promotionRepository.update(token.getCompanyId(), dbPromo.getId(), dbPromo);

        //Save updated promotion in discount version collection
        DiscountVersion discountVersion = new DiscountVersion(dbPromo);
        discountVersionRepository.save(discountVersion);

        realtimeService.sendRealTimeEvent(token.getShopId().toString(), RealtimeService.RealtimeEventType.PromotionUpdateEvent, null);
        return dbPromo;
    }

    @Override
    public void deletePromotion(String promotionId) {

        promotionRepository.removeByIdSetState(token.getCompanyId(), promotionId);
        realtimeService.sendRealTimeEvent(token.getShopId().toString(), RealtimeService.RealtimeEventType.PromotionUpdateEvent, null);
    }

    @Override
    public PromotionResult getPromotionById(String promotionId) {
        PromotionResult promotionResult = promotionRepository.get(token.getCompanyId(), promotionId, PromotionResult.class);
        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());

        if (promotionResult == null) {
            throw new BlazeInvalidArgException("Promotion", "Promotion not found");
        }

        if (shop == null) {
            throw new BlazeInvalidArgException("Shop", "Shop is not found");
        }

        if (promotionResult.isEnableDayDuration()) {
            if (promotionResult.getDayStartTime() != null) {

                Integer dayStartTime = promotionResult.getDayStartTime();

                String startTime = DateUtil.toDateTimeFormatted(dayStartTime.longValue(), shop.getTimeZone(), "hh:mm a");
                promotionResult.setStartTime(startTime);
            } else {
                promotionResult.setStartTime("");
            }

            if (promotionResult.getDayEndTime() != null) {
                Integer dayEndTime = promotionResult.getDayEndTime();
                String endTime = DateUtil.toDateTimeFormatted(dayEndTime.longValue(), shop.getTimeZone(), "hh:mm a");

                promotionResult.setEndTime(endTime);
            } else {
                promotionResult.setEndTime("");
            }
        } else {
            promotionResult.setStartTime("");
            promotionResult.setEndTime("");
        }
        return promotionResult;
    }

    @Override
    public DateSearchResult<Promotion> getPromotionsByDate(long afterDate, long beforeDate) {
        return promotionRepository.findItemsWithDate(token.getCompanyId(), token.getShopId(), afterDate, beforeDate);
    }

    @Override
    public SearchResult<Promotion> getPromotions(int start, int limit) {
        return promotionRepository.findItems(token.getCompanyId(), token.getShopId(), "{name:1}", start, limit);
    }


}
