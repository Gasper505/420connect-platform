package com.fourtwenty.core.services.mgmt.impl;

import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.config.pdf.PdfGenerator;
import com.fourtwenty.core.domain.models.App;
import com.fourtwenty.core.domain.models.company.*;
import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.models.generic.Asset;
import com.fourtwenty.core.domain.models.generic.Note;
import com.fourtwenty.core.domain.models.generic.UniqueSequence;
import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.models.purchaseorder.*;
import com.fourtwenty.core.domain.models.testsample.TestSample;
import com.fourtwenty.core.domain.models.thirdparty.MetrcAccount;
import com.fourtwenty.core.domain.models.transaction.*;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.event.BlazeEventBus;
import com.fourtwenty.core.event.purchaseorder.AssignProductBatchDetailsEvent;
import com.fourtwenty.core.event.purchaseorder.AssignProductBatchDetailsResult;
import com.fourtwenty.core.event.purchaseorder.ValidateCompleteShipmentArrivalEvent;
import com.fourtwenty.core.exceptions.BlazeAuthException;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.exceptions.BlazeOperationException;
import com.fourtwenty.core.managed.AmazonServiceManager;
import com.fourtwenty.core.rest.comments.ActivityCommentRequest;
import com.fourtwenty.core.rest.common.EmailRequest;
import com.fourtwenty.core.rest.dispensary.requests.inventory.*;
import com.fourtwenty.core.rest.dispensary.requests.inventory.BatchBundleItems.BatchItems;
import com.fourtwenty.core.rest.dispensary.response.BulkPostResponse;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.ShipmentBillResult;
import com.fourtwenty.core.rest.dispensary.results.common.AssetStreamResult;
import com.fourtwenty.core.rest.dispensary.results.common.UploadFileResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.BundleItemResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.POProductRequestResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.PurchaseOrderItemResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.PurchaseOrderResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.VendorResult;
import com.fourtwenty.core.rest.purchaseorders.POCompleteRequest;
import com.fourtwenty.core.rest.purchaseorders.POProductBORequest;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.comments.UserActivityService;
import com.fourtwenty.core.services.common.BackgroundJobService;
import com.fourtwenty.core.services.global.CompanyUniqueSequenceService;
import com.fourtwenty.core.services.mgmt.*;
import com.fourtwenty.core.services.taxes.TaxCalulationService;
import com.fourtwenty.core.services.testsample.request.POBatchAddRequest;
import com.fourtwenty.core.services.testsample.request.POBundleBatchAddRequest;
import com.fourtwenty.core.services.testsample.request.TestSampleAddRequest;
import com.fourtwenty.core.services.thirdparty.AmazonS3Service;
import com.fourtwenty.core.services.thirdparty.MetrcAccountService;
import com.fourtwenty.core.services.thirdparty.MetrcService;
import com.fourtwenty.core.services.thirdparty.models.MetrcVerifiedPackage;
import com.fourtwenty.core.util.*;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.google.inject.Provider;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.*;
import java.util.regex.Matcher;

/**
 * Created by decipher on 3/10/17 3:58 PM
 * Abhishek Samuel (Software Engineer)
 * abhishek.decipher@gmail.com
 * Decipher Zone Softwares
 * www.decipherzone.com
 */
public class PurchaseOrderServiceImpl extends AbstractAuthServiceImpl implements PurchaseOrderService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PurchaseOrderServiceImpl.class);

    private static final String PO_NOT_FOUND = "Error! Purchase order not found.";
    private static final String ATTACHMENT_NOT_FOUND = "Attachment not found.";
    private static final String PURCHASE_ORDER = "PurchaseOrder";
    private static final String CUSTOM_DATE = "Custom date";
    private static final String NOT_VALID_PRODUCT = "Please provide valid products.";
    private static final String NOT_CUSTOM_DATE = "Please provide custom date.";
    private static final String SHOP_NOT_FOUND = "Shop does not found";
    private static final String SHOP = "Shop";
    private static final String VENDOR = "Vendor";
    private static final String VENDOR_NOT_FOUND = "Vendor does not found";
    private static final String QR_CODE_ERROR = "Error while creating QR code";
    private static final String PO_HTML_RESOURCE = "/purchaseOrderPdf.html";
    private static final String PO_SHIPPING_MANIFEST_HTML_RESOURCE = "/po_shipping_manifest_form.html";
    private static final String PO_NOT_ALLOWED_FOR_CUSTOMER = "Purchase Order not allowed for customer";
    private static final String SAFE_NOT_FOUND = "Safe inventory not for shop";
    private static final String INVENTORY = "Inventory";
    private static final String NOT_ENOUGH_QUANTITY_FOR_RETURN = "Not enough inventory in safe to execute a return to vendor.";
    private static final String PRODUCT_NOT_FOUND = "Product Id not found.";
    private static final String VALID_ADJUSTMENT_AMOUNT = "Please enter valid adjustment amount";
    private static final String CUSTOMER_LICENSE_EXPIRED = "Company license has been expired";

    private PurchaseOrderRepository purchaseOrderRepository;
    private VendorRepository vendorRepository;
    private POActivityService poActivityService;
    private EmployeeService employeeService;
    private RoleService roleService;
    private BarcodeService barcodeService;
    private MetrcAccountService metrcAccountService;
    private ProductRepository productRepository;
    private ProductBatchRepository productBatchRepository;
    private ShipmentBillService shipmentBillService;
    private AmazonServiceManager amazonServiceManager;
    private ConnectConfiguration connectConfiguration;
    private CompanyRepository companyRepository;
    private InventoryService inventoryService;
    private ShopRepository shopRepository;
    private EmployeeRepository employeeRepository;
    private AppRepository appRepository;
    private ExciseTaxInfoRepository exciseTaxInfoRepository;
    private ExciseTaxInfoService exciseTaxInfoService;
    private ProductCategoryRepository productCategoryRepository;
    private UserActivityService userActivityService;
    private AmazonS3Service amazonS3Service;
    private CultivationTaxInfoService cultivationTaxInfoService;
    private CompanyAssetRepository companyAssetRepository;

    @Inject
    private MetrcService metrcService;
    @Inject
    private ProductWeightToleranceRepository weightToleranceRepository;
    @Inject
    private UniqueSequenceRepository uniqueSequenceRepository;
    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private QueuePOSServiceImpl queuePOSServiceImpl;
    @Inject
    private BatchQuantityRepository batchQuantityRepository;
    @Inject
    private TerminalRepository terminalRepository;
    @Inject
    private InventoryRepository inventoryRepository;
    @Inject
    private QueuedTransactionRepository queuedTransactionRepository;
    @Inject
    private BackgroundJobService backgroundJobService;
    @Inject
    private BlazeEventBus blazeEventBus;
    @Inject
    private PoTestSampleService poTestSampleService;
    @Inject
    private CompanyContactRepository companyContactRepository;
    @Inject
    private AdjustmentRepository adjustmentRepository;
    @Inject
    private CompanyUniqueSequenceService companyUniqueSequenceService;
    @Inject
    private TaxCalulationService taxCalulationService;
    @Inject
    private BatchActivityLogService batchActivityLogService;
    @Inject
    private ShipmentRepository shipmentRepository;
    @Inject
    public PurchaseOrderServiceImpl(Provider<ConnectAuthToken> tokenProvider,
                                    PurchaseOrderRepository purchaseOrderRepository,
                                    VendorRepository vendorRepository,
                                    POActivityService poActivityService, EmployeeService employeeService,
                                    RoleService roleService, BarcodeService barcodeService,
                                    MetrcAccountService metrcAccountService,
                                    ProductRepository productRepository,
                                    ProductBatchRepository productBatchRepository,
                                    ShipmentBillService shipmentBillService,
                                    AmazonServiceManager amazonServiceManager,
                                    ConnectConfiguration connectConfiguration,
                                    CompanyRepository companyRepository,
                                    InventoryService inventoryService,
                                    ShopRepository shopRepository,
                                    EmployeeRepository employeeRepository, AppRepository appRepository, ExciseTaxInfoRepository exciseTaxInfoRepository, ExciseTaxInfoService exciseTaxInfoService,
                                    ProductCategoryRepository productCategoryRepository,
                                    UserActivityService userActivityService, AmazonS3Service amazonS3Service,
                                    CultivationTaxInfoService cultivationTaxInfoService,
                                    CompanyAssetRepository companyAssetRepository) {
        super(tokenProvider);
        this.purchaseOrderRepository = purchaseOrderRepository;
        this.vendorRepository = vendorRepository;
        this.poActivityService = poActivityService;
        this.employeeService = employeeService;
        this.roleService = roleService;
        this.barcodeService = barcodeService;
        this.metrcAccountService = metrcAccountService;
        this.productRepository = productRepository;
        this.productBatchRepository = productBatchRepository;
        this.inventoryService = inventoryService;
        this.shopRepository = shopRepository;
        this.appRepository = appRepository;
        this.exciseTaxInfoRepository = exciseTaxInfoRepository;
        this.exciseTaxInfoService = exciseTaxInfoService;
        this.productCategoryRepository = productCategoryRepository;
        this.shipmentBillService = shipmentBillService;
        this.amazonServiceManager = amazonServiceManager;
        this.connectConfiguration = connectConfiguration;
        this.companyRepository = companyRepository;
        this.employeeRepository = employeeRepository;
        this.userActivityService = userActivityService;
        this.amazonS3Service = amazonS3Service;
        this.cultivationTaxInfoService = cultivationTaxInfoService;
        this.companyAssetRepository = companyAssetRepository;
    }

    /**
     * This method process creation of purchase order (PO) request
     *
     * @param purchaseOrderAddRequest : PurchaseOrderAddRequest's instance that contains data for creation of purchase order
     * @return dbPurchaseOrder : saved purchase order
     */
    @Override
    public PurchaseOrder addPurchaseOrder(PurchaseOrderAddRequest purchaseOrderAddRequest) {

        PurchaseOrder purchaseOrder = preparePurchaseOrder(purchaseOrderAddRequest);

        if (purchaseOrder != null) {
            purchaseOrder.prepare(token.getCompanyId());
            purchaseOrder.setShopId(token.getShopId());
            long ordinal = getNextSequence();
            purchaseOrder.setPoNumber(ordinal + "-N");

            PurchaseOrder dbPurchaseOrder = purchaseOrderRepository.save(purchaseOrder);


            if (dbPurchaseOrder != null) {
                poActivityService.addPOActivity(dbPurchaseOrder.getId(), token.getActiveTopUser().getUserId(), "Created PO");
                UploadFileResult result = null;
                File file = null;
                try {
                    InputStream inputStream = QrCodeUtil.getQrCode(dbPurchaseOrder.getPoNumber());
                    file = QrCodeUtil.stream2file(inputStream, ".png");
                    String keyName = dbPurchaseOrder.getId() + "-" + dbPurchaseOrder.getPoNumber().replaceAll(" ", "");

                    keyName = keyName.replaceAll("[&#$%^]", "");

                    result = amazonS3Service.uploadFilePublic(file, keyName, ".png");
                    if (Objects.nonNull(result) && StringUtils.isNotBlank(result.getUrl())) {
                        CompanyAsset asset = new CompanyAsset();
                        asset.prepare(token.getCompanyId());
                        asset.setName("Invoice");
                        asset.setKey(result.getKey());
                        asset.setActive(true);
                        asset.setType(Asset.AssetType.Photo);
                        asset.setPublicURL(result.getUrl());
                        asset.setSecured(false);
                        companyAssetRepository.save(asset);
                        dbPurchaseOrder.setPoQrCodeAsset(asset);
                        dbPurchaseOrder.setPoQrCodeUrl(result.getUrl());
                        purchaseOrderRepository.update(dbPurchaseOrder.getId(), dbPurchaseOrder);
                    }
                } catch (IOException ex) {
                    LOGGER.error(QR_CODE_ERROR);
                }
            }
            return dbPurchaseOrder;
        }
        return purchaseOrder;

    }

    @Override
    public TaxResult calculateArmsLengthExciseTax(Shop shop, List<POProductRequest> poProductRequestList, Map<String, Product> productMap, List<ObjectId> productCategoryIds, boolean isPurchaseOrder) {

        ExciseTaxInfo exciseTaxInfo = exciseTaxInfoService.getExciseTaxInfoForShop(shop);
        ExciseTaxInfo nonCannabisTaxInfo = exciseTaxInfoRepository.getExciseTaxInfoByState(shop.getAddress().getState(), shop.getAddress().getCountry(), ExciseTaxInfo.CannabisTaxType.NonCannabis);

        double totalExciseTax = 0.0;
        if (shop.isEnableExciseTax() || shop.isEnableNonCannabisTax()) {
            HashMap<String, ProductCategory> categoryHashMap = productCategoryRepository.listAsMap(token.getCompanyId(), productCategoryIds);

            for (POProductRequest poProductRequest : poProductRequestList) {
                Product product = productMap.get(poProductRequest.getProductId());
                poProductRequest.setExciseTax(new BigDecimal(0));
                poProductRequest.setTotalExciseTax(new BigDecimal(0));
                if (product != null && categoryHashMap.containsKey(product.getCategoryId())) {
                    ProductCategory productCategory = categoryHashMap.get(product.getCategoryId());
                    boolean cannabis = product.isCannabisProduct(productCategory != null && productCategory.isCannabis());
                    double stateMarkUp = 0;
                    double stateExciseTax = 0;

                    if (cannabis && shop.isEnableExciseTax() && exciseTaxInfo != null) {
                        stateMarkUp = exciseTaxInfo.getStateMarkUp().doubleValue();
                        stateMarkUp = 1 + (stateMarkUp / 100);
                        stateExciseTax = exciseTaxInfo.getExciseTax().doubleValue();
                    } else if (!cannabis && shop.isEnableNonCannabisTax() && nonCannabisTaxInfo != null) {
                        stateMarkUp = nonCannabisTaxInfo.getStateMarkUp().doubleValue();
                        stateMarkUp = 1 + (stateMarkUp / 100);
                        stateExciseTax = nonCannabisTaxInfo.getExciseTax().doubleValue();
                    }

                    BigDecimal quantity;
                    if (isPurchaseOrder) {
                        quantity = poProductRequest.getRequestQuantity();
                    } else {
                        quantity = poProductRequest.getReceivedQuantity();
                    }
                    BigDecimal subTotal = poProductRequest.getUnitPrice().multiply(quantity);
                   // subTotal = subTotal.subtract(poProductRequest.getDiscount());
                    double productRequestTax = (subTotal.doubleValue() * stateMarkUp * stateExciseTax) / 100;

                    double unitProductExciseTax = (quantity.doubleValue() >= 0) ? (productRequestTax / quantity.doubleValue()) : 0D;
                    totalExciseTax = totalExciseTax + productRequestTax;

                    poProductRequest.setExciseTax(new BigDecimal(unitProductExciseTax));
                    poProductRequest.setTotalExciseTax(new BigDecimal(productRequestTax));

                }
            }
        }

        TaxResult taxResult = new TaxResult();
        taxResult.prepare();

        taxResult.setTotalExciseTax(new BigDecimal(totalExciseTax));

        return taxResult;
    }

    @Deprecated
    private TaxResult applyPurchaseOrderTax(Shop shop, PurchaseOrder purchaseOrder, Map<String, Product> productMap) {

        double totalPostTax = 0;
        double totalPreTax = 0;

        double totalCityTax = 0d;
        double totalCountyTax = 0d;
        double totalStateTax = 0d;
        double totalFedTax = 0d;

        int roundTaxDecimal = 2;
        App app = appRepository.getAppByName(App.BLAZE_APP_NAME);
        if (app != null && app.getDefaultTaxRoundOff() != 0) {
            roundTaxDecimal = app.getDefaultTaxRoundOff();
        }


        for (POProductRequest productRequest : purchaseOrder.getPoProductRequestList()) {

            Product product = productMap.get(productRequest.getProductId());
            if (product != null) {
                //COMPLEX TAX CALCULATION

                if (shop.isUseComplexTax()) {

                    ConsumerType consumerType = ConsumerType.AdultUse;

                    CompoundTaxTable taxTable = null;
                    if (shop.getTaxTables() != null) {
                        for (CompoundTaxTable compoundTaxTable : shop.getTaxTables()) {
                            if (compoundTaxTable.getConsumerType() == consumerType) {
                                taxTable = compoundTaxTable;
                                break;
                            }
                        }
                    }

                    //TODO : Do we need to reset CompoundTaxTable?

                    if (taxTable.getTaxType() == TaxInfo.TaxType.Exempt) {
                        continue;
                    }

                    if (taxTable != null) {
                        double lineTotalPostTax = 0;
                        double lineTotalPreTax = 0;
                        if (taxTable.getTaxOrder() == TaxInfo.TaxProcessingOrder.PostTaxed) {
                            //Calculate post tax (Complex tax)

                            double totalCostWithTax = productRequest.getTotalCost().doubleValue();

                            double curTax = 0d;
                            for (CompoundTaxRate curTaxRate : taxTable.getActivePostTaxes()) {
                                if (curTaxRate.isActive()) {
                                    double productTax = 0d;
                                    if (curTaxRate.isCompound()) {
                                        totalCostWithTax += NumberUtils.round(curTax, roundTaxDecimal);

                                        // reset curTax
                                        curTax = 0d;
                                        productTax = totalCostWithTax * (curTaxRate.getTaxRate().doubleValue() / 100);
                                        curTax += productTax;

                                    } else {
                                        productTax = totalCostWithTax * (curTaxRate.getTaxRate().doubleValue() / 100);
                                        curTax += productTax;
                                    }

                                    if (curTaxRate.getTerritory() == CompoundTaxRate.CompoundTaxTerritory.City) {
                                        totalCityTax += productTax;
                                    }

                                    if (curTaxRate.getTerritory() == CompoundTaxRate.CompoundTaxTerritory.County) {
                                        totalCountyTax += productTax;
                                    }

                                    if (curTaxRate.getTerritory() == CompoundTaxRate.CompoundTaxTerritory.State) {
                                        totalStateTax += productTax;
                                    }

                                    if (curTaxRate.getTerritory() == CompoundTaxRate.CompoundTaxTerritory.Federal) {
                                        totalFedTax += productTax;
                                    }
                                }
                            }
                            totalCostWithTax += NumberUtils.round(curTax, roundTaxDecimal);


                            lineTotalPostTax = NumberUtils.round(totalCostWithTax, roundTaxDecimal) - NumberUtils.round(productRequest.getTotalCost().doubleValue(), roundTaxDecimal);
                            lineTotalPostTax = NumberUtils.round(lineTotalPostTax, roundTaxDecimal);

                            totalPostTax += lineTotalPostTax;

                        } else {

                            // calculate preTax
                            List<CompoundTaxTable.ActiveTax> activeTaxes = taxTable.getActivePreTaxes();

                            double totalNonCompTaxes = 0;
                            for (CompoundTaxTable.ActiveTax activeTax : activeTaxes) {
                                if (!activeTax.compound) {
                                    totalNonCompTaxes += activeTax.taxRate.doubleValue();
                                }
                            }

                            for (CompoundTaxTable.ActiveTax activeTax : activeTaxes) {
                                double preCost;
                                double preTax;
                                if (activeTax.compound) {
                                    preCost = (productRequest.getTotalCost().doubleValue() - lineTotalPreTax) / (1 + activeTax.taxRate.doubleValue() / 100);
                                    if (preCost < 0) {
                                        preCost = 0;
                                    }
                                    preTax = (productRequest.getTotalCost().doubleValue() - lineTotalPreTax) - preCost;
                                } else {
                                    preCost = productRequest.getTotalCost().doubleValue() / (1 + totalNonCompTaxes / 100);
                                    if (preCost < 0) {
                                        preCost = 0;
                                    }

                                    preTax = preCost * activeTax.taxRate.doubleValue() / 100;
                                }

                                lineTotalPreTax += preTax;
                            }
                            totalPreTax += lineTotalPreTax;

                        }
                    }

                } else {
                    //SIMPLE TAX CALCULATION

                    //Tax info of shop
                    TaxInfo taxInfo = shop.getTaxInfo();
                    TaxInfo.TaxProcessingOrder taxOrder = shop.getTaxOrder();

                    // If it's custom, then assign the taxOrder and taxInfo
                    if (product.getTaxType() == TaxInfo.TaxType.Custom && product.getCustomTaxInfo() != null) {
                        taxInfo = product.getCustomTaxInfo();
                        taxOrder = product.getTaxOrder();
                    }

                    if (product.getTaxType() == TaxInfo.TaxType.Exempt) {
                        continue;
                    }

                    if (taxInfo != null) {
                        if (TaxInfo.TaxProcessingOrder.PostTaxed == taxOrder) {

                            double productTax = taxInfo.getTotalTax().doubleValue();

                            totalPostTax += productTax;

                        } else {

                            if (taxInfo.getTotalTax().doubleValue() > 0) {

                                double preCost = productRequest.getTotalCost().doubleValue() / (1 + taxInfo.getTotalTax().doubleValue());
                                if (preCost < 0) {
                                    preCost = 0;
                                }

                                double preTax = productRequest.getTotalCost().doubleValue() - preCost;

                                totalPreTax += preTax;
                            }
                        }
                    }

                }
            }
        }

        TaxResult taxResult = new TaxResult();
        taxResult.prepare();
        taxResult.setTotalPreCalcTax(new BigDecimal(totalPreTax));
        taxResult.setTotalPostCalcTax(new BigDecimal(totalPostTax));
        taxResult.setTotalCityTax(new BigDecimal(totalCityTax));
        taxResult.setTotalCountyTax(new BigDecimal(totalCountyTax));
        taxResult.setTotalStateTax(new BigDecimal(totalStateTax));
        taxResult.setTotalFedTax(new BigDecimal(totalFedTax));
        return taxResult;

    }

    private long getNextSequence() {
        return companyUniqueSequenceService.getNewIdentifier(token.getCompanyId(), PURCHASE_ORDER, purchaseOrderRepository.count(token.getCompanyId()));
    }

    /**
     * Prepare customized PO object by id
     *
     * @param purchaseOrderId : purchase order id
     */
    @Override
    public PurchaseOrderItemResult getPurchaseOrderItemById(String purchaseOrderId) {

        PurchaseOrderItemResult purchaseOrder = purchaseOrderRepository.getPOById(token.getCompanyId(), purchaseOrderId);

        if (purchaseOrder == null) {
            throw new BlazeInvalidArgException(PURCHASE_ORDER, PO_NOT_FOUND);
        }

        LinkedHashSet<ObjectId> employeeIds = new LinkedHashSet<>();
        LinkedHashSet<ObjectId> vendorIds = new LinkedHashSet<>();
        LinkedHashSet<ObjectId> productsIds = new LinkedHashSet<>();
        LinkedHashSet<ObjectId> customerCompanyIds = new LinkedHashSet<>();

        this.getInfoForPurchaseOrder(purchaseOrder, employeeIds, vendorIds, productsIds, customerCompanyIds);

        HashMap<String, Employee> employeeMap = employeeRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(employeeIds));
        HashMap<String, Vendor> vendorMap = vendorRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(vendorIds));
        HashMap<String, Product> productMap = productRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(productsIds));

        preparePurchaseOrderResult(purchaseOrder, employeeMap, vendorMap, productMap);

        return purchaseOrder;
    }

    private void preparePurchaseOrderResult(PurchaseOrderItemResult orderResult, HashMap<String, Employee> employeeMap, HashMap<String, Vendor> vendorMap, HashMap<String, Product> productMap) {

        Vendor vendor = vendorMap.get(orderResult.getVendorId());

        orderResult.setVendor(vendor);

        Employee employee = null;

        if (orderResult.getApprovedBy() != null) {
            employee = employeeMap.get(orderResult.getApprovedBy());
        }

        orderResult.setApprovedByMember(employee);

        LinkedHashSet<ObjectId> bundleItemsIds = new LinkedHashSet<>();
        for (Map.Entry<String,Product> entry : productMap.entrySet()){
            Product product = entry.getValue();
            if(product.getProductType().equals(Product.ProductType.BUNDLE)){
                for(BundleItem bundle : product.getBundleItems()) {
                    bundleItemsIds.add(new ObjectId(bundle.getProductId()));
                }
            }
        }
        HashMap<String, Product> bundleItemMap = productRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(bundleItemsIds));


        List<POProductRequestResult> poProductRequestResultList = new ArrayList<>();
        if (!orderResult.getPoProductRequestList().isEmpty()) {
            Product product = null;
            List<BundleItemResult> bundleItems = new ArrayList<>();
            POProductRequestResult requestResult;
            for (POProductRequest productRequest : orderResult.getPoProductRequestList()) {

                if (!productRequest.getProductId().isEmpty()) {
                    product = productMap.get(productRequest.getProductId());
                    if(product.getProductType().equals(Product.ProductType.BUNDLE)){
                        for(BundleItem bundleItem : product.getBundleItems()) {
                            Product bundleProduct = bundleItemMap.get(bundleItem.getProductId());

                            BundleItemResult bundleItemResult = new BundleItemResult();
                            bundleItemResult.setProductId(bundleItem.getProductId());
                            bundleItemResult.setProductName(bundleProduct.getName());
                            bundleItemResult.setQuantity(bundleItem.getQuantity());

                            bundleItems.add(bundleItemResult);
                        }
                    }
                }

                if (product != null) {
                    requestResult = new POProductRequestResult();
                    requestResult.setProduct(product);
                    requestResult.setBundleItems(bundleItems);
                    requestResult.setCompanyId(productRequest.getCompanyId());
                    requestResult.setId(productRequest.getId());
                    requestResult.setCreated(productRequest.getCreated());
                    requestResult.setDeleted(productRequest.isDeleted());
                    requestResult.setModified(productRequest.getModified());
                    requestResult.setDirty(productRequest.isDirty());
                    requestResult.setShopId(productRequest.getShopId());
                    requestResult.setUpdated(productRequest.isUpdated());

                    requestResult.setDeclineReason(productRequest.getDeclineReason());
                    requestResult.setNotes(productRequest.getNotes());
                    requestResult.setProductId(productRequest.getProductId());
                    requestResult.setProductName(productRequest.getProductName());
                    requestResult.setReceivedQuantity(productRequest.getReceivedQuantity());
                    requestResult.setRequestQuantity(productRequest.getRequestQuantity());
                    requestResult.setUnitPrice(productRequest.getUnitPrice());
                    requestResult.setTrackTraceSystem(productRequest.getTrackTraceSystem());
                    requestResult.setRequestStatus(productRequest.getRequestStatus());
                    requestResult.setTotalCost(productRequest.getTotalCost());
                    requestResult.setTrackingPackagesList(productRequest.getTrackingPackagesList());
                    requestResult.setExciseTax(productRequest.getExciseTax());
                    requestResult.setTotalExciseTax(productRequest.getTotalExciseTax());
                    requestResult.setTotalCultivationTax(productRequest.getTotalCultivationTax());
                    requestResult.setReceiveBatchStatus(productRequest.getReceiveBatchStatus());
                    requestResult.setBatchAddDetails(productRequest.getBatchAddDetails());

                    requestResult.setDiscount(productRequest.getDiscount());
                    requestResult.setFinalTotalCost(productRequest.getFinalTotalCost());
                    poProductRequestResultList.add(requestResult);
                }
            }
        }

        orderResult.setPoProductRequestList(null);
        orderResult.setPoProductRequestResultList(poProductRequestResultList);
        SearchResult<POActivity> poActivitySearchResult = poActivityService.getAllPOActivityList(0, Integer.MAX_VALUE, orderResult.getId());
        if (poActivitySearchResult != null && !poActivitySearchResult.getValues().isEmpty()) {
            orderResult.setPoActivityLog(poActivitySearchResult.getValues());
        }
        Company company = companyRepository.getById(orderResult.getCompanyId());
        String logoURL = company.getLogoURL() != null ? company.getLogoURL() : "https://connect-assets.s3.amazonaws.com/email/logo.jpg";
        orderResult.setCompanyLogo(logoURL);
    }

    /**
     * This method gets purchase order by id
     *
     * @param purchaseOrderId : id of purchase order that needs to be find
     * @return dbPurchaseOrder : purchase order's object find by id
     */
    @Override
    public PurchaseOrder getPurchaseOrderById(String purchaseOrderId) {
        PurchaseOrder purchaseOrder = purchaseOrderRepository.get(token.getCompanyId(), purchaseOrderId);
        if (purchaseOrder == null) {
            throw new BlazeInvalidArgException(PURCHASE_ORDER, PO_NOT_FOUND);
        }
        return purchaseOrder;
    }

    /**
     * This method gets list of all present purchase order
     *
     * @param start
     * @param limit
     * @return List<PurchaseOrder> : list of all purchase order
     */
    @Override
    public SearchResult<PurchaseOrderItemResult> getAllPurchaseOrder(int start, int limit, PurchaseOrder.CustomerType customerType, String searchTerm, String vendorId, PurchaseOrder.PurchaseOrderSort sortOption) {
        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());

        customerType = (customerType == null) ? PurchaseOrder.CustomerType.VENDOR : customerType;
        if (limit == 0) {
            limit = Integer.MAX_VALUE;
        }
        sortOption = (sortOption == null) ? PurchaseOrder.PurchaseOrderSort.DATE : sortOption;

        String sortOptionStr = "";

        if (PurchaseOrder.PurchaseOrderSort.CREATED_DATE == sortOption) {
            sortOptionStr = "{created: -1}";
        } else if (PurchaseOrder.PurchaseOrderSort.DELIVERY_DATE == sortOption) {
            sortOptionStr = "{deliveryDate: 1}";
        } else {
            //default sorting option is modified date
            sortOptionStr = "{modified: -1}";

        }


        SearchResult<PurchaseOrderItemResult> resultSearchResult;
        if (StringUtils.isNotBlank(searchTerm)) {
            SearchResult<VendorResult> vendorSearchResult = vendorRepository.findVendorsByTerm(token.getCompanyId(), token.getShopId(), searchTerm, VendorResult.class);
            List<VendorResult> vendorList = vendorSearchResult.getValues();
            List<String> vendorIds = new ArrayList<>();
            if (!vendorList.isEmpty()) {
                for (VendorResult vendorResult : vendorList) {
                    vendorIds.add(vendorResult.getId());
                }
            }
            resultSearchResult = purchaseOrderRepository.listAllByTerm(token.getCompanyId(), token.getShopId(), start, limit, sortOptionStr, searchTerm, customerType, vendorIds);
        } else if (StringUtils.isNotBlank(vendorId)) {
            resultSearchResult = purchaseOrderRepository.listAllByVendorId(token.getCompanyId(), token.getShopId(), start, limit, vendorId, sortOptionStr);
        } else {
            if (!shop.isRetail()) {
                resultSearchResult = purchaseOrderRepository.getAllPurchaseOrderWithSorting(token.getCompanyId(), token.getShopId(), sortOptionStr, start, limit, customerType);
            } else {
                resultSearchResult = purchaseOrderRepository.getAllPurchaseOrder(token.getCompanyId(), token.getShopId(), sortOptionStr, start, limit, customerType);
            }
        }

        if (resultSearchResult.getValues() != null) {

            HashMap<String, Object> preparedResult = this.getInfoForPurchaseOrderList(resultSearchResult.getValues());
            HashMap<String, Employee> employeeMap = (HashMap<String, Employee>) preparedResult.get("Employee");
            HashMap<String, Vendor> vendorMap = (HashMap<String, Vendor>) preparedResult.get("Vendor");
            HashMap<String, Product> productMap = (HashMap<String, Product>) preparedResult.get("Product");

            for (PurchaseOrderItemResult purchaseOrderItemResult : resultSearchResult.getValues()) {
                preparePurchaseOrderResult(purchaseOrderItemResult, employeeMap, vendorMap, productMap);
            }
        }

        return resultSearchResult;
    }

    /**
     * This method list PO by type of status
     *
     * @param start
     * @param limit
     * @param status : status type filter
     */
    @Override
    public SearchResult<PurchaseOrderItemResult> getPOListByStatus(int start, int limit, String status, PurchaseOrder.CustomerType customerType, String searchTerm, PurchaseOrder.PurchaseOrderSort sortOption) {

        customerType = (customerType == null) ? PurchaseOrder.CustomerType.VENDOR : customerType;

        sortOption = (sortOption == null) ? PurchaseOrder.PurchaseOrderSort.DATE : sortOption;

        String sortOptionStr = "";

        if (PurchaseOrder.PurchaseOrderSort.CREATED_DATE == sortOption) {
            sortOptionStr = "{created: -1}";
        } else if (PurchaseOrder.PurchaseOrderSort.DELIVERY_DATE == sortOption) {
            sortOptionStr = "{deliveryDate: 1}";
        } else {
            //default sorting option is modified date
            sortOptionStr = "{modified: -1}";

        }
        SearchResult<PurchaseOrderItemResult> searchResult;
        if (StringUtils.isBlank(searchTerm)) {
            searchResult = purchaseOrderRepository.listAllByStatus(token.getCompanyId(), token.getShopId(), start, limit, sortOptionStr , status, customerType);
        } else {
            SearchResult<VendorResult> vendorSearchResult = vendorRepository.findVendorsByTerm(token.getCompanyId(), token.getShopId(), searchTerm, VendorResult.class);
            List<VendorResult> vendorList = vendorSearchResult.getValues();
            List<String> vendorIds = new ArrayList<>();
            if (!vendorList.isEmpty()) {
                for (VendorResult vendorResult : vendorList) {
                    vendorIds.add(vendorResult.getId());
                }
            }
            searchResult = purchaseOrderRepository.listAllByTermWithStatus(token.getCompanyId(), token.getShopId(), start, limit, sortOptionStr, searchTerm, customerType, status, vendorIds);
        }

        if (searchResult.getValues() != null) {

            HashMap<String, Object> preparedResult = this.getInfoForPurchaseOrderList(searchResult.getValues());
            HashMap<String, Employee> employeeMap = (HashMap<String, Employee>) preparedResult.get("Employee");
            HashMap<String, Vendor> vendorMap = (HashMap<String, Vendor>) preparedResult.get("Vendor");
            HashMap<String, Product> productMap = (HashMap<String, Product>) preparedResult.get("Product");

            for (PurchaseOrderItemResult purchaseOrderItemResult : searchResult.getValues()) {
                preparePurchaseOrderResult(purchaseOrderItemResult, employeeMap, vendorMap, productMap);
            }
        }

        return searchResult;
    }

    private HashMap<String, Object> getInfoForPurchaseOrderList(List<PurchaseOrderItemResult> purchaseOrderItemResult) {

        HashMap<String, Object> result = new HashMap<>();
        LinkedHashSet<ObjectId> employeeIds = new LinkedHashSet<>();
        LinkedHashSet<ObjectId> vendorIds = new LinkedHashSet<>();
        LinkedHashSet<ObjectId> productsIds = new LinkedHashSet<>();
        LinkedHashSet<ObjectId> customerCompanyIds = new LinkedHashSet<>();

        for (PurchaseOrderItemResult purchaseOrder : purchaseOrderItemResult) {

            this.getInfoForPurchaseOrder(purchaseOrder, employeeIds, vendorIds, productsIds, customerCompanyIds);
        }

        HashMap<String, Employee> employeeMap = employeeRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(employeeIds));
        HashMap<String, Vendor> vendorMap = vendorRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(vendorIds));
        HashMap<String, Product> productMap = productRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(productsIds));

        result.put("Employee", employeeMap);
        result.put("Vendor", vendorMap);
        result.put("Product", productMap);

        return result;
    }

    private void getInfoForPurchaseOrder(PurchaseOrderItemResult purchaseOrder, LinkedHashSet<ObjectId> employeeIds, LinkedHashSet<ObjectId> vendorIds, LinkedHashSet<ObjectId> productsIds, LinkedHashSet<ObjectId> customerCompanyIds) {
        if (StringUtils.isNotBlank(purchaseOrder.getApprovedBy()) && ObjectId.isValid(purchaseOrder.getApprovedBy())) {
            employeeIds.add(new ObjectId(purchaseOrder.getApprovedBy()));
        }
        if (StringUtils.isNotBlank(purchaseOrder.getVendorId()) && ObjectId.isValid(purchaseOrder.getVendorId())) {
            vendorIds.add(new ObjectId(purchaseOrder.getVendorId()));
        }

        if (purchaseOrder.getPoProductRequestList() != null) {
            for (POProductRequest request : purchaseOrder.getPoProductRequestList()) {
                if (StringUtils.isNotBlank(request.getProductId()) && ObjectId.isValid(request.getProductId())) {
                    productsIds.add(new ObjectId(request.getProductId()));

                }
            }
        }
    }


    /**
     * Creates PoProductRequests from PoProductAddRequestList
     *
     * @param poProductAddRequestList : list of poProductAddRequest
     * @param poProductRequests       : list of poProductRequests
     */
    private Map<String, Object> createPOProductRequest(List<POProductAddRequest> poProductAddRequestList, List<POProductRequest> poProductRequests) {
        Map<String, Object> productReqResult = new HashMap<>();
        List<ObjectId> productCategoryList = new ArrayList<>();
        List<ObjectId> productIds = new ArrayList<>();

        productReqResult.put("totalAmount", 0);
        productReqResult.put("products", new ArrayList<>());
        productReqResult.put("productCategoryList", productCategoryList);

        BigDecimal totalAmount = BigDecimal.ZERO;
        BigDecimal totalDiscount = BigDecimal.ZERO;
        for (POProductAddRequest addRequest : poProductAddRequestList) {
            if (StringUtils.isNotBlank(addRequest.getProductId()) && ObjectId.isValid(addRequest.getProductId())) {
                productIds.add(new ObjectId(addRequest.getProductId()));
            }
        }
        HashMap<String, Product> productMap = productRepository.listAsMap(token.getCompanyId(), productIds);

        for (POProductAddRequest poProductAddRequest : poProductAddRequestList) {
            Product product = null;

            if (StringUtils.isNotBlank(poProductAddRequest.getProductId())) {
                product = productMap.get(poProductAddRequest.getProductId());
            }


            if (product != null) {
                if (product.getProductType().equals(Product.ProductType.BUNDLE) || product.getProductType().equals(Product.ProductType.DERIVED)) {
                    throw new BlazeInvalidArgException(PURCHASE_ORDER, "Cannot add derived or bundle products in purchase order.");
                }
                productCategoryList.add(new ObjectId(product.getCategoryId()));

                POProductRequest poProductRequest = new POProductRequest();
                poProductRequest.prepare(token.getCompanyId());
                poProductRequest.setShopId(token.getShopId());
                poProductRequest.setNotes(poProductAddRequest.getNotes());
                poProductRequest.setProductId(poProductAddRequest.getProductId());
                poProductRequest.setProductName(product.getName());
                poProductRequest.setRequestQuantity(poProductAddRequest.getRequestQuantity());
                poProductRequest.setDiscount(poProductAddRequest.getDiscount());


                BigDecimal unitPrice = BigDecimal.ZERO;
                BigDecimal totalCost = BigDecimal.ZERO;
                BigDecimal finalTotalCost = BigDecimal.ZERO;
                BigDecimal discount = poProductAddRequest.getDiscount() == null ? BigDecimal.ZERO : poProductAddRequest.getDiscount();

                if (poProductAddRequest.getTotalCost() != null) {
                    totalCost = poProductAddRequest.getTotalCost();
                }
                if (poProductAddRequest.getRequestQuantity() != null && !poProductAddRequest.getRequestQuantity().equals(BigDecimal.ZERO)) {
                    unitPrice = BigDecimal.valueOf(totalCost.doubleValue() / poProductAddRequest.getRequestQuantity().doubleValue());
                }

                if (poProductAddRequest.getDiscount() != null && poProductAddRequest.getDiscount().doubleValue() > totalCost.doubleValue()) {
                    throw new BlazeInvalidArgException(PURCHASE_ORDER, "Discount cannot be greater than total Cost.");
                }

                finalTotalCost = totalCost.subtract(discount);

                poProductRequest.setUnitPrice(unitPrice);
                poProductRequest.setTotalCost(totalCost);

                poProductRequest.setFinalTotalCost(finalTotalCost);
                totalAmount = totalAmount.add(finalTotalCost);

                totalDiscount = totalDiscount.add(discount);

                poProductRequests.add(poProductRequest);
            } else {
                throw new BlazeInvalidArgException("Product", "Product does not exist for this shop.");
            }
        }

        productReqResult.put("totalAmount", totalAmount);
        productReqResult.put("products", productMap);
        productReqResult.put("productCategoryList", productCategoryList);
        productReqResult.put("totalDiscount", totalDiscount);
        return productReqResult;
    }

    /**
     * Update POProduct status(accept/decline) and quantity
     *
     * @param poID                   : purchase order id
     * @param poProductRequestUpdate : it contains required update information (POId, PRId, status, received quantity)
     * @return instance of PurchaseOrderItemResult
     */
    @Override
    public PurchaseOrderItemResult updatePOProductRequestStatus(String poID, POProductRequestUpdate poProductRequestUpdate) {

        PurchaseOrderItemResult dbPurchaseOrder = purchaseOrderRepository.get(token.getCompanyId(), poID, PurchaseOrderItemResult.class);

        if (dbPurchaseOrder != null) {

            POProductRequest poProductRequest = getPOPurchaseOrder(dbPurchaseOrder.getPoProductRequestList(), poProductRequestUpdate.getProductRequestId());

            Product product = productRepository.getById(poProductRequest.getProductId());

            if (product == null) {
                throw new BlazeInvalidArgException(PURCHASE_ORDER, "Product not found.");
            }

            if (poProductRequest != null) {

                poProductRequest.setRequestStatus(poProductRequestUpdate.isStatus() ? POProductRequest.RequestStatus.ACCEPTED : POProductRequest.RequestStatus.DECLINED);
                poProductRequest.setReceivedQuantity(poProductRequestUpdate.getReceivedQuantity());
                poProductRequest.setDeclineReason(poProductRequestUpdate.getDeclineReason());
                poProductRequest.setReceiveBatchStatus(poProductRequestUpdate.getReceiveBatchStatus());
                poProductRequest.setBatchAddDetails(poProductRequestUpdate.getBatchAddDetails());

                validateBatchSku(product, poProductRequest);
                purchaseOrderRepository.update(token.getCompanyId(), dbPurchaseOrder.getId(), dbPurchaseOrder);
            } else {
                throw new BlazeInvalidArgException("Product Request", "Error! Product Request not found");
            }
        } else {
            throw new BlazeInvalidArgException(PURCHASE_ORDER, PO_NOT_FOUND);
        }

        LinkedHashSet<ObjectId> employeeIds = new LinkedHashSet<>();
        LinkedHashSet<ObjectId> vendorIds = new LinkedHashSet<>();
        LinkedHashSet<ObjectId> productsIds = new LinkedHashSet<>();
        LinkedHashSet<ObjectId> customerCompanyIds = new LinkedHashSet<>();

        this.getInfoForPurchaseOrder(dbPurchaseOrder, employeeIds, vendorIds, productsIds, customerCompanyIds);

        HashMap<String, Employee> employeeMap = employeeRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(employeeIds));
        HashMap<String, Vendor> vendorMap = vendorRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(vendorIds));
        HashMap<String, Product> productMap = productRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(productsIds));

        preparePurchaseOrderResult(dbPurchaseOrder, employeeMap, vendorMap, productMap);
        return dbPurchaseOrder;
    }

    /**
     * Get POProductRequest from list of POProductRequest in PO
     *
     * @param poProductRequestList : list of POProductRequest in purchase order
     * @param productRequestId     : id of product request
     */
    private POProductRequest getPOPurchaseOrder(List<POProductRequest> poProductRequestList, String productRequestId) {
        POProductRequest poProductRequest = null;
        for (POProductRequest productRequest : poProductRequestList) {
            if (productRequest != null && productRequestId.equals(productRequest.getId())) {
                poProductRequest = productRequest;
                break;
            }
        }
        return poProductRequest;
    }

    /**
     * This method update status of purchase order
     *
     * @param poID
     * @param request :
     * @return updated purchase order
     */
    @Override
    public PurchaseOrder updatePurchaseOrderStatus(String poID, POStatusUpdateRequest request) {
        PurchaseOrder dbPurchaseOrder = null;

        PurchaseOrder purchaseOrder = purchaseOrderRepository.get(token.getCompanyId(), poID);
        if (purchaseOrder != null) {

            boolean statusChange = false;
            if (request.getStatus().equals(PurchaseOrder.PurchaseOrderStatus.RequiredApproval)) {
                purchaseOrder.setPurchaseOrderStatus(request.getStatus());
                purchaseOrder.setSubmitForApprovalDate(DateTime.now().getMillis());
                statusChange = true;
            } else if (request.getStatus().equals(PurchaseOrder.PurchaseOrderStatus.Cancel)) {
                purchaseOrder.setPurchaseOrderStatus(request.getStatus());
                statusChange = true;
            } else if (request.getStatus().equals(PurchaseOrder.PurchaseOrderStatus.Decline)) {
                purchaseOrder.setPurchaseOrderStatus(request.getStatus());
                purchaseOrder.setDeclineReason(request.getDeclineMessage());
                statusChange = true;
            } else if (request.getStatus().equals(PurchaseOrder.PurchaseOrderStatus.InProgress) && purchaseOrder.getPurchaseOrderStatus().equals(PurchaseOrder.PurchaseOrderStatus.Approved)) {
                purchaseOrder.setPurchaseOrderStatus(request.getStatus());
                statusChange = true;
            } else if (request.getStatus().equals(PurchaseOrder.PurchaseOrderStatus.InProgress) && purchaseOrder.getPurchaseOrderStatus().equals(PurchaseOrder.PurchaseOrderStatus.RequiredApproval)) {
                purchaseOrder.setPurchaseOrderStatus(request.getStatus());
                statusChange = true;
            }

            if (statusChange) {
                dbPurchaseOrder = purchaseOrderRepository.update(token.getCompanyId(), purchaseOrder.getId(), purchaseOrder);

                if (dbPurchaseOrder != null) {
                    if (PurchaseOrder.PurchaseOrderStatus.Decline == request.getStatus()) {
                        addActivityForPODecline(dbPurchaseOrder.getDeclineReason(), dbPurchaseOrder.getId());
                    } else {
                        String status = request.getStatus().name();
                        if (status.equals("InProgress")) {
                            status = "In Progress";
                        } else if (status.equals("ReceivingShipment")) {
                            status = "Receiving Shipment";
                        } else if (status.equals("RequiredApproval")) {
                            status = "Required Approval";
                        } else if (status.equals("WaitingShipment")) {
                            status = "Waiting Shipment";
                        } else if (status.equals("ReceivedShipment")) {
                            status = "Received Shipment";
                        }
                        poActivityService.addPOActivity(dbPurchaseOrder.getId(), token.getActiveTopUser().getUserId(), "PO Status update : " + status);
                    }

                }
            }
        } else {
            throw new BlazeInvalidArgException(PURCHASE_ORDER, PO_NOT_FOUND);
        }

        return dbPurchaseOrder;
    }

    /**
     * This method updates approval information of purchase order by manager
     *
     * @param poID    : purchase order id
     * @param request : information required for manager approval
     */
    @Override
    public PurchaseOrder updatePOManagerApproval(String poID, POManagerApprovalRequest request) {
        PurchaseOrder dbPurchaseOrder = null;


        Employee employee = employeeRepository.get(token.getCompanyId(), token.getActiveTopUser().getUserId());

        if (employee == null) {
            throw new BlazeAuthException("Employee", "Employee not found.");
        }

        if (!request.getPin().equalsIgnoreCase(employee.getPin())) {
            throw new BlazeAuthException("Purchase Order", "Invalid pin.");
        }

        String roleId = employee.getRoleId();
        Role role = roleService.getRoleById(token.getCompanyId(), roleId);


        if (role.getName().equalsIgnoreCase("Manager")
                || role.getName().equalsIgnoreCase("Admin")
                || role.getName().equalsIgnoreCase("Shop Manager")) {
            PurchaseOrder purchaseOrder = this.getPurchaseOrderById(poID);

            if (purchaseOrder != null) {
                CompanyAsset approvedSignature = request.getCompanyAsset();

                if (approvedSignature != null) {
                    approvedSignature.prepare(token.getCompanyId());
                }

                purchaseOrder.setApprovedSignature(approvedSignature);
                purchaseOrder.setApprovedBy(token.getActiveTopUser().getUserId());
                if (request.getStatus().trim().equals(PurchaseOrder.PurchaseOrderStatus.Approved.toString())) {
                    purchaseOrder.setPurchaseOrderStatus(PurchaseOrder.PurchaseOrderStatus.Approved);
                    purchaseOrder.setApprovedDate(DateTime.now().getMillis());
                } else if (request.getStatus().trim().equals(PurchaseOrder.PurchaseOrderStatus.Decline.toString())) {
                    purchaseOrder.setDeclineDate(DateTime.now().getMillis());
                    purchaseOrder.setPurchaseOrderStatus(PurchaseOrder.PurchaseOrderStatus.Decline);
                    purchaseOrder.setDeclineReason(request.getDeclineReason());
                } else if (request.getStatus().trim().equals(PurchaseOrder.PurchaseOrderStatus.Cancel.toString())) {
                    purchaseOrder.setPurchaseOrderStatus(PurchaseOrder.PurchaseOrderStatus.Cancel);
                }

                dbPurchaseOrder = purchaseOrderRepository.update(token.getCompanyId(), purchaseOrder.getId(), purchaseOrder);

                if (dbPurchaseOrder != null) {
                    if (PurchaseOrder.PurchaseOrderStatus.Decline.toString().equals(request.getStatus().trim())) {
                        addActivityForPODecline(dbPurchaseOrder.getDeclineReason(), dbPurchaseOrder.getId());
                    } else {
                        String status = dbPurchaseOrder.getPurchaseOrderStatus().name();
                        if (status.equals("InProgress")) {
                            status = "In Progress";
                        } else if (status.equals("ReceivingShipment")) {
                            status = "Receiving Shipment";
                        } else if (status.equals("RequiredApproval")) {
                            status = "Required Approval";
                        } else if (status.equals("WaitingShipment")) {
                            status = "Waiting Shipment";
                        } else if (status.equals("ReceivedShipment")) {
                            status = "Received Shipment";
                        }
                        String log = "PO " + status.toLowerCase();
                        poActivityService.addPOActivity(dbPurchaseOrder.getId(), token.getActiveTopUser().getUserId(), log);
                    }

                }

            } else {
                throw new BlazeInvalidArgException(PURCHASE_ORDER, PO_NOT_FOUND);
            }
        } else {
            throw new BlazeOperationException("Sorry! You are not authorized to perform this operation");
        }

        return dbPurchaseOrder;
    }

    /**
     * It updates purchase order's information
     *
     * @param poID    : purchase order id
     * @param request : contains information required for update of purchase order
     * @return updates PurchaseOrder's instance
     */
    @Override
    public PurchaseOrder updatePurchaseOrder(String poID, PurchaseOrder request) {

        PurchaseOrder updatedPurchaseOrder = null;
        HashMap<String, Product> productMap = new HashMap<>();
        List<ObjectId> productCategoryIds = new ArrayList<>();
        PurchaseOrder dbPurchaseOrder = this.getPurchaseOrderById(poID);

        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());
        if (shop == null) {
            throw new BlazeInvalidArgException(SHOP, SHOP_NOT_FOUND);
        }

        if (request.getPoProductRequestList() == null || request.getPoProductRequestList().size() == 0) {
            throw new BlazeInvalidArgException(PURCHASE_ORDER, NOT_VALID_PRODUCT);
        }

        if (PurchaseOrder.POPaymentTerms.CUSTOM_DATE.equals(request.getPoPaymentTerms()) &&
                request.getCustomTermDate() == 0) {
            throw new BlazeInvalidArgException(CUSTOM_DATE, NOT_CUSTOM_DATE);
        }

        if (dbPurchaseOrder != null && request.getPoProductRequestList() != null && dbPurchaseOrder.isEditEnable()) {
            if (CollectionUtils.isNotEmpty(request.getPoProductRequestList())) {
                List<ObjectId> productIds = new ArrayList<>();
                for (POProductRequest poProductRequest : request.getPoProductRequestList()) {
                    if (StringUtils.isNotBlank(poProductRequest.getProductId()) && ObjectId.isValid(poProductRequest.getProductId())) {
                        productIds.add(new ObjectId(poProductRequest.getProductId()));
                    }
                }
                productMap = productRepository.listAsMap(token.getCompanyId(), productIds);
                for (POProductRequest poProductRequest : request.getPoProductRequestList()) {
                    Product product = productMap.get(poProductRequest.getProductId());
                    if (product == null) {
                        throw new BlazeInvalidArgException("Product", "Product does not exist");
                    }
                    if (product.getProductType().equals(Product.ProductType.BUNDLE) || product.getProductType().equals(Product.ProductType.DERIVED)) {
                        throw new BlazeInvalidArgException(PURCHASE_ORDER, "Cannot add derived or bundle products in purchase order.");
                    }
                    productCategoryIds.add(new ObjectId(product.getCategoryId()));
                }
            }
            dbPurchaseOrder.prepare(token.getCompanyId());
            dbPurchaseOrder.setShopId(token.getShopId());
            dbPurchaseOrder.setCreatedByEmployeeId(token.getActiveTopUser().getUserId());
            dbPurchaseOrder.setVendorId(request.getVendorId());
            dbPurchaseOrder.setNotes(request.getNotes());
            dbPurchaseOrder.setPoPaymentTerms(request.getPoPaymentTerms());
            dbPurchaseOrder.setPoPaymentOptions(request.getPoPaymentOptions());
            dbPurchaseOrder.setCustomTermDate(request.getCustomTermDate());
            dbPurchaseOrder.setCustomerType(request.getCustomerType());

            if (StringUtils.isBlank(request.getVendorId())) {
                throw new BlazeInvalidArgException(PURCHASE_ORDER, "Vendor can't be empty");
            }
            Vendor vendor = vendorRepository.getById(request.getVendorId());
            if (vendor == null) {
                throw new BlazeInvalidArgException(PURCHASE_ORDER, VENDOR_NOT_FOUND);
            }
            if (vendor.getVendorType().equals(Vendor.VendorType.CUSTOMER)) {
                throw new BlazeInvalidArgException(PURCHASE_ORDER, PO_NOT_ALLOWED_FOR_CUSTOMER);
            }
            CompanyLicense companyLicense = vendor.getCompanyLicense(request.getLicenseId());
            if (companyLicense == null) {
                    throw new BlazeInvalidArgException(PURCHASE_ORDER, "Company License not found");
            }

            dbPurchaseOrder.setVendorId(request.getVendorId());
            dbPurchaseOrder.setLicenseId(companyLicense.getId());

            if (request.getTransactionType() == null && vendor.getArmsLengthType() != null) {
                dbPurchaseOrder.setTransactionType(vendor.getArmsLengthType());
            } else {
                dbPurchaseOrder.setTransactionType(request.getTransactionType());
            }

            dbPurchaseOrder.setTransactionType(request.getTransactionType());

            HashMap<String, BigDecimal> infoMap = processIncomingProductRequest(request.getPoProductRequestList());
            BigDecimal totalAmount = infoMap.get("totalAmount");
            BigDecimal totalItemDiscount = infoMap.get("totalDiscount");

            dbPurchaseOrder.setPoProductRequestList(request.getPoProductRequestList());
            dbPurchaseOrder.setTotalCost(totalAmount);
            dbPurchaseOrder.setReference(request.getReference());
            if (request.getDiscount().doubleValue() > dbPurchaseOrder.getTotalCost().doubleValue()) {
                throw new BlazeInvalidArgException(PURCHASE_ORDER, "Discount cannot be greater than total amount.");
            }

            dbPurchaseOrder.setTotalDiscount(totalItemDiscount.add(request.getDiscount()));

            //Reset calc taxes
            dbPurchaseOrder.setGrandTotal(totalAmount);
            dbPurchaseOrder.setTotalCalcTax(new BigDecimal(0));
            dbPurchaseOrder.setTotalPreCalcTax(new BigDecimal(0));

            TaxResult taxResult = null;
            if (Vendor.ArmsLengthType.ARMS_LENGTH.equals(dbPurchaseOrder.getTransactionType()) && CompanyFeatures.AppTarget.Distribution != shop.getAppTarget()) {
                taxResult = calculateArmsLengthExciseTax(shop, dbPurchaseOrder.getPoProductRequestList(), productMap, productCategoryIds, true);
            }
            //OZ tax calculation
            CultivationTaxResult totalCultivationTax = null;
            if (Vendor.CompanyType.CULTIVATOR.equals(companyLicense.getCompanyType()) || Vendor.CompanyType.MANUFACTURER.equals(companyLicense.getCompanyType())) {
                if (PurchaseOrder.FlowerSourceType.CULTIVATOR_DIRECT == request.getFlowerSourceType()) {
                    totalCultivationTax = this.calculateOzTax(shop, dbPurchaseOrder, productMap, productCategoryIds, true);
                }
            }

            if (taxResult == null) {
                taxResult = new TaxResult();
            }
            BigDecimal cultivationTax = new BigDecimal(0);
            if (totalCultivationTax != null) {
                cultivationTax = totalCultivationTax.getTotalCultivationTax();
            }

            taxResult.setCultivationTaxResult(totalCultivationTax == null ? new CultivationTaxResult() : totalCultivationTax);
            dbPurchaseOrder.setDiscount(request.getDiscount());
            dbPurchaseOrder.setFees(request.getFees());
            dbPurchaseOrder.setTaxResult(taxResult);

            BigDecimal grandTotal = new BigDecimal(0);
            BigDecimal totalTax = new BigDecimal(0);
            if (cultivationTax.doubleValue() > 0) {
                grandTotal = grandTotal.add(cultivationTax);
            }
            grandTotal = grandTotal.add(dbPurchaseOrder.getTotalCost());
            if (dbPurchaseOrder.getTaxResult() != null) {
                totalTax = dbPurchaseOrder.getTaxResult().getTotalExciseTax();
                if (dbPurchaseOrder.getTaxResult().getCultivationTaxResult() != null) {
                    totalTax = dbPurchaseOrder.getTaxResult().getTotalExciseTax().add(dbPurchaseOrder.getTaxResult().getCultivationTaxResult().getTotalCultivationTax());
                }

                grandTotal = grandTotal.add(dbPurchaseOrder.getTaxResult().getTotalExciseTax());
                if (PurchaseOrder.CustomerType.CUSTOMER_COMPANY.equals(dbPurchaseOrder.getCustomerType())) {
                    applyDeliveryCharge(shop, dbPurchaseOrder);
                }
            }

            grandTotal = grandTotal.add(dbPurchaseOrder.getFees());
            grandTotal = grandTotal.subtract(dbPurchaseOrder.getDiscount());
            dbPurchaseOrder.setGrandTotal(grandTotal.add(dbPurchaseOrder.getDeliveryCharge()));

            dbPurchaseOrder.setTotalTax(totalTax);
            dbPurchaseOrder.setDeliveryAddress(request.getDeliveryAddress());
            dbPurchaseOrder.setDeliveryTime(request.getDeliveryTime());
            dbPurchaseOrder.setTermsAndCondition(request.getTermsAndCondition());
            dbPurchaseOrder.setDeliveryDate(request.getDeliveryDate());
            dbPurchaseOrder.setReceivedDate(request.getReceivedDate());

            dbPurchaseOrder.setManagerReceiveSignature(request.getManagerReceiveSignature());

            dbPurchaseOrder.setPurchaseOrderDate(request.getPurchaseOrderDate());
            dbPurchaseOrder.setDueDate(request.getPODueDate());
            dbPurchaseOrder.setAdjustmentInfoList(request.getAdjustmentInfoList());
            //process adjustments
            dbPurchaseOrder.setGrandTotal(processAdjustments(dbPurchaseOrder.getAdjustmentInfoList(), dbPurchaseOrder.getGrandTotal()));


            updatedPurchaseOrder = purchaseOrderRepository.update(token.getCompanyId(), dbPurchaseOrder.getId(), dbPurchaseOrder);

            if (updatedPurchaseOrder != null) {
                poActivityService.addPOActivity(updatedPurchaseOrder.getId(), token.getActiveTopUser().getUserId(), "Updated PO");
            }
        } else {
            throw new BlazeInvalidArgException(PURCHASE_ORDER, PO_NOT_FOUND + "or Purchase Order is not allowed to edit");
        }

        return updatedPurchaseOrder;
    }

    private HashMap<String, BigDecimal> processIncomingProductRequest(List<POProductRequest> poProductRequests) {
        BigDecimal totalAmount = BigDecimal.ZERO;
        BigDecimal totalDiscount = BigDecimal.ZERO;

        for (POProductRequest poProductRequest : poProductRequests) {
            poProductRequest.prepare(token.getCompanyId()); // always prepare
            poProductRequest.setShopId(token.getShopId());

            if (StringUtils.isBlank(poProductRequest.getProductId())) {
                throw new BlazeInvalidArgException("ProductRequest", "Please select a valid product.");
            }

            Product product = productRepository.get(token.getCompanyId(), poProductRequest.getProductId());
            if (product == null) {
                throw new BlazeInvalidArgException("Product", "Product does not exist.");
            }
            if (!product.getShopId().equalsIgnoreCase(token.getShopId())) {
                throw new BlazeInvalidArgException("Product", "Please choose a product from the same shop.");
            }
            poProductRequest.setProductName(product.getName());
            poProductRequest.setDiscount(poProductRequest.getDiscount());

            BigDecimal unitPrice = BigDecimal.ZERO;
            BigDecimal totalCost = poProductRequest.getTotalCost();
            BigDecimal quantity = poProductRequest.getRequestQuantity();
            BigDecimal finalTotalCost = BigDecimal.ZERO;

            if (quantity.doubleValue() <= 0) {
                throw new BlazeInvalidArgException("Quantity", "Quantity should be greater than 0.");
            }
            if (totalCost != null && poProductRequest.getRequestQuantity() != null && !poProductRequest.getRequestQuantity().equals(BigDecimal.ZERO)) {
                unitPrice = BigDecimal.valueOf(totalCost.doubleValue() / quantity.doubleValue());
            }
            if (poProductRequest.getDiscount() != null) {
                finalTotalCost = totalCost.subtract(poProductRequest.getDiscount());
            }

            poProductRequest.setUnitPrice(unitPrice);
            poProductRequest.setTotalCost(totalCost);

            poProductRequest.setFinalTotalCost(finalTotalCost);
            totalAmount = totalAmount.add(finalTotalCost);
            totalDiscount = totalDiscount.add(poProductRequest.getDiscount());
        }
        HashMap<String, BigDecimal> returnMap = new HashMap<>();
        returnMap.put("totalAmount", totalAmount);
        returnMap.put("totalDiscount", totalDiscount);

        return returnMap;
    }

    /**
     * This method add attachment in PO
     *
     * @param poID    : purchase order id
     * @param request : information required for attachments add in PO
     * @return updated purchase order
     */
    @Override
    public PurchaseOrder addPOAttachment(String poID, POAttachmentRequest request) {
        PurchaseOrder dbPurchaseOrder = null;

        PurchaseOrder purchaseOrder = this.getPurchaseOrderById(poID);
        if (purchaseOrder != null) {
            CompanyAsset asset = request.getAsset();
            asset.prepare(token.getCompanyId());
            List<CompanyAsset> companyAssetList = purchaseOrder.getCompanyAssetList();


            if (companyAssetList == null) {
                companyAssetList = new ArrayList<>();
            }
            companyAssetList.add(asset);
            purchaseOrder.setCompanyAssetList(companyAssetList);

            dbPurchaseOrder = purchaseOrderRepository.update(token.getCompanyId(), purchaseOrder.getId(), purchaseOrder);

            if (dbPurchaseOrder != null) {
                poActivityService.addPOActivity(dbPurchaseOrder.getId(), token.getActiveTopUser().getUserId(), asset.getName() + " attachment added");
            }
        } else {
            throw new BlazeInvalidArgException(PURCHASE_ORDER, PO_NOT_FOUND);
        }

        return dbPurchaseOrder;
    }

    /**
     * This method gets attachment by PO id and attachment id
     *
     * @param purchaseOrderId : PO id
     * @param attachmentId    : attachment id that needs to be find
     */
    @Override
    public CompanyAsset getPOAttachment(String purchaseOrderId, String attachmentId) {

        CompanyAsset asset = null;
        PurchaseOrder purchaseOrder = this.getPurchaseOrderById(purchaseOrderId);

        if (purchaseOrder != null) {
            asset = getPOAttachmentById(purchaseOrder.getCompanyAssetList(), attachmentId);
        }
        if (asset == null) {
            throw new BlazeInvalidArgException(PURCHASE_ORDER, "Error! Attachment not found");
        }
        return asset;
    }

    /**
     * Update PO attachment
     *
     * @param purchaseOrderId : purchase order id
     * @param attachmentId    : attachment id
     * @param request         : information for attachment update
     */
    @Override
    public PurchaseOrder updatePOAttachment(String purchaseOrderId, String attachmentId, POAttachmentRequest request) {
        PurchaseOrder dbPurchaseOrder = null;

        PurchaseOrder purchaseOrder = this.getPurchaseOrderById(purchaseOrderId);
        if (purchaseOrder != null) {
            CompanyAsset asset = request.getAsset();
            List<CompanyAsset> companyAssetList = purchaseOrder.getCompanyAssetList();

            CompanyAsset companyAsset = getPOAttachmentById(companyAssetList, attachmentId);

            if (companyAsset != null) {
                companyAsset.setName(asset.getName());
                companyAsset.setKey(asset.getKey());
                companyAsset.setType(asset.getType());
                companyAsset.setPublicURL(asset.getPublicURL());
                companyAsset.setActive(asset.isActive());
                companyAsset.setPriority(asset.getPriority());
                companyAsset.setSecured(asset.isSecured());


                dbPurchaseOrder = purchaseOrderRepository.update(token.getCompanyId(), purchaseOrder.getId(), purchaseOrder);

                if (dbPurchaseOrder != null) {
                    poActivityService.addPOActivity(dbPurchaseOrder.getId(), token.getActiveTopUser().getUserId(), asset.getName() + " attachment updated");
                }
            } else {
                throw new BlazeInvalidArgException(PURCHASE_ORDER, ATTACHMENT_NOT_FOUND);
            }
        } else {
            throw new BlazeInvalidArgException(PURCHASE_ORDER, PO_NOT_FOUND);
        }

        return dbPurchaseOrder;
    }

    /**
     * This method delete attachment by attachment id
     *
     * @param purchaseOrderId : PO id
     * @param attachmentId    : attachment id
     */
    @Override
    public void deletePOAttachment(String purchaseOrderId, String attachmentId) {
        PurchaseOrder purchaseOrder = this.getPurchaseOrderById(purchaseOrderId);

        if (purchaseOrder != null) {

            List<CompanyAsset> companyAssetList = purchaseOrder.getCompanyAssetList();
            CompanyAsset asset = getPOAttachmentById(companyAssetList, attachmentId);
            if (asset != null) {
                companyAssetList.remove(asset);
                PurchaseOrder dbPurchaseOrder = purchaseOrderRepository.update(token.getCompanyId(), purchaseOrder.getId(), purchaseOrder);

                if (dbPurchaseOrder != null) {
                    poActivityService.addPOActivity(dbPurchaseOrder.getId(), token.getActiveTopUser().getUserId(), asset.getName() + " attachment deleted");
                }
            } else {
                throw new BlazeInvalidArgException(PURCHASE_ORDER, ATTACHMENT_NOT_FOUND);
            }
        } else {
            throw new BlazeInvalidArgException(PURCHASE_ORDER, PO_NOT_FOUND);
        }
    }

    /**
     * This method gets attachment from provided PO attachment list by id
     *
     * @param companyAssetList : PO attachment list
     * @param attachmentId     : id of attachment that needs to be find
     */
    private CompanyAsset getPOAttachmentById(List<CompanyAsset> companyAssetList, String attachmentId) {
        CompanyAsset asset = null;
        if (companyAssetList != null) {
            for (CompanyAsset dbAsset : companyAssetList) {
                if (dbAsset.getId().equals(attachmentId)) {
                    asset = dbAsset;
                    break;
                }
            }
        }

        return asset;
    }

    /**
     * This method updates status of PO as ReceivingShipment
     *
     * @param purchaseOrderId : Purchase Order Id
     * @return PurchaseOrder : purchase order instance
     */
    @Override
    public PurchaseOrder receivePurchaseOrder(String purchaseOrderId) {
        PurchaseOrder dbPurchaseOrder = null;

        PurchaseOrder purchaseOrder = purchaseOrderRepository.get(token.getCompanyId(), purchaseOrderId);

        if (purchaseOrder != null) {
            purchaseOrder.setPurchaseOrderStatus(PurchaseOrder.PurchaseOrderStatus.ReceivingShipment);
            purchaseOrder.setReceivedDate(DateTime.now().getMillis());
            purchaseOrder.setReceivedByEmployeeId(token.getActiveTopUser().getUserId());

            dbPurchaseOrder = purchaseOrderRepository.update(token.getCompanyId(), purchaseOrder.getId(), purchaseOrder);

            if (dbPurchaseOrder != null) {
                poActivityService.addPOActivity(dbPurchaseOrder.getId(), token.getActiveTopUser().getUserId(), "PO Status update : Receiving Shipment");
            }
        } else {
            throw new BlazeInvalidArgException(PURCHASE_ORDER, PO_NOT_FOUND);
        }

        return dbPurchaseOrder;
    }

    /**
     * This method marks status of PO as WaitingForShipment
     *
     * @param purchaseOrderId : purchase order id
     * @return instance of purchase order
     */
    @Override
    public PurchaseOrder markPurchaseOrder(String purchaseOrderId) {
        PurchaseOrder dbPurchaseOrder = null;

        PurchaseOrder purchaseOrder = purchaseOrderRepository.get(token.getCompanyId(), purchaseOrderId);

        if (purchaseOrder != null) {
            purchaseOrder.setPurchaseOrderStatus(PurchaseOrder.PurchaseOrderStatus.WaitingShipment);

            dbPurchaseOrder = purchaseOrderRepository.update(token.getCompanyId(), purchaseOrder.getId(), purchaseOrder);

            if (dbPurchaseOrder != null) {
                poActivityService.addPOActivity(dbPurchaseOrder.getId(), token.getActiveTopUser().getUserId(), "PO Status update : Waiting Shipment");
            }
        } else {
            throw new BlazeInvalidArgException(PURCHASE_ORDER, PO_NOT_FOUND);
        }

        return dbPurchaseOrder;
    }

    /**
     * Complete shipment bill arrival
     *
     * @param poId    : purchase order id
     * @param request
     */
    @Override
    public ShipmentBillResult completeShipmentArrival(String poId, POCompleteRequest request) {
        ShipmentBillResult shipmentBillResult = new ShipmentBillResult();
        shipmentBillResult.setStatus(false);
        final PurchaseOrder purchaseOrder = purchaseOrderRepository.get(token.getCompanyId(), poId);
        if (purchaseOrder != null) {

            if (purchaseOrder.getPurchaseOrderStatus().equals(PurchaseOrder.PurchaseOrderStatus.Closed)) {
                throw new BlazeInvalidArgException(PURCHASE_ORDER, "This purchase order is already closed.");
            }

            Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());
            // double check for errors
            if (shop != null && shop.getAppTarget() == CompanyFeatures.AppTarget.Retail) {
                if (StringUtils.isNotBlank(purchaseOrder.getShipmentBillId())) {
                    if (purchaseOrder.getPurchaseOrderStatus() == PurchaseOrder.PurchaseOrderStatus.ReceivedShipment) {
                        throw new BlazeInvalidArgException(PURCHASE_ORDER, "PO is closed. System is processing shipment.");
                    } else if (purchaseOrder.getPurchaseOrderStatus() != PurchaseOrder.PurchaseOrderStatus.Closed) {
                        purchaseOrderRepository.updatePurchaseOrderStatus(purchaseOrder.getId(), PurchaseOrder.PurchaseOrderStatus.Closed);
                        throw new BlazeInvalidArgException(PURCHASE_ORDER, "This purchase order is already closed.");
                    }
                }
            }

            List<POProductRequest> poProductRequests = new ArrayList<>();
            BigDecimal totalAmount = completePOProductRequest(request.getPoProductRequestList(), purchaseOrder.getPoProductRequestList(), poProductRequests, request.getManagerReceiveSignature(), true);

            purchaseOrder.setPoProductRequestList(poProductRequests);
            Vendor vendor = vendorRepository.getById(purchaseOrder.getVendorId());
            purchaseOrder.setCompletedDate(DateTime.now().getMillis());
            purchaseOrder.setReceivedDate(request != null ? request.getReceivedDate() : purchaseOrder.getReceivedDate());
            CompanyLicense companyLicense = vendor.getCompanyLicense(purchaseOrder.getLicenseId());

            purchaseOrder.setReceivedDate(request != null ? request.getReceivedDate() : purchaseOrder.getReceivedDate());

            final List<POProductRequest> productRequestList = request.getPoProductRequestList();
            if (!productRequestList.isEmpty()) {
                // Add validation for to check batch sku
                validateBatchSkuForAll(productRequestList);
                for (POProductRequest poProductRequest : productRequestList) {

                    POProductRequest dbProductRequest = null;
                    for (POProductRequest productRequest : poProductRequests) {
                        if (poProductRequest.getId().equalsIgnoreCase(productRequest.getId())) {
                            dbProductRequest = productRequest;
                            break;
                        }
                    }

                    if (dbProductRequest != null) {
                        dbProductRequest.setBatchAddDetails(poProductRequest.getBatchAddDetails());
                    }

                    if (POProductRequest.RequestStatus.ACCEPTED == poProductRequest.getRequestStatus()) {
                        // Validate Arrival Event
                        ValidateCompleteShipmentArrivalEvent validateCompleteShipmentArrivalEvent =
                                new ValidateCompleteShipmentArrivalEvent();
                        validateCompleteShipmentArrivalEvent.setPayload(poProductRequest);
                        blazeEventBus.post(validateCompleteShipmentArrivalEvent);
                        validateCompleteShipmentArrivalEvent.getResponse();

                        // Determine method to create batch (with trackingPackages or without packages)
                        final List<TrackingPackages> trackingPackagesList = poProductRequest.getTrackingPackagesList();
                        if (trackingPackagesList != null && !trackingPackagesList.isEmpty()) {
                            createProductBatchWithPackages(trackingPackagesList, poProductRequest, purchaseOrder, dbProductRequest);
                        } else {
                            createProductBatchWithoutPackages(poProductRequest, purchaseOrder, dbProductRequest);
                        }
                    }
                }
            }


            purchaseOrder.setCompletedByEmployeeId(token.getActiveTopUser().getUserId());
            StringBuilder log = new StringBuilder();
            if (shop != null && shop.getAppTarget() == CompanyFeatures.AppTarget.Retail) {
                //purchaseOrder.setPurchaseOrderStatus(PurchaseOrder.PurchaseOrderStatus.Closed);
                purchaseOrder.setPurchaseOrderStatus(PurchaseOrder.PurchaseOrderStatus.ReceivedShipment);
                log.append("PO completed");
            } else {
                purchaseOrder.setPurchaseOrderStatus(PurchaseOrder.PurchaseOrderStatus.ReceivedShipment);
                Employee employee = employeeRepository.get(token.getCompanyId(), StringUtils.isBlank(request.getReceivedByEmployeeId()) ? token.getActiveTopUser().getUserId() : request.getReceivedByEmployeeId());
                if (employee != null) {
                    log.append("Receive ");
                }
            }
            purchaseOrder.setDeliveredBy(request.getDeliveredBy());
            purchaseOrder.setCompletedDate(DateTime.now().getMillis());
            purchaseOrder.setTotalCost(totalAmount);
            purchaseOrder.setGrandTotal(totalAmount);
            purchaseOrder.setNotes(request.getNotes());

            poActivityService.addPOActivity(purchaseOrder.getId(), StringUtils.isBlank(request.getReceivedByEmployeeId()) ? token.getActiveTopUser().getUserId() : request.getReceivedByEmployeeId(), log.toString());

            if (request.getBackOrderProductList() != null && !request.getBackOrderProductList().isEmpty()) {
                createBackOrderPO(purchaseOrder, request);
            }

            ShipmentBill shipmentBill = shipmentBillService.addShipmentBill(poId, request, purchaseOrder);

            if (shipmentBill != null) {
                HashMap<String, Product> productMap = new HashMap<>();
                List<ObjectId> productCategoryIds = new ArrayList<>();
                Product product = null;
                if (request.getPoProductRequestList().size() > 0) {
                    for (POProductRequest poProductRequest : request.getPoProductRequestList()) {

                        if (!poProductRequest.getProductId().isEmpty()) {
                            product = productRepository.get(token.getCompanyId(), poProductRequest.getProductId());
                            if (product == null) {
                                throw new BlazeInvalidArgException("Product", "Product does not exist");
                            }
                            if (product.getProductType().equals(Product.ProductType.BUNDLE) || product.getProductType().equals(Product.ProductType.DERIVED)) {
                                throw new BlazeInvalidArgException(PURCHASE_ORDER, "Bundle and derived products are not allowed for receiving shipment.");
                            }
                            productMap.put(product.getId(), product);
                            productCategoryIds.add(new ObjectId(product.getCategoryId()));
                        }
                    }
                }


                TaxResult taxResult = null;
                if (Vendor.ArmsLengthType.ARMS_LENGTH.equals(purchaseOrder.getTransactionType()) && CompanyFeatures.AppTarget.Distribution != shop.getAppTarget()) {
                    taxResult = calculateArmsLengthExciseTax(shop, purchaseOrder.getPoProductRequestList(), productMap, productCategoryIds, true);
                } else {
                    taxResult = new TaxResult();
                    taxResult.prepare();
                }
                //OZ tax calculation
                CultivationTaxResult totalCultivationTax = null;
                if ((Vendor.CompanyType.CULTIVATOR.equals(companyLicense.getCompanyType()) || Vendor.CompanyType.MANUFACTURER.equals(companyLicense.getCompanyType())) && PurchaseOrder.FlowerSourceType.CULTIVATOR_DIRECT == purchaseOrder.getFlowerSourceType()) {
                    totalCultivationTax = this.calculateOzTax(shop, purchaseOrder, productMap, productCategoryIds, true);
                }
                BigDecimal cultivationTax = new BigDecimal(0);
                if (totalCultivationTax != null) {
                    cultivationTax = totalCultivationTax.getTotalCultivationTax();
                }
                taxResult.setCultivationTaxResult(totalCultivationTax == null ? new CultivationTaxResult() : totalCultivationTax);
                purchaseOrder.setTaxResult(taxResult);
                BigDecimal grandTotal = new BigDecimal(0);
                BigDecimal totalTax = new BigDecimal(0);
                if (cultivationTax.doubleValue() > 0) {
                    grandTotal = grandTotal.add(cultivationTax);
                }
                grandTotal = grandTotal.add(purchaseOrder.getTotalCost());

                if (purchaseOrder.getTaxResult() != null) {
                    totalTax = purchaseOrder.getTaxResult().getTotalExciseTax();
                    if (purchaseOrder.getTaxResult().getCultivationTaxResult() != null) {
                        totalTax = purchaseOrder.getTaxResult().getTotalExciseTax().add(purchaseOrder.getTaxResult().getCultivationTaxResult().getTotalCultivationTax());
                    }
                    grandTotal = grandTotal.add(purchaseOrder.getTaxResult().getTotalExciseTax());
                    if (PurchaseOrder.CustomerType.CUSTOMER_COMPANY.equals(purchaseOrder.getCustomerType())) {
                        applyDeliveryCharge(shop, purchaseOrder);
                    }
                }
                grandTotal = grandTotal.add(purchaseOrder.getFees());
                grandTotal = grandTotal.subtract(purchaseOrder.getDiscount());
                purchaseOrder.setGrandTotal(grandTotal.add(purchaseOrder.getDeliveryCharge()));

                purchaseOrder.setTotalTax(totalTax);

                purchaseOrder.setAmountPaid(shipmentBill.getAmountPaid());


                purchaseOrder.setShipmentBillId(shipmentBill.getId());

                purchaseOrder.setManagerReceiveSignature(request.getManagerReceiveSignature());
                //process adjustments
                purchaseOrder.setGrandTotal(processAdjustments(purchaseOrder.getAdjustmentInfoList(), purchaseOrder.getGrandTotal()));

                purchaseOrderRepository.update(poId, purchaseOrder);
                shipmentBillResult.setStatus(true);


                // now queue this up to process inventory
                this.createQueueTransactionJobForPO(purchaseOrder);
            }
        } else {
            throw new BlazeInvalidArgException(PURCHASE_ORDER, PO_NOT_FOUND);
        }
        return shipmentBillResult;
    }

    private void createQueueTransactionJobForPO(PurchaseOrder purchaseOrder) {

        QueuedTransaction queuedTransaction = new QueuedTransaction();
        queuedTransaction.setShopId(token.getShopId());
        queuedTransaction.setCompanyId(token.getCompanyId());
        queuedTransaction.setId(ObjectId.get().toString());
        queuedTransaction.setCart(null);
        queuedTransaction.setPendingStatus(Transaction.TransactionStatus.InProgress);
        queuedTransaction.setStatus(QueuedTransaction.QueueStatus.Pending);
        queuedTransaction.setQueuedTransType(QueuedTransaction.QueuedTransactionType.PurchaseOrder);
        queuedTransaction.setSellerId(purchaseOrder.getCompletedByEmployeeId());
        queuedTransaction.setMemberId(null);
        queuedTransaction.setTerminalId(null);
        queuedTransaction.setTransactionId(purchaseOrder.getId());
        queuedTransaction.setRequestTime(DateTime.now().getMillis());

        queuedTransaction.setReassignTransfer(Boolean.FALSE);
        queuedTransaction.setNewTransferInventoryId(null);

        queuedTransactionRepository.save(queuedTransaction);

        backgroundJobService.addTransactionJob(token.getCompanyId(), token.getShopId(), queuedTransaction.getId());

    }

    private BigDecimal completePOProductRequest(List<POProductRequest> poProductUpdateRequests, List<POProductRequest> oldPOProductRequests, List<POProductRequest> newPOProductRequests, CompanyAsset managerReceiveSignature, boolean isRequired) {
        BigDecimal totalAmount = BigDecimal.ZERO;
        for (POProductRequest poProductUpdateRequest : poProductUpdateRequests) {
            Product product = null;
            if (!poProductUpdateRequest.getProductId().isEmpty()) {
                product = productRepository.get(token.getCompanyId(), poProductUpdateRequest.getProductId());
            }
            if (product != null) {
                POProductRequest productRequest = null;
                for (POProductRequest poProductRequest : oldPOProductRequests) {
                    if (poProductUpdateRequest.getId().equals(poProductRequest.getId())) {
                        productRequest = poProductRequest;
                        break;
                    }
                }

                if (productRequest != null) {

                    if (isRequired && (poProductUpdateRequest.getReceivedQuantity().doubleValue() > poProductUpdateRequest.getRequestQuantity().doubleValue()) && managerReceiveSignature == null) {
                        throw new BlazeInvalidArgException("Manager signature", "Manager approval required");
                    }

                    productRequest.setNotes(poProductUpdateRequest.getNotes());
                    productRequest.setProductId(poProductUpdateRequest.getProductId());
                    productRequest.setProductName(product.getName());
                    productRequest.setReceivedQuantity(poProductUpdateRequest.getReceivedQuantity());
                    productRequest.setRequestStatus(poProductUpdateRequest.getRequestStatus());
                    productRequest.setDeclineReason(poProductUpdateRequest.getDeclineReason());
                    productRequest.setReceiveBatchStatus(poProductUpdateRequest.getReceiveBatchStatus());
                    productRequest.setBatchAddDetails(poProductUpdateRequest.getBatchAddDetails());

                    productRequest.setDiscount(poProductUpdateRequest.getDiscount());


                    BigDecimal unitPrice = BigDecimal.ZERO;
                    BigDecimal totalCost = poProductUpdateRequest.getTotalCost();
                    BigDecimal finalTotalCost = BigDecimal.ZERO;
                    if (totalCost != null && poProductUpdateRequest.getRequestQuantity() != null && !poProductUpdateRequest.getRequestQuantity().equals(BigDecimal.ZERO)) {
                        unitPrice = BigDecimal.valueOf(totalCost.doubleValue() / poProductUpdateRequest.getRequestQuantity().doubleValue());
                    }
                    finalTotalCost = totalCost.subtract(poProductUpdateRequest.getDiscount() == null ? BigDecimal.ZERO : poProductUpdateRequest.getDiscount());

                    productRequest.setUnitPrice(unitPrice);
                    productRequest.setTotalCost(totalCost);
                    productRequest.setFinalTotalCost(finalTotalCost);


                    if (poProductUpdateRequest.getTrackingPackagesList() != null && !poProductUpdateRequest.getTrackingPackagesList().isEmpty()) {
                        for (TrackingPackages trackingPackages : poProductUpdateRequest.getTrackingPackagesList()) {
                            trackingPackages.prepare(token.getCompanyId());
                            trackingPackages.setShopId(token.getShopId());
                        }
                    }

                    productRequest.setTrackTraceSystem(poProductUpdateRequest.getTrackTraceSystem());
                    productRequest.setTrackingPackagesList(poProductUpdateRequest.getTrackingPackagesList());

                    totalAmount = totalAmount.add(finalTotalCost);

                    productRequest.setReceiveBatchStatus(poProductUpdateRequest.getReceiveBatchStatus());

                    newPOProductRequests.add(productRequest);
                }
            } else {
                throw new BlazeInvalidArgException("Product", "Product does not exist for this ship.");
            }
        }
        validateBatchSkuForAll(newPOProductRequests);
        return totalAmount;
    }

    private void createBackOrderPO(PurchaseOrder purchaseOrderById, POCompleteRequest request) {
        List<POProductBORequest> backOrderProductList = request.getBackOrderProductList();
        List<POProductRequest> poProductRequestList = purchaseOrderById.getPoProductRequestList();

        PurchaseOrder dbPurchaseOrder;
        Vendor vendor = null;

        vendor = vendorRepository.getById(purchaseOrderById.getVendorId());

        if (vendor != null && vendor.getBackOrderEnabled()) {
            CompanyLicense companyLicense = vendor.getCompanyLicense(purchaseOrderById.getLicenseId());

            PurchaseOrder boPurchaseOrder = new PurchaseOrder();
            boPurchaseOrder.setPoType(PurchaseOrder.POType.BackOrder);
            boPurchaseOrder.prepare(token.getCompanyId());
            boPurchaseOrder.setShopId(token.getShopId());
            boPurchaseOrder.setCreatedByEmployeeId(token.getActiveTopUser().getUserId());
            boPurchaseOrder.setPoPaymentTerms(purchaseOrderById.getPoPaymentTerms());
            boPurchaseOrder.setPoPaymentOptions(purchaseOrderById.getPoPaymentOptions());
            boPurchaseOrder.setCustomTermDate(purchaseOrderById.getCustomTermDate());
            boPurchaseOrder.setPurchaseOrderStatus(PurchaseOrder.PurchaseOrderStatus.InProgress);
            boPurchaseOrder.setParentPOId(purchaseOrderById.getId());
            boPurchaseOrder.setParentPONumber(purchaseOrderById.getPoNumber());
            boPurchaseOrder.setFlowerSourceType(purchaseOrderById.getFlowerSourceType());

            List<POProductRequest> poProductRequests = new ArrayList<>();
            Map<String, Object> poProductRequestResult = createBOPOProductRequest(poProductRequests, backOrderProductList, poProductRequestList);
            BigDecimal totalAmount = (BigDecimal) poProductRequestResult.get("totalAmount");
            Map<String, Product> productMap = (Map<String, Product>) poProductRequestResult.get("products");
            List<ObjectId> productCategoryIds = (List<ObjectId>) poProductRequestResult.get("productCategoryList");
            BigDecimal totalDiscount = (BigDecimal) poProductRequestResult.get("totalDiscount");

            boPurchaseOrder.setPoProductRequestList(poProductRequests);
            boPurchaseOrder.setTotalCost(totalAmount);
            boPurchaseOrder.setGrandTotal(totalAmount);

            boPurchaseOrder.setPoNumber(createBackOrderPONumber(purchaseOrderById.getPoNumber()));
            if (purchaseOrderById.getTransactionType() == null && vendor.getArmsLengthType() != null) {
                boPurchaseOrder.setTransactionType(vendor.getArmsLengthType());
            } else {
                boPurchaseOrder.setTransactionType(purchaseOrderById.getTransactionType());
            }

            Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());

            TaxResult taxResult = null;
            if (Vendor.ArmsLengthType.ARMS_LENGTH.equals(boPurchaseOrder.getTransactionType()) && shop.isRetail()) {
                taxResult = calculateArmsLengthExciseTax(shop, boPurchaseOrder.getPoProductRequestList(), productMap, productCategoryIds, true);

            } else {
                taxResult = new TaxResult();
                taxResult.prepare();
            }
            //OZ tax calculation
            CultivationTaxResult totalCultivationTax = null;
            if ((Vendor.CompanyType.CULTIVATOR.equals(companyLicense.getCompanyType()) || Vendor.CompanyType.MANUFACTURER.equals(companyLicense.getCompanyType())) && PurchaseOrder.FlowerSourceType.CULTIVATOR_DIRECT == boPurchaseOrder.getFlowerSourceType()) {
                totalCultivationTax = this.calculateOzTax(shop, boPurchaseOrder, productMap, productCategoryIds, true);
            }
            BigDecimal cultivationTax = new BigDecimal(0);
            if (totalCultivationTax != null) {
                cultivationTax = totalCultivationTax.getTotalCultivationTax();
            }
            taxResult.setCultivationTaxResult(totalCultivationTax == null ? new CultivationTaxResult() : totalCultivationTax);

            boPurchaseOrder.setTaxResult(taxResult);

            boPurchaseOrder.setFees(purchaseOrderById.getFees());
            boPurchaseOrder.setDiscount(purchaseOrderById.getDiscount());
            boPurchaseOrder.setTotalDiscount(totalDiscount.add(purchaseOrderById.getDiscount()));

            BigDecimal grandTotal = new BigDecimal(0);
            grandTotal = grandTotal.add(boPurchaseOrder.getTotalCost());
            if (boPurchaseOrder.getTaxResult() != null) {
                grandTotal = grandTotal.add(boPurchaseOrder.getTaxResult().getTotalExciseTax());
                grandTotal = grandTotal.add(cultivationTax);
            }
            grandTotal = grandTotal.add(purchaseOrderById.getFees());
            grandTotal = grandTotal.subtract(purchaseOrderById.getDiscount());
            boPurchaseOrder.setGrandTotal(grandTotal);

            boPurchaseOrder.setVendorId(purchaseOrderById.getVendorId());
            if (purchaseOrderById.getTransactionType() == null && vendor.getArmsLengthType() != null) {
                boPurchaseOrder.setTransactionType(vendor.getArmsLengthType());
            } else {
                boPurchaseOrder.setTransactionType(purchaseOrderById.getTransactionType());
            }

            boPurchaseOrder.setDueDate(purchaseOrderById.getDueDate());
            boPurchaseOrder.setDeliveryDate(purchaseOrderById.getDeliveryDate());
            boPurchaseOrder.setDeliveryTime(purchaseOrderById.getDeliveryTime());
            boPurchaseOrder.setNotes(purchaseOrderById.getNotes());
            boPurchaseOrder.setTermsAndCondition(purchaseOrderById.getTermsAndCondition());

            dbPurchaseOrder = purchaseOrderRepository.save(boPurchaseOrder);

            if (dbPurchaseOrder != null) {
                poActivityService.addPOActivity(dbPurchaseOrder.getId(), token.getActiveTopUser().getUserId(), "Created PO (Back Order)");
            }
        }
    }

    /**
     * This method created po number for back order
     *
     * @param parentPONumber : parent PO number
     */
    private String createBackOrderPONumber(String parentPONumber) {
        String poNumber;

        if (parentPONumber.contains("N")) {
            poNumber = parentPONumber.split("-")[0] + "-BO";
        } else {
            int boNumber = 1;
            try {
                boNumber = Integer.parseInt(parentPONumber.split("-BO")[1]);
            } catch (Exception e) {

            }
            poNumber = parentPONumber.split("-")[0] + "-BO" + (boNumber + 1);
        }

        return poNumber;
    }

    private ProductBatch prepareBundleProductBatch(PurchaseOrder purchaseOrder, POProductRequest poProductRequest, POBatchAddRequest batchAddRequest, Product dbProduct, ProductCategory dbCategory, ProductBatch.TrackTraceSystem trackTraceSystem) {
        List<ProductBatch> productBatchList = new ArrayList<>();

        List<BatchBundleItems> batchBundleItems = new ArrayList<>();
        Map<String,List<BatchItems>> batchBundleMap = new HashMap<>();
        if(CollectionUtils.isNotEmpty(batchAddRequest.getBundleItemsBatches())) {
            HashMap<String, ProductCategory> productCategoryMap = productCategoryRepository.listAsMap(token.getCompanyId(), purchaseOrder.getShopId());
            for(POBundleBatchAddRequest bundleBatchRequest: batchAddRequest.getBundleItemsBatches()) {
                Product product = productRepository.get(purchaseOrder.getCompanyId(), poProductRequest.getProductId());
                ProductBatch bundleBatch = prepareProductBatch(purchaseOrder, poProductRequest, bundleBatchRequest.getBatchSku() , bundleBatchRequest.getQuantity(), bundleBatchRequest.getMetrcLabel(), product, productCategoryMap.get(product.getCategoryId()), ProductBatch.TrackTraceSystem.MANUAL, null, bundleBatchRequest.getTrackHarvestBatchId(), bundleBatchRequest.getTrackHarvestBatchDate(), bundleBatchRequest.getBatchExpirationDate(), bundleBatchRequest.getSellBy(), null);

                String productId = bundleBatchRequest.getProductId();
                BatchItems batchItem = new BatchItems();
                batchItem.setBatchId(bundleBatchRequest.getBatchId());
                batchItem.setInventoryId(bundleBatch.getRoomId());
                batchItem.setQuantity(bundleBatchRequest.getQuantity());
                if (batchBundleMap.containsKey(productId)){
                    batchBundleMap.get(productId).add(batchItem);
                }else{
                    List<BatchItems> batchItems = new ArrayList<>();
                    batchItems.add(batchItem);
                    batchBundleMap.put(productId,batchItems);
                }
            }
        }

        batchBundleMap.forEach((key,value) -> {
            BatchBundleItems batchBundleItem = new BatchBundleItems();
            batchBundleItem.setProductId(key);
            batchBundleItem.setBatchItems(value);
            batchBundleItems.add(batchBundleItem);
        });

        ProductBatch productBatch = prepareProductBatch(purchaseOrder, poProductRequest, batchAddRequest.getBatchSku(), batchAddRequest.getQuantity(), batchAddRequest.getMetrcLabel(), dbProduct, dbCategory, ProductBatch.TrackTraceSystem.MANUAL, null, batchAddRequest.getTrackHarvestBatchId(), batchAddRequest.getTrackHarvestBatchDate(), batchAddRequest.getBatchExpirationDate(), batchAddRequest.getSellBy(), batchAddRequest.getConnectedBatchId());
        productBatch.setBundleItems(batchBundleItems);
        return productBatch;
    }

    private Map<String, Object> createBOPOProductRequest(List<POProductRequest> poProductRequests, List<POProductBORequest> backOrderProductList, List<POProductRequest> dbPOProductRequestList) {

        Map<String, Object> productReqResult = new HashMap<>();
        List<ObjectId> productCategoryList = new ArrayList<>();
        productReqResult.put("totalAmount", 0);
        productReqResult.put("products", new ArrayList<>());
        productReqResult.put("productCategoryList", productCategoryList);

        BigDecimal totalAmount = BigDecimal.ZERO;
        BigDecimal totalDiscount = BigDecimal.ZERO;
        POProductRequest poProductRequest;
        HashMap<String, Product> productMap = new HashMap<>();
        for (POProductBORequest productBORequest : backOrderProductList) {
            for (POProductRequest dbProductRequest : dbPOProductRequestList) {
                if (dbProductRequest.getId().equals(productBORequest.getId()) &&
                        dbProductRequest.getReceivedQuantity().compareTo(dbProductRequest.getRequestQuantity()) == -1) {

                    Product product = productRepository.get(token.getCompanyId(), dbProductRequest.getProductId());

                    if (product == null) {
                        throw new BlazeInvalidArgException("Product", "Product not found");
                    }

                    productMap.put(product.getId(), product);
                    productCategoryList.add(new ObjectId(product.getCategoryId()));

                    poProductRequest = new POProductRequest();

                    BigDecimal difference = dbProductRequest.getRequestQuantity().subtract(dbProductRequest.getReceivedQuantity());
                    poProductRequest.prepare(token.getCompanyId());
                    poProductRequest.setShopId(token.getShopId());
                    poProductRequest.setProductId(dbProductRequest.getProductId());
                    poProductRequest.setProductName(dbProductRequest.getProductName());
                    poProductRequest.setRequestQuantity(difference);
                    poProductRequest.setDiscount(dbProductRequest.getDiscount());

                    BigDecimal unitPrice = dbProductRequest.getUnitPrice() != null ? dbProductRequest.getUnitPrice() : BigDecimal.ZERO;
                    BigDecimal totalCost = BigDecimal.valueOf(difference.doubleValue() * unitPrice.doubleValue());

                    poProductRequest.setUnitPrice(unitPrice);

                    BigDecimal finalTotalCost = BigDecimal.ZERO;
                    BigDecimal discount = dbProductRequest.getDiscount() == null ? BigDecimal.ZERO : dbProductRequest.getDiscount();

                    finalTotalCost = totalCost.subtract(discount);

                    poProductRequest.setTotalCost(totalCost);
                    poProductRequest.setFinalTotalCost(finalTotalCost);
                    totalAmount = totalAmount.add(finalTotalCost);
                    totalDiscount = totalDiscount.add(discount);

                    poProductRequests.add(poProductRequest);
                    break;
                }
            }
        }

        productReqResult.put("totalAmount", totalAmount);
        productReqResult.put("products", productMap);
        productReqResult.put("productCategoryList", productCategoryList);
        productReqResult.put("totalDiscount", totalDiscount);
        return productReqResult;
    }

    /**
     * private method for creation a new product batch for set packages
     * if tracking packages are not empty
     *
     * @param trackingPackagesList : tracking package list
     * @param poProductRequest     : poProductRequest
     * @param purchaseOrder        : purchase order
     * @param dbProductRequest     : dbProductRequest
     */
    private void createProductBatchWithPackages(final List<TrackingPackages> trackingPackagesList, final POProductRequest poProductRequest, final PurchaseOrder purchaseOrder, POProductRequest dbProductRequest) {
        HashMap<String, ProductCategory> productCategoryMap = productCategoryRepository.listAsMap(token.getCompanyId(), purchaseOrder.getShopId());

        Shop shop = shopRepository.get(token.getCompanyId(), purchaseOrder.getShopId());
        if (shop == null) {
            throw new BlazeInvalidArgException(PURCHASE_ORDER, "Shop not found");
        }
        Product dbProduct = productRepository.get(purchaseOrder.getCompanyId(), poProductRequest.getProductId());
        if (dbProduct == null) {
            throw new BlazeInvalidArgException(PURCHASE_ORDER, "Product not found");
        }
        PurchaseOrder.CustomerType customerType = checkCustomerType(purchaseOrder);

        Map<String, BigDecimal> batchQuantityMap = new HashMap<>();

        List<ProductBatch> productBatchList = new ArrayList<>();
        List<ObjectId> batchIdList = null;
        List<TestSampleAddRequest> testSampleAddRequests = new ArrayList<>();

        for (TrackingPackages trackingPackages : trackingPackagesList) {
            if (trackingPackages.getQuantity().doubleValue() <= 0) {
                continue;
            }
            ProductBatch productBatch = prepareProductBatch(purchaseOrder, poProductRequest, trackingPackages.getBatchSku(), trackingPackages.getQuantity(), trackingPackages.getPackageLabel(), dbProduct, productCategoryMap.get(dbProduct.getCategoryId()), ProductBatch.TrackTraceSystem.METRC, trackingPackages, trackingPackages.getTrackHarvestBatchId(), trackingPackages.getTrackHarvestBatchDate(), trackingPackages.getBatchExpirationDate(), trackingPackages.getSellBy(), null);
            productBatchList.add(productBatch);
            batchIdList.add(new ObjectId(productBatch.getId()));
            batchQuantityMap.put(productBatch.getId(), trackingPackages.getQuantity());

            if (CompanyFeatures.AppTarget.Distribution == shop.getAppTarget() && poProductRequest.getReceiveBatchStatus() == ProductBatch.BatchStatus.READY_FOR_SALE && StringUtils.isNotBlank(trackingPackages.getReferenceNo())) {
                testSampleAddRequests.add(prepareTestResult(purchaseOrder, productBatch, trackingPackages.getReferenceNo(), trackingPackages.getTestingCompanyId(), trackingPackages.getDateSent(), poProductRequest.getReceiveBatchStatus()));
            }

            batchQuantityMap.put(productBatch.getId(), productBatch.getQuantity());
        }
        dbProductRequest.setBatchQuantityMap(batchQuantityMap);
        productBatchRepository.save(productBatchList);
        if (!testSampleAddRequests.isEmpty()) {
            poTestSampleService.addTestSamples(testSampleAddRequests);
        }
        batchActivityLogService.addBatchActivityLog(batchIdList,token.getActiveTopUser().getUserId()," Add batch for PO ");
    }

    /**
     * private method for creation a new product batch
     * if tracking packages are empty and Metrc enforce is false
     *
     * @param poProductRequest : poProductRequest
     * @param purchaseOrder    : purchaseOrder
     * @param dbProductRequest : dbProductRequest
     */
    private void createProductBatchWithoutPackages(final POProductRequest poProductRequest, final PurchaseOrder purchaseOrder, POProductRequest dbProductRequest) {
        HashMap<String, ProductCategory> productCategoryMap = productCategoryRepository.listAsMap(token.getCompanyId(), purchaseOrder.getShopId());

        Shop shop = shopRepository.get(token.getCompanyId(), purchaseOrder.getShopId());
        if (shop == null) {
            throw new BlazeInvalidArgException(PURCHASE_ORDER, "Shop not found");
        }
        boolean isMetrcEnabled = false;
        try {

            MetrcAccount metrcAccount = metrcService.getMetrcAccountByShopId(token.getCompanyId(),shop.getId());
            isMetrcEnabled = metrcAccount != null && metrcAccount.isEnabled();
        } catch (Exception e) {
            LOGGER.info("Failed to get metrc info");
        }

        Product dbProduct = productRepository.get(purchaseOrder.getCompanyId(), poProductRequest.getProductId());
        if (dbProduct == null) {
            throw new BlazeInvalidArgException(PURCHASE_ORDER, "Product not found");
        }
        PurchaseOrder.CustomerType customerType = checkCustomerType(purchaseOrder);

        /* Add batch add request if no detail is provided or if shop's app target is Retail*/
        if (poProductRequest.getBatchAddDetails() == null || poProductRequest.getBatchAddDetails().isEmpty()) {
            POBatchAddRequest batchAddRequest = new POBatchAddRequest();
            batchAddRequest.setQuantity(poProductRequest.getReceivedQuantity());
            poProductRequest.setBatchAddDetails(Lists.newArrayList(batchAddRequest));
        }

        Map<String, BigDecimal> batchQuantityMap = new HashMap<>();

        List<ProductBatch> productBatchList = new ArrayList<>();
        List<ObjectId> batchIdList = new ArrayList<>();
        List<TestSampleAddRequest> testSampleAddRequests = new ArrayList<>();

        for (POBatchAddRequest batchAddRequest : poProductRequest.getBatchAddDetails()) {
            if (batchAddRequest.getQuantity().doubleValue() <= 0) {
                continue;
            }
            /*ProductBatch productBatch = null;
            if(dbProduct.getProductType()  == Product.ProductType.BUNDLE) {
                productBatch = prepareBundleProductBatch(purchaseOrder, poProductRequest, batchAddRequest, dbProduct, productCategoryMap.get(dbProduct.getCategoryId()), ProductBatch.TrackTraceSystem.MANUAL);
            }else{
                productBatch = prepareProductBatch(purchaseOrder, poProductRequest, batchAddRequest.getBatchSku(), batchAddRequest.getQuantity(), batchAddRequest.getMetrcLabel(), dbProduct, productCategoryMap.get(dbProduct.getCategoryId()), ProductBatch.TrackTraceSystem.MANUAL, null, batchAddRequest.getTrackHarvestBatchId(), batchAddRequest.getTrackHarvestBatchDate(), batchAddRequest.getBatchExpirationDate(), batchAddRequest.getSellBy(),batchAddRequest.getConnectedBatchId());
            }*/
            ProductBatch productBatch = prepareProductBatch(purchaseOrder, poProductRequest, batchAddRequest.getBatchSku(), batchAddRequest.getQuantity(), batchAddRequest.getMetrcLabel(), dbProduct, productCategoryMap.get(dbProduct.getCategoryId()), ProductBatch.TrackTraceSystem.MANUAL, null, batchAddRequest.getTrackHarvestBatchId(), batchAddRequest.getTrackHarvestBatchDate(), batchAddRequest.getBatchExpirationDate(), batchAddRequest.getSellBy(), batchAddRequest.getConnectedBatchId());
            productBatchList.add(productBatch);
            batchIdList.add(new ObjectId(productBatch.getId()));
                if (isMetrcEnabled && StringUtils.isNotBlank(batchAddRequest.getMetrcLabel())) {
                productBatch.setTrackTraceSystem(ProductBatch.TrackTraceSystem.METRC);
                productBatch.setTrackPackageLabel(batchAddRequest.getMetrcLabel());
            }

            batchQuantityMap.put(productBatch.getId(), batchAddRequest.getQuantity());

            if (CompanyFeatures.AppTarget.Distribution == shop.getAppTarget() && poProductRequest.getReceiveBatchStatus() == ProductBatch.BatchStatus.READY_FOR_SALE && StringUtils.isNotBlank(batchAddRequest.getTestId())) {
                testSampleAddRequests.add(prepareTestResult(purchaseOrder, productBatch, batchAddRequest.getTestId(), batchAddRequest.getTestingCompanyId(), batchAddRequest.getDateSent(), poProductRequest.getReceiveBatchStatus()));
            }
        }

        dbProductRequest.setBatchQuantityMap(batchQuantityMap);
        productBatchRepository.save(productBatchList);
        if (!testSampleAddRequests.isEmpty()) {
            poTestSampleService.addTestSamples(testSampleAddRequests);
        }
        batchActivityLogService.addBatchActivityLog(batchIdList,token.getActiveTopUser().getUserId()," Add batch for PO ");

    }

    /**
     * sendEmailToAccounting is used to send email with all shipment bill details , payment history,
     * product information and tracking packages details.
     *
     * @param poId         : purchase order id
     * @param emailRequest
     */
    @Override
    public void sendEmailToAccounting(String poId, EmailRequest emailRequest) {
        final PurchaseOrder purchaseOrder = purchaseOrderRepository.getById(poId);
        if (purchaseOrder != null) {
            final Company company = companyRepository.getById(purchaseOrder.getCompanyId());
            final Shop shop = shopRepository.getById(purchaseOrder.getShopId());
            String body = (shop != null && !shop.isRetail()) ?
                    getPurchaseOrderPdfHtml(purchaseOrder) :
                    emailPOAccountingBody(company, purchaseOrder, shop);

            String fromEMail = "";
            if (shop != null && StringUtils.isNotBlank(shop.getEmailAdress())) {
                fromEMail = shop.getEmailAdress();
            } else {
                fromEMail = "support@blaze.me";
            }

            amazonServiceManager.sendEmail(fromEMail, emailRequest.getEmail(), connectConfiguration.getAppName(), body, null, shop.getEmailAdress(), shop.getName());
        } else {
            throw new BlazeInvalidArgException(PURCHASE_ORDER, PO_NOT_FOUND);
        }
    }

    private String emailPOAccountingBody(final Company company, final PurchaseOrder purchaseOrder, Shop shop) {
        try {
            InputStream inputStream = PurchaseOrderServiceImpl.class.getResourceAsStream("/purchaseOrder.html");
            StringWriter writer = new StringWriter();
            try {
                IOUtils.copy(inputStream, writer, "UTF-8");
            } catch (IOException e) {
                LOGGER.error("Error in emailAccountingBody : " + e);
            }

            String timeZone = token.getRequestTimeZone();
            if (timeZone == null && shop != null) {
                timeZone = shop.getTimeZone();
            }
            int timeZoneOffset = DateUtil.getTimezoneOffsetInMinutes(timeZone);

            String body = writer.toString();
            body = body.replaceAll("==blankSpace==", " ");
            StringBuilder contentSB = new StringBuilder();

            Vendor vendor = vendorRepository.getById(purchaseOrder.getVendorId());
            String vendorName = getVendorSection(vendor.getName());
            body = body.replaceAll("==customerName==", vendorName);
            body = body.replaceAll("==companyName==", shop.getName());
            final Employee employee = employeeService.getEmployeeById(purchaseOrder.getCreatedByEmployeeId());
            body = body.replaceAll("==createdBy==", employee == null ? "" : employee.getFirstName() + " " + employee.getLastName());
            if (purchaseOrder.getCreated() != null) {
                DateTime createdDateTime = new DateTime(purchaseOrder.getCreated());
                long createdDate;
                if (timeZoneOffset < 0) {
                    createdDate = createdDateTime.minusMinutes(timeZoneOffset).getMillis();
                } else {
                    createdDate = createdDateTime.plusMinutes(timeZoneOffset).getMillis();
                }
                body = body.replaceAll("==poCreatedDate==", TextUtil.toDateTime(createdDate, "MM/dd/yyyy hh:mm:ss a"));
            } else {
                body = body.replaceAll("==poCreatedDate==", "");
            }
            if (purchaseOrder.getApprovedDate() != null) {
                DateTime approvedDateTime = new DateTime(purchaseOrder.getApprovedDate());
                long approvedDate;
                if (timeZoneOffset < 0) {
                    approvedDate = approvedDateTime.minusMinutes(timeZoneOffset).getMillis();
                } else {
                    approvedDate = approvedDateTime.plusMinutes(timeZoneOffset).getMillis();
                }

                body = body.replaceAll("==poApprovedDate==", TextUtil.toDateTime(approvedDate, "MM/dd/yyyy hh:mm:ss a"));
            } else {
                body = body.replaceAll("==poApprovedDate==", "");
            }
            String status = purchaseOrder.getPurchaseOrderStatus().toString();
            if (status.equalsIgnoreCase(PurchaseOrder.PurchaseOrderStatus.InProgress.toString())) {
                status = "In Progress";
            }
            if (status.equalsIgnoreCase(PurchaseOrder.PurchaseOrderStatus.ReceivingShipment.toString())) {
                status = "Receiving Shipment";
            }
            if (status.equalsIgnoreCase(PurchaseOrder.PurchaseOrderStatus.RequiredApproval.toString())) {
                status = "Required Approval";
            }
            if (status.equalsIgnoreCase(PurchaseOrder.PurchaseOrderStatus.WaitingShipment.toString())) {
                status = "Waiting Shipment";
            }
            if (status.equalsIgnoreCase(PurchaseOrder.PurchaseOrderStatus.ReceivedShipment.toString())) {
                status = "Received Shipment";
            }
            if (purchaseOrder.isArchive()) {
                status = "Archived";
            }
            body = body.replaceAll("==poStatus==", status);
            body = body.replaceAll("==poTerm==", String.valueOf(purchaseOrder.getPoPaymentTerms()).replaceAll("_", " "));
            body = body.replaceAll("==poPayment==", String.valueOf(purchaseOrder.getPoPaymentOptions().getPaymentType()).replaceAll("_", " "));
            body = body.replaceAll("==notes==", purchaseOrder.getNotes() == null ? "" : purchaseOrder.getNotes());
            body = body.replaceAll("==poNumber==", purchaseOrder.getPoNumber());

            body = body.replaceAll("==cost==", TextUtil.formatToTwoDecimalPoints(purchaseOrder.getGrandTotal()));
            body = body.replaceAll("==subTotal==", TextUtil.formatToTwoDecimalPoints(purchaseOrder.getTotalCost()));
            body = body.replaceAll("==preferredEmailColor==", company.getPreferredEmailColor());


            if (purchaseOrder.getReceivedDate() != null) {
                body = body.replaceAll("==receivedDate==", TextUtil.toDateTime(purchaseOrder.getReceivedDate(), "MM/dd/yyyy hh:mm:ss a"));
            } else {
                body = body.replaceAll("==receivedDate==", "");
            }

            if (purchaseOrder.getCompletedDate() != null) {
                body = body.replaceAll("==completedDate==", TextUtil.toDateTime(purchaseOrder.getCompletedDate()));
            } else {
                body = body.replaceAll("==completedDate==", "");
            }

            body = body.replaceAll("==poDate==", purchaseOrder.getPurchaseOrderDate() > 0 ? DateUtil.toDateTimeFormatted(purchaseOrder.getPurchaseOrderDate(), timeZone, "MM/dd/yyyy") : "N/A");
            body = body.replaceAll("==deliveryDate==", purchaseOrder.getDeliveryDate() > 0 ? DateUtil.toDateTimeFormatted(purchaseOrder.getDeliveryDate(), timeZone, "MM/dd/yyyy hh:mm a") : "N/A");

            if (purchaseOrder.getFees() == null || purchaseOrder.getFees().doubleValue() == 0) {
                body = body.replaceAll("==showFees==", TextUtil.textOrEmpty("display:none"));
            } else {
                body = body.replaceAll("==showFees==", TextUtil.textOrEmpty(""));
                body = body.replaceAll("==fees==", TextUtil.formatToTwoDecimalPoints(purchaseOrder.getFees()));
            }

            if (purchaseOrder.getDiscount() == null || purchaseOrder.getDiscount().doubleValue() == 0) {
                body = body.replaceAll("==showDiscount==", TextUtil.textOrEmpty("display:none"));
            } else {
                body = body.replaceAll("==showDiscount==", TextUtil.textOrEmpty(""));
                body = body.replaceAll("==discount==", TextUtil.formatToTwoDecimalPoints(purchaseOrder.getDiscount()));
            }

            body = body.replaceAll("==showTotalDiscount==", TextUtil.textOrEmpty(""));
            body = body.replaceAll("==totalDiscount==", TextUtil.formatToTwoDecimalPoints(getTotalDiscount(purchaseOrder)));

            boolean showExciseTax = Boolean.FALSE;
            if (shop.isEnableExciseTax() && purchaseOrder.getTaxResult() != null && purchaseOrder.getTaxResult().getTotalExciseTax().doubleValue() != 0) {
                if (ExciseTaxInfo.ExciseTaxType.TOTAL_AMOUNT.equals(shop.getExciseTaxType())) {
                    body = body.replaceAll("==showExciseTax==", TextUtil.textOrEmpty("display:none"));
                    body = body.replaceAll("==exciseTaxMessage==", TextUtil.textOrEmpty("The cannabis excise taxes are included in the total amount of this invoice."));
                    body = body.replaceAll("==showCultivationTax==", TextUtil.textOrEmpty("display:none"));
                } else {
                    showExciseTax = Boolean.TRUE;
                    body = body.replaceAll("==showExciseTaxMessage==", TextUtil.textOrEmpty("display:none"));
                    body = body.replaceAll("==exciseTax==", TextUtil.toEscapeCurrency(purchaseOrder.getTaxResult().getTotalExciseTax().doubleValue(), shop.getDefaultCountry()));
                    body = body.replaceAll("==showCultivationTax==", TextUtil.textOrEmpty("display:none"));
                }
            } else if (!shop.isRetail()) {
                showExciseTax = Boolean.TRUE;
                body = body.replaceAll("==showExciseTaxMessage==", TextUtil.textOrEmpty("display:none"));
                body = body.replaceAll("==showExciseTax==", TextUtil.textOrEmpty("display:none"));

                BigDecimal cultivationTax = BigDecimal.ZERO;
                if (purchaseOrder.getTaxResult().getCultivationTaxResult() != null) {
                    cultivationTax = purchaseOrder.getTaxResult().getCultivationTaxResult().getTotalCultivationTax();
                }
                body = body.replaceAll("==cultivationTax==", TextUtil.toEscapeCurrency(cultivationTax.doubleValue(), shop.getDefaultCountry()));

            } else {
                body = body.replaceAll("==showExciseTax==", TextUtil.textOrEmpty("display:none"));
                body = body.replaceAll("==showExciseTaxMessage==", TextUtil.textOrEmpty("display:none"));
                body = body.replaceAll("==showCultivationTax==", TextUtil.textOrEmpty("display:none"));
            }

            if (purchaseOrder.getPoProductRequestList() != null) {
                int index = 0;
                StringBuilder section = new StringBuilder();
                if (showExciseTax) {
                    for (POProductRequest poProductRequest : purchaseOrder.getPoProductRequestList()) {
                        try {
                            Product product = productRepository.get(token.getCompanyId(), poProductRequest.getProductId());
                            section.append(getProductExciseTaxInformation(++index, poProductRequest, product, shop));

                        } catch (Exception ex) {
                            LOGGER.error("Exception in product information", ex);
                        }
                        body = body.replaceAll("==showNonCannabisProduct==", TextUtil.textOrEmpty("display:none"));
                        if (!shop.isRetail()) {
                            body = body.replaceAll("==showCannabisProductExcise==", TextUtil.textOrEmpty("display:none"));
                        } else {
                            body = body.replaceAll("==showCannabisProductCultivation==", TextUtil.textOrEmpty("display:none"));
                        }
                    }
                } else {

                    for (POProductRequest poProductRequest : purchaseOrder.getPoProductRequestList()) {
                        try {
                            Product product = productRepository.get(token.getCompanyId(), poProductRequest.getProductId());
                            section.append(getProductInformationSection(++index, poProductRequest, product, shop));
                        } catch (Exception ex) {
                            LOGGER.error("Exception in product information", ex);
                        }
                        body = body.replaceAll("==showCannabisProductExcise==", TextUtil.textOrEmpty("display:none"));
                        body = body.replaceAll("==showCannabisProductCultivation==", TextUtil.textOrEmpty("display:none"));
                    }

                }

                body = body.replaceAll("==trackPackages==", Matcher.quoteReplacement(section.toString()));
            }

            body = body.replaceAll("==companyEmail==", (StringUtils.isNotBlank(shop.getEmailAdress())) ? shop.getEmailAdress() : "");
            body = body.replaceAll("==companyPhone==", (StringUtils.isNotBlank(shop.getPhoneNumber())) ? shop.getPhoneNumber() : "");

            String logoURL = shop.getLogo() != null ? shop.getLogo().getPublicURL() : "https://connect-assets.s3.amazonaws.com/email/logo.jpg";
            body = body.replaceAll("==logo==", logoURL);

            if (CollectionUtils.isEmpty(purchaseOrder.getAdjustmentInfoList())) {
                body = body.replaceAll("==showAdjustments==", TextUtil.textOrEmpty("display:none"));
            } else {
                body = body.replaceAll("==showAdjustments==", TextUtil.textOrEmpty(""));
                List<ObjectId> objectIds = new ArrayList<>();
                for (AdjustmentInfo adjustmentInfo : purchaseOrder.getAdjustmentInfoList()) {
                    objectIds.add(new ObjectId(adjustmentInfo.getAdjustmentId()));
                }
                HashMap<String, Adjustment> infoHashMap = adjustmentRepository.listAsMap(token.getCompanyId(), objectIds);
                StringBuilder adjustmentInfoStr = new StringBuilder();

                for (AdjustmentInfo adjustmentInfo : purchaseOrder.getAdjustmentInfoList()) {
                    Adjustment adjustment = infoHashMap.get(adjustmentInfo.getAdjustmentId());
                    if (adjustment == null) {
                        continue;
                    }
                    String amountStr = TextUtil.toCurrency(adjustmentInfo.getAmount().doubleValue(), shop.getDefaultCountry());
                    String color = adjustmentInfo.isNegative() ? "red" : "black";
                    amountStr = adjustmentInfo.isNegative() ? "- {" + amountStr + "}" : amountStr;

                    adjustmentInfoStr.append("<tr><td width=\"330\" height=\"30\">" + adjustment.getName() + "</td>" +
                            "<td width=\"210\" height=\"30\" style =\"color:" + color + "\"><sup style=\"font-size:0.6em;\"></sup>" + amountStr + "</td></tr>");

                }
                body = body.replaceAll("==adjustmentInfo==", Matcher.quoteReplacement(TextUtil.textOrEmpty(adjustmentInfoStr.toString())));
            }
            return body;
        } catch (Exception ex) {
            LOGGER.error("Problem in Purchase Order Information", ex);
            return "Information is not completed";
        }
    }

    private String getProductExciseTaxInformation(int index, POProductRequest poProductRequest, Product product, Shop shop) {
        String requestQuantity = poProductRequest.getRequestQuantity() + " " + (product == null ? "" : product.getCategory().getUnitType().toString());
        double tax = (CompanyFeatures.AppTarget.Distribution == shop.getAppTarget()) ? poProductRequest.getTotalCultivationTax().doubleValue() : poProductRequest.getTotalExciseTax().doubleValue();

        BigDecimal finalTotalCost = poProductRequest.getFinalTotalCost();
        double discount = poProductRequest.getDiscount() == null ? 0 : poProductRequest.getDiscount().doubleValue();

        if (finalTotalCost == null || finalTotalCost.doubleValue() == 0) {
            finalTotalCost = poProductRequest.getTotalCost().subtract(poProductRequest.getDiscount());
        }

        double amount = finalTotalCost.doubleValue() + tax;

        return "<tr>\n" +
                "<td width=\"50\" height=\"30\">" + index + "</td>\n" +
                "<td width=\"100\" height=\"30\">" + poProductRequest.getProductName() + "</td>\n" +
                "<td width=\"100\" height=\"30\">" + "&nbsp;" + productRepository.get(token.getCompanyId(), poProductRequest.getProductId()).getSku() + "   " + "</td>\n" +
                "<td width=\"100\" height=\"30\">" + "&nbsp; &nbsp;" + requestQuantity + "</td>\n" +
                "<td width=\"100\" height=\"30\">" + TextUtil.toCurrency(poProductRequest.getUnitPrice().doubleValue(), shop.getDefaultCountry()) + "</td>\n" +
                "<td width=\"100\" height=\"30\">" + TextUtil.toCurrency(tax, shop.getDefaultCountry()) + "</td>\n" +
                "<td width=\"100\" height=\"30\">" + "&nbsp; &nbsp;" + TextUtil.toCurrency(discount, shop.getDefaultCountry()) + "</td>\n" +
                "<td width=\"100\" height=\"30\">" + TextUtil.toCurrency(amount, shop.getDefaultCountry()) + "</td>\n" +
                "</tr>";
    }


    private String getProductInformationSection(final int index, final POProductRequest poProductRequest, Product product, Shop shop) {
        String requestQuantity = poProductRequest.getRequestQuantity() + " " + (product == null ? "" : product.getCategory().getUnitType().toString());
        double tax = (!shop.isRetail()) ? poProductRequest.getTotalCultivationTax().doubleValue() : poProductRequest.getTotalExciseTax().doubleValue();

        BigDecimal finalTotalCost = poProductRequest.getFinalTotalCost();
        double discount = poProductRequest.getDiscount() == null ? 0 : poProductRequest.getDiscount().doubleValue();

        if (finalTotalCost == null || finalTotalCost.doubleValue() == 0) {
            finalTotalCost = poProductRequest.getTotalCost().subtract(poProductRequest.getDiscount());
        }

        double amount = finalTotalCost.doubleValue() + tax;

        return "<tr>\n" +
                "<td width=\"50\" height=\"30\">" + index + "</td>\n" +
                "<td width=\"100\" height=\"30\">" + poProductRequest.getProductName() + "</td>\n" +
                "<td width=\"100\" height=\"30\">" + "&nbsp;" + productRepository.get(token.getCompanyId(), poProductRequest.getProductId()).getSku() + "</td>\n" +
                "<td width=\"100\" height=\"30\">" + "&nbsp; &nbsp;" + requestQuantity + "</td>\n" +
                "<td width=\"100\" height=\"30\">" + TextUtil.toCurrency(poProductRequest.getUnitPrice().doubleValue(), shop.getDefaultCountry()) + "</td>\n" +
                "<td width=\"100\" height=\"30\">" + "&nbsp; &nbsp;" + TextUtil.toCurrency(discount, shop.getDefaultCountry())  + "</td>\n" +
                "<td width=\"100\" height=\"30\">" + "&nbsp; &nbsp;" + TextUtil.toCurrency(amount, shop.getDefaultCountry()) + "</td>\n" +
                "</tr>";
    }

    private String getTrackingPackagesSection() {
        return "<tr>\n" +
                "\t\t\t\t\t\t\t\t\t\t<td width=\"330\" height=\"40\">==packageLabel==</td>\n" +
                "\t\t\t\t\t\t\t\t\t\t<td width=\"330\" height=\"40\">==quantity==</td>\n" +
                "\t\t\t\t\t\t\t\t\t</tr>";
    }

    private String getCustomerCompanySection(final String name) {
        return "<tr>\n" +
                "                                <td width=\"330\" height=\"30\"\t>Customer Company Name</td>\n" +
                "                                <td width=\"210\" height=\"30\"><sup style=\"font-size:0.6em;\"></sup>" + name + "</td>\n" +
                "                            </tr>";
    }

    private String getVendorSection(final String name) {
        return "<tr>\n" +
                "                                <td width=\"330\" height=\"30\"\t>Vendor Name</td>\n" +
                "                                <td width=\"210\" height=\"30\"><sup style=\"font-size:0.6em;\"></sup>" + name + "</td>\n" +
                "                            </tr>\n";
    }

    @Override
    public PurchaseOrder saveAsPendingPO(String poId, PurchaseOrder request) {
        PurchaseOrder updatedPurchaseOrder = null;

        PurchaseOrder purchaseOrder = this.getPurchaseOrderById(poId);

        Vendor vendor = vendorRepository.getById(request.getVendorId());

        if (vendor == null) {
            throw new BlazeInvalidArgException(PURCHASE_ORDER, "Please provide a valid vendor.");
        }

        if (request.getPoProductRequestList() == null || request.getPoProductRequestList().size() == 0) {
            throw new BlazeInvalidArgException(PURCHASE_ORDER, NOT_VALID_PRODUCT);
        }

        if (purchaseOrder != null) {

            purchaseOrder.setDeliveredBy(request.getDeliveredBy());
            purchaseOrder.setNotes(request.getNotes());

            List<POProductRequest> poProductRequestList = new ArrayList<>();
            BigDecimal totalAmount = completePOProductRequest(request.getPoProductRequestList(), purchaseOrder.getPoProductRequestList(), poProductRequestList, request.getManagerReceiveSignature(), false);

            purchaseOrder.setPoProductRequestList(poProductRequestList);
            purchaseOrder.setTotalCost(totalAmount);
            purchaseOrder.setGrandTotal(totalAmount);

            HashMap<String, Product> productMap = new HashMap<>();
            List<ObjectId> productCategoryIds = new ArrayList<>();
            Product product = null;
            if (request.getPoProductRequestList().size() > 0) {
                for (POProductRequest poProductRequest : request.getPoProductRequestList()) {

                    if (!poProductRequest.getProductId().isEmpty()) {
                        product = productRepository.get(token.getCompanyId(), poProductRequest.getProductId());
                        if (product == null) {
                            throw new BlazeInvalidArgException("Product", "Product does not exist");
                        }
                        productMap.put(product.getId(), product);
                        productCategoryIds.add(new ObjectId(product.getCategoryId()));
                    }
                }
            }


            TaxResult taxResult = null;
            Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());
            if (Vendor.ArmsLengthType.ARMS_LENGTH.equals(purchaseOrder.getTransactionType()) && CompanyFeatures.AppTarget.Distribution != shop.getAppTarget()) {
                taxResult = calculateArmsLengthExciseTax(shop, purchaseOrder.getPoProductRequestList(), productMap, productCategoryIds, true);
            }

            purchaseOrder.setFees(request.getFees());
            purchaseOrder.setDiscount(request.getDiscount());

            purchaseOrder.setTaxResult(taxResult);
            BigDecimal grandTotal = new BigDecimal(0);
            grandTotal = grandTotal.add(purchaseOrder.getTotalCost());
            if (purchaseOrder.getTaxResult() != null) {
                grandTotal = grandTotal.add(purchaseOrder.getTaxResult().getTotalExciseTax());
            }
            grandTotal = grandTotal.add(purchaseOrder.getFees());
            grandTotal = grandTotal.subtract(purchaseOrder.getDiscount());
            purchaseOrder.setGrandTotal(processAdjustments(request.getAdjustmentInfoList(), grandTotal));
            purchaseOrder.setReceivedDate(request.getReceivedDate() != 0 ? request.getReceivedDate() : purchaseOrder.getReceivedDate());

            //Old assigned shipments to false
            if (CollectionUtils.isNotEmpty(purchaseOrder.getAssignedShipments())) {
                for (Shipment shipment : purchaseOrder.getAssignedShipments()){
                    shipment.setAssigned(false);
                    shipmentRepository.save(shipment);
                }
            }

            purchaseOrder.sendAssignedShipments(request.getAssignedShipments());

            updatedPurchaseOrder = purchaseOrderRepository.update(token.getCompanyId(), purchaseOrder.getId(), purchaseOrder);

            //New assigned shipments to true
            if (CollectionUtils.isNotEmpty(purchaseOrder.getAssignedShipments())) {
                for (Shipment shipment : purchaseOrder.getAssignedShipments()){
                    shipment.setAssigned(true);
                    shipmentRepository.save(shipment);
                }
            }

            if (updatedPurchaseOrder != null) {
                poActivityService.addPOActivity(updatedPurchaseOrder.getId(), token.getActiveTopUser().getUserId(), "Save as pending");
            }

        } else {
            throw new BlazeInvalidArgException(PURCHASE_ORDER, PO_NOT_FOUND);
        }

        return updatedPurchaseOrder;
    }

    /**
     * This method get metrics package by label
     *
     * @param label : label for which Metrcs need to get
     */
    @Override
    public MetrcVerifiedPackage getMetricPackageByLabel(final String label) {

        final MetrcVerifiedPackage metrcVerifiedPackage = metrcAccountService.getMetrcVerifiedPackage(label);

        if (metrcVerifiedPackage == null) {
            throw new BlazeInvalidArgException("label", "Please provide a valid label.");
        }

        return metrcVerifiedPackage;
    }

    /**
     * This method get archived PO list
     *
     * @param start : start
     * @param limit : limit
     */
    @Override
    public SearchResult<PurchaseOrderItemResult> getArchivedPurchaseOrder(int start, int limit, PurchaseOrder.CustomerType customerType, String term) {
        customerType = (customerType == null) ? PurchaseOrder.CustomerType.VENDOR : customerType;
        SearchResult<PurchaseOrderItemResult> searchResult;
        if (StringUtils.isBlank(term)) {
            searchResult = purchaseOrderRepository.listAllArchivedPO(token.getCompanyId(), token.getShopId(), customerType, start, limit, "{archiveDate:-1}");
        } else {
            searchResult = purchaseOrderRepository.listAllArchiveByTerm(token.getCompanyId(), token.getShopId(), customerType, start, limit, "{archiveDate:-1}", term, PurchaseOrderItemResult.class);
        }
        if (searchResult.getValues() != null) {

            HashMap<String, Object> preparedResult = this.getInfoForPurchaseOrderList(searchResult.getValues());
            HashMap<String, Employee> employeeMap = (HashMap<String, Employee>) preparedResult.get("Employee");
            HashMap<String, Vendor> vendorMap = (HashMap<String, Vendor>) preparedResult.get("Vendor");
            HashMap<String, Product> productMap = (HashMap<String, Product>) preparedResult.get("Product");

            for (PurchaseOrderItemResult purchaseOrderItemResult : searchResult.getValues()) {
                preparePurchaseOrderResult(purchaseOrderItemResult, employeeMap, vendorMap, productMap);
            }
        }

        return searchResult;
    }

    /**
     * Mark PO as archived
     *
     * @param purchaseOrderId : id of
     *                        purchase order that need to mark as archived
     */
    @Override
    public PurchaseOrder archivePurchaseOrder(String purchaseOrderId) {
        PurchaseOrder updatedPurchaseOrder;
        PurchaseOrder dbPurchaseOrder = this.getPurchaseOrderById(purchaseOrderId);

        dbPurchaseOrder.setArchive(true);
        dbPurchaseOrder.setArchiveDate(DateTime.now().getMillis());

        updatedPurchaseOrder = purchaseOrderRepository.update(token.getCompanyId(), dbPurchaseOrder.getId(), dbPurchaseOrder);

        if (updatedPurchaseOrder != null) {
            poActivityService.addPOActivity(updatedPurchaseOrder.getId(), token.getActiveTopUser().getUserId(), "PO mark as archived");
        }

        return updatedPurchaseOrder;
    }

    /**
     * This method send PO to vendor
     *
     * @param purchaseOrderId : purchase order id
     */
    @Override
    public void emailPOToVendor(String purchaseOrderId) {
        final PurchaseOrder purchaseOrder = purchaseOrderRepository.getById(purchaseOrderId);
        if (purchaseOrder == null) {
            throw new BlazeInvalidArgException(PURCHASE_ORDER, PO_NOT_FOUND);
        }

        Vendor vendor = vendorRepository.getById(purchaseOrder.getVendorId());
        if (vendor == null) {
            throw new BlazeInvalidArgException(PURCHASE_ORDER, VENDOR_NOT_FOUND);
        }

        if (StringUtils.isBlank(vendor.getEmail())) {
            throw new BlazeInvalidArgException(PURCHASE_ORDER, "Error! Vendor does not have email address");
        }

        final Company company = companyRepository.getById(purchaseOrder.getCompanyId());
        Shop shop = shopRepository.getById(purchaseOrder.getShopId());
        String body = emailPOAccountingBody(company, purchaseOrder, shop);

        String fromEmail = (shop != null && StringUtils.isNotBlank(shop.getEmailAdress())) ? shop.getEmailAdress() : "support@blaze.me";

        amazonServiceManager.sendEmail(fromEmail, vendor.getEmail(), connectConfiguration.getAppName(), body, null, shop.getEmailAdress(), shop.getName());
    }


    @Override
    public PurchaseOrderItemResult updateAllPOProductRequestStatus(String poID, List<POProductRequestUpdate> request) {

        PurchaseOrderItemResult purchaseOrderItemResult = purchaseOrderRepository.get(token.getCompanyId(), poID, PurchaseOrderItemResult.class);

        if (purchaseOrderItemResult == null) {
            throw new BlazeInvalidArgException(PURCHASE_ORDER, PO_NOT_FOUND);
        }
        List<POProductRequest> poProductRequestList = purchaseOrderItemResult.getPoProductRequestList();

        for (POProductRequest poProductRequest : poProductRequestList) {
            for (POProductRequestUpdate poProductRequestUpdate : request) {
                if (poProductRequest.getId().equals(poProductRequestUpdate.getProductRequestId())) {

                    poProductRequest.setRequestStatus(poProductRequestUpdate.isStatus() ? POProductRequest.RequestStatus.ACCEPTED : POProductRequest.RequestStatus.DECLINED);
                    poProductRequest.setReceivedQuantity(poProductRequestUpdate.getReceivedQuantity());
                    poProductRequest.setDeclineReason(poProductRequestUpdate.getDeclineReason());
                    poProductRequest.setReceiveBatchStatus(poProductRequestUpdate.getReceiveBatchStatus());
                    poProductRequest.setBatchAddDetails(poProductRequestUpdate.getBatchAddDetails());

                }
            }
        }

        validateBatchSkuForAll(poProductRequestList);

        purchaseOrderRepository.update(token.getCompanyId(), purchaseOrderItemResult.getId(), purchaseOrderItemResult);

        LinkedHashSet<ObjectId> employeeIds = new LinkedHashSet<>();
        LinkedHashSet<ObjectId> vendorIds = new LinkedHashSet<>();
        LinkedHashSet<ObjectId> productsIds = new LinkedHashSet<>();
        LinkedHashSet<ObjectId> customerCompanyIds = new LinkedHashSet<>();

        this.getInfoForPurchaseOrder(purchaseOrderItemResult, employeeIds, vendorIds, productsIds, customerCompanyIds);

        HashMap<String, Employee> employeeMap = employeeRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(employeeIds));
        HashMap<String, Vendor> vendorMap = vendorRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(vendorIds));
        HashMap<String, Product> productMap = productRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(productsIds));

        preparePurchaseOrderResult(purchaseOrderItemResult, employeeMap, vendorMap, productMap);
        return purchaseOrderItemResult;
    }

    /**
     * This Method is to get customerType from purchaseOrder and provide valid customerType
     *
     * @param purchaseOrder : PurchaseOrder object
     * @return customerType
     */
    private PurchaseOrder.CustomerType checkCustomerType(PurchaseOrder purchaseOrder) {
        PurchaseOrder.CustomerType customerType = null;

        Vendor vendor = vendorRepository.getById(purchaseOrder.getVendorId());
        if (vendor == null) {
            throw new BlazeInvalidArgException(PURCHASE_ORDER, VENDOR_NOT_FOUND);
        }
        customerType = PurchaseOrder.CustomerType.VENDOR;


        return customerType;
    }

    /**
     * This method send PO to customer company
     *
     * @param purchaseOrderId : purchase order id
     */
    @Override
    public void emailPOToCustomerCompany(String purchaseOrderId) {
        final PurchaseOrder purchaseOrder = purchaseOrderRepository.getById(purchaseOrderId);
        if (purchaseOrder == null) {
            throw new BlazeInvalidArgException(PURCHASE_ORDER, PO_NOT_FOUND);
        }

        Vendor customerCompany = vendorRepository.getById(purchaseOrder.getVendorId());
        if (customerCompany == null) {
            throw new BlazeInvalidArgException(PURCHASE_ORDER, "Error! Customer Company not found");
        }

        if (StringUtils.isBlank(customerCompany.getEmail())) {
            throw new BlazeInvalidArgException(PURCHASE_ORDER, "Error! Customer Company does not have email address");
        }

        final Company company = companyRepository.getById(purchaseOrder.getCompanyId());
        Shop shop = shopRepository.getById(purchaseOrder.getShopId());
        String body = emailPOAccountingBody(company, purchaseOrder, shop);

        String ccEmail = null;
        if (shop != null) {
            ccEmail = shop.getEmailAdress();
        }

        amazonServiceManager.sendEmail("support@blaze.me", customerCompany.getEmail(), connectConfiguration.getAppName(), body, null, ccEmail, shop.getName());
    }

    private PurchaseOrder applyDeliveryCharge(Shop shop, PurchaseOrder purchaseOrder) {

        if (shop != null && purchaseOrder != null) {
            BigDecimal totalAmount = purchaseOrder.getTotalCost();
            if (totalAmount == null) {
                totalAmount = new BigDecimal(0);
            }

            List<DeliveryFee> deliveryFees = shop.getDeliveryFees();
            deliveryFees.sort(new DeliveryFee.DeliveryFeeComparator());

            for (DeliveryFee deliveryFee : deliveryFees) {
                boolean enabled = deliveryFee.isEnabled();

                if (enabled && totalAmount.compareTo(deliveryFee.getSubTotalThreshold()) >= 0) {
                    purchaseOrder.setDeliveryCharge(deliveryFee.getFee());
                    purchaseOrder.setEnableDeliveryCharge(true);
                }
                /*
                if (!enabled) {
                    purchaseOrder.setDeliveryCharge(BigDecimal.ZERO);
                    purchaseOrder.setEnableDeliveryCharge(false);
                }*/
            }
        }
        return purchaseOrder;
    }

    /***
     * Prepare and return purchase order for add request
     * @param purchaseOrderAddRequest
     * @return
     */
    @Override
    public PurchaseOrder preparePurchaseOrder(PurchaseOrderAddRequest purchaseOrderAddRequest) {

        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());
        if (shop == null) {
            throw new BlazeInvalidArgException(SHOP, SHOP_NOT_FOUND);
        }

        if (purchaseOrderAddRequest.getPoProductAddRequestList() == null
                || purchaseOrderAddRequest.getPoProductAddRequestList().size() == 0) {
            throw new BlazeInvalidArgException(PURCHASE_ORDER, NOT_VALID_PRODUCT);
        }

        if (PurchaseOrder.POPaymentTerms.CUSTOM_DATE.toString().equals(purchaseOrderAddRequest.getPoPaymentTerms()) &&
                purchaseOrderAddRequest.getCustomTermDate() == 0) {
            throw new BlazeInvalidArgException(CUSTOM_DATE, NOT_CUSTOM_DATE);
        }

        PurchaseOrder purchaseOrder = new PurchaseOrder();
        purchaseOrder.setCreatedByEmployeeId(token.getActiveTopUser().getUserId());
        purchaseOrder.setNotes(purchaseOrderAddRequest.getNotes());
        purchaseOrder.setPoPaymentTerms(PurchaseOrder.POPaymentTerms.valueOf(purchaseOrderAddRequest.getPoPaymentTerms()));
        purchaseOrder.setPoPaymentOptions(PurchaseOrder.POPaymentOptions.toPOPaymentOptions(purchaseOrderAddRequest.getPaymentType()));
        purchaseOrder.setPurchaseOrderStatus(PurchaseOrder.PurchaseOrderStatus.InProgress);
        purchaseOrder.setCustomTermDate(purchaseOrderAddRequest.getCustomTermDate());

        List<POProductRequest> poProductRequests = new ArrayList<>();
        Map<String, Object> poProductRequestResult = createPOProductRequest(purchaseOrderAddRequest.getPoProductAddRequestList(), poProductRequests);
        BigDecimal totalAmount = (BigDecimal) poProductRequestResult.get("totalAmount");
        Map<String, Product> productMap = (Map<String, Product>) poProductRequestResult.get("products");
        List<ObjectId> productCategoryIds = (List<ObjectId>) poProductRequestResult.get("productCategoryList");
        BigDecimal totalItemDiscount = (BigDecimal) poProductRequestResult.getOrDefault("totalDiscount", BigDecimal.ZERO);

        purchaseOrder.setPoProductRequestList(poProductRequests);
        purchaseOrder.setTotalCost(totalAmount);
        purchaseOrder.setGrandTotal(totalAmount);
        purchaseOrder.setReference(purchaseOrderAddRequest.getReference());
        if (purchaseOrderAddRequest.getDiscount().doubleValue() > purchaseOrder.getTotalCost().doubleValue()) {
            throw new BlazeInvalidArgException(PURCHASE_ORDER, "Discount cannot be greater than total amount.");
        }
        if (StringUtils.isBlank(purchaseOrderAddRequest.getVendorId())) {
            throw new BlazeInvalidArgException(PURCHASE_ORDER, VENDOR_NOT_FOUND);
        }

        Vendor vendor = vendorRepository.getById(purchaseOrderAddRequest.getVendorId());
        if (vendor == null) {
            throw new BlazeInvalidArgException(PURCHASE_ORDER, VENDOR_NOT_FOUND);
        }
        if (vendor.getVendorType().equals(Vendor.VendorType.CUSTOMER)) {
            throw new BlazeInvalidArgException(PURCHASE_ORDER, PO_NOT_ALLOWED_FOR_CUSTOMER);
        }

        CompanyLicense companyLicense = vendor.getCompanyLicense(purchaseOrderAddRequest.getLicenseId());
        if (companyLicense == null) {
            throw new BlazeInvalidArgException(PURCHASE_ORDER, "Company License not found");
        }
        if (!shop.isRetail()) {
            DateTime currentTime = DateTime.now().withTimeAtStartOfDay();
            String timeZone = shop.getTimeZone();
            if (StringUtils.isBlank(timeZone)) {
                timeZone = DateTimeZone.UTC.toString();
            }
            DateTime expirationDate = DateUtil.toDateTime(companyLicense.getLicenseExpirationDate(), timeZone).withTimeAtStartOfDay();

            if (Objects.nonNull(companyLicense.getLicenseExpirationDate()) && companyLicense.getLicenseExpirationDate() != 0 && expirationDate.isBefore(currentTime)) {
                throw new BlazeInvalidArgException(PURCHASE_ORDER, CUSTOMER_LICENSE_EXPIRED);
            }
        }

        purchaseOrder.setCustomerType(purchaseOrderAddRequest.getCustomerType());

        purchaseOrder.setVendorId(purchaseOrderAddRequest.getVendorId());
        purchaseOrder.setLicenseId(companyLicense.getId());
        if (purchaseOrderAddRequest.getTransactionType() == null && vendor.getArmsLengthType() != null) {
            purchaseOrder.setTransactionType(vendor.getArmsLengthType());
        } else {
            purchaseOrder.setTransactionType(purchaseOrderAddRequest.getTransactionType());
        }

        purchaseOrder.setDiscount(purchaseOrderAddRequest.getDiscount());
        purchaseOrder.setFees(purchaseOrderAddRequest.getFees());
        purchaseOrder.setTransactionType(purchaseOrderAddRequest.getTransactionType());
        purchaseOrder.setFlowerSourceType(purchaseOrderAddRequest.getFlowerSourceType());
        purchaseOrder.setTotalDiscount(totalItemDiscount.add(purchaseOrder.getDiscount()));

        TaxResult taxResult = null;
        if (Vendor.ArmsLengthType.ARMS_LENGTH.equals(purchaseOrder.getTransactionType()) && shop.isRetail()) {
            taxResult = calculateArmsLengthExciseTax(shop, purchaseOrder.getPoProductRequestList(), productMap, productCategoryIds, true);
        }
        //OZ tax calculation
        CultivationTaxResult totalCultivationTax = null;
        if ((!shop.isRetail() && Vendor.CompanyType.CULTIVATOR.equals(companyLicense.getCompanyType()) || Vendor.CompanyType.MANUFACTURER.equals(companyLicense.getCompanyType())) && PurchaseOrder.FlowerSourceType.CULTIVATOR_DIRECT == purchaseOrder.getFlowerSourceType()) {
            totalCultivationTax = this.calculateOzTax(shop, purchaseOrder, productMap, productCategoryIds, true);
        }
        if (taxResult == null) {
            taxResult = new TaxResult();
            taxResult.prepare();
        }
        BigDecimal cultivationTax = new BigDecimal(0);
        if (totalCultivationTax != null) {
            cultivationTax = totalCultivationTax.getTotalCultivationTax();
        }
        taxResult.setCultivationTaxResult(totalCultivationTax == null ? new CultivationTaxResult() : totalCultivationTax);
        purchaseOrder.setTaxResult(taxResult);

        BigDecimal grandTotal = new BigDecimal(0);
        BigDecimal totalTax = new BigDecimal(0);
        if (cultivationTax.doubleValue() > 0) {
            grandTotal = grandTotal.add(cultivationTax);
        }
        grandTotal = grandTotal.add(purchaseOrder.getFees());
        grandTotal = grandTotal.subtract(purchaseOrder.getDiscount());
        grandTotal = grandTotal.add(purchaseOrder.getTotalCost());
        if (purchaseOrder.getTaxResult() != null) {
            totalTax = purchaseOrder.getTaxResult().getTotalExciseTax();
            if (purchaseOrder.getTaxResult().getCultivationTaxResult() != null) {
                totalTax = purchaseOrder.getTaxResult().getTotalExciseTax().add(purchaseOrder.getTaxResult().getCultivationTaxResult().getTotalCultivationTax());
            }

            grandTotal = grandTotal.add(purchaseOrder.getTaxResult().getTotalExciseTax());
            if (PurchaseOrder.CustomerType.CUSTOMER_COMPANY.equals(purchaseOrder.getCustomerType())) {
                applyDeliveryCharge(shop, purchaseOrder);
            }
        }
        purchaseOrder.setGrandTotal(grandTotal.add(purchaseOrder.getDeliveryCharge()));

        purchaseOrder.setTotalTax(totalTax);
        purchaseOrder.setDeliveryAddress(purchaseOrderAddRequest.getDeliveryAddress());
        purchaseOrder.setDeliveryTime(purchaseOrderAddRequest.getDeliveryTime());
        purchaseOrder.setTermsAndCondition(purchaseOrderAddRequest.getTermsAndCondition());
        purchaseOrder.setDeliveryDate(purchaseOrderAddRequest.getDeliveryDate());

        purchaseOrder.setPurchaseOrderDate(purchaseOrderAddRequest.getPurchaseOrderDate());

        purchaseOrder.setDueDate(purchaseOrder.getPODueDate());
        purchaseOrder.setAdjustmentInfoList(purchaseOrderAddRequest.getAdjustmentInfoList());

        //process adjustments
        purchaseOrder.setGrandTotal(processAdjustments(purchaseOrder.getAdjustmentInfoList(), purchaseOrder.getGrandTotal()));

        return purchaseOrder;

    }

    /**
     * This method process adjustments
     * @param adjustmentInfoList : adjustment info list
     * @param reqGrandTotal : grand total
     */
    @Override
    public BigDecimal processAdjustments(List<AdjustmentInfo> adjustmentInfoList, BigDecimal reqGrandTotal) {
        if (adjustmentInfoList == null || adjustmentInfoList.size() == 0) {
            return reqGrandTotal;
        }

        List<ObjectId> adjustmentIds = new ArrayList<>();
        for (AdjustmentInfo info : adjustmentInfoList) {
            if (StringUtils.isNotBlank(info.getAdjustmentId()) && ObjectId.isValid(info.getAdjustmentId())) {
                adjustmentIds.add(new ObjectId(info.getAdjustmentId()));
            }
        }

        HashMap<String, Adjustment> adjustmentMap = adjustmentRepository.listAsMap(token.getCompanyId(), adjustmentIds);

        double adjAmount = 0;
        int factor;
        for (AdjustmentInfo info : adjustmentInfoList) {

            Adjustment adjustment = adjustmentMap.get(info.getAdjustmentId());
            if (adjustment == null || !adjustment.isActive()) {
                throw new BlazeInvalidArgException(PURCHASE_ORDER, "Please add valid adjustment");
            }
            if (info.getAmount() == null || info.getAmount().doubleValue() == 0) {
                throw new BlazeInvalidArgException(PURCHASE_ORDER, VALID_ADJUSTMENT_AMOUNT);
            }

            factor = 1;
            if (info.isNegative()) {
                factor = -1;
            }
            adjAmount += (info.getAmount().doubleValue() * factor);
        }
        double grandTotal = reqGrandTotal.doubleValue() + adjAmount;

        return new BigDecimal(grandTotal);
    }

    private CultivationTaxResult calculateOzTax(Shop shop, PurchaseOrder purchaseOrder, Map<String, Product> productMap, List<ObjectId> productCategoryIds, boolean isPurchaseOrder) {
        HashMap<String, ProductCategory> categoryHashMap = productCategoryRepository.listAsMap(shop.getCompanyId(), productCategoryIds);
        return taxCalulationService.calculateOzTax(shop.getCompanyId(), shop.getId(), purchaseOrder, productMap, categoryHashMap, isPurchaseOrder);
    }

    /**
     * This method is used to generated pdf for purchase order
     *
     * @param purchaseOrderId
     * @return
     */
    @Override
    public InputStream createPdfForPurchaseOrder(String purchaseOrderId) {
        if (StringUtils.isBlank(purchaseOrderId)) {
            throw new BlazeInvalidArgException(PURCHASE_ORDER, PO_NOT_FOUND);
        }
        final PurchaseOrder purchaseOrder = purchaseOrderRepository.get(token.getCompanyId(), purchaseOrderId);
        if (purchaseOrder == null) {
            throw new BlazeInvalidArgException(PURCHASE_ORDER, PO_NOT_FOUND);
        }

        String body = getPurchaseOrderPdfHtml(purchaseOrder);
        return new ByteArrayInputStream(PdfGenerator.exportToPdfBox(body, PO_HTML_RESOURCE));
    }

    /**
     * This is private method to get html body for pdf
     *
     * @param purchaseOrder
     * @return
     */
    private String getPurchaseOrderPdfHtml(PurchaseOrder purchaseOrder) {
        try {
            Company company = companyRepository.getById(purchaseOrder.getCompanyId());
            Vendor vendor = vendorRepository.getById(purchaseOrder.getVendorId());
            Shop shop = shopRepository.getById(purchaseOrder.getShopId());
            CompanyLicense companyLicense = null;
            if (vendor != null) {
                companyLicense = vendor.getCompanyLicense(purchaseOrder.getLicenseId());
            }

            BigDecimal amountPaid = purchaseOrder.getAmountPaid();
            BigDecimal totalAmount = purchaseOrder.getGrandTotal();

            amountPaid = (amountPaid == null) ? new BigDecimal(0) : amountPaid;
            totalAmount = (totalAmount == null) ? new BigDecimal(0) : totalAmount;

            BigDecimal discount = new BigDecimal(0);
            BigDecimal totalDiscount = new BigDecimal(0);
            BigDecimal fees = new BigDecimal(0);
            BigDecimal balanceDue = new BigDecimal(0);
            if (totalAmount.doubleValue() > amountPaid.doubleValue()) {
                balanceDue = totalAmount.subtract(amountPaid);
            }

            BigDecimal subTotal = purchaseOrder.getTotalCost();
            subTotal = (subTotal == null) ? new BigDecimal(0) : subTotal;

            discount = (discount == null) ? discount : purchaseOrder.getDiscount();
            fees = (fees == null) ? fees : purchaseOrder.getFees();

            totalDiscount = (purchaseOrder.getTotalDiscount() == null) ? getTotalDiscount(purchaseOrder) : purchaseOrder.getTotalDiscount();

            BigDecimal changeReturn = amountPaid.doubleValue() > totalAmount.doubleValue() ? amountPaid.subtract(totalAmount) : BigDecimal.ZERO;

            BigDecimal deliveryCharge = purchaseOrder.getDeliveryCharge();
            deliveryCharge = (deliveryCharge == null) ? new BigDecimal(0) : deliveryCharge;
            InputStream inputStream = PurchaseOrderServiceImpl.class.getResourceAsStream(PO_HTML_RESOURCE);
            StringWriter writer = new StringWriter();
            try {
                IOUtils.copy(inputStream, writer, "UTF-8");
            } catch (IOException ex) {
                LOGGER.error("Error! in po email", ex);
            }
            String paymentStatus = purchaseOrder.getPurchaseOrderStatus().toString();
            if (paymentStatus.equalsIgnoreCase(PurchaseOrder.PurchaseOrderStatus.InProgress.toString())) {
                paymentStatus = "In Progress";
            }
            if (paymentStatus.equalsIgnoreCase(PurchaseOrder.PurchaseOrderStatus.ReceivingShipment.toString())) {
                paymentStatus = "Receiving Shipment";
            }
            if (paymentStatus.equalsIgnoreCase(PurchaseOrder.PurchaseOrderStatus.RequiredApproval.toString())) {
                paymentStatus = "Required Approval";
            }
            if (paymentStatus.equalsIgnoreCase(PurchaseOrder.PurchaseOrderStatus.WaitingShipment.toString())) {
                paymentStatus = "Waiting Shipment";
            }
            if (paymentStatus.equalsIgnoreCase(PurchaseOrder.PurchaseOrderStatus.ReceivedShipment.toString())) {
                paymentStatus = "Received Shipment";
            }
            if (purchaseOrder.isArchive()) {
                paymentStatus = "Archived";
            }
            String body = writer.toString();
            body = body.replaceAll("==blankSpace==", TextUtil.textOrEmpty("  "));
            body = body.replaceAll("==paymentStatus==", paymentStatus);
            body = body.replaceAll("==companyName==", shop.getName());
            if (shop.getAddress() != null) {
                Address address = shop.getAddress();
                body = body.replaceAll("==address==", TextUtil.textOrEmpty(address.getAddress()));
                body = body.replaceAll("==city==", TextUtil.textOrEmpty(address.getCity()));
                body = body.replaceAll("==state==", TextUtil.textOrEmpty(address.getState()));
                body = body.replaceAll("==zipCode==", TextUtil.textOrEmpty(address.getZipCode()));
                body = body.replaceAll("==country==", TextUtil.textOrEmpty(address.getCountry()));
                body = body.replaceAll("==showCompanyAddress==", TextUtil.textOrEmpty(""));
            } else {
                body = body.replaceAll("==showCompanyAddress==", TextUtil.textOrEmpty("display:none;"));
            }

            if (shop != null && StringUtils.isNotBlank(shop.getLicense())) {
                body = body.replaceAll("==licenseNumber==", shop.getLicense());
            } else {
                body = body.replaceAll("==licenseNumber==", TextUtil.textOrEmpty(""));
            }

            boolean showExciseTax = Boolean.FALSE;

            BigDecimal totalTax = purchaseOrder.getTotalTax();
            BigDecimal ozTax = new BigDecimal(0);

            if (purchaseOrder.getTaxResult() != null && purchaseOrder.getTaxResult().getCultivationTaxResult() != null) {
                ozTax = purchaseOrder.getTaxResult().getCultivationTaxResult().getTotalCultivationTax();
            }

            body = body.replaceAll("==showTotalTax==", TextUtil.textOrEmpty((totalTax != null && totalTax.doubleValue() <= 0) ? "display:none" : ""));
            body = body.replaceAll("==totalTax==", TextUtil.formatToTwoDecimalPoints(totalTax.doubleValue()));

            body = body.replaceAll("==showOZTax==", TextUtil.textOrEmpty((ozTax != null && ozTax.doubleValue() <= 0) ? "display:none" : ""));
            body = body.replaceAll("==ozTax==", TextUtil.formatToTwoDecimalPoints(ozTax.doubleValue()));


            if (purchaseOrder.getPoProductRequestList() != null) {
                StringBuilder section = new StringBuilder();
                List<ObjectId> productIds = new ArrayList<>();
                for (POProductRequest poProductRequest : purchaseOrder.getPoProductRequestList()) {
                    if (StringUtils.isNotBlank(poProductRequest.getProductId()) && ObjectId.isValid(poProductRequest.getProductId()))
                        productIds.add(new ObjectId(poProductRequest.getProductId()));
                }
                if (!productIds.isEmpty()) {
                    HashMap<String, Product> productHashMap = productRepository.findProductsByIdsAsMap(token.getCompanyId(), token.getShopId(), productIds);
                    int index = 0;
                    for (POProductRequest poProductRequest : purchaseOrder.getPoProductRequestList()) {
                        Product product = productHashMap.get(poProductRequest.getProductId());
                        if (product == null) {
                            continue;
                        }
                        section.append(getProductInformationSectionForPdf(++index, product, poProductRequest));

                    }
                    if (purchaseOrder.getPoProductRequestList().size() > 2 && purchaseOrder.getPoProductRequestList().size() <= 4) {
                        if ((totalTax != null && totalTax.doubleValue() > 0) && (ozTax.doubleValue() > 0)) {
                            body = body.replaceAll("==pageBreak==", "page-break-before: always");
                        }
                    }
                    String sectionData = section.toString();
                    sectionData = sectionData.replaceAll("&", "&amp;");
                    body = body.replaceAll("==productInformation==", Matcher.quoteReplacement(sectionData));
                }
            }

            if (vendor != null) {
                String vendorName = vendor.getName();
                if (StringUtils.isNotBlank(vendor.getDbaName())) {
                    vendorName += "(" + vendor.getDbaName() + ")";
                }

                body = body.replaceAll("==billTo==", vendorName);

            } else {
                body = body.replaceAll("==billTo==", TextUtil.textOrEmpty(""));
            }

            StringBuilder billToLicenseNo = new StringBuilder();
            if (companyLicense != null) {
                billToLicenseNo.append((StringUtils.isNotBlank(companyLicense.getLicenseNumber()) ? companyLicense.getLicenseNumber() : "N/A"))
                        .append(" (").append(companyLicense.getCompanyType()).append(")");
            }

            body = body.replaceAll("==showBillTo==", TextUtil.textOrEmpty((vendor != null && vendor.getAddress() != null) ? "" : "display:none;"));
            body = body.replaceAll("==billToLicenseNumber==", Matcher.quoteReplacement(billToLicenseNo.toString()));

            if (vendor != null && vendor.getAddress() != null) {
                Address address = vendor.getAddress();
                body = body.replaceAll("==billToAddress==", TextUtil.textOrEmpty(address.getAddress()));
                body = body.replaceAll("==billToCity==", TextUtil.textOrEmpty(address.getCity()));
                body = body.replaceAll("==billToState==", TextUtil.textOrEmpty(address.getState()));
                body = body.replaceAll("==billToZipCode==", TextUtil.textOrEmpty(address.getZipCode()));
                body = body.replaceAll("==billToCountry==", TextUtil.textOrEmpty(address.getCountry()));
            } else {
                body = body.replaceAll("==billToAddress==", TextUtil.textOrEmpty(""));
                body = body.replaceAll("==billToCity==", TextUtil.textOrEmpty(""));
                body = body.replaceAll("==billToState==", TextUtil.textOrEmpty(""));
                body = body.replaceAll("==billToZipCode==", TextUtil.textOrEmpty(""));
                body = body.replaceAll("==billToCountry==", TextUtil.textOrEmpty(""));
            }

            String timeZone = token.getRequestTimeZone();
            if (StringUtils.isBlank(timeZone)) {
                timeZone = shop.getTimeZone();
            }
            if (purchaseOrder.getDeliveryDate() > 0) {
                body = body.replaceAll("==deliveryDate==", TextUtil.toDate(DateUtil.toDateTime(purchaseOrder.getDeliveryDate(), timeZone)));
            } else {
                body = body.replaceAll("==deliveryDate==", "");
            }
            if (purchaseOrder.getDeliveryTime() != null && purchaseOrder.getDeliveryTime() > 0) {
                body = body.replaceAll("==deliveryTime==", TextUtil.toTime(DateUtil.toDateTime(purchaseOrder.getDeliveryTime(), timeZone)));
            } else {
                body = body.replaceAll("==deliveryTime==", "");
            }
            body = body.replaceAll("==poDate==", TextUtil.toDate(DateUtil.toDateTime(purchaseOrder.getPurchaseOrderDate(), timeZone)));
            body = body.replaceAll("==term==", purchaseOrder.getPoPaymentTerms().toString());
            body = body.replaceAll("==poNumber==", purchaseOrder.getPoNumber());
            body = body.replaceAll("==dueDate==", TextUtil.toDate(DateUtil.toDateTime((purchaseOrder.getDueDate() == 0 ? purchaseOrder.getPODueDate() : purchaseOrder.getDueDate()), timeZone)));

            body = body.replaceAll("==subTotal==", TextUtil.formatToTwoDecimalPoints(subTotal));
            body = body.replaceAll("==discount==", TextUtil.formatToTwoDecimalPoints(discount));
            body = body.replaceAll("==fees==", TextUtil.formatToTwoDecimalPoints(fees));
            body = body.replaceAll("==totalDiscount==", TextUtil.formatToTwoDecimalPoints(totalDiscount));

            body = body.replaceAll("==deliveryCharge==", TextUtil.formatToTwoDecimalPoints(deliveryCharge));
            body = body.replaceAll("==paymentMade==", TextUtil.formatToTwoDecimalPoints(amountPaid));
            body = body.replaceAll("==total==", TextUtil.formatToTwoDecimalPoints(totalAmount));
            body = body.replaceAll("==balanceDue==", TextUtil.formatToTwoDecimalPoints(balanceDue));

            body = body.replaceAll("==showChangeReturn==", TextUtil.textOrEmpty((changeReturn.doubleValue() <= 0) ? "display:none" : ""));
            body = body.replaceAll("==changeReturn==", TextUtil.formatToTwoDecimalPoints(changeReturn));

            body = body.replaceAll("==notes==", Matcher.quoteReplacement(purchaseOrder.getNotes() == null ? "" : purchaseOrder.getNotes().replaceAll("&", "&amp;")));

            body = body.replaceAll("==termsConditions==",  Matcher.quoteReplacement(StringUtils.isBlank(purchaseOrder.getTermsAndCondition()) ? " " : purchaseOrder.getTermsAndCondition().replaceAll("&", "&amp;")));

            String logoURL = (shop.getLogo() != null && org.apache.commons.lang3.StringUtils.isNotBlank(shop.getLogo().getLargeURL())) ? shop.getLogo().getLargeURL() : "https://connect-assets.s3.amazonaws.com/email/logo.jpg";
            body = body.replaceAll("==logo==", logoURL);
            /*if (purchaseOrder.getPoQrCodeAsset () != null)
            body = body.replaceAll ( "==QRCodeImage==",(purchaseOrder.getPoQrCodeAsset ().getPublicURL () == null)?"":purchaseOrder.getPoQrCodeAsset ().getPublicURL ());*/
            body = body.replaceAll("==QRCodeImage==", (purchaseOrder.getPoQrCodeUrl() == null) ? "" : purchaseOrder.getPoQrCodeUrl());

            if (purchaseOrder.getAdjustmentInfoList() == null || purchaseOrder.getAdjustmentInfoList().size() == 0) {
                body = body.replaceAll("==showAdjustments==" , "display:none");
            } else {

                if ((totalTax != null && totalTax.doubleValue() > 0) && (ozTax != null && ozTax.doubleValue() > 0)) {
                    body = body.replaceAll("==pageBreak==", "page-break-before: always");
                }

                List<ObjectId> adjustmentIds = new ArrayList<>();
                for (AdjustmentInfo info : purchaseOrder.getAdjustmentInfoList()) {
                    adjustmentIds.add(new ObjectId(info.getAdjustmentId()));
                }

                HashMap<String, Adjustment> adjustmentMap = adjustmentRepository.listAsMap(token.getCompanyId(), adjustmentIds);

                StringBuilder adjustmentData = new StringBuilder();

                for (AdjustmentInfo info : purchaseOrder.getAdjustmentInfoList()) {
                    Adjustment adjustment = adjustmentMap.get(info.getAdjustmentId());
                    if (adjustment == null) {
                        continue;
                    }
                    adjustmentData.append(prepareAdjustmentInfo(adjustment, info.getAmount().doubleValue(), info.isNegative()));
                }

                String adjustments = adjustmentData.toString();
                adjustments = adjustments.replaceAll("&", "&amp;");
                body = body.replaceAll("==adjustmentList==", Matcher.quoteReplacement(adjustments));
                body = body.replaceAll("==showAdjustments==" , " ");
            }

            return body;
        } catch (Exception ex) {
            LOGGER.error(PURCHASE_ORDER, ex.getCause());
            return "Information are not completed";
        }

    }

    private String prepareAdjustmentInfo(Adjustment adjustment, double amount, boolean negative) {
        StringBuilder adjustmentData = new StringBuilder();
        adjustmentData.append("<div style=\"width:100%;float:left;background-color: rgb(238, 238, 238)\">\n");
        adjustmentData.append("<div style=\"width:50%;float:left;color: #5a5a5a;text-align: right;padding:8px;font-size:12px;font-family:'Open Sans',Helvetica,Arial,sans-serif!important;\">" + adjustment.getName() + "</div>");

        if (negative) {
            adjustmentData.append("<div style=\"width:35%;float:right;text-align: right;color: red;padding:8px;font-size:12px;font-family:'Open Sans',Helvetica,Arial,sans-serif!important;\"> -$" + TextUtil.formatToTwoDecimalPoints(amount) + "</div>");
        } else {
            adjustmentData.append("<div style=\"width:35%;float:right;text-align: right;color: #5a5a5a;padding:8px;font-size:12px;font-family:'Open Sans',Helvetica,Arial,sans-serif!important;\"> $" + TextUtil.formatToTwoDecimalPoints(amount) + "</div>");
        }
        adjustmentData.append("</div>");

        return adjustmentData.toString();
    }

    /**
     * This private method is used to get product information for email under these parameters.
     *
     * @param index
     * @param product
     * @param productRequest
     * @return
     */
    private String getProductInformationSectionForPdf(final int index, final Product product, final POProductRequest productRequest) {
        BigDecimal finalTotalCost = productRequest.getFinalTotalCost();
        BigDecimal discount = productRequest.getDiscount() == null ? BigDecimal.ZERO : productRequest.getDiscount();
        if (finalTotalCost == null || finalTotalCost.doubleValue() == 0) {
            finalTotalCost =  productRequest.getTotalCost().subtract(discount);
        }

        BigDecimal amount = finalTotalCost;
        amount = amount.add(productRequest.getTotalCultivationTax());

        return "<tr>\n" +
                "<td style=\"width:10%;font-family:'Open Sans',Helvetica,Arial,sans-serif!important;text-align:center;vertical-align:middle;padding: 10px 5px 10px 5px;line-height:1.42857;border-top:1px solid #e7ecf1;font-size:12px;color:#5a5a5a;\">" + index + "</td>\n" +
                "<td style=\"width:60%;font-family:'Open Sans',Helvetica,Arial,sans-serif!important;text-align:left;vertical-align:middle;padding: 10px 5px 10px 5px;line-height:1.42857;border-top:1px solid #e7ecf1;font-size:12px;color:#5a5a5a;\">" + product.getName() + "</td>\n" +
                "<td style=\"width:10%;font-family:'Open Sans',Helvetica,Arial,sans-serif!important;text-align:center;vertical-align:middle;padding: 10px 5px 10px 5px;line-height:1.42857;border-top:1px solid #e7ecf1;font-size:12px;color:#5a5a5a;\">" + TextUtil.formatToTwoDecimalPoints(productRequest.getRequestQuantity()) + "</td>\n" +
                "<td style=\"width:10%;font-family:'Open Sans',Helvetica,Arial,sans-serif!important;text-align:center;vertical-align:middle;padding: 10px 5px 10px 5px;line-height:1.42857;border-top:1px solid #e7ecf1;font-size:12px;color:#5a5a5a;\">" + TextUtil.formatToTwoDecimalPoints(productRequest.getUnitPrice()) + "</td>\n" +
                "<td style=\"width:10%;font-family:'Open Sans',Helvetica,Arial,sans-serif!important;text-align:center;vertical-align:middle;padding: 10px 5px 10px 5px;line-height:1.42857;border-top:1px solid #e7ecf1;font-size:12px;color:#5a5a5a;\">" + TextUtil.formatToTwoDecimalPoints(productRequest.getTotalCultivationTax()) + "</td>\n" +
                "<td style=\"width:10%;font-family:'Open Sans',Helvetica,Arial,sans-serif!important;text-align:center;vertical-align:middle;padding: 10px 5px 10px 5px;line-height:1.42857;border-top:1px solid #e7ecf1;font-size:12px;color:#5a5a5a;\">" + TextUtil.formatToTwoDecimalPoints(productRequest.getDiscount() == null ? BigDecimal.ZERO : productRequest.getDiscount()) + "</td>\n" +
                "<td style=\"width:10%;font-family:'Open Sans',Helvetica,Arial,sans-serif!important;text-align:right;vertical-align:middle;padding: 10px 5px 10px 5px;line-height:1.42857;border-top:1px solid #e7ecf1;font-size:12px;color:#5a5a5a;\">" + TextUtil.formatToTwoDecimalPoints(amount) + "</td>\n" +
                "</tr>";
    }

    /**
     * Override method to get all purchase order by dates
     *
     * @param startDate
     * @param endDate
     * @return
     */
    @Override
    public DateSearchResult<PurchaseOrderResult> getAllPOByDates(long startDate, long endDate) {
        if (endDate <= 0) endDate = DateTime.now().getMillis();
        List<PurchaseOrder.PurchaseOrderStatus> status = new ArrayList<>();
        status.add(PurchaseOrder.PurchaseOrderStatus.ReceivingShipment);
        status.add(PurchaseOrder.PurchaseOrderStatus.Closed);

        DateSearchResult<PurchaseOrderResult> searchResult = purchaseOrderRepository.findItemsWithDate(token.getCompanyId(), token.getShopId(), startDate, endDate, PurchaseOrderResult.class, "{poNumber: 1, purchaseOrderStatus: 1, poProductRequestList: 1, parentPONumber: 1, parentPOId: 1,poType:1,poPaymentTerms:1,poPaymentOptions:1,vendorId:1,totalCost:1,amountPaid:1,grandTotal:1,receivedDate:1,}", status);

        if (searchResult != null && searchResult.getValues() != null && !searchResult.getValues().isEmpty()) {
            LinkedHashSet<ObjectId> employeeIds = new LinkedHashSet<>();
            LinkedHashSet<ObjectId> vendorIds = new LinkedHashSet<>();
            LinkedHashSet<ObjectId> productsIds = new LinkedHashSet<>();
            LinkedHashSet<ObjectId> customerCompanyIds = new LinkedHashSet<>();
            LinkedHashSet<String> purchaseOrderIds = new LinkedHashSet<>();


            for (PurchaseOrderResult result : searchResult.getValues()) {
                this.getInfoForPurchaseOrder(result, employeeIds, vendorIds, productsIds, customerCompanyIds);
                purchaseOrderIds.add(result.getId());
            }

            HashMap<String, ShipmentBill> shipmentBillByPOMap = shipmentBillService.getShipmentBillByPoAsMap(Lists.newArrayList(purchaseOrderIds));

            for (PurchaseOrderResult result : searchResult.getValues()) {
                // preparePurchaseOrderResult(result, employeeMap, vendorMap, productMap);
                ShipmentBill shipmentBill = shipmentBillByPOMap.get(result.getId());
                result.setShipmentBill(shipmentBill);
                result.setReceived(PurchaseOrder.PurchaseOrderStatus.Closed.equals(result.getPurchaseOrderStatus()));

                boolean isMetrc = false;
                LinkedHashSet<String> metrcId = new LinkedHashSet<>();

                List<POProductRequest> productRequestList = result.getPoProductRequestList();
                if (productRequestList != null && !productRequestList.isEmpty()) {
                    for (POProductRequest poProductRequest : productRequestList) {
                        final List<TrackingPackages> trackingPackagesList = poProductRequest.getTrackingPackagesList();
                        if (!trackingPackagesList.isEmpty()) {
                            isMetrc = true;
                            for (TrackingPackages trackingPackages : trackingPackagesList) {
                                metrcId.add(trackingPackages.getPackageLabel());
                            }
                        }


                    }
                }
                result.setMetrc(isMetrc);
                result.setMetrcId(metrcId);
            }
        }
        return searchResult;
    }

    @Override
    public List<BulkPostResponse> compositePOUpdate(List<PurchaseOrder> request) {
        List<BulkPostResponse> poErrorList = new ArrayList<>();
        for (PurchaseOrder purchaseOrder : request) {
            try {
                if (StringUtils.isBlank(purchaseOrder.getId())) {
                    throw new BlazeInvalidArgException(PURCHASE_ORDER, "Purchase Order Id cannot be blank.");
                }
                if (purchaseOrder.getPurchaseOrderStatus() == null) {
                    throw new BlazeInvalidArgException(PURCHASE_ORDER, "Purchase Order Status not found.");
                }
                if (purchaseOrder.getPoProductRequestList() == null && !purchaseOrder.getPoProductRequestList().isEmpty()) {
                    throw new BlazeInvalidArgException(PURCHASE_ORDER, "Product request list cannot be empty");
                }
                if (PurchaseOrder.PurchaseOrderStatus.ReceivingShipment == purchaseOrder.getPurchaseOrderStatus() || PurchaseOrder.PurchaseOrderStatus.Closed == purchaseOrder.getPurchaseOrderStatus()) {
                    POCompleteRequest poCompleteRequest = new POCompleteRequest();

                    poCompleteRequest.setAsset(purchaseOrder.getCompanyAssetList());
                    poCompleteRequest.setCreatedBy(purchaseOrder.getCreatedByEmployeeId());
                    poCompleteRequest.setCustomerType(purchaseOrder.getCustomerType());
                    poCompleteRequest.setDeliveredBy(purchaseOrder.getDeliveredBy());
                    poCompleteRequest.setManagerReceiveSignature(purchaseOrder.getManagerReceiveSignature());
                    poCompleteRequest.setNotes(purchaseOrder.getNotes());

                    List<POProductRequest> poRequestList = purchaseOrder.getPoProductRequestList();
                    for (POProductRequest poProductRequest : poRequestList) {
                        if (poProductRequest.getRequestStatus() == null || POProductRequest.RequestStatus.PENDING.equals(poProductRequest.getRequestStatus())) {
                            poProductRequest.setRequestStatus(POProductRequest.RequestStatus.ACCEPTED);
                        }

                        if (StringUtils.isBlank(poProductRequest.getId())) {
                            throw new BlazeInvalidArgException(PURCHASE_ORDER, "Product request id cannot be blank in request list.");
                        }
                        if (StringUtils.isBlank(poProductRequest.getProductId())) {
                            throw new BlazeInvalidArgException(PURCHASE_ORDER, "Product cannot be blank in request list.");
                        }
                        if (POProductRequest.RequestStatus.ACCEPTED.equals(poProductRequest.getRequestStatus()) &&
                                (poProductRequest.getReceivedQuantity() == null || poProductRequest.getReceivedQuantity().doubleValue() <= 0)) {
                            throw new BlazeInvalidArgException(PURCHASE_ORDER, "Received quantity does not exist.");
                        }

                    }
                    poCompleteRequest.setPoProductRequestList(purchaseOrder.getPoProductRequestList());
                    poCompleteRequest.setReceivedByEmployeeId(purchaseOrder.getReceivedByEmployeeId());
                    poCompleteRequest.setReceivedDate((purchaseOrder.getReceivedDate() == null) ? 0 : purchaseOrder.getReceivedDate());
                    poCompleteRequest.setVendorId(purchaseOrder.getVendorId());
                    completeShipmentArrival(purchaseOrder.getId(), poCompleteRequest);
                }

            } catch (Exception e) {
                BulkPostResponse response = new BulkPostResponse(purchaseOrder, e.getMessage());
                poErrorList.add(response);
            }
        }
        return poErrorList;
    }

    public TaxResult calculateCultivationTax(TaxResult taxResult, List<POProductRequest> shipmentProductList, PurchaseOrder purchaseOrder, HashMap<String, Product> productMap, List<ObjectId> productCategoryId) {
        if (taxResult == null) {
            taxResult = new TaxResult();
            taxResult.prepare();
        }
        Vendor vendor = vendorRepository.get(token.getCompanyId(), purchaseOrder.getVendorId());
        Shop shop = shopRepository.get(token.getCompanyId(), purchaseOrder.getShopId());
        CompanyLicense companyLicense = vendor.getCompanyLicense(purchaseOrder.getLicenseId());

        if ((Vendor.CompanyType.CULTIVATOR.equals(companyLicense.getCompanyType()) || Vendor.CompanyType.MANUFACTURER.equals(companyLicense.getCompanyType())) || PurchaseOrder.FlowerSourceType.CULTIVATOR_DIRECT.equals(purchaseOrder.getFlowerSourceType())) {
            List<POProductRequest> poProductRequests = purchaseOrder.getPoProductRequestList();
            //Assign shipment bill product to calculate oz tax
            purchaseOrder.setPoProductRequestList(shipmentProductList);
            CultivationTaxResult cultivationTaxResult = calculateOzTax(shop, purchaseOrder, productMap, productCategoryId, false);

            //re assign old po products
            purchaseOrder.setPoProductRequestList(poProductRequests);

            taxResult.setCultivationTaxResult(cultivationTaxResult == null ? new CultivationTaxResult() : cultivationTaxResult);

        }
        return taxResult;
    }

    private POActivity addActivityForPODecline(String comment, String targetId) {
        ActivityCommentRequest request = new ActivityCommentRequest();
        request.setComment(comment);
        request.setTargetId(targetId);
        return poActivityService.addActivityComment(request);
    }


    @Override
    public void returnToVendorFromPOs(VendorReturnRequest request) {

        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());
        if (shop == null) {
            throw new BlazeInvalidArgException("Shop", "Shop does not exist.");
        }

        ProductBatch productBatch = productBatchRepository.getByShopAndId(token.getCompanyId(), token.getShopId(), request.getBatchId());
        if (productBatch == null) {
            throw new BlazeInvalidArgException("ProductBatch", "ProductBatch does not exist.");
        }

        Product product = productRepository.get(token.getCompanyId(), productBatch.getProductId());
        if (product == null) {
            throw new BlazeInvalidArgException("Product", "Product does not exist.");
        }

        if (!product.getVendorId().equalsIgnoreCase(request.getVendorId())) {
            throw new BlazeInvalidArgException("Vendor", "Invalid Vendor");
        }

        Vendor vendor = vendorRepository.get(token.getCompanyId(), request.getVendorId());
        if (vendor == null) {
            throw new BlazeInvalidArgException("Vendor", "Vendor does not exist.");
        }

        Terminal terminal = terminalRepository.get(token.getCompanyId(), token.getTerminalId());
        if (terminal == null) {
            throw new BlazeInvalidArgException("Terminal", "Terminal does not exist.");
        }

        Inventory safeInventory = inventoryRepository.getInventory(token.getCompanyId(), token.getShopId(), Inventory.SAFE);

        if (safeInventory == null) {
            throw new BlazeInvalidArgException(INVENTORY, SAFE_NOT_FOUND);
        }

        BatchQuantity batchQuantityForInventory = batchQuantityRepository.getBatchQuantityForInventory(token.getCompanyId(), token.getShopId(), productBatch.getProductId(), safeInventory.getId(), request.getBatchId());
        if (batchQuantityForInventory == null) {
            throw new BlazeInvalidArgException(INVENTORY, NOT_ENOUGH_QUANTITY_FOR_RETURN);
        }

        if (batchQuantityForInventory.getQuantity().doubleValue() < request.getQuantity().doubleValue()) {
            throw new BlazeInvalidArgException(INVENTORY, NOT_ENOUGH_QUANTITY_FOR_RETURN);
        }

        UniqueSequence sequence = uniqueSequenceRepository.getNewIdentifier(token.getCompanyId(), token.getShopId(), "TransactionPriority");

        Transaction trans = new Transaction();
        trans.setActive(false);
        trans.setTransNo("" + sequence.getCount());
        trans.prepare(token.getCompanyId());
        trans.setShopId(token.getShopId());
        trans.setQueueType(Transaction.QueueType.None);
        trans.setStatus(Transaction.TransactionStatus.Void);
        trans.setTerminalId(token.getTerminalId());
        trans.setMemberId(null);
        trans.setCheckinTime(DateTime.now().getMillis());
        trans.setSellerId(token.getActiveTopUser().getUserId());
        trans.setPriority(sequence.getCount());
        trans.setStartTime(DateTime.now().getMillis());
        trans.setEndTime(DateTime.now().getMillis());
        trans.setTransType(Transaction.TransactionType.ReturnToVendor);
        trans.setOverrideInventoryId(safeInventory.getId());

        Note note = new Note();
        note.prepare();
        note.setWriterId(token.getActiveTopUser().getUserId());
        note.setWriterName(token.getActiveTopUser().getFirstName() + " " + token.getActiveTopUser().getLastName());
        note.setMessage(request.getNotes());
        trans.setNote(note);

        Cart cart = new Cart();
        cart.setId(ObjectId.get().toString());
        cart.setCompanyId(token.getCompanyId());
        cart.setPaymentOption(Cart.PaymentOption.None);

        OrderItem orderItem = new OrderItem();
        orderItem.prepare(token.getCompanyId());
        orderItem.setStatus(OrderItem.OrderItemStatus.Active);
        orderItem.setCost(new BigDecimal(0));

        orderItem.setQuantity(request.getQuantity());
        orderItem.setFinalPrice(new BigDecimal(0));
        orderItem.setProductId(productBatch.getProductId());
        orderItem.setBatchId(productBatch.getId());

        cart.getItems().add(orderItem);

        trans.setCart(cart);

        transactionRepository.save(trans);

        queuePOSServiceImpl.queueTransactionJob(trans.getId(), trans, trans.getStatus(), shop);
    }

    @Override
    public SearchResult<PurchaseOrder> getPurchaseOrdersByProductId(int start, int limit, String sortOptions, String productId) {
        if (limit == 0) {
            limit = Integer.MAX_VALUE;
        }
        SearchResult<PurchaseOrder> searchResult;
        if (productId == null && productId.isEmpty()) {
            throw new BlazeInvalidArgException(PURCHASE_ORDER, PRODUCT_NOT_FOUND);
        }
        searchResult = purchaseOrderRepository.getPurchaseOrdersByProductId(token.getCompanyId(), token.getShopId(), productId, start, limit, sortOptions);
        return searchResult;


    }

    @Override
    public PurchaseOrderItemResult getPurchaseOrderByNo(String poNumber) {

        PurchaseOrderItemResult purchaseOrder = purchaseOrderRepository.getPurchaseOrderByNo(token.getCompanyId(), token.getShopId(), poNumber, PurchaseOrderItemResult.class);

        if (purchaseOrder == null) {
            throw new BlazeInvalidArgException(PURCHASE_ORDER, PO_NOT_FOUND);
        }

        LinkedHashSet<ObjectId> employeeIds = new LinkedHashSet<>();
        LinkedHashSet<ObjectId> vendorIds = new LinkedHashSet<>();
        LinkedHashSet<ObjectId> productsIds = new LinkedHashSet<>();
        LinkedHashSet<ObjectId> customerCompanyIds = new LinkedHashSet<>();

        this.getInfoForPurchaseOrder(purchaseOrder, employeeIds, vendorIds, productsIds, customerCompanyIds);

        HashMap<String, Employee> employeeMap = employeeRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(employeeIds));
        HashMap<String, Vendor> vendorMap = vendorRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(vendorIds));
        HashMap<String, Product> productMap = productRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(productsIds));

        preparePurchaseOrderResult(purchaseOrder, employeeMap, vendorMap, productMap);

        return purchaseOrder;

    }

    /**
     * Private method to prepare test sample add request
     *
     * @param purchaseOrder     : purchase order
     * @param productBatch      : product batch
     * @param testId            : testId
     * @return : return test sample add request
     */
    private TestSampleAddRequest prepareTestResult(PurchaseOrder purchaseOrder, ProductBatch productBatch, String testId, String testingCompanyId, Long dateSent, ProductBatch.BatchStatus batchStatus) {
        TestSampleAddRequest testSampleAddRequest = new TestSampleAddRequest();
        testSampleAddRequest.setBatchId(productBatch.getId());
        testSampleAddRequest.setReferenceNo(StringUtils.isBlank(testId) ? "PO #" + purchaseOrder.getPoNumber() : testId);
        testSampleAddRequest.setSample(BigDecimal.ZERO);
        testSampleAddRequest.setDateTested((dateSent == null || dateSent == 0) ? DateTime.now().getMillis() : dateSent);
        testSampleAddRequest.setDateSent((dateSent == null || dateSent == 0) ? DateTime.now().getMillis() : dateSent);
        testSampleAddRequest.setStatus((batchStatus == ProductBatch.BatchStatus.READY_FOR_SALE) ? TestSample.SampleStatus.PASSED : TestSample.SampleStatus.IN_TESTING);
        testSampleAddRequest.setTestId(testId);
        testSampleAddRequest.setTestingCompanyId(testingCompanyId);

        return testSampleAddRequest;
    }

    /**
     * Private method to validate batch sku for all products if already available
     *
     * @param poProductRequests       : poProductRequests
     */
    private void validateBatchSkuForAll(List<POProductRequest> poProductRequests) {
        if (poProductRequests == null || poProductRequests.isEmpty()) {
            return;
        }

        List<ObjectId> productIds = new ArrayList<>();
        for (POProductRequest poProductRequest : poProductRequests) {
            productIds.add(new ObjectId(poProductRequest.getProductId()));
        }

        HashMap<String, Product> productHashMap = productRepository.listAsMap(token.getCompanyId(), productIds);

        for (POProductRequest poProductRequest : poProductRequests) {
            Product product = productHashMap.get(poProductRequest.getProductId());
            if (product == null) {
                continue;
            }

            validateBatchSku(product, poProductRequest);
        }

    }

    /**
     * Private method to validate batch sku if already available
     *
     * @param product            : product
     * @param poProductRequest   : po product request
     */
    private void validateBatchSku(Product product, POProductRequest poProductRequest) {
        List<String> skus = new ArrayList<>();

        if (poProductRequest.getBatchAddDetails() == null || poProductRequest.getBatchAddDetails().size() == 0) {
            return;
        }

        double batchQuantities = 0;
        for (POBatchAddRequest batchAddRequest : poProductRequest.getBatchAddDetails()) {
            if (StringUtils.isNotBlank(batchAddRequest.getBatchSku())) {
                if (skus.contains(batchAddRequest.getBatchSku().toUpperCase())) {
                    throw new BlazeInvalidArgException(PURCHASE_ORDER, String.format("Batch Id %s is added for multiple batches for product : %s", batchAddRequest.getBatchSku(), product.getName()));
                }
                skus.add(batchAddRequest.getBatchSku().toUpperCase());

                if(batchAddRequest.getBundleItemsBatches() != null) {
                    for(POBatchAddRequest bundleItemBatch : batchAddRequest.getBundleItemsBatches()) {
                        skus.add(bundleItemBatch.getBatchSku().toUpperCase());
                    }
                }
            }

            if (batchAddRequest.getQuantity() != null) {
                batchQuantities += batchAddRequest.getQuantity().doubleValue();
            }
        }

        poProductRequest.setReceivedQuantity(BigDecimal.valueOf(batchQuantities));

        List<BarcodeItem> barCodes = barcodeService.getAvailableBarCodes(token.getCompanyId(),token.getShopId(), BarcodeItem.BarcodeEntityType.Batch, skus);
        if (!barCodes.isEmpty()) {
            StringBuilder usedSku = new StringBuilder();
            for (BarcodeItem barcodeItem : barCodes) {
                usedSku.append((StringUtils.isNotBlank(usedSku.toString())) ? "," : "").append(barcodeItem.getBarcode());
            }
            throw new BlazeInvalidArgException(PURCHASE_ORDER, String.format("Sku %s not available for product : %s", usedSku, product.getName()));
        }
    }

    /**
     * Private method to prepare productbatch for metrc and without metrc
     *
     * @param purchaseOrder       : purchaseorder
     * @param poProductRequest    : poProductRequest
     * @param batchSku            : batchSku
     * @param quantity            : quantity
     * @param trackingLabel       : tracking label
     * @param dbProduct           : product
     * @param dbCategory          : product category
     * @param trackTraceSystem    : tracktraceSystem
     * @param trackingPackages    : tracking packages
     * @param harvestBatchId      : harvest batch id
     * @param harvestBatchDate    : harvest batch date
     * @param batchExpirationDate : batch expirationDate
     * @param  sellBy             : sellBy
     * @return : return product batch
     */
    private ProductBatch prepareProductBatch(PurchaseOrder purchaseOrder, POProductRequest poProductRequest, String batchSku, BigDecimal quantity, String trackingLabel, Product dbProduct, ProductCategory dbCategory, ProductBatch.TrackTraceSystem trackTraceSystem, TrackingPackages trackingPackages, String harvestBatchId, String harvestBatchDate, long batchExpirationDate, long sellBy, String connectedBatchId) {
        final ProductBatch productBatch = new ProductBatch();

        productBatch.prepare(purchaseOrder.getCompanyId());
        productBatch.setProductId(poProductRequest.getProductId());
        productBatch.setShopId(purchaseOrder.getShopId());
        productBatch.setVendorId(purchaseOrder.getVendorId());
        productBatch.setQuantity(quantity);
        productBatch.setTrackTraceSystem(trackTraceSystem);
        productBatch.setPurchasedDate((purchaseOrder.getReceivedDate() != null && purchaseOrder.getReceivedDate() > 0) ? purchaseOrder.getReceivedDate() : (DateUtil.nowUTC().getMillis()));
        productBatch.setPurchaseOrderId(purchaseOrder.getId());
        productBatch.setSellBy(sellBy);
        productBatch.setPoNumber(purchaseOrder.getPoNumber());
        productBatch.setReceiveDate(purchaseOrder.getReceivedDate());
        productBatch.setTrackHarvestBatch(harvestBatchId);
        productBatch.setTrackHarvestDate(harvestBatchDate);
        productBatch.setExpirationDate(batchExpirationDate);
        productBatch.setTrackPackageLabel(trackingLabel);

        if(connectedBatchId != null) {
            productBatch.setConnectedBatchId(connectedBatchId);
        }

        // productBatch.setTrackTraceSystem(ProductBatch.TrackTraceSystem.MANUAL);
        BarcodeItem item = null;
        productBatch.setBrandId(dbProduct.getBrandId());
        productBatch.setSku((StringUtils.isNotBlank(batchSku) ? batchSku : null));

        /* If track trace system is metrc then integrate batches with compliance metrc batches*/

        if (trackTraceSystem == ProductBatch.TrackTraceSystem.METRC && StringUtils.isNotBlank(trackingLabel)) {
            // Compliance integration
            AssignProductBatchDetailsEvent assignProductBatchDetailsEvent = new AssignProductBatchDetailsEvent();
            assignProductBatchDetailsEvent.setPayload(trackingPackages, productBatch);
            blazeEventBus.post(assignProductBatchDetailsEvent);
            AssignProductBatchDetailsResult assignProductBatchDetailsResult =
                    assignProductBatchDetailsEvent.getResponse();
            BigDecimal productQuantity = assignProductBatchDetailsResult.getQuantity(); // In Grams

            if (dbCategory.getUnitType() == ProductCategory.UnitType.units) {

                // Compute divider
                ProductWeightTolerance.WeightKey weightKey =
                        ModelUtil.getWeightKeyFor(dbProduct.getWeightPerUnit());
                ProductWeightTolerance productWeightTolerance =
                        weightToleranceRepository.getToleranceForWeight(token.getCompanyId(), weightKey);
                BigDecimal quantityDivider = weightKey.weightValue;
                if (productWeightTolerance != null && productWeightTolerance.getUnitValue().doubleValue() > 0) {
                    quantityDivider = productWeightTolerance.getUnitValue();
                }

                // Set Quantity According to metrc
                //productBatch.setQuantity(productQuantity.divide(quantityDivider,4, RoundingMode.HALF_EVEN));//new BigDecimal(productQuantity / quantityDivider));

                if (dbProduct.getWeightPerUnit() == Product.WeightPerUnit.CUSTOM_GRAMS &&
                        dbProduct.getCustomWeight() != null && dbProduct.getCustomWeight().doubleValue() > 0) {

                    if (dbProduct.getCustomGramType() == Product.CustomGramType.GRAM) {
                        quantityDivider = dbProduct.getCustomWeight();
                        //double quantityInGrams = productQuantity * dbProduct.getCustomWeight().doubleValue();
                        //productBatch.setQuantity(new BigDecimal(NumberUtils.round(quantityInGrams, 2)));
                    } else if (Product.CustomGramType.MILLIGRAM == dbProduct.getCustomGramType()) {
                        quantityDivider = dbProduct.getCustomWeight().multiply(new BigDecimal(1000));
                        //double quantityInGrams = productQuantity * (dbProduct.getCustomWeight().doubleValue() / 1000);
                        //productBatch.setQuantity(new BigDecimal(NumberUtils.round(quantityInGrams, 3)));
                    }
                }
                productBatch.setQuantity(productQuantity.divide(quantityDivider,4, RoundingMode.HALF_EVEN));
            }
        }




        // recalculate unit price of the original cost
        double unitPrice = poProductRequest.getUnitPrice().doubleValue();
        BigDecimal dbProductTotalCost = poProductRequest.getTotalCost();
        if (dbProductTotalCost != null && poProductRequest.getRequestQuantity() != null && poProductRequest.getRequestQuantity() != BigDecimal.ZERO) {
            unitPrice = dbProductTotalCost.doubleValue() / poProductRequest.getRequestQuantity().doubleValue();
        }

        BigDecimal productCost = new BigDecimal(productBatch.getQuantity().doubleValue() * unitPrice);
        BigDecimal actualProductCost = productCost;
        BigDecimal actualUnitCost = BigDecimal.valueOf(unitPrice);

        BigDecimal itemDiscount = BigDecimal.ZERO;
        if (poProductRequest.getDiscount() != null && poProductRequest.getDiscount().doubleValue() != 0) {
            itemDiscount = BigDecimal.valueOf(poProductRequest.getDiscount().multiply(productCost).doubleValue() / poProductRequest.getTotalCost().doubleValue());
        }

        double discount = 0;
        double fees = 0;
        if (purchaseOrder.getDiscount().doubleValue() != 0 && purchaseOrder.getTotalCost().doubleValue() != 0) {
            discount = (purchaseOrder.getDiscount().doubleValue() / purchaseOrder.getTotalCost().doubleValue()) * productCost.doubleValue();
        }
        if (purchaseOrder.getFees().doubleValue() != 0 && purchaseOrder.getTotalCost().doubleValue() != 0) {
            fees = (purchaseOrder.getFees().doubleValue() / purchaseOrder.getTotalCost().doubleValue()) * productCost.doubleValue();
        }
        productCost = BigDecimal.valueOf((productCost.doubleValue() - itemDiscount.doubleValue() - discount) + fees);

        productBatch.setCost(productCost);
        if (quantity.doubleValue() > 0) {
            productBatch.setCostPerUnit(productCost.divide(quantity, new MathContext(5)));
            actualUnitCost = actualProductCost.divide(quantity, new MathContext(5));
        }

        if (dbCategory != null) {
            inventoryService.calculateExciseTaxForBatch(productBatch, actualUnitCost, dbProduct.isCannabisProduct(dbCategory != null && dbCategory.isCannabis()));
        }

        BigDecimal cultivationTax = poProductRequest.getTotalCultivationTax();
        double perUnitCultivationTax = 0;
        if (cultivationTax != null && poProductRequest.getRequestQuantity() != null && poProductRequest.getRequestQuantity() != BigDecimal.ZERO) {
            perUnitCultivationTax = cultivationTax.doubleValue() / poProductRequest.getRequestQuantity().doubleValue();
        }

        productBatch.setStatus(poProductRequest.getReceiveBatchStatus());
        productBatch.setTotalCultivationTax(BigDecimal.valueOf(productBatch.getQuantity().doubleValue() * perUnitCultivationTax));

        // Update sku if needed

        try {
            item = barcodeService.createBarcodeItemIfNeeded(dbProduct.getCompanyId(),dbProduct.getShopId(), dbProduct.getId(),
                    BarcodeItem.BarcodeEntityType.Batch,
                    productBatch.getId(), productBatch.getSku(), null, false);
        } catch (Exception ex) {

        }

        if (item != null) {
            productBatch.setSku(item.getBarcode());
            productBatch.setBatchNo(item.getNumber());
        }

        if (StringUtils.isNotBlank(productBatch.getSku())) {
            CompanyAsset companyAsset = new CompanyAsset();
            File file = null;
            InputStream inputStream = QrCodeUtil.getQrCode(productBatch.getSku());
            try {
                file = QrCodeUtil.stream2file(inputStream, ".png");
            } catch (IOException e) {
                e.printStackTrace();
            }
            String keyName = productBatch.getId() + "-" + productBatch.getSku();
            UploadFileResult uploadFileResult = amazonS3Service.uploadFilePublic(file, keyName, ".png");
            companyAsset.setPublicURL(uploadFileResult.getUrl());
            companyAsset.prepare(token.getCompanyId());
            companyAsset.setKey(uploadFileResult.getKey());
            companyAsset.setType(Asset.AssetType.Photo);
            companyAsset.setSecured(false);
            companyAsset.setActive(true);
            productBatch.setBatchQRAsset(companyAsset);
        }

        return productBatch;
    }

    /**
     * Override method to purchase order generate shipping manifest.
     * @param purchaseOrderId : purchase order id
     * @return : return stream
     */
    @Override
    public InputStream generatePoShippingManifest(String purchaseOrderId) {
       if (StringUtils.isBlank(purchaseOrderId)) {
                throw new BlazeInvalidArgException(PURCHASE_ORDER, PO_NOT_FOUND);
            }
            final PurchaseOrder purchaseOrder = purchaseOrderRepository.get(token.getCompanyId(), purchaseOrderId);
            if (purchaseOrder == null) {
                throw new BlazeInvalidArgException(PURCHASE_ORDER, PO_NOT_FOUND);
            }

            String body = getPoShippingManifestForm(purchaseOrder);
            return new ByteArrayInputStream(PdfGenerator.exportToPdfBox(body, PO_SHIPPING_MANIFEST_HTML_RESOURCE));
    }

    private String getPoShippingManifestForm(PurchaseOrder order) {

        String body = StringUtils.EMPTY;
        MathContext precision = new MathContext(3);

        InputStream inputStream = PurchaseOrderServiceImpl.class.getResourceAsStream(PO_SHIPPING_MANIFEST_HTML_RESOURCE);
        StringWriter writer = new StringWriter();
        try {
            IOUtils.copy(inputStream, writer, "UTF-8");
        } catch (IOException ex) {
            LOGGER.error("Error! in po email", ex);
            return body;
        }

        body = writer.toString();

        String timeZone = token.getRequestTimeZone();

        String emptyString = "-";
        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());
        if (shop == null) {
            throw new BlazeInvalidArgException("Shop", "Shop not found");
        }
        Vendor vendor = vendorRepository.getById(order.getVendorId());
        if (vendor == null) {
            throw new BlazeInvalidArgException(PURCHASE_ORDER, VENDOR_NOT_FOUND);
        }

        if (StringUtils.isBlank(timeZone)) {
            timeZone = shop.getTimeZone();
        }

        DateTimeFormatter dateFormatter = DateTimeFormat.forPattern("MM/dd/yy");
        DateTimeFormatter timeFormatter = DateTimeFormat.forPattern("hh:mm");
        DateTimeFormatter timeSuffixFormatter = DateTimeFormat.forPattern("a");

        long shippingDate = DateUtil.toDateTime(order.getDeliveryDate(), timeZone).getMillis();
        long shippingTime = DateUtil.toDateTime(order.getDeliveryTime(), timeZone).getMillis();
        String shippingSuffix = timeSuffixFormatter.print(shippingTime);

        body = body.replaceAll("==poNo==", order.getPoNumber());
        body = body.replaceAll("==totalPageNo==", "2");
        body = body.replaceAll("==pageNo==", "2");

        body = body.replaceAll("==shippingDate==", dateFormatter.print(shippingDate) + " " + timeFormatter.print(shippingTime));
        body = body.replaceAll("==estimatedDate==", TextUtil.textOrEmpty(" "));
        if (shippingSuffix.equals("AM")) {
            body = body.replaceAll("==shippingAM==", "");
            body = body.replaceAll("==shippingPM==", "display:none;");
        } else {
            body = body.replaceAll("==shippingAM==", "display:none;");
            body = body.replaceAll("==shippingPM==", "");
        }

        CompanyLicense shipperLicense = null;
        String shipperAddress1 = "";
        StringBuilder shipperAddress2 = new StringBuilder();
        StringBuilder contactName = new StringBuilder();
        shipperLicense = vendor.getCompanyLicense(order.getLicenseId());

        CompanyContact companyContact = companyContactRepository.getCompanyContactByVendor(token.getCompanyId(), vendor.getImportId());
        if (companyContact != null) {
            contactName.append(StringUtils.isNotBlank(companyContact.getFirstName()) ? companyContact.getFirstName() + " " : "")
                    .append(StringUtils.isNotBlank(companyContact.getLastName()) ? companyContact.getLastName() + " " : "");
        }

        if (vendor.getAddress() != null) {
            shipperAddress1 =  vendor.getAddress().getAddress();
            this.prepareAddress(vendor.getAddress(), shipperAddress2);
        }

        String vendorName = vendor.getName();
        if (StringUtils.isNotBlank(vendor.getDbaName())) {
            vendorName = vendorName + " ("  + vendor.getDbaName() + ")";
        }

        body = body.replaceAll("==shipperLicenseNo==", (shipperLicense != null && StringUtils.isNotBlank(shipperLicense.getLicenseNumber())) ? shipperLicense.getLicenseNumber() : emptyString);
        body = body.replaceAll("==shipperLicenseType==", shipperLicense != null && shipperLicense.getLicenseType() != null ? shipperLicense.getLicenseType().name() : emptyString);
        body = body.replaceAll("==shipperBusinessName==", TextUtil.textOrEmpty(vendorName));
        body = body.replaceAll("==shipperAddress1==", StringUtils.isNotBlank(shipperAddress1) ? shipperAddress1 : emptyString);
        body = body.replaceAll("==shipperAddress2==", StringUtils.isNotBlank(shipperAddress2.toString()) ? shipperAddress2.toString() : emptyString);
        body = body.replaceAll("==shipperPhone==", StringUtils.isNotBlank(vendor.getPhone()) ? vendor.getPhone() : emptyString);
        body = body.replaceAll("==shipperContactName==", TextUtil.textOrEmpty(contactName.toString()));

        shipperAddress1 = "";
        shipperAddress2 = new StringBuilder();
        if (shop.getAddress() != null) {
            shipperAddress1 =  shop.getAddress().getAddress();
            this.prepareAddress(shop.getAddress(), shipperAddress2);
        }

        StringBuilder receiverContactName = new StringBuilder();
        Employee receiverEmployee = employeeRepository.getById(StringUtils.isNotBlank(order.getCreatedByEmployeeId()) ? order.getCreatedByEmployeeId() : token.getActiveTopUser().getUserId());
        if (receiverEmployee != null) {
            receiverContactName.append(StringUtils.isNotBlank(receiverEmployee.getFirstName()) ? receiverEmployee.getFirstName() + " " : "")
                    .append(StringUtils.isNotBlank(receiverEmployee.getLastName()) ? receiverEmployee.getLastName() + " " : "");
        }

        body = body.replaceAll("==receiverLicenseNo==", StringUtils.isNotBlank(shop.getLicense()) ? shop.getLicense() : emptyString);
        body = body.replaceAll("==receiverLicenseType==", TextUtil.textOrEmpty("Shop"));
        body = body.replaceAll("==receiverBusinessName==", StringUtils.isNotBlank(shop.getName()) ? shop.getName() : emptyString);
        body = body.replaceAll("==receiverAddress1==", StringUtils.isNotBlank(shipperAddress1) ? shipperAddress1 : emptyString);
        body = body.replaceAll("==receiverAddress2==", StringUtils.isNotBlank(shipperAddress2.toString()) ? shipperAddress2.toString() : emptyString);
        body = body.replaceAll("==receiverPhone==", StringUtils.isNotBlank(shop.getPhoneNumber()) ? shop.getPhoneNumber() : emptyString);
        body = body.replaceAll("==receiverContactName==", StringUtils.isNotBlank(receiverContactName.toString()) ? receiverContactName.toString() : emptyString);

        body = body.replaceAll("==distributorLicenseNo==", TextUtil.textOrEmpty(StringUtils.isNotBlank(shop.getLicense()) ? shop.getLicense() : emptyString));
        body = body.replaceAll("==distributorLicenseType==",TextUtil.textOrEmpty(shop.getShopType().name()));
        body = body.replaceAll("==distributorBusinessName==", TextUtil.textOrEmpty(StringUtils.isNotBlank(shop.getName()) ? shop.getName() : emptyString));
        body = body.replaceAll("==distributorAddress1==", TextUtil.textOrEmpty((shop.getAddress() != null && shop.getAddress().getAddress() != null) ? shop.getAddress().getAddress() : emptyString));
        body = body.replaceAll("==distributorAddress2==", TextUtil.textOrEmpty(StringUtils.isNotBlank(shipperAddress2.toString()) ? shipperAddress2.toString() : emptyString));
        body = body.replaceAll("==distributorPhone==",  TextUtil.textOrEmpty(StringUtils.isNotBlank(shop.getPhoneNumber()) ? shop.getPhoneNumber() : emptyString));
        body = body.replaceAll("==distributorContactName==", TextUtil.textOrEmpty(contactName.toString()));

        body = body.replaceAll("==driverName==", TextUtil.textOrEmpty(emptyString));
        body = body.replaceAll("==driverLicenseNo==", TextUtil.textOrEmpty(emptyString));
        body = body.replaceAll("==driverVehicleMake==", TextUtil.textOrEmpty(emptyString));
        body = body.replaceAll("==driverVehicleModel==", TextUtil.textOrEmpty(emptyString));
        body = body.replaceAll("==driverVehiclePlate==", TextUtil.textOrEmpty(emptyString));
        body = body.replaceAll("==arrivalTime==", TextUtil.textOrEmpty(emptyString));

        body = body.replaceAll("==rejectionReason==", StringUtils.isNotBlank(order.getDeclineReason()) ? order.getDeclineReason() : "");

        List<ObjectId> productIds = new ArrayList<>();
        List<ObjectId> batchIds = new ArrayList<>();
        for (POProductRequest poProductRequest : order.getPoProductRequestList()) {
            if (StringUtils.isNotBlank(poProductRequest.getProductId())
                    && ObjectId.isValid(poProductRequest.getProductId())) {
                productIds.add(new ObjectId(poProductRequest.getProductId()));

                if (poProductRequest.getBatchQuantityMap() != null && poProductRequest.getBatchQuantityMap().size() > 0) {
                    for (String batchId : poProductRequest.getBatchQuantityMap().keySet()) {
                        if (StringUtils.isNotBlank(batchId)
                                && ObjectId.isValid(batchId)) {

                            batchIds.add(new ObjectId(batchId));
                        }
                    }
                }
            }
        }

        HashMap<String, Product> productMap = productRepository.listAsMap(token.getCompanyId(), productIds);
        HashMap<String, ProductCategory> categoryMap = productCategoryRepository.listAsMap(token.getCompanyId(), token.getShopId());
        HashMap<String, ProductBatch> batchMap = productBatchRepository.listAsMap(token.getCompanyId(), batchIds);

        int productCount = 0;
        StringBuilder productInfo = new StringBuilder();
        for (POProductRequest request : order.getPoProductRequestList()) {

            Product product = productMap.get(request.getProductId());
            if (product == null) {
                continue;
            }

            String categoryStr = " units";
            ProductCategory category = categoryMap.get(product.getCategoryId());
            if (category != null) {
                categoryStr = category.getUnitType().toString();
            }

            String productName = product.getName();
            String batchUid = "-";
            boolean iterated = Boolean.FALSE;
            String itemQty = "-";
            if (order.getPurchaseOrderStatus() == PurchaseOrder.PurchaseOrderStatus.ReceivedShipment
                    || order.getPurchaseOrderStatus() == PurchaseOrder.PurchaseOrderStatus.Closed) {

                if (request.getBatchQuantityMap() != null && request.getBatchQuantityMap().size() > 0) {

                    for (String batchId : request.getBatchQuantityMap().keySet()) {
                        ProductBatch batch = batchMap.get(batchId);

                        BigDecimal quantity = request.getBatchQuantityMap().get(batchId);
                        if (batch != null) {
                            batchUid = StringUtils.isNotBlank(batch.getTrackHarvestBatch()) ? batch.getTrackHarvestBatch() + "(" + DateUtil.toDateFormatted(batch.getPurchasedDate(), timeZone, "MM/dd/yyyy") + ")" : "-";
                            productName = product.getName() + " (" + batch.getSku() + ")";
                            itemQty = (!iterated) ? TextUtil.formatToTwoDecimalPoints(request.getRequestQuantity()) + " " + categoryStr : "-";
                        }
                        this.prepareProductDetails(productInfo, productName, quantity, request, shop, emptyString, categoryStr, batchUid, itemQty);
                        iterated = Boolean.TRUE;
                        productCount++;
                    }
                } else {
                    itemQty = TextUtil.formatToTwoDecimalPoints(request.getRequestQuantity()) + " " + categoryStr;
                    prepareProductDetails(productInfo, productName, request.getReceivedQuantity(), request, shop, emptyString, categoryStr, batchUid, itemQty);
                    productCount++;
                }

            } else {
                itemQty = TextUtil.formatToTwoDecimalPoints(request.getRequestQuantity()) + " " + categoryStr;
                prepareProductDetails(productInfo, productName, request.getReceivedQuantity(), request, shop, emptyString, categoryStr, batchUid, itemQty);
                productCount++;
            }

        }
        for (int i = 0; i < 3; i++) {
            this.addEmptyProductRaw(productInfo);
            productCount++;
        }

        body = body.replaceAll("==productInformation==", productInfo.toString());
        body = body.replaceAll("&", "&amp;");

        body = body.replaceAll("==pageBreakPage1==", productCount < 10 ?  "page-break-before: always" : "");

        return body;
    }

    private void addEmptyProductRaw(StringBuilder productInfo) {
        productInfo.append("<tr style=\"height: 30px;\"><td class=\"content text-center\" style=\"width: 15%\">  </td>")
                .append("<td class=\"content text-center\" style=\"width: 25%; word-wrap: break-word;\"> </td>")
                .append("<td class=\"content text-center\" style=\"width: 9%\"> </td>")
                .append("<td class=\"bg-dark-grey content text-center\" style=\"width: 10%\"> </td>")
                .append("<td class=\"content text-center\" style=\"width: 10%\"> </td>")
                .append("<td class=\"content text-center\" style=\"width: 10%\"> </td>")
                .append("<td class=\"content text-center\" style=\"width: 10%\"> </td>")
                .append("<td style=\"width: 11%\" class=\"bg-dark-grey content text-center\"> </td>")
                .append("<td class=\"bg-dark-grey content text-center\"> </td>")
                .append("</tr>");
    }

    private void prepareProductDetails(StringBuilder productInfo, String productName, BigDecimal quantity, POProductRequest request, Shop shop, String emptyString, String categoryStr, String batchUId, String itemQty) {
        MathContext precision = new MathContext(3);
        BigDecimal batchDiscount = new BigDecimal(0);
        String receivedQty;
        BigDecimal batchFinalCost;
        if (request.getReceivedQuantity().compareTo(BigDecimal.ZERO) > 0) {
            if (request.getDiscount() != null && request.getDiscount().doubleValue() > 0) {
                batchDiscount = request.getDiscount().divide(request.getRequestQuantity(), precision).multiply(quantity);
            }
            batchFinalCost = (quantity.multiply(request.getUnitPrice())).subtract(batchDiscount);
            receivedQty = TextUtil.formatToTwoDecimalPoints(quantity) + categoryStr;
        } else {
            receivedQty = "-";
            batchDiscount = request.getDiscount();
            batchFinalCost = request.getRequestQuantity().multiply(request.getUnitPrice()).subtract(batchDiscount);
        }


        productInfo.append("<tr><td class=\"content text-center\" style=\"width: 15%\">" + (StringUtils.isNotBlank(batchUId) ? batchUId : emptyString) + "</td>")
                .append("<td class=\"content text-center\" style=\"width: 25%; word-wrap: break-word;\">" + productName + "</td>")
                .append("<td class=\"content text-center\" style=\"width: 9%\">" + itemQty + "</td>")
                .append("<td class=\"bg-dark-grey content text-center\" style=\"width: 10%\">" + receivedQty + "</td>")
                .append("<td class=\"content text-center\" style=\"width: 10%\">" + TextUtil.toEscapeCurrency(request.getUnitPrice().doubleValue(), shop.getDefaultCountry()) + "</td>")
                .append("<td class=\"content text-center\" style=\"width: 10%\">" + TextUtil.toEscapeCurrency(batchDiscount.doubleValue(), shop.getDefaultCountry()) + "</td>")
                .append("<td class=\"content text-center\" style=\"width: 10%\">" + TextUtil.toEscapeCurrency(batchFinalCost.doubleValue(), shop.getDefaultCountry()) + "</td>")
                .append("<td style=\"width: 11%\" class=\"bg-dark-grey content text-center\"> </td>")
                .append("<td class=\"bg-dark-grey content text-center\"> </td>")
                .append("</tr>");
    }

    private void prepareAddress(Address address, StringBuilder shipperAddress2) {

        if (StringUtils.isNotBlank(address.getCity())) {
            shipperAddress2.append(address.getCity());
        }
        if (StringUtils.isNotBlank(address.getState())) {
            shipperAddress2.append(", ").append(address.getState());
        }
        if (StringUtils.isNotBlank(address.getZipCode())) {
            shipperAddress2.append(", ").append(address.getZipCode());
        }
    }

    private String getPoShippingManifest(PurchaseOrder purchaseOrder) {
        String body = "";

        InputStream inputStream = PurchaseOrderServiceImpl.class.getResourceAsStream("/po_shipping_manifest.html");
        StringWriter writer = new StringWriter();
        try {
            IOUtils.copy(inputStream, writer, "UTF-8");
        } catch (IOException ex) {
            LOGGER.error("Error! in po email", ex);
            return body;
        }
        body = writer.toString();

        Shop shop = shopRepository.getById(purchaseOrder.getShopId());

        Vendor vendor = vendorRepository.getById(purchaseOrder.getVendorId());

        List<ObjectId> productIds = new ArrayList<>();
        List<ObjectId> categoryIds = new ArrayList<>();

        for (POProductRequest poProductRequest : purchaseOrder.getPoProductRequestList()) {
            if (StringUtils.isNotBlank(poProductRequest.getProductId())) {
                productIds.add(new ObjectId(poProductRequest.getProductId()));
            }
        }

        HashMap<String, Product> productHashMap = productRepository.listAsMap(token.getCompanyId(), productIds);

        for (Product product : productHashMap.values()) {
            if (StringUtils.isNotBlank(product.getCategoryId())) {
                categoryIds.add(new ObjectId(product.getCategoryId()));
            }
        }

        HashMap<String, ProductCategory> categoryHashMap = productCategoryRepository.listAsMap(token.getCompanyId(), categoryIds);


        StringBuilder shopAddressStr = new StringBuilder();
        StringBuilder addressStr = new StringBuilder();

        String logo = (shop != null && shop.getLogo() != null) ? shop.getLogo().getPublicURL() : "https://connect-assets.s3.amazonaws.com/email/logo.jpg";

        String timeZone = token.getRequestTimeZone();

        if (StringUtils.isBlank(timeZone) && shop != null) {
            timeZone = shop.getTimeZone();
        }

        BigDecimal balanceDue = purchaseOrder.getGrandTotal();

        if (purchaseOrder.getAmountPaid() != null) {
            balanceDue = purchaseOrder.getGrandTotal().subtract(purchaseOrder.getAmountPaid());
        }

        CompanyAsset qrCode = purchaseOrder.getPoQrCodeAsset();

        if (shop != null && shop.getAddress() != null) {
            Address address = shop.getAddress();
            shopAddressStr.append(StringUtils.isNotBlank(address.getAddress()) ? address.getAddress() + " <br/> " : "");
            shopAddressStr.append(StringUtils.isNotBlank(address.getCity()) ? address.getCity() + " <br/> " : "");
            shopAddressStr.append(StringUtils.isNotBlank(address.getState()) ? address.getState() + " <br/> " : "");
            shopAddressStr.append(StringUtils.isNotBlank(address.getCountry()) ? address.getCountry() + " <br/> " : "");
            shopAddressStr.append(StringUtils.isNotBlank(address.getZipCode()) ? address.getZipCode() : "");
        }
        body = body.replaceAll("==shopName==", TextUtil.textOrEmpty(shop != null ? shop.getName() : ""));
        body = body.replaceAll("==shopAddress==", TextUtil.textOrEmpty(shopAddressStr.toString()));
        body = body.replaceAll("==showShopAddress==", StringUtils.isNotBlank(shopAddressStr.toString()) ? "" : "display:none;");
        body = body.replaceAll("==logo==", logo);
        body = body.replaceAll("==licenseNumber==", TextUtil.textOrEmpty(shop != null ? shop.getLicense() : ""));

        body = body.replaceAll("==QRCodeImage==", qrCode != null ? qrCode.getPublicURL() : "");
        body = body.replaceAll("==poNumber==", TextUtil.textOrEmpty(purchaseOrder.getPoNumber()));
        body = body.replaceAll("==balanceDue==", TextUtil.toEscapeCurrency(balanceDue.doubleValue(), shop.getDefaultCountry()));
        body = body.replaceAll("==poDate==", TextUtil.textOrEmpty(DateUtil.toDateFormatted(purchaseOrder.getCreated(), timeZone, "MMM dd, yyyy")));
        body = body.replaceAll("==deliverDate==", TextUtil.textOrEmpty(DateUtil.toDateFormatted(purchaseOrder.getDeliveryDate(), timeZone, "MMM dd, yyyy")));
        body = body.replaceAll("==dueDate==", TextUtil.textOrEmpty(DateUtil.toDateFormatted((purchaseOrder.getDueDate() == 0 ? purchaseOrder.getPODueDate() : purchaseOrder.getDueDate()), timeZone, "MMM dd, yyyy")));
        body = body.replaceAll("==term==", TextUtil.textOrEmpty(purchaseOrder.getPoPaymentTerms().name().replaceAll("_", " ")));

        if (vendor != null && vendor.getAddress() != null) {
            Address address = vendor.getAddress();
            addressStr.append(StringUtils.isNotBlank(address.getAddress()) ? address.getAddress() + " <br/> " : "")
                    .append(StringUtils.isNotBlank(address.getCity()) ? address.getCity() + " <br/> " : "")
                    .append(StringUtils.isNotBlank(address.getState()) ? address.getState() + " <br/> " : "")
                    .append(StringUtils.isNotBlank(address.getCountry()) ? address.getCountry() + " <br/> " : "")
                    .append(StringUtils.isNotBlank(address.getZipCode()) ? address.getZipCode() : " ");
        }

        CompanyLicense companyLicense = null;
        CompanyContact companyContact;
        StringBuilder contactName = new StringBuilder();
        if (vendor != null) {
            companyLicense = vendor.getCompanyLicense(purchaseOrder.getLicenseId());
            companyContact = companyContactRepository.getCompanyContactByVendor(token.getCompanyId(), vendor.getImportId());

            if (companyContact != null) {
                contactName.append(StringUtils.isNotBlank(companyContact.getFirstName()) ? companyContact.getFirstName() + " " : "")
                        .append(StringUtils.isNotBlank(companyContact.getLastName()) ? companyContact.getLastName() + " " : "");
            }
        }

        StringBuilder receiverContactName = new StringBuilder();
        Employee receiverEmployee = employeeRepository.getById(StringUtils.isNotBlank(purchaseOrder.getCreatedByEmployeeId()) ? purchaseOrder.getCreatedByEmployeeId() : token.getActiveTopUser().getUserId());
        if (receiverEmployee != null) {
            receiverContactName.append(StringUtils.isNotBlank(receiverEmployee.getFirstName()) ? receiverEmployee.getFirstName() + " " : "")
                    .append(StringUtils.isNotBlank(receiverEmployee.getLastName()) ? receiverEmployee.getLastName() + " " : "");
        }

        body = body.replaceAll("==shipperCompanyName==", TextUtil.textOrEmpty(vendor != null ? vendor.getName() : ""));
        body = body.replaceAll("==shipperContactName==", TextUtil.textOrEmpty(contactName.toString()));
        body = body.replaceAll("==shipperCompanyType==", TextUtil.textOrEmpty(companyLicense != null ? companyLicense.getCompanyType().toString() : ""));
        body = body.replaceAll("==shipperStateLicense==", TextUtil.textOrEmpty(companyLicense != null ? companyLicense.getLicenseNumber() : ""));
        body = body.replaceAll("==shipperAddress==", TextUtil.textOrEmpty(addressStr.toString()));
        body = body.replaceAll("==shipperPhoneNumber==", TextUtil.textOrEmpty(vendor != null ? vendor.getPhone() : ""));

        body = body.replaceAll("==receiverCompanyName==", TextUtil.textOrEmpty(shop != null ? shop.getName() : ""));
        body = body.replaceAll("==receiverContactName==", TextUtil.textOrEmpty(receiverContactName.toString()));
        body = body.replaceAll("==receiverCompanyType==", TextUtil.textOrEmpty("Shop"));
        body = body.replaceAll("==receiverStateLicense==", TextUtil.textOrEmpty(shop != null ? shop.getLicense() : ""));
        body = body.replaceAll("==receiverAddress==", TextUtil.textOrEmpty(shopAddressStr.toString()));
        body = body.replaceAll("==receiverPhoneNumber==", TextUtil.textOrEmpty(shop != null ? shop.getPhoneNumber() : ""));


        StringBuilder productInfo = new StringBuilder();
        int i = 0;
        for (POProductRequest poProductRequest : purchaseOrder.getPoProductRequestList()) {
            Product product = productHashMap.get(poProductRequest.getProductId());
            if (product == null) {
                continue;
            }

            String unitStr = ProductCategory.UnitType.units.toString();

            ProductCategory category = categoryHashMap.get(product.getCategoryId());
            unitStr = (category != null) ? category.getUnitType().toString() : unitStr;

            String status = poProductRequest.getRequestStatus().toString();
            if (purchaseOrder.getPurchaseOrderStatus() == PurchaseOrder.PurchaseOrderStatus.ReceivedShipment
                    || purchaseOrder.getPurchaseOrderStatus() == PurchaseOrder.PurchaseOrderStatus.Closed) {
                body = body.replaceAll("==showRequestTable==", TextUtil.textOrEmpty("display:none;"));
                if (CollectionUtils.isNotEmpty(poProductRequest.getBatchAddDetails())) {
                    body = body.replaceAll("==showBatchTable==", TextUtil.textOrEmpty(""));
                    body = body.replaceAll("==showEmptyBatchTable==", TextUtil.textOrEmpty("display:none;"));
                    for (POBatchAddRequest batchAddRequest : poProductRequest.getBatchAddDetails()) {
                        productInfo.append("<tr><td>" + (++i) + ".</td>")
                                .append("<td class=\"product-Name_30\">" + product.getName() + "</td>")
                                .append("<td style=\"width:10%;\">" + TextUtil.textOrEmpty(batchAddRequest.getBatchSku()) + "</td>")
                                .append("<td style=\"width:10%;\">" + TextUtil.textOrEmpty(batchAddRequest.getTestId()) + "</td>")
                                .append("<td style=\"width:10%;padding:0\">" + TextUtil.textOrEmpty((batchAddRequest.getDateSent() != 0 ? DateUtil.toDateFormatted(batchAddRequest.getDateSent(), timeZone, "MM/dd/YYYY") : "-")) + "</td>")
                                .append("<td style=\"width:10%;text-align:center;\">" + TextUtil.textOrEmpty(status) + "</td>")
                                .append("<td style=\"width:5%;\">" + TextUtil.textOrEmpty(batchAddRequest.getQuantity().doubleValue() + " " + unitStr) + "</td>")
                                .append("<td style=\"width:20%;\">  </td></tr>");
                    }
                } else {
                    body = body.replaceAll("==showBatchTable==", TextUtil.textOrEmpty("display:none;"));
                    body = body.replaceAll("==showEmptyBatchTable==", TextUtil.textOrEmpty(""));
                    productInfo.append("<tr><td>" + (++i) + ".</td>")
                            .append("<td class=\"product-Name_30\">" + product.getName() + "</td>")
                            .append("<td style=\"width:10%;\">" + TextUtil.textOrEmpty("-") + "</td>")
                            .append("<td style=\"width:10%;\">" + TextUtil.textOrEmpty("-") + "</td>")
                            .append("<td style=\"width:10%;\">" + TextUtil.textOrEmpty("-") + "</td>")
                            .append("<td style=\"width:10%;text-align:center;\">" + TextUtil.textOrEmpty(status) + "</td>")
                            .append("<td style=\"width:5%;\">" + TextUtil.textOrEmpty(poProductRequest.getRequestQuantity().doubleValue() + " " + unitStr) + "</td>")
                            .append("<td style=\"width:5%;\">" + TextUtil.textOrEmpty(poProductRequest.getReceivedQuantity().doubleValue() + " " + unitStr) + "</td>")
                            .append("<td style=\"width:15%;\">  </td></tr>");
                }
            } else {
                body = body.replaceAll("==showBatchTable==", TextUtil.textOrEmpty("display:none;"));
                body = body.replaceAll("==showRequestTable==", TextUtil.textOrEmpty(""));
                body = body.replaceAll("==showEmptyBatchTable==", TextUtil.textOrEmpty("display:none;"));
                productInfo.append("<tr><td>" + (++i) + ".</td>")
                        .append("<td class=\"product-Name_35\">" + product.getName() + "</td>")
                        .append("<td style=\"width:10%;\">" + TextUtil.textOrEmpty(poProductRequest.getRequestQuantity().doubleValue() + " " + unitStr) + "</td>")
                        .append("<td style=\"width:15%;\">  </td>")
                        .append("<td style=\"width:35%;\">  </td></tr>");
            }

        }

        body = body.replaceAll("==productInformation==", TextUtil.textOrEmpty(productInfo.toString()));

        body = body.replaceAll("==deliveryDate==", TextUtil.toDate(DateUtil.toDateTime(purchaseOrder.getDeliveryDate(), timeZone)));
        body = body.replaceAll("==deliveryTime==", TextUtil.toTime(DateUtil.toDateTime(purchaseOrder.getDeliveryTime(), timeZone)));
        body = body.replaceAll("==shippingAddress==", TextUtil.textOrEmpty(StringUtils.isNotBlank(purchaseOrder.getDeliveryAddress()) ? purchaseOrder.getDeliveryAddress() : addressStr.toString()));

        if (purchaseOrder.getManagerReceiveSignature() != null && StringUtils.isNotBlank(purchaseOrder.getManagerReceiveSignature().getKey())) {
            try {
                AssetStreamResult assetStreamResult = amazonS3Service.downloadFile(purchaseOrder.getManagerReceiveSignature().getKey(), true);
                InputStream stream = assetStreamResult.getStream();
                String imageStr = Base64.encodeBase64String(IOUtils.toByteArray(stream));
                body = body.replaceAll("==signatureImage==", imageStr);
                body = body.replaceAll("==showSignature==", TextUtil.textOrEmpty(""));
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
            }
        } else if (purchaseOrder.getManagerReceiveSignature() == null || StringUtils.isBlank(purchaseOrder.getManagerReceiveSignature().getKey())) {
            body = body.replaceAll("data:image/jpeg;base64,==signatureImage==", TextUtil.textOrEmpty(""));
            body = body.replaceAll("==showSignature==", TextUtil.textOrEmpty("display:none;"));
        }

        if (purchaseOrder.getManagerReceiveSignature() != null) {
            long signDateTime = purchaseOrder.getManagerReceiveSignature().getCreated();
            DateTime dateTime = DateUtil.toDateTime(signDateTime, timeZone);
            body = body.replaceAll("==signatureDate==", TextUtil.toDate(dateTime));
            body = body.replaceAll("==signatureTime==", TextUtil.toTime(dateTime));
        } else {
            body = body.replaceAll("==signatureDate==", TextUtil.textOrEmpty(""));
            body = body.replaceAll("==signatureTime==", TextUtil.textOrEmpty(""));
        }
        return body;
    }


    /**
     * Private method to return total calculated purchase order discount
     *
     * @param purchaseOrder : purchase order
     * @return : return total calculated discount
     */
    private BigDecimal getTotalDiscount(PurchaseOrder purchaseOrder) {
        BigDecimal totalDiscount = BigDecimal.ZERO;

        for (POProductRequest poProductRequest : purchaseOrder.getPoProductRequestList()) {
            totalDiscount = totalDiscount.add(poProductRequest.getDiscount() == null ? BigDecimal.ZERO : poProductRequest.getDiscount());
        }

        return purchaseOrder.getDiscount().add(totalDiscount);
    }

    /**
     * Override method to get purchase order limited details.
     * @param status    : status
     * @param vendorIds : vendorIds
     * @param productIds : productIds
     * @return : po limited list
     */
    @Override
    public List<PurchaseOrderLimitedResult> getPoLimitedDetails(PurchaseOrder.PurchaseOrderStatus status, List<String> vendorIds, List<String> productIds) {
        if (CollectionUtils.isEmpty(vendorIds) && CollectionUtils.isEmpty(productIds)) {
            return purchaseOrderRepository.getPurchaseOrderByStatus(token.getCompanyId(), token.getShopId(), status, PurchaseOrderLimitedResult.class);
        }
        return purchaseOrderRepository.getPOByVendorAndProductStatus(token.getCompanyId(), token.getShopId(), vendorIds, productIds, status, PurchaseOrderLimitedResult.class);
    }

    /**
     *
     * @param purchaseOrderId : : id of purchase order that  will copied
     * @return
     */
    @Override
    public PurchaseOrder copyPurchaseOrder(String purchaseOrderId){
        PurchaseOrder dbPurchaseOrder = purchaseOrderRepository.get(token.getCompanyId(), purchaseOrderId);

        if(Objects.isNull(dbPurchaseOrder)){
            throw new BlazeInvalidArgException(PURCHASE_ORDER, PO_NOT_FOUND);
        }
        PurchaseOrderAddRequest addRequest = new PurchaseOrderAddRequest();
        addRequest.setAdjustmentInfoList(dbPurchaseOrder.getAdjustmentInfoList());

        addRequest.setCustomerType(dbPurchaseOrder.getCustomerType());
        addRequest.setCustomTermDate(dbPurchaseOrder.getCustomTermDate());

        addRequest.setDeliveryAddress(dbPurchaseOrder.getDeliveryAddress());
        addRequest.setDeliveryDate(dbPurchaseOrder.getDeliveryDate());
        addRequest.setDeliveryTime(dbPurchaseOrder.getDeliveryTime());
        addRequest.setDiscount(dbPurchaseOrder.getDiscount());

        addRequest.setFees(dbPurchaseOrder.getFees());
        addRequest.setFlowerSourceType(dbPurchaseOrder.getFlowerSourceType());

        addRequest.setLicenseId(dbPurchaseOrder.getLicenseId());
        addRequest.setNotes(dbPurchaseOrder.getNotes());
        addRequest.setPaymentType(dbPurchaseOrder.getPoPaymentOptions().getPaymentType());
        addRequest.setPoPaymentTerms(dbPurchaseOrder.getPoPaymentTerms().toString());
        List<POProductAddRequest> poProductAddRequestList = new ArrayList<>();
        for(POProductRequest poProductRequest : dbPurchaseOrder.getPoProductRequestList()){
            POProductAddRequest poProductAddRequest = new POProductAddRequest();
            poProductAddRequest.setDiscount(poProductRequest.getDiscount());
            poProductAddRequest.setFinalTotalCost(poProductRequest.getFinalTotalCost());
            poProductAddRequest.setNotes(poProductRequest.getNotes());
            poProductAddRequest.setProductId(poProductRequest.getProductId());
            poProductAddRequest.setRequestQuantity(poProductRequest.getRequestQuantity());
            poProductAddRequest.setTotalCost(poProductRequest.getTotalCost());
            poProductAddRequestList.add(poProductAddRequest);
        }
        addRequest.setPoProductAddRequestList(poProductAddRequestList);
        addRequest.setPurchaseOrderDate(dbPurchaseOrder.getPurchaseOrderDate());
        addRequest.setReference(dbPurchaseOrder.getReference());
        addRequest.setTermsAndCondition(dbPurchaseOrder.getTermsAndCondition());
        addRequest.setTransactionType(dbPurchaseOrder.getTransactionType());
        addRequest.setVendorId(dbPurchaseOrder.getVendorId());
        return this.addPurchaseOrder(addRequest);
    }
}

