package com.fourtwenty.core.services.mgmt.impl;

import com.esotericsoftware.kryo.Kryo;
import com.fourtwenty.core.config.pdf.PdfGenerator;
import com.fourtwenty.core.domain.models.checkin.CheckIn;
import com.fourtwenty.core.domain.models.company.*;
import com.fourtwenty.core.domain.models.consumer.ConsumerUser;
import com.fourtwenty.core.domain.models.customer.*;
import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.models.generic.Asset;
import com.fourtwenty.core.domain.models.generic.Note;
import com.fourtwenty.core.domain.models.generic.UniqueSequence;
import com.fourtwenty.core.domain.models.loyalty.Promotion;
import com.fourtwenty.core.domain.models.loyalty.PromotionReqLog;
import com.fourtwenty.core.domain.models.notification.NotificationInfo;
import com.fourtwenty.core.domain.models.payment.ShopPaymentOption;
import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.models.receipt.ReceiptInfo;
import com.fourtwenty.core.domain.models.store.ConsumerCart;
import com.fourtwenty.core.domain.models.tookan.EmployeeTookanInfo;
import com.fourtwenty.core.domain.models.tookan.TookanTeamInfo;
import com.fourtwenty.core.domain.models.transaction.*;
import com.fourtwenty.core.domain.models.loyalty.*;
import com.fourtwenty.core.domain.repositories.loyalty.LoyaltyRewardRepository;
import com.fourtwenty.core.domain.repositories.checkin.CheckInRepository;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.domain.repositories.store.ConsumerCartRepository;
import com.fourtwenty.core.domain.repositories.store.ConsumerUserRepository;
import com.fourtwenty.core.event.BlazeEventBus;
import com.fourtwenty.core.event.transaction.CloverPaymentCardsForTransactionEvent;
import com.fourtwenty.core.event.transaction.ConsumerCartEvent;
import com.fourtwenty.core.event.transaction.MtracPaymentCardsForTransactionEvent;
import com.fourtwenty.core.event.transaction.ProcessPaymentCardsForTransactionEvent;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.managed.*;
import com.fourtwenty.core.rest.dispensary.requests.inventory.BulkInventoryTransferRequest;
import com.fourtwenty.core.rest.dispensary.requests.inventory.InventoryTransferRequest;
import com.fourtwenty.core.rest.dispensary.requests.queues.*;
import com.fourtwenty.core.rest.dispensary.requests.shop.SendReceiptRequest;
import com.fourtwenty.core.rest.dispensary.requests.terminals.DeliveryLocationRequest;
import com.fourtwenty.core.rest.dispensary.requests.transaction.AddOrderTagsRequest;
import com.fourtwenty.core.rest.dispensary.requests.transaction.OrderScheduleUpdateRequest;
import com.fourtwenty.core.rest.dispensary.requests.transaction.PaymentOptionUpdateRequest;
import com.fourtwenty.core.rest.dispensary.results.*;
import com.fourtwenty.core.rest.dispensary.results.common.AssetStreamResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.PurchaseProduct;
import com.fourtwenty.core.rest.dispensary.results.queue.CompositeMemberTransResult;
import com.fourtwenty.core.rest.dispensary.results.shop.AllQueueResult;
import com.fourtwenty.core.rest.dispensary.results.shop.ConsumerCartResult;
import com.fourtwenty.core.rest.dispensary.results.shop.DashboardQueueResult;
import com.fourtwenty.core.rest.store.webhooks.ConsumerOrderData;
import com.fourtwenty.core.rest.store.webhooks.TransactionData;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.common.BackgroundJobService;
import com.fourtwenty.core.services.common.ConsumerNotificationService;
import com.fourtwenty.core.services.common.EmployeeNotificationService;
import com.fourtwenty.core.services.common.RealtimeService;
import com.fourtwenty.core.services.compliance.metrc.MetrcProcessingService;
import com.fourtwenty.core.services.global.CannabisLimitService;
import com.fourtwenty.core.services.global.CashDrawerProcessorService;
import com.fourtwenty.core.services.mgmt.*;
import com.fourtwenty.core.services.thirdparty.AmazonS3Service;
import com.fourtwenty.core.services.tookan.TookanService;
import com.fourtwenty.core.thirdparty.onfleet.services.OnFleetService;
import com.fourtwenty.core.thirdparty.tookan.model.response.TookanTaskResult;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.core.util.NumberUtils;
import com.fourtwenty.core.util.TextUtil;
import com.google.common.collect.Lists;
import com.google.inject.Provider;
import com.fourtwenty.core.domain.models.transaction.FcmPayload;
import joptsimple.internal.Strings;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.stream.Collectors;

import static com.fourtwenty.core.util.ReceiptUtil.*;

/**
 * Created by mdo on 9/27/15.
 */
public class QueuePOSServiceImpl extends AbstractAuthServiceImpl implements QueuePOSService {
    private static final Log LOG = LogFactory.getLog(QueuePOSServiceImpl.class);
    private static final String ONFLEET_OBJECT_ERROR = "Error while copying transaction property in OnFleet transaction's result.";
    private static final String TRANSACTION = "Transaction";
    private static final String TRANSACTION_LOCKED = "Transaction is locked, so you can't modify it.";
    private static final String TRANSACTION_NOT_FOUND = "Transaction does not exist.";
    private static final String EMPLOYEE = "Employee";
    private static final String EMPLOYEE_NOT_FOUND = "Employee does not exist.";
    private static final String TRANSACTION_LOCK_PERMISSION = "You don't have permission to perform lock/unlock transaction.";
    private static final String SHOP = "Shop";
    private static final String SHOP_NOT_FOUND = "Shop not found";
    private static final String TRANSACTION_NOT_COMPLETE = "Transaction %s is still is in Progress.";
    private static final String BLANK_REQUEST = "Request cannot be blank.";
    private static final String BLANK_PAYMENT_OPTION = "Payment option cannot be blank or None.";
    private static final String SPLIT_PAYMENT_NOT_FOUND = "Split payment was not received. Please specify split payments.";
    private static final Logger LOGGER = LoggerFactory.getLogger(InventoryTransferHistoryServiceImpl.class);

    @Inject
    ShopRepository shopRepository;
    @Inject
    TransactionRepository transactionRepository;
    @Inject
    MemberRepository memberRepository;
    @Inject
    RealtimeService realtimeService;
    @Inject
    UniqueSequenceRepository uniqueSequenceRepository;
    @Inject
    CompanyRepository companyRepository;
    @Inject
    AmazonServiceManager amazonServiceManager;
    @Inject
    ProductRepository productRepository;
    @Inject
    BackgroundJobService backgroundJobService;
    @Inject
    QueuedTransactionRepository queuedTransactionRepository;
    @Inject
    ProductCategoryRepository productCategoryRepository;
    @Inject
    ContractRepository contractRepository;
    @Inject
    EmployeeRepository employeeRepository;
    @Inject
    TerminalRepository terminalRepository;
    @Inject
    ProductWeightToleranceRepository toleranceRepository;
    @Inject
    CartService cartService;
    @Inject
    InventoryRepository inventoryRepository;
    @Inject
    CompanyFeaturesRepository companyFeaturesRepository;
    @Inject
    DefaultDataService defaultDataService;
    @Inject
    PrepackageRepository prepackageRepository;
    @Inject
    PrepackageProductItemRepository prepackageProductItemRepository;
    @Inject
    ProductWeightToleranceRepository weightToleranceRepository;
    @Inject
    ProductPrepackageQuantityRepository prepackageQuantityRepository;
    @Inject
    ConsumerCartRepository consumerCartRepository;
    @Inject
    ConsumerUserRepository consumerUserRepository;
    @Inject
    MemberGroupRepository memberGroupRepository;
    @Inject
    CompanyAssetRepository companyAssetRepository;
    @Inject
    DoctorRepository doctorRepository;
    @Inject
    MemberGroupPricesRepository memberGroupPricesRepository;
    @Inject
    ConsumerNotificationService consumerNotificationService;
    @Inject
    EmployeeNotificationService employeeNotificationService;
    @Inject
    PushNotificationManager pushNotificationManager;
    @Inject
    RoleService roleService;
    @Inject
    CashDrawerSessionRepository cashDrawerSessionRepository;
    @Inject
    AssetService assetService;
    @Inject
    private DeliveryService deliveryService;
    @Inject
    private CashDrawerActivityLogService cashDrawerActivityLogService;
    @Inject
    private ElasticSearchManager elasticSearchManager;
    @Inject
    private OnFleetService onFleetService;
    @Inject
    MetrcProcessingService metrcProcessingService;
    @Inject
    private AmazonS3Service amazonS3Service;
    @Inject
    private CannabisLimitService cannabisLimitService;
    @Inject
    private CompanyService companyService;
    @Inject
    private PaidInOutItemRepository paidInOutItemRepository;
    @Inject
    private PartnerWebHookManager partnerWebHookManager;
    @Inject
    RefundTransactionRequestRepository refundTransactionRequestRepository;
    @Inject
    private SpringBigManager springBigManager;
    @Inject
    private SpringbigRepository springbigRepository;
    @Inject
    private TookanService tookanService;
    @Inject
    private BlazeEventBus eventBus;
    @Inject
    private CheckInRepository checkInRepository;
    @Inject
    private BrandRepository brandRepository;
    @Inject
    private ReportManager reportManager;
    @Inject
    private ShopPaymentOptionService shopPaymentOptionService;
    @Inject
    private PromotionRepository promotionRepository;
    @Inject
    PromotionProcessService promotionProcessService;
    @Inject
    LoyaltyRewardRepository rewardRepository;
    @Inject
    CashDrawerProcessorService cashDrawerProcessorService;

    @Inject
    public QueuePOSServiceImpl(Provider<ConnectAuthToken> token) {
        super(token);
    }

    @Override
    public AllQueueResult getTransactionsWithDate(long startDate, long endDate) {
        startDate = (startDate < 0) ? 0 : startDate;
        endDate = (endDate == 0) ? DateTime.now().getMillis() : endDate;

        DateSearchResult<Transaction> allTransactions = transactionRepository.getActiveTransactions(token.getCompanyId(), token.getShopId(), true, startDate, endDate);
        AllQueueResult result = new AllQueueResult();

        // let's do the filter
        List<ObjectId> objectIds = new ArrayList<>();
        for (Transaction transaction : allTransactions.getValues()) {
            if (transaction.getMemberId() != null && ObjectId.isValid(transaction.getMemberId())) {
                objectIds.add(new ObjectId(transaction.getMemberId()));
            }
        }


        Iterable<Member> members = memberRepository.findItemsIn(token.getCompanyId(), objectIds, "{firstName:1,lastName:1,status:1,memberGroup:1}");
        HashMap<String, Member> memberHashMap = new HashMap<>();
        for (Member m : members) {
            memberHashMap.put(m.getId(), m);
        }

        for (Transaction transaction : allTransactions.getValues()) {
            Member m = memberHashMap.get(transaction.getMemberId());
            transaction.setMember(m);
            if (transaction.getQueueType() == Transaction.QueueType.WalkIn) {
                result.getWalkin().getValues().add(transaction);
            } else if (transaction.getQueueType() == Transaction.QueueType.Delivery) {
                result.getDelivery().getValues().add(transaction);
            } else if (transaction.getQueueType() == Transaction.QueueType.Online) {
                result.getOnline().getValues().add(transaction);
            } else if (transaction.getQueueType() == Transaction.QueueType.Special) {
                result.getSpecial().getValues().add(transaction);
            }
        }

        Iterable<ConsumerCartResult> incomingCarts = consumerCartRepository.getPlacedConsumerCartsByStatus(token.getCompanyId(),
                token.getShopId(), Lists.newArrayList(ConsumerCart.ConsumerCartStatus.Placed), startDate, endDate, ConsumerCartResult.class);

        SearchResult<DashboardQueueResult> dashboardQueueResultSearchResult = prepareDashboardIncomingOrders(incomingCarts);

        result.setIncoming(dashboardQueueResultSearchResult);
        result.getWalkin().setTotal((long) result.getWalkin().getValues().size());
        result.getDelivery().setTotal((long) result.getDelivery().getValues().size());
        result.getOnline().setTotal((long) result.getOnline().getValues().size());
        result.getSpecial().setTotal((long) result.getSpecial().getValues().size());
        return result;
    }

    @Override
    public SearchResult<ConsumerCartResult> getIncomingOrders(int start, int limit) {
        limit = (limit == 0) ? Integer.MAX_VALUE : limit;
        SearchResult<ConsumerCartResult> incomingCarts = consumerCartRepository.getPlacedConsumerCartsByStatus(token.getCompanyId(),
                token.getShopId(), Lists.newArrayList(ConsumerCart.ConsumerCartStatus.Placed), start, limit, ConsumerCartResult.class);
        return prepareIncomingOrders(incomingCarts);
    }

    @Override
    public SearchResult<ConsumerCartResult> getRejectedOrders(int start, int limit) {
        limit = (limit == 0) ? 200 : limit;
        SearchResult<ConsumerCartResult> incomingCarts = consumerCartRepository.getPlacedConsumerCartsByStatus(token.getCompanyId(),
                token.getShopId(),
                Lists.newArrayList(ConsumerCart.ConsumerCartStatus.Declined,
                        ConsumerCart.ConsumerCartStatus.CanceledByConsumer,
                        ConsumerCart.ConsumerCartStatus.CanceledByDispensary),
                start, limit, ConsumerCartResult.class);
        return prepareIncomingOrders(incomingCarts);
    }

    @Override
    public DateSearchResult<Transaction> getTransactionsForMembers(String memberId, long afterDate, long beforeDate) {
        if (beforeDate <= 0) {
            beforeDate = DateTime.now().getMillis();
        }
        if (afterDate < 0) {
            afterDate = 0;
        }
        DateSearchResult<Transaction> trans = transactionRepository.getTransactionsForMemberDates(token.getCompanyId(), token.getShopId(), memberId, afterDate, beforeDate);

        assignDependentObjects(trans, true, false);
        return trans;
    }

    private SearchResult<DashboardQueueResult> prepareDashboardIncomingOrders(final Iterable<ConsumerCartResult> incomingCarts) {

        List<ObjectId> consumerIds = new ArrayList<>();
        for (ConsumerCartResult consumerCart : incomingCarts) {
            if (consumerCart.getConsumerId() != null && ObjectId.isValid(consumerCart.getConsumerId())) {
                consumerIds.add(new ObjectId(consumerCart.getConsumerId()));
            }
        }

        HashMap<String, ConsumerUser> consumerUserHashMap = consumerUserRepository.findItemsInAsMap(consumerIds);
        SearchResult<DashboardQueueResult> results = new SearchResult<>();
        for (ConsumerCartResult consumerCart : incomingCarts) {
            ConsumerUser consumerUser = consumerUserHashMap.get(consumerCart.getConsumerId());
            DashboardQueueResult result = new DashboardQueueResult();
            result.setOrderNo(consumerCart.getOrderNo());
            result.setStartTime(consumerCart.getOrderPlacedTime());
            if (consumerUser != null) {
                result.setMemberName(consumerUser.getFirstName() + " " + consumerUser.getLastName());
            }

            results.getValues().add(result);
        }
        results.setTotal((long) results.getValues().size());

        return results;
    }

    private SearchResult<ConsumerCartResult> prepareIncomingOrders(final SearchResult<ConsumerCartResult> incomingCarts) {
        // let's do the filter
        HashSet<ObjectId> memberIds = new HashSet<>();
        HashSet<ObjectId> productIds = new LinkedHashSet<>();
        List<ObjectId> consumerIds = new ArrayList<>();
        for (ConsumerCartResult consumerCart : incomingCarts.getValues()) {
            if (consumerCart.getMemberId() != null && ObjectId.isValid(consumerCart.getMemberId())) {
                memberIds.add(new ObjectId(consumerCart.getMemberId()));
            }

            if (consumerCart.getConsumerId() != null && ObjectId.isValid(consumerCart.getConsumerId())) {
                consumerIds.add(new ObjectId(consumerCart.getConsumerId()));
            }

            if (consumerCart.getCart() != null && consumerCart.getCart().getItems() != null) {
                for (OrderItem orderItem : consumerCart.getCart().getItems()) {
                    if (orderItem.getProductId() != null && ObjectId.isValid(orderItem.getProductId())) {
                        productIds.add(new ObjectId(orderItem.getProductId()));
                    }
                }
            }
        }

        HashMap<String, ConsumerUser> consumerUserHashMap = consumerUserRepository.findItemsInAsMap(consumerIds);
        for (ConsumerUser consumerUser : consumerUserHashMap.values()) {
            String memberId = consumerUser.getMemberId();
            if (memberId != null) {
                if (ObjectId.isValid(memberId)) {
                    memberIds.add(new ObjectId(memberId));
                }
            }
        }


        HashMap<String, Product> productHashMap = productRepository.findItemsInAsMap(token.getCompanyId(), Lists.newArrayList(productIds));
        HashMap<String, ProductCategory> categoryHashMap = productCategoryRepository.listAsMap(token.getCompanyId(), token.getShopId());

        Iterable<Member> members = memberRepository.findItemsIn(token.getCompanyId(), Lists.newArrayList(memberIds), "{firstName:1,lastName:1,status:1,memberGroup:1,primaryPhone:1,email:1,dob:1,address:1,medical:1}");
        HashMap<String, Member> memberHashMap = new HashMap<>();
        for (Member m : members) {
            memberHashMap.put(m.getId(), m);
        }


        // show agree or non-agree
        List<ConsumerCartResult> consumerCarts = new ArrayList<>();
        for (ConsumerCartResult consumerCart : incomingCarts.getValues()) {

            ConsumerUser cu = consumerUserHashMap.get(consumerCart.getConsumerId());
            if (cu != null) {
                consumerCarts.add(consumerCart);
                consumerCart.setMembershipAccepted(false);
                consumerCart.setConsumerUser(cu);
                cu.setPassword(null);
                consumerCart.setMembershipAccepted(cu.isAccepted());
                consumerCart.setMemberId(cu.getMemberId());

                /*for (ConsumerMemberStatus consumerMemberStatus : cu.getMemberStatuses()) {
                    if (consumerMemberStatus.getShopId().equalsIgnoreCase(token.getShopId())
                            && consumerMemberStatus.getMemberId() != null) {
                        consumerCart.setMembershipAccepted(consumerMemberStatus.isAccepted());
                        consumerCart.setMemberId(consumerMemberStatus.getMemberId());
                        break;
                    }
                }*/

                Member member = memberHashMap.get(consumerCart.getMemberId());
                consumerCart.setMember(member);


            }
            if (consumerCart.getCart() != null && consumerCart.getCart().getItems() != null) {
                for (OrderItem orderItem : consumerCart.getCart().getItems()) {
                    Product product = productHashMap.get(orderItem.getProductId());
                    if (product != null) {
                        product.setCategory(categoryHashMap.get(product.getCategoryId()));
                    }
                    orderItem.setProduct(product);
                }
            }
        }

        incomingCarts.setValues(consumerCarts);
        return incomingCarts;
    }

    @Override
    public DateSearchResult<ConsumerCartResult> getConsumerOrders(long afterDate, long beforeDate) {
        DateSearchResult<ConsumerCartResult> result = consumerCartRepository.findItemsWithDate(afterDate, beforeDate, ConsumerCartResult.class);

        List<ObjectId> consumerIds = new ArrayList<>();
        for (ConsumerCart consumerCart : result.getValues()) {
            if (consumerCart.getConsumerId() != null && ObjectId.isValid(consumerCart.getConsumerId())) {
                consumerIds.add(new ObjectId(consumerCart.getConsumerId()));
            }
        }
        HashMap<String, ConsumerUser> consumerUserHashMap = consumerUserRepository.findItemsInAsMap(consumerIds);
        // show agree or non-agree
        List<ConsumerCartResult> consumerCarts = new ArrayList<>();
        for (ConsumerCartResult consumerCart : result.getValues()) {
            consumerCarts.add(consumerCart);

            ConsumerUser cu = consumerUserHashMap.get(consumerCart.getConsumerId());
            consumerCart.setMembershipAccepted(false);
            consumerCart.setConsumerUser(cu);
            cu.setPassword(null);
            if (cu != null) {
                consumerCart.setConsumerUser(cu);
                consumerCart.setMembershipAccepted(cu.isAccepted());
                /*for (ConsumerMemberStatus consumerMemberStatus : cu.getMemberStatuses()) {
                    if (consumerMemberStatus.getShopId().equalsIgnoreCase(token.getShopId())) {
                        consumerCart.setMembershipAccepted(consumerMemberStatus.isAccepted());
                        break;
                    }
                }*/
            }
        }

        return result;
    }

    // Transactions

    /**
     *
     * @param queueName - Queue Name
     * @param request - Add Member Request Object
     * @param ticketNumber - Ticket Number. Enter -1 to have it auto-generate
     * @return
     */
    @Override
    public Transaction addToQueue(String queueName, QueueAddMemberRequest request, long ticketNumber) {
        if (queueName == null) {
            throw new BlazeInvalidArgException("QueueName", "Please specify a queue name.");
        }

        Member member = memberRepository.get(token.getCompanyId(), request.getMemberId());
        if (member == null) {
            throw new BlazeInvalidArgException("Member", "Member does not exist.");
        }

        if (member.isBanPatient() == true) {
            throw new BlazeInvalidArgException("Member", "Banned member cannot be added to queue");
        }

        if (member.getStatus() == Member.MembershipStatus.Inactive) {
            throw new BlazeInvalidArgException("Member", "Cannot add inactive member to queue.");
        }

        Transaction.QueueType queueType = Transaction.QueueType.None;
        try {
            queueType = Transaction.QueueType.valueOf(queueName);
        } catch (Exception e) {
            throw new BlazeInvalidArgException("Queue name", "Invalid queue name. Must be of: "
                    + Transaction.QueueType.Online.name() + ", " + Transaction.QueueType.Delivery.name() + ", " + Transaction.QueueType.WalkIn.name() + ", " + Transaction.QueueType.Special.name());
        }
        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());

        if (shop == null) {
            throw new BlazeInvalidArgException("Shop", "Shop does not exist.");
        }

        // Check if employee is valid
        Employee employee = null;
        if (StringUtils.isNotEmpty(request.getEmployeeId())) {
            employee = employeeRepository.get(token.getCompanyId(), request.getEmployeeId());
            if (employee == null || employee.isDeleted()) {
                throw new BlazeInvalidArgException("Employee", "Employee does not exist.");
            }

            if (employee.isDisabled()) {
                throw new BlazeInvalidArgException("Employee", "Employee is inactive.");
            }

            if (request.isCreateTookanTask() && shop.isEnableTookan()) {
                this.createTookanTask(null, employee, member, request.getTookanTeamId(), false, false, queueType);
            }

        }


        //check member age limit
        int memberAge = 0;
        if (member.getDob() != null) {
            memberAge = DateUtil.getYearsBetweenTwoDates(member.getDob(), DateTime.now().getMillis());
        }

        if (!member.isAnonymous()) {
            if (shop.isEnableMedicinalAge() && (ConsumerType.MedicinalThirdParty.equals(member.getConsumerType()) || ConsumerType.MedicinalState.equals(member.getConsumerType()))) {
                int medicalAge = shop.getMedicinalAge();
                if (medicalAge > memberAge) {
                    throw new BlazeInvalidArgException("Member", String.format("Member does not meet minimum age of %d.", shop.getMedicinalAge()));
                }
            }

            if (shop.isEnableAgeLimit() && ConsumerType.AdultUse.equals(member.getConsumerType())) {
                int adultAge = shop.getAgeLimit();
                if (adultAge > memberAge) {
                    throw new BlazeInvalidArgException("Member", String.format("Member does not meet minimum age of %d.", shop.getAgeLimit()));
                }
            }
        }


        if (shop.isRequireValidRecDate() && member.getConsumerType() != ConsumerType.AdultUse) {
            //TODO: FOR THE TIME BEING, THIS IS ENABLED FOR ALL. ENABLE THIS ONLY FOR MEDICINAL
            // check member rec
            if (!member.hasValidRecommendation()) {
                throw new BlazeInvalidArgException("Recommendation", "Member/patient is missing a recommendation or it has been expired.");
            }

        }

        long activeTransCount = transactionRepository.getActiveTransactionCount(token.getShopId(), member.getId());
        if (shop.getNumAllowActiveTrans() > activeTransCount) {

            //Create Sequence
            if (ticketNumber < 0) {
                UniqueSequence sequence = uniqueSequenceRepository.getNewIdentifier(token.getCompanyId(), token.getShopId(), Transaction.UNIQUE_SEQ_PRIORITY);
                ticketNumber = sequence.getCount();

            }
            //Start Transaction
            Transaction trans = new Transaction();
            trans.prepare(token.getCompanyId());
            trans.setActive(true);
            trans.setTransNo("" + ticketNumber);
            trans.setMemberId(request.getMemberId());
            trans.setShopId(token.getShopId());
            trans.setQueueType(queueType);
            trans.setMemberId(member.getId());
            trans.setCheckinTime(DateTime.now().getMillis());
            trans.setSellerId(token.getActiveTopUser().getUserId());
            trans.setCreatedById(token.getActiveTopUser().getUserId());
            trans.setPriority(ticketNumber);
            trans.setOrderTags(request.getOrderTags());

            if (trans.getOrderTags() == null) {
                trans.setOrderTags(new HashSet<String>());
            }

            //Create Cart
            Cart cart = new Cart();
            cart.prepare(token.getCompanyId());

            trans.setCart(cart);
            trans.setTransType(Transaction.TransactionType.Sale);
            trans.setTimeZone(token.getRequestTimeZone());
            trans.setAssignedEmployeeId(request.getEmployeeId());
            trans.setAssigned((StringUtils.isNotBlank(request.getEmployeeId())));
            trans.setFulfillmentStep(Transaction.FulfillmentStep.Prepare);
            trans.setMemo(request.getMemo());


            // create order tags
            Set<String> orderTags = trans.getOrderTags();
            trans.setCompleteAfter(request.getCompleteAfter());
            if (trans.getQueueType() == Transaction.QueueType.Delivery) {
                orderTags.add(trans.getQueueType().name());
                if (request.getDeliveryDate() != null) {
                    trans.setDeliveryDate(request.getDeliveryDate());
                }
            } else {
                trans.setPickUpDate(request.getPickUpDate());
            }

            if (member != null) {
                if (member.getAddress() != null) {
                    Address address = member.getAddress();
                    address.resetPrepare(token.getCompanyId());
                    trans.setDeliveryAddress(address);
                }
                // check verified
                if (member.getIdentifications() != null) {
                    for (Identification identification : member.getIdentifications()) {
                        if (identification != null) {
                            if (identification.isVerified()) {
                                orderTags.add(Identification.VERIFY_TAG);
                                break;
                            }
                        }
                    }
                }

                // check verified
                if (member.getRecommendations() != null) {
                    for (Recommendation recommendation : member.getRecommendations()) {
                        if (recommendation != null) {
                            if (recommendation.isVerified()) {
                                orderTags.add(Recommendation.VERIFY_TAG);
                                break;
                            }
                        }
                    }
                }
            }
            trans.setOrderTags(orderTags);

            //Get Seller Id
            String sellerTerminalId = getSellerTerminalId(shop, trans.getAssignedEmployeeId());
            Terminal sellerTerminal = terminalRepository.getTerminalById(token.getCompanyId(), sellerTerminalId);

            trans.setTerminalId(sellerTerminalId);
            trans.setCheckoutType((sellerTerminal != null && sellerTerminal.getCheckoutType() != null) ? sellerTerminal.getCheckoutType() : shop.getCheckoutType());


            //Member Info
            final CustomerInfo customerInfo = getMemberGroup(trans.getMemberId());
            cartService.prepareCart(shop, trans, false, Transaction.TransactionStatus.Hold, false, customerInfo, true);


            if (trans.getQueueType() == Transaction.QueueType.Delivery) {
                if (shop != null) {
                    boolean enabledDelivery = false;
                    for (DeliveryFee deliveryFee : shop.getDeliveryFees()) {
                        if (deliveryFee.isEnabled()) {
                            enabledDelivery = true;
                            break;
                        }
                    }
                    cart.setEnableDeliveryFee(enabledDelivery);
                    // Always start with 0
                    cart.setDeliveryFee(new BigDecimal(0));
                }
            }
            trans.setPreparedBy(token.getActiveTopUser().getUserId());
            trans.setCreateTookanTask(request.isCreateTookanTask());

            final Transaction dbTransaction = transactionRepository.save(trans);

            if (request.getCreateOnFleetTask() && shop.isEnableOnFleet()) {
                this.createTaskAtOnFleet(shop, employee, trans, request.getOnFleetTeamId(), member);
            }
            if (request.isCreateTookanTask() && shop.isEnableTookan()) {
                this.createTookanTask(trans, employee, member, request.getTookanTeamId(), false, true, queueType);
            }

            // ElasticSearch prereqs and upload
            dbTransaction.setMember(memberRepository.get(token.getCompanyId(), dbTransaction.getMemberId()));
            dbTransaction.setSeller(employeeRepository.get(token.getCompanyId(), dbTransaction.getSellerId()));
            elasticSearchManager.createOrUpdateIndexedDocument(dbTransaction);

            if (dbTransaction != null && employee != null) {
                if (employee.getAssignedTerminalId() != null) {
                    Terminal terminal = terminalRepository.get(employee.getCompanyId(), employee.getAssignedTerminalId());
                    if (terminal != null) {
                        String message = "You have been assigned to a member in " + queueType + "." + "Order Number : #" + dbTransaction.getTransNo();
                        String title = "New Order";
                            pushNotificationManager.sendPushNotification(message, Lists.newArrayList(terminal), 0, FcmPayload.Type.TASK, dbTransaction.getId(), dbTransaction.getTransNo(), title, FcmPayload.SubType.NEW_ORDER);
                    }
                }
            }
            if (StringUtils.isNotBlank(request.getKioskCheckInId())) {
                checkInRepository.updateCheckInState(request.getKioskCheckInId(), false, CheckIn.CheckInStatus.AddedToQueue, dbTransaction.getId());
            }

            memberRepository.updateModified(member.getId());

            realtimeService.sendRealTimeEvent(token.getShopId(), RealtimeService.RealtimeEventType.MembersUpdateEvent, null);
            realtimeService.sendRealTimeEvent(token.getShopId(), RealtimeService.RealtimeEventType.QueueUpdateEvent, null);
            return dbTransaction;
        } else {
            if (shop.getNumAllowActiveTrans() > 1) {
                throw new BlazeInvalidArgException("Transaction", String.format("Member has %s active orders in queue. Please complete current order(s) before accepting new one.", shop.getNumAllowActiveTrans()));
            } else {
                throw new BlazeInvalidArgException("Transaction", "Member is already in the queue.");
            }
        }
    }


    // Transactions
    @Override
    public Transaction addToQueue(String transID, String queueName, QueueAddMemberRequest request) {

        if (queueName == null) {
            throw new BlazeInvalidArgException("QueueName", "Please specify a queue name.");
        }

        Member member = memberRepository.get(token.getCompanyId(), request.getMemberId());
        if (member == null) {
            throw new BlazeInvalidArgException("Member", "Member does not exist.");
        }

        if (member.getStatus() == Member.MembershipStatus.Inactive) {
            throw new BlazeInvalidArgException("Member", "Cannot add inactive member to queue.");
        }

        Transaction.QueueType queueType = Transaction.QueueType.None;
        try {
            queueType = Transaction.QueueType.valueOf(queueName);
        } catch (Exception e) {
            throw new BlazeInvalidArgException("Queue name", "Invalid queue name. Must be of: " + Transaction.QueueType.Online.name() + ", " + Transaction.QueueType.Delivery.name() + ", " + Transaction.QueueType.WalkIn.name() + ", " + Transaction.QueueType.Special.name());
        }

        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());

        if (shop == null) {
            throw new BlazeInvalidArgException("Shop", "Shop does not exist.");
        }

        // Check if employee is valid
        Employee employee = null;
        if (StringUtils.isNotEmpty(request.getEmployeeId())) {
            employee = employeeRepository.get(token.getCompanyId(), request.getEmployeeId());
            if (employee == null || employee.isDeleted()) {
                throw new BlazeInvalidArgException("Employee", "Employee does not exist.");
            }

            if (employee.isDisabled()) {
                throw new BlazeInvalidArgException("Employee", "Employee is inactive.");
            }

            if (request.isCreateTookanTask() && shop.isEnableTookan()) {
                this.createTookanTask(null, employee, member, request.getTookanTeamId(), false, false, queueType);
            }

        }

        if (shop.isRequireValidRecDate() && member.isMedical()) {
            //TODO: FOR THE TIME BEING, THIS IS ENABLED FOR ALL. ENABLE THIS ONLY FOR MEDICINAL
            // check member rec
            if (!member.hasValidRecommendation()) {
                throw new BlazeInvalidArgException("Recommendation", "Member/patient is missing a recommendation or it has been expired.");
            }

        }

        long activeTransCount = transactionRepository.getActiveTransactionCount(token.getShopId(), member.getId());
        if (shop.getNumAllowActiveTrans() > activeTransCount) {

            //Create Sequence
            UniqueSequence sequence = uniqueSequenceRepository.getNewIdentifier(token.getCompanyId(), token.getShopId(), "TransactionPriority");

            //Start Transaction
            Transaction trans = new Transaction();
            trans.setId(transID);
            trans.prepare(token.getCompanyId());
            trans.setActive(true);
            trans.setTransNo("" + sequence.getCount());
            trans.setMemberId(request.getMemberId());
            trans.setShopId(token.getShopId());
            trans.setQueueType(queueType);
            trans.setMemberId(member.getId());
            trans.setCheckinTime(DateTime.now().getMillis());
            trans.setSellerId(token.getActiveTopUser().getUserId());
            trans.setCreatedById(token.getActiveTopUser().getUserId());

            // create order tags
            Set<String> orderTags = trans.getOrderTags();
            if (orderTags == null) {
                orderTags = new HashSet<>();
            }
            if (trans.getQueueType() == Transaction.QueueType.Delivery) {
                orderTags.add(trans.getQueueType().name());
            }

            if (member != null) {
                if (member.getAddress() != null) {
                    Address address = member.getAddress();
                    address.resetPrepare(token.getCompanyId());
                    trans.setDeliveryAddress(address);
                }
                // check verified
                if (member.getIdentifications() != null) {
                    for (Identification identification : member.getIdentifications()) {
                        if (identification != null) {
                            if (identification.isVerified()) {
                                orderTags.add("ID Verified");
                                break;
                            }
                        }
                    }
                }

                // check verified
                if (member.getRecommendations() != null) {
                    for (Recommendation recommendation : member.getRecommendations()) {
                        if (recommendation != null) {
                            if (recommendation.isVerified()) {
                                orderTags.add("REC Verified");
                                break;
                            }
                        }
                    }
                }
            }
            trans.setOrderTags(orderTags);

            Transaction dbTrans = transactionRepository.get(token.getCompanyId(), transID);
            if (dbTrans == null) {
                trans.setPriority(sequence.getCount());
            } else {
                trans.setPriority(dbTrans.getPriority());
            }

            //Create Cart
            Cart cart = new Cart();
            cart.prepare(token.getCompanyId());
            trans.setCart(cart);

            trans.setTransType(Transaction.TransactionType.Sale);
            trans.setTimeZone(token.getRequestTimeZone());
            trans.setAssignedEmployeeId(request.getEmployeeId());
            trans.setAssigned((StringUtils.isNotBlank(request.getEmployeeId())));
            trans.setFulfillmentStep(Transaction.FulfillmentStep.Prepare);
            trans.setMemo(request.getMemo());

            //Get Seller Id
            String sellerTerminalId = getSellerTerminalId(shop, trans.getAssignedEmployeeId());
            Terminal sellerTerminal = terminalRepository.getTerminalById(token.getCompanyId(),  sellerTerminalId);

            trans.setCheckoutType((sellerTerminal != null && sellerTerminal.getCheckoutType() != null) ? sellerTerminal.getCheckoutType() : shop.getCheckoutType());

            trans.setTerminalId(sellerTerminalId);

            //Member Info
            final CustomerInfo customerInfo = getMemberGroup(trans.getMemberId());

            //Prepare Cart
            cartService.prepareCart(shop, trans, false, Transaction.TransactionStatus.Hold, false, customerInfo, true);


            if (trans.getQueueType() == Transaction.QueueType.Delivery) {
                if (shop != null) {
                    boolean enabledDelivery = false;
                    for (DeliveryFee deliveryFee : shop.getDeliveryFees()) {
                        if (deliveryFee.isEnabled()) {
                            enabledDelivery = true;
                            break;
                        }
                    }
                    cart.setEnableDeliveryFee(enabledDelivery);
                    // Always start with 0
                    cart.setDeliveryFee(new BigDecimal(0));
                }
            }

            trans.setPreparedBy(token.getActiveTopUser().getUserId());

            if (request.getCreateOnFleetTask() && shop.isEnableOnFleet()) {
                this.createTaskAtOnFleet(shop, employee, trans, request.getOnFleetTeamId(), member);
            }
            if (request.isCreateTookanTask() && shop.isEnableTookan()) {
                trans.setCreateTookanTask(request.isCreateTookanTask());
                this.createTookanTask(trans, employee, member, request.getTookanTeamId(), false, true, queueType);
            }

            final Transaction dbTransaction = transactionRepository.save(trans);

            // ElasticSearch prereqs and upload
            dbTransaction.setMember(memberRepository.get(token.getCompanyId(), dbTransaction.getMemberId()));
            dbTransaction.setSeller(employeeRepository.get(token.getCompanyId(), dbTransaction.getSellerId()));
            elasticSearchManager.createOrUpdateIndexedDocument(dbTransaction);

            if (dbTransaction != null && employee != null) {
                if (employee.getAssignedTerminalId() != null) {
                    Terminal terminal = terminalRepository.get(employee.getCompanyId(), employee.getAssignedTerminalId());
                    if (terminal != null) {
                        String message = "You have been assigned to a member in " + queueType + "." + "Order Number : #" + dbTransaction.getTransNo();
                        String title = "New Order";
                            pushNotificationManager.sendPushNotification(message, Lists.newArrayList(terminal), 0, FcmPayload.Type.TASK, dbTransaction.getId(),dbTransaction.getTransNo(), title, FcmPayload.SubType.NEW_ORDER);
                    }
                }
            }

            memberRepository.updateModified(member.getId());

            realtimeService.sendRealTimeEvent(token.getShopId(), RealtimeService.RealtimeEventType.MembersUpdateEvent, null);
            realtimeService.sendRealTimeEvent(token.getShopId(), RealtimeService.RealtimeEventType.QueueUpdateEvent, null);

            return dbTransaction;
        } else {
            if (shop.getNumAllowActiveTrans() > 1) {
                throw new BlazeInvalidArgException("Transaction", String.format("Member has %s active orders in queue. Please complete current order(s) before accepting new one.", shop.getNumAllowActiveTrans()));
            } else {
                throw new BlazeInvalidArgException("Transaction", "Member is already in the queue.");
            }
        }
    }


    /**
     * Create an anonymous user and automatically add that user to the queue
     * @param queueName
     * @param request
     * @return
     */
    @Override
    public CompositeMemberTransResult addAnonymousUserToQueue(String queueName, AnonymousQueueAddRequest request) {
        UniqueSequence sequence = uniqueSequenceRepository.getNewIdentifier(token.getCompanyId(), token.getShopId(), Transaction.UNIQUE_SEQ_PRIORITY);
        // Add New Anonymous User
        Member newMember = new Member();
        newMember.prepare(token.getCompanyId());
        newMember.setConsumerType(request.getConsumerType());
        newMember.setFirstName("Anon-" + queueName);
        newMember.setLastName(sequence.getCount() + "");
        newMember.setAnonymous(true);
        newMember.setStatus(Member.MembershipStatus.Active);
        newMember.setDob(-2208987012000l);

        memberRepository.save(newMember);

        QueueAddMemberRequest queueAddMemberRequest = new QueueAddMemberRequest();
        queueAddMemberRequest.setMemberId(newMember.getId());
        queueAddMemberRequest.setEmployeeId(token.getActiveTopUser().getUserId());
        // Add member to queue
        Transaction transaction = this.addToQueue(queueName,queueAddMemberRequest,sequence.getCount());

        CompositeMemberTransResult result = new CompositeMemberTransResult();
        result.setMember(newMember);
        result.setTransaction(transaction);
        return result;
    }

    private void createTaskAtOnFleet(Shop shop, Employee employee, Transaction trans, String onFleetTeamId, Member member) {
        String employeeWorkerId = "";
        EmployeeOnFleetInfo employeeOnFleetInfo = onFleetService.getEmployeeOnfFleetInfoByShop(employee, trans.getShopId());
        if (employeeOnFleetInfo != null) {
            employeeWorkerId = employeeOnFleetInfo.getOnFleetWorkerId();
        }

        onFleetService.createOnFleetTask(shop, trans, employeeWorkerId, onFleetTeamId, employee, employeeOnFleetInfo, member, false);
    }

    @Override
    public Transaction getTransactionById(String transactionId) {
        Transaction trans = transactionRepository.get(token.getCompanyId(), transactionId);
        if (trans == null) {
            throw new BlazeInvalidArgException("Transaction", "Transaction does not exist for id " + transactionId);
        }
        // NOTE: SLOPPY WAY OF DOING THIS BUT IT'S CONVENIENT. HAHA.
        SearchResult<Transaction> result = new SearchResult<>();
        result.getValues().add(trans);

        assignDependentObjects(result, true, true);
        return trans;
    }

    @Override
    public Transaction getTransactionByTransNo(String transNo) {
        Transaction tran = transactionRepository.getTransactionByTransNo(token.getCompanyId(), token.getShopId(), transNo);
        if (tran == null) {
            throw new BlazeInvalidArgException("Transaction", "Transaction does not exist for #" + transNo);
        }
        // NOTE: SLOPPY WAY OF DOING THIS BUT IT'S CONVENIENT. HAHA.
        SearchResult<Transaction> result = new SearchResult<>();
        result.getValues().add(tran);

        assignDependentObjects(result, true, true);
        return tran;
    }

    @Override
    public SearchResult<Transaction> getTransactions(String queueName, String transNo, String transactionId, String memberId, boolean active, int start, int limit) {

        Transaction.QueueType queueType = Transaction.QueueType.None;
        try {
            queueType = Transaction.QueueType.valueOf(queueName);
        } catch (Exception e) {
            throw new BlazeInvalidArgException("Queue name", "Invalid queue name. Must be of: "
                    + Transaction.QueueType.Online.name() + ", " + Transaction.QueueType.Delivery.name() + ", " + Transaction.QueueType.WalkIn.name() + ", " + Transaction.QueueType.Special.name());
        }


        SearchResult<Transaction> trans;

        if (StringUtils.isNotBlank(transNo)) {
            trans = transactionRepository.findRecentTransactions(token.getCompanyId(), token.getShopId(), transNo);
        } else if (StringUtils.isNotEmpty(transactionId)) {
            trans = new SearchResult<>();
            Transaction transaction = transactionRepository.get(token.getCompanyId(), transactionId);
            if (transaction != null) {
                trans.getValues().add(transaction);
                trans.setTotal(1l);
            }

        } else if (StringUtils.isNotBlank(memberId)) {
            trans = transactionRepository.getTransactionsForMember(token.getCompanyId(), token.getShopId(), memberId, start, limit);
        } else if (queueType == Transaction.QueueType.None) {
            if (active) {
                if (roleService.canAccessRestricted(token.getCompanyId(), token.getShopId(), token.getActiveTopUser().getUserId())) {
                    trans = transactionRepository.getActiveTransactions(token.getCompanyId(), token.getShopId());
                } else {
                    trans = transactionRepository.getActiveTransactionsAssigned(token.getCompanyId(), token.getShopId(), token.getActiveTopUser().getUserId());
                }

            } else {
                if (roleService.canAccessRestricted(token.getCompanyId(), token.getShopId(), token.getActiveTopUser().getUserId())) {
                    trans = transactionRepository.getCompletedTransactions(token.getCompanyId(), token.getShopId(), start, limit);
                } else {
                    trans = transactionRepository.getCompletedTransactions(token.getCompanyId(), token.getShopId(), token.getActiveTopUser().getUserId(), start, limit);
                }
            }
        } else {
            if (active) {
                if (roleService.canAccessRestricted(token.getCompanyId(), token.getShopId(), token.getActiveTopUser().getUserId())) {
                    trans = transactionRepository.getActiveTransactionsForQueue(token.getCompanyId(), token.getShopId(), queueType);
                } else {
                    trans = transactionRepository.getActiveTransactionsForQueue(token.getCompanyId(), token.getShopId(), token.getActiveTopUser().getUserId(), queueType);
                }
            } else {
                if (roleService.canAccessRestricted(token.getCompanyId(), token.getShopId(), token.getActiveTopUser().getUserId())) {
                    trans = transactionRepository.getCompletedTransactionsForQueue(token.getCompanyId(), token.getShopId(), queueType, start, limit);
                } else {
                    trans = transactionRepository.getCompletedTransactionsForQueue(token.getCompanyId(), token.getShopId(), queueType, start, limit);
                }
            }
        }
        assignDependentObjects(trans, true, true);
        return trans;
    }

    @Override
    public SearchResult<Transaction> getCompletedTransactionsWithDates(String transNo, String transactionId, String memberId, String startDate, String endDate, int timezoneOffset) {

        SearchResult<Transaction> trans = null;

        // Parse Incoming Dates
        DateTimeFormatter formatter = DateTimeFormat.forPattern("MM/dd/yyyy");
        long startDateMillis = 0l;
        DateTime jodaEndDate = DateTime.now();

        if (startDate != null) {
            startDateMillis = formatter.parseDateTime(startDate).getMillis();
        }

        if (endDate != null) {
            jodaEndDate = formatter.parseDateTime(endDate);
            jodaEndDate = jodaEndDate.withTimeAtStartOfDay().plusDays(1).minusSeconds(1);
        }


        DateTime estartDate = new DateTime(startDateMillis);
        long timeZoneStartDateMillis = estartDate.withTimeAtStartOfDay().plusMinutes(timezoneOffset).getMillis();
        long timeZoneEndDateMillis = jodaEndDate.plusMinutes(timezoneOffset).getMillis();


        if (StringUtils.isNotEmpty(transNo)) {
            trans = transactionRepository.findRecentTransactions(token.getCompanyId(), token.getShopId(), transNo);
        } else if (StringUtils.isNotEmpty(transactionId)) {
            trans = new SearchResult<>();
            Transaction transaction = transactionRepository.get(token.getCompanyId(), transactionId);
            if (transaction != null) {
                trans.getValues().add(transaction);
                trans.setTotal(1l);
            }
        } else if (StringUtils.isNotEmpty(memberId)) {
            trans = transactionRepository.getTransactionsForMemberDates(token.getCompanyId(), token.getShopId(), memberId, timeZoneStartDateMillis, timeZoneEndDateMillis);
        } else {
            trans = transactionRepository.getCompletedTransactionsDates(token.getCompanyId(), token.getShopId(), timeZoneStartDateMillis, timeZoneEndDateMillis);
        }

        return trans;
    }

    @Override
    public DateSearchResult<Transaction> getTransactions(long afterDate, long beforeDate) {
        //if (beforeDate <= 0) {
        beforeDate = DateTime.now().getMillis();
        //}
        if (afterDate < 0) {
            afterDate = 0;
        }
        DateSearchResult<Transaction> trans = null;
        if (roleService.canAccessRestricted(token.getCompanyId(), token.getShopId(), token.getActiveTopUser().getUserId())) {
            DateTime week1 = DateTime.now().minusWeeks(1);
            if (afterDate < week1.getMillis()) {
                afterDate = week1.getMillis();
            }

            trans = transactionRepository.getSalesRefundsTransactions(token.getCompanyId(), token.getShopId(), afterDate);


            DateTime days1 = DateTime.now().minusDays(1);
            if (afterDate < days1.getMillis()) {
                // if afterDate is less than a day, get all active transactions as well (to make sure)

                Iterable<Transaction> transactions = transactionRepository.getAllActiveTransactions(token.getCompanyId(), token.getShopId());
                LinkedHashSet<Transaction> hashSet = new LinkedHashSet<>();
                hashSet.addAll(Lists.newArrayList(transactions));
                hashSet.addAll(trans.getValues());
                trans.setValues(Lists.newArrayList(hashSet));
            }
        } else {
            DateTime days10 = DateTime.now().minusDays(10);
            if (afterDate > days10.getMillis()) {
                // get all transactions past week
                Iterable<Transaction> transactions = transactionRepository.getAllActiveTransactionsForAssignedEmployee(token.getCompanyId(), token.getShopId(), token.getActiveTopUser().getUserId());

                trans = transactionRepository.getSalesTransactions(token.getCompanyId(), token.getShopId(),
                        afterDate, DateTime.now().getMillis());

                LinkedHashSet<Transaction> linkedHashSet = new LinkedHashSet<>();
                linkedHashSet.addAll(trans.getValues());
                for (Transaction tran : transactions) {
                    linkedHashSet.add(tran);
                }
                trans.setValues(Lists.newArrayList(linkedHashSet));
                trans.setTotal((long) trans.getValues().size());

            } else {
                // get all transactions past months
                DateTime after = DateTime.now().minusWeeks(2);
                trans = transactionRepository.getAllTransactionsAssigned(token.getCompanyId(), token.getShopId(), token.getActiveTopUser().getUserId(),
                        after.getMillis());
            }
        }

        assignDependentObjects(trans, true, false);
        return trans;
    }

    @Override
    public SearchResult<Transaction> findRecentTransactions(String transNo) {
        SearchResult<Transaction> trans = transactionRepository.findRecentTransactions(token.getCompanyId(), token.getShopId(), transNo);
        assignDependentObjects(trans, false, false);
        return trans;
    }


    private void assignDependentObjects(SearchResult<Transaction> transactions, boolean assignedProducts, boolean assignEmployees) {
        Set<ObjectId> membersIds = new HashSet<>();
        Set<ObjectId> productIds = new HashSet<>();
        Set<ObjectId> employeeIds = new HashSet<>();
        Set<String> productWithPrepackages = new HashSet<>();

        for (Transaction transaction : transactions.getValues()) {
            if (StringUtils.isNotBlank(transaction.getMemberId()) && ObjectId.isValid(transaction.getMemberId())) {
                membersIds.add(new ObjectId(transaction.getMemberId()));
            } else {
                LOG.info("Invalid member: trans - " + transaction.getId());
            }
            if (assignEmployees && StringUtils.isNotBlank(transaction.getSellerId()) && ObjectId.isValid(transaction.getSellerId())) {
                employeeIds.add(new ObjectId(transaction.getSellerId()));
            } else {
                LOG.info("Invalid sellerId: trans - " + transaction.getId());
            }
            if (StringUtils.isNotEmpty(transaction.getAssignedEmployeeId()) && ObjectId.isValid(transaction.getAssignedEmployeeId())) {
                employeeIds.add(new ObjectId(transaction.getAssignedEmployeeId()));
            }
            if (assignedProducts) {
                for (OrderItem item : transaction.getCart().getItems()) {
                    productIds.add(new ObjectId(item.getProductId()));

                    if (StringUtils.isNotEmpty(item.getPrepackageItemId())
                            && ObjectId.isValid(item.getPrepackageItemId())) {
                        productWithPrepackages.add(item.getProductId());
                    }
                }
            }
        }

        HashMap<String, Member> memberHashMap = memberRepository.findItemsInAsMap(token.getCompanyId(), Lists.newArrayList(membersIds));
        HashMap<String, Product> productHashMap = new HashMap<>();
        HashMap<String, Employee> employeeHashMap = new HashMap<>();
        HashMap<String, Prepackage> prepackageHashMap = new HashMap<>();
        HashMap<String, PrepackageProductItem> prepackageProductItemHashMap = new HashMap<>();
        HashMap<String, ProductWeightTolerance> weightToleranceHashMap = weightToleranceRepository.listAsMap(token.getCompanyId());

        HashMap<String, Terminal> terminalHashMap = terminalRepository.listAllAsMap(token.getCompanyId(), token.getShopId());
        HashMap<String, ProductCategory> categoryHashMap = productCategoryRepository.listAllAsMap(token.getCompanyId(), token.getShopId());
        HashMap<String, Brand> brandHashMap = brandRepository.listAllAsMap(token.getCompanyId());

        if (assignedProducts) {
            productHashMap = productRepository.findItemsInAsMap(token.getCompanyId(), token.getShopId(), Lists.newArrayList(productIds));

            List<String> pids = Lists.newArrayList(productWithPrepackages);
            prepackageHashMap = prepackageRepository.getPrepackagesForProductsAsMap(token.getCompanyId(), token.getShopId(), pids);
            prepackageProductItemHashMap = prepackageProductItemRepository.getPrepackagesForProductsAsMap(token.getCompanyId(), token.getShopId(), pids);
        }
        if (assignEmployees) {
            employeeHashMap = employeeRepository.findItemsInAsMap(token.getCompanyId(), Lists.newArrayList(employeeIds));
        }

        for (Transaction transaction : transactions.getValues()) {
            if (StringUtils.isNotEmpty(transaction.getMemberId())) {
                transaction.setMember(memberHashMap.get(transaction.getMemberId()));
            }
            if (assignedProducts) {
                for (OrderItem item : transaction.getCart().getItems()) {
                    item.setProduct(productHashMap.get(item.getProductId()));
                    if (item.getProduct() != null) {
                        item.getProduct().setCategory(categoryHashMap.get(item.getProduct().getCategoryId()));
                        item.getProduct().setBrand(brandHashMap.get(item.getProduct().getBrandId()));
                    }

                    if (StringUtils.isNotEmpty(item.getPrepackageItemId())) {
                        PrepackageProductItem prepackageProductItem = prepackageProductItemHashMap.get(item.getPrepackageItemId());
                        item.setPrepackageProductItem(prepackageProductItem);
                        if (prepackageProductItem != null) {
                            Prepackage prepackage = prepackageHashMap.get(prepackageProductItem.getPrepackageId());
                            item.setPrepackage(prepackage);
                            item.setPrepackageProductItem(prepackageProductItem);

                            if (prepackage != null) {
                                ProductWeightTolerance weightTolerance = weightToleranceHashMap.get(prepackage.getToleranceId());
                                if (weightTolerance != null) {
                                    prepackage.setName(weightTolerance.getName());
                                    prepackage.setUnitValue(weightTolerance.getUnitValue());
                                }
                            }
                        }
                    }
                }
            }
            if (assignEmployees) {
                Employee employee = employeeHashMap.get(transaction.getSellerId());
                if (employee != null) {
                    employee.setPassword(null);
                    employee.setPin(null);
                    transaction.setSeller(employee);
                }


                Employee assignedEmp = employeeHashMap.get(transaction.getAssignedEmployeeId());
                if (assignedEmp != null) {
                    assignedEmp.setPassword(null);
                    assignedEmp.setPin(null);
                    transaction.setAssignedEmployee(assignedEmp);
                }
            }
            // assign terminals
            transaction.setSellerTerminal(terminalHashMap.get(transaction.getSellerTerminalId()));
            transaction.setAssigned(StringUtils.isNotBlank(transaction.getAssignedEmployeeId()));
        }

    }

    private String getSellerTerminalId(Shop shop, String assignedEmployeeId) {
        String sellerTerminalId = token.getTerminalId();

        if (shop != null) {
            Employee employee = null;
            if (shop.getTermSalesOption() == Shop.TerminalSalesOption.AssignedEmployeeTerminal) {
                employee = employeeRepository.get(token.getCompanyId(), assignedEmployeeId);
            } else if (shop.getTermSalesOption() == Shop.TerminalSalesOption.SellerAssignedTerminal) {
                employee = employeeRepository.get(token.getCompanyId(), token.getActiveTopUser().getUserId());
            }

            if (employee != null && StringUtils.isNotBlank(employee.getAssignedTerminalId())) {
                return employee.getAssignedTerminalId();
            }
        }
        return sellerTerminalId;
    }

    @Override
    public Transaction signTransaction(String transactionId, InputStream inputStream, String fileName, String mimeType) {
        if (inputStream == null) {
            throw new BlazeInvalidArgException("TransactionId", "Invalid signature.");
        }

        Transaction dbTrans = transactionRepository.get(token.getCompanyId(), transactionId);
        if (dbTrans == null) {
            throw new BlazeInvalidArgException("TransactionId", "Transaction does not exist.");
        }

        CompanyAsset asset = assetService.uploadAssetPublic(inputStream, fileName, Asset.AssetType.Photo, mimeType);
        dbTrans.setMemberSignature(asset);

        transactionRepository.update(token.getCompanyId(), dbTrans.getId(), dbTrans);
        realtimeService.sendRealTimeEvent(token.getShopId(), RealtimeService.RealtimeEventType.QueueUpdateEvent, null);
        return dbTrans;
    }

    @Override
    public void deleteFromQueue(String transactionId, TransactionDeleteRequest deleteRequest) {
        if (StringUtils.isBlank(transactionId)) {
            throw new BlazeInvalidArgException("TransactionId", "Invalid transactionId");
        }
        Transaction dbTransaction = transactionRepository.get(token.getCompanyId(), transactionId);
        if (dbTransaction == null) {
            throw new BlazeInvalidArgException("TransactionId", "Invalid transactionId");
        }
        if (dbTransaction.isActive()) {
            Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());

            if (deleteRequest != null) {
                Note note = new Note();
                note.prepare();
                note.setMessage(deleteRequest.getReason());
                note.setWriterId(token.getActiveTopUser().getUserId());
                note.setWriterName(token.getActiveTopUser().getFirstName() + " " + token.getActiveTopUser().getLastName());
                dbTransaction.setDeleteNote(note);
            }

            // add some simple checks
            // only check if it's not empty
            if (dbTransaction.getCart() != null && !dbTransaction.getCart().getItems().isEmpty()) {
                if (dbTransaction.getQueueType() == Transaction.QueueType.WalkIn) {
                    if (dbTransaction.getStatus() == Transaction.TransactionStatus.InProgress) {
                        LOG.info("Transaction is in progress. Please put on hold before canceling.");
                        throw new BlazeInvalidArgException("Transaction", "Transaction is in progress. Please put on hold before canceling.");
                    } else if (dbTransaction.getStatus() == Transaction.TransactionStatus.Completed) {
                        dbTransaction.setActive(false);
                        LOG.info("Transaction is already completed.");
                        transactionRepository.deactivate(token.getCompanyId(), dbTransaction.getId());
                        realtimeService.sendRealTimeEvent(token.getShopId(), RealtimeService.RealtimeEventType.QueueUpdateEvent, null);
                        return;
                    }
                }
            }

            boolean isAssigned = dbTransaction.isAssigned();
            if (isAssigned && dbTransaction.getStatus() == Transaction.TransactionStatus.InProgress) {
                LOG.info("Transaction is in progress. Please put on hold or claim before canceling.");
                Employee employee = employeeRepository.get(token.getCompanyId(),dbTransaction.getSellerId());
                if (employee != null) {
                    throw new BlazeInvalidArgException("Transaction",
                            String.format("Transaction is in progress by '%s %s'. Please put on hold or claim before canceling.",
                                    employee.getFirstName(), employee.getLastName()));
                } else {
                    throw new BlazeInvalidArgException("Transaction", "Transaction is in progress. Please put on hold or claim before canceling.");
                }
            }

            if (dbTransaction.isPreparingFulfillment()) {
                dbTransaction.setActive(false);
                dbTransaction.setStatus(Transaction.TransactionStatus.Canceled);
                dbTransaction.setProcessedTime(DateTime.now().getMillis());
                dbTransaction.setCompletedTime(DateTime.now().getMillis());
                dbTransaction.setSellerId(token.getActiveTopUser().getUserId());
                // clear and then update
                transactionRepository.update(token.getCompanyId(), transactionId, dbTransaction);
                realtimeService.sendRealTimeEvent(token.getShopId(), RealtimeService.RealtimeEventType.QueueUpdateEvent, null);
            } else {


                dbTransaction.setActive(false);
                dbTransaction.setProcessedTime(DateTime.now().getMillis());
                dbTransaction.setCompletedTime(DateTime.now().getMillis());
                // update first
                transactionRepository.update(token.getCompanyId(), transactionId, dbTransaction);
                // clear and then send to queue transaction to deduct amt
                //dbTransaction.getCart().getItems().clear();


                queueTransactionJob(transactionId, dbTransaction, Transaction.TransactionStatus.Canceled, shop);
            }

            if (StringUtils.isNotBlank(dbTransaction.getConsumerCartId())) {
                ConsumerCart consumerCart = consumerCartRepository.get(token.getCompanyId(), dbTransaction.getConsumerCartId());
                consumerCart.setCartStatus(ConsumerCart.ConsumerCartStatus.CanceledByDispensary);
                consumerCart.setTrackingStatus(ConsumerCart.ConsumerTrackingStatus.Declined);
                consumerCartRepository.update(token.getCompanyId(), consumerCart.getId(), consumerCart);
                updateConsumerOrderWebHook(consumerCart);
            }

            if (dbTransaction.isCreateTookanTask() && StringUtils.isNotBlank(dbTransaction.getTookanTaskId())) {
                tookanService.updateTaskStatus(token.getCompanyId(), token.getShopId(), dbTransaction, TookanTaskResult.TookanTaskStatus.CANCEL);
            }

            if (dbTransaction.getCreateOnfleetTask() && StringUtils.isNotBlank(dbTransaction.getOnFleetTaskId())) {
                this.deleteOrMarkFailedTaskAtOnfleet(shop, dbTransaction);
            }

            // delete member if anonymous
            Member member = memberRepository.get(token.getCompanyId(),dbTransaction.getMemberId());
            if (member != null && member.isAnonymous()) {
                memberRepository.removeById(token.getCompanyId(),member.getId());
            }
        }
    }

    private void deleteOrMarkFailedTaskAtOnfleet(Shop shop, Transaction transaction) {
        onFleetService.deleteOrMarkFailedTaskAtOnfleet(shop, transaction);
    }


    @Override
    public Transaction completeTransaction(String transactionId, Transaction transaction, boolean avoidCannabis, boolean fromBulk) {
        Transaction dbTrans = transactionRepository.get(token.getCompanyId(), transactionId);
        if (dbTrans == null) {
            throw new BlazeInvalidArgException("TransactionId", "Transaction does not exist.");
        }
        Member member = null;
        if (StringUtils.isNotBlank(dbTrans.getMemberId())) {
            member = memberRepository.get(token.getCompanyId(), dbTrans.getMemberId());
            if (member == null) {
                throw new BlazeInvalidArgException("Member", "Member does not found");
            }

            if (member.isBanPatient()) {
                throw new BlazeInvalidArgException("Transaction", "Transaction can not be completed because member is banned");
            }
        }

        if (dbTrans.isPreparingFulfillment()) {
            throw new BlazeInvalidArgException("Transaction", "This transaction is currently in preparing mode.");
        }

        if (dbTrans.isLocked() && this.didLockedOrderChange(dbTrans, transaction)) {
            throw new BlazeInvalidArgException(TRANSACTION, TRANSACTION_LOCKED);
        }

        if (transaction.getCart() != null && transaction.getCart().getPaymentOption().equals(Cart.PaymentOption.Split)) {
            transaction.getCart().setPaymentType(Cart.PaymentType.Split);

            if (transaction.getCart().getSplitPayment() == null) {
                throw new BlazeInvalidArgException("Transaction", SPLIT_PAYMENT_NOT_FOUND);
            }
        }

        if (transaction.getCart() != null && transaction.getCart().getPaymentOption() == Cart.PaymentOption.None) {
            throw new BlazeInvalidArgException("Transaction", "Please choose a valid payment option.");
        }

        if (dbTrans.isActive() == false) {
            // if it was canceled && there's a note, then just returned the canceled version
            // otherwise, let this continue to process
            if (!fromBulk) {
                // if incoming is from bulk, let's process it
                // otherwise, check if canceled
                if (dbTrans.getStatus() == Transaction.TransactionStatus.Canceled) {
                    if (dbTrans.getDeleteNote() != null && StringUtils.isNotBlank(dbTrans.getDeleteNote().getMessage())) {
                        // there was a reason it was canceled, so let's returned it
                        return dbTrans;
                    }
                } else {
                    return dbTrans;
                }
            }
        }

        if (dbTrans.getStatus() == Transaction.TransactionStatus.Completed) {
            return dbTrans;
        }

        if (transaction.getCart().getItems() == null || transaction.getCart().getItems().isEmpty()) {
            throw new BlazeInvalidArgException("Transaction", "Your cart is empty. Please try again.");
        }

        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());

        if (shop.isEnableCannabisLimit() && !avoidCannabis) {
            boolean status = cannabisLimitService.checkCannabisLimit(token.getCompanyId(), shop, dbTrans.getMemberId(), transaction.getCart(), null, false);
            if (!status) {
                throw new BlazeInvalidArgException("CannabisLimit", "State Cannabis Limit is reached.");
            }
        }

        String sellerTerminalId = getSellerTerminalId(shop, dbTrans.getAssignedEmployeeId());

        Terminal sellerTerminal = terminalRepository.getTerminalById(token.getCompanyId(), sellerTerminalId);
        if (sellerTerminal == null || sellerTerminal.isDeleted() || sellerTerminal.isActive() == false) {

            throw new BlazeInvalidArgException("Terminal",
                    String.format("Terminal '%s' is no longer active.",
                            sellerTerminal == null ? "" : sellerTerminal.getName()));
        }

        // set override inventory
        transaction.setOverrideInventoryId(dbTrans.getOverrideInventoryId());
        // Check Available
        checkAvailableInventory(transaction, dbTrans, "complete", null, sellerTerminalId, Boolean.FALSE);

        checkCashDrawerAvailability(shop, sellerTerminalId, sellerTerminal);

        dbTrans.setSellerId(token.getActiveTopUser().getUserId());
        dbTrans.setSellerTerminalId(sellerTerminalId);
        if (dbTrans.getStartTime() == null || dbTrans.getStartTime() == 0) {
            if (transaction.getStartTime() == null || transaction.getStartTime() <= 0) {
                dbTrans.setStartTime(dbTrans.getCreated());
            } else {
                dbTrans.setStartTime(transaction.getStartTime());
            }
        }
        dbTrans.setEndTime(DateTime.now().getMillis());
        dbTrans.setStatus(Transaction.TransactionStatus.Completed);
        dbTrans.setLoc(transaction.getLoc());
        dbTrans.setActive(false);
        dbTrans.setTimeZone(token.getRequestTimeZone());
        dbTrans.setRouting(false);

        if ((dbTrans.getDeliveryAddress() == null || !dbTrans.getDeliveryAddress().isValid())
                && member != null && member.getAddress() != null) {
            Address address = member.getAddress();
            address.resetPrepare(token.getCompanyId());
            dbTrans.setDeliveryAddress(address);
        }

        if (StringUtils.isBlank(dbTrans.getAssignedEmployeeId())) {
            dbTrans.setAssignedEmployeeId(token.getActiveTopUser().getUserId());
        }
        dbTrans.setAssigned(true);

        if (dbTrans.getStartRouteDate() == null || dbTrans.getStartRouteDate() == 0) {
            dbTrans.setStartRouteDate(dbTrans.getCreated());
        }
        if (dbTrans.getEndRouteDate() == null || dbTrans.getEndRouteDate() == 0) {
            dbTrans.setEndRouteDate(DateTime.now().getMillis());
        }

        if (transaction.getMemberSignature() != null) {
            dbTrans.setMemberSignature(transaction.getMemberSignature());
        }

        if (dbTrans.isPaid() == false) {
            if (transaction.getProcessedTime() != null && transaction.getProcessedTime() != 0) {
                dbTrans.setProcessedTime(transaction.getProcessedTime());
            } else {
                dbTrans.setProcessedTime(DateTime.now().getMillis());
            }
            // set completed time
            dbTrans.setCompletedTime(dbTrans.getProcessedTime());
        } else {
            // set completed time if it's paid, leave processedTime alone
            if (transaction.getProcessedTime() != null && transaction.getProcessedTime() != 0) {
                dbTrans.setCompletedTime(transaction.getProcessedTime());
            } else {
                dbTrans.setCompletedTime(DateTime.now().getMillis());
            }
        }

        dbTrans.setPaid(true);

        // create new note
        if (transaction.getNote() != null && StringUtils.isNotEmpty(transaction.getNote().getMessage())) {
            Note note = new Note();
            note.setId(ObjectId.get().toString());
            note.setMessage(transaction.getNote().getMessage());
            dbTrans.setNote(note);
        }


        final CustomerInfo customerInfo = getMemberGroup(dbTrans.getMemberId());
        try {
            cartService.prepareCart(shop, dbTrans, false, Transaction.TransactionStatus.InProgress, false, customerInfo, false); // current transaction is on hold
        } catch (Exception ex) {
            LOG.warn("Error in dbTransaction no " + dbTrans.getTransNo() + " : " + ex.getMessage());
        }
        cartService.prepareCart(shop, transaction, false, Transaction.TransactionStatus.Completed, false, customerInfo, false); // new transaction is completed

        if (dbTrans.getQueueType() == Transaction.QueueType.Delivery) {
            double mileage = deliveryService.calculateMileageByTransaction(transaction);
            dbTrans.setMileage(new BigDecimal(mileage));
        }
        dbTrans.setMileageCalculated(true);


        // Process Loyalty Card Payments (ie Linx, etc) [Need support for split payment?]
        if (transaction.isPayingWithPaymentCard()) {
            processPaymentOption(dbTrans, transaction);
        }
        if (StringUtils.isBlank(dbTrans.getPackedBy())) {
            dbTrans.setPackedBy(token.getActiveTopUser().getUserId()); // If it's empty then person who sold is packed it
            dbTrans.setPackedDate(new DateTime().getMillis());
        }


        transactionRepository.update(token.getCompanyId(), dbTrans.getId(), dbTrans);


        dbTrans.setMember(memberRepository.get(token.getCompanyId(), dbTrans.getMemberId()));
        if (transaction.getCart() != null) {
            dbTrans.setCart(transaction.getCart());
        }

        // if we're completing, make sure the final transaction has everything fulfilled
        for (OrderItem item : transaction.getCart().getItems()) {
            item.setFulfilled(true);
        }

        queueTransactionJob(transactionId, transaction, Transaction.TransactionStatus.Completed, shop);

        updateCorrespondingConsumerCart(shop, dbTrans, transaction, member);

        /*Company company = companyRepository.getById(token.getCompanyId());
        SpringBigInfo springBigInfo = springbigRepository.getInfoByShop(token.getCompanyId(), token.getShopId());
        if(company != null && company.isEnableSpringBig() && springBigInfo != null && springBigInfo.isActive()) {
            springBigManager.createVisit(transaction, member);
        }*/

        if (dbTrans.isCreateTookanTask() && StringUtils.isNotBlank(dbTrans.getTookanTaskId())) {
            tookanService.updateTaskStatus(token.getCompanyId(), token.getShopId(), dbTrans, TookanTaskResult.TookanTaskStatus.SUCCESSFUL);
        }

        if (dbTrans.getCreateOnfleetTask() && StringUtils.isNotBlank(dbTrans.getOnFleetTaskId())) {
            this.completeTaskAtOnfleet(shop, dbTrans, Boolean.TRUE);
        }
        if (fromBulk) {
            transactionRepository.update(token.getCompanyId(), dbTrans.getId(), dbTrans);
        }

        if (member != null) {
            if (member.isAnonymous()) {
                // delete anonymous user
                memberRepository.removeById(token.getCompanyId(), member.getId());
            }
        }
        return dbTrans;
    }

    /**
     * Complete transaction at onfleet
     * @param shop: shop object
     * @param trans: transaction
     */
    private void completeTaskAtOnfleet(Shop shop, Transaction trans, boolean taskStatus) {
        onFleetService.completeTaskAtOnFleet(shop, trans, taskStatus);
    }

    @Override
    public Transaction holdTransaction(String transactionId, Transaction incomingTrans) {
        Transaction dbTrans = transactionRepository.get(token.getCompanyId(), transactionId);
        if (dbTrans == null) {
            throw new BlazeInvalidArgException("TransactionId", "Transaction does not exist.");
        }
        if (!dbTrans.isActive()) {
            return dbTrans;
        }
        if (dbTrans.getStatus() == Transaction.TransactionStatus.Completed) {
            dbTrans.setActive(false);
            transactionRepository.deactivate(token.getCompanyId(), dbTrans.getId());
            return dbTrans;
        }

        if (dbTrans.isFulfillingFulfillment()) {
            if (incomingTrans.getCart().getItems().size() == 0
                    && dbTrans.getCart().getItems().size() > 0) {
                return dbTrans;
            }
        }

        if (dbTrans.isLocked() && this.didLockedOrderChange(dbTrans, incomingTrans)) {
            throw new BlazeInvalidArgException(TRANSACTION, TRANSACTION_LOCKED);
        }

        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());

        String sellerTerminalId = getSellerTerminalId(shop, dbTrans.getAssignedEmployeeId());

        // set override inventory
        incomingTrans.setOverrideInventoryId(dbTrans.getOverrideInventoryId());

        if (!incomingTrans.isPreparingFulfillment()) {
            checkAvailableInventory(incomingTrans, dbTrans, "hold", null, sellerTerminalId, Boolean.FALSE);
        }
        // create new note
        if (incomingTrans.getNote() != null && StringUtils.isNotEmpty(incomingTrans.getNote().getMessage())) {
            Note note = new Note();
            note.setId(ObjectId.get().toString());
            note.setMessage(incomingTrans.getNote().getMessage());
            dbTrans.setNote(note);
        }


        Transaction.TransactionStatus newStatus = Transaction.TransactionStatus.Hold;
        if (incomingTrans.getCart().getItems().size() == 0) {
            newStatus = Transaction.TransactionStatus.Queued; // Go back to the queue
            dbTrans.setStartTime(0L);
        }
        dbTrans.setSellerTerminalId(sellerTerminalId);
        dbTrans.setSellerId(token.getActiveTopUser().getUserId());
        dbTrans.setStatus(newStatus);
        dbTrans.setTimeZone(token.getRequestTimeZone());
        boolean isPickUp = dbTrans.getQueueType() == Transaction.QueueType.Special;
        if (isPickUp) {
            dbTrans.setPickUpDate(incomingTrans.getPickUpDate());
        } else {
            dbTrans.setDeliveryDate(incomingTrans.getDeliveryDate());
        }
        dbTrans.setCompleteAfter(incomingTrans.getCompleteAfter());

        final CustomerInfo customerInfo = getMemberGroup(dbTrans.getMemberId());
        //cartService.prepareCart(shop, dbTrans, false, newStatus, false, customerInfo, false);
        cartService.prepareCart(shop, incomingTrans, false, newStatus, false, customerInfo, false);

        // If it's preparing, just save it (we're not taking it from the inventory)
        if (dbTrans.isPreparingFulfillment()) {
            // we're reparing to just save it
            dbTrans.setCart(incomingTrans.getCart());
            dbTrans.setStatus(newStatus);
            populatePrepareQty(dbTrans);
            transactionRepository.update(token.getCompanyId(), dbTrans.getId(), dbTrans);
        } else {
            transactionRepository.update(token.getCompanyId(), dbTrans.getId(), dbTrans);
            queueTransactionJob(transactionId, incomingTrans, newStatus, shop);
        }
        realtimeService.sendRealTimeEvent(token.getShopId(), RealtimeService.RealtimeEventType.QueueUpdateEvent, null);

        dbTrans.setCart(incomingTrans.getCart());
        dbTrans.setMember(memberRepository.get(token.getCompanyId(), dbTrans.getMemberId()));
        if (dbTrans.isCreateTookanTask() && StringUtils.isNotBlank(dbTrans.getTookanTaskId())) {
            tookanService.updateTaskStatus(token.getCompanyId(), token.getShopId(), dbTrans, null);
        }

        if (StringUtils.isNotBlank(dbTrans.getAssignedEmployeeId())) {
            Employee employee = employeeRepository.getById(dbTrans.getAssignedEmployeeId());
            dbTrans.setAssignedEmployee(employee);
        }

        ConsumerCartEvent event = new ConsumerCartEvent();
        event.setTransaction(dbTrans);
        event.setCompanyId(token.getCompanyId());
        event.setShopId(token.getShopId());
        eventBus.post(event);
        if(StringUtils.isNotBlank(dbTrans.getAssignedEmployeeId())) {
            // Send notification
            String message = String.format("Transaction #%s was updated.", dbTrans.getTransNo());
            String title = "Update Transaction Product";
            sendNotification(dbTrans, message, title, FcmPayload.SubType.UPDATE);
        }
        return dbTrans;
    }

    @Override
    public Transaction claimTransaction(String transactionId) {
        Transaction dbTrans = transactionRepository.get(token.getCompanyId(), transactionId);

        Transaction incomingTrans = (new Kryo()).copy(dbTrans);

        if (dbTrans == null) {
            throw new BlazeInvalidArgException("TransactionId", "Transaction does not exist.");
        }
        if (!dbTrans.isActive()) {
            return dbTrans;
        }
        if (dbTrans.getStatus() == Transaction.TransactionStatus.Completed) {
            dbTrans.setActive(false);
            transactionRepository.deactivate(token.getCompanyId(), dbTrans.getId());
            return dbTrans;
        }

        if (dbTrans.isFulfillingFulfillment()) {
            if (incomingTrans.getCart().getItems().size() == 0
                    && dbTrans.getCart().getItems().size() > 0) {
                return dbTrans;
            }
        }

        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());

        // use the current logged in user
        incomingTrans.setAssignedEmployeeId(token.getActiveTopUser().getUserId());
        String sellerTerminalId = getSellerTerminalId(shop, incomingTrans.getAssignedEmployeeId());


        //if (dbTrans.getStartTime() == null || dbTrans.getStartTime() <= 0) {
        incomingTrans.setStartTime(DateTime.now().getMillis());
//if (dbTrans.getStartTime() == null || dbTrans.getStartTime() <= 0) {

        //}
        incomingTrans.setAssigned(true);

        incomingTrans.setSellerTerminalId(sellerTerminalId);
        incomingTrans.setSellerId(token.getActiveTopUser().getUserId());
        incomingTrans.setStatus(Transaction.TransactionStatus.InProgress);


        // set override inventory
        incomingTrans.setOverrideInventoryId(dbTrans.getOverrideInventoryId());

        if (!incomingTrans.isPreparingFulfillment()) {
            checkAvailableInventory(incomingTrans, dbTrans, "claim", null, sellerTerminalId, Boolean.FALSE);
        }

        Transaction.TransactionStatus newStatus = Transaction.TransactionStatus.Hold;
        if (incomingTrans.getCart().getItems().size() == 0) {
            newStatus = Transaction.TransactionStatus.Queued; // Go back to the queue
            dbTrans.setStartTime(0L);
        }
        dbTrans.setSellerTerminalId(sellerTerminalId);
        dbTrans.setSellerId(token.getActiveTopUser().getUserId());
        dbTrans.setStatus(newStatus);
        dbTrans.setTimeZone(token.getRequestTimeZone());
        if (shop.getTermSalesOption() == Shop.TerminalSalesOption.SellerAssignedTerminal) {
            dbTrans.setAssignedEmployeeId(token.getActiveTopUser().getUserId());
        }

        // If it's preparing, just save it (we're not taking it from the inventory)
        if (dbTrans.isPreparingFulfillment()) {
            // we're reparing to just save it
            dbTrans.setCart(incomingTrans.getCart());
            dbTrans.setStatus(newStatus);
            populatePrepareQty(dbTrans);
            transactionRepository.update(token.getCompanyId(), dbTrans.getId(), dbTrans);
        } else {
            transactionRepository.update(token.getCompanyId(), dbTrans.getId(), dbTrans);
            queueTransactionJob(transactionId, incomingTrans, newStatus, shop);
        }


        if (StringUtils.isNotBlank(dbTrans.getAssignedEmployeeId())) {
            Employee employee = employeeRepository.getById(dbTrans.getAssignedEmployeeId());
            dbTrans.setAssignedEmployee(employee);
        }
        realtimeService.sendRealTimeEvent(token.getShopId(), RealtimeService.RealtimeEventType.QueueUpdateEvent, null);

        return dbTrans;
    }

    @Override
    public Transaction exitTransaction(String transactionId) {
        Transaction dbTrans = transactionRepository.get(token.getCompanyId(), transactionId);
        if (dbTrans == null) {
            throw new BlazeInvalidArgException("TransactionId", "Transaction does not exist.");
        }

        if (!dbTrans.isActive()) {
            return dbTrans;
        }


        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());


        QueuedTransaction queuedTransaction = queuedTransactionRepository.getRecentCompletedTransaction(token.getCompanyId(), token.getShopId(), transactionId);

        String sellerTerminalId = getSellerTerminalId(shop, dbTrans.getAssignedEmployeeId());
        String sellerId = token.getActiveTopUser().getUserId();
        Transaction.TransactionStatus status = Transaction.TransactionStatus.Queued;

        if (queuedTransaction != null && queuedTransaction.getPendingStatus() == Transaction.TransactionStatus.Completed) {
            return dbTrans;
        }

        if (queuedTransaction != null) {
            sellerTerminalId = queuedTransaction.getTerminalId();
            sellerId = queuedTransaction.getSellerId();
        }


        dbTrans.setAssigned(true);
        dbTrans.setSellerTerminalId(sellerTerminalId);
        dbTrans.setSellerId(sellerId);
        dbTrans.setStatus(Transaction.TransactionStatus.InProgress);

        //transactionRepository.update(token.getCompanyId(), dbTrans.getId(), dbTrans);
        transactionRepository.updateTransactionState(token.getCompanyId(),dbTrans.getId(),
                status,
                sellerId,
                sellerTerminalId);

        realtimeService.sendRealTimeEvent(token.getShopId(), RealtimeService.RealtimeEventType.QueueUpdateEvent, null);
        dbTrans.setMember(memberRepository.get(token.getCompanyId(), dbTrans.getMemberId()));
        return dbTrans;
    }

    /*
        This function just updates the cart and do some calculations.
        It does not
         */
    @Override
    public Transaction prepareCart(String transactionId, Transaction transaction) {
        Transaction dbTrans = transactionRepository.get(token.getCompanyId(), transactionId);
        if (dbTrans == null) {
            throw new BlazeInvalidArgException("TransactionId", "Transaction does not exist.");
        }

        if (transaction.getQueueType().equals("Delivery")) {

        }

        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());
        if (shop.isEnableCannabisLimit()) {
            boolean status = cannabisLimitService.checkCannabisLimit(token.getCompanyId(), shop, dbTrans.getMemberId(), transaction.getCart(), null, false);
            if (!status) {
                throw new BlazeInvalidArgException("CannabisLimit", "State Cannabis Limit is reached.");
            }
        }

        final CustomerInfo customerInfo = getMemberGroup(dbTrans.getMemberId());


        cartService.prepareCart(shop, transaction, false, Transaction.TransactionStatus.InProgress, true, customerInfo, false);
        if (transaction.getCart() != null) {
            // prepared so let's clear promocode
            transaction.getCart().setPromoCode(Strings.EMPTY);
        }
        queuePrepareTransactionJob(dbTrans.getId(), transaction,
                Transaction.TransactionStatus.InProgress, shop,
                token.getActiveTopUser().getUserId(), false, null);
        return transaction;
    }

    @Override
    public Transaction prepareRefundCart(String transactionId, RefundRequest request) {

        // get refund terminal id
        Transaction dbTrans = transactionRepository.get(token.getCompanyId(), transactionId);
        if (dbTrans == null) {
            throw new BlazeInvalidArgException("TransactionId", "Transaction does not exist.");
        }
        String sellerTerminalId = getRefundSellerTerminal(dbTrans);
        //fetch data
        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());

        RefundTransactionRequest refundRequest = createRefundRequest(transactionId,request,dbTrans,shop);


        Transaction refundTransction = cartService.createRefundTransaction(shop, dbTrans, refundRequest, sellerTerminalId, token.getActiveTopUser().getUserId());

        return refundTransction;
    }

    private RefundTransactionRequest createRefundRequest(String transactionId, RefundRequest request, Transaction dbTrans, Shop shop) {


        if (dbTrans.isActive()) {
            throw new BlazeInvalidArgException("TransactionId", "Cannot refund an active transaction.");
        }

        if (Transaction.TransactionType.Refund == dbTrans.getTransType()) {
            throw new BlazeInvalidArgException("Transaction", "Refund type transaction can not be refunded");
        }

        if (request.getRefundVersion() == RefundRequest.RefundVersion.NEW && (request.getTotalRefundAmt() == null || request.getTotalRefundAmt() == BigDecimal.ZERO)) {
            throw new BlazeInvalidArgException("Transaction", "Refund total amount cannot be 0");
        }
        double availableAmount = prepareRefundItems(request, dbTrans);
        if (request.getRefundVersion() == RefundRequest.RefundVersion.NEW && (request.getTotalRefundAmt().doubleValue() > availableAmount)) {
            throw new BlazeInvalidArgException("Transaction", "Refund amount cannot be greater than " + availableAmount);
        }

        if (request.getRefundAs() != null && !dbTrans.getRefundPaymentOptions().contains(request.getRefundAs())) {
            String validOptions = String.join(",", dbTrans.getRefundPaymentOptions()
                    .stream()
                    .map(o -> o.name())
                    .collect(Collectors.toList()));
            throw new BlazeInvalidArgException("RefundAs",
                    String.format("You must specify a valid RefundAs option. Valid options for this transaction are %s",
                            validOptions));
        } else if (request.getRefundAs() == null) {
            // Backwards compatibility
            request.setRefundAs(dbTrans.getCart().getPaymentOption()); // Default refundAs to original payment option
        }

        // recreate new refund mechanism
        if (request.getRefundOrders() != null && request.getRefundOrders().size() > 0) {
            LinkedHashSet<RefundOrderItemRequest> refundOrderItemRequests = new LinkedHashSet<>();
            for (String refundId : request.getRefundOrders()) {
                for (OrderItem orderItem : dbTrans.getCart().getItems()) {
                    if (orderItem.getOrderItemId().equalsIgnoreCase(refundId)) {
                        RefundOrderItemRequest itemRequest = new RefundOrderItemRequest();
                        itemRequest.setOrderItemId(refundId);
                        itemRequest.setQuantity(orderItem.getQuantity());
                        //itemRequest.setQuantity(BigDecimal.valueOf(1));


                        // TEMP SOLUTION: THIS SHOULD BE PROPORTIONALLY CALCULATED WHEN SUPPORTING PARTIAL REFUNDS
                        itemRequest.setRefundAmt(orderItem.getFinalPrice());

                        refundOrderItemRequests.add(itemRequest);
                    }
                }
            }
            request.setRefundOrderItemRequests(Lists.newArrayList(refundOrderItemRequests));
            request.getRefundOrders().clear();
        } else {
            //CALCULATE REFUND AMT BASED ON QTY
            for (RefundOrderItemRequest itemRequest : request.getRefundOrderItemRequests()) {
                for (OrderItem orderItem : dbTrans.getCart().getItems()) {
                    if (StringUtils.isNotBlank(itemRequest.getOrderItemId()) && itemRequest.getOrderItemId().equalsIgnoreCase(orderItem.getOrderItemId())) {
                        double refundAmt = orderItem.getUnitPrice().doubleValue()*itemRequest.getQuantity().doubleValue();
                        itemRequest.setRefundAmt(new BigDecimal(refundAmt));
                        break;
                    }
                }
            }
        }


        List<ObjectId> objectIds = new ArrayList<>();
        List<String> productIds = new ArrayList<>();
        // get products
        for (OrderItem orderItem : dbTrans.getCart().getItems()) {

            if (StringUtils.isBlank(orderItem.getOrderItemId())) {
                orderItem.setOrderItemId(UUID.randomUUID().toString());
            }

            if (orderItem.getProductId() != null && ObjectId.isValid(orderItem.getProductId())) {
                objectIds.add(new ObjectId(orderItem.getProductId()));
                productIds.add(orderItem.getProductId());
            }
        }
        HashMap<String, Product> productHashMap = productRepository.findItemsInAsMap(shop.getCompanyId(),
                shop.getId(), objectIds);
        HashMap<String, ProductCategory> productCategoryHashMap = productCategoryRepository.listAllAsMap(shop.getCompanyId(), shop.getId());
        HashMap<String, PrepackageProductItem> prepackageProductItemHashMap =
                prepackageProductItemRepository.getPrepackagesForProductsAsMap(shop.getCompanyId(),
                        shop.getId(), productIds);
        Iterable<ProductWeightTolerance> tolerances = weightToleranceRepository.list(shop.getCompanyId());
        HashMap<String, ProductWeightTolerance> weightToleranceHashMap = new HashMap<>();
        for (ProductWeightTolerance tolerance : tolerances) {
            weightToleranceHashMap.put(tolerance.getWeightKey().weightName, tolerance);
        }
        Set<ObjectId> rewardIds = new HashSet<>();
        for (PromotionReq promotionReq : dbTrans.getCart().getPromotionReqs()) {
            if (StringUtils.isNotBlank(promotionReq.getRewardId()) && ObjectId.isValid(promotionReq.getRewardId())) {
                rewardIds.add(new ObjectId(promotionReq.getRewardId()));
            }
        }

        // Create Refund Requests
        RefundTransactionRequest refundRequest = new RefundTransactionRequest();
        refundRequest.prepare(token.getCompanyId());
        refundRequest.setShopId(token.getShopId());
        refundRequest.setLoc(request.getLoc());
        refundRequest.setNote(request.getNote());
        refundRequest.setRefundAs(request.getRefundAs());
        refundRequest.setRefundOrders(request.getRefundOrders());
        refundRequest.setRefundType(request.getRefundType());
        refundRequest.setWithInventory(request.isWithInventory());
        refundRequest.setStatus(RefundTransactionRequest.RefundStatus.Pending);
        refundRequest.setTransactionId(dbTrans.getId());
        refundRequest.setTotalRefundAmt(request.getTotalRefundAmt());
        Employee employee = employeeRepository.get(token.getCompanyId(), token.getActiveTopUser().getUserId());
        String employeeName ="";
        if (employee == null || employee.isDeleted()) {
            throw new BlazeInvalidArgException("Employee", "Employee does not exist.");
        }else{
            employeeName=employee.getFirstName()+" "+employee.getLastName();
        }

        //Calculate final unit prices, rewards, cartDiscounts
        Map<String,OrderItem> orderItemMap = new HashMap<>();
        Map<String,Double> orderFinalPriceMap = new HashMap<>();
        Map<String, LoyaltyReward> rewardMap = rewardRepository.listAsMap(shop.getCompanyId(), Lists.newArrayList(rewardIds));
        double totalQuantity = 0;

        BigDecimal totalCost = BigDecimal.ZERO;
        for (OrderItem orderItem : dbTrans.getCart().getItems()) {
            String orderItemId = orderItem.getOrderItemId();
            orderItemMap.put(orderItemId,orderItem);
            totalQuantity+=orderItem.getQuantity().doubleValue();
//            double finalUnitPrice = (orderItem.getFinalPrice().doubleValue()+orderItem.getDiscount())/
//                    orderItem.getQuantity().doubleValue();
//            orderFinalPriceMap.put(orderItem.getOrderItemId(),new Double(finalUnitPrice));


            totalCost = totalCost.add(orderItem.getCost());
        }
        //Get Cart Discount per unit
        BigDecimal cartDiscountUnit = dbTrans.getCart().getCalcCartDiscount()!=null?
                dbTrans.getCart().getCalcCartDiscount():BigDecimal.ZERO;



        for (RefundOrderItemRequest refundOrderItemRequest:request.getRefundOrderItemRequests()) {
            refundOrderItemRequest.setCreated(DateTime.now().getMillis());
            refundOrderItemRequest.setCreatedById(token.getActiveTopUser().getUserId());
            refundOrderItemRequest.setRefundAs(request.getRefundAs());
            refundOrderItemRequest.setRefundType(request.getRefundType());
            refundOrderItemRequest.setWithInventory(request.isWithInventory());
            refundOrderItemRequest.setNote(request.getNote());
            refundOrderItemRequest.setCreatedUser(employeeName);
            String orderItemId = refundOrderItemRequest.getOrderItemId();

            //Reduce reward discount amount
            Double rewardAmountReduce = 0D;
            List<PromotionReq> rewardList = new ArrayList<>();
            OrderItem orderItem = orderItemMap.get(orderItemId);
            Iterator<PromotionReq> iter=orderItem.getPromotionReqs().iterator();
            //Add all promotionReq that are rewards
            while(iter.hasNext()){
                PromotionReq promotionReq = iter.next();
                if (StringUtils.isNotEmpty(promotionReq.getRewardId())){
                    rewardList.add(promotionReq);
                }
            }
            Double totalRefundQuantity = orderItem.getTotalRefundQty()!=null?
                    orderItem.getTotalRefundQty().doubleValue():0D;
            //Calculate total Reward Amount for an Order Item
            Double totalRewardAmount = 0D;

            //Partial Refund
            if (Double.compare(totalRefundQuantity+refundOrderItemRequest.getQuantity().doubleValue(),orderItem.getQuantity().doubleValue())<0){
                for (int i=0;i<rewardList.size();i++){
                    PromotionReq promotionReq = rewardList.get(i);
                    LoyaltyReward reward = rewardMap.get(promotionReq.getRewardId());
                    BigDecimal amount = promotionProcessService.getRewardDiscountAmountForProduct(reward,orderItem,productHashMap,
                            productCategoryHashMap,weightToleranceHashMap);
                    totalRewardAmount+=amount.doubleValue();
                }
            }else{
                refundLoyaltyPoints(dbTrans,orderItemId,rewardList,rewardMap);
            }
            //Calculate refund amount
            //Q*(PU-CartDiscountUnit) - Discount + RewardDiscount
            BigDecimal ratio = BigDecimal.valueOf(1.0d);
            double ratioCartDiscount = 0;
            if (totalCost.doubleValue() > 0) {
                BigDecimal refundCost = orderItem.getUnitPrice().multiply(refundOrderItemRequest.getQuantity());
                ratio = refundCost.divide(totalCost,2, RoundingMode.HALF_EVEN);
            }

            ratioCartDiscount = ratio.multiply(cartDiscountUnit).doubleValue();

            double refundAmt = (orderItem.getUnitPrice().doubleValue())*refundOrderItemRequest.getQuantity().doubleValue();

            BigDecimal qtyRatio = refundOrderItemRequest.getQuantity().divide(orderItem.getQuantity(),2,BigDecimal.ROUND_HALF_EVEN);

            refundAmt = refundAmt - qtyRatio.multiply(orderItem.getCalcDiscount()).doubleValue() - ratioCartDiscount;

            refundOrderItemRequest.setRefundAmt(new BigDecimal(NumberUtils.round(refundAmt, 2)));
        }
        refundRequest.setRefundOrderItemRequests(request.getRefundOrderItemRequests());


        refundRequest.setRefundVersion(request.getRefundVersion());

        return refundRequest;
    }





    private boolean didLockedOrderChange(Transaction dbTrans, Transaction transaction) {

        if (dbTrans.getCart().getItems().size() != transaction.getCart().getItems().size()) {
            return true;
        } else {
            for (OrderItem item : dbTrans.getCart().getItems()) {
                for (OrderItem orderItem : transaction.getCart().getItems()) {
                    if (item.getId().equalsIgnoreCase(orderItem.getId())) {
                        if (item.getQuantity().doubleValue() != orderItem.getQuantity().doubleValue()) {
                            return true;
                        }
                        break;
                    }
                }
            }
        }

        return false;
    }

    @Override
    public List<Cart.PaymentOption> getRefundPaymentOptions(String transactionId) {
        Transaction dbTrans = transactionRepository.get(token.getCompanyId(), transactionId);
        if (dbTrans == null) {
            throw new BlazeInvalidArgException("TransactionId", "Transaction does not exist.");
        }
        List<Cart.PaymentOption> cartPaymentOptions = dbTrans.getRefundPaymentOptions();
        List<ShopPaymentOption> paymentOptions = shopPaymentOptionService.getPaymentOptions();
        for (ShopPaymentOption option : paymentOptions) {
            if (!option.isEnabled()) {
                cartPaymentOptions.remove(option.getPaymentOption());
            }
        }
        if (cartPaymentOptions.isEmpty()) {
            cartPaymentOptions.add(Cart.PaymentOption.Cash);
        }
        return cartPaymentOptions;
    }

    @Override
    public Transaction refundTransaction(String transactionId, RefundRequest request) {

        // get refund terminal id
        Transaction dbTrans = transactionRepository.get(token.getCompanyId(), transactionId);
        if (dbTrans == null) {
            throw new BlazeInvalidArgException("TransactionId", "Transaction does not exist.");
        }
        String sellerTerminalId = getRefundSellerTerminal(dbTrans);
        //fetch data
        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());

        RefundTransactionRequest refundRequest = createRefundRequest(transactionId,request,dbTrans,shop);


        // persist request
        refundTransactionRequestRepository.save(refundRequest);

        // update db for pending request
        updateRefundOrderItemInTransaction(request, dbTrans);


        // queue request
        queueRefundJob(refundRequest.getId(), refundRequest, token.getActiveTopUser().getUserId(), sellerTerminalId);
        return dbTrans;
    }

    private void refundLoyaltyPoints(Transaction dbTrans,
                                    String orderItemId,
                                    List<PromotionReq> rewardList,
                                    Map<String, LoyaltyReward> rewardMap){
        Cart cart=dbTrans.getCart();
        List<String> completedOrderItem = new ArrayList<>();
        List<LoyaltyReward.LoyaltyRewardTarget> banList = new ArrayList<>();
        banList.add(LoyaltyReward.LoyaltyRewardTarget.WholeCart);
        banList.add(LoyaltyReward.LoyaltyRewardTarget.DeliveryFee);
        for (PromotionReq promotionReq:rewardList) {
            LoyaltyReward reward = rewardMap.get(promotionReq.getRewardId());
            if (reward.getRewardTarget().equals(LoyaltyReward.LoyaltyRewardTarget.AllSelectedProducts)){
                boolean isApplied=false;
                for (String productId : reward.getProductIds()) {
                    if (isApplied){
                        break;
                    }
                    for (OrderItem orderItem : cart.getItems()) {
                        if (orderItem.getOrderItemId().equalsIgnoreCase(orderItemId)){
                            continue;
                        }
                        if (orderItem.getProductId().equalsIgnoreCase(productId)) {
                            BigDecimal totalRefundQty = orderItem.getTotalRefundQty();
                            if (totalRefundQty==null || totalRefundQty.compareTo(orderItem.getQuantity())!=0){
                                isApplied=true;
                                break;
                            }
                        }
                    }
                }
                //is reward is not applied in the cart, points are returned.
                if (!isApplied){
                    memberRepository.addLoyaltyPoints(token.getCompanyId(),
                            dbTrans.getMemberId(), reward.getPoints());
                }
            }else if (!banList.contains(reward.getRewardTarget())){
                //If reward doesnt belong to banlist add points
                memberRepository.addLoyaltyPoints(token.getCompanyId(),
                        dbTrans.getMemberId(), reward.getPoints());
            }
        }
        //Check condition for reward refund cart
        for (PromotionReq promotionReq:cart.getPromotionReqs()) {
            //Consider promoReq that has rewardId
            if (StringUtils.isNotEmpty(promotionReq.getRewardId())){
                LoyaltyReward reward = rewardMap.get(promotionReq.getRewardId());
                if (reward.getRewardTarget().equals(LoyaltyReward.LoyaltyRewardTarget.WholeCart)){
                    boolean isApplied=false;
                    for (OrderItem orderItem : cart.getItems()) {
                        if (orderItem.getOrderItemId().equalsIgnoreCase(orderItemId)){
                            continue;
                        }
                        BigDecimal totalRefundQty = orderItem.getTotalRefundQty();
                        if (totalRefundQty==null || totalRefundQty.compareTo(orderItem.getQuantity())!=0){
                            isApplied=true;
                            break;
                        }
                    }
                    if (!isApplied){
                        memberRepository.addLoyaltyPoints(token.getCompanyId(),
                                dbTrans.getMemberId(), reward.getPoints());
                    }
                }else{
                    continue;
                }
            }
        }
    }

    private String getRefundSellerTerminal(final Transaction dbTrans) {

        // Route the terminal correctly
        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());
        String sellerTerminalId = getSellerTerminalId(shop, token.getActiveTopUser().getUserId());
        if (StringUtils.isBlank(sellerTerminalId)) {
            sellerTerminalId = dbTrans.getSellerTerminalId();
        } else {
            Terminal sellerTerm = terminalRepository.get(token.getCompanyId(),sellerTerminalId);
            if (sellerTerm != null && !sellerTerm.getShopId().equals(token.getShopId())) {
                // terminal exist but it's in a differnet shop, so just continue to use original seller's terminal id
                sellerTerminalId = dbTrans.getSellerTerminalId();
            }
        }

        if (shop != null) {
            // make sure the current terminal has a cash drawer
            if (shop.isEnforceCashDrawers()) {
                Terminal sellerTerminal = terminalRepository.get(token.getCompanyId(),sellerTerminalId);

                long nowMillis = DateTime.now().getMillis();
                String todayDate = DateUtil.toDateFormatted(nowMillis, shop.getTimeZone());
                String todayFormatted = DateUtil.toDateFormatted(nowMillis, shop.getTimeZone(), "yyyyMMdd");

                CashDrawerSession dbLogResult = cashDrawerSessionRepository.getCashDrawerForDate(token.getCompanyId(),
                        shop.getId(), sellerTerminalId, todayFormatted);

                if (dbLogResult == null) {
                    throw new BlazeInvalidArgException("Terminal",
                            String.format("Please create a cash drawer for today's date, %s, for terminal '%s' before refunding.",
                                    todayDate,
                                    sellerTerminal.getName()));
                } else if (dbLogResult != null && dbLogResult.getStatus() == CashDrawerSession.CashDrawerLogStatus.Closed) {

                    throw new BlazeInvalidArgException("Terminal",
                            String.format("Please open a cash drawer for today's date, %s, for terminal '%s' before refunding.",
                                    todayDate,
                                    sellerTerminal.getName()));
                }
            }
        }
        return sellerTerminalId;
    }

    private void updateRefundItemQuantityMap(Map<String ,BigDecimal> map,String orderItemID,BigDecimal quantity){
        if (map.containsKey(orderItemID)){
            BigDecimal updateQuantity = map.get(orderItemID).add(quantity);
            map.put(orderItemID, updateQuantity);
        }else{
            map.put(orderItemID, quantity);
        }
    }

    private void updateRefundOrderItemInTransaction(RefundRequest refundRequest, Transaction transaction) {
//        Map<String, BigDecimal> refundOrderItemInDbMap = new HashMap<>();
//        Map<String, BigDecimal> refundOrderAmtInDbMap = new HashMap<>();
//        if (transaction.getCart().getRefundOrderItemRequests().size() > 0) {
//            for (RefundOrderItemRequest itemRequest : transaction.getCart().getRefundOrderItemRequests()) {
//                refundOrderItemInDbMap.put(itemRequest.getOrderItemId(), itemRequest.getQuantity());
//                refundOrderAmtInDbMap.put(itemRequest.getOrderItemId(), itemRequest.getRefundAmt());
//            }
//        }
//
//        Map<String, BigDecimal> refundOrderItemMap = new HashMap<>();
//        Map<String, BigDecimal> refundOrderAmtMap = new HashMap<>();
//        if (refundRequest.getRefundOrderItemRequests().size() > 0) {
//            for (RefundOrderItemRequest refundOrderItemRequest : refundRequest.getRefundOrderItemRequests()) {
//                if (refundOrderItemInDbMap.containsKey(refundOrderItemRequest.getOrderItemId())) {
//                    BigDecimal updateQuantity = refundOrderItemInDbMap.get(refundOrderItemRequest.getOrderItemId()).add(refundOrderItemRequest.getQuantity());
//                    refundOrderItemMap.put(refundOrderItemRequest.getOrderItemId(), updateQuantity);
//                    BigDecimal updateAmt = refundOrderAmtInDbMap.get(refundOrderItemRequest.getOrderItemId());
//                    updateAmt = (updateAmt == null) ? BigDecimal.ZERO : updateAmt; // check if null
//                    updateAmt = updateAmt.add(refundOrderItemRequest.getRefundAmt());
//                    refundOrderAmtMap.put(refundOrderItemRequest.getOrderItemId(), updateAmt);
//
//                } else {
//                    refundOrderItemMap.put(refundOrderItemRequest.getOrderItemId(), refundOrderItemRequest.getQuantity());
//                    refundOrderAmtMap.put(refundOrderItemRequest.getOrderItemId(), refundOrderItemRequest.getRefundAmt());
//                }
//            }
//        }
        //Calculate total refunded quantity for each order item
        Map<String,BigDecimal> refundOrderItemQuantityMap = new HashMap<>();
        if (transaction.getCart().getRefundOrderItemRequests().size() > 0) {
            for (RefundOrderItemRequest itemRequest : transaction.getCart().getRefundOrderItemRequests()) {
                updateRefundItemQuantityMap(refundOrderItemQuantityMap,itemRequest.getOrderItemId(),itemRequest.getQuantity());
            }
        }
        if (refundRequest.getRefundOrderItemRequests().size() > 0) {
            for (RefundOrderItemRequest refundOrderItemRequest : refundRequest.getRefundOrderItemRequests()) {
                updateRefundItemQuantityMap(refundOrderItemQuantityMap,refundOrderItemRequest.getOrderItemId(),refundOrderItemRequest.getQuantity());
            }
        }

        for (OrderItem orderItem : transaction.getCart().getItems()) {
            BigDecimal refundedTotalQuantity = new BigDecimal(0);
            if (refundOrderItemQuantityMap.containsKey(orderItem.getOrderItemId())){
                refundedTotalQuantity = refundOrderItemQuantityMap.get(orderItem.getOrderItemId());
            }
            if (orderItem.getQuantity().doubleValue() < refundedTotalQuantity.doubleValue()) {
                LOG.info("Refunded Total Quantity: " + refundedTotalQuantity.doubleValue() + "  orderItemQty: " + orderItem.getQuantity().doubleValue());
                throw new BlazeInvalidArgException("Transaction", "Amount refund requested is greater than what's available to refund.");
            }
        }

        List<RefundOrderItemRequest> refundOrderItemRequests = transaction.getCart().getRefundOrderItemRequests();
        for (RefundOrderItemRequest refundOrderItemRequest:refundRequest.getRefundOrderItemRequests()) {
            refundOrderItemRequests.add(refundOrderItemRequest);
        }

//        connectAuthToken.getActiveTopUser().getUserId()
//
//        List<RefundOrderItemRequest> refundOrderItemRequests = new ArrayList<>();
//        BigDecimal refundedTotalQuantity = new BigDecimal(0);
//        BigDecimal refundedTotalAmount = new BigDecimal(0);
//        for (OrderItem orderItem : transaction.getCart().getItems()) {
//            if (refundOrderItemMap.containsKey(orderItem.getOrderItemId())) {
//                refundedTotalQuantity = refundOrderItemMap.get(orderItem.getOrderItemId());
//                refundedTotalAmount = refundOrderAmtMap.get(orderItem.getOrderItemId());
//            } else if (refundOrderItemInDbMap.containsKey(orderItem.getOrderItemId())) {
//                refundedTotalQuantity = refundOrderItemInDbMap.get(orderItem.getOrderItemId());
//                refundedTotalAmount = refundOrderAmtInDbMap.get(orderItem.getOrderItemId());
//            } else {
//                continue;
//            }
//
//            if (orderItem.getQuantity().doubleValue() < refundedTotalQuantity.doubleValue()) {
//                LOG.info("Refunded Total Quantity: " + refundedTotalQuantity.doubleValue() + "  orderItemQty: " + orderItem.getQuantity().doubleValue());
//                throw new BlazeInvalidArgException("Transaction", "Amount refund requested is greater than what's available to refund.");
//            }
//
//            RefundOrderItemRequest itemRequest = new RefundOrderItemRequest();
//            itemRequest.setOrderItemId(orderItem.getOrderItemId());
//            itemRequest.setQuantity(refundedTotalQuantity);
//            //orderItem.setTotalRefundQty(refundedTotalQuantity);
//
//
//
//            // CALCULATE REFUND AMOUNT FOR PARTIAL REFUNDS
//            if (refundRequest.getRefundVersion() == RefundRequest.RefundVersion.OLD) {
//                double refundAmt = orderItem.getUnitPrice().doubleValue()*itemRequest.getQuantity().doubleValue();
//                itemRequest.setRefundAmt(new BigDecimal(refundAmt));
//            } else {
//                itemRequest.setRefundAmt(refundedTotalAmount);
//            }
//
//            refundOrderItemRequests.add(itemRequest);
//        }

        transaction.getCart().setRefundOrderItemRequests(refundOrderItemRequests);
        transaction.setTransactionRefundStatus(Transaction.TransactionRefundStatus.In_Progress);
//
        transactionRepository.update(token.getCompanyId(), transaction.getId(), transaction);

    }

    @Override
    public Transaction startTransaction(String transactionId, boolean reassignEmployee) {
        Transaction dbTrans = transactionRepository.get(token.getCompanyId(), transactionId);
        if (dbTrans == null) {
            throw new BlazeInvalidArgException("TransactionId", "Transaction does not exist.");
        }

        if (!dbTrans.isActive()) {
            throw new BlazeInvalidArgException("TransactionId", "Cannot start an inactive transaction.");
        }

        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());
        String assignedEmployeeId = dbTrans.getAssignedEmployeeId();
        String sellerTerminalId = getSellerTerminalId(shop, dbTrans.getAssignedEmployeeId());
        String sellerId = token.getActiveTopUser().getUserId();
        Transaction.TransactionStatus status = Transaction.TransactionStatus.InProgress;

        //if (dbTrans.getStartTime() == null || dbTrans.getStartTime() <= 0) {
        if (reassignEmployee && (StringUtils.isBlank(assignedEmployeeId) || shop.getTermSalesOption() == Shop.TerminalSalesOption.SellerAssignedTerminal)) {
            dbTrans.setAssignedEmployeeId(token.getActiveTopUser().getUserId());
            assignedEmployeeId = token.getActiveTopUser().getUserId();
        }


        dbTrans.setAssigned(true);
        dbTrans.setSellerTerminalId(sellerTerminalId);
        dbTrans.setSellerId(sellerId);
        dbTrans.setStatus(Transaction.TransactionStatus.InProgress);

        if (reassignEmployee) {
            transactionRepository.startTransaction(token.getCompanyId(), dbTrans.getId(), Transaction.TransactionStatus.InProgress,assignedEmployeeId,sellerId,sellerTerminalId);
        } else {
            transactionRepository.startTransaction(token.getCompanyId(), dbTrans.getId(), Transaction.TransactionStatus.InProgress, sellerId, sellerTerminalId);
        }
        //transactionRepository.update(token.getCompanyId(), dbTrans.getId(), dbTrans);
        realtimeService.sendRealTimeEvent(token.getShopId(), RealtimeService.RealtimeEventType.QueueUpdateEvent, null);

        SearchResult<Transaction> result = new SearchResult<>();
        result.setValues(Lists.newArrayList(dbTrans));
        assignDependentObjects(result, true, true);
        return dbTrans;
    }

    @Override
    public Transaction stopTransaction(String transactionId) {
        Transaction dbTrans = transactionRepository.get(token.getCompanyId(), transactionId);
        if (dbTrans == null) {
            throw new BlazeInvalidArgException("TransactionId", "Transaction does not exist.");
        }

        if (!dbTrans.isActive()) {
            return dbTrans;
        }
        if (dbTrans.getStartTime() == null || dbTrans.getStartTime() <= 0) {
            dbTrans.setStartTime(DateTime.now().getMillis());
        }

        dbTrans.setSellerId(token.getActiveTopUser().getUserId());
        dbTrans.setStatus(Transaction.TransactionStatus.Queued);
        transactionRepository.update(token.getCompanyId(), dbTrans.getId(), dbTrans);
        realtimeService.sendRealTimeEvent(token.getShopId(), RealtimeService.RealtimeEventType.QueueUpdateEvent, null);
        dbTrans.setMember(memberRepository.get(token.getCompanyId(), dbTrans.getMemberId()));
        return dbTrans;
    }

    @Override
    public Transaction updateProcessedTime(String transactionId, UpdateProcessedTimeRequest request) {
        Transaction dbTrans = transactionRepository.get(token.getCompanyId(), transactionId);
        if (dbTrans == null) {
            throw new BlazeInvalidArgException("TransactionId", "Transaction does not exist.");
        }

        if (request.getProcessedTime() == null) {
            throw new BlazeInvalidArgException("Transaction.ProcessedTime", "Invalid processed time.");
        }
        dbTrans.setProcessedTime(request.getProcessedTime());
        dbTrans.setCompletedTime(request.getProcessedTime());
        transactionRepository.update(token.getCompanyId(), dbTrans.getId(), dbTrans);
        realtimeService.sendRealTimeEvent(token.getShopId(), RealtimeService.RealtimeEventType.QueueUpdateEvent, null);

        // NOTE: SLOPPY WAY OF DOING THIS BUT IT'S CONVENIENT. HAHA.
        SearchResult<Transaction> result = new SearchResult<>();
        result.getValues().add(dbTrans);

        assignDependentObjects(result, true, true);
        return dbTrans;
    }

    @Override
    public Transaction updateTransactionNote(String transactionId, UpdateTransMemoRequest memoRequest) {
        Transaction dbTrans = transactionRepository.get(token.getCompanyId(), transactionId);
        if (dbTrans == null) {
            throw new BlazeInvalidArgException("TransactionId", "Transaction does not exist.");
        }

        dbTrans.setMemo(memoRequest.getMemo());
        transactionRepository.update(token.getCompanyId(), dbTrans.getId(), dbTrans);
        realtimeService.sendRealTimeEvent(token.getShopId(), RealtimeService.RealtimeEventType.QueueUpdateEvent, null);

        // NOTE: SLOPPY WAY OF DOING THIS BUT IT'S CONVENIENT. HAHA.
        SearchResult<Transaction> result = new SearchResult<>();
        result.getValues().add(dbTrans);

        assignDependentObjects(result, true, true);
        return dbTrans;
    }

    @Override
    public Transaction reassignEmployee(String transactionId, EmployeeReassignRequest request) {

        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());
        if (shop == null) {
            throw new BlazeInvalidArgException("Shop", "Shop does not exist");
        }

        Transaction dbTrans = transactionRepository.get(token.getCompanyId(), transactionId);
        if (dbTrans == null) {
            throw new BlazeInvalidArgException("TransactionId", "Transaction does not exist.");
        }

        if (dbTrans.isActive() == false) {
            throw new BlazeInvalidArgException("TransactionId", "Transaction is no longer active.");
        }

        Employee employee = null;
        // Check if employee is valid
        Transaction.TransactionStatus newStatus = Transaction.TransactionStatus.Hold;
        if (StringUtils.isNotEmpty(request.getEmployeeId())) {
            employee = employeeRepository.get(token.getCompanyId(), request.getEmployeeId());
            if (employee == null || employee.isDeleted()) {
                throw new BlazeInvalidArgException("Employee", "Employee does not exist.");
            }

            if (employee.isDisabled()) {
                throw new BlazeInvalidArgException("Employee", "Employee does is inactive.");
            }

            if (request.isCreateTookanTask() && shop.isEnableTookan()) {
                Member member = memberRepository.get(token.getCompanyId(), dbTrans.getMemberId());
                dbTrans.setCreateTookanTask(request.isCreateTookanTask());
                createTookanTask(dbTrans, employee, member, request.getTookanTeamId(), true, false, dbTrans.getQueueType());
            }

            Terminal terminal = terminalRepository.get(token.getCompanyId(), employee.getAssignedTerminalId());

            if (terminal != null && terminal.getCheckoutType() != dbTrans.getCheckoutType()) {
                throw new BlazeInvalidArgException("Employee", "Cannot reassign transaction terminal checkout type is not equivalent to transaction checkout type.");
            }

            if (StringUtils.isNotBlank(request.getOverrideInventoryId())) {
                dbTrans.setOverrideInventoryId(request.getOverrideInventoryId());
            }

            if (terminal != null || StringUtils.isNotBlank(request.getOverrideInventoryId())) {
                if (!request.isTransferItems()) {
                    // if we're transfering inventory, then do not check available inventory
                    String terminalId = null;
                    if (terminal != null) {
                        terminalId = terminal.getId();
                        dbTrans.setSellerTerminalId(terminal.getId());
                    }
                    checkAvailableInventory(dbTrans, dbTrans, "reassign", employee, terminalId, Boolean.FALSE);
                }
            } else if (!dbTrans.isPreparingFulfillment()) {
                throw new BlazeInvalidArgException("Employee", "Employee does not have an assigned terminal.");
            }

            dbTrans.setSellerId(request.getEmployeeId());
            dbTrans.setStatus(newStatus);
            dbTrans.setTimeZone(token.getRequestTimeZone());

            /* If order is not assigned previosly update consumer cart order*/
            if (StringUtils.isNotBlank(dbTrans.getConsumerCartId())) {
                ConsumerCart dbConsumerCart = consumerCartRepository.get(token.getCompanyId(), dbTrans.getConsumerCartId());
                if (dbConsumerCart != null) {
                    dbConsumerCart.setEmployeeName(employee.getFirstName() + " " + ((employee.getLastName() != null && employee.getLastName().length() > 0) ? employee.getLastName().substring(0, 1) : ""));
                    dbConsumerCart.setAssignedEmployeeId(employee.getId());
                    consumerCartRepository.update(token.getCompanyId(), dbConsumerCart.getId(), dbConsumerCart);
                }
            }

            employeeNotificationService.sendAssignedOrderNotification(dbTrans, employee.getId());
        }
        dbTrans.setAssignedEmployeeId(request.getEmployeeId());
        dbTrans.setAssigned((StringUtils.isNotBlank(request.getEmployeeId())));
        dbTrans.setCreateOnfleetTask(request.getCreateOnfleetTask());
        if (StringUtils.isNotBlank(request.getPackedBy())) {
            dbTrans.setPackedBy(request.getPackedBy());
            dbTrans.setPackedDate(new DateTime().getMillis());
        } else {
            dbTrans.setPackedBy(StringUtils.EMPTY);
            dbTrans.setPackedDate(0L);
        }

        // check if employee's terminal has the correct inventory
        transactionRepository.update(token.getCompanyId(), dbTrans.getId(), dbTrans);

        String transferInventoryId = "";
        // if we're transferring items,
        if (request.isTransferItems() && dbTrans.getCart() != null && dbTrans.getCart().getItems().size() > 0) {

            Terminal terminal = terminalRepository.getById(employee.getAssignedTerminalId());
            if (terminal != null) {
                transferInventoryId = terminal.getAssignedInventoryId();
            }


            UniqueSequence sequence = uniqueSequenceRepository.getNewIdentifier(token.getCompanyId(), token.getShopId(), Transaction.UNIQUE_SEQ_PRIORITY);

            Transaction trans = new Kryo().copy(dbTrans);
            trans.setId(null);
            trans.setTransNo("" + sequence.getCount());
            trans.setActive(false);
            trans.setCompanyId(token.getCompanyId());
            trans.setShopId(token.getShopId());
            trans.setQueueType(Transaction.QueueType.None);
            trans.setStatus(Transaction.TransactionStatus.Void);
            trans.setCheckinTime(DateTime.now().getMillis());
            trans.setSellerId(token.getActiveTopUser().getUserId());

            trans.setPriority(sequence.getCount());
            trans.setStartTime(DateTime.now().getMillis());
            trans.setEndTime(DateTime.now().getMillis());
            trans.setTransType(Transaction.TransactionType.Transfer);

            if (trans.getCart() != null) {
                trans.getCart().setTotal(new BigDecimal(0));
                trans.getCart().setChangeDue(new BigDecimal(0));
                trans.getCart().setCashReceived(new BigDecimal(0));
                trans.getCart().setSubTotal(new BigDecimal(0));
                trans.getCart().setTaxResult(new TaxResult());
                trans.getCart().setTotalCalcTax(new BigDecimal(0));


                BulkInventoryTransferRequest transferRequest = new BulkInventoryTransferRequest();
                transferRequest.setFromShopId(token.getShopId());
                transferRequest.setToShopId(token.getShopId());

                for (OrderItem orderItem : trans.getCart().getItems()) {
                    orderItem.setFinalPrice(new BigDecimal(0));
                    orderItem.setCost(new BigDecimal(0));
                    orderItem.setCalcTax(new BigDecimal(0));
                    orderItem.setUnitPrice(new BigDecimal(0));


                    if (orderItem.getQuantityLogs().size() > 0) {
                        QuantityLog quantityLog = orderItem.getQuantityLogs().get(0);


                        if (!quantityLog.getInventoryId().equalsIgnoreCase(transferInventoryId)) {
                            InventoryTransferRequest trequest = new InventoryTransferRequest();
                            trequest.setFromBatchId(orderItem.getBatchId());
                            trequest.setFromInventoryId(quantityLog.getInventoryId()); // old inventory
                            trequest.setToInventoryId(transferInventoryId); // new inventory
                            trequest.setTransferAmount(orderItem.getQuantity());
                            trequest.setPrepackageItemId(orderItem.getPrepackageItemId());
                            transferRequest.getTransfers().add(trequest);
                        }
                    }
                }

                // set the transfer requests
                if (transferRequest.getTransfers().size() > 0) {
                    trans.setTransferRequest(transferRequest);
                }
            }


            // Add note
            Note note = new Note();
            note.prepare();
            note.setWriterId(token.getActiveTopUser().getUserId());
            note.setWriterName(token.getActiveTopUser().getFirstName() + " " + token.getActiveTopUser().getLastName());
            note.setMessage(String.format("Re-assigning to '%s %s' with transfer inventory.", employee.getFirstName(), employee.getLastName()));
            trans.setNote(note);


            // ony create the transfer if there's something we're transfering
            if (trans.getTransferRequest() != null) {
                transactionRepository.save(trans);
            }


            dbTrans = transactionRepository.save(trans);
        }


        final CustomerInfo customerInfo = getMemberGroup(dbTrans.getMemberId());
        cartService.prepareCart(shop, dbTrans, false, Transaction.TransactionStatus.Hold, false, customerInfo, false);

        // If it's preparing, just save it (we're not taking it from the inventory)
        if (dbTrans.isPreparingFulfillment()) {
            // we're reparing to just save it
            dbTrans.setCart(dbTrans.getCart());
            dbTrans.setStatus(newStatus);
            populatePrepareQty(dbTrans);
            transactionRepository.update(token.getCompanyId(), dbTrans.getId(), dbTrans);
        } else {
            transactionRepository.update(token.getCompanyId(), dbTrans.getId(), dbTrans);
            queueTransactionJob(transactionId, dbTrans, newStatus, shop, dbTrans.getSellerId(), request.isTransferItems(), transferInventoryId);
        }


        if (shop.isEnableOnFleet() && dbTrans.getCreateOnfleetTask()) {

            String employeeWorkerId = "";
            EmployeeOnFleetInfo employeeOnFleetInfo = onFleetService.getEmployeeOnfFleetInfoByShop(employee, shop.getId());
            if (employeeOnFleetInfo != null) {
                employeeWorkerId = employeeOnFleetInfo.getOnFleetWorkerId();
            }

            Member member = memberRepository.get(token.getCompanyId(), dbTrans.getMemberId());
            onFleetService.reAssignTask(shop, dbTrans, employeeWorkerId, request.getOnFleetTeamId(), employee, employeeOnFleetInfo, member);

        }
        if (request.isCreateTookanTask() && shop.isEnableTookan()) {
            Member member = memberRepository.get(token.getCompanyId(), dbTrans.getMemberId());
            dbTrans.setCreateTookanTask(request.isCreateTookanTask());
            createTookanTask(dbTrans, employee, member, request.getTookanTeamId(), true, true, dbTrans.getQueueType());
        }

        memberRepository.updateModified(dbTrans.getMemberId());
        realtimeService.sendRealTimeEvent(token.getShopId(), RealtimeService.RealtimeEventType.MembersUpdateEvent, null);
        realtimeService.sendRealTimeEvent(token.getShopId(), RealtimeService.RealtimeEventType.QueueUpdateEvent, null);
        // NOTE: SLOPPY WAY OF DOING THIS BUT IT'S CONVENIENT. HAHA.
        SearchResult<Transaction> result = new SearchResult<>();
        result.getValues().add(dbTrans);
        assignDependentObjects(result, true, true);
        return dbTrans;
    }

    @Override
    public Transaction changeQueueType(String transactionId, Transaction transactionRequest) {
        Transaction dbTrans = transactionRepository.get(token.getCompanyId(), transactionId);
        if (dbTrans == null) {
            throw new BlazeInvalidArgException("TransactionId", "Transaction does not exist.");
        }

        if (dbTrans.isActive() == false) {
            throw new BlazeInvalidArgException("TransactionId", "Transaction is no longer active.");
        }

        dbTrans.setQueueType(transactionRequest.getQueueType());
        transactionRepository.update(token.getCompanyId(), dbTrans.getId(), dbTrans);
        realtimeService.sendRealTimeEvent(token.getShopId(), RealtimeService.RealtimeEventType.QueueUpdateEvent, null);
        return dbTrans;
    }


    @Override
    public Transaction unassignedTransaction(String transactionId) {
        Transaction dbTrans = transactionRepository.get(token.getCompanyId(), transactionId);
        if (dbTrans == null) {
            throw new BlazeInvalidArgException("TransactionId", "Transaction does not exist.");
        }

        if (dbTrans.isActive() == false) {
            throw new BlazeInvalidArgException("TransactionId", "Transaction is no longer active.");
        }

        dbTrans.setAssignedEmployeeId(Strings.EMPTY);
        dbTrans.setSellerId(Strings.EMPTY);
        dbTrans.setAssigned(false);

        // TODO: PUT BACK THE INVENTORY
        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());

        transactionRepository.markAsUnassigned(token.getCompanyId(), dbTrans.getId());
        queueTransactionJob(dbTrans.getId(), dbTrans, Transaction.TransactionStatus.Queued, shop);

        realtimeService.sendRealTimeEvent(token.getShopId(), RealtimeService.RealtimeEventType.QueueUpdateEvent, null);
        return dbTrans;
    }

    @Override
    public void sendToMetrc(String transactionId) {
        Transaction dbTrans = transactionRepository.get(token.getCompanyId(), transactionId);
        if (dbTrans == null) {
            throw new BlazeInvalidArgException("TransactionId", "Transaction does not exist.");
        }

        metrcProcessingService.submitUpdateReceipt(dbTrans, true);
    }

    @Override
    public void resendToMetrc(long startDate, long endDate, boolean submitErrors) {
        if (startDate == 0) {
            return;
        }
        if (endDate <= 0) {
            endDate = DateUtil.nowUTC().getMillis();
        }

        Iterable<Transaction> allTransactions = transactionRepository.getBracketSales(token.getCompanyId(), token.getShopId(), startDate, endDate);

        for (Transaction transaction : allTransactions) {
            boolean shouldSubmit = true;
            if (submitErrors) {
                if (transaction.getTraceSubmitStatus() != Transaction.TraceSubmissionStatus.SubmissionError) {
                    shouldSubmit = false;
                }

            }

            if (shouldSubmit && (transaction.getMetrcId() == null || transaction.getMetrcId() == 0)) {
                LOG.info("Resubmitting to METRC: " + transaction.getTransNo());
                metrcProcessingService.submitUpdateReceipt(transaction, true);
            } else {
                LOG.info("Skipping METRC: " + transaction.getTransNo());
            }
        }
        LOG.info("Completed resubmitting to METRC");

    }

    @Override
    public Transaction acceptIncomingOrder(String consumerCartId, EmployeeReassignRequest request) {
        ConsumerCartResult dbConsumerCart = consumerCartRepository.get(token.getCompanyId(), consumerCartId, ConsumerCartResult.class);
        if (dbConsumerCart == null) {
            throw new BlazeInvalidArgException("ConsumerCart", "Consumer cart does not exist.");
        }


        Employee employee = null;

        if (StringUtils.isNotBlank(request.getEmployeeId())) {
            employee = employeeRepository.get(token.getCompanyId(), request.getEmployeeId());
            if (employee == null) {
                throw new BlazeInvalidArgException("ConsumerCart", "Employee does not exist.");
            }
        }

        Member member = memberRepository.get(token.getCompanyId(), dbConsumerCart.getMemberId());
        ConsumerUser consumerUser = consumerUserRepository.getById(dbConsumerCart.getConsumerId());

        if (member == null) {
            member = memberRepository.getMemberWithConsumerId(token.getCompanyId(), dbConsumerCart.getConsumerId());
        }
        // see if member can be found in the consumer user
        if (member == null && consumerUser != null) {
            member = memberRepository.get(token.getCompanyId(), consumerUser.getMemberId());
            /*for (ConsumerMemberStatus memberStatus : consumerUser.getMemberStatuses()) {
                if (memberStatus.getShopId().equalsIgnoreCase(token.getShopId())) {
                    member = memberRepository.get(token.getCompanyId(), memberStatus.getMemberId());
                }
            }*/
        }

        if (member == null) {
            throw new BlazeInvalidArgException("ConsumerCart", "Member does not exist for cart.");
        }

        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());

        final CustomerInfo customerInfo = getMemberGroup(member.getId());
        if (shop.isEnableCannabisLimit()) {
            Cart myCart = dbConsumerCart.getCart();
            if (request.isCartUpdated() && request.getConsumerCart() != null) {
                myCart = request.getConsumerCart().getCart();

                //cartService.prepareCart(shop,trans, false, Transaction.TransactionStatus.Hold,false, customerInfo,true);
            }
            boolean status = cannabisLimitService.checkCannabisLimit(token.getCompanyId(), shop, member.getId(), myCart, null, false);
            if (!status) {
                throw new BlazeInvalidArgException("CannabisLimit", "State Cannabis Limit is reached.");
            }
        }
        //If there are any update in cart then update consumer cart with requested cart
        if (request.isCartUpdated() && request.getConsumerCart() != null) {
            dbConsumerCart.setCart(request.getConsumerCart().getCart());
            consumerCartRepository.update(dbConsumerCart.getId(), dbConsumerCart);
        }


        dbConsumerCart.setMembershipAccepted(consumerUser.isAccepted());
        /*for (ConsumerMemberStatus memberStatus : consumerUser.getMemberStatuses()) {
            if (memberStatus.getShopId().equalsIgnoreCase(token.getShopId())) {
                dbConsumerCart.setMembershipAccepted(memberStatus.isAccepted());
                break;
            }
        }*/


        Transaction.QueueType queueType = dbConsumerCart.getPickupType() == ConsumerCart.ConsumerOrderPickupType.Pickup
                ? Transaction.QueueType.Special : Transaction.QueueType.Delivery;
        if (queueType == Transaction.QueueType.Special) {
            // Force delivery if shop does not have walk-in enabled
            if (!shop.isShowSpecialQueue()) {
                queueType = Transaction.QueueType.Delivery;
            }
        }


        // check if this order was accepted
        Transaction oldTransaction = transactionRepository.getTransactionByConsumerCartId(token.getCompanyId(), token.getShopId(), dbConsumerCart.getId());
        if (oldTransaction != null && oldTransaction.isActive()) {
            throw new BlazeInvalidArgException("DuplicateTransaction", "This order has already been accepted and is in the queue.");
        }

        if (shop.isRequireValidRecDate() && member.getConsumerType() != ConsumerType.AdultUse) {
            //TODO: FOR THE TIME BEING, THIS IS ENABLED FOR ALL. ENABLE THIS ONLY FOR MEDICINAL
            // check member rec
            if (!member.hasValidRecommendation()) {
                throw new BlazeInvalidArgException("Recommendation", "Member/patient is missing a recommendation or it has been expired.");
            }

        }

        if (request.isCreateTookanTask() && shop.isEnableTookan()) {
            this.createTookanTask(null, employee, member, request.getTookanTeamId(), false, false, queueType);
        }

        long activeTransCount = transactionRepository.getActiveTransactionCount(token.getShopId(), member.getId());
        if (shop.getNumAllowActiveTrans() > activeTransCount) {
            UniqueSequence sequence = uniqueSequenceRepository.getNewIdentifier(token.getCompanyId(), token.getShopId(), "TransactionPriority");
            Transaction trans = new Transaction();
            trans.prepare(token.getCompanyId());
            trans.setActive(true);
            trans.setTransNo("" + sequence.getCount());
            trans.setMemberId(member.getId());
            trans.setShopId(token.getShopId());
            trans.setQueueType(queueType);
            trans.setMemberId(member.getId());
            trans.setCheckinTime(DateTime.now().getMillis());
            trans.setSellerId(token.getActiveTopUser().getUserId());
            trans.setCreatedById(token.getActiveTopUser().getUserId());
            trans.setPriority(sequence.getCount());
            trans.setSource(dbConsumerCart.getSource());


            // create order tags
            Set<String> orderTags = new HashSet<>();
            if (dbConsumerCart.getOrderTags() != null) {
                orderTags.addAll(dbConsumerCart.getOrderTags());
            }

            orderTags.add(dbConsumerCart.getPickupType().name());

            if (member != null) {
                if (member.getAddress() != null) {
                    Address address = member.getAddress();
                    address.resetPrepare(token.getCompanyId());
                    trans.setDeliveryAddress(address);
                }
                // check verified
                if (member.getIdentifications() != null) {
                    for (Identification identification : member.getIdentifications()) {
                        if (identification != null) {
                            if (identification.isVerified()) {
                                orderTags.add("ID Verified");
                                break;
                            }
                        }
                    }
                }

                // check verified
                if (member.getRecommendations() != null) {
                    for (Recommendation recommendation : member.getRecommendations()) {
                        if (recommendation != null) {
                            if (recommendation.isVerified()) {
                                orderTags.add("REC Verified");
                                break;
                            }
                        }
                    }
                }
            }
            trans.setOrderTags(orderTags);


            if (dbConsumerCart.getCart() != null) {
                Kryo kryo = new Kryo();
                Cart cart = kryo.copy(dbConsumerCart.getCart());
                cart.resetPrepare(token.getCompanyId());


                if (cart != null) {
                    for (OrderItem orderItem : cart.getItems()) {
                        orderItem.resetPrepare(token.getCompanyId());
                        orderItem.setOrderItemId(ObjectId.get().toString());
                        if (orderItem.getTaxTable() != null) {
                            orderItem.getTaxTable().resetPrepare(token.getCompanyId());
                        }

                        if (orderItem.getTaxInfo() != null) {
                            orderItem.getTaxInfo().setId(ObjectId.get().toString());
                        }

                    }
                }

                trans.setCart(cart);
            }
            trans.setTransType(Transaction.TransactionType.Sale);
            trans.setTimeZone(token.getRequestTimeZone());
            trans.setAssignedEmployeeId(request.getEmployeeId());
            trans.setAssigned((StringUtils.isNotBlank(request.getEmployeeId())));
            trans.setConsumerCartId(dbConsumerCart.getId());
            trans.setFulfillmentStep(Transaction.FulfillmentStep.Prepare);
            trans.setMemo(dbConsumerCart.getMemo());
            trans.setPickUpDate(dbConsumerCart.getPickUpDate());
            trans.setDeliveryDate(dbConsumerCart.getDeliveryDate());
            trans.setDeliveryAddress(dbConsumerCart.getDeliveryAddress());
            trans.setCompleteAfter(dbConsumerCart.getCompleteAfter());

            String sellerTerminalId = getSellerTerminalId(shop, trans.getAssignedEmployeeId());
            trans.setTerminalId(sellerTerminalId);
            Terminal sellerTerminal = terminalRepository.getTerminalById(token.getCompanyId(), sellerTerminalId);
            trans.setCheckoutType((sellerTerminal != null && sellerTerminal.getCheckoutType() != null) ? sellerTerminal.getCheckoutType() : shop.getCheckoutType());

            // copy promoCode
            String promoCode = dbConsumerCart.getCart().getPromoCode();

            trans.setOverrideInventoryId(null);

            trans.setLocked(false);

            // Check inventory
            if (!trans.isPreparingFulfillment() && employee != null) {
                checkAvailableInventory(trans, null, "accept", employee, sellerTerminalId, Boolean.FALSE);
            }

            cartService.prepareCart(shop, trans, false, Transaction.TransactionStatus.Hold, false, customerInfo, true);

            dbConsumerCart.setAssignedEmployeeId(request.getEmployeeId());
            dbConsumerCart.setTransNo(trans.getTransNo());
            dbConsumerCart.setCartStatus(ConsumerCart.ConsumerCartStatus.Accepted);
            dbConsumerCart.setTrackingStatus(ConsumerCart.ConsumerTrackingStatus.Accepted);
            dbConsumerCart.setTransactionId(trans.getId());
            dbConsumerCart.setAccepted(true);
            dbConsumerCart.setAcceptedTime(DateTime.now().getMillis());
            if (employee != null) {
                dbConsumerCart.setEmployeeName(employee.getFirstName() + " " + ((employee.getLastName() != null && employee.getLastName().length() > 0) ? employee.getLastName().substring(0, 1) : ""));
            }
            if (request.getEta() > 0) {
                dbConsumerCart.setEta(request.getEta());
                trans.setEta(request.getEta());
            } else {
                if (shop.getOnlineStoreInfo() != null) {
                    dbConsumerCart.setEta(shop.getOnlineStoreInfo().getDefaultETA());
                    trans.setEta(shop.getOnlineStoreInfo().getDefaultETA());
                }
            }
            trans.setTrackingStatus(dbConsumerCart.getTrackingStatus());
            trans.setPreparedBy(token.getActiveTopUser().getUserId());
            // save transactions

            // remove promo code
            trans.getCart().setPromoCode(Strings.EMPTY);

            if (trans.isPreparingFulfillment()) {
                trans.setStatus(Transaction.TransactionStatus.Hold);
                transactionRepository.save(trans);
                realtimeService.sendRealTimeEvent(token.getShopId(), RealtimeService.RealtimeEventType.QueueUpdateEvent, null);

            } else {
                transactionRepository.save(trans);
                queueTransactionJob(trans.getId(), trans, Transaction.TransactionStatus.Hold, shop);
            }

            if (request.getCreateOnfleetTask() && shop.isEnableOnFleet()) {
                this.createTaskAtOnFleet(shop, employee, trans, request.getOnFleetTeamId(), member);
            }

            if (request.isCreateTookanTask() && shop.isEnableTookan()) {
                this.createTookanTask(trans, employee, member, request.getTookanTeamId(), false, true, queueType);
            }
            // maintain promocode
            dbConsumerCart.getCart().setPromoCode(promoCode);
            consumerCartRepository.update(token.getCompanyId(), dbConsumerCart.getId(), dbConsumerCart);

            realtimeService.sendRealTimeEvent(token.getShopId(),
                    RealtimeService.RealtimeEventType.ConsumerCartsUpdate, null);
            //if (ConsumerCart.TransactionSource.WooCommerce != dbConsumerCart.getTransactionSource()) {
            consumerNotificationService.sendUpdateOrderdNotification(dbConsumerCart, consumerUser, shop, NotificationInfo.NotificationType.Consumer_Accept_Order);
            //}
            if (employee != null) {
                employeeNotificationService.sendAssignedOrderNotification(trans, employee.getId());
            }
            updateConsumerOrderWebHook(dbConsumerCart);

            return trans;
        } else {
            if (shop.getNumAllowActiveTrans() > 1) {
                throw new BlazeInvalidArgException("Transaction", String.format("Member has %s active orders in queue. Please complete current order(s) before accepting new one.", shop.getNumAllowActiveTrans()));
            } else {
                throw new BlazeInvalidArgException("Transaction", "Member is already in the queue. Please complete current order before accepting new one.");
            }
        }
    }

    @Override
    public ConsumerCartResult declineIncomingOrder(String consumerCartId, DeclineRequest declineRequest) {
        ConsumerCartResult dbConsumertCart = consumerCartRepository.get(token.getCompanyId(), consumerCartId, ConsumerCartResult.class);
        if (dbConsumertCart == null) {
            throw new BlazeInvalidArgException("ConsumerCart", "Consumer cart does not exist.");
        }


        dbConsumertCart.setReason(declineRequest.getReason());
        dbConsumertCart.setCartStatus(ConsumerCart.ConsumerCartStatus.Declined);
        dbConsumertCart.setTrackingStatus(ConsumerCart.ConsumerTrackingStatus.Declined);
        dbConsumertCart.setDeclinedTime(DateTime.now().getMillis());
        consumerCartRepository.update(token.getCompanyId(), dbConsumertCart.getId(), (ConsumerCart) dbConsumertCart);


        ConsumerUser consumerUser = consumerUserRepository.getById(dbConsumertCart.getConsumerId());

        dbConsumertCart.setMembershipAccepted(consumerUser.isAccepted());
        /*for (ConsumerMemberStatus memberStatus : consumerUser.getMemberStatuses()) {
            if (memberStatus.getShopId().equalsIgnoreCase(token.getShopId())) {
                dbConsumertCart.setMembershipAccepted(memberStatus.isAccepted());
                break;
            }
        }*/

        realtimeService.sendRealTimeEvent(token.getShopId(),
                RealtimeService.RealtimeEventType.ConsumerCartsUpdate, null);
        Shop shop = shopRepository.get(token.getCompanyId(), dbConsumertCart.getShopId());
        //if (ConsumerCart.TransactionSource.WooCommerce != dbConsumertCart.getTransactionSource()) {
        consumerNotificationService.sendUpdateOrderdNotification(dbConsumertCart, consumerUser, shop, NotificationInfo.NotificationType.Consumer_Update_Order);
        // }
        updateConsumerOrderWebHook(dbConsumertCart);
        return dbConsumertCart;
    }


    @Override
    public ConsumerCartResult acceptIncomingMemberStatus(String consumerCartId, ConsumerCartResult inConsumerCart) {
        ConsumerCartResult dbConsumerCart = consumerCartRepository.get(token.getCompanyId(), consumerCartId, ConsumerCartResult.class);
        if (dbConsumerCart == null) {
            throw new BlazeInvalidArgException("ConsumerCart", "Consumer cart does not exist.");
        }

        ConsumerUser dbConsumerUser = consumerUserRepository.getById(dbConsumerCart.getConsumerId());
        if (dbConsumerUser == null) {
            throw new BlazeInvalidArgException("ConsumerUser", "Consumer user does not exist.");
        }


        dbConsumerUser.prepare(token.getCompanyId());

        /*ConsumerMemberStatus memberStatus = null;
        for (ConsumerMemberStatus status : dbConsumerUser.getMemberStatuses()) {
            if (status.getShopId().equalsIgnoreCase(token.getShopId())) {
                memberStatus = status;
                break;
            }
        }
        if (memberStatus == null) {
            // new one
            memberStatus = new ConsumerMemberStatus();
            memberStatus.prepare(token.getCompanyId());
            memberStatus.setShopId(token.getShopId());
            memberStatus.setAccepted(true);
            memberStatus.setAcceptedDate(DateTime.now().getMillis());
            dbConsumerUser.getMemberStatuses().add(memberStatus);
        } else {
            // Update only if it was previously declined
            if (!memberStatus.isAccepted()) {
                memberStatus.prepare(token.getCompanyId());
                memberStatus.setShopId(token.getShopId());
                memberStatus.setAccepted(true);
                memberStatus.setAcceptedDate(DateTime.now().getMillis());
                memberStatus.setReason("");
            }
        }
        memberStatus.setLastSyncDate(DateTime.now().getMillis());*/
        // Get Member
        // set the fields accordingly
        ConsumerUser consumerUser = (new Kryo()).copy(dbConsumerUser);
        ConsumerUser inUser = null;
        if (inConsumerCart != null && inConsumerCart.getConsumerUser() != null) {
            inUser = inConsumerCart.getConsumerUser();
        }

        Member member = acceptOrUpdateConsumer(consumerUser, inUser, dbConsumerCart.getMemberId());

        if (member != null) {
            dbConsumerCart.setMemberId(member.getId());
            dbConsumerCart.setMembershipAccepted(true);
        }


        consumerCartRepository.update(token.getCompanyId(), dbConsumerCart.getId(), dbConsumerCart);

        realtimeService.sendRealTimeEvent(token.getShopId(),
                RealtimeService.RealtimeEventType.ConsumerCartsUpdate, null);
        dbConsumerCart.setMembershipAccepted(true);
        dbConsumerCart.setConsumerUser(consumerUser);
        //dbConsumerCart.setMemberId(member.getId());
        return dbConsumerCart;
    }

    private Member createOrUpdateMember(String consumerCartMemberId, ConsumerUser consumerUser) {
        Member member = null;
        if (StringUtils.isNotBlank(consumerUser.getMemberId())) {
            member = memberRepository.get(token.getCompanyId(), consumerUser.getMemberId());
        }
        /*if (consumerUser.getMemberStatuses() != null) {
            String memberId = null;
            if (token.getMembersShareOption() == Company.CompanyMembersShareOption.Shared) {
                memberId = consumerUser.getMemberIdForCompany(token.getCompanyId());
            } else {
                memberId = consumerUser.getMemberIdForShop(token.getShopId());
            }
            member = memberRepository.get(token.getCompanyId(), memberId);
        }*/

        if (member == null) {
            member = memberRepository.get(token.getCompanyId(), consumerCartMemberId);

            if (member == null) {
                // Find member by consumerId
                member = memberRepository.getMemberWithConsumerId(token.getCompanyId(), consumerUser.getId());

                if (member == null) {
                    // Get new member by email
                    Iterable<Member> membersByEmail = memberRepository.getMemberByEmail(token.getCompanyId(),
                            consumerUser.getEmail().toLowerCase());
                    for (Member m : membersByEmail) {
                        member = m;
                        break;
                    }
                }
            }
        }


        // If member is still null
        if (member == null) {
            // Then create a new member
            MemberGroup defaultGroup = memberGroupRepository.getDefaultMemberGroup(token.getCompanyId());

            member = new Member();
            member.setStatus(Member.MembershipStatus.Pending);
            member.prepare(token.getCompanyId());
            member.setShopId(token.getShopId());
            member.setAddress(consumerUser.getAddress());
            if (member.getAddress() == null) {
                Address address = new Address();
                address.prepare(token.getCompanyId());
                member.setAddress(address);
            } else {
                member.getAddress().resetPrepare(token.getCompanyId());
            }
            if (defaultGroup != null) {
                member.setMemberGroupId(defaultGroup.getId());
                member.setMemberGroup(defaultGroup);
            }
            member.setFirstName(consumerUser.getFirstName());
            member.setLastName(consumerUser.getLastName());
            member.setMiddleName(consumerUser.getMiddleName());
            member.setDob(consumerUser.getDob());
            member.setEmail(consumerUser.getEmail());
            member.setPrimaryPhone(consumerUser.getPrimaryPhone());
            member.setTextOptIn(consumerUser.isTextOptIn());
            member.setEmailOptIn(consumerUser.isEmailOptIn());
            member.setMedical(consumerUser.isMedical());
            member.setSex(consumerUser.getSex());
            member.setStartDate(DateTime.now().getMillis());
            member.setConsumerType(consumerUser.getConsumerType());
            if (member.getConsumerType() == ConsumerType.AdultUse) {
                if (member.isMedical() && consumerUser.getRecPhoto() != null && consumerUser.getConsumerType() == null) {
                    member.setConsumerType(ConsumerType.MedicinalThirdParty);
                }
            }
            // Drivers License
            Identification identification = new Identification();
            identification.prepare(token.getCompanyId());
            identification.setExpirationDate(consumerUser.getDlExpiration());
            identification.setState(consumerUser.getDlState());
            identification.setLicenseNumber(consumerUser.getDlNo());
            identification.setVerified(true);
            if (consumerUser.getDlPhoto() != null) {
                // check if this key exist for this company
                CompanyAsset dlPhoto = consumerUser.getDlPhoto().toCompanyAsset(token.getCompanyId());

                CompanyAsset dbCompanyAsset = companyAssetRepository.getAssetByKey(token.getCompanyId(), dlPhoto.getKey());
                if (dbCompanyAsset == null) {
                    // this is a new asset from the customer
                    companyAssetRepository.save(dlPhoto);
                }
                identification.setFrontPhoto(dlPhoto);


                // Refresh assets just in case
                List<CompanyAsset> assets = new ArrayList<>();
                assets.add(dlPhoto);
                identification.setAssets(assets);
            }
            member.getIdentifications().add(identification);

            // Recommendation
            Recommendation recommendation = new Recommendation();
            recommendation.prepare(token.getCompanyId());
            recommendation.setState(consumerUser.getDlState());

            if (consumerUser.getRecPhoto() != null) {
                CompanyAsset recPhoto = consumerUser.getRecPhoto().toCompanyAsset(token.getCompanyId());

                CompanyAsset dbCompanyAsset = companyAssetRepository.getAssetByKey(token.getCompanyId(), recPhoto.getKey());
                if (dbCompanyAsset == null) {
                    // this is a new asset from the customer
                    companyAssetRepository.save(recPhoto);
                }

                recommendation.setFrontPhoto(recPhoto);

                // Refresh assets just in case
                List<CompanyAsset> assets = new ArrayList<>();
                assets.add(recPhoto);
                recommendation.setAssets(assets);
            }
            recommendation.setRecommendationNumber(consumerUser.getRecNo());
            recommendation.setExpirationDate(consumerUser.getRecExpiration());
            recommendation.setIssueDate(consumerUser.getRecIssueDate());
            recommendation.setVerifyPhoneNumber(consumerUser.getVerificationPhone());
            recommendation.setVerifyWebsite(consumerUser.getVerificationWebsite());
            recommendation.setVerified(consumerUser.isVerified());

            // Doctor
            createOrUpdateDoctor(recommendation, consumerUser);


            member.getRecommendations().add(recommendation);

            member.setConsumerUserId(consumerUser.getId());
            member.setContracts(consumerUser.getSignedContracts());

            if (StringUtils.isNotBlank(consumerUser.getSourceCompanyId())
                    && consumerUser.getSourceCompanyId().equalsIgnoreCase(token.getCompanyId())) {
                member.setMarketingSource(consumerUser.getMarketingSource());
            }

            if(consumerUser.getAddresses() != null && consumerUser.getAddresses().size() > 0) {
                member.setAddresses(consumerUser.getAddresses());
            }
            // Save member
            memberRepository.save(member);
            elasticSearchManager.createOrUpdateIndexedDocument(member);
            realtimeService.sendRealTimeEvent(token.getShopId(), RealtimeService.RealtimeEventType.MembersUpdateEvent, null);

        } else {
            // Update
            member.setFirstName(consumerUser.getFirstName());
            member.setLastName(consumerUser.getLastName());
            member.setPrimaryPhone(consumerUser.getPrimaryPhone());
            member.setTextOptIn(consumerUser.isTextOptIn());
            member.setEmailOptIn(consumerUser.isEmailOptIn());
            member.setConsumerUserId(consumerUser.getId());

            if (consumerUser.getAddress() != null
                    && StringUtils.isNotBlank(consumerUser.getAddress().getZipCode())) {
                member.setAddress(consumerUser.getAddress());
                member.getAddress().setId(ObjectId.get().toString());
                member.getAddress().setCompanyId(token.getCompanyId());
            }


            if (StringUtils.isNotBlank(consumerUser.getSourceCompanyId())
                    && consumerUser.getSourceCompanyId().equalsIgnoreCase(token.getCompanyId())) {
                member.setMarketingSource(consumerUser.getMarketingSource());
            }

            if (member.getIdentifications().size() == 0) {
                Identification identification = new Identification();
                identification.setCompanyId(token.getCompanyId());
                identification.setExpirationDate(consumerUser.getDlExpiration());
                identification.setState(consumerUser.getDlState());
                identification.setLicenseNumber(consumerUser.getDlNo());
                identification.setVerified(true);
                if (consumerUser.getDlPhoto() != null) {
                    CompanyAsset dlPhoto = consumerUser.getDlPhoto().toCompanyAsset(token.getCompanyId());
                    companyAssetRepository.save(dlPhoto);
                    identification.setFrontPhoto(dlPhoto);

                    List<CompanyAsset> companyAssets = identification.getAssets();
                    if (companyAssets == null) {
                        companyAssets = new ArrayList<>();
                    }
                    companyAssets.add(dlPhoto);
                    identification.setAssets(companyAssets);
                }
                member.getIdentifications().add(identification);

            } else {
                Identification identification = member.getIdentifications().get(0);
                if (consumerUser.getDlExpiration() != null &&
                        identification.getExpirationDate() != consumerUser.getDlExpiration()) {
                    identification.setExpirationDate(consumerUser.getDlExpiration());
                    identification.setState(consumerUser.getDlState());
                    identification.setLicenseNumber(consumerUser.getDlNo());
                    if (consumerUser.getDlPhoto() != null) {
                        CompanyAsset dlPhoto = consumerUser.getDlPhoto().toCompanyAsset(token.getCompanyId());
                        try {
                            companyAssetRepository.save(dlPhoto);
                            identification.setFrontPhoto(dlPhoto);


                            List<CompanyAsset> companyAssets = identification.getAssets();
                            if (companyAssets == null) {
                                companyAssets = new ArrayList<>();
                            }
                            companyAssets.add(dlPhoto);
                            identification.setAssets(companyAssets);
                        } catch (Exception e) {
                            // Ignore
                        }
                    }
                }
            }

            Recommendation recommendation = null;
            if (member.getRecommendations().size() > 0) {
                // Recommendation
                recommendation = member.getRecommendations().get(0);

                if (consumerUser.getRecExpiration() != null
                        && StringUtils.isNotBlank(consumerUser.getRecNo())
                        && !consumerUser.getRecNo().equalsIgnoreCase(recommendation.getRecommendationNumber())) {
                    recommendation.setState(consumerUser.getDlState());

                    if (consumerUser.getRecPhoto() != null) {
                        CompanyAsset recPhoto = consumerUser.getRecPhoto().toCompanyAsset(token.getCompanyId());
                        companyAssetRepository.save(recPhoto);
                        recommendation.setFrontPhoto(recPhoto);


                        List<CompanyAsset> companyAssets = recommendation.getAssets();
                        if (companyAssets == null) {
                            companyAssets = new ArrayList<>();
                        }
                        companyAssets.add(recPhoto);
                        recommendation.setAssets(companyAssets);
                    }
                    recommendation.setRecommendationNumber(consumerUser.getRecNo());
                    recommendation.setExpirationDate(consumerUser.getRecExpiration());
                    recommendation.setIssueDate(consumerUser.getRecIssueDate());
                    recommendation.setVerifyPhoneNumber(consumerUser.getVerificationPhone());
                    recommendation.setVerifyWebsite(consumerUser.getVerificationWebsite());


                    // Doctor
                    createOrUpdateDoctor(recommendation, consumerUser);

                }
            }
            if (recommendation == null) {
                recommendation = new Recommendation();
                recommendation.prepare(token.getCompanyId());
                recommendation.setState(consumerUser.getDlState());

                if (consumerUser.getRecPhoto() != null) {
                    CompanyAsset recPhoto = consumerUser.getRecPhoto().toCompanyAsset(token.getCompanyId());
                    companyAssetRepository.save(recPhoto);
                    recommendation.setFrontPhoto(recPhoto);


                    List<CompanyAsset> companyAssets = recommendation.getAssets();
                    if (companyAssets == null) {
                        companyAssets = new ArrayList<>();
                    }
                    companyAssets.add(recPhoto);
                    recommendation.setAssets(companyAssets);
                }
                recommendation.setRecommendationNumber(consumerUser.getRecNo());
                recommendation.setExpirationDate(consumerUser.getRecExpiration());
                recommendation.setIssueDate(consumerUser.getRecIssueDate());
                recommendation.setVerifyPhoneNumber(consumerUser.getVerificationPhone());
                recommendation.setVerifyWebsite(consumerUser.getVerificationWebsite());

                member.getRecommendations().add(recommendation);


                // Doctor
                createOrUpdateDoctor(recommendation, consumerUser);
            }

            member.setConsumerType(consumerUser.getConsumerType());
            if (member.getConsumerType() == ConsumerType.AdultUse) {
                if (member.isMedical() || member.getRecommendations().size() > 0) {
                    member.setConsumerType(ConsumerType.MedicinalThirdParty);
                }
            }

            // Update signed contracts
            for (SignedContract signedContract : consumerUser.getSignedContracts()) {
                // found
                boolean found = false;
                for (SignedContract currentSC : member.getContracts()) {
                    if (currentSC.getContractId().equalsIgnoreCase(signedContract.getContractId())) {
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    member.getContracts().add(signedContract);
                }
            }

            if(consumerUser.getAddresses() != null && consumerUser.getAddresses().size() > 0) {
                member.setAddresses(consumerUser.getAddresses());
            }
            // Update
            memberRepository.update(token.getCompanyId(), member.getId(), member);
            elasticSearchManager.createOrUpdateIndexedDocument(member);
            realtimeService.sendRealTimeEvent(token.getShopId(), RealtimeService.RealtimeEventType.MembersUpdateEvent, null);

        }

        return member;
    }

    private Recommendation createOrUpdateDoctor(Recommendation recommendation, ConsumerUser consumerUser) {

        // Doctor
        if (StringUtils.isNotBlank(consumerUser.getDoctorLicense())) {
            Doctor doctor = doctorRepository.getDoctorByLicense(token.getCompanyId(), consumerUser.getDoctorLicense());
            if (doctor != null) {
                recommendation.setDoctor(doctor);
                recommendation.setDoctorId(doctor.getId());
                consumerUser.setDoctorFirstName(doctor.getFirstName());
                consumerUser.setDoctorLastName(doctor.getLastName());
                consumerUser.setVerificationWebsite(doctor.getWebsite());
                consumerUser.setVerificationPhone(doctor.getPhoneNumber());
            } else {
                // Create a new doctor
                doctor = new Doctor();
                doctor.setCompanyId(token.getCompanyId());
                doctor.setFirstName(consumerUser.getDoctorFirstName());
                doctor.setLastName(consumerUser.getDoctorLastName());
                doctor.setLicense(consumerUser.getDoctorLicense());
                doctor.setWebsite(consumerUser.getVerificationWebsite());
                doctor.setPhoneNumber(consumerUser.getVerificationPhone());
                doctor.setActive(true);
                doctorRepository.save(doctor);
                recommendation.setDoctor(doctor);
                recommendation.setDoctorId(doctor.getId());
                realtimeService.sendRealTimeEvent(token.getShopId(), RealtimeService.RealtimeEventType.DoctorsUpdateEvent, doctor);

            }
        }
        return recommendation;
    }

    @Override
    public ConsumerCartResult declineIncomingMemberStatus(String consumerCartId, DeclineRequest request) {
        ConsumerCartResult dbConsumerCart = consumerCartRepository.get(token.getCompanyId(), consumerCartId, ConsumerCartResult.class);
        if (dbConsumerCart == null) {
            throw new BlazeInvalidArgException("ConsumerCart", "Consumer cart does not exist.");
        }

        ConsumerUser consumerUser = consumerUserRepository.getById(dbConsumerCart.getConsumerId());
        if (consumerUser == null) {
            throw new BlazeInvalidArgException("ConsumerUser", "Consumer user does not exist.");
        }

        dbConsumerCart.setDeclinedTime(DateTime.now().getMillis());
        dbConsumerCart.setCartStatus(ConsumerCart.ConsumerCartStatus.Declined);
        dbConsumerCart.setTrackingStatus(ConsumerCart.ConsumerTrackingStatus.Declined);

        consumerUser.setAccepted(Boolean.FALSE);
        consumerUser.setRejectedDate(DateTime.now().getMillis());
        consumerUser.setReason(request.getReason());
        consumerUserRepository.update(consumerUser.getId(), consumerUser);

        /*ConsumerMemberStatus memberStatus = null;
        for (ConsumerMemberStatus status : consumerUser.getMemberStatuses()) {
            if (status.getShopId().equalsIgnoreCase(token.getShopId())) {
                memberStatus = status;
                break;
            }
        }

        consumerUser.prepare(token.getCompanyId());
        if (Objects.isNull(memberStatus)) {
            // new one
            memberStatus = new ConsumerMemberStatus();
            memberStatus.prepare(token.getCompanyId());
            memberStatus.setShopId(token.getShopId());
            memberStatus.setAccepted(false);
            memberStatus.setAcceptedDate(null);
            memberStatus.setReason(request.getReason());
            consumerUser.getMemberStatuses().add(memberStatus);

            consumerUserRepository.update(consumerUser.getId(), consumerUser);
        } else {
            // Update only if it was previously declined
            memberStatus.setAccepted(false);
            memberStatus.setAcceptedDate(null);
            memberStatus.setReason(request.getReason());

            consumerUserRepository.update(consumerUser.getId(), consumerUser);
        }*/

        consumerCartRepository.update(token.getCompanyId(), dbConsumerCart.getId(), (ConsumerCart) dbConsumerCart);

        realtimeService.sendRealTimeEvent(token.getShopId(),
                RealtimeService.RealtimeEventType.ConsumerCartsUpdate, null);

        dbConsumerCart.setMembershipAccepted(true);
        dbConsumerCart.setMemberId(null);
        return dbConsumerCart;
    }

    @Override
    public ConsumerCartResult undoRejectedIncomingOrder(String consumerCartId) {
        ConsumerCartResult dbConsumerCart = consumerCartRepository.get(token.getCompanyId(), consumerCartId, ConsumerCartResult.class);
        if (dbConsumerCart == null) {
            throw new BlazeInvalidArgException("ConsumerCart", "Consumer cart does not exist.");
        }

        ConsumerUser consumerUser = consumerUserRepository.getById(dbConsumerCart.getConsumerId());
        if (consumerUser == null) {
            throw new BlazeInvalidArgException("ConsumerUser", "Consumer user does not exist.");
        }

        Transaction transaction = transactionRepository.get(token.getCompanyId(), dbConsumerCart.getTransactionId());
        if (transaction != null) {
            if (transaction.getStatus() == Transaction.TransactionStatus.Canceled) {
                throw new BlazeInvalidArgException("ConsumerCart", "This order has been completed previously.");
            }
            transaction.setStatus(Transaction.TransactionStatus.Hold);
            transaction.setActive(true);
            dbConsumerCart.setCartStatus(ConsumerCart.ConsumerCartStatus.Accepted);
            dbConsumerCart.setTrackingStatus(ConsumerCart.ConsumerTrackingStatus.Accepted);

            transactionRepository.update(token.getCompanyId(), transaction.getId(), transaction);

            realtimeService.sendRealTimeEvent(token.getShopId(),
                    RealtimeService.RealtimeEventType.QueueUpdateEvent, null);
        } else {
            dbConsumerCart.setCartStatus(ConsumerCart.ConsumerCartStatus.Placed);
            dbConsumerCart.setTrackingStatus(ConsumerCart.ConsumerTrackingStatus.Placed);
        }

        consumerCartRepository.update(token.getCompanyId(), dbConsumerCart.getId(), (ConsumerCart) dbConsumerCart);

        realtimeService.sendRealTimeEvent(token.getShopId(),
                RealtimeService.RealtimeEventType.ConsumerCartsUpdate, null);
        dbConsumerCart.setConsumerUser(consumerUser);
        updateConsumerOrderWebHook(dbConsumerCart);
        return dbConsumerCart;
    }

    @Override
    public ConsumerCartResult updateTrackingStatus(String consumerCartId, ConsumerOrderUpdateETAStatusRequest request) {
        ConsumerCartResult dbConsumerCart = consumerCartRepository.get(token.getCompanyId(), consumerCartId, ConsumerCartResult.class);
        if (dbConsumerCart == null) {
            throw new BlazeInvalidArgException("ConsumerCart", "Consumer cart does not exist.");
        }
        ConsumerUser consumerUser = consumerUserRepository.getById(dbConsumerCart.getConsumerId());
        if (consumerUser == null) {
            throw new BlazeInvalidArgException("ConsumerUser", "Consumer user does not exist.");
        }

        consumerUser.setPassword(null);
        dbConsumerCart.setTrackingStatus(request.getTrackingStatus());
        dbConsumerCart.setEta(request.getEta());

        if (request.getTrackingStatus() == ConsumerCart.ConsumerTrackingStatus.Packaged) {
            dbConsumerCart.setPackagedTime(DateTime.now().getMillis());
            dbConsumerCart.setPackaged(true);
        } else if (request.getTrackingStatus() == ConsumerCart.ConsumerTrackingStatus.OnTheWay) {
            dbConsumerCart.setOnTheWayTime(DateTime.now().getMillis());
            dbConsumerCart.setOnTheWay(true);

            if (dbConsumerCart.getPackagedTime() == null || dbConsumerCart.getPackagedTime() <= 0) {
                dbConsumerCart.setPackagedTime(DateTime.now().getMillis());
                dbConsumerCart.setPackaged(true);
            }
        } else if (request.getTrackingStatus() == ConsumerCart.ConsumerTrackingStatus.WaitingPickup) {

        }

        Transaction transaction = transactionRepository.get(dbConsumerCart.getCompanyId(), dbConsumerCart.getTransactionId());
        if (transaction != null) {
            transaction.setTrackingStatus(request.getTrackingStatus());
            transaction.setEta(request.getEta());
            transactionRepository.update(dbConsumerCart.getCompanyId(), transaction.getId(), transaction);

            realtimeService.sendRealTimeEvent(token.getShopId(), RealtimeService.RealtimeEventType.QueueUpdateEvent, null);
        }

        consumerCartRepository.update(token.getCompanyId(), dbConsumerCart.getId(), dbConsumerCart);

        realtimeService.sendRealTimeEvent(token.getShopId(),
                RealtimeService.RealtimeEventType.ConsumerCartsUpdate, null);
        dbConsumerCart.setConsumerUser(consumerUser);

        Shop shop = shopRepository.get(token.getCompanyId(), dbConsumerCart.getShopId());
        //if (ConsumerCart.TransactionSource.WooCommerce != dbConsumerCart.getTransactionSource()) {
        consumerNotificationService.sendUpdateOrderdNotification(dbConsumerCart, consumerUser, shop, NotificationInfo.NotificationType.Consumer_Update_Order);
        //}
        updateConsumerOrderWebHook(dbConsumerCart);
        return dbConsumerCart;
    }

    public void queueTransactionJob(String transactionId, Transaction transaction, Transaction.TransactionStatus status, Shop shop) {
        queueTransactionJob(transactionId, transaction, status, shop, token.getActiveTopUser().getUserId(), false, "");
    }

    private void queueRefundJob(String transactionId,
                                RefundTransactionRequest refundTransactionRequest,
                                String sellerId,
                                String terminalId) {

        QueuedTransaction queuedTransaction = new QueuedTransaction();
        queuedTransaction.setShopId(token.getShopId());
        queuedTransaction.setCompanyId(token.getCompanyId());
        queuedTransaction.setId(ObjectId.get().toString());
        if (refundTransactionRequest.isWithInventory()) {
            queuedTransaction.setPendingStatus(Transaction.TransactionStatus.RefundWithInventory);
        } else {
            queuedTransaction.setPendingStatus(Transaction.TransactionStatus.RefundWithoutInventory);
        }
        queuedTransaction.setStatus(QueuedTransaction.QueueStatus.Pending);
        queuedTransaction.setQueuedTransType(QueuedTransaction.QueuedTransactionType.RefundRequest);
        queuedTransaction.setSellerId(sellerId);
        queuedTransaction.setMemberId(null);
        queuedTransaction.setTerminalId(terminalId); // User the current terminal
        queuedTransaction.setTransactionId(transactionId);
        queuedTransaction.setRequestTime(DateTime.now().getMillis());

        queuedTransaction.setReassignTransfer(false);
        queuedTransaction.setNewTransferInventoryId(null);

        queuedTransactionRepository.save(queuedTransaction);

        backgroundJobService.addTransactionJob(token.getCompanyId(), token.getShopId(), queuedTransaction.getId());
    }

    private void queueTransactionJob(String transactionId, Transaction transaction, Transaction.TransactionStatus status, Shop shop, String sellerId, boolean transferItems, String transferInventoryId) {
        String sellerTerminalId = getSellerTerminalId(shop, transaction.getAssignedEmployeeId());

        QueuedTransaction queuedTransaction = new QueuedTransaction();
        queuedTransaction.setShopId(token.getShopId());
        queuedTransaction.setCompanyId(token.getCompanyId());
        queuedTransaction.setId(ObjectId.get().toString());
        queuedTransaction.setCart(transaction.getCart());
        queuedTransaction.setPendingStatus(status);
        queuedTransaction.setStatus(QueuedTransaction.QueueStatus.Pending);
        queuedTransaction.setQueuedTransType(QueuedTransaction.QueuedTransactionType.SalesTrans);
        queuedTransaction.setSellerId(sellerId);
        queuedTransaction.setMemberId(transaction.getMemberId());
        queuedTransaction.setTerminalId(sellerTerminalId); // User the current terminal
        queuedTransaction.setTransactionId(transactionId);
        queuedTransaction.setRequestTime(DateTime.now().getMillis());

        queuedTransaction.setReassignTransfer(transferItems);
        queuedTransaction.setNewTransferInventoryId(transferInventoryId);

        queuedTransactionRepository.save(queuedTransaction);

        backgroundJobService.addTransactionJob(token.getCompanyId(), token.getShopId(), queuedTransaction.getId());
    }

    private void queuePrepareTransactionJob(String transactionId, Transaction transaction, Transaction.TransactionStatus status,
                                            Shop shop,
                                            String sellerId,
                                            boolean transferItems, String transferInventoryId) {
        String sellerTerminalId = getSellerTerminalId(shop, transaction.getAssignedEmployeeId());

        QueuedTransaction queuedTransaction = new QueuedTransaction();
        queuedTransaction.setShopId(token.getShopId());
        queuedTransaction.setCompanyId(token.getCompanyId());
        queuedTransaction.setId(ObjectId.get().toString());
        queuedTransaction.setCart(transaction.getCart());
        queuedTransaction.setPendingStatus(status);
        queuedTransaction.setStatus(QueuedTransaction.QueueStatus.Prepare);
        queuedTransaction.setSellerId(sellerId);
        queuedTransaction.setMemberId(transaction.getMemberId());
        queuedTransaction.setTerminalId(sellerTerminalId); // User the current terminal
        queuedTransaction.setTransactionId(transactionId);
        queuedTransaction.setRequestTime(0);

        queuedTransaction.setReassignTransfer(transferItems);
        queuedTransaction.setNewTransferInventoryId(transferInventoryId);

        queuedTransactionRepository.save(queuedTransaction);
    }

    @Override
    public void sendTransactionReceipt(String transactionId, SendReceiptRequest receiptRequest, Boolean isDeliveryManifest) {
        Transaction dbTrans = transactionRepository.get(token.getCompanyId(), transactionId);
        if (dbTrans == null) {
            throw new BlazeInvalidArgException("TransactionId", "Transaction does not exist.");
        }

        String email = "";
        Member member = null;
        if (Transaction.TransactionType.Adjustment != dbTrans.getTransType() && Transaction.TransactionType.Transfer != dbTrans.getTransType()
                && (Transaction.TransactionStatus.Canceled != null && StringUtils.isNotBlank(dbTrans.getMemberId()))) {
            member = memberRepository.get(token.getCompanyId(), dbTrans.getMemberId());
            if (member == null) {
                throw new BlazeInvalidArgException("Member", "Member does not exist.");
            }
        }

        if (member != null && (member.getEmail() == null || StringUtils.isBlank(member.getEmail()))) {
            if (StringUtils.isNotBlank(receiptRequest.getEmail())) {
                member.setEmail(receiptRequest.getEmail().toLowerCase().trim());
                member = memberRepository.update(token.getCompanyId(), member.getId(), member);
                elasticSearchManager.createOrUpdateIndexedDocument(member);
                realtimeService.sendRealTimeEvent(token.getShopId(), RealtimeService.RealtimeEventType.MembersUpdateEvent, null);
            }
        }

        if (member != null && (receiptRequest == null || StringUtils.isBlank(receiptRequest.getEmail()))) {
            email = member.getEmail();
        } else {
            email = receiptRequest.getEmail();
        }

        List<ObjectId> objectIds = new ArrayList<>();
        Set<ObjectId> brandIds = new HashSet<>();
        for (OrderItem item : dbTrans.getCart().getItems()) {
            objectIds.add(new ObjectId((item.getProductId())));
        }
        HashMap<String, Product> productHashMap = productRepository.findProductsByIdsAsMap(token.getCompanyId(), token.getShopId(), objectIds);

        for (Product product : productHashMap.values()) {
            if (StringUtils.isNotBlank(product.getBrandId()) && ObjectId.isValid(product.getBrandId())) {
                brandIds.add(new ObjectId(product.getBrandId()));
            }
        }

        HashMap<String, Brand> brandHashMap = brandRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(brandIds));

        String bccEmail = null;
        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());
        if (shop != null && shop.isEnableBCCReceipt()) {
            bccEmail = shop.getBccEmailAddress();
        }

        Company company = companyRepository.getById(token.getCompanyId());

        String fromEMail = "";
        if (shop != null && StringUtils.isNotBlank(shop.getEmailAdress())) {
            fromEMail = shop.getEmailAdress();
        } else {
            fromEMail = "support@blaze.me";
        }

        if (StringUtils.isNotEmpty(email)) {
            // send receipt inf
            InputStream inputStream = ShopServiceImpl.class.getResourceAsStream("/receipt.html");
            StringWriter writer = new StringWriter();
            try {
                IOUtils.copy(inputStream, writer, "UTF-8");
            } catch (IOException e) {
                e.printStackTrace();
            }
            String body;
            if (isDeliveryManifest) {
                InputStream inputStreamDriverManifest = ShopServiceImpl.class.getResourceAsStream("/driver_manifest.html");
                StringWriter writerDriverManifest = new StringWriter();
                try {
                    IOUtils.copy(inputStreamDriverManifest, writerDriverManifest, "UTF-8");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                body = writerDriverManifest.toString();
                body = generateManifestBody(writerDriverManifest, company, member, dbTrans, productHashMap, shop, brandHashMap);
            } else {
                body = writer.toString();
                String orderDetails = getReceiptBody(company, member, dbTrans, productHashMap, shop, brandHashMap);
                body = body.replace("==orderInformationDetails==", orderDetails);
            }
            if (!Objects.isNull(shop.getEmailAttachment())) {
                try {
                    CompanyAsset assetByKey = shop.getEmailAttachment();
                    if (assetByKey == null) {
                        throw new BlazeInvalidArgException("CompanyAsset", "Attachment does not exist.");
                    }
                    AssetStreamResult assetStreamResult = amazonS3Service.downloadFile(assetByKey.getKey(), true);
                    if (assetStreamResult == null) {
                        throw new BlazeInvalidArgException("AssetStreamResult", "Attachment does not exist.");
                    }

                    InputStream stream = assetStreamResult.getStream();
                    HashMap<String, HashMap<String, InputStream>> attachmentMap = new HashMap<>();
                    HashMap<String, InputStream> attachmentList = new HashMap<>();
                    attachmentList.put(assetByKey.getName(), stream);
                    attachmentMap.put("attachment", attachmentList);
                    amazonServiceManager.sendMultiPartEmail(fromEMail, email, company.getName() + " Receipt", body, bccEmail, null, attachmentMap, assetStreamResult.getContentType(), assetByKey.getName(), shop.getName());
                } catch (Exception e) {
                    amazonServiceManager.sendEmail(fromEMail, email, company.getName() + " Receipt", body, bccEmail, null, shop.getName());
                }
            } else {
                amazonServiceManager.sendEmail(fromEMail, email, company.getName() + " Receipt", body, bccEmail, null, shop.getName());
            }
        }
    }

    private String getReceiptBody(Company company, Member member, Transaction transaction, HashMap<String,
            Product> productHashMap, Shop shop, HashMap<String, Brand> brandHashMap) {
        InputStream inputStream = ShopServiceImpl.class.getResourceAsStream("/receipt-details.html");
        StringWriter writer = new StringWriter();
        try {
            IOUtils.copy(inputStream, writer, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }

        ReceiptInfo.ReceiptType receiptType = null;
        if (transaction.isPreparingFulfillment()) {
            receiptType = ReceiptInfo.ReceiptType.FULFILLMENT;
        } else {
            receiptType = ReceiptInfo.ReceiptType.SALES;
        }


        ReceiptInfo receiptInfo = new ReceiptInfo();
        if (shop.getReceiptInfo() != null) {
            for (ReceiptInfo receipt : shop.getReceiptInfo()) {
                if (receipt.getReceiptType().equals(receiptType)) {
                    receiptInfo = receipt;
                }
            }
        }

        if (Objects.isNull(receiptInfo)) {
            receiptInfo = new ReceiptInfo();
        }

        DateTime dt = new DateTime();

        String date = DateUtil.toDateFormatted(dt.getMillis(), shop.getTimeZone());
        String time = DateUtil.toTimeFormatted(dt.getMillis(), shop.getTimeZone());
        String dateTime = date.concat(",").concat(" ").concat(time);

        String theString = writer.toString();

        StringBuilder contentSB = new StringBuilder();

        theString = theString.replaceAll("==today'sDate==", dateTime);
        theString = theString.replaceAll("==shopName==", shop.getName() != null ? StringEscapeUtils.escapeHtml4(shop.getName()) : "");

        if (receiptInfo.isEnableShopAddress() && shop.getAddress() != null) {
            Address address = shop.getAddress();
            StringBuilder sb = new StringBuilder();
            if (StringUtils.isNotBlank(address.getAddress())) {
                sb.append(address.getAddress()).append("\n");
            }
            if (StringUtils.isNotBlank(address.getCity())) {
                sb.append(address.getCity()).append(",").append(" ");
            }
            if (StringUtils.isNotBlank(address.getState())) {
                sb.append(address.getState()).append(" ");
            }

            if (StringUtils.isNotBlank(address.getZipCode())) {
                sb.append(address.getZipCode()).append("\n");
            }

            theString = theString.replaceAll("==showShopAddress==", TextUtil.textOrEmpty(""));
            theString = theString.replaceAll("==shopAddress==", StringEscapeUtils.escapeHtml4(sb.toString()));
        } else {
            theString = theString.replaceAll("==showShopAddress==", TextUtil.textOrEmpty("display:none"));
        }

        if (receiptInfo.isEnableShopPhoneNo() && shop.getPhoneNumber() != null) {
            theString = theString.replaceAll("==showShopPhoneNumber==", TextUtil.textOrEmpty(""));
            theString = theString.replaceAll("==shopPhoneNo==", shop.getPhoneNumber());
        } else {
            theString = theString.replaceAll("==showShopPhoneNumber==", TextUtil.textOrEmpty("display:none"));
        }

        String shopEmail = StringUtils.isNotBlank(shop.getEmailAdress()) ? shop.getEmailAdress() : company.getEmail();
        if (StringUtils.isNotBlank(shopEmail)) {
            theString = theString.replaceAll("==showShopEmail==", TextUtil.textOrEmpty(""));
            theString = theString.replaceAll("==shopEmail==", StringEscapeUtils.escapeHtml4(shopEmail));
        } else {
            theString = theString.replaceAll("==showShopEmail==", TextUtil.textOrEmpty("display:none"));
        }

        if (transaction.isFulfillingFulfillment()) {
            theString = theString.replaceAll("==showFulfillmentStep==", TextUtil.textOrEmpty(""));
            theString = theString.replaceAll("==fulfillmentStep==", "FULFILL");
        } else if (transaction.isPreparingFulfillment()) {
            theString = theString.replaceAll("==showFulfillmentStep==", TextUtil.textOrEmpty(""));
            theString = theString.replaceAll("==fulfillmentStep==", "PREPARE");
        } else {
            theString = theString.replaceAll("==showFulfillmentStep==", TextUtil.textOrEmpty("display:none"));
        }

        if (transaction.getCheckoutType() == Shop.ShopCheckoutType.Fulfillment) {
            if (StringUtils.isNotBlank(transaction.getPreparedBy())) {
                Employee prepareEmployee = employeeRepository.get(token.getCompanyId(), transaction.getPreparedBy());
                if (prepareEmployee != null) {
                    String prepareBy = transaction.getPreparedBy();
                    String shortId = prepareBy.substring(prepareBy.length() - 5, prepareBy.length());
                    String employeeName = String.format("%s %s (%s)", prepareEmployee.getFirstName(), prepareEmployee.getLastName(), shortId);
                    theString = theString.replaceAll("==showPrepBy==", TextUtil.textOrEmpty(""));
                    theString = theString.replaceAll("==prepBy==", employeeName);

                    if (transaction.getPreparedDate() != null) {
                        String preparedDateTime = DateUtil.toDateTimeFormatted(transaction.getPreparedDate(), shop.getTimeZone()); //ProcessorUtil.timeStampWithOffset(transaction.getPreparedDate(),shop.getTimezoneOffsetInMinutes()); //getDateTimeString(transaction.getPreparedDate());
                        theString = theString.replaceAll("==showPrepDate==", TextUtil.textOrEmpty(""));
                        theString = theString.replaceAll("==prepDate==", preparedDateTime);
                    } else {
                        theString = theString.replaceAll("==showPrepDate==", TextUtil.textOrEmpty("display:none"));
                    }
                } else {
                    theString = theString.replaceAll("==showPrepBy==", TextUtil.textOrEmpty("display:none"));
                    theString = theString.replaceAll("==showPrepDate==", TextUtil.textOrEmpty("display:none"));
                }
            }
        } else {
            theString = theString.replaceAll("==showPrepBy==", TextUtil.textOrEmpty("display:none"));
            theString = theString.replaceAll("==showPrepDate==", TextUtil.textOrEmpty("display:none"));
        }

        if (StringUtils.isNotBlank(transaction.getPackedBy())) {
            Employee packedEmployee = employeeRepository.getById(transaction.getPackedBy());
            if (packedEmployee != null) {
                String shortId = packedEmployee.getId().substring(packedEmployee.getId().length() - 5);
                String employeeName = String.format("%s %s (%s)", packedEmployee.getFirstName(), packedEmployee.getLastName(), shortId);
                theString = theString.replaceAll("==packedBy==", TextUtil.textOrEmpty(StringEscapeUtils.escapeHtml4(employeeName)));
                theString = theString.replaceAll("==showPackedBy==", TextUtil.textOrEmpty(""));
            }  else {
                theString = theString.replaceAll("==showPackedBy==", TextUtil.textOrEmpty("display:none"));
            }
        } else {
            theString = theString.replaceAll("==showPackedBy==", TextUtil.textOrEmpty("display:none"));
        }

        theString = theString.replaceAll("==transNo==", "#" + transaction.getTransNo());

        if (receiptInfo.isEnableMemberId() || receiptInfo.isEnableMemberAddress() || receiptInfo.isEnableMemberName()) {
            theString = theString.replaceAll("==showMember==", TextUtil.textOrEmpty(""));
        } else {
            theString = theString.replaceAll("==showMember==", TextUtil.textOrEmpty("display:none"));
        }

        if (transaction.getMemberId() != null) {
            if (receiptInfo.isEnableMemberId()) {
                theString = theString.replaceAll("==showMemberId==", TextUtil.textOrEmpty(""));
                theString = theString.replaceAll("==memberId==", member.getId());
            } else {
                theString = theString.replaceAll("==showMemberId==", TextUtil.textOrEmpty("display:none"));
            }
            if (receiptInfo.isEnableMemberName()) {
                String memberName = String.format("%s %s", member.getFirstName(), member.getLastName());
                theString = theString.replaceAll("==showMemberName==", TextUtil.textOrEmpty(""));
                theString = theString.replaceAll("==memberName==", StringEscapeUtils.escapeHtml4(memberName));
            } else {
                theString = theString.replaceAll("==showMemberName==", TextUtil.textOrEmpty("display:none"));
            }

            if (receiptInfo.isEnableMemberAddress()) {
                Address address = transaction.getDeliveryAddress() != null ? transaction.getDeliveryAddress() : member.getAddress();
                StringBuilder sb = new StringBuilder();
                if (address != null) {
                    if (StringUtils.isNotBlank(address.getAddress())) {
                        sb.append(address.getAddress()).append("\n");
                    }
                    if (StringUtils.isNotBlank(address.getCity())) {
                        sb.append(address.getCity()).append(",").append(" ");
                    }
                    if (StringUtils.isNotBlank(address.getState())) {
                        sb.append(address.getState()).append(" ");
                    }
                    if (StringUtils.isNotBlank(address.getZipCode())) {
                        sb.append(address.getZipCode()).append("\n");
                    }
                    theString = theString.replaceAll("==showMemberAddress==", TextUtil.textOrEmpty(""));
                    theString = theString.replaceAll("==memberAddress==", StringEscapeUtils.escapeHtml4(sb.toString()));
                }
            } else {
                theString = theString.replaceAll("==showMemberAddress==", TextUtil.textOrEmpty("display:none"));
            }

            if (receiptInfo.isEnableMemberPhoneNo() && StringUtils.isNotBlank(member.getPrimaryPhone())) {
                theString = theString.replaceAll("==showMemberPhoneNo==", TextUtil.textOrEmpty(""));
                theString = theString.replaceAll("==MemberPhoneNo==", member.getPrimaryPhone());
            } else {
                theString = theString.replaceAll("==showMemberPhoneNo==", TextUtil.textOrEmpty("display:none"));
            }
        }


        if (transaction.getSellerId() != null) {
            Employee sellerEmployee = employeeRepository.get(token.getCompanyId(), transaction.getSellerId());
            if (sellerEmployee != null && receiptInfo.isEnableEmployeeName()) {
                String sellerId = transaction.getSellerId();
                String shortId = sellerId.substring(sellerId.length() - 5, sellerId.length());

                String employeeName = String.format("%s %s (%s)", sellerEmployee.getFirstName(), sellerEmployee.getLastName(), shortId);
                theString = theString.replaceAll("==showEmployee==", TextUtil.textOrEmpty(""));
                theString = theString.replaceAll("==employee==", StringEscapeUtils.escapeHtml4(employeeName));
            } else {
                theString = theString.replaceAll("==showEmployee==", TextUtil.textOrEmpty("display:none"));
            }
        } else {
            theString = theString.replaceAll("==showEmployee==", TextUtil.textOrEmpty("display:none"));
        }


        long dtTime = transaction.getProcessedTime() != null ? transaction.getProcessedTime() : 0;

        if (transaction.isActive()) {
            dtTime = transaction.getModified();
        }
        String toDate = DateUtil.toDateFormatted(dtTime, shop.getTimeZone());
        String toTime = DateUtil.toTimeFormatted(dtTime, shop.getTimeZone());
        String transDateTime = toDate.concat(",").concat(" ").concat(toTime);

        theString = theString.replaceAll("==transDate==", transDateTime);


        if (receiptInfo.isEnabledFreeText()) {
            theString = theString.replaceAll("==showSalesFreeText==", TextUtil.textOrEmpty(""));
            theString = theString.replaceAll("==salesFreeText==", Matcher.quoteReplacement(StringEscapeUtils.escapeHtml4(receiptInfo.getAboveFreeText())));
        } else {
            theString = theString.replaceAll("==showSalesFreeText==", TextUtil.textOrEmpty("display:none"));
        }


        theString = theString.replaceAll("==receiptType==", StringEscapeUtils.escapeHtml4(receiptType.toString()));


        Iterable<ProductCategory> categorySearchResult = productCategoryRepository.listByShop(token.getCompanyId(), token.getShopId());
        HashMap<String, ProductCategory> categoryHashMap = new HashMap<>();
        for (ProductCategory category : categorySearchResult) {
            categoryHashMap.put(category.getId(), category);
        }
        double totalDiscounts = 0.0;
        int sno = 1;

        for (OrderItem item : transaction.getCart().getItems()) {
            Product product = productHashMap.get(item.getProductId());
            if (product == null) {
                throw new BlazeInvalidArgException("Product", "Product not found.");
            }

            Brand brand = brandHashMap.get(product.getBrandId());

            String section = getOrderSection();
            String productName = product != null ? product.getName() : "Unknown product";

            section = section.replaceAll("==#==", String.valueOf(sno));
            section = section.replaceAll("==description==", Matcher.quoteReplacement(StringEscapeUtils.escapeHtml4(productName)));
            if (receiptInfo.isEnableIncludeItemInSKU()) {
                section = section.replaceAll("==sku==", StringEscapeUtils.escapeHtml4(product.getSku()));
            } else {
                section = section.replaceAll("==sku==", "N/A");
            }

            section = section.replaceAll("==brand==", receiptInfo.isEnableBrand() && brand != null && StringUtils.isNotBlank(brand.getName()) ? StringEscapeUtils.escapeHtml4(brand.getName()) : "N/A");

            ProductCategory category = categoryHashMap.get(product != null ? product.getCategoryId() : "Unknown category");
            String prepackageItemId = item.getPrepackageItemId();
            if (StringUtils.isNotEmpty(prepackageItemId)) {
                PrepackageProductItem prepackageProductItem = prepackageProductItemRepository.getPrepackageProductById(prepackageItemId);

                if (prepackageProductItem != null) {
                    String prepackageId = prepackageProductItem.getPrepackageId();
                    Prepackage prepackage = prepackageRepository.getPrepackageById(prepackageId);
                    if (prepackage != null) {

                        HashMap<String, ProductWeightTolerance> weightToleranceHashMap = weightToleranceRepository.listAsMap(token.getCompanyId());
                        ProductWeightTolerance weightTolerance = weightToleranceHashMap.get(prepackage.getToleranceId());
                        if (weightTolerance != null) {
                            prepackage.setName(weightTolerance.getName());
                            prepackage.setUnitValue(weightTolerance.getUnitValue());
                        }

                        BigDecimal unitValue = prepackage.getUnitValue();
                        String quantity = this.buildProductQuantity(category, unitValue);
                        section = section.replaceAll("==qty==", quantity + " " + category.getUnitType());
                    }
                }
            } else {
                String categoryName = category != null ? category.getUnitType().getType() : "";
                String quantity = this.buildProductQuantity(category, item.getQuantity());
                section = section.replaceAll("==qty==", quantity + " " + categoryName);
            }

            String amount = TextUtil.toEscapeCurrency(item.getCost().doubleValue(), shop.getDefaultCountry());
            section = section.replaceAll("==price==", amount);

            String discount = TextUtil.toEscapeCurrency(item.getDiscount().doubleValue(), shop.getDefaultCountry());
            if(receiptInfo.isEnableItemDiscount()){
                section = section.replaceAll("==discount==", discount);
            } else {
                section = section.replaceAll("==discount==", "N/A");
            }

            if(receiptInfo.isEnableItemDiscountNotes()){
                section = section.replaceAll("==discountNotes==",StringUtils.isNotBlank(item.getDiscountNotes()) ? StringEscapeUtils.escapeHtml4(item.getDiscountNotes()) : "N/A");
                section = section.replaceAll("==showDiscountPadding==","padding-top:10px");
            }else{
                section = section.replaceAll("==showDiscountPadding==","padding-top:0px");
                section = section.replaceAll("==discountNotes==", "N/A");

            }


            contentSB.append(section);

            // calculate total discounts
            if (item.getDiscountType() == OrderItem.DiscountType.Cash) {
                totalDiscounts += item.getDiscount().doubleValue();
            } else {
                totalDiscounts += item.getCost().doubleValue() * (item.getDiscount().doubleValue() / 100);
            }
            sno++;
        }
        String items = Matcher.quoteReplacement(contentSB.toString());
        theString = theString.replaceAll("==orderItems==", items);

        theString = theString.replaceAll("==subtotal==", TextUtil.toEscapeCurrency(transaction.getCart().getSubTotal().doubleValue(), shop.getDefaultCountry()));
        theString = theString.replaceAll("==discounts==", TextUtil.toEscapeCurrency(transaction.getCart().getTotalDiscount().doubleValue(), shop.getDefaultCountry()));

        if (receiptInfo.isEnableExciseTaxAsItem()) {
            TaxResult taxResult = transaction.getCart().getTaxResult();
            BigDecimal preAlExcise = BigDecimal.ZERO;
            BigDecimal postAlExcise = BigDecimal.ZERO;
            BigDecimal preNalExcise = BigDecimal.ZERO;
            BigDecimal postNalExcise = BigDecimal.ZERO;


            if (taxResult != null) {
                preAlExcise = transaction.getCart().getTaxResult().getTotalALExciseTax();
                postAlExcise = transaction.getCart().getTaxResult().getTotalALPostExciseTax();
                preNalExcise = transaction.getCart().getTaxResult().getTotalNALPreExciseTax();
                postNalExcise = transaction.getCart().getTaxResult().getTotalExciseTax();
            }

            theString = theString.replaceAll("==preAlExciseTax==", TextUtil.toEscapeCurrency(preAlExcise.doubleValue(), shop.getDefaultCountry()));
            theString = theString.replaceAll("==showALPreExciseTax==", TextUtil.textOrEmpty(preAlExcise.doubleValue() > 0 ? "" : "display:none"));

            theString = theString.replaceAll("==preNalExciseTax==", TextUtil.toEscapeCurrency(preNalExcise.doubleValue(), shop.getDefaultCountry()));
            theString = theString.replaceAll("==showNALPreExciseTax==", TextUtil.textOrEmpty(preNalExcise.doubleValue() > 0 ? "" : "display:none"));

            theString = theString.replaceAll("==alExciseTax==", TextUtil.toEscapeCurrency(postAlExcise.doubleValue(), shop.getDefaultCountry()));
            theString = theString.replaceAll("==showALExciseTax==", TextUtil.textOrEmpty(postAlExcise.doubleValue() > 0 ? "" : "display:none"));

            theString = theString.replaceAll("==nalExciseTax==", TextUtil.toEscapeCurrency(postNalExcise.doubleValue(), shop.getDefaultCountry()));
            theString = theString.replaceAll("==showNALExciseTax==", TextUtil.textOrEmpty(postNalExcise.doubleValue() > 0 ? "" : "display:none"));

            BigDecimal exciseTax = postAlExcise.add(postNalExcise);
            theString = theString.replaceAll("==exciseTax==", TextUtil.toEscapeCurrency(exciseTax.doubleValue(), shop.getDefaultCountry()));
            theString = theString.replaceAll("==showExciseTax==", TextUtil.textOrEmpty(exciseTax.doubleValue() > 0 ? "" : "display:none"));

            BigDecimal preExciseTax = preAlExcise.add(preNalExcise);
            theString = theString.replaceAll("==preExciseTax==", TextUtil.toEscapeCurrency(preExciseTax.doubleValue(), shop.getDefaultCountry()));
            theString = theString.replaceAll("==showPreExciseTax==", TextUtil.textOrEmpty(preExciseTax.doubleValue() > 0 ? "" : "display:none"));

        } else {
            theString = theString.replaceAll("==showALExciseTax==", TextUtil.textOrEmpty("display:none"));
            theString = theString.replaceAll("==showNALExciseTax==", TextUtil.textOrEmpty("display:none"));
            theString = theString.replaceAll("==showALPreExciseTax==", TextUtil.textOrEmpty("display:none"));
            theString = theString.replaceAll("==showNALPreExciseTax==", TextUtil.textOrEmpty("display:none"));
            theString = theString.replaceAll("==showExciseTax==", TextUtil.textOrEmpty("display:none"));
            theString = theString.replaceAll("==showPreExciseTax==", TextUtil.textOrEmpty("display:none"));
        }

        TaxResult taxResult = transaction.getCart().getTaxResult();
        boolean showTaxHeading = false;
        if (taxResult != null) {
            if (taxResult.getTotalCityTax().doubleValue() > 0) {
                showTaxHeading = true;
                theString = theString.replaceAll("==showCityTax==", TextUtil.textOrEmpty(""));
                theString = theString.replaceAll("==cityTax==", TextUtil.toEscapeCurrency(taxResult.getTotalCityTax().doubleValue(), shop.getDefaultCountry()));
            } else {
                theString = theString.replaceAll("==showCityTax==", TextUtil.textOrEmpty("display:none"));
            }

            if (taxResult.getTotalCountyTax().doubleValue() > 0) {
                showTaxHeading = true;
                theString = theString.replaceAll("==showCountyTax==", TextUtil.textOrEmpty(""));
                theString = theString.replaceAll("==countyTax==", TextUtil.toEscapeCurrency(taxResult.getTotalCountyTax().doubleValue(), shop.getDefaultCountry()));
            } else {
                theString = theString.replaceAll("==showCountyTax==", TextUtil.textOrEmpty("display:none"));
            }

            if (taxResult.getTotalStateTax().doubleValue() > 0) {
                showTaxHeading = true;
                theString = theString.replaceAll("==showStateTax==", TextUtil.textOrEmpty(""));
                theString = theString.replaceAll("==stateTax==", TextUtil.toEscapeCurrency(taxResult.getTotalStateTax().doubleValue(), shop.getDefaultCountry()));
            } else {
                theString = theString.replaceAll("==showStateTax==", TextUtil.textOrEmpty("display:none"));
            }

            showTaxHeading = (taxResult.getTotalFedTax().doubleValue() > 0) || showTaxHeading;
            theString = theString.replaceAll("==showFedTax==", TextUtil.textOrEmpty((taxResult.getTotalFedTax().doubleValue() > 0) ? "" : "display:none;"));
            theString = theString.replaceAll("==federalTax==", TextUtil.toEscapeCurrency(taxResult.getTotalFedTax().doubleValue(), shop.getDefaultCountry()));

        }

        if (taxResult != null) {
            if (taxResult.getTotalCityPreTax().doubleValue() > 0) {
                showTaxHeading = true;
                theString = theString.replaceAll("==showPreCityTax==", TextUtil.textOrEmpty(""));
                theString = theString.replaceAll("==preCityTax==", TextUtil.toEscapeCurrency(taxResult.getTotalCityPreTax().doubleValue(), shop.getDefaultCountry()));
            } else {
                theString = theString.replaceAll("==showPreCityTax==", TextUtil.textOrEmpty("display:none"));
            }

            if (taxResult.getTotalCountyPreTax().doubleValue() > 0) {
                showTaxHeading = true;
                theString = theString.replaceAll("==showPreCountyTax==", TextUtil.textOrEmpty(""));
                theString = theString.replaceAll("==preCountyTax==", TextUtil.toEscapeCurrency(taxResult.getTotalCountyPreTax().doubleValue(), shop.getDefaultCountry()));
            } else {
                theString = theString.replaceAll("==showPreCountyTax==", TextUtil.textOrEmpty("display:none"));
            }

            if (taxResult.getTotalStatePreTax().doubleValue() > 0) {
                showTaxHeading = true;
                theString = theString.replaceAll("==showPreStateTax==", TextUtil.textOrEmpty(""));
                theString = theString.replaceAll("==preStateTax==", TextUtil.toEscapeCurrency(taxResult.getTotalStatePreTax().doubleValue(), shop.getDefaultCountry()));
            } else {
                theString = theString.replaceAll("==showPreStateTax==", TextUtil.textOrEmpty("display:none"));
            }

            showTaxHeading = (taxResult.getTotalFedPreTax().doubleValue() > 0) || showTaxHeading;
            theString = theString.replaceAll("==showPreFedTax==", TextUtil.textOrEmpty((taxResult.getTotalFedPreTax().doubleValue() > 0) ? "" : "display:none;"));
            theString = theString.replaceAll("==preFederalTax==", TextUtil.toEscapeCurrency(taxResult.getTotalFedPreTax().doubleValue(), shop.getDefaultCountry()));
        }


        theString = theString.replaceAll("==postTaxes==", TextUtil.toEscapeCurrency(transaction.getCart().getTaxResult().getTotalPostCalcTax().doubleValue(), shop.getDefaultCountry()));

        if (transaction.getCart().getAppliedAfterTaxDiscount() != null && transaction.getCart().getAppliedAfterTaxDiscount().doubleValue() > 0) {
            theString = theString.replaceAll("==showAfterTaxDiscount==", TextUtil.textOrEmpty(""));
            theString = theString.replaceAll("==afterTaxDiscount==", TextUtil.toEscapeCurrency(transaction.getCart().getAppliedAfterTaxDiscount().doubleValue(), shop.getDefaultCountry()));
        } else {
            theString = theString.replaceAll("==showAfterTaxDiscount==", TextUtil.textOrEmpty("display:none"));
        }

        if (transaction.getCart().getDeliveryFee().doubleValue() > 0 && transaction.getTransType() != Transaction.TransactionType.Refund) {
            theString = theString.replaceAll("==showDeliveryFee==", TextUtil.textOrEmpty(""));
            theString = theString.replaceAll("==deliveryFee==", TextUtil.toEscapeCurrency(transaction.getCart().getDeliveryFee().doubleValue(), shop.getDefaultCountry()));
        } else {
            theString = theString.replaceAll("==showDeliveryFee==", TextUtil.textOrEmpty("display:none"));
        }

        if (transaction.getCart().getCreditCardFee() != null && transaction.getCart().getCreditCardFee().doubleValue() > 0 && transaction.getTransType() != Transaction.TransactionType.Refund) {
            theString = theString.replaceAll("==showCreditCardFee==", TextUtil.textOrEmpty(""));
            theString = theString.replaceAll("==creditCardFee==", TextUtil.toEscapeCurrency(transaction.getCart().getCreditCardFee().doubleValue(), shop.getDefaultCountry()));
        } else {
            theString = theString.replaceAll("==showCreditCardFee==", TextUtil.textOrEmpty("display:none"));
        }

        if (transaction.getCart().getRoundAmt().doubleValue() > 0) {
            theString = theString.replaceAll("==showRoundedAmount==", TextUtil.textOrEmpty(""));
            theString = theString.replaceAll("==roundAmt==", TextUtil.toEscapeCurrency(transaction.getCart().getRoundAmt().doubleValue(), shop.getDefaultCountry()));
        } else {
            theString = theString.replaceAll("==showRoundedAmount==", TextUtil.textOrEmpty("display:none"));
        }


        if (shop.getTaxRoundOffType() != null && StringUtils.isNotBlank(shop.getRoundUpMessage())) {
            theString = theString.replaceAll("==displayRoundMessage==", TextUtil.textOrEmpty(""));
            theString = theString.replaceAll("==roundedMessage==", StringEscapeUtils.escapeHtml4(shop.getRoundUpMessage()));
            if (shop.getTaxRoundOffType() == Shop.TaxRoundOffType.UP_DOLLAR) {
                theString = theString.replaceAll("==roundType==", "Round Up");
            } else if (shop.getTaxRoundOffType() == Shop.TaxRoundOffType.DOWN_DOLLAR) {
                theString = theString.replaceAll("==roundType==", "Round Down");
            } else if (shop.getTaxRoundOffType() == Shop.TaxRoundOffType.NEAREST_DOLLAR) {
                theString = theString.replaceAll("==roundType==", "Rounded to nearest dollar");
            } else if (shop.getTaxRoundOffType() == Shop.TaxRoundOffType.FIVE_CENT) {
                theString = theString.replaceAll("==roundType==", "Rounded to nearest dollar");
            } else {
                theString = theString.replaceAll("==roundType==", " ");
            }
        } else if (shop.getTaxRoundOffType() == null || StringUtils.isBlank(shop.getRoundUpMessage())) {
            theString = theString.replaceAll("==displayRoundMessage==", "display:  none;");
        }


        if (member != null && receiptInfo.isEnableMemberLoyaltyPoints() && member.getLoyaltyPoints().intValue() > 0) {
            theString = theString.replaceAll("==showLoyaltyPoints==", TextUtil.textOrEmpty(""));
            theString = theString.replaceAll("==loyaltyPoints==", String.format("%.1f", member.getLoyaltyPoints()));
            theString = theString.replaceAll("==pointsEarned==", String.format("%.1f", transaction.getPointsEarned()));
        } else {
            theString = theString.replaceAll("==showLoyaltyPoints==", TextUtil.textOrEmpty("display:none"));
            theString = theString.replaceAll("==showPointsEarned==", TextUtil.textOrEmpty("display:none"));
        }

        theString = theString.replaceAll("==total==", TextUtil.toEscapeCurrency(transaction.getCart().getTotal().doubleValue(), shop.getDefaultCountry()));

        if (transaction.getCart() != null) {
            Cart cart = transaction.getCart();
            if (cart != null) {
                theString = theString.replaceAll("==displayPaymentType==", TextUtil.textOrEmpty(""));
                Cart.PaymentOption paymentOption = transaction.getCart().getPaymentOption();
                if (paymentOption == Cart.PaymentOption.Clover) {
                    paymentOption = Cart.PaymentOption.Credit;
                }
                theString = theString.replaceAll("==paymentType==", paymentOption.name());
            }

            double changeAmount = 0.0;
            double totalReceived = 0.0;
            if (cart.getPaymentOption() == Cart.PaymentOption.Split && cart.getSplitPayment() != null) {
                //TODO: SPLIT PAYMENT TYPE
                double checks = NumberUtils.round(cart.getSplitPayment().getCheckAmt().doubleValue(), 2);
                double credits = NumberUtils.round(cart.getSplitPayment().getCreditDebitAmt().doubleValue(), 2);
                double cashReceived = NumberUtils.round(cart.getSplitPayment().getCashAmt().doubleValue(), 2);

                double totalCashValue = cart.getTotal().doubleValue() - checks - credits;
                changeAmount = cashReceived - totalCashValue;

                LOG.info("Checks: " + checks);
                LOG.info("cashReceived: " + cashReceived);
                LOG.info("credits: " + credits);
                LOG.info("Change due: " + changeAmount);
                if (changeAmount <= 0) {
                    changeAmount = 0;
                }

                LOG.info("Change due after: " + changeAmount);
                totalReceived = checks + credits + cashReceived;

            } else if (cart.getPaymentOption() == Cart.PaymentOption.Cash || cart.getPaymentOption() == Cart.PaymentOption.CashlessATM) {
                changeAmount = transaction.getCart().getChangeDue().doubleValue();
                totalReceived = transaction.getCart().getCashReceived().doubleValue();
            } else if (cart.getPaymentOption() == Cart.PaymentOption.Credit
                    || cart.getPaymentOption() == Cart.PaymentOption.Check
                    || cart.getPaymentOption() == Cart.PaymentOption.StoreCredit
                    || cart.getPaymentOption() == Cart.PaymentOption.Clover
                    || cart.getPaymentOption() == Cart.PaymentOption.Mtrac) {
                totalReceived = transaction.getCart().getTotal().doubleValue();
            }


            String totalPaymentReceived = TextUtil.toCurrency(totalReceived, shop.getDefaultCountry());
            theString = theString.replace("==totalPaymentReceived==", totalPaymentReceived);

            String changeDue = TextUtil.toCurrency(changeAmount, shop.getDefaultCountry());
            theString = theString.replace("==changeDue==", changeDue);


            if (receiptInfo.isEnableExciseTaxAsItem()) {
                theString = theString.replaceAll("==exciseMessage==", TextUtil.textOrEmpty(" The cannabis excise taxes are included in the total amount of this invoice."));
            } else if (shop.isEnableExciseTax() == false && shop.getAddress() != null && shop.getAddress().getState() != null && shop.getAddress().getState().equalsIgnoreCase("CA")) {
                theString = theString.replaceAll("==exciseMessage==", TextUtil.textOrEmpty(" The cannabis excise taxes are included in the total amount of this invoice."));
            } else {
                theString = theString.replaceAll("==exciseMessage==", TextUtil.textOrEmpty(""));
            }

            double paymentReceived = transaction.getCart().getCashReceived().doubleValue();
            if (transaction.getCart().getPaymentOption() != Cart.PaymentOption.Cash) {
                paymentReceived = transaction.getCart().getTotal().doubleValue();
            }
            theString = theString.replaceAll("==displayTotalPaymentReceived==", TextUtil.textOrEmpty(""));
            theString = theString.replaceAll("==totalPaymentReceived==", TextUtil.toEscapeCurrency(paymentReceived, shop.getDefaultCountry()));
        } else {
            theString = theString.replaceAll("==displayPaymentType==", TextUtil.textOrEmpty("display:none"));
            theString = theString.replaceAll("==displayChangeDue==", TextUtil.textOrEmpty("display:none"));
            theString = theString.replaceAll("==displayTotalPaymentReceived==", TextUtil.textOrEmpty("display:none"));
        }


        if (shop != null && StringUtils.isNotBlank(shop.getEmailMessage())) {
            theString = theString.replaceAll("==shopMessage==", StringEscapeUtils.unescapeHtml4(shop.getEmailMessage()));
        } else {
            theString = theString.replaceAll("==shopMessage==", "");
        }





        String memberSignatureThumbURL = "";
        if (transaction.getMemberSignature() != null) {
            memberSignatureThumbURL = transaction.getMemberSignature().getThumbURL();
        }

        if (StringUtils.isNotBlank(memberSignatureThumbURL)) {
            theString = theString.replaceAll("==showMemberSignature==", TextUtil.textOrEmpty(""));
            theString = theString.replaceAll("==memberSignature==", StringEscapeUtils.escapeHtml4(memberSignatureThumbURL));
        } else {
            theString = theString.replaceAll("==showMemberSignature==", TextUtil.textOrEmpty("display:none"));
        }

        if (receiptInfo.isEnabledBottomFreeText() && StringUtils.isNotBlank(receiptInfo.getFreeText())) {
            theString = theString.replaceAll("==bottomFreeText==", Matcher.quoteReplacement(StringEscapeUtils.escapeHtml4(receiptInfo.getFreeText())));
        } else {
            theString = theString.replaceAll("==showBottomFreeText==", TextUtil.textOrEmpty("display:none"));
        }

        double totalCalcTax = transaction.getCart().getTotalCalcTax().doubleValue();
        double totalPreTaxes = transaction.getCart().getTotalPreCalcTax().doubleValue();
        double totalPostTaxes = totalCalcTax - totalPreTaxes;
        if (totalPostTaxes < 0) {
            totalPostTaxes = 0;
        }

        theString = theString.replaceAll("==preTaxes==", TextUtil.toEscapeCurrency(totalPreTaxes, shop.getDefaultCountry()));
        theString = theString.replaceAll("==postTaxes==", TextUtil.toEscapeCurrency(totalPostTaxes, shop.getDefaultCountry()));


        //theString = theString.replaceAll("==companyName==", company.getName());
        //theString = theString.replaceAll("==companyEmail==", company.getEmail());
        //theString = theString.replaceAll("==companyPhone==", company.getPhoneNumber());

        String logoURL = null;
        if (shop.getLogo() != null && shop.getLogo().getThumbURL() != null) {
            logoURL = shop.getLogo().getThumbURL();
        }
        if (logoURL == null) {
            // try using company
            logoURL = company.getLogoURL() != null ? company.getLogoURL() : "https://connect-assets.s3.amazonaws.com/email/logo.jpg";
        }
        //
        theString = theString.replaceAll("==logo==", logoURL);
        //In case of cancel transaction end time is null so setting it empty in that case
        theString = theString.replaceAll("==dateTime==", transaction.getEndTime() != null ? TextUtil.toDateTime(transaction.getEndTime()) : "");
        theString = theString.replaceAll("==preferredEmailColor==", TextUtil.textOrEmpty("background:" + company.getPreferredEmailColor()));

        if (transaction.getCart().getTipAmount().doubleValue() > 0) {
            theString = theString.replaceAll("==showTipAmount==", TextUtil.textOrEmpty(""));
            theString = theString.replaceAll("==tipAmount==", TextUtil.toEscapeCurrency(transaction.getCart().getTipAmount().doubleValue(), shop.getDefaultCountry()));
        } else {
            theString = theString.replaceAll("==showTipAmount==", TextUtil.textOrEmpty("display:none"));
        }

        if (transaction.getNote() != null && StringUtils.isNotBlank(transaction.getNote().getMessage())) {
            theString = theString.replaceAll("==notes==", transaction.getNote().getMessage());
        } else {
            theString = theString.replaceAll("==displayNotes==", TextUtil.textOrEmpty("display:none"));
        }

        return theString;
    }

    private String buildProductQuantity(ProductCategory category, BigDecimal unitValue) {
        String quantity = "0";
        if (category != null) {
            if (ProductCategory.UnitType.units == category.getUnitType()) {
                DecimalFormat decimalFormat = new DecimalFormat("#.#");
                quantity = decimalFormat.format(unitValue.doubleValue());
            } else {
                quantity = TextUtil.formatToTwoDecimalPoints(unitValue);
            }
        }

        return quantity;
    }

    private String getOrderSection() {
        return "<tr>\n" +
                "<td class=\"table-content\" style=\"text-align:left\" width=\"3%\" height=\"40\">==#==</td>\n" +
                "<td class=\"table-content\" style=\"text-align:left\" width=\"7%\" height=\"40\">==qty==</td>\n" +
                "<td class=\"table-content\" style=\"text-align:left\" width=\"14%\" height=\"40\">==sku==</td>\n" +
                "<td class=\"table-content\" style=\"text-align:middle\" width=\"10%\" height=\"40\">==brand==</td>\n" +
                "<td class=\"table-content\" style=\"text-align:left\" width=\"23%\" height=\"40\">==description==</td>\n" +
                "<td class=\"table-content\" style=\"text-align:left\" width=\"20%\" height=\"40\">==price==</td>\n" +
                "<td class=\"table-content\" style=\"text-align:left\" width=\"10%\" height=\"40\">==discount==</td>\n" +
                "<td class=\"table-content\" style=\"text-align:left;==showDiscountPadding==\" width=\"20%\" height=\"40\">==discountNotes==</td>\n" +
                "</tr>";
    }


    // Check Inventory Available
    private void checkAvailableInventory(final Transaction inTransaction, final Transaction dbTransaction, String action, Employee employee, String sellerTerminalId, Boolean checkInAllInventory) {

        //Get list of associated products
        List<ObjectId> productIds = new ArrayList<>();
        List<String> productIdsStr = new ArrayList<>();
        if (inTransaction.getCart() != null) {
            for (OrderItem orderItem : inTransaction.getCart().getItems()) {
                if (orderItem.getStatus() == OrderItem.OrderItemStatus.Refunded) {
                    continue;
                }
                if (orderItem.getProductId() != null && ObjectId.isValid(orderItem.getProductId())) {
                    productIds.add(new ObjectId(orderItem.getProductId()));
                    productIdsStr.add(orderItem.getProductId());
                }
            }
        }
        Iterable<Product> products = productRepository.list(token.getCompanyId(), productIds);
        Iterable<ProductPrepackageQuantity> prepackageQuantities = prepackageQuantityRepository.getQuantitiesForProducts(token.getCompanyId(), token.getShopId(), productIdsStr);

        // For each product in list
        // Sum available for quantity in dbTransaction for product
        // Sum newamount in InTransaction for product
        // new quantity = available + oldAmount - newAmount
        Inventory inventory = null;
        final String terminalId = sellerTerminalId; //employee == null ? null : employee.getAssignedTerminalId();

        Terminal terminal = terminalRepository.get(token.getCompanyId(), (terminalId == null ? token.getTerminalId() : terminalId));

        boolean useOverrideInventory = Boolean.FALSE;
        if (terminal != null) {
            String assignedInventoryId = terminal.getAssignedInventoryId();
            if (assignedInventoryId != null && StringUtils.isBlank(inTransaction.getOverrideInventoryId())) {
                inventory = inventoryRepository.get(token.getCompanyId(), assignedInventoryId);
            } else if (StringUtils.isNotBlank(inTransaction.getOverrideInventoryId())) {
                inventory = inventoryRepository.get(token.getCompanyId(), inTransaction.getOverrideInventoryId());
                useOverrideInventory = Boolean.TRUE;
            }
        }
        if (inventory == null && !checkInAllInventory) {
            throw new BlazeInvalidArgException("Transaction", "Inventory is not assigned.");
        }

        for (Product product : products) {
            double available = 0d;
            int prepackagesAvailable = 0;
            for (ProductQuantity productQuantity : product.getQuantities()) {
                if (!checkInAllInventory && productQuantity.getInventoryId().equalsIgnoreCase(inventory.getId())) {
                    available = productQuantity.getQuantity().doubleValue();
                    break;
                } else if (checkInAllInventory) {
                    available += productQuantity.getQuantity().doubleValue();
                }
            }

            for (ProductPrepackageQuantity prepackageQuantity : prepackageQuantities) {
                if (!checkInAllInventory && prepackageQuantity.getProductId().equalsIgnoreCase(product.getId())
                        && prepackageQuantity.getInventoryId().equalsIgnoreCase(inventory.getId())) {
                    prepackagesAvailable += prepackageQuantity.getQuantity();
                } else if (checkInAllInventory && prepackageQuantity.getProductId().equalsIgnoreCase(product.getId())) {
                    prepackagesAvailable += prepackageQuantity.getQuantity();
                }
            }


            // get old amount
            double oldAmount = 0d;
            int prepackageOldAmount = 0;
            if (dbTransaction != null) {
                for (OrderItem orderItem : dbTransaction.getCart().getItems()) {
                    if (orderItem.getQuantityLogs().size() > 0) {
                        for (QuantityLog log : orderItem.getQuantityLogs()) {
                            if (!checkInAllInventory && log.getInventoryId().equalsIgnoreCase(inventory.getId())) {
                                if (StringUtils.isNotBlank(log.getPrepackageItemId())) {
                                    prepackageOldAmount += orderItem.getQuantity().intValue();
                                } else {
                                    oldAmount += orderItem.getQuantity().doubleValue();
                                }
                            } else if (checkInAllInventory) {
                                if (StringUtils.isNotBlank(log.getPrepackageItemId())) {
                                    prepackageOldAmount += orderItem.getQuantity().intValue();
                                } else {
                                    oldAmount += orderItem.getQuantity().doubleValue();
                                }
                            }
                        }

                    }/* else {
                        if (product.getId().equalsIgnoreCase(orderItem.getProductId())) {
                            if (BLStringUtils.isNotBlank(orderItem.getPrepackageItemId())) {
                                prepackageOldAmount += orderItem.getQuantity().intValue();

                            } else {
                                oldAmount += orderItem.getQuantity().doubleValue();
                            }
                        }
                    }*/
                }
            }

            double newAmount = 0d;
            int prepackageNewAmount = 0;
            for (OrderItem orderItem : inTransaction.getCart().getItems()) {
                if (orderItem.getStatus() == OrderItem.OrderItemStatus.Refunded) {
                    continue;
                }
                if (product.getId().equalsIgnoreCase(orderItem.getProductId())) {
                    if (StringUtils.isNotBlank(orderItem.getPrepackageItemId())) {
                        prepackageNewAmount += orderItem.getQuantity().intValue();
                    } else {
                        newAmount += orderItem.getQuantity().doubleValue();
                    }
                }
            }

            double newAvailable = available + oldAmount - newAmount;
            int prepackageNewAvailable = prepackagesAvailable + prepackageOldAmount - prepackageNewAmount;
            double rounded = NumberUtils.round(newAvailable, 2);
            LOG.info(String.format("RAW: rounded: %.3f, available: %.3f, oldAmount: %.3f, newAmount: %.3f", rounded, available, oldAmount, newAmount));
            LOG.info(String.format("Pre" +
                            "packages: newAvailable: %d, preAvailable: %d, preOldAmount: %d, preNewAmount: %d",
                    prepackageNewAvailable, prepackagesAvailable, prepackageOldAmount, prepackageNewAmount));
            if (rounded < 0 || prepackageNewAvailable < 0) {
                String msg = StringUtils.EMPTY;
                if (checkInAllInventory) {
                    msg = String.format("Not have enough stock for product '%s' to %s transaction.", product.getName(), action);
                } else {
                    msg = String.format("Inventory '%s' does not have enough stock for product '%s' to %s transaction.",
                            inventory.getName(), product.getName(), action);
                }

                if (!useOverrideInventory && employee != null) {
                    msg = String.format("Employee '%s' does not have enough stock for product '%s' in his/her inventory '%s'.",
                            employee.getFirstName() + " " + employee.getLastName(), product.getName(), inventory.getName());
                }
                throw new BlazeInvalidArgException("Transaction", msg);
            }
        }
    }

    private CustomerInfo getMemberGroup(final String memberId) {
        Member member = memberRepository.get(token.getCompanyId(), memberId);
        CustomerInfo customerInfo = new CustomerInfo();
        if (member != null) {
            MemberGroup group = memberGroupRepository.get(token.getCompanyId(), member.getMemberGroupId());
            customerInfo.setMemberGroup(group);
            customerInfo.setConsumerType(member.getConsumerType());
        }
        customerInfo.setPatient(member);
        return customerInfo;
    }

    @Override
    public Transaction setTransactionToFulfill(Transaction transaction, String transactionId) {
        final Transaction dbTransaction = transactionRepository.get(token.getCompanyId(), transactionId);
        if (dbTransaction == null) {
            throw new BlazeInvalidArgException("Transaction", "Transaction does not exist.");
        }
        // loop through and set the qty to the prepareQty
        if (dbTransaction.isPreparingFulfillment()) {
            populatePrepareQty(transaction);
        }
        dbTransaction.setStatus(Transaction.TransactionStatus.Hold);
        dbTransaction.setFulfillmentStep(Transaction.FulfillmentStep.Fulfill);
        dbTransaction.setPreparedBy(token.getActiveTopUser().getUserId());
        dbTransaction.setPreparedDate(DateTime.now().getMillis());

        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());

        final CustomerInfo customerInfo = getMemberGroup(dbTransaction.getMemberId());
        cartService.prepareCart(shop, transaction, false, dbTransaction.getStatus(), false, customerInfo, false);

        // set the incoming cart
        dbTransaction.setCart(transaction.getCart());

        transactionRepository.update(transaction.getId(), dbTransaction);
        realtimeService.sendRealTimeEvent(token.getShopId(), RealtimeService.RealtimeEventType.QueueUpdateEvent, null);
        return dbTransaction;
    }

    private void populatePrepareQty(Transaction transaction) {
        if (transaction != null) {
            for (OrderItem orderItem : transaction.getCart().getItems()) {
                orderItem.setPreparedQty(orderItem.getQuantity());
            }
        }
    }

    /**
     * updateDeliveryStartRoute is used to update the delivery starting route and update longitude
     * and latitude locations.
     *
     * @param transactionId
     * @param locationRequest
     * @return
     */
    @Override
    public Transaction updateDeliveryStartRoute(final String transactionId, final DeliveryLocationRequest locationRequest) {
        final Transaction dbTransaction = transactionRepository.get(token.getCompanyId(), transactionId);
        if (dbTransaction == null) {
            throw new BlazeInvalidArgException("TransactionId", "Transaction does not exist.");
        }
        dbTransaction.setStartRouteDate(DateUtil.nowUTC().getMillis());
        final List<Double> longitudeList = new ArrayList<>();
        longitudeList.add(locationRequest.getLongitude());
        longitudeList.add(locationRequest.getLatitude());
        dbTransaction.setStartRouteLocation(longitudeList);
        dbTransaction.setRouting(true);
        transactionRepository.update(token.getCompanyId(), dbTransaction.getId(), dbTransaction);
        realtimeService.sendRealTimeEvent(token.getShopId(), RealtimeService.RealtimeEventType.QueueUpdateEvent, null);
        return dbTransaction;
    }

    /**
     * updateDeliveryEndRoute is used to update the delivery ending route and update longitude
     * and latitude locations.
     *
     * @param transactionId
     * @param locationRequest
     * @return
     */
    @Override
    public Transaction updateDeliveryEndRoute(final String transactionId, final DeliveryLocationRequest locationRequest) {
        final Transaction dbTransaction = transactionRepository.get(token.getCompanyId(), transactionId);
        if (dbTransaction == null) {
            throw new BlazeInvalidArgException("TransactionId", "Transaction does not exist.");
        }
        dbTransaction.setEndRouteDate(DateUtil.nowUTC().getMillis());
        final List<Double> longitudeList = new ArrayList<>();
        longitudeList.add(locationRequest.getLongitude());
        longitudeList.add(locationRequest.getLatitude());
        dbTransaction.setEndRouteLocation(longitudeList);
        dbTransaction.setRouting(false);
        transactionRepository.update(token.getCompanyId(), dbTransaction.getId(), dbTransaction);
        realtimeService.sendRealTimeEvent(token.getShopId(), RealtimeService.RealtimeEventType.QueueUpdateEvent, null);
        return dbTransaction;
    }

    /**
     * Get all transaction by filtered result
     *
     * @param status
     * @param start    : start
     * @param limit    : limit
     * @param memberId : member id
     */
    @Override
    public SearchResult<TransactionResult> getTransactionsForShop(TransactionsRequest.QueryType status, int start, int limit, String memberId, String startDate, String endDate) {
        SearchResult<Transaction> searchResult = null;
        List<Transaction.TransactionStatus> transactionStatusList;
        List<Transaction.TransactionType> transactionTypeList;
        /* changes for dates */

        DateTimeFormatter formatter = DateTimeFormat.forPattern("MM/dd/yyyy");

        DateTime endDateTime;
        if (StringUtils.isNotBlank(endDate)) {
            endDateTime = formatter.parseDateTime(endDate).plusDays(1).minusSeconds(1);
        } else {
            endDateTime = DateUtil.nowUTC().plusDays(1).minusSeconds(1);
        }

        DateTime startDateTime;
        if (StringUtils.isNotBlank(startDate)) {
            startDateTime = formatter.parseDateTime(startDate).withTimeAtStartOfDay();
        } else {
            startDateTime = DateUtil.nowUTC().withTimeAtStartOfDay();
        }
        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());
        String timeZone = token.getRequestTimeZone();
        if (timeZone == null && shop != null) {
            timeZone = shop.getTimeZone();
        }

        int timeZoneOffset = DateUtil.getTimezoneOffsetInMinutes(timeZone);

        long timeZoneStartDateMillis = startDateTime.minusMinutes(timeZoneOffset).getMillis();
        long timeZoneEndDateMillis = endDateTime.minusMinutes(timeZoneOffset).getMillis();

        switch (status) {

            case SALES:
                transactionStatusList = new ArrayList<>();
                transactionStatusList.add(Transaction.TransactionStatus.Completed);
                transactionStatusList.add(Transaction.TransactionStatus.RefundWithInventory);
                transactionStatusList.add(Transaction.TransactionStatus.RefundWithoutInventory);
                searchResult = getTransactionsSearchResult(transactionStatusList, start, limit, memberId, timeZoneStartDateMillis, timeZoneEndDateMillis);

                break;

            case ADJUSTMENT:
                transactionTypeList = new ArrayList<>();
                transactionTypeList.add(Transaction.TransactionType.Adjustment);
                transactionTypeList.add(Transaction.TransactionType.Transfer);
                if (StringUtils.isNotBlank(memberId)) {
                    searchResult = transactionRepository.getTransactionsByTransactionTypesAndMember(transactionTypeList, token.getCompanyId(), token.getShopId(), start, limit, memberId);
                } else {
                    if (roleService.canAccessRestricted(token.getCompanyId(), token.getShopId(), token.getActiveTopUser().getUserId())) {
                        searchResult = transactionRepository.getTransactionsByTransactionTypes(transactionTypeList, token.getCompanyId(), token.getShopId(), start, limit, timeZoneStartDateMillis, timeZoneEndDateMillis);
                    } else {
                        searchResult = transactionRepository.getTransactionsByTransactionTypes(transactionTypeList, token.getActiveTopUser().getUserId(), token.getCompanyId(), token.getShopId(), start, limit, timeZoneStartDateMillis, timeZoneEndDateMillis);
                    }
                }
                break;

            case OTHER:
                transactionStatusList = new ArrayList<>();
                transactionStatusList.add(Transaction.TransactionStatus.Canceled);
                searchResult = getTransactionsSearchResult(transactionStatusList, start, limit, memberId, timeZoneStartDateMillis, timeZoneEndDateMillis);
                break;

        }

        return prepareTransactionResult(searchResult);
    }

    private SearchResult<Transaction> getTransactionsSearchResult(List<Transaction.TransactionStatus> transactionStatusList, int start, int limit, String memberId, long timeZoneStartDateMillis, long timeZoneEndDateMillis) {
        if (StringUtils.isNotBlank(memberId)) {
            return transactionRepository.getTransactionsByTransactionStatusAndMember(transactionStatusList, token.getCompanyId(), token.getShopId(), start, limit, memberId);
        } else {
            if (roleService.canAccessRestricted(token.getCompanyId(), token.getShopId(), token.getActiveTopUser().getUserId())) {
                return transactionRepository.getTransactionsByTransactionStatus(transactionStatusList, token.getCompanyId(), token.getShopId(), start, limit, timeZoneStartDateMillis, timeZoneEndDateMillis);
            } else {
                return transactionRepository.getTransactionsByTransactionStatus(transactionStatusList, token.getActiveTopUser().getUserId(), token.getCompanyId(), token.getShopId(), start, limit, timeZoneStartDateMillis, timeZoneEndDateMillis);
            }
        }
    }

    private SearchResult<TransactionResult> prepareTransactionResult(SearchResult<Transaction> searchResult) {

        SearchResult<TransactionResult> result = new SearchResult<>();

        List<TransactionResult> transactionResults = new ArrayList<>();

        if (searchResult != null && searchResult.getValues() != null) {
            HashMap<String, Employee> employeeHashMap = employeeRepository.listAllAsMap(token.getCompanyId());

            List<ObjectId> memberIds = new ArrayList<>();
            List<ObjectId> sellerTerminalId = new ArrayList<>();
            for (Transaction transaction : searchResult.getValues()) {
                if (StringUtils.isNotBlank(transaction.getMemberId()) && ObjectId.isValid(transaction.getMemberId())) {
                    memberIds.add(new ObjectId(transaction.getMemberId()));
                }
                if (StringUtils.isNotBlank(transaction.getSellerTerminalId()) && ObjectId.isValid(transaction.getSellerTerminalId())) {
                    sellerTerminalId.add(new ObjectId(transaction.getSellerTerminalId()));
                } else if (StringUtils.isNotBlank(transaction.getTerminalId()) && ObjectId.isValid(transaction.getTerminalId())) {
                    sellerTerminalId.add(new ObjectId(transaction.getTerminalId()));
                    transaction.setSellerTerminalId(transaction.getTerminalId());
                }
            }
            HashMap<String, Member> memberHashMap = memberRepository.listAsMap(token.getCompanyId(), memberIds);

            HashMap<String, Terminal> terminalHashMap = terminalRepository.findItemsInAsMap(token.getCompanyId(), token.getShopId(), sellerTerminalId);

            result.setTotal(searchResult.getTotal());
            result.setLimit(searchResult.getLimit());
            result.setSkip(searchResult.getSkip());

            TransactionResult transactionResult;
            for (Transaction transaction : searchResult.getValues()) {

                transactionResult = new TransactionResult();

                transactionResult.setId(transaction.getId());
                if (transaction.getProcessedTime() != null) {
                    transactionResult.setProcessedTime(transaction.getProcessedTime());
                }
                transactionResult.setTransNo(transaction.getTransNo());
                transactionResult.setQueueType(transaction.getQueueType());
                transactionResult.setTransType(transaction.getTransType());
                transactionResult.setMetrcId(transaction.getMetrcId());

                if (StringUtils.isNotEmpty(transaction.getSellerId())) {
                    Employee employee = employeeHashMap.get(transaction.getSellerId());
                    if (employee != null) {
                        transactionResult.setSellerName(employee.getFirstName() + " " + employee.getLastName());
                    } else {
                        transactionResult.setSellerName("");
                    }
                }

                if (StringUtils.isNotEmpty(transaction.getMemberId())) {
                    Member member = memberHashMap.get(transaction.getMemberId());

                    if (member != null) {
                        transactionResult.setMemberName(member.getFirstName() + " " + member.getLastName());
                    } else {
                        transactionResult.setMemberName("");
                    }
                }

                transactionResult.setTotal(transaction.getCart().getTotal());

                Terminal terminal = terminalHashMap.get(transaction.getSellerTerminalId());
                String terminalName = "";
                if (terminal != null) {
                    terminalName = terminal.getName();
                }
                transactionResult.setSellerTerminalName(terminalName);

                transactionResults.add(transactionResult);
            }
        }

        result.setValues(transactionResults);
        return result;
    }

    @Override
    public String getTransactionReceipt(String transactionId, int width, ReceiptInfo.ReceiptType receiptType) {
        int length;
        if (width == 39 || width == 32) {
            length = width;
        } else {
            length = 30;
        }
        /// force 30
        if (length == 32) {
            length = 30;
        }

        if (receiptType == null) {
            receiptType = ReceiptInfo.ReceiptType.SALES;
        }

        Company currentCompany = companyService.getCurrentCompany();
        if (currentCompany == null) {
            throw new BlazeInvalidArgException("Company", "Company dose not found");
        }

        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());
        if (shop == null) {
            throw new BlazeInvalidArgException("Shop", "Shop is not found.");
        }

        Transaction transaction = transactionRepository.get(token.getCompanyId(), transactionId);
        if (transaction == null) {
            throw new BlazeInvalidArgException("Transaction", "Transaction not found.");
        }

        QueuedTransaction queuedTransaction = queuedTransactionRepository.getRecentQueuedTransaction(token.getCompanyId(), token.getShopId(), transactionId);
        if (queuedTransaction != null && queuedTransaction.getStatus() != QueuedTransaction.QueueStatus.Prepare) {
            transaction.setCart(queuedTransaction.getCart());
        }
        if (transaction.isActive()) {
            if (queuedTransaction != null && queuedTransaction.getCart().getItems().size() > 0) {
                if (transaction.getCheckoutType() == Shop.ShopCheckoutType.FulfillmentThreeStep) {
                    if (StringUtils.isBlank(transaction.getPackedBy())
                            && transaction.getCart().getItems().isEmpty()) {
                        transaction.setCart(queuedTransaction.getCart());
                    } else if (transaction.getFulfillmentStep() == Transaction.FulfillmentStep.Prepare) {
                        // somehow we're still in preparing (meaning db wasn't updated), so let's used what's in the queued transaction
                        transaction.setCart(queuedTransaction.getCart());
                    }
                }
            }

            final CustomerInfo customerInfo = getMemberGroup(transaction.getMemberId());
            cartService.prepareCart(shop, transaction, false, Transaction.TransactionStatus.Hold, false, customerInfo, true);
        }

        List<ObjectId> productIds = new ArrayList<>();
        List<String> productIdsStr = new ArrayList<>();
        List<OrderItem> items = transaction.getCart().getItems();
        for (OrderItem item : items) {
            productIds.add(new ObjectId(item.getProductId()));
            productIdsStr.add(item.getProductId());
        }

        HashMap<String, Product> productHashMap = productRepository.listAsMap(token.getCompanyId(), productIds);
        HashMap<String, Prepackage> prepackageHashMap = prepackageRepository.getPrepackagesForProductsAsMap(token.getCompanyId(), token.getShopId(), productIdsStr);
        HashMap<String, PrepackageProductItem> prepackagesForProductsAsMap = prepackageProductItemRepository.getPrepackagesForProductsAsMap(token.getCompanyId(), token.getShopId(), productIdsStr);

        HashMap<String, Brand> brandHashMap = brandRepository.listAllAsMap(token.getCompanyId());

        DateTime dt = new DateTime();

        String date = DateUtil.toDateFormatted(dt.getMillis(), shop.getTimeZone()); //ProcessorUtil.dateStringWithOffset(dt.getMillis(),shop.getTimezoneOffsetInMinutes()); //  fmt.print(dt);
        String time = DateUtil.toTimeFormatted(dt.getMillis(), shop.getTimeZone()); //ProcessorUtil.timeOfDayWithOffset(dt.getMillis(), shop.getTimezoneOffsetInMinutes());
        String dateTime = date.concat(",").concat(" ").concat(time);

        StringBuilder sb = new StringBuilder();

        ReceiptInfo receiptInfo = new ReceiptInfo();
        if (shop.getReceiptInfo() != null) {
            for (ReceiptInfo receipt : shop.getReceiptInfo()) {
                if (receipt.getReceiptType().equals(receiptType)) {
                    receiptInfo = receipt;
                }
            }
        }

        if (Objects.isNull(receiptInfo)) {
            throw new BlazeInvalidArgException("Receipt", "No receipt found for " + receiptType);
        }

        if (receiptInfo.isEnabledFreeText()) {
            sb.append(getCommonItem("", receiptInfo.getAboveFreeText(), length)).append("\n");
        }

        sb.append(getCommonItem("Today's Date", dateTime, length)).append("\n")
                .append(getCommonItem("", shop.getName(), length)).append("\n");


        if (receiptInfo.isEnableShopAddress() && shop.getAddress() != null) {
            Address address = shop.getAddress();
            sb.append(getCommonItem("", address.getAddress(), length)).append("\n")
                    .append(getCommonItem("", address.getCity().concat(",").concat(" ").concat(address.getState()).concat(" ").concat(address.getZipCode()), length)).append("\n");
        }

        if (receiptInfo.isEnableShopPhoneNo() && shop.getPhoneNumber() != null) {
            sb.append(getCommonItem("", shop.getPhoneNumber(), length)).append("\n");

        }

        if (transaction.isFulfillingFulfillment()) {
            sb.append(getCommonItem("Fulfillment Step", "FULFILL", length)).append("\n");
        } else if (transaction.isPreparingFulfillment()) {
            sb.append(getCommonItem("Fulfillment Step", "PREPARE", length)).append("\n");
        }


        sb.append("\n");
        sb.append(getCommonItem("Trans No", transaction.getTransNo(), length)).append("\n");

        long dtTime = transaction.getProcessedTime() != null ? transaction.getProcessedTime() : 0;
        if (transaction.isActive()) {
            dtTime = transaction.getModified();
        }
        String transDateTime = DateUtil.toDateTimeFormatted(dtTime, shop.getTimeZone()); //getDateTimeString(transaction.getModified());
        sb.append(getCommonItem("Trans Date", transDateTime, length)).append("\n\n");

        if (transaction.getCheckoutType() != Shop.ShopCheckoutType.Direct) {
            if (StringUtils.isNotBlank(transaction.getPreparedBy())) {
                Employee prepareEmployee = employeeRepository.get(token.getCompanyId(), transaction.getPreparedBy());
                if (prepareEmployee != null) {
                    String prepareBy = transaction.getPreparedBy();
                    String shortId = prepareBy.substring(prepareBy.length() - 5, prepareBy.length());
                    String employeeName = String.format("%s %s (%s)", prepareEmployee.getFirstName(), prepareEmployee.getLastName().substring(0, 1), shortId);
                    sb.append(getCommonItem("Prep By", employeeName, length)).append("\n");

                    if (transaction.getPreparedDate() != null) {
                        String preparedDateTime = DateUtil.toDateTimeFormatted(transaction.getPreparedDate(), shop.getTimeZone()); //ProcessorUtil.timeStampWithOffset(transaction.getPreparedDate(),shop.getTimezoneOffsetInMinutes()); //getDateTimeString(transaction.getPreparedDate());


                        sb.append(getCommonItem("Prep Date", preparedDateTime, length)).append("\n");
                    }
                }
            }

            if (StringUtils.isNotBlank(transaction.getPackedBy())) {
                Employee prepareEmployee = employeeRepository.get(token.getCompanyId(), transaction.getPackedBy());
                if (prepareEmployee != null) {
                    String prepareBy = transaction.getPreparedBy();
                    String shortId = prepareBy.substring(prepareBy.length() - 5, prepareBy.length());
                    String employeeName = String.format("%s %s (%s)", prepareEmployee.getFirstName(), prepareEmployee.getLastName().substring(0, 1), shortId);
                    sb.append(getCommonItem("Packed By", employeeName, length)).append("\n");

                    if (transaction.getPreparedDate() != null) {
                        String preparedDateTime = DateUtil.toDateTimeFormatted(transaction.getPreparedDate(), shop.getTimeZone()); //ProcessorUtil.timeStampWithOffset(transaction.getPreparedDate(),shop.getTimezoneOffsetInMinutes()); //getDateTimeString(transaction.getPreparedDate());

                        sb.append(getCommonItem("Packed Date", preparedDateTime, length)).append("\n");
                    }
                }
            }

        }


        if (transaction.getSellerId() != null) {
            Employee sellerEmployee = employeeRepository.get(token.getCompanyId(), transaction.getSellerId());
            if (sellerEmployee != null && receiptInfo.isEnableEmployeeName()) {
                String sellerId = transaction.getSellerId();
                String shortId = sellerId.substring(sellerId.length() - 5, sellerId.length());

                String employeeName = String.format("%s %s (%s)", sellerEmployee.getFirstName(), sellerEmployee.getLastName().substring(0, 1), shortId);
                sb.append(getCommonItem("Completed By", employeeName, length)).append("\n\n");
            }
        }

        if (transaction.getMemberId() != null) {
            Member member = memberRepository.get(token.getCompanyId(), transaction.getMemberId());

            if (receiptInfo.isEnableMemberId()) {
                String memberId = member.getId();
                sb.append(getCommonItem("Member ID", memberId.substring(memberId.length() - 5, memberId.length()), length)).append("\n");
            }
            if (receiptInfo.isEnableMemberName()) {
                String memberName = String.format("%s %s", member.getFirstName(), member.getLastName().substring(0, 1));
                sb.append(getCommonItem("Member", memberName, length)).append("\n");
            }

            if (receiptInfo.isEnableMemberPhoneNo() && StringUtils.isNotBlank(member.getPrimaryPhone())) {
                sb.append(getCommonItem("Member Phone", member.getPrimaryPhone(), length)).append("\n");
            }

            if (receiptInfo.isEnableMemberAddress()) {
                Address address = member.getAddress();
                if (StringUtils.isNotBlank(address.getAddress())) {
                    sb.append(getCommonItem("", address.getAddress(), length)).append("\n");
                }
                if (StringUtils.isNotBlank(address.getCity())) {
                    sb.append(getCommonItem("", address.getCity(), length)).append(",").append(" ");
                }
                if (StringUtils.isNotBlank(address.getState())) {
                    sb.append(getCommonItem("", address.getState(), length)).append(" ");
                }

                if (StringUtils.isNotBlank(address.getZipCode())) {
                    sb.append(getCommonItem("", address.getZipCode(), length)).append("\n");
                } else {
                    sb.append("\n");
                }
            }
        }



        sb.append(repeatContent(length, "", "-", Boolean.TRUE).concat("\n"));
        sb.append("ITEMS").append("\n");
        sb.append(repeatContent(length, "", "-", Boolean.TRUE).concat("\n"));
        sb.append(addDataWithSpace("Name", "Price", length));
        sb.append("\n");

        for (OrderItem item : items) {
            Product product = productHashMap.get(item.getProductId());
            sb.append(addProductWithWordWrap(product.getName(), TextUtil.toCurrency(item.getFinalPrice().doubleValue(), shop.getDefaultCountry()), length));
            sb.append("\n");
            if (receiptInfo.isEnableBrand()) {
                Brand brand = brandHashMap.get(product.getBrandId());
                if (brand != null) {
                    sb.append(String.format("-Brand: %s\n", brand.getName()));
                }
            }
            if (receiptInfo.isEnableIncludeItemInSKU()) {
                sb.append(String.format("-SKU: %s\n", product.getSku()));
            }


            String unitType = "ea";
            if (StringUtils.isNotBlank(item.getPrepackageItemId())) {
                PrepackageProductItem prepackageProductItem = prepackagesForProductsAsMap.get(item.getPrepackageItemId());
                if (prepackageProductItem != null) {
                    Prepackage prepackage = prepackageHashMap.get(prepackageProductItem.getPrepackageId());
                    if (prepackage != null) {
                        unitType = prepackage.getName();
                    }
                }
            } else {
                if (product.getCategory() != null) {
                    if (product.getCategory().getUnitType() == ProductCategory.UnitType.grams) {
                        unitType = "g";
                    }
                }
            }


            sb.append(String.format("-Qty: %s %s\n", String.valueOf(NumberUtils.round(item.getQuantity())), unitType));
            if (item.getCalcDiscount().doubleValue() > 0) {
                sb.append(String.format("-Discount: %s\n", TextUtil.toCurrency(item.getCalcDiscount().doubleValue(), shop.getDefaultCountry())));
            }


            if (item.getStatus() == OrderItem.OrderItemStatus.Refunded) {
                sb.append("-Refunded\n");
            }
            sb.append("\n");

        }

        sb.append(addDataWithSpace("Subtotal", TextUtil.toCurrency(transaction.getCart().getSubTotal().doubleValue(), shop.getDefaultCountry()), length)).append("\n").append("\n");

        addTaxOrSkip(sb, "Total Discounts", transaction.getCart().getTotalDiscount(), shop.getDefaultCountry(), length, false);
        if (transaction.getCart().getTotalDiscount().doubleValue() > 0) {
            addTaxOrSkip(sb, "Subtotal w/ disc", transaction.getCart().getSubTotalDiscount(), shop.getDefaultCountry(), length, false);
        }

        if (receiptInfo.isEnableExciseTaxAsItem()) {
            if (currentCompany.getOnPremCompanyConfig() != null && currentCompany.getOnPremCompanyConfig().isOnPremEnable()) {
                BigDecimal preExciseTaxes =
                        transaction.getCart().getTaxResult().getTotalALExciseTax().add(transaction.getCart().getTaxResult().getTotalNALPreExciseTax());


                BigDecimal postExciseTaxes =
                        transaction.getCart().getTaxResult().getTotalALPostExciseTax().add(transaction.getCart().getTaxResult().getTotalExciseTax());


                addTaxOrSkip(sb, "Excise Taxes", preExciseTaxes, shop.getDefaultCountry(), length, true);
                addTaxOrSkip(sb, "Excise Taxes", postExciseTaxes, shop.getDefaultCountry(), length, false);
            } else {
                BigDecimal preExciseTaxes =
                        transaction.getCart().getTaxResult().getTotalALExciseTax().add(transaction.getCart().getTaxResult().getTotalNALPreExciseTax());

                BigDecimal postExciseTaxes =
                        transaction.getCart().getTaxResult().getTotalALPostExciseTax().add(transaction.getCart().getTaxResult().getTotalExciseTax());

                addTaxOrSkip(sb, "Excise Taxes", preExciseTaxes, shop.getDefaultCountry(), length, true);
                addTaxOrSkip(sb, "Excise Taxes", postExciseTaxes, shop.getDefaultCountry(), length, false);
            }
        }


        String line = repeatContent(length, "", "-", Boolean.TRUE);


        if (transaction.getCart().getTaxResult() != null) {
            addTaxOrSkip(sb, "City Tax", transaction.getCart().getTaxResult().getTotalCityPreTax(), shop.getDefaultCountry(), length, true);
            addTaxOrSkip(sb, "City Tax", transaction.getCart().getTaxResult().getTotalCityTax(), shop.getDefaultCountry(), length, false);


            addTaxOrSkip(sb, "County Tax", transaction.getCart().getTaxResult().getTotalCountyPreTax(), shop.getDefaultCountry(), length, true);
            addTaxOrSkip(sb, "County Tax", transaction.getCart().getTaxResult().getTotalCountyTax(), shop.getDefaultCountry(), length, false);

            addTaxOrSkip(sb, "State Tax", transaction.getCart().getTaxResult().getTotalStatePreTax(), shop.getDefaultCountry(), length, true);
            addTaxOrSkip(sb, "State Tax", transaction.getCart().getTaxResult().getTotalStateTax(), shop.getDefaultCountry(), length, false);


            addTaxOrSkip(sb, "Federal Tax", transaction.getCart().getTaxResult().getTotalFedPreTax(), shop.getDefaultCountry(), length, true);
            addTaxOrSkip(sb, "Federal Tax", transaction.getCart().getTaxResult().getTotalFedTax(), shop.getDefaultCountry(), length, false);
        }

        addTaxOrSkip(sb, "Total Pre-Tax", transaction.getCart().getTotalPreCalcTax(), shop.getDefaultCountry(), length, true);

        sb.append(addDataWithSpace("Total Tax", TextUtil.toCurrency(transaction.getCart().getTaxResult().getTotalPostCalcTax().doubleValue(), shop.getDefaultCountry()), length)).append("\n");


        addTaxOrSkip(sb, "Delivery Fee", transaction.getCart().getDeliveryFee(), shop.getDefaultCountry(), length, false);
        addTaxOrSkip(sb, "Credit/Debit Fee", transaction.getCart().getCreditCardFee(), shop.getDefaultCountry(), length, false);
        addTaxOrSkip(sb, "After Tax Discount", transaction.getCart().getAppliedAfterTaxDiscount(), shop.getDefaultCountry(), length, false);

        sb.append(line).append("\n");
        sb.append(addDataWithSpace("Total", TextUtil.toCurrency(transaction.getCart().getTotal().doubleValue(), shop.getDefaultCountry()), length)).append("\n");
        sb.append(line).append("\n").append("\n");

        if (transaction.getTransType() == Transaction.TransactionType.Refund) {
            addTaxOrSkip(sb, "Refund Amount", transaction.getCart().getRefundAmount(), shop.getDefaultCountry(), length, false);
            sb.append(line).append("\n").append("\n");
        }


        if (transaction.getCart() != null) {
            Cart cart = transaction.getCart();
            if (cart != null) {
                Cart.PaymentOption paymentOption = cart.getPaymentOption();
                if (paymentOption == Cart.PaymentOption.Clover) {
                    paymentOption = Cart.PaymentOption.Credit;
                }
                String cashPayment = getCommonItem("Payment Type", paymentOption.name(), length);
                sb.append(cashPayment).append("\n");
                double changedDue = 0.0;
                double totalReceived = 0.0;

                if (cart.getPaymentOption() == Cart.PaymentOption.Split && cart.getSplitPayment() != null) {
                    //TODO: SPLIT PAYMENT TYPE
                    double checks = NumberUtils.round(cart.getSplitPayment().getCheckAmt().doubleValue(), 2);
                    double credits = NumberUtils.round(cart.getSplitPayment().getCreditDebitAmt().doubleValue(), 2);
                    double cashReceived = NumberUtils.round(cart.getSplitPayment().getCashAmt().doubleValue(), 2);

                    double totalCashValue = cart.getTotal().doubleValue() - checks - credits;
                    changedDue = cashReceived - totalCashValue;

                    LOG.info("Checks: " + checks);
                    LOG.info("cashReceived: " + cashReceived);
                    LOG.info("credits: " + credits);
                    LOG.info("Change due: " + changedDue);
                    if (changedDue <= 0) {
                        // implies cashReceived > totalCashValue
                        changedDue = 0;
                    }
                    LOG.info("Change due after: " + changedDue);
                    totalReceived = checks + credits + cashReceived;

                    if (cashReceived > 0) {
                        sb.append(getCommonItem("Cash", TextUtil.toCurrency(cashReceived, shop.getDefaultCountry()), length)).append("\n");
                    }
                    if (checks > 0) {
                        sb.append(getCommonItem("Check", TextUtil.toCurrency(checks, shop.getDefaultCountry()), length)).append("\n");
                    }
                    if (credits > 0) {
                        sb.append(getCommonItem("Credit/Debit", TextUtil.toCurrency(credits, shop.getDefaultCountry()), length)).append("\n");
                    }


                } else if (cart.getPaymentOption() == Cart.PaymentOption.Cash || cart.getPaymentOption() == Cart.PaymentOption.CashlessATM) {
                    changedDue = transaction.getCart().getChangeDue().doubleValue();
                    totalReceived = transaction.getCart().getCashReceived().doubleValue();
                } else if (cart.getPaymentOption() == Cart.PaymentOption.Credit
                        || cart.getPaymentOption() == Cart.PaymentOption.Check
                        || cart.getPaymentOption() == Cart.PaymentOption.Clover
                        || cart.getPaymentOption() == Cart.PaymentOption.Mtrac) {
                    totalReceived = transaction.getCart().getTotal().doubleValue();
                }

                sb.append(getCommonItem("Total Payment Received", TextUtil.toCurrency(totalReceived, shop.getDefaultCountry()), length)).append("\n");
                sb.append(getCommonItem("Change Due", TextUtil.toCurrency(changedDue, shop.getDefaultCountry()), length)).append("\n").append("\n");
            }
        }


        if (receiptInfo.isEnableExciseTaxAsItem()) {
            sb.append(getCommonItem("", "The cannabis excise taxes are included in the total amount of this invoice.", length)).append("\n").append("\n");
        } else if (shop.isEnableExciseTax() == false && shop.getAddress() != null && shop.getAddress().getState() != null && shop.getAddress().getState().equalsIgnoreCase("CA")) {
            sb.append(getCommonItem("", "The cannabis excise taxes are included in the total amount of this invoice.", length)).append("\n").append("\n");
        }


        if (receiptInfo.isEnabledBottomFreeText()) {
            sb.append("\n").append("\n").append("\n")
                    .append("\n").append("\n")
                    .append(getCommonItem("", receiptInfo.getFreeText(), length)).append("\n");
        }
        sb.append("\n").append("\n");
        if (currentCompany.getOnPremCompanyConfig() != null && currentCompany.getOnPremCompanyConfig().isOnPremEnable()) {
            sb.append("Powered by BLAZE");
        }

        boolean shouldAppendText = true;
        if (transaction.getMemberSignature() != null) {
            shouldAppendText = false;
        }
        if (length < 32) {
            shouldAppendText = false;
        }
        sb.append("\n").append("\n");
        if (shouldAppendText) {
            sb.append(line);
            sb.append("\n").append("\n");
        }

        return sb.toString();
    }

    @Override
    public Transaction unlockTransaction(String transactionId, StatusRequest request) {
        Transaction transaction = transactionRepository.get(token.getCompanyId(), transactionId);

        if (transaction == null) {
            throw new BlazeInvalidArgException(TRANSACTION, TRANSACTION_NOT_FOUND);
        }

        Employee employee = employeeRepository.get(token.getCompanyId(), token.getActiveTopUser().getUserId());

        if (employee == null) {
            throw new BlazeInvalidArgException(EMPLOYEE, EMPLOYEE_NOT_FOUND);
        }

        Role role = roleService.getRoleById(employee.getRoleId());

        if (role != null && role.getPermissions() != null && role.getPermissions().contains(Role.Permission.WebUnlockOrder)) {
            transaction.setLocked(request.isStatus());
            transactionRepository.updateTransactionLockStatus(token.getCompanyId(), transactionId, request.isStatus());

            realtimeService.sendRealTimeEvent(token.getShopId(), RealtimeService.RealtimeEventType.QueueUpdateEvent, null);
        } else {
            throw new BlazeInvalidArgException(TRANSACTION, TRANSACTION_LOCK_PERMISSION);
        }

        return transaction;
    }

    /**
     * This is private method for update consumer order web hook
     *
     * @param dbConsumerCart
     */
    private void updateConsumerOrderWebHook(ConsumerCart dbConsumerCart) {
        final ConsumerOrderData consumerOrderData = new ConsumerOrderData();
        StringBuilder memberName = new StringBuilder();
        StringBuilder consumerName = new StringBuilder();
        if (StringUtils.isNotBlank(dbConsumerCart.getMemberId())) {
            Member member = memberRepository.getById(dbConsumerCart.getMemberId());
            if (member == null) {
                member = memberRepository.getMemberWithConsumerId(token.getCompanyId(), dbConsumerCart.getConsumerId());
            }
            if (member != null) {
                memberName.append(member.getFirstName())
                        .append((member.getMiddleName() == null) ? " " : member.getMiddleName())
                        .append((member.getLastName() == null) ? "" : member.getLastName());
                consumerOrderData.setLoyaltyPoints(member.getLoyaltyPoints());
                consumerOrderData.setMemberId(member.getId());
            }
        }
        if (StringUtils.isNotBlank(dbConsumerCart.getConsumerId())) {
            ConsumerUser consumerUser = consumerUserRepository.getById(dbConsumerCart.getConsumerId());
            if (consumerUser != null) {
                consumerName.append(consumerUser.getFirstName())
                        .append((consumerUser.getMiddleName() == null) ? " " : consumerUser.getMiddleName())
                        .append((consumerUser.getLastName() == null) ? "" : consumerUser.getLastName());
                consumerOrderData.setConsumerId(consumerUser.getId());
                consumerOrderData.setMembershipAccepted(consumerUser.isAccepted());
            }
        }
        consumerOrderData.setConsumerName(consumerName.toString());
        consumerOrderData.setConsumerOrderId(dbConsumerCart.getId());
        consumerOrderData.setConsumerOrderNo(dbConsumerCart.getOrderNo());
        consumerOrderData.setOrderStatus(dbConsumerCart.getCartStatus());
        consumerOrderData.setOrderTime(dbConsumerCart.getOrderPlacedTime());
        consumerOrderData.setMemberName(memberName.toString());
        consumerOrderData.setCart(dbConsumerCart.getCart());

        try {
            partnerWebHookManager.updateConsumerOrderHook(token.getCompanyId(), token.getShopId(), consumerOrderData);
        } catch (Exception e) {
            LOG.warn("Update consumer web hook failed for shop id :" + token.getShopId() + " and company id:" + token.getCompanyId(), e);
        }
    }

    /**
     * This method is for bulk request for updating transaction.
     *
     * @param transID
     * @param queueName
     * @param request
     * @return
     */
    @Override
    public Transaction handleAddToQueueForBulk(String transID, String queueName, QueueAddMemberRequest request) {
        if (StringUtils.isBlank(transID)) {
            throw new BlazeInvalidArgException(TRANSACTION, "Transaction does not exist.");
        }
        if (StringUtils.isBlank(queueName)) {
            throw new BlazeInvalidArgException(TRANSACTION, "Queue cannot be blank.");
        }
        Transaction.QueueType queueType = Transaction.QueueType.None;
        try {
            queueType = Transaction.QueueType.valueOf(queueName);
        } catch (Exception e) {
            throw new BlazeInvalidArgException("Queue name", "Invalid queue name. Must be of: " + Transaction.QueueType.Online.name() + ", " + Transaction.QueueType.Delivery.name() + ", " + Transaction.QueueType.WalkIn.name() + ", " + Transaction.QueueType.Special.name());
        }
        Transaction dbTransaction = transactionRepository.getById(transID);
        if (dbTransaction == null) {
            dbTransaction = addToQueue(transID, queueName, request);
        } else {
            Member member = memberRepository.get(token.getCompanyId(), request.getMemberId());
            if (member == null) {
                throw new BlazeInvalidArgException("Member", "Member does not exist.");
            }

            if (member.getStatus() == Member.MembershipStatus.Inactive) {
                throw new BlazeInvalidArgException("Member", "Cannot add inactive member to queue.");
            }

            // Check if employee is valid
            Employee employee = null;
            if (StringUtils.isNotBlank(request.getEmployeeId())) {
                employee = employeeRepository.get(token.getCompanyId(), request.getEmployeeId());
                if (employee == null || employee.isDeleted()) {
                    throw new BlazeInvalidArgException("Employee", "Employee does not exist.");
                }

                if (employee.isDisabled()) {
                    throw new BlazeInvalidArgException("Employee", "Employee is inactive.");
                }

            }
            dbTransaction.setMemberId(request.getMemberId());
            dbTransaction.setAssignedEmployeeId(request.getEmployeeId());
            dbTransaction.setAssigned((StringUtils.isNotBlank(request.getEmployeeId())));
            dbTransaction.setMemo(request.getMemo());
            dbTransaction.setCreateOnfleetTask(request.getCreateOnFleetTask());
            dbTransaction.setOnFleetTaskId(request.getOnFleetTeamId());
            dbTransaction.setQueueType(queueType);

            if (member != null && member.getAddress() != null) {
                Address address = member.getAddress();
                address.resetPrepare(token.getCompanyId());
                dbTransaction.setDeliveryAddress(address);
            }
            transactionRepository.update(token.getCompanyId(), dbTransaction.getId(), dbTransaction);

            realtimeService.sendRealTimeEvent(token.getShopId(), RealtimeService.RealtimeEventType.QueueUpdateEvent, null);
        }
        return dbTransaction;
    }

    @Override
    public SearchResult<Transaction> getTransactionByStatus(TransactionsRequest.AssignStatus status, int start, int limit, String term, long afterDate, long beforeDate) {
        SearchResult<Transaction> result = new SearchResult<>();
        if (beforeDate == 0) {
            beforeDate = DateTime.now().getMillis();
        }
        if (limit == 0) {
            limit = Integer.MAX_VALUE;
        }
        if (StringUtils.isBlank(term)) {
            if (status == null || status == TransactionsRequest.AssignStatus.INPROGRESS) {
                if (roleService.canAccessRestricted(token.getCompanyId(), token.getShopId(), token.getActiveTopUser().getUserId())) {
                    result = transactionRepository.getActiveTransactionByDate(token.getCompanyId(), token.getShopId(), start, limit, afterDate, beforeDate);
                } else {
                    result = transactionRepository.getAllTransactionsAssigned(token.getCompanyId(), token.getShopId(), token.getActiveTopUser().getUserId(), afterDate, beforeDate, start, limit);
                }
            } else if (status == TransactionsRequest.AssignStatus.COMPLETED) {
                result = transactionRepository.getCompletedTransactionsByDate(token.getCompanyId(), token.getShopId(), start, limit, afterDate, beforeDate);
            } else {
                if (roleService.canAccessRestricted(token.getCompanyId(), token.getShopId(), token.getActiveTopUser().getUserId())) {
                    result = transactionRepository.getAllAssignedTransaction(token.getCompanyId(), token.getShopId(), start, limit, (status == TransactionsRequest.AssignStatus.ASSIGNED), afterDate, beforeDate);
                } else {
                    if (status == TransactionsRequest.AssignStatus.ASSIGNED) {
                        result = transactionRepository.getAllTransactionsAssigned(token.getCompanyId(), token.getShopId(), token.getActiveTopUser().getUserId(), afterDate, beforeDate, start, limit);
                    } else {
                        result = transactionRepository.getAllAssignedTransaction(token.getCompanyId(), token.getShopId(), start, limit, false, afterDate, beforeDate);
                    }
                }
            }
        } else {
            if (status == null || status == TransactionsRequest.AssignStatus.INPROGRESS) {
                if (roleService.canAccessRestricted(token.getCompanyId(), token.getShopId(), token.getActiveTopUser().getUserId())) {
                    result = transactionRepository.getActiveTransactionsByTerm(token.getCompanyId(), token.getShopId(), start, limit, term, afterDate, beforeDate);
                } else {
                    result = transactionRepository.getActiveTransactionsByTerm(token.getCompanyId(), token.getShopId(), token.getActiveTopUser().getUserId(), start, limit, term, afterDate, beforeDate);
                }
            } else if (status == TransactionsRequest.AssignStatus.COMPLETED) {
                result = transactionRepository.getCompletedTransactionsByTerm(token.getCompanyId(), token.getShopId(), term, start, limit, afterDate, beforeDate);
            } else {
                if (roleService.canAccessRestricted(token.getCompanyId(), token.getShopId(), token.getActiveTopUser().getUserId())) {
                    result = transactionRepository.getAllAssignedTransactionByTerm(token.getCompanyId(), token.getShopId(), start, limit, term, (status == TransactionsRequest.AssignStatus.ASSIGNED), afterDate, beforeDate);
                } else {
                    if (status == TransactionsRequest.AssignStatus.ASSIGNED) {
                        result = transactionRepository.getActiveTransactionsByTerm(token.getCompanyId(), token.getShopId(), token.getActiveTopUser().getUserId(), start, limit, term, afterDate, beforeDate);
                    } else {
                        result = transactionRepository.getAllAssignedTransactionByTerm(token.getCompanyId(), token.getShopId(), start, limit, term, false, afterDate, beforeDate);
                    }

                }

            }
        }
        assignDependentObjects(result, true, true);
        return result;
    }

    @Override
    public TransactionCountResult getOrdersCount() {
        boolean canAccessRestricted = roleService.canAccessRestricted(token.getCompanyId(), token.getShopId(), token.getActiveTopUser().getUserId());
        long incomingCount = consumerCartRepository.countIncomingOrders(token.getCompanyId(), token.getShopId(), Lists.newArrayList(ConsumerCart.ConsumerCartStatus.Placed));
        long inProgressCount = canAccessRestricted ?
                transactionRepository.countTransactionByStatus(token.getCompanyId(), token.getShopId(), Lists.newArrayList(Transaction.TransactionStatus.InProgress, Transaction.TransactionStatus.Hold, Transaction.TransactionStatus.Queued), true)
                : transactionRepository.countTransactionByStatus(token.getCompanyId(), token.getShopId(), token.getActiveTopUser().getUserId(), Lists.newArrayList(Transaction.TransactionStatus.InProgress, Transaction.TransactionStatus.Hold, Transaction.TransactionStatus.Queued), true);

        long unassignedCount = transactionRepository.countTransactionsByAssignStatus(token.getCompanyId(), token.getShopId(), false);
        long assignCount = canAccessRestricted ?
                transactionRepository.countTransactionsByAssignStatus(token.getCompanyId(), token.getShopId(), true)
                : transactionRepository.countTransactionByStatus(token.getCompanyId(), token.getShopId(), token.getActiveTopUser().getUserId(), Lists.newArrayList(Transaction.TransactionStatus.InProgress, Transaction.TransactionStatus.Hold, Transaction.TransactionStatus.Queued), true);
        long completedTransaction = transactionRepository.countTransactionByStatus(token.getCompanyId(), token.getShopId(), Lists.newArrayList(Transaction.TransactionStatus.Completed, Transaction.TransactionStatus.RefundWithInventory, Transaction.TransactionStatus.RefundWithoutInventory), false);
        TransactionCountResult result = new TransactionCountResult();
        result.setAssigned(assignCount);
        result.setUnAssigned(unassignedCount);
        result.setIncomingOrders(incomingCount);
        result.setInProgress(inProgressCount);
        result.setCompleted(completedTransaction);
        return result;

    }

    @Override
    public Transaction completeTransaction(String transactionId, Transaction transaction, boolean avoidCannabis) {
        return completeTransaction(transactionId, transaction, avoidCannabis, false);
    }

    @Override
    public Transaction markAsPaid(String transactionId, Cart.PaymentOption paymentOption) {
        Transaction dbTrans = transactionRepository.get(token.getCompanyId(),transactionId);
        if (dbTrans == null) {
            throw new BlazeInvalidArgException(TRANSACTION, "Transaction does not exist.");
        }
        if (!dbTrans.isActive()) {
            return dbTrans;
        }

        dbTrans.setPaid(true);
        dbTrans.setProcessedTime(DateTime.now().getMillis());
        if (paymentOption != null) {
            dbTrans.getCart().setPaymentOption(paymentOption);
        } else {
            paymentOption =  dbTrans.getCart().getPaymentOption();
        }
        transactionRepository.markTransactionAsPaid(token.getCompanyId(),transactionId,dbTrans.getProcessedTime(),paymentOption);
        realtimeService.sendRealTimeEvent(token.getShopId(), RealtimeService.RealtimeEventType.QueueUpdateEvent, null);
        return dbTrans;
    }

    @Override
    public Transaction markAsUnpaid(String transactionId, Cart.PaymentOption paymentOption) {
        Transaction dbTrans = transactionRepository.get(token.getCompanyId(),transactionId);
        if (dbTrans == null) {
            throw new BlazeInvalidArgException(TRANSACTION, "Transaction does not exist.");
        }

        if (!dbTrans.isActive()) {
            return dbTrans;
        }

        dbTrans.setPaid(false);
        dbTrans.setProcessedTime(null);
        if (paymentOption != null) {
            dbTrans.getCart().setPaymentOption(paymentOption);
        } else {
            paymentOption = dbTrans.getCart().getPaymentOption();
        }
        transactionRepository.markTransactionAsUnpaid(token.getCompanyId(),transactionId, paymentOption);
        realtimeService.sendRealTimeEvent(token.getShopId(), RealtimeService.RealtimeEventType.QueueUpdateEvent, null);
        return dbTrans;
    }

    @Override
    public MemberSalesDetail getMemberSalesDetail(String memberId) {
        Member member = memberRepository.get(token.getCompanyId(), memberId);
        if (member == null) {
            throw new BlazeInvalidArgException(TRANSACTION, "Member does not exist.");
        }
        MemberSalesDetail memberSalesDetail = transactionRepository.getSalesByMember(token.getCompanyId(), token.getShopId(), member.getId());
        double avgAmount = (memberSalesDetail.getTotalVisit() == 0) ? 0.0 : memberSalesDetail.getTotal().doubleValue() / memberSalesDetail.getTotalVisit();
        memberSalesDetail.setAvgAmount(new BigDecimal(avgAmount));

        HashMap<ObjectId, Double> purchaseProductsMap = new HashMap<>();
        if (memberSalesDetail.getPurchaseProducts() != null && !memberSalesDetail.getPurchaseProducts().isEmpty()) {
            for (PurchaseProduct product : memberSalesDetail.getPurchaseProducts()) {
                if (StringUtils.isBlank(product.getProductId()) || !ObjectId.isValid(product.getProductId())) {
                    continue;
                }
                double qty = purchaseProductsMap.getOrDefault(new ObjectId(product.getProductId()), 0.0);
                purchaseProductsMap.put(new ObjectId(product.getProductId()), qty + product.getQuantity().doubleValue());
            }
        }

        HashMap<String, Product> productHashMap = productRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(purchaseProductsMap.keySet()));
        List<PurchaseProduct> purchaseProductList = new ArrayList<>();
        for (ObjectId key : purchaseProductsMap.keySet()) {
            PurchaseProduct purchaseProduct = new PurchaseProduct();
            purchaseProduct.setProductName((productHashMap.containsKey(key.toString())) ? productHashMap.get(key.toString()).getName() : "");
            purchaseProduct.setProductId(key.toString());
            purchaseProduct.setQuantity(new BigDecimal(purchaseProductsMap.get(key)));
            purchaseProductList.add(purchaseProduct);
        }
        purchaseProductList.sort(new Comparator<PurchaseProduct>() {
            @Override
            public int compare(PurchaseProduct o1, PurchaseProduct o2) {
                return o2.getQuantity().compareTo(o1.getQuantity());
            }
        });
        if (!purchaseProductList.isEmpty()) {
            memberSalesDetail.setPurchaseProducts(purchaseProductList.subList(0, (purchaseProductList.size() < 5) ? purchaseProductList.size() : 5));
        }
        return memberSalesDetail;
    }


    @Override
    public SearchResult<QueuedTransaction> getQueuedTransactions(String transactionId) {
        if (StringUtils.isBlank(transactionId) || !ObjectId.isValid(transactionId)) {
            throw new BlazeInvalidArgException("Transaction", "Transaction does not exist.");
        }
        return queuedTransactionRepository.getQueuedTransactionsBySourceId(token.getCompanyId(), token.getShopId(), transactionId);
    }

    private void createTookanTask(Transaction transaction, Employee employee, Member member, String teamId, boolean isReassign, boolean isCreate, Transaction.QueueType queueType) {

        EmployeeTookanInfo employeeTookanInfo = null;

        if (employee != null) {

            if (employee.getTookanInfoList() == null || employee.getTookanInfoList().isEmpty()) {
                throw new BlazeInvalidArgException("Tookan Task", "Employee is not linked with tookan.");
            }
            for (EmployeeTookanInfo tookanInfo : employee.getTookanInfoList()) {
                if (tookanInfo.getShopId().equals(token.getShopId())) {
                    employeeTookanInfo = tookanInfo;
                }
            }
            if (employeeTookanInfo == null) {
                throw new BlazeInvalidArgException("Tookan Task", "Employee is not linked with tookan.");
            }
            if (StringUtils.isNotBlank(teamId)) {
                TookanTeamInfo teamInfo = tookanService.getTookanTeamInfo(token.getCompanyId(), token.getShopId(), teamId);
                if (teamInfo == null) {
                    throw new BlazeInvalidArgException("Tookan Task", "Team does not exist.");
                }
                boolean isTeamAssigned = tookanService.checkTeamAssignment(token.getShopId(), teamId, employeeTookanInfo);
                if (!isTeamAssigned) {
                    LOG.info("Team :" + teamInfo.getTeamName() + " does not assigned to employee :" + employee.getFirstName());
                    throw new BlazeInvalidArgException("Tookan Task", "Team :" + teamInfo.getTeamName() + " does not assigned to employee :" + employee.getFirstName());
                }
            }
        }
        if (queueType == Transaction.QueueType.WalkIn && member == null) {
            throw new BlazeInvalidArgException("Tookan Task", "Member is not found");
        }

        if (isCreate) {
            tookanService.createTookanTask(transaction, employee, member, teamId, (employeeTookanInfo != null) ? employeeTookanInfo.getTookanAgentId() : "", isReassign);
        }
    }

    @Override
    public Transaction holdUnAssignedTransaction(String transactionId, Transaction incomingTrans) {
        Transaction dbTrans = transactionRepository.get(token.getCompanyId(), transactionId);
        if (dbTrans == null) {
            throw new BlazeInvalidArgException("TransactionId", "Transaction does not exist.");
        }
        if (!dbTrans.isActive()) {
            return dbTrans;
        }
        if (dbTrans.getStatus() == Transaction.TransactionStatus.Completed) {
            dbTrans.setActive(false);
            transactionRepository.deactivate(token.getCompanyId(), dbTrans.getId());
            return dbTrans;
        }

        if (dbTrans.isFulfillingFulfillment()) {
            if (incomingTrans.getCart().getItems().size() == 0
                    && dbTrans.getCart().getItems().size() > 0) {
                return dbTrans;
            }
        }

        if (dbTrans.isLocked() && this.didLockedOrderChange(dbTrans, incomingTrans)) {
            throw new BlazeInvalidArgException(TRANSACTION, TRANSACTION_LOCKED);
        }

        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());

        String sellerTerminalId = StringUtils.EMPTY;

        // set override inventory
        incomingTrans.setOverrideInventoryId(dbTrans.getOverrideInventoryId());

        if (!incomingTrans.isAssigned()) {
            checkAvailableInventory(incomingTrans, dbTrans, "hold", null, sellerTerminalId, Boolean.TRUE);
        } else {
            sellerTerminalId = getSellerTerminalId(shop, dbTrans.getAssignedEmployeeId());

            checkAvailableInventory(incomingTrans, dbTrans, "hold", null, sellerTerminalId, Boolean.FALSE);
        }

        // create new note
        if (incomingTrans.getNote() != null && StringUtils.isNotEmpty(incomingTrans.getNote().getMessage())) {
            Note note = new Note();
            note.setId(ObjectId.get().toString());
            note.setMessage(incomingTrans.getNote().getMessage());
            dbTrans.setNote(note);
        }


        Transaction.TransactionStatus newStatus = Transaction.TransactionStatus.Hold;
        if (incomingTrans.getCart().getItems().size() == 0) {
            newStatus = Transaction.TransactionStatus.Queued; // Go back to the queue
            dbTrans.setStartTime(0L);
        }
        dbTrans.setSellerTerminalId(sellerTerminalId);
        dbTrans.setSellerId(token.getActiveTopUser().getUserId());
        dbTrans.setStatus(newStatus);
        dbTrans.setTimeZone(token.getRequestTimeZone());
        boolean isPickUp = dbTrans.getQueueType() == Transaction.QueueType.Special;
        if (isPickUp) {
            dbTrans.setPickUpDate(incomingTrans.getPickUpDate());
        } else {
            dbTrans.setDeliveryDate(incomingTrans.getDeliveryDate());
        }
        dbTrans.setCompleteAfter(incomingTrans.getCompleteAfter());

        final CustomerInfo customerInfo = getMemberGroup(dbTrans.getMemberId());
        //cartService.prepareCart(shop, dbTrans, false, newStatus, false, customerInfo, false);
        cartService.prepareCart(shop, incomingTrans, false, newStatus, false, customerInfo, false);

        // If it's preparing, just save it (we're not taking it from the inventory)
        if (dbTrans.isPreparingFulfillment()) {
            // we're reparing to just save it
            dbTrans.setCart(incomingTrans.getCart());
            dbTrans.setStatus(newStatus);
            populatePrepareQty(dbTrans);
            transactionRepository.update(token.getCompanyId(), dbTrans.getId(), dbTrans);
        } else {
            dbTrans.setCart(incomingTrans.getCart());
            transactionRepository.update(token.getCompanyId(), dbTrans.getId(), dbTrans);
        }

        dbTrans.setCart(incomingTrans.getCart());
        dbTrans.setMember(memberRepository.get(token.getCompanyId(), dbTrans.getMemberId()));

        realtimeService.sendRealTimeEvent(token.getShopId(), RealtimeService.RealtimeEventType.QueueUpdateEvent, null);
        return dbTrans;
    }

    @Override
    public Transaction addOrUpdateOrderTag(String transactionId, AddOrderTagsRequest request) {

        Transaction transaction = transactionRepository.get(token.getCompanyId(), transactionId);
        if (transaction == null) {
            throw new BlazeInvalidArgException(TRANSACTION, TRANSACTION_NOT_FOUND);
        }

        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());
        if (shop == null) {
            throw new BlazeInvalidArgException(SHOP, SHOP_NOT_FOUND);
        }

        transaction.setOrderTags(request.getOrderTags());

        Set<String> shopOrderTags = shop.getOrderTags();
        if (shopOrderTags != null) {
            boolean updateShopTag = Boolean.FALSE;
            for (String tag : request.getOrderTags()) {
                if (!shopOrderTags.contains(tag)) {
                    shopOrderTags.add(tag);
                    updateShopTag = Boolean.TRUE;
                }
            }
            if (updateShopTag) {
                shopRepository.updateOrderTags(token.getCompanyId(), token.getShopId(), shopOrderTags);
            }
        }

        transactionRepository.updateOrderTags(token.getCompanyId(), transactionId, request.getOrderTags());

        realtimeService.sendRealTimeEvent(token.getShopId(), RealtimeService.RealtimeEventType.QueueUpdateEvent, null);
        return transaction;
    }

    @Override
    public Transaction updatePackedBy(String transactionId, EmployeePackByRequest request) {
        Transaction transaction = transactionRepository.get(token.getCompanyId(), transactionId);
        if (transaction == null) {
            throw new BlazeInvalidArgException(TRANSACTION, TRANSACTION_NOT_FOUND);
        }

        String packByEmployeeId = request.getPackedBy();

        if (StringUtils.isNotBlank(packByEmployeeId)) {
            Employee employee = employeeRepository.get(token.getCompanyId(), packByEmployeeId);

            if (employee == null) {
                throw new BlazeInvalidArgException(TRANSACTION, EMPLOYEE_NOT_FOUND);
            }

            transaction.setPackedBy(employee.getId());
            transaction.setPackedDate(DateTime.now().getMillis());
        } else {
            transaction.setPackedBy(StringUtils.EMPTY);
            transaction.setPackedDate(0L);
        }

        transactionRepository.updatePackedBy(transactionId, transaction.getPackedBy(), transaction.getPackedDate());

        realtimeService.sendRealTimeEvent(token.getShopId(), RealtimeService.RealtimeEventType.QueueUpdateEvent, null);
        return transaction;
    }

    private double prepareRefundItems(RefundRequest request, Transaction transaction) {
        if (transaction.getCart() == null || transaction.getCart().getItems() == null || transaction.getCart().getItems().isEmpty()) {
            return 0;
        }

        double availableRefundAmt = 0;
        BigDecimal reqRefundAmount = request.getTotalRefundAmt();
        BigDecimal totalRefundedAmount = BigDecimal.ZERO;
        BigDecimal cartTotal = BigDecimal.ZERO;
        BigDecimal refundTotal = BigDecimal.ZERO;
        List<ObjectId> productIds = new ArrayList<>();

        for (OrderItem orderItem : transaction.getCart().getItems()) {
            productIds.add(new ObjectId(orderItem.getProductId()));

        }

        HashMap<String, Product> productMap = productRepository.listAsMap(token.getCompanyId(), productIds);
        HashMap<String, Double> origPropMap = new HashMap<>();

        /* Calculate already refunded amount*/
        for (RefundOrderItemRequest item : transaction.getCart().getRefundOrderItemRequests()) {
            totalRefundedAmount = totalRefundedAmount.add(item.getRefundAmt());
        }

        for (OrderItem orderItem : transaction.getCart().getItems()) {
            Product product = productMap.get(orderItem.getProductId());
            if (product != null) {
                if (product.isDiscountable()) {
                    cartTotal = cartTotal.add(orderItem.getFinalPrice());
                }
            }
        }

        for (OrderItem orderItem : transaction.getCart().getItems()) {
            Product product = productMap.get(orderItem.getProductId());
            if (product != null) {
                double ratio = 1;
                if (product.isDiscountable() && cartTotal.doubleValue() > 0) {
                    ratio = orderItem.getFinalPrice().doubleValue() / cartTotal.doubleValue();
                }
                origPropMap.put(orderItem.getId(), ratio);
            }
        }

        for (RefundOrderItemRequest itemRequest : request.getRefundOrderItemRequests()) {
            for (OrderItem item : transaction.getCart().getItems()) {
                if (!item.getOrderItemId().equals(itemRequest.getOrderItemId())) {
                    continue;
                }
                refundTotal = refundTotal.add(item.getFinalPrice());
            }
        }

        for (RefundOrderItemRequest refundOrderItemRequest : request.getRefundOrderItemRequests()) {
            for (OrderItem item : transaction.getCart().getItems()) {
                if (!item.getOrderItemId().equals(refundOrderItemRequest.getOrderItemId())) {
                    continue;
                }
                double ratio = origPropMap.getOrDefault(item.getId(), 1D);

                Double propCartDiscount = transaction.getCart().getCalcCartDiscount() != null ? transaction.getCart().getCalcCartDiscount().doubleValue() * ratio : 0;
                Double propAfterTaxDiscount = transaction.getCart().getAppliedAfterTaxDiscount() != null ? transaction.getCart().getAppliedAfterTaxDiscount().doubleValue() * ratio : 0;

                //Add Taxes
                double itemTaxes = 0;
                if (item.getTaxResult() != null) {
                    TaxResult taxResult = item.getTaxResult();
                    itemTaxes += taxResult.getTotalALPostExciseTax().doubleValue()
                            + taxResult.getTotalExciseTax().doubleValue()
                            + taxResult.getTotalCityTax().doubleValue()
                            + taxResult.getTotalFedTax().doubleValue()
                            + taxResult.getTotalStateTax().doubleValue()
                            + taxResult.getTotalCountyTax().doubleValue();

                }
                if (itemTaxes == 0) {
                    double subTotalAfterDiscount = item.getFinalPrice().doubleValue() - propCartDiscount;
                    if ((item.getTaxTable() == null || !item.getTaxTable().isActive()) && item.getTaxInfo() != null) {
                        TaxInfo taxInfo = item.getTaxInfo();
                        if (item.getTaxOrder() == TaxInfo.TaxProcessingOrder.PostTaxed) {
                            itemTaxes += subTotalAfterDiscount * taxInfo.getCityTax().doubleValue();
                            itemTaxes += subTotalAfterDiscount * taxInfo.getStateTax().doubleValue();
                            itemTaxes += subTotalAfterDiscount * taxInfo.getFederalTax().doubleValue();
                        }
                    }
                }

                if (itemTaxes == 0) {
                    itemTaxes += item.getCalcTax().doubleValue();
                }

                double itemFinalPrice = item.getFinalPrice().doubleValue() - propCartDiscount
                        + itemTaxes - propAfterTaxDiscount;

                // per quantity refund amount should not be greater than perUnitFinalPrice.
                item.setPerUnitRefundAmt(BigDecimal.valueOf(itemFinalPrice / item.getQuantity().doubleValue()));
                availableRefundAmt += item.getPerUnitRefundAmt().doubleValue() * refundOrderItemRequest.getQuantity().doubleValue();

                if (Objects.equals(reqRefundAmount, BigDecimal.ZERO) && request.getRefundVersion() != RefundRequest.RefundVersion.NEW) {
                    reqRefundAmount = BigDecimal.valueOf(itemFinalPrice);
                }
                // set refund amount proportionally
                if (refundTotal.doubleValue() > 0) {
                    ratio = item.getFinalPrice().doubleValue() / refundTotal.doubleValue();
                    refundOrderItemRequest.setRefundAmt(reqRefundAmount.multiply(BigDecimal.valueOf(ratio)));
                }
            }
        }

        return NumberUtils.round(availableRefundAmt, 2);
    }

    @Override
    public Transaction updateScheduleTimeForTransaction(String transactionId, OrderScheduleUpdateRequest request) {
        Transaction dbTransaction = transactionRepository.getByShopAndId(token.getCompanyId(), token.getShopId(), transactionId);
        if (dbTransaction == null) {
            throw new BlazeInvalidArgException(TRANSACTION, TRANSACTION_NOT_FOUND);
        }
        Shop shop = shopRepository.getById(token.getShopId());

        if (request == null) {
            throw new BlazeInvalidArgException(TRANSACTION, "Request cannot be blank.");
        }
        boolean isPickUp = dbTransaction.getQueueType() == Transaction.QueueType.Special;

        if (request.getScheduleDate() == 0) {
            throw new BlazeInvalidArgException(TRANSACTION, "Schedule date cannot be blank.");
        }

        if (isPickUp) {
            transactionRepository.updatePickupDate(dbTransaction.getId(), request.getCompleteAfter(), request.getScheduleDate());
            dbTransaction.setPickUpDate(request.getScheduleDate());
        } else {
            transactionRepository.updateDeliveryDate(dbTransaction.getId(), request.getCompleteAfter(),request.getScheduleDate());
            dbTransaction.setDeliveryDate(request.getScheduleDate());
        }
        dbTransaction.setCompleteAfter(request.getCompleteAfter());

        if (shop != null && shop.isEnableOnFleet() && StringUtils.isNotBlank(dbTransaction.getOnFleetTaskId())) {
            Employee employee = employeeRepository.get(token.getCompanyId(), dbTransaction.getAssignedEmployeeId());
            String employeeWorkerId = "";
            EmployeeOnFleetInfo employeeOnFleetInfo = onFleetService.getEmployeeOnfFleetInfoByShop(employee, shop.getId());
            if (employeeOnFleetInfo != null) {
                employeeWorkerId = employeeOnFleetInfo.getOnFleetWorkerId();
            }

            Member member = memberRepository.get(token.getCompanyId(), dbTransaction.getMemberId());
            onFleetService.updateOnfleetTaskSchedule(shop, dbTransaction, employeeWorkerId, employee, employeeOnFleetInfo, member);
        }

        if (dbTransaction.isCreateTookanTask() && StringUtils.isNotBlank(dbTransaction.getTookanTaskId())) {
            tookanService.updateTaskStatus(token.getCompanyId(), token.getShopId(), dbTransaction, null);
        }
        return dbTransaction;


    }

    @Override
    public Transaction updateDeliveryAddress(String transactionId, Address request) {
        boolean isFound = false;
        Transaction dbTransaction = transactionRepository.getByShopAndId(token.getCompanyId(), token.getShopId(), transactionId);
        if (dbTransaction == null) {
            throw new BlazeInvalidArgException(TRANSACTION, TRANSACTION_NOT_FOUND);
        }

        if (request == null) {
            throw new BlazeInvalidArgException(TRANSACTION, "Request cannot be blank.");
        }
        if (dbTransaction.isActive() == false) {
            throw new BlazeInvalidArgException(TRANSACTION, "Transaction is no longer active.");
        }
        request.prepare(token.getCompanyId());

        transactionRepository.updateDeliveryAddress(dbTransaction.getId(), request);
        dbTransaction.setDeliveryAddress(request);
        Member member = memberRepository.get(token.getCompanyId(), dbTransaction.getMemberId());

        if (StringUtils.isNotBlank(dbTransaction.getOnFleetTaskId())) {
            Shop shop = shopRepository.getById(token.getShopId());
            if (shop != null && shop.isEnableOnFleet()) {
                Employee employee = employeeRepository.get(token.getCompanyId(), dbTransaction.getAssignedEmployeeId());
                String employeeWorkerId = "";
                EmployeeOnFleetInfo employeeOnFleetInfo = onFleetService.getEmployeeOnfFleetInfoByShop(employee, shop.getId());
                if (employeeOnFleetInfo != null) {
                    employeeWorkerId = employeeOnFleetInfo.getOnFleetWorkerId();
                }
                onFleetService.updateOnfleetTaskSchedule(shop, dbTransaction, employeeWorkerId, employee, employeeOnFleetInfo, member);
            }
        }

        if (dbTransaction.isCreateTookanTask() && StringUtils.isNotBlank(dbTransaction.getTookanTaskId())) {
            tookanService.updateTaskStatus(token.getCompanyId(), token.getShopId(), dbTransaction, null);
        }

        if (member != null) {
            for (Address address : member.getAddresses()) {
                if ((address.getAddressString().equalsIgnoreCase(request.getAddressString()))) {
                    isFound = true;
                }
            }
            if (!isFound) {
                memberRepository.updateMemberAddress(token.getCompanyId(), member.getId(), request);
            }
        }
        String message = String.format("Delivery Address updated for Transaction %s", dbTransaction.getTransNo());
        String title = "Update Delivery Address";
        sendNotification(dbTransaction, message, title, FcmPayload.SubType.UPDATE);
        return dbTransaction;


    }

    @Override
    public Member acceptOrUpdateConsumer(ConsumerUser dbConsumerUser, ConsumerUser consumerUser, String memberId) {

        dbConsumerUser.setAccepted(Boolean.TRUE);
        dbConsumerUser.setAcceptedDate(DateTime.now().getMillis());
        dbConsumerUser.setLastSyncDate(DateTime.now().getMillis());


        if (consumerUser != null) {
            dbConsumerUser.setDlState(consumerUser.getDlState());
            dbConsumerUser.setDlExpiration(consumerUser.getDlExpiration());
            dbConsumerUser.setFirstName(consumerUser.getFirstName());
            dbConsumerUser.setLastName(consumerUser.getLastName());
            if (consumerUser.getDlPhoto() != null) {
                if (consumerUser.getDlPhoto().getAssetType() != null) {
                    dbConsumerUser.setDlPhoto(consumerUser.getDlPhoto());
                }
            }

            dbConsumerUser.setNotificationType(consumerUser.getNotificationType());
            dbConsumerUser.setDob(consumerUser.getDob());

            dbConsumerUser.setAddress(consumerUser.getAddress());
            dbConsumerUser.setSex(consumerUser.getSex());
            dbConsumerUser.setEmailOptIn(consumerUser.isEmailOptIn());
            dbConsumerUser.setTextOptIn(consumerUser.isTextOptIn());
            dbConsumerUser.setVerified(consumerUser.isVerified());
            dbConsumerUser.setPrimaryPhone(consumerUser.getPrimaryPhone());


            dbConsumerUser.setDoctorFirstName(consumerUser.getDoctorFirstName());
            dbConsumerUser.setDoctorLastName(consumerUser.getDoctorLastName());
            dbConsumerUser.setDoctorLicense(consumerUser.getDoctorLicense());
            dbConsumerUser.setVerificationPhone(consumerUser.getVerificationPhone());
            dbConsumerUser.setVerificationWebsite(consumerUser.getVerificationWebsite());
            dbConsumerUser.setVerifyMethod(consumerUser.getVerifyMethod());
            dbConsumerUser.setConsumerType(consumerUser.getConsumerType());
            dbConsumerUser.setRecExpiration(consumerUser.getRecExpiration());
            dbConsumerUser.setRecNo(consumerUser.getRecNo());
            dbConsumerUser.setRecIssueDate(consumerUser.getRecIssueDate());
            if (consumerUser.getRecPhoto() != null && consumerUser.getRecPhoto().getAssetType() != null) {
                dbConsumerUser.setRecPhoto(consumerUser.getRecPhoto());
                dbConsumerUser.setMedical(true);
            } else {
                dbConsumerUser.setMedical(false);
            }
            if (dbConsumerUser.getAddress() != null) {
                dbConsumerUser.getAddress().setId(null);
            }
            dbConsumerUser.setMarketingSource(consumerUser.getMarketingSource());
        }

        Member member = createOrUpdateMember(memberId, dbConsumerUser);
        //set updated doctor info
        dbConsumerUser.setDoctorFirstName(dbConsumerUser.getDoctorFirstName());
        dbConsumerUser.setDoctorLastName(dbConsumerUser.getDoctorLastName());
        dbConsumerUser.setVerificationWebsite(StringUtils.isNoneBlank(dbConsumerUser.getVerificationWebsite()) ? dbConsumerUser.getVerificationWebsite() : dbConsumerUser.getVerificationWebsite());
        dbConsumerUser.setVerificationPhone(StringUtils.isNoneBlank(dbConsumerUser.getVerificationPhone()) ? dbConsumerUser.getVerificationPhone() : dbConsumerUser.getVerificationPhone());

        dbConsumerUser.setMemberId(member.getId());
//        memberStatus.setMemberId(member.getId());

        // save the db consumeruser
        consumerUserRepository.update(dbConsumerUser.getId(), dbConsumerUser);

        member.setStatus(Member.MembershipStatus.Active);

        return memberRepository.update(token.getCompanyId(), member.getId(), member);
    }

    /**
     * This method list all transaction with :
     * @param afterDate : afterDate
     * @param beforeDate : beforeDate
     * @param  start : start,
     * @param  limit : limit
     * returns TransactionLimitedResultList with limited data.
     */
    @Override
    public SearchResult<TransactionLimitedResult> getTransactionsWithLimitedResult(long afterDate, long beforeDate, int start, int limit) {

        SearchResult<TransactionLimitedResult> result = new SearchResult<>();

        List<TransactionLimitedResult> transactionList = new ArrayList<>();

        if (afterDate < 0) {
            afterDate = 0;
        }
        if (beforeDate == 0) {
            beforeDate = DateTime.now().getMillis();
        }
        if (start < 0) start = 0;
        if (limit <= 0 || limit > 500) {
            limit = 200;
        }


        SearchResult<Transaction> transactions = transactionRepository.getAllTransactions(token.getCompanyId(), token.getShopId(), start, limit, afterDate, beforeDate);

        for (Transaction transaction : transactions.getValues()) {

            TransactionLimitedResult limitedResult = new TransactionLimitedResult();

            limitedResult.setId(transaction.getId());
            limitedResult.setShopId(transaction.getShopId());
            limitedResult.setCompanyId(transaction.getCompanyId());
            limitedResult.setTransNo(transaction.getTransNo());
            limitedResult.setStatus(transaction.getStatus());
            limitedResult.setPickUpDate(transaction.getPickUpDate());
            limitedResult.setQueueType(transaction.getQueueType());
            limitedResult.setAssignedEmployeeId(transaction.getAssignedEmployeeId());
            limitedResult.setDeliveryAddress(transaction.getDeliveryAddress());
            limitedResult.setDeliveryDate(transaction.getDeliveryDate());
            limitedResult.setMemberId(transaction.getMemberId());
            limitedResult.setCart(transaction.getCart());
            limitedResult.setSellerId(transaction.getSellerId());
            limitedResult.setSellerTerminalId(transaction.getSellerTerminalId());
            limitedResult.setTransType(transaction.getTransType());
            limitedResult.setTraceSubmitStatus(transaction.getTraceSubmitStatus());
            limitedResult.setStartTime(transaction.getStartTime());
            limitedResult.setEndTime(transaction.getEndTime());
            limitedResult.setProcessedTime(transaction.getProcessedTime());
            limitedResult.setActive(transaction.isActive());
            limitedResult.setPaid(transaction.isPaid());
            limitedResult.setPaidTime(transaction.getPaidTime());
            limitedResult.setMemo(transaction.getMemo());
            limitedResult.setOverrideInventoryId(transaction.getOverrideInventoryId());
            limitedResult.setCheckinTime(transaction.getCheckinTime());
            limitedResult.setPreparedDate(transaction.getPreparedDate());
            limitedResult.setPreparedBy(transaction.getPreparedBy());
            limitedResult.setConsumerCartId(transaction.getConsumerCartId());

            transactionList.add(limitedResult);
        }

        result.setValues(transactionList);
        result.setTotal(transactions.getTotal());
        result.setLimit(transactions.getLimit());

        return result;
    }



    @Override
    public AllQueueResult getTransactionsWithDate() {
        return getTransactionsWithDate(0, DateTime.now().getMillis());
    }

    @Override
    public Transaction finalizeTransaction(String transactionId, boolean finalize) {
        Transaction transaction = transactionRepository.get(token.getCompanyId(), transactionId);
        if (transaction == null) {
            throw new BlazeInvalidArgException(TRANSACTION, TRANSACTION_NOT_FOUND);
        }
        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());

        //If status is true, the selected order should be finalized
        if (finalize) {
            cartService.finalizeAllOrderItems(shop, transaction);
        } else {
            cartService.unfinalizeAllOrderItems(shop, transaction);
        }

        final Transaction dbTransaction = transactionRepository.save(transaction);
        return dbTransaction;
    }

    /**
     *  This method is used to update Payment Option for given transaction.
     * @param transactionId :
     * @param paymentOption
     * @param isPrepare
     * @return
     */
    @Override
    public Transaction updatePaymentMethod(String transactionId, PaymentOptionUpdateRequest paymentOption, boolean isPrepare) {
        Employee employee = employeeRepository.getById(token.getActiveTopUser().getUserId());
        if (employee == null) {
            throw new BlazeInvalidArgException(TRANSACTION, "Employee Not Found");
        }
        roleService.checkPermission(token.getCompanyId(), employee.getRoleId(), Role.Permission.WebEditPosPayment);

        Transaction dbTransaction = transactionRepository.getByShopAndId(token.getCompanyId(), token.getShopId(), transactionId);
        if (dbTransaction == null) {
            throw new BlazeInvalidArgException(TRANSACTION, TRANSACTION_NOT_FOUND);
        }
        if (dbTransaction.getStatus() != Transaction.TransactionStatus.Completed
                && dbTransaction.getStatus() != Transaction.TransactionStatus.RefundWithInventory
                && dbTransaction.getStatus() != Transaction.TransactionStatus.RefundWithoutInventory) {
            throw new BlazeInvalidArgException(TRANSACTION, String.format(TRANSACTION_NOT_COMPLETE, dbTransaction.getTransNo()));
        }
        if (paymentOption == null) {
            throw new BlazeInvalidArgException(TRANSACTION, BLANK_REQUEST);
        }
        if (paymentOption.getPaymentOption() == null || paymentOption.getPaymentOption() == Cart.PaymentOption.None) {
            throw new BlazeInvalidArgException(TRANSACTION, BLANK_PAYMENT_OPTION);
        }
        if (paymentOption.getPaymentOption() == Cart.PaymentOption.Split && paymentOption.getSplitPayment() == null) {
            throw new BlazeInvalidArgException(TRANSACTION, SPLIT_PAYMENT_NOT_FOUND);
        }
        if (dbTransaction.isPayingWithPaymentCard()) {
            throw new BlazeInvalidArgException(TRANSACTION, String.format("Cannot update payment type for %s", dbTransaction.getCart().getPaymentType()));
        }

        if (paymentOption.getPaymentOption() == Cart.PaymentOption.Split) {
            SplitPayment splitPayment = paymentOption.getSplitPayment();
            BigDecimal total = dbTransaction.getCart().getTotal().subtract(dbTransaction.getCart().getCreditCardFee());
            if (splitPayment.getTotalSplits().doubleValue() < total.doubleValue()) {
                throw new BlazeInvalidArgException(TRANSACTION, "Split payment amount is less then transaction's total amount.");
            }
        }

        Shop shop = shopRepository.get(token.getCompanyId(), dbTransaction.getShopId());
        String sellerTerminalId = getSellerTerminalId(shop, dbTransaction.getAssignedEmployeeId());
        Member member = memberRepository.getById(dbTransaction.getMemberId());

        //Member Info
        final CustomerInfo customerInfo = getMemberGroup(dbTransaction.getMemberId());
        Terminal sellerTerminal = terminalRepository.getTerminalById(token.getCompanyId(), sellerTerminalId);
        if (sellerTerminal == null || sellerTerminal.isDeleted() || !sellerTerminal.isActive()) {

            throw new BlazeInvalidArgException("Terminal",
                    String.format("Terminal '%s' is no longer active.",
                            sellerTerminal == null ? "" : sellerTerminal.getName()));
        }

        checkCashDrawerAvailability(shop, sellerTerminalId, sellerTerminal);

        dbTransaction.getCart().setSplitPayment(paymentOption.getSplitPayment());
        dbTransaction.getCart().setPaymentOption(paymentOption.getPaymentOption());
        dbTransaction.setProcessedTime(DateTime.now().getMillis());
        dbTransaction.setPaymentEditedBy(employee.getId());
        dbTransaction.setPaymentEditedTime(DateTime.now().getMillis());
        if (dbTransaction.getCart().getSplitPayment() != null && dbTransaction.getCart().getPaymentOption() != Cart.PaymentOption.Split) {
            dbTransaction.getCart().setSplitPayment(null);
        }

        cartService.prepareCartForPaymentType(shop, dbTransaction, customerInfo);

        if (!isPrepare) {
            transactionRepository.updatePaymentOption(token.getCompanyId(), transactionId, dbTransaction.getCart());
            processCurrentCashDrawer(shop, sellerTerminalId);
            updateCorrespondingConsumerCart(shop, dbTransaction, dbTransaction, member);
        }
        return dbTransaction;
    }

    /**
     * This method is used to generate bulk PDF zip file for given transactions.
     * @param transactionIds
     * @return
     */
    @Override
    public InputStream bulkPdfForTransactions(String transactionIds) {
        if (StringUtils.isBlank(transactionIds)) {
            throw new BlazeInvalidArgException(TRANSACTION, TRANSACTION_NOT_FOUND);
        }
        List<String> transactionIdsList = (Arrays.asList(StringUtils.split(transactionIds, "\\s*,\\s*")));
        Set<ObjectId> transactionObjId = new HashSet<>();
        for (String id : transactionIdsList) {
            if (StringUtils.isNotBlank(id) && ObjectId.isValid(id.trim())) {
                transactionObjId.add(new ObjectId(id.trim()));
            }
        }
        HashMap<String, Transaction> transactionIdMap = transactionRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(transactionObjId));
        HashMap<String, InputStream> streamList = new HashMap<>();
        Company company = companyRepository.getById(token.getCompanyId());
        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());
        Set<ObjectId> membersIds = new HashSet<>();
        List<ObjectId> objectIds = new ArrayList<>();
        Set<ObjectId> brandIds = new HashSet<>();
        for (Transaction transaction : transactionIdMap.values()) {
            if (StringUtils.isNotBlank(transaction.getMemberId()) && ObjectId.isValid(transaction.getMemberId())) {
                membersIds.add(new ObjectId(transaction.getMemberId()));
            }
            for (OrderItem item : transaction.getCart().getItems()) {
                objectIds.add(new ObjectId((item.getProductId())));
            }
        }
        HashMap<String, Product> productHashMap = productRepository.findProductsByIdsAsMap(token.getCompanyId(), token.getShopId(), objectIds);
        for (Product product : productHashMap.values()) {
            if (StringUtils.isNotBlank(product.getBrandId()) && ObjectId.isValid(product.getBrandId())) {
                brandIds.add(new ObjectId(product.getBrandId()));
            }
        }
        HashMap<String, Member> memberHashMap = memberRepository.findItemsInAsMap(token.getCompanyId(), Lists.newArrayList(membersIds));
        HashMap<String, Brand> brandHashMap = brandRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(brandIds));

        InputStream inputStream = ShopServiceImpl.class.getResourceAsStream("/receipt.html");
        StringWriter writer = new StringWriter();
        try {
            IOUtils.copy(inputStream, writer, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        String body = writer.toString();
        StringBuilder orderDetails = new StringBuilder();
        for (Transaction transaction : transactionIdMap.values()) {
            orderDetails.append(getReceiptBody(company, memberHashMap.get(transaction.getMemberId()), transaction, productHashMap, shop, brandHashMap));
        }
        body = body.replace("==orderInformationDetails==", orderDetails);
        byte[] bytes = PdfGenerator.exportToPdfBox(body, "/receipt.html");

        Employee employee = employeeRepository.get(token.getCompanyId(), token.getActiveTopUser().getUserId());
        if (employee != null && StringUtils.isNotBlank(employee.getEmail())) {
            CompanyAsset asset = assetService.uploadAssetPrivate(new ByteArrayInputStream(bytes), "Bulk_dispatch_orders.pdf", Asset.AssetType.Document, "application/pdf");
            companyAssetRepository.save(asset);
            reportManager.uploadSendBulkOrderMail(token.getCompanyId(), token.getShopId(), employee, asset);
        }
        return new ByteArrayInputStream(bytes);
    }

    /**
     * This method is used for transaction  payment  with Linux, Clover, Mtrac payment type.
     * @param dbTrans
     * @param transaction
     * @return
     */
    private Transaction processPaymentOption(Transaction dbTrans, Transaction transaction) {
        if (transaction.getCart().getPaymentOption() == Cart.PaymentOption.Linx) {
            ProcessPaymentCardsForTransactionEvent processLoyaltyCards = new ProcessPaymentCardsForTransactionEvent();
            processLoyaltyCards.setPayload(dbTrans, transaction);
            eventBus.post(processLoyaltyCards);
            processLoyaltyCards.getResponse();
            dbTrans = processLoyaltyCards.getDbTransaction();
        } else if (transaction.getCart().getPaymentOption() == Cart.PaymentOption.Clover) {
            CloverPaymentCardsForTransactionEvent processLoyaltyCards = new CloverPaymentCardsForTransactionEvent();
            processLoyaltyCards.setPayload(dbTrans, transaction);
            eventBus.post(processLoyaltyCards);
            processLoyaltyCards.getResponse();
            dbTrans = processLoyaltyCards.getDbTransaction();
        } else if (transaction.getCart().getPaymentOption() == Cart.PaymentOption.Mtrac) {
            MtracPaymentCardsForTransactionEvent processLoyaltyCards = new MtracPaymentCardsForTransactionEvent();
            processLoyaltyCards.setPayload(dbTrans, transaction);
            eventBus.post(processLoyaltyCards);
            processLoyaltyCards.getResponse();
            dbTrans = processLoyaltyCards.getDbTransaction();
        }

        return dbTrans;
    }

    @Override
    public SearchResult<Transaction> getAllTransactionByEmployee(String employeeId, int start, int limit, String searchTerm) {
        start = (start < 0) ? 0 : start;
        limit = (limit == 0) ? 200 : limit;
        List<Transaction.TransactionStatus> statuses = new ArrayList<>();
        statuses.add(Transaction.TransactionStatus.Queued);
        statuses.add(Transaction.TransactionStatus.Hold);
        statuses.add(Transaction.TransactionStatus.InProgress);
        statuses.add(Transaction.TransactionStatus.Completed);
        statuses.add(Transaction.TransactionStatus.RefundWithInventory);
        statuses.add(Transaction.TransactionStatus.RefundWithoutInventory);
        SearchResult<Transaction> result = transactionRepository.getAllTransactionsAssignedByStatus(token.getCompanyId(), token.getShopId(), employeeId, statuses, start, limit, searchTerm);
        assignDependentObjects(result, true, true);
        return result;

    }

    @Override
    public InputStream createPdfForTransaction(String transactionId) {

        if (StringUtils.isBlank(transactionId)) {
            throw new BlazeInvalidArgException(TRANSACTION, TRANSACTION_NOT_FOUND);
        }

        Transaction transaction = transactionRepository.get(token.getCompanyId(), transactionId);

        if (transaction == null) {
            throw new BlazeInvalidArgException("Transaction", "Transaction  does not found");
        }


        List<ObjectId> productIds = new ArrayList<>();
        Set<ObjectId> brandIds = new HashSet<>();
        for (OrderItem item : transaction.getCart().getItems()) {
            productIds.add(new ObjectId((item.getProductId())));
        }

        HashMap<String, Product> productHashMap = productRepository.findProductsByIdsAsMap(token.getCompanyId(), token.getShopId(), productIds);

        for (Product product : productHashMap.values()) {
            if (StringUtils.isNotBlank(product.getBrandId()) && ObjectId.isValid(product.getBrandId())) {
                brandIds.add(new ObjectId(product.getBrandId()));
            }
        }

        HashMap<String, Brand> brandHashMap = brandRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(brandIds));
        Member member = memberRepository.get(token.getCompanyId(), transaction.getMemberId());

        InputStream inputStream = QueuePOSServiceImpl.class.getResourceAsStream("/driver_manifest.html");
        StringWriter writer = new StringWriter();
        try {
            IOUtils.copy(inputStream, writer, "UTF-8");
        } catch (IOException ex) {
            LOGGER.error("Error! in  manifest form", ex);
        }
        Company company = companyRepository.getById(token.getCompanyId());
        if (company == null) {
            throw new BlazeInvalidArgException("Company", "Company not found");
        }
        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());
        if (shop == null) {
            throw new BlazeInvalidArgException("Shop", "Shop not found");
        }
        String body = writer.toString();

        body = generateManifestBody(writer, company, member, transaction, productHashMap, shop, brandHashMap);

        return new ByteArrayInputStream(PdfGenerator.exportToPdfBox(body, "/driver_manifest.html"));
    }

    private String generateManifestBody(StringWriter writer, Company company, Member member, Transaction transaction, HashMap<String, Product> productHashMap, Shop shop, HashMap<String, Brand> brandHashMap) {
        String body = writer.toString();
        Employee employee = employeeRepository.get(token.getCompanyId(), transaction.getSellerId());
        Terminal terminal = null;
        Inventory inventory = null;

        if (employee != null && StringUtils.isNotBlank(employee.getAssignedTerminalId())) {
            terminal = terminalRepository.get(token.getCompanyId(), employee.getAssignedTerminalId());
        }
        if (terminal != null && StringUtils.isNotBlank(terminal.getAssignedInventoryId())) {
            inventory = inventoryRepository.get(token.getCompanyId(), terminal.getAssignedInventoryId());
        }
        String emptyString = "-";

        String timeZone = shop.getTimeZone();
        if (StringUtils.isBlank(timeZone)) {
            timeZone = token.getRequestTimeZone();
        }

        body = body.replaceAll("==transactionId==", StringUtils.isNotBlank(transaction.getTransNo()) ? transaction.getTransNo() : emptyString);
        body = body.replaceAll("==dispatchTime==", (transaction.getProcessedTime() != null && transaction.getProcessedTime() > 0) ? DateUtil.toDateTimeFormatted(transaction.getProcessedTime(), timeZone) : "");
        body = body.replaceAll("==terminalName==", terminal != null && StringUtils.isNotBlank(terminal.getName()) ? terminal.getName() : "N/A");
        body = body.replaceAll("==inventoryName==", inventory != null && StringUtils.isNotBlank(inventory.getName()) ? inventory.getName() : "N/A");

        //member  details
        body = body.replaceAll("==customerName==", member != null ? member.getFirstName() + "_" + member.getLastName() : "N/A");
        body = body.replaceAll("==deliveryAddress==", (transaction.getDeliveryAddress() != null) ? transaction.getDeliveryAddress().getAddressString() : (member != null && member.getAddress() != null) ? member.getAddress().getAddressString() : "N/A");
        body = body.replaceAll("==scheduledTime==", (transaction.getPickUpDate() != null && transaction.getPickUpDate() > 0) ? DateUtil.toDateTimeFormatted(transaction.getPickUpDate(), timeZone) : "");
        if (transaction.getStatus() == Transaction.TransactionStatus.Completed) {
            body = body.replaceAll("==delieverdTime==", (transaction.getCompletedTime() != null && transaction.getCompletedTime() > 0) ? DateUtil.toDateTimeFormatted(transaction.getCompletedTime(), timeZone) : "");
        } else {
            body = body.replaceAll("==delieverdTime==", transaction.getDeliveryDate() > 0 ? DateUtil.toDateTimeFormatted(transaction.getDeliveryDate(), timeZone) : "");
        }

       //company details.
        body = body.replaceAll("==companyName==", company.getName());
        body = body.replaceAll("==companyContactName==", company.getPrimaryContact() != null ? company.getPrimaryContact().getName() : "N/A");
        body = body.replaceAll("==companyLicense==", shop != null && StringUtils.isNotBlank(shop.getLicense()) ? shop.getLicense() : "N/A");
        body = body.replaceAll("==companyAddress==", company.getAddress() != null ? company.getAddress().getAddressString() : "N/A");
        body = body.replaceAll("==phoneNumber==", StringUtils.isNotBlank(company.getPhoneNumber()) ? company.getPhoneNumber() : "N/A");
        body = body.replaceAll("==website==", StringUtils.isNotBlank(company.getWebsite()) ? company.getWebsite() : "N/A");

        //driver  details
        body = body.replaceAll("==driverName==", employee != null ? (employee.getFirstName() + " " + employee.getLastName()) : "N/A");
        body = body.replaceAll("==licenseNumber==", ((employee != null) && (employee.getDriversLicense() != null)) ? employee.getDriversLicense() : "N/A");
        body = body.replaceAll("==vechialMake==", (employee != null && StringUtils.isNotBlank(employee.getVehicleMake())) ? employee.getVehicleMake() : "N/A");
        body = body.replaceAll("==vehicleModel==", (employee != null && StringUtils.isNotBlank(employee.getVehicleModel())) ? employee.getVehicleModel() : "N/A");
        body = body.replaceAll("==vehiclelicensePlate==", (employee != null && StringUtils.isNotBlank(employee.getVehicleLicensePlate())) ? employee.getVehicleLicensePlate() : "N/A");


        String logoURL = null;
        if (shop.getLogo() != null && shop.getLogo().getThumbURL() != null) {
            logoURL = shop.getLogo().getThumbURL();
        }
        if (logoURL == null) {
            // try using company
            logoURL = company.getLogoURL() != null ? company.getLogoURL() : "https://connect-assets.s3.amazonaws.com/email/logo.jpg";
        }
        body = body.replaceAll("==logo==", logoURL);
        String memberSignatureThumbURL = "";
        if (transaction.getMemberSignature() != null) {
            memberSignatureThumbURL = transaction.getMemberSignature().getThumbURL();
        }
        if (StringUtils.isNotBlank(memberSignatureThumbURL)) {
            body = body.replaceAll("==showMemberSignature==", "");
            body = body.replaceAll("==signTime==", transaction.getMemberSignature().getCreated() != null ? DateUtil.toDateTimeFormatted(transaction.getMemberSignature().getCreated(), timeZone) : "");
            body = body.replaceAll("==memberSignature==", StringEscapeUtils.escapeHtml4(memberSignatureThumbURL));
            body = body.replaceAll("==memberSignature==", StringEscapeUtils.escapeHtml4(memberSignatureThumbURL));
        } else {
            body = body.replaceAll("==showMemberSignature==", "display:none");
        }
        body = body.replaceAll("==departureTime==", (transaction.getStartTime() != null && transaction.getStartTime() > 0) ? DateUtil.toDateTimeFormatted(transaction.getStartTime(), timeZone) : "");
        StringBuilder productInfo = new StringBuilder();
        int productCount = 0;
        if (transaction != null && transaction.getCart() != null) {
            body = body.replace("==subTotal==", TextUtil.toCurrency(transaction.getCart().getSubTotal() != null ? transaction.getCart().getSubTotal().doubleValue() : 0d , shop.getDefaultCountry()));
            body = body.replace("==cartDiscount==", TextUtil.toCurrency(transaction.getCart().getCalcCartDiscount() != null ? transaction.getCart().getCalcCartDiscount().doubleValue() : 0d, shop.getDefaultCountry()));
            body = body.replace("==totalDiscount==", TextUtil.toCurrency(transaction.getCart().getTotalDiscount() != null ? transaction.getCart().getTotalDiscount().doubleValue() : 0d, shop.getDefaultCountry()));

           //Calculate Transaction Tax
            BigDecimal exciseTax = transaction.getCart().getTaxResult().getTotalALPostExciseTax().add(transaction.getCart().getTaxResult().getTotalExciseTax());
            BigDecimal cityTax = transaction.getCart().getTaxResult().getTotalCityTax() != null ? transaction.getCart().getTaxResult().getTotalCityTax() : BigDecimal.ZERO;
            BigDecimal stateTax = transaction.getCart().getTaxResult().getTotalStateTax() != null ? transaction.getCart().getTaxResult().getTotalStateTax() : BigDecimal.ZERO;
            BigDecimal countyTax = transaction.getCart().getTaxResult().getTotalCountyTax() != null ? transaction.getCart().getTaxResult().getTotalCountyTax() : BigDecimal.ZERO;
            BigDecimal federalTax = transaction.getCart().getTaxResult().getTotalFedTax() != null ? transaction.getCart().getTaxResult().getTotalFedTax() : BigDecimal.ZERO;
            BigDecimal excisePreTax = transaction.getCart().getTaxResult().getTotalNALPreExciseTax().add(transaction.getCart().getTaxResult().getTotalALExciseTax());
            BigDecimal cityPreTax = transaction.getCart().getTaxResult().getTotalCityPreTax();
            BigDecimal statePreTax = transaction.getCart().getTaxResult().getTotalStatePreTax();
            BigDecimal countyPreTax = transaction.getCart().getTaxResult().getTotalCountyPreTax();
            BigDecimal federalPreTax = transaction.getCart().getTaxResult().getTotalFedPreTax();

            //Show Tax Details
            body = (exciseTax.compareTo(BigDecimal.ZERO) > 0) ? (body.replace("==exciseTax==", TextUtil.toCurrency(exciseTax.doubleValue(), shop.getDefaultCountry()))) : (body.replaceAll("==showExciseTax==", "display:none"));
            body = (cityTax.compareTo(BigDecimal.ZERO) > 0) ? (body.replace("==cityTax==", TextUtil.toCurrency(cityTax.doubleValue(), shop.getDefaultCountry()))) : (body.replaceAll("==showCityTax==", "display:none"));
            body = (stateTax.compareTo(BigDecimal.ZERO) > 0) ? (body.replace("==stateTax==", TextUtil.toCurrency(stateTax.doubleValue(), shop.getDefaultCountry()))) : (body.replaceAll("==showStateTax==", "display:none"));
            body = (countyTax.compareTo(BigDecimal.ZERO) > 0) ? (body.replace("==countyTax==", TextUtil.toCurrency(countyTax.doubleValue(), shop.getDefaultCountry()))) : (body.replaceAll("==showCountyTax==", "display:none"));
            body = (federalTax.compareTo(BigDecimal.ZERO) > 0) ? (body.replace("==federalTax==", TextUtil.toCurrency(federalTax.doubleValue(), shop.getDefaultCountry()))) : (body.replaceAll("==showFederalTax==", "display:none"));
            body = (excisePreTax.compareTo(BigDecimal.ZERO) > 0) ? (body.replace("==excisePreTax==", TextUtil.toCurrency(excisePreTax.doubleValue(), shop.getDefaultCountry()))) : (body.replaceAll("==showExcisePreTax==", "display:none"));
            body = (cityPreTax.compareTo(BigDecimal.ZERO) > 0) ? (body.replace("==cityPreTax==", TextUtil.toCurrency(cityPreTax.doubleValue(), shop.getDefaultCountry()))) : (body.replaceAll("==showCityPreTax==", "display:none"));
            body = (statePreTax.compareTo(BigDecimal.ZERO) > 0) ? (body.replace("==statePreTax==", TextUtil.toCurrency(statePreTax.doubleValue(), shop.getDefaultCountry()))) : (body.replaceAll("==showStatePreTax==", "display:none"));
            body = (countyPreTax.compareTo(BigDecimal.ZERO) > 0) ? (body.replace("==countyPreTax==", TextUtil.toCurrency(countyPreTax.doubleValue(), shop.getDefaultCountry()))) : (body.replaceAll("==showCountyPreTax==", "display:none"));
            body = (federalPreTax.compareTo(BigDecimal.ZERO) > 0) ? (body.replace("==federalPreTax==", TextUtil.toCurrency(federalPreTax.doubleValue(), shop.getDefaultCountry()))) : (body.replaceAll("==showFederalPreTax==", "display:none"));


            body = (transaction.getCart().getTaxResult().getTotalPostCalcTax() != null && transaction.getCart().getTaxResult().getTotalPostCalcTax().compareTo(BigDecimal.ZERO) > 0) ? (body.replace("==totalTax==", TextUtil.toCurrency(transaction.getCart().getTaxResult().getTotalPostCalcTax().doubleValue(), shop.getDefaultCountry()))) : (body.replaceAll("==showTotalTax==", "display:none"));
            body = (transaction.getCart().getTaxResult().getTotalPreCalcTax() != null && transaction.getCart().getTaxResult().getTotalPreCalcTax().compareTo(BigDecimal.ZERO) > 0) ? (body.replace("==totalPreTax==", TextUtil.toCurrency(transaction.getCart().getTaxResult().getTotalPreCalcTax().doubleValue(), shop.getDefaultCountry()))) : (body.replaceAll("==showTotalPreTax==", "display:none"));
            body = (transaction.getCart().getCreditCardFee() != null && transaction.getCart().getCreditCardFee().compareTo(BigDecimal.ZERO) > 0 && transaction.getTransType() != Transaction.TransactionType.Refund) ? (body.replace("==creditCardFee==", TextUtil.toCurrency(transaction.getCart().getCreditCardFee().doubleValue(), shop.getDefaultCountry()))) : (body.replaceAll("==showCreditCardFee==", "display:none"));
            body = (transaction.getCart().getDeliveryFee() != null && transaction.getCart().getDeliveryFee().compareTo(BigDecimal.ZERO) > 0 && transaction.getTransType() != Transaction.TransactionType.Refund) ? (body.replace("==deliveryFee==", TextUtil.toCurrency(transaction.getCart().getDeliveryFee().doubleValue(), shop.getDefaultCountry()))) : (body.replaceAll("==showDeliveryFee==", "display:none"));
            body = (transaction.getCart().getTipAmount() != null && transaction.getCart().getTipAmount().compareTo(BigDecimal.ZERO) > 0) ? (body.replace("==tipAmount==", TextUtil.toCurrency(transaction.getCart().getTipAmount().doubleValue(), shop.getDefaultCountry()))) : (body.replaceAll("==showTipAmount==", "display:none"));
            body = body.replace("==total==", TextUtil.toCurrency(transaction.getCart().getTotal() != null ? transaction.getCart().getTotal().doubleValue() : 0d, shop.getDefaultCountry()));
        }

        if (transaction.getCart().getItems() != null) {
            body = body.replaceAll("==showproduct==", "");
            for (OrderItem item : transaction.getCart().getItems()) {
                Product product = productHashMap.get(item.getProductId());
                if (product == null) {
                    continue;
                }
                Brand brand = brandHashMap.get(product.getBrandId());
                StringBuilder productName = new StringBuilder(product.getName());
                String brandName = (brand != null) ? brand.getName() : "N/A";
                String cannabisType = product.getCannabisType().name();
                String uidTag = product != null && StringUtils.isNotBlank(product.getSku()) ? product.getSku() : "N/A";

                String discountNotes = StringUtils.isNotBlank(item.getDiscountNotes()) ? item.getDiscountNotes() : "N/A";

                productInfo.append("<tr><td class=\"content text-center\" style=\"width : 15%; word-wrap: break-word;\">" + productName + "</td>")
                        .append("<td class=\"content text-center\" style=\"width: 15%; word-wrap: break-word;\">" + brandName + "</td>")
                        .append("<td class=\"content text-center\" style=\"width: 10%\">" + uidTag + "</td>")
                        .append("<td class=\"content text-center\" style=\"width: 9%\">" + cannabisType + "</td>")
                        .append("<td class=\"bg-dark-grey content text-center\" style=\"width: 10%\">" + item.getQuantity().setScale(2, BigDecimal.ROUND_HALF_UP) + "</td>")
                        .append("<td class=\"content text-center\" style=\"width: 10%\">" + TextUtil.toCurrency(product.getUnitPrice().doubleValue(), shop.getDefaultCountry()) + "</td>")
                        .append("<td class=\"content text-center\" style=\"width: 10%\">" + TextUtil.toCurrency(item.getDiscount().doubleValue(), shop.getDefaultCountry()) + "</td>")
                        .append("<td class=\"content text-center\" style=\"width: 10%\">" + discountNotes + "</td>")
                        .append("<td class=\"content text-center\" style=\"width: 10%\">" + TextUtil.toCurrency(item.getFinalPrice().doubleValue(), shop.getDefaultCountry()) + "</td>")
                        .append("</tr>");
                productCount++;
            }
        } else {
            body = body.replaceAll("==showproduct==", "display:none");
        }
        body = body.replace("==productInformation==", productInfo.toString());
        body = body.replaceAll("&", "&amp;");

        if (transaction.getNote() != null && StringUtils.isNotBlank(transaction.getNote().getMessage())) {
            body = body.replaceAll("==notes==", transaction.getNote().getMessage());
        } else {
            body = body.replaceAll("==displayNotes==", "display:none");
        }
        body = body.replaceAll("==status==", transaction.getStatus() != null ? transaction.getStatus().toString() : "N/A");
        body = body.replaceAll("==paymentOption==", transaction.getCart() != null && StringUtils.isNotBlank(transaction.getCart().getPaymentOption().toString()) ? transaction.getCart().getPaymentOption().toString() : "N/A");
        StringBuilder promotionUsedInfo = new StringBuilder();
        if (!CollectionUtils.isEmpty(transaction.getCart().getPromotionReqLogs())) {
            LinkedHashSet<ObjectId> promoIds = new LinkedHashSet<>();
            for (PromotionReqLog promotion : transaction.getCart().getPromotionReqLogs()) {
                if (StringUtils.isNotBlank(promotion.getPromotionId()) && ObjectId.isValid(promotion.getPromotionId())) {
                    promoIds.add(new ObjectId(promotion.getPromotionId()));
                }
            }
            HashMap<String, Promotion> promotionReqLogHashMap = promotionRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(promoIds));
            body = body.replaceAll("==showPromotionUsed==", "");
            for (PromotionReqLog promotionReqLog : transaction.getCart().getPromotionReqLogs()) {
                Promotion promotion = promotionReqLogHashMap.get(promotionReqLog.getPromotionId());
                if (promotion == null) {
                    continue;
                }
                promotionUsedInfo.append("<tr><td class=\"content text-center\" style=\"width: 100%; word-wrap: break-word;\">" + Matcher.quoteReplacement(promotion.getName()+ " " + TextUtil.toCurrency(promotionReqLog.getAmount().doubleValue(), shop.getDefaultCountry()) + "</td> </tr>"));
            }
        } else {
            body = body.replaceAll("==showPromotionUsed==", "display:none");
        }
        body = body.replaceAll("==promotionUsed==", promotionUsedInfo.toString());
        StringBuilder orderTagInfo = new StringBuilder();
        if (!CollectionUtils.isEmpty(transaction.getOrderTags())) {
            body = body.replaceAll("==showOrderTag==", "");
            for (String order : transaction.getOrderTags()) {
                orderTagInfo.append("<tr><td class=\"content text-center\" style=\"width: 100%; word-wrap: break-word;\">" + order + "</td> </tr>");
            }
        } else {
            body = body.replaceAll("==showOrderTag==", "display:none");
        }
        body = body.replaceAll("==OrderTags==", orderTagInfo.toString());
        return body;
    }

    /**
     * send push notiofication
     * @param dbTrans
     * @param message
     * @param title
     */
    private void sendNotification(Transaction dbTrans, String message, String title, FcmPayload.SubType subType) {
        Employee sellerEmployee = employeeRepository.get(token.getCompanyId(), dbTrans.getSellerId());
        Terminal terminal = terminalRepository.get(token.getCompanyId(), sellerEmployee.getAssignedTerminalId());

        if (terminal != null) {
            pushNotificationManager.sendPushNotification(String.format(message), Lists.newArrayList(terminal), 0, FcmPayload.Type.TASK, dbTrans.getId(), dbTrans.getTransNo(), title, subType);
        }

    }

    /**
     * Private method to update corresponding consumer cart
     *
     * @param shop          : shop
     * @param dbTransaction : dbTransaction
     * @param transaction   : transaction
     * @param member        : member
     */
    private void updateCorrespondingConsumerCart(Shop shop, Transaction dbTransaction, Transaction transaction, Member member) {
        String consumerUserId = null;
        boolean membershipStatus = false;
        if (StringUtils.isNotBlank(dbTransaction.getConsumerCartId())) {
            ConsumerCart consumerCart = consumerCartRepository.get(token.getCompanyId(), dbTransaction.getConsumerCartId());
            if (dbTransaction.getCart() != null) {
                consumerCart.setCart(dbTransaction.getCart());
            }
            consumerCart.setCartStatus(ConsumerCart.ConsumerCartStatus.Completed);
            consumerCart.setCompleted(true);
            consumerCart.setCompletedTime(DateTime.now().getMillis());
            consumerCart.setTrackingStatus(ConsumerCart.ConsumerTrackingStatus.Delivered);
            consumerCartRepository.update(token.getCompanyId(), consumerCart.getId(), consumerCart);
            realtimeService.sendRealTimeEvent(token.getShopId(),
                    RealtimeService.RealtimeEventType.ConsumerCartsUpdate, null);

            ConsumerUser consumerUser = consumerUserRepository.getById(consumerCart.getConsumerId());
            //if (ConsumerCart.TransactionSource.WooCommerce != consumerCart.getTransactionSource()) {
            if (consumerUser != null) {
                consumerUserId = consumerCart.getConsumerId();
                membershipStatus = consumerUser.isAccepted();
            }
            consumerNotificationService.sendUpdateOrderdNotification(consumerCart, consumerUser, shop, NotificationInfo.NotificationType.Consumer_Update_Order);
            //}
            updateConsumerOrderWebHook(consumerCart);
        }

        final TransactionData transactionData = new TransactionData();
        transactionData.setId(transaction.getId());
        transactionData.setShopId(transaction.getShopId());
        transactionData.setTerminalId(transaction.getSellerTerminalId());
        transactionData.setCreated(transaction.getCreated());
        transactionData.setTransactionNo(transaction.getTransNo());
        if (member != null) {
            StringBuilder sb = new StringBuilder();
            transactionData.setMemberName(sb.append(member.getFirstName()).append(" ").append(member.getLastName()).toString());
            transactionData.setMemberId(member.getId());
        }

        if (transaction.getConsumerCartId() != null && ObjectId.isValid(transaction.getConsumerCartId())) {
            transactionData.setConsumerCartId(transaction.getConsumerCartId());
            transactionData.setConsumerUserId(consumerUserId);
        }

        if (transaction.getProcessedTime() == null) {
            transactionData.setProcessedTime(dbTransaction.getProcessedTime());
        } else {
            transactionData.setProcessedTime(transaction.getProcessedTime());
        }

        transactionData.setTransactionType(transaction.getTransType());
        transactionData.setQueueType(transaction.getQueueType());
        transactionData.setFulfillmentStep(transaction.getFulfillmentStep());
        if (StringUtils.isNotBlank(transaction.getSellerId())) {
            Employee employee = employeeRepository.get(token.getCompanyId(), transaction.getSellerId());
            if (employee != null) {
                StringBuilder sb = new StringBuilder();
                transactionData.setSellerName(sb.append(employee.getFirstName()).append(" ").append(employee.getLastName()).toString());
            }
        }
        transactionData.setPaymentOption(transaction.getCart().getPaymentOption());
        transactionData.setPaymentType(transaction.getCart().getPaymentType());
        transactionData.setDiscount(transaction.getCart().getDiscount() != null ? transaction.getCart().getDiscount() : BigDecimal.ZERO);
        transactionData.setTotalTax(transaction.getCart().getTotalCalcTax() != null ? transaction.getCart().getTotalCalcTax() : BigDecimal.ZERO);
        transactionData.setTotal(transaction.getCart().getTotal() != null ? transaction.getCart().getTotal() : BigDecimal.ZERO);
        transactionData.setPromoCode(transaction.getCart().getPromoCode() != null ? transaction.getCart().getPromoCode() : "");
        transactionData.setLoyaltyPoints(member != null ? member.getLoyaltyPoints() : BigDecimal.ZERO);
        transactionData.setMembershipAccepted(membershipStatus);

        partnerWebHookManager.completeTransactionWebHook(token.getCompanyId(), token.getShopId(), transactionData);
    }

    /**
     * Private method to update current cash drawer
     *
     * @param shop       : shop
     * @param terminalId : terminalId
     */
    void processCurrentCashDrawer(final Shop shop, final String terminalId) {
        CashDrawerSession dbLogResult = cashDrawerSessionRepository.getLatestDrawerForTerminal(shop.getCompanyId(), shop.getId(), terminalId);

        if (dbLogResult != null && dbLogResult.getStatus() != CashDrawerSession.CashDrawerLogStatus.Closed) {
            cashDrawerProcessorService.processCurrentCashDrawer(shop, dbLogResult, false);

            cashDrawerSessionRepository.update(shop.getCompanyId(), dbLogResult.getId(), dbLogResult);
            realtimeService.sendRealTimeEvent(shop.getId(), RealtimeService.RealtimeEventType.CashDrawerUpdateEvent, null);
        }

    }

    /**
     * Private method to check if drawer available for current day
     *
     * @param shop             : shop
     * @param sellerTerminalId : sellerTerminalId
     * @param sellerTerminal   : sellerTerminal
     */
    private void checkCashDrawerAvailability(Shop shop, String sellerTerminalId, Terminal sellerTerminal) {
        if (shop != null) {
            // make sure the current terminal has a cash drawer
            if (shop.isEnforceCashDrawers()) {
                long nowMillis = DateTime.now().getMillis();
                String todayDate = DateUtil.toDateFormatted(nowMillis, shop.getTimeZone());
                String todayFormatted = DateUtil.toDateFormatted(nowMillis, shop.getTimeZone(), "yyyyMMdd");

                CashDrawerSession dbLogResult = cashDrawerSessionRepository.getCashDrawerForDate(token.getCompanyId(),
                        shop.getId(), sellerTerminalId, todayFormatted);

                if (dbLogResult == null) {
                    throw new BlazeInvalidArgException("Terminal",
                            String.format("Please create a cash drawer for today's date, %s, for terminal '%s' before completing your sale.",
                                    todayDate,
                                    sellerTerminal.getName()));
                } else if (dbLogResult.getStatus() == CashDrawerSession.CashDrawerLogStatus.Closed) {

                    throw new BlazeInvalidArgException("Terminal",
                            String.format("Please open a cash drawer for today's date, %s, for terminal '%s' before completing your sale.",
                                    todayDate,
                                    sellerTerminal.getName()));
                }
            }


        }
    }
}