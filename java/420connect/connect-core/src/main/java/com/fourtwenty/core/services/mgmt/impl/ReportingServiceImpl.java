package com.fourtwenty.core.services.mgmt.impl;

import com.amazonaws.util.CollectionUtils;
import com.fourtwenty.core.domain.models.company.CashDrawerSession;
import com.fourtwenty.core.domain.models.company.ConsumerType;
import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.company.ReportTrack;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.company.Terminal;
import com.fourtwenty.core.domain.models.global.ReportingInfo;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.TaxResult;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.CashDrawerSessionRepository;
import com.fourtwenty.core.domain.repositories.dispensary.EmployeeRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ReportTrackRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TerminalRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.domain.repositories.global.ReportingInfoRepository;
import com.fourtwenty.core.event.BlazeEventBus;
import com.fourtwenty.core.event.report.EmailReportEvent;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.managed.ReportManager;
import com.fourtwenty.core.reporting.ReportType;
import com.fourtwenty.core.reporting.gather.impl.GathererManager;
import com.fourtwenty.core.reporting.model.GathererReport;
import com.fourtwenty.core.reporting.model.Report;
import com.fourtwenty.core.reporting.model.ReportFilter;
import com.fourtwenty.core.reporting.processing.FormatProcessor;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.mgmt.CompanyService;
import com.fourtwenty.core.services.mgmt.ReportingService;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.core.util.TextUtil;
import com.google.inject.Provider;
import org.apache.commons.lang3.StringUtils;
import org.assertj.core.util.Lists;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.*;

import static com.fourtwenty.core.util.ReceiptUtil.getLineItem;
import static com.fourtwenty.core.util.ReceiptUtil.repeatContent;

/**
 * Created by Stephen Schmidt on 3/29/2016.
 */
public class ReportingServiceImpl extends AbstractAuthServiceImpl implements ReportingService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ReportingServiceImpl.class);
    private static final String GENERATE_REPORT = "Your report is being generated and will be sent to your mail address.";

    @Inject
    GathererManager gathererManager;

    @Inject
    FormatProcessor formatProcessor;

    @Inject
    private ReportTrackRepository reportTrackRepository;
    @Inject
    private CompanyService companyService;
    @Inject
    private TerminalRepository terminalRepository;
    @Inject
    private CashDrawerSessionRepository cashDrawerRepository;
    @Inject
    private ShopRepository shopRepository;
    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private EmployeeRepository employeeRepository;
    @Inject
    private BlazeEventBus blazeEventBus;
    @Inject
    private ReportingInfoRepository reportingInfoRepository;
    @Inject
    private ReportManager reportManager;

    @Inject
    public ReportingServiceImpl(Provider<ConnectAuthToken> token) {
        super(token);
    }

    @Override
    public Report getReport(String typeString, String startDate, String endDate, String filterString, String formatString, ReportTrack.ReportSectionType section, String email, boolean generate) {
        //Get the Enum type for this specific report
        ReportType type = ReportType.getType(typeString);
        if (type == ReportType.DEFAULT) {
            throw new BlazeInvalidArgException("ReportService", "Invalid Argument - Report Type");
        }
        FormatProcessor.ReportFormat format = FormatProcessor.ReportFormat.getType(formatString);
        if (format == FormatProcessor.ReportFormat.DEFAULT) {
            throw new BlazeInvalidArgException("ReportService", "Invalid Argument - Report Format");
        }

        Shop shop = shopRepository.get(token.getCompanyId(),token.getShopId());

        //Build the filter object to be used to sort the data we want
        ReportFilter filter = new ReportFilter(shop,type, token, filterString, startDate, endDate);
        //Commenting as we are using it streamline
        /*int months = DateUtil.getMonthsBetweenTwoDates(filter.getTimeZoneStartDateMillis(), filter.getTimeZoneEndDateMillis());

        boolean reportCacheAvailable = this.sendReport(filter, email, filter.getTimezoneOffset(), months);

        if (reportCacheAvailable) {
            throw new BlazeInvalidArgException("Report", "Report has been sent on email");
        }

        DateTime currentTimeInPDT = DateUtil.nowWithTimeZone(null);
        int hrOfDay = currentTimeInPDT.getHourOfDay();
        if (hrOfDay > 9 && hrOfDay < 22) {
            if (months > 3) {
                throw new BlazeInvalidArgException("ReportDuration", "Report duration cannot be more than 3 months period.");
            }
        } else {
            if (months > 6) {
                throw new BlazeInvalidArgException("ReportDuration", "Report duration cannot be more than 6 months period.");
            }
        }*/

        if (generate || filter.shouldGenerate()) {
            reportManager.generateReport(type, filter);
            throw new BlazeInvalidArgException("Report", GENERATE_REPORT);
        }
        GathererReport report = reportManager.getReport(type, filter);
        if (report == null) {
            throw new BlazeInvalidArgException("Report", "Report not available.");
        }

        if (!report.getData().isEmpty() && report.getData().size() > ReportFilter.COUNT) {
            reportManager.generateReport(filter, report, type);
            throw new BlazeInvalidArgException("Report", GENERATE_REPORT);
        } else {
            Report reportResult = formatProcessor.process(format, report);
            if (reportResult != null && ReportTrack.ReportSectionType.REPORT.equals(section)) {
                this.addOrUpdateTrackCount(type);
            }
            return reportResult;
        }
    }

    private boolean sendReport(ReportFilter filter, String email, int timezoneOffset, int months) {
        boolean reportCacheAvailable = Boolean.FALSE;

        if (months > 3 && StringUtils.isNotBlank(email)) {
            ReportType infoType = null;
            for (ReportingInfo info : reportingInfoRepository.list()) {
                if (info.getActive() && (info.getReportingInfoType().toString().equalsIgnoreCase(filter.getType().toString())
                        || (!CollectionUtils.isNullOrEmpty(info.getSubReportTypes()) && info.getSubReportTypes().contains(filter.getType())))) {
                    infoType = filter.getType();
                    reportCacheAvailable = Boolean.TRUE;
                    break;
                }
            }

            if (reportCacheAvailable) {
                EmailReportEvent event = new EmailReportEvent();
                event.setEmail(email);
                event.setCompanyId(filter.getCompanyId());
                event.setShopId(filter.getShopId());
                event.setStartDate(filter.getTimeZoneStartDateMillis());
                event.setEndDate(filter.getTimeZoneEndDateMillis());
                event.setTimezoneOffset(timezoneOffset);
                event.setReportingInfoType(infoType);
                blazeEventBus.post(event);
            }
        }

        return reportCacheAvailable;
    }

    /**
     * This method add or update track count for particular report
     *
     * @param type : report type
     */
    @Override
    public ReportTrack addOrUpdateTrackCount(ReportType type) {
        ReportTrack reportTrack = reportTrackRepository.getReportTrackByType(token.getCompanyId(), token.getShopId(), type);

        if (reportTrack == null) {
            reportTrack = new ReportTrack();

            reportTrack.prepare(token.getCompanyId());
            reportTrack.setShopId(token.getShopId());
            reportTrack.setCount(0);
        }

        reportTrack.setReportType(type);
        reportTrack.setCount(reportTrack.getCount() + 1);

        return reportTrackRepository.upsert(reportTrack.getId(), reportTrack);

    }

    @Override
    public List<ReportTrack> getFrequentlyUsedReport() {
        List<ReportType> dashboardNames =  Lists.newArrayList(ReportType.DAILY_TOTAL_SALES_DASHBOARD, ReportType.DAILY_NEW_MEMBERS_DASHBOARD, ReportType.DAILY_TOTAL_VISITS_DASHBOARD, ReportType.DAILY_BEST_SELLING_PRODUCT_DASHBOARD, ReportType.SALES_BY_BRACKET_DASHBOARD, ReportType.RECENT_TRANSACTIONS_DASHBOARD, ReportType.CLOCKED_IN_EMPLOYEES_DASHBOARD, ReportType.SALES_BY_CANNABIS_TYPE_DASHBOARD, ReportType.OVERVIEW_DASHBOARD, ReportType.AVERAGE_ORDER_DASHBOARD, ReportType.AVERAGE_DISCOUNT_DASHBOARD, ReportType.DELIVERY_FEE_DASHBOARD, ReportType.CREDIT_CARD_FEE_DASHBOARD, ReportType.CATEGORY_SALES_DASHBOARD, ReportType.EMPLOYEE_SALES_DASHBOARD, ReportType.BRAND_SALES_DASHBOARD, ReportType.EXCISE_TAX_DASHBOARD, ReportType.CITY_TAX_DASHBOARD, ReportType.STATE_TAX_DASHBOARD, ReportType.COUNTY_TAX_DASHBOARD, ReportType.GENDER_SALES_DASHBOARD, ReportType.BEST_SELLING_PRODUCTS_DASHBOARD, ReportType.NEW_MEMBERS_BY_TYPE_DASHBOARD, ReportType.SALES_BY_PAYMENT_TYPE_DASHBOARD, ReportType.SALES_BY_AGE_GROUP_DASHBOARD, ReportType.MARKETING_SOURCE_DASHBOARD, ReportType.WEEKLY_PERFORMANCE_DASHBOARD, ReportType.EMPLOYEE_SALES_BY_CATEGORY_DASHBOARD, ReportType.LOW_INVENTORY_DASHBOARD, ReportType.DAILY_SUMMARY_DASHBOARD, ReportType.HOURLY_PERFORMANCE_DASHBOARD, ReportType.COMPANY_SALES_BY_AGE_DASHBOARD, ReportType.COMPANY_SALES_BY_PAYMENT_DASHBOARD, ReportType.COMPANY_SALES_BY_BRAND_DASHBOARD, ReportType.COMPANY_SALES_BY_HOUR_DASHBOARD, ReportType.COMPANY_TOTAL_SALES_DASHBOARD, ReportType.COMPANY_SALES_BY_LOCATION_DASHBOARD);
        int limit = 10;
        String sortOptions = "{count : -1}";
        return reportTrackRepository.getFrequentlyUsedReport(token.getCompanyId(), token.getShopId(), limit, sortOptions, dashboardNames);
    }

    @Override
    public String getDailySummaryReport(String terminalId, String date, int width) {
        try {
            int length;
            if (width == 39 || width == 33) {
                length = width;
            } else {
                length = 33;
            }
            DailySummary dailySummary = new DailySummary();

            DateTime cdDate = null;
            try {
                cdDate = DateUtil.parseDateKey(date);
            } catch (Exception e) {
                return "Request date should be 'yyyyMMdd";
            }

            Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());
            if (shop == null) {
                return "Shop is not found.";
            }

            Terminal terminal = terminalRepository.getTerminalById(token.getCompanyId(), terminalId);
            if (terminal == null) {
                return "Terminal is not available";
            }

            dailySummary.company = shop.getName();
            dailySummary.terminal = terminal.getName();

            String timeZone = shop.getTimeZone();
            int timeZoneOffset = DateUtil.getTimezoneOffsetInMinutes(timeZone);

            long timeZoneStartDateMillis = cdDate.withTimeAtStartOfDay().minusMinutes(timeZoneOffset).getMillis();
            DateTime jodaEndDate = cdDate.withTimeAtStartOfDay().plusDays(1).minusSeconds(1);
            long timeZoneEndDateMillis = jodaEndDate.minusMinutes(timeZoneOffset).getMillis();


            String day = DateUtil.toDateFormatted(cdDate.getMillis());
            dailySummary.date = DateUtil.toDateFormatted(DateTime.now().getMillis(), token.getRequestTimeZone());
            dailySummary.time = DateUtil.toTimeFormatted(DateTime.now().getMillis(), token.getRequestTimeZone());

            CashDrawerSession session = cashDrawerRepository.getCashDrawerForDate(token.getCompanyId(),
                    token.getShopId(),
                    token.getTerminalId(),
                    date);


            if (session != null) {
                dailySummary.startingCash += session.getStartCash().doubleValue();
                dailySummary.cashSales += session.getCashSales().doubleValue();
                dailySummary.checkSales += session.getCheckSales().doubleValue();
                dailySummary.debitSales += session.getCreditSales().doubleValue();
                dailySummary.paidIn += session.getTotalCashIn().doubleValue();
                dailySummary.paidOut += session.getTotalCashOut().doubleValue();
                dailySummary.cashDrops += session.getTotalCashDrop().doubleValue();
                dailySummary.expectedCash += session.getExpectedCash().doubleValue();
                dailySummary.actualCash += session.getActualCash().doubleValue();
                dailySummary.varianceCash = dailySummary.expectedCash - dailySummary.actualCash;
                dailySummary.cashRefunds += session.getCashRefunds().doubleValue();
                dailySummary.creditRefunds += session.getCreditRefunds().doubleValue();
                dailySummary.storeCreditRefunds += session.getStoreCreditRefunds().doubleValue();
                dailySummary.checkRefunds += session.getCheckRefunds().doubleValue();
                dailySummary.customRefundTotal = session.getCustomRefundTotals();
                dailySummary.customSalesTotal = session.getCustomSalesTotals();
                dailySummary.totalCashlessAtmSales += session.getTotalCashlessAtmSales().doubleValue();
                dailySummary.totalCashlessAtmRefunds += session.getTotalCashlessAtmRefunds().doubleValue();

            }

            Iterable<Transaction> bracketSales = transactionRepository.getBracketSalesForTerminalId(token.getCompanyId(), token.getShopId(), terminalId, timeZoneStartDateMillis, timeZoneEndDateMillis);
            HashMap<String, Employee> employeeMap = employeeRepository.listAllAsMap(token.getCompanyId());

            HashSet<String> employeeSalesKey = new HashSet<>();

            double sale = 0.0;
            double salesMapValue = 0.0;

            if (bracketSales != null) {
                for (Transaction transaction : bracketSales) {
                    Cart cart = transaction.getCart();

                    if (cart != null) {
                        Employee employee = employeeMap.get(transaction.getSellerId());

                        sale = cart.getSubTotal().doubleValue();

                        if (dailySummary.empMap.containsKey(employee.getId())) {
                            salesMapValue = dailySummary.empMap.get(employee.getId()) + sale;
                            dailySummary.empMap.put(employee.getId(), salesMapValue);
                        } else {
                            dailySummary.empMap.put(employee.getId(), sale);
                            employeeSalesKey.add(employee.getId());
                        }

                        TaxResult taxResult = cart.getTaxResult();

                        dailySummary.totalSales += cart.getTotal().doubleValue();
                        dailySummary.cityTax += taxResult.getTotalCityTax().doubleValue();
                        dailySummary.countyTax += taxResult.getTotalCountyTax().doubleValue();
                        dailySummary.stateTax += taxResult.getTotalStateTax().doubleValue();
                        dailySummary.federalTax += taxResult.getTotalFedTax().doubleValue();
                        dailySummary.alExciseTax += taxResult.getTotalALPostExciseTax().doubleValue();
                        dailySummary.nalExciseTax += taxResult.getTotalExciseTax().doubleValue();
                        dailySummary.totalTaxes += taxResult.getTotalPostCalcTax().doubleValue();

                        ConsumerType consumerType = cart.getFinalConsumerTye();

                        if (consumerType.equals(ConsumerType.AdultUse)) {
                            dailySummary.recreationalSales += sale;
                        } else {
                            dailySummary.medicalSales += sale;
                        }
                    }
                }
                dailySummary.totalTaxes = dailySummary.cityTax + dailySummary.countyTax + dailySummary.stateTax;
            }
            TextUtil.toEscapeCurrency(dailySummary.totalSales, shop.getDefaultCountry());

            String companyName = getLineItem("Shop", dailySummary.company, length);
            String receiptDay = getLineItem("Receipt Day", day, length);
            String dateData = getLineItem("Current Time", dailySummary.date, length);
            String timeData = getLineItem("", dailySummary.time, length);
            String terminalData = getLineItem("Terminal", dailySummary.terminal, length);
            String totalSale = getLineItem("Total Sales", TextUtil.toCurrency(dailySummary.totalSales, shop.getDefaultCountry()), length);
            String totalCash = getLineItem("Cash Sales", TextUtil.toCurrency(dailySummary.cashSales, shop.getDefaultCountry()), length);
            String totalDebit = getLineItem("Credit/Debit Sales", TextUtil.toCurrency(dailySummary.debitSales, shop.getDefaultCountry()), length);
            String totalCheck = getLineItem("Check Sales", TextUtil.toCurrency(dailySummary.checkSales, shop.getDefaultCountry()), length);
            String recreational = getLineItem("Recreational Sales", TextUtil.toCurrency(dailySummary.recreationalSales, shop.getDefaultCountry()), length);
            String medicinal = getLineItem("Medical Sales", TextUtil.toCurrency(dailySummary.medicalSales, shop.getDefaultCountry()), length);
            StringBuilder employeeSales = new StringBuilder();
            double exciseTx = dailySummary.alExciseTax + dailySummary.nalExciseTax;
            String exciseTax = getLineItem("Excise Tax", TextUtil.toCurrency(exciseTx, shop.getDefaultCountry()), length);
//           Commented as both are merger in excise tax.
//           String alExciseTax = getLineItem("AL Excise Tax", TextUtil.toCurrency(dailySummary.alExciseTax, shop.getDefaultCountry()), length);
//           String nalExciseTax = getLineItem("NAL Excise Tax", TextUtil.toCurrency(dailySummary.nalExciseTax, shop.getDefaultCountry()), length);
            String cityTax = getLineItem("City Tax", TextUtil.toCurrency(dailySummary.cityTax, shop.getDefaultCountry()), length);
            String countyTax = getLineItem("County Tax", TextUtil.toCurrency(dailySummary.countyTax, shop.getDefaultCountry()), length);
            String stateTax = getLineItem("State Tax", TextUtil.toCurrency(dailySummary.stateTax, shop.getDefaultCountry()), length);
            String fedTax = getLineItem("Federal Tax", TextUtil.toCurrency(dailySummary.federalTax, shop.getDefaultCountry()), length);
            String totalTaxes = getLineItem("Total Tax", TextUtil.toCurrency(dailySummary.totalTaxes, shop.getDefaultCountry()), length);
            String startingCash = getLineItem("Starting Cash", TextUtil.toCurrency(dailySummary.startingCash, shop.getDefaultCountry()), length);
            String cashDrops = getLineItem("Cash Drops", TextUtil.toCurrency(dailySummary.cashDrops, shop.getDefaultCountry()), length);
            String paidOut = getLineItem("Paid Out", TextUtil.toCurrency(dailySummary.paidOut, shop.getDefaultCountry()), length);
            String paidIn = getLineItem("Paid In", TextUtil.toCurrency(dailySummary.paidIn, shop.getDefaultCountry()), length);
            String expectedCash = getLineItem("Expected Cash", TextUtil.toCurrency(dailySummary.expectedCash, shop.getDefaultCountry()), length);
            String actualCash = getLineItem("Actual Cash", TextUtil.toCurrency(dailySummary.actualCash, shop.getDefaultCountry()), length);
            String varianceCash = getLineItem("Variance Cash", TextUtil.toCurrency(dailySummary.varianceCash, shop.getDefaultCountry()), length);
            String cashRefunds = getLineItem("Cash Refunds", TextUtil.toCurrency(dailySummary.cashRefunds, shop.getDefaultCountry()), length);
            String creditRefunds = getLineItem("Credit Refunds", TextUtil.toCurrency(dailySummary.creditRefunds, shop.getDefaultCountry()), length);
            String checkRefunds = getLineItem("Check Refunds", TextUtil.toCurrency(dailySummary.checkRefunds, shop.getDefaultCountry()), length);
            String storeCreditRefunds = getLineItem("Store Credit Refunds", TextUtil.toCurrency(dailySummary.storeCreditRefunds, shop.getDefaultCountry()), length);
            String totalCashlessAtmRefundStr = getLineItem("Cashless Atm Sales", TextUtil.toCurrency(dailySummary.totalCashlessAtmSales, shop.getDefaultCountry()), length);
            String totalCashlessAtmSalesStr = getLineItem("Cashless Atm Refunds", TextUtil.toCurrency(dailySummary.totalCashlessAtmRefunds, shop.getDefaultCountry()), length);

            for (String key : employeeSalesKey) {
                Employee employee = employeeMap.get(key);
                Double salesValue = dailySummary.empMap.get(key);
                String empData = String.format("Sales by %s %s", employee.getFirstName(), employee.getLastName()); //common.concat(employee.getFirstName()).concat(" ").concat(employee.getLastName());
                employeeSales.append(getLineItem(empData, TextUtil.toCurrency(salesValue, shop.getDefaultCountry()), length)).append("\n");
            }

            StringBuilder customSalesStr = new StringBuilder();
            if (dailySummary.customSalesTotal != null) {
                for (Map.Entry<Cart.PaymentOption, BigDecimal> entry : dailySummary.customSalesTotal.entrySet()) {
                    customSalesStr.append(getLineItem(String.format("%s Sales", entry.getKey()), TextUtil.toCurrency(entry.getValue().doubleValue(), shop.getDefaultCountry()), length));
                    customSalesStr.append("\n");
                }
            }

            StringBuilder customRefundStr = new StringBuilder();
            if (dailySummary.customRefundTotal != null) {
                for (Map.Entry<Cart.PaymentOption, BigDecimal> entry : dailySummary.customRefundTotal.entrySet()) {
                    customRefundStr.append(getLineItem(String.format("%s Refunds", entry.getKey()), TextUtil.toCurrency(entry.getValue().doubleValue(), shop.getDefaultCountry()), length));
                    customRefundStr.append("\n");
                }
            }

            String string = "";
            StringBuilder text = new StringBuilder();
            text.append(String.format("%s\n", companyName));
            text.append(String.format("%s\n", terminalData));
            text.append(String.format("%s\n", receiptDay));
            text.append(String.format("%s\n", dateData));
            text.append(String.format("%s\n", timeData));
            text.append(String.format("\n%s\n", totalSale));
            text.append(String.format("%s\n", totalCash));
            text.append(String.format("%s\n", totalDebit));
            text.append(String.format("%s\n", totalCheck));
            text.append(String.format("%s\n", totalCashlessAtmSalesStr));
            if (StringUtils.isNotBlank(customSalesStr)) {
                text.append(String.format("%s", customSalesStr));
            }
            text.append(String.format("\nRefunds\n"));
            text.append(String.format("%s\n", cashRefunds));
            text.append(String.format("%s\n", creditRefunds));
            text.append(String.format("%s\n", checkRefunds));
            text.append(String.format("%s\n", storeCreditRefunds));
            text.append(String.format("%s\n", totalCashlessAtmRefundStr));
            if (StringUtils.isNotBlank(customRefundStr)) {
                text.append(String.format("%s", customRefundStr));
            }
            text.append(String.format("\nSales Break Down\n"));
            text.append(String.format("%s\n", recreational));
            text.append(String.format("%s\n\n", medicinal));
            text.append(String.format("%s\n", employeeSales.toString()));
            text.append(String.format("%s\n", "Taxes"));

            if (dailySummary.exciseTax > 0) {
                text.append(String.format("%s\n", exciseTax));
            }
            if (dailySummary.cityTax > 0) {
                text.append(String.format("%s\n", cityTax));
            }

            if (dailySummary.countyTax > 0) {
                text.append(String.format("%s\n", countyTax));
            }

            if (dailySummary.stateTax > 0) {
                text.append(String.format("%s\n", stateTax));
            }
            if (dailySummary.federalTax > 0) {
                text.append(String.format("%s\n", fedTax));
            }
            text.append(String.format("%s\n", totalTaxes));
            text.append(String.format("\n%s\n", startingCash));
            text.append(String.format("%s\n", cashDrops));
            text.append(String.format("%s\n", paidOut));
            text.append(String.format("%s\n", paidIn));
            text.append(String.format("%s\n\n", expectedCash));
            text.append(String.format("%s\n", actualCash));
            text.append(String.format("%s\n", varianceCash));


            text.append("\n\n");
            boolean shouldAppendText = true;
            if (length < 32) {
                shouldAppendText = false;
            }
            text.append("\n").append("\n");
            if (shouldAppendText) {

                String line = repeatContent(length, "", "-", Boolean.TRUE);

                text.append(line);
                text.append("\n").append("\n");
            }

/*
            string = string.concat(companyName)+"\n"
                    .concat(receiptDay)+"\n"
                    .concat(dateData)+"\n"
                    .concat(terminalData)+"\n"+"\n"
                    .concat(totalSale)+"\n"
                    .concat(totalCash)+"\n"
                    .concat(totalDebit)+"\n"
                    .concat(totalCheck)+"\n"+"\n"
                    .concat("Sales Break Down")+"\n"
                    .concat(recreational)+"\n"
                    .concat(medicinal)+"\n"+"\n"
                    .concat(employeeSales)+"\n"
                    .concat("Total taxes")+"\n"
                    .concat(cityTax)+"\n"
                    .concat(countyTax)+"\n"
                    .concat(stateTax)+"\n"+"\n"
                    .concat(startingCash)+"\n"
                    .concat(cashDrops)+"\n"
                    .concat(paidOut)+"\n"
                    .concat(paidIn)+"\n"
                    .concat(expectedCash)+"\n"+"\n"
                    .concat(actualCash)+"\n"
                    .concat(varianceCash);*/

            return text.toString();
        } catch (Exception e) {
            LOGGER.error("Exception in Daily Summary Report", e);
            return "Information is not completed";
        }
    }


    private class DailySummary {
        String company;
        String date;
        String time;
        String terminal;
        double totalSales = 0.0;
        double cashSales = 0.0;
        double debitSales = 0.0;
        double checkSales = 0.0;
        double recreationalSales = 0.0;
        double medicalSales = 0.0;

        //<employeeId,memberSales>
        Map<String, Double> empMap = new HashMap<>();

        double alExciseTax = 0.0;
        double nalExciseTax = 0.0;
        double cityTax = 0.0;
        double stateTax = 0.0;
        double countyTax = 0.0;
        double federalTax = 0.0;
        double totalTaxes = 0.0;
        double startingCash = 0.0;
        double cashDrops = 0.0;
        double paidOut = 0.0;
        double paidIn = 0.0;
        double expectedCash = 0.0;
        double actualCash = 0.0;
        double varianceCash = 0.0;
        double cashRefunds = 0.0;
        double checkRefunds = 0.0;
        double creditRefunds = 0.0;
        double storeCreditRefunds = 0.0;
        HashMap<Cart.PaymentOption, BigDecimal> customSalesTotal = new HashMap<>();
        HashMap<Cart.PaymentOption, BigDecimal> customRefundTotal = new HashMap<>();
        double totalCashlessAtmSales = 0;
        double totalCashlessAtmRefunds = 0;
        double exciseTax = 0.0;

        public DailySummary() {

        }

    }
}
