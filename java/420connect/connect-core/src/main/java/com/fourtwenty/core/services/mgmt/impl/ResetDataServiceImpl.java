package com.fourtwenty.core.services.mgmt.impl;

import com.fourtwenty.core.domain.models.transaction.QueuedTransaction;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.rest.dispensary.requests.company.ResetDeleteRequest;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.common.BackgroundJobService;
import com.fourtwenty.core.services.common.RealtimeService;
import com.fourtwenty.core.services.mgmt.ResetDataService;
import com.fourtwenty.core.services.mgmt.SettingService;
import com.google.inject.Provider;
import org.bson.types.ObjectId;

import javax.inject.Inject;

public class ResetDataServiceImpl extends AbstractAuthServiceImpl implements ResetDataService {

    @Inject
    MemberRepository memberRepository;
    @Inject
    EmployeeRepository employeeRepository;
    @Inject
    SettingService settingService;
    @Inject
    ProductRepository productRepository;
    @Inject
    InventoryRepository inventoryRepository;
    @Inject
    VendorRepository vendorRepository;
    @Inject
    DoctorRepository doctorRepository;
    @Inject
    ProductPrepackageQuantityRepository productPrepackageQuantityRepository;
    @Inject
    BarcodeItemRepository barcodeItemRepository;
    @Inject
    RealtimeService realtimeService;
    @Inject
    private QueuedTransactionRepository queuedTransactionRepository;
    @Inject
    private BackgroundJobService backgroundJobService;

    @Inject
    public ResetDataServiceImpl(Provider<ConnectAuthToken> token) {
        super(token);
    }

    @Override
    public void deleteAllMembers(ResetDeleteRequest deleteRequest) {

        boolean hasPermission = settingService.checkForPermission(deleteRequest.getPassword());
        if (!hasPermission) {
            throw new BlazeInvalidArgException("Member", "Do not have permission.");
        }

        long count = memberRepository.count(token.getCompanyId());
        if (count == 0) {
            throw new BlazeInvalidArgException("Member", "No Members");
        }
        memberRepository.removeAllSetState(token.getCompanyId());

        realtimeService.sendRealTimeEventCompanyId(token.getCompanyId(), RealtimeService.RealtimeEventType.MembersUpdateEvent, null);
    }

    @Override
    public void deleteAllProducts(ResetDeleteRequest deleteRequest) {

        boolean hasPermission = settingService.checkForPermission(deleteRequest.getPassword());
        if (!hasPermission) {
            throw new BlazeInvalidArgException("Product", "Do not have permission.");
        }

        long count = productRepository.count(token.getCompanyId());
        if (count == 0) {
            throw new BlazeInvalidArgException("Product", "No Products");
        }
        productPrepackageQuantityRepository.removeAllSetState(token.getCompanyId(), token.getShopId());

        // delete products
        productRepository.removeAllSetState(token.getCompanyId(), token.getShopId());
        // remove barcodes (completely)
        barcodeItemRepository.deleteAllBarcodeItems(token.getCompanyId(), token.getShopId());

        realtimeService.sendRealTimeEvent(token.getShopId(), RealtimeService.RealtimeEventType.ProductsUpdateEvent, null);
    }

    @Override
    public void deleteAllProductStocks(ResetDeleteRequest deleteRequest) {
        boolean hasPermission = settingService.checkForPermission(deleteRequest.getPassword());
        if (!hasPermission) {
            throw new BlazeInvalidArgException("Product", "Do not have permission.");
        }

        long count = productRepository.count(token.getCompanyId(), token.getShopId());
        if (count == 0) {
            throw new BlazeInvalidArgException("Product", "No Products");
        }

        this.createDeleteAllProductQueuedJob();
    }

    private void createDeleteAllProductQueuedJob() {
        QueuedTransaction queuedTransaction = new QueuedTransaction();
        queuedTransaction.setShopId(token.getShopId());
        queuedTransaction.setCompanyId(token.getCompanyId());
        queuedTransaction.setId(ObjectId.get().toString());
        queuedTransaction.setPendingStatus(Transaction.TransactionStatus.InProgress);
        queuedTransaction.setStatus(QueuedTransaction.QueueStatus.Pending);
        queuedTransaction.setQueuedTransType(QueuedTransaction.QueuedTransactionType.StockReset);
        queuedTransaction.setSellerId(token.getActiveTopUser().getUserId());

        queuedTransactionRepository.save(queuedTransaction);

        backgroundJobService.addTransactionJob(token.getCompanyId(), token.getShopId(), queuedTransaction.getId());
    }

    @Override
    public void deleteAllVendors(ResetDeleteRequest deleteRequest) {
        boolean hasPermission = settingService.checkForPermission(deleteRequest.getPassword());
        if (!hasPermission) {
            throw new BlazeInvalidArgException("Vendor", "Do not have permission.");
        }

        // Check all products must be deleted
        long count = productRepository.count(token.getCompanyId());
        if (count != 0) {
            throw new BlazeInvalidArgException("Vendor", "Products must be deleted before deleting Vendors.");
        }
        vendorRepository.removeAllSetState(token.getCompanyId());
        realtimeService.sendRealTimeEvent(token.getShopId(), RealtimeService.RealtimeEventType.InventoryUpdateEvent, null);
    }

    @Override
    public void deleteAllDoctors(ResetDeleteRequest deleteRequest) {
        boolean hasPermission = settingService.checkForPermission(deleteRequest.getPassword());
        if (!hasPermission) {
            throw new BlazeInvalidArgException("Doctor", "Do not have permission.");
        }

        // Check all members must be deleted
        long count = memberRepository.count(token.getCompanyId());
        if (count != 0) {
            throw new BlazeInvalidArgException("Doctor", "Members must be deleted before deleting Physicians.");
        }
        doctorRepository.removeAllSetState(token.getCompanyId());
        realtimeService.sendRealTimeEvent(token.getShopId(), RealtimeService.RealtimeEventType.DoctorsUpdateEvent, null);
    }
}
