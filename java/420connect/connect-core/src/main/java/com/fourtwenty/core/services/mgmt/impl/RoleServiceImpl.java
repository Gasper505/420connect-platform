package com.fourtwenty.core.services.mgmt.impl;

import com.fourtwenty.core.domain.models.company.*;
import com.fourtwenty.core.domain.repositories.dispensary.AppRolePermissionRepository;
import com.fourtwenty.core.domain.repositories.dispensary.EmployeeRepository;
import com.fourtwenty.core.domain.repositories.dispensary.RoleRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.exceptions.BlazeAuthException;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.lifecycle.model.DefaultData;
import com.fourtwenty.core.rest.dispensary.requests.company.AppRolePermissionRequest;
import com.fourtwenty.core.rest.dispensary.requests.company.RoleAddRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.company.AppRolePermissionResult;
import com.fourtwenty.core.rest.dispensary.results.company.RolePermissionResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.mgmt.RoleService;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.google.inject.Provider;
import org.apache.commons.lang3.NotImplementedException;
import org.bson.types.ObjectId;

import java.util.*;

/**
 * Created by mdo on 6/15/16.
 */
public class RoleServiceImpl extends AbstractAuthServiceImpl implements RoleService {

    private static final String ROLE_MANAGER = "Manager";
    private static final String ROLE_ADMIN = "Admin";
    private static final String ROLE_DISPATCHER = "Dispatcher";
    private static final String ROLE_SHOP_MANAGER = "Shop Manager";
    private static final String ROLE_SHOP_DISPATCHER = "Shop Dispatcher";
    private static final String ROLE_FRONT_DESK = "Front Desk";
    private static final String ROLE_DELIVERY_DRIVER = "Delivery Driver";
    private static final String ROLE_BUDTENDER = "Budtender";

    private static final String ROLE = "Role";
    private static final String ROLE_NOT_FOUND = "Role not found";

    @Inject
    RoleRepository roleRepository;
    @Inject
    ShopRepository shopRepository;
    @Inject
    EmployeeRepository employeeRepository;
    @Inject
    private AppRolePermissionRepository appRolePermissionRepository;
    @Inject
    DefaultData defaultData;

    @Inject
    public RoleServiceImpl(Provider<ConnectAuthToken> token) {
        super(token);
    }


    @Override
    public Role getRoleById(String roleId) {
        return getRoleById(token.getCompanyId(), roleId);
    }

    @Override
    public Role getRoleById(String companyId, String roleId) {
        Role oldRole = roleRepository.get(companyId, roleId);
        return oldRole;
    }

    @Override
    public Role addRole(RoleAddRequest request) {
        Role oldRole = roleRepository.getRoleByName(token.getCompanyId(), request.getName());
        if (oldRole != null) {
            throw new BlazeInvalidArgException("Role", "Role name already exist.");
        }
        Role role = new Role();
        role.setCompanyId(token.getCompanyId());
        role.setId(ObjectId.get().toString());
        role.setPermissions(request.getPermissions());
        role.setName(request.getName());
        Role dbRole = roleRepository.save(role);
        Iterable<AppRolePermission> companyRolePermissions = appRolePermissionRepository.list(token.getCompanyId());
        List<AppRolePermission> appRolePermissions = Lists.newArrayList(companyRolePermissions);
        for (AppRolePermission appRolePermission : appRolePermissions) {
            if (!appRolePermission.getRoleList().contains(dbRole.getId())) {
                appRolePermission.getRoleList().add(dbRole.getId());
                appRolePermissionRepository.update(appRolePermission.getId(), appRolePermission);
            }
        }
        return dbRole;
    }

    @Override
    public SearchResult<Role> getAllRoles() {
        return roleRepository.findItemsWithSort(token.getCompanyId(), "{name:1}", 0, Integer.MAX_VALUE);
    }

    @Override
    public SearchResult<Role> getRoles(long afterDate, long beforeDate) {
        return roleRepository.findItemsWithDate(token.getCompanyId(), afterDate, beforeDate);
    }

    @Override
    public Role updateRole(String roleId, Role role) {
        Role dbRole = roleRepository.get(token.getCompanyId(), roleId);
        if (dbRole == null) {
            throw new BlazeInvalidArgException("Role", "Role doesn't exist.");
        }

        if (dbRole.getName().equalsIgnoreCase(ROLE_ADMIN) || dbRole.getName().equalsIgnoreCase(ROLE_MANAGER)
                || dbRole.getName().equalsIgnoreCase(ROLE_DELIVERY_DRIVER) || dbRole.getName().equalsIgnoreCase(ROLE_BUDTENDER)
                || dbRole.getName().equalsIgnoreCase(ROLE_FRONT_DESK) || dbRole.getName().equalsIgnoreCase(ROLE_DISPATCHER)
                || dbRole.getName().equalsIgnoreCase(ROLE_SHOP_MANAGER) || dbRole.getName().equalsIgnoreCase(ROLE_SHOP_DISPATCHER)) {
            throw new BlazeInvalidArgException(ROLE, "You can not update " + dbRole.getName() + " role");
        }

        if (!dbRole.getName().equalsIgnoreCase(role.getName())) {
            Role newNameRole = roleRepository.getRoleByName(token.getCompanyId(), role.getName());
            if (newNameRole != null) {
                throw new BlazeInvalidArgException("Role", "Role name already exist.");
            }
        }
        dbRole.setName(role.getName());
        dbRole.setPermissions(role.getPermissions());
        return roleRepository.update(token.getCompanyId(), roleId, dbRole);
    }

    @Override
    public boolean hasPermission(String companyId, String roleId, Role.Permission permission) {
        Role dbRole = roleRepository.get(companyId, roleId);
        if (dbRole == null) {
            return false;
        }

        return dbRole.getPermissions().contains(permission);
    }

    @Override
    public void checkPermission(String companyId, String roleId, Role.Permission permission) {
        Role dbRole = roleRepository.get(companyId, roleId);
        if (dbRole == null) {
            throw new BlazeAuthException("Role.Permission", permission.getErrorMsg());
        }
        boolean hasPerm = false;
        for (Role.Permission perm : dbRole.getPermissions()) {
            if (perm.name().equalsIgnoreCase(permission.name())) {
                hasPerm = true;
                break;
            }
        }
        if (!hasPerm) {
            throw new BlazeAuthException("Role.Permission", permission.getErrorMsg());
        }
    }


    @Override
    public boolean canAccessRestricted(String companyId, String shopId, String employeeId) {
        Shop shop = shopRepository.get(companyId, shopId);
        if (shop != null) {
            if (shop.isRestrictedViews()) {
                // it's restricted, so now check the employee
                Employee employee = employeeRepository.get(companyId, employeeId);
                if (employee == null) {
                    return false;
                }

                Role role = roleRepository.get(companyId, employee.getRoleId());
                // if role exists, and if it's any of these roles, then they automatically have access to members
                if (role != null) {
                    String roleName = role.getName();
                    if (roleName.equalsIgnoreCase(ROLE_ADMIN)
                            || roleName.equalsIgnoreCase(ROLE_MANAGER)
                            || roleName.equalsIgnoreCase(ROLE_DISPATCHER)
                            || roleName.equalsIgnoreCase(ROLE_SHOP_DISPATCHER)
                            || roleName.equalsIgnoreCase(ROLE_SHOP_MANAGER)
                            || roleName.equalsIgnoreCase(ROLE_FRONT_DESK)) {
                        return true;
                    }
                }


                // websettingsView == manager level
                return hasPermission(companyId, employee.getRoleId(), Role.Permission.WebSettingsView);
            }
        }
        return true;
    }

    @Override
    public void deleteRole(String roleId) {
        throw new NotImplementedException("Note implemented");
    }

    @Override
    public AppRolePermission assignPermissionByRole(AppRolePermissionRequest request) {
        AppRolePermission rolePermission = null;
        Role role = roleRepository.get(token.getCompanyId(), request.getRoleId());
        if (role == null) {
            throw new BlazeInvalidArgException(ROLE, ROLE_NOT_FOUND);
        }

        rolePermission = appRolePermissionRepository.getPermissionsByCompanyAndRole(token.getCompanyId(), request.getAppTarget());
        boolean create = false;
        LinkedHashSet<String> roleList;
        if (rolePermission == null) {
            create = true;
            rolePermission = new AppRolePermission();
            rolePermission.prepare(token.getCompanyId());
            roleList = new LinkedHashSet<>();
        } else {
            roleList = rolePermission.getRoleList();
        }

        role.setPermissions(this.filterPermissions(request.getEnabledPermissions(), request.getDisabledPermissions()));
        Role updatedRole = roleRepository.update(token.getCompanyId(), role.getId(), role);

        roleList.add(role.getId());
        rolePermission.setRoleList(roleList);
        if (updatedRole != null) {
            if (create) {
                appRolePermissionRepository.save(rolePermission);
            } else {
                appRolePermissionRepository.update(token.getCompanyId(), rolePermission.getId(), rolePermission);
            }
        }

        return rolePermission;
    }

    private Set<Role.Permission> filterPermissions(Set<Role.Permission> enabledPermissions, Set<Role.Permission> disabledPermissions) {
        Set<Role.Permission> permissions = new HashSet<>();
        Set<Role.Permission> disabled = new HashSet<>();

        permissions.addAll(enabledPermissions);

        // Remove all disabled permissions from enabled set
        if (!Objects.isNull(disabledPermissions) && !disabledPermissions.isEmpty()) {
            for (Role.Permission disabledPermission : disabledPermissions) {
                List<Role.Permission> permissionList = Role.Permission.getDisabledPermission(disabledPermission);
                if (!Objects.isNull(permissionList)) {
                    disabled.addAll(permissionList);
                }
            }
        }
        enabledPermissions.removeAll(disabled);
        permissions.removeAll(disabled);

        // If manage in enabled and is view not in disabled then enable view
        for (Role.Permission manage : enabledPermissions) {
            List<Role.Permission> enabledPermission = Role.Permission.getEnabledPermission(manage);
            if (!Objects.isNull(enabledPermission)) {
                for (Role.Permission view : enabledPermission) {
                    if (!disabledPermissions.contains(view)) {
                        permissions.add(view);
                    }
                }
            }
        }
        return permissions;
    }

    /**
     * This method gets app permission by company
     */
    @Override
    public List<AppRolePermissionResult> getPermissionsByCompany() {
        Iterable<AppRolePermissionResult> rolePermissions = appRolePermissionRepository.list(token.getCompanyId(), AppRolePermissionResult.class);
        ArrayList<AppRolePermissionResult> appRolePermissionList = Lists.newArrayList(rolePermissions);
        HashMap<String, Role> roleMap = roleRepository.listAsMap(token.getCompanyId());

        if (appRolePermissionList.isEmpty()) {

            LinkedHashSet<String> rolesList = new LinkedHashSet<>();
            roleMap.values().forEach(role -> rolesList.add(role.getId()));

            for (CompanyFeatures.AppTarget appTarget : CompanyFeatures.AppTarget.values()) {
                AppRolePermission rolePermission = new AppRolePermission();
                rolePermission.prepare(token.getCompanyId());
                rolePermission.setAppTarget(appTarget);
                rolePermission.setRoleList(rolesList);
                appRolePermissionRepository.save(rolePermission);
            }
            rolePermissions = appRolePermissionRepository.list(token.getCompanyId(), AppRolePermissionResult.class);
            appRolePermissionList = Lists.newArrayList(rolePermissions);
        }

        if (!appRolePermissionList.isEmpty()) {
            this.prepareAppPermissionResult(appRolePermissionList, roleMap);
        }

        return appRolePermissionList;
    }

    private void prepareAppPermissionResult(List<AppRolePermissionResult> rolePermissions, HashMap<String, Role> roleMap) {
        for (AppRolePermissionResult permission : rolePermissions) {
            LinkedHashSet<String> roleList = permission.getRoleList();
            List<Role> roles = permission.getRoles();
            List<Role> updatedRoles = new ArrayList<>();
            if (roles == null) {
                roles = new ArrayList<>();
            }

            if (roleList != null) {
                for (String roleId : roleList) {
                    Role role = roleMap.get(roleId);
                    if (role != null) {
                        roles.add(role);
                    }
                }
            }
            updatedRoles.addAll(roles);
            for (Role role : roles) {
                if (role.getName().equalsIgnoreCase("Admin")) {
                    for (Role defaultRole : defaultData.getRoles()) {
                        if (defaultRole.getName().equalsIgnoreCase("Admin")) {
                            role.setPermissions(defaultRole.getPermissions());
                            updatedRoles.set(updatedRoles.indexOf(role), role);
                        }
                    }
                }
            }

            permission.setRoles(updatedRoles);
        }
    }

    @Override
    public List<RolePermissionResult> getAllPermissions() {
        List<RolePermissionResult> rolePermissionResults = new ArrayList<>();
        Role.Permission[] values = Role.Permission.values();

        for (Role.Permission value : values) {
            RolePermissionResult rolePermission = new RolePermissionResult();
            rolePermission.setPermission(value.getPermission());
            rolePermission.setDescription(value.getDescription());
            rolePermission.setKey(value.toString());
            rolePermission.setGroup(value.getGroup());
            rolePermission.setSection(value.getSection());
            rolePermissionResults.add(rolePermission);
        }

        return rolePermissionResults;
    }
}
