package com.fourtwenty.core.services.mgmt.impl;

import com.fourtwenty.core.domain.models.company.*;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.loyalty.Promotion;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductPriceRange;
import com.fourtwenty.core.domain.models.product.ProductWeightTolerance;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.exceptions.BlazeAuthException;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.reporting.model.reportmodels.MemberByField;
import com.fourtwenty.core.rest.dispensary.requests.UpdateToleranceRequest;
import com.fourtwenty.core.rest.dispensary.requests.company.MemberGroupAddRequest;
import com.fourtwenty.core.rest.dispensary.requests.promotions.PromotionAddRequest;
import com.fourtwenty.core.rest.dispensary.results.ListResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.company.MemberGroupResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.common.RealtimeService;
import com.fourtwenty.core.services.mgmt.PromotionService;
import com.fourtwenty.core.services.mgmt.RoleService;
import com.fourtwenty.core.services.mgmt.SettingService;
import com.fourtwenty.core.util.SecurityUtil;
import com.google.common.collect.Lists;
import com.google.inject.Provider;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.function.Predicate;

/**
 * Created by mdo on 10/25/15.
 */
public class SettingServiceImpl extends AbstractAuthServiceImpl implements SettingService {

    private static final String PROMOTION = "Promotion";
    private static final String PROMOTION_NAME_EXIST = "Promotion is already exist with this name";

    @Inject
    ProductWeightToleranceRepository toleranceRepository;
    @Inject
    MemberGroupRepository memberGroupRepository;
    @Inject
    RealtimeService realtimeService;
    @com.google.inject.Inject
    MemberRepository memberRepository;
    @Inject
    ProductRepository productRepository;
    @Inject
    QueuedTransactionRepository queuedTransactionRepository;
    @Inject
    RoleService roleService;
    @Inject
    SecurityUtil securityUtil;
    @Inject
    EmployeeRepository employeeRepository;
    @Inject
    ShopRepository shopRepository;
    @Inject
    private PromotionService promotionService;
    @Inject
    private PromotionRepository promotionRepository;

    @Inject
    public SettingServiceImpl(Provider<ConnectAuthToken> token) {
        super(token);
    }

    @Override
    public ListResult<ProductWeightTolerance> getProductTolerances() {
        Iterable<ProductWeightTolerance> toleranceList = toleranceRepository.listSort(token.getCompanyId(), "{startWeight:1}");
        return new ListResult<ProductWeightTolerance>(Lists.newArrayList(toleranceList));
    }

    @Override
    public void updateTolerance(String toleranceId, UpdateToleranceRequest request) {

        ProductWeightTolerance tolerance = toleranceRepository.get(token.getCompanyId(), toleranceId);
        if (tolerance == null) {
            throw new BlazeInvalidArgException("ToleranceId", "Item not found.");
        }
        // Check if there are current pending queued items

        //tolerance.setName(request.getName());
        tolerance.setEndWeight(request.getEndWeight());
        tolerance.setStartWeight(request.getStartWeight());
        tolerance.setUnitValue(request.getUnitValue());
        tolerance.setPriority(request.getPriority());
        tolerance.setModified(DateTime.now().getMillis());
        tolerance.setEnabled(request.isEnabled());
        toleranceRepository.update(token.getCompanyId(), tolerance.getId(), tolerance);

        Iterable<Product> products = productRepository.findProductsWithRanges(token.getCompanyId(), token.getShopId());

        for (Product product : products) {
            if (tolerance.isEnabled() == false) {
                // remove
                int oldRangeCount = product.getPriceRanges().size();
                product.getPriceRanges().removeIf(new Predicate<ProductPriceRange>() {
                    @Override
                    public boolean test(ProductPriceRange productPriceRange) {

                        return tolerance.getId().equalsIgnoreCase(productPriceRange.getWeightToleranceId());
                    }
                });
                if (oldRangeCount != product.getPriceRanges().size()) {
                    // was removed
                    productRepository.updateProductPriceRanges(token.getCompanyId(), product.getId(), product.getPriceRanges());
                }

            } else {
                for (ProductPriceRange range : product.getPriceRanges()) {
                    if (range.getWeightToleranceId().equalsIgnoreCase(tolerance.getId())) {
                        range.setWeightTolerance(tolerance);
                        productRepository.updateProductPriceRanges(token.getCompanyId(), product.getId(), product.getPriceRanges());
                        break;
                    }
                }
            }
        }
        realtimeService.sendRealTimeEvent(token.getShopId(), RealtimeService.RealtimeEventType.InventoryUpdateEvent, null);
    }

    // Member Groups
    @Override
    public SearchResult<MemberGroupResult> getMemberGroups() {
        SearchResult<MemberGroupResult> result = null;
        if (token.getMembersShareOption() == Company.CompanyMembersShareOption.Shared) {
            result = memberGroupRepository.findItems(token.getCompanyId(), 0, Integer.MAX_VALUE, MemberGroupResult.class);
        } else {
            result = memberGroupRepository.findItems(token.getCompanyId(), token.getShopId(), 0, Integer.MAX_VALUE, MemberGroupResult.class);
        }
        setMembersCount(result);
        return result;
    }

    @Override
    public SearchResult<MemberGroupResult> getActiveMemberGroups() {
        SearchResult<MemberGroupResult> result = null;

        if (token.getMembersShareOption() == Company.CompanyMembersShareOption.Shared) {
            result = memberGroupRepository.getActiveMemberGroups(token.getCompanyId(), MemberGroupResult.class);
        } else {
            result = memberGroupRepository.getActiveMemberGroupsShop(token.getCompanyId(), token.getShopId(), MemberGroupResult.class);
        }
        setMembersCount(result);
        return result;
    }

    private void setMembersCount(SearchResult<MemberGroupResult> searchResult) {
        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());
        long lastSync = shop.getMembersCountSyncDate() != null ? shop.getMembersCountSyncDate() : 0l;

        DateTime lastSync2 = new DateTime(lastSync).plusMinutes(60);
        if (lastSync2.isBeforeNow()) {

            Iterable<MemberByField> results = null;
            Iterable<MemberByField> textResults = null;
            Iterable<MemberByField> emailResults = null;

            if (token.getMembersShareOption() == Company.CompanyMembersShareOption.Shared) {
                results = memberRepository.getMembershipsByGroup(token.getCompanyId());
                textResults = memberRepository.getMembershipsByGroupTextOptIn(token.getCompanyId());
                emailResults = memberRepository.getMembershipsByGroupEmailOptIn(token.getCompanyId());
            } else {
                results = memberRepository.getMembershipsByGroup(token.getCompanyId(), token.getShopId());
                textResults = memberRepository.getMembershipsByGroupTextOptIn(token.getCompanyId(), token.getShopId());
                emailResults = memberRepository.getMembershipsByGroupEmailOptIn(token.getCompanyId(), token.getShopId());
            }

            HashMap<String, MemberByField> memberByFieldHashMap = new HashMap<>();
            for (MemberByField field : results) {
                memberByFieldHashMap.put(field.get_id(), field);
            }

            HashMap<String, MemberByField> memberTextByFieldHashMap = new HashMap<>();
            for (MemberByField field : textResults) {
                memberTextByFieldHashMap.put(field.get_id(), field);
            }

            HashMap<String, MemberByField> memberEmailByFieldHashMap = new HashMap<>();
            for (MemberByField field : emailResults) {
                memberEmailByFieldHashMap.put(field.get_id(), field);
            }

            for (MemberGroupResult group : searchResult.getValues()) {
                // Regular count
                MemberByField field = memberByFieldHashMap.get(group.getId());
                if (field != null && field.getCount() != null) {
                    group.setMemberCount(field.getCount().intValue());
                } else {
                    group.setMemberCount(0);
                }

                // TextCount count
                MemberByField textCount = memberTextByFieldHashMap.get(group.getId());
                if (textCount != null && textCount.getCount() != null) {
                    group.setMemberCountTextOptIn(textCount.getCount().intValue());
                } else {
                    group.setMemberCountTextOptIn(0);
                }

                // Email Count count
                MemberByField emailCount = memberEmailByFieldHashMap.get(group.getId());
                if (emailCount != null && emailCount.getCount() != null) {
                    group.setMemberCountEmailOptIn(emailCount.getCount().intValue());
                } else {
                    group.setMemberCountEmailOptIn(0);
                }

                memberGroupRepository.update(token.getCompanyId(), group.getId(), group);
            }


            shopRepository.updateLastMembersSyncDate(token.getCompanyId(), token.getShopId());
        }

    }


    @Override
    public MemberGroup updateMemberGroup(String memberGroupId, MemberGroup group) {
        MemberGroup dbGroup = memberGroupRepository.get(token.getCompanyId(), memberGroupId);
        if (dbGroup == null) {
            throw new BlazeInvalidArgException("MemberGroup", "Member group does not exist with this id.");
        }
        //check updated name is duplicate or not
        if (!group.getName().equals(dbGroup.getName())) {

            MemberGroup memberGroups = null;

            if (token.getMembersShareOption() == Company.CompanyMembersShareOption.Shared) {
                memberGroups = memberGroupRepository.getMemberGroup(token.getCompanyId(), group.getName());
            } else {
                memberGroups = memberGroupRepository.getMemberGroup(token.getCompanyId(), token.getShopId(), group.getName());
            }
            if (memberGroups != null) {
                throw new BlazeInvalidArgException("MemberGroup", "Member group already exist with this name.");
            }
        }

        if (dbGroup.isDefaultGroup() && !group.isDefaultGroup()) {
            // we're making this membergroup to false.. which is no.
            throw new BlazeInvalidArgException("MemberGroup", "Please set another member group as 'default' before setting this member group to non-default.");
        }

        dbGroup.setDefaultGroup(group.isDefaultGroup());
        dbGroup.setName(group.getName());
        dbGroup.setDiscount(group.getDiscount());
        dbGroup.setActive(group.isActive());
        dbGroup.setDiscountType(group.getDiscountType());
        dbGroup.setEnablePromotion(group.isEnablePromotion());

        if (dbGroup.isDefaultGroup()) {
            if (token.getMembersShareOption() == Company.CompanyMembersShareOption.Shared) {
                memberGroupRepository.setMemberGroupFalse(token.getCompanyId());
            } else {
                memberGroupRepository.setMemberGroupFalse(token.getCompanyId(), token.getShopId());
            }
        }

        Promotion dbPromotion = null;
        if (StringUtils.isNotBlank(dbGroup.getPromotionId())) {
            dbPromotion = promotionRepository.get(dbGroup.getCompanyId(), dbGroup.getPromotionId());
        }

        if (dbGroup.isEnablePromotion()) {
            if (dbPromotion != null) {
                if (!dbPromotion.getName().equals("MemberGroup: " + dbGroup.getName())) {
                    Promotion promotionByName = promotionRepository.getPromotionByName(token.getCompanyId(), token.getShopId(), dbPromotion.getName());
                    if (promotionByName != null) {
                        throw new BlazeInvalidArgException(PROMOTION, PROMOTION_NAME_EXIST);
                    }
                }
                if (dbGroup.isActive()) {
                    dbPromotion.setActive(Boolean.TRUE);
                } else if (!dbGroup.isActive()) {
                    dbPromotion.setActive(Boolean.FALSE);
                }

                dbPromotion.setName("MemberGroup: " + dbGroup.getName());
                //dbPromotion.setDiscountAmt(dbGroup.getDiscount());
                dbPromotion.setPromoSource(Promotion.PromoSource.MemberGroup);
                promotionRepository.update(dbPromotion.getId(), dbPromotion);
            } else {
                dbPromotion = this.createPromotionForMemberGroup("MemberGroup: " + dbGroup.getName(), dbGroup.getDiscount(), dbGroup.getDiscountType(), dbGroup.isActive());
            }
        } else if ((dbPromotion != null && dbPromotion.isActive()) || !dbGroup.isActive()) {
            dbPromotion.setActive(Boolean.FALSE);
            promotionRepository.update(dbPromotion.getId(), dbPromotion);
        }

        if (dbPromotion != null) {
            dbGroup.setPromotionId(dbPromotion.getId());
        }

        memberGroupRepository.update(token.getCompanyId(), memberGroupId, dbGroup);


        memberRepository.updateMemberGroup(token.getCompanyId(), memberGroupId, dbGroup);


        realtimeService.sendRealTimeEvent(token.getShopId(), RealtimeService.RealtimeEventType.MemberGroupsUpdateEvent, null);
        return dbGroup;
    }

    /**
     * Create New Member Group
     *
     * @param group
     * @return New Memeber Group Object
     */
    @Override
    public MemberGroup createMemberGroup(MemberGroupAddRequest group) {

        String companyId = token.getCompanyId();
        String name = group.getName();

        MemberGroup memberGroups = null;

        if (token.getMembersShareOption() == Company.CompanyMembersShareOption.Shared) {
            memberGroups = memberGroupRepository.getMemberGroup(companyId, name);
        } else {
            memberGroups = memberGroupRepository.getMemberGroup(companyId, token.getShopId(), name);
        }
        if (memberGroups != null) {
            throw new BlazeInvalidArgException("MemberGroup", "Member group already exist with this name.");
        }

        // If incoming group is set to default group, then set other group to default = false
        if (group.isDefaultGroup()) {
            if (token.getMembersShareOption() == Company.CompanyMembersShareOption.Shared) {
                memberGroupRepository.setMemberGroupFalse(token.getCompanyId());
            } else {
                memberGroupRepository.setMemberGroupFalse(token.getCompanyId(), token.getShopId());
            }
        }


        MemberGroup memberGroup = new MemberGroup();
        memberGroup.prepare(token.getCompanyId());
        memberGroup.setShopId(token.getShopId());
        memberGroup.setActive(group.isActive());
        memberGroup.setName(group.getName());
        memberGroup.setDiscount(group.getDiscount());
        memberGroup.setDefaultGroup(group.isDefaultGroup());
        memberGroup.setDiscountType(group.getDiscountType());
        memberGroup.setEnablePromotion(group.isEnablePromotion());

        if (memberGroup.isEnablePromotion()) {
            //Check if promotion exist with member group's name
            Promotion promotionByName = promotionRepository.getPromotionByName(token.getCompanyId(), token.getShopId(), "MemberGroup: " + memberGroup.getName());

            if (promotionByName != null) {
                throw new BlazeInvalidArgException(PROMOTION, PROMOTION_NAME_EXIST);
            }

            Promotion promotion = this.createPromotionForMemberGroup(memberGroup.getName(), memberGroup.getDiscount(), memberGroup.getDiscountType(), memberGroup.isActive());

            if (promotion == null) {
                throw new BlazeInvalidArgException("Member Group", "Promotion could not be created");
            }

            memberGroup.setPromotionId(promotion.getId());
        }

        memberGroupRepository.save(memberGroup);

        realtimeService.sendRealTimeEvent(token.getCompanyId().toString(), RealtimeService.RealtimeEventType.MemberGroupsUpdateEvent, null);
        return memberGroup;
    }

    private Promotion createPromotionForMemberGroup(String name, BigDecimal discount, OrderItem.DiscountType discountType, boolean active) {
        PromotionAddRequest promotionAddRequest = new PromotionAddRequest();
        promotionAddRequest.setName(name);
        promotionAddRequest.setPromoSource(Promotion.PromoSource.MemberGroup);
        promotionAddRequest.setDiscountAmt(discount);
        promotionAddRequest.setActive(active);
        promotionAddRequest.setDiscountType(discountType);
        promotionAddRequest.setStackable(false);

        return promotionService.addPromotion(promotionAddRequest);
    }

    @Override
    public void deleteMemberGroup(String memberGroupId) {
        MemberGroup memberGroup = memberGroupRepository.get(token.getCompanyId(), memberGroupId);
        if (memberGroup == null) {
            throw new BlazeInvalidArgException("MemberGroup", "Membergroup does not exist.");
        }

        if (memberGroup.isDefaultGroup()) {
            throw new BlazeInvalidArgException("MemberGroup", "Cannot delete a default member group.");
        }

        // Get default memberGroup
        MemberGroup defaultMemberGroup = memberGroupRepository.getDefaultMemberGroup(token.getCompanyId());

        if (defaultMemberGroup == null) {
            throw new BlazeInvalidArgException("MemberGroup", "A default group does not exist to re-assigned members.");
        }

        Iterable<Member> members = memberRepository.getMemberByMemberGroup(token.getCompanyId(), memberGroup.getId());
        for (Member member : members) {
            member.setMemberGroupId(defaultMemberGroup.getId());
            member.setMemberGroup(defaultMemberGroup);
            memberRepository.update(token.getCompanyId(), member.getId(), member);
        }

        if (StringUtils.isNotBlank(memberGroup.getPromotionId())) {
            Promotion promotion = promotionRepository.get(token.getCompanyId(), memberGroup.getPromotionId());

            if (promotion != null) {
                promotionService.deletePromotion(promotion.getId());
            }
        }

        // delete member group
        memberGroupRepository.removeById(token.getCompanyId(), memberGroupId, memberGroup.getName());


        realtimeService.sendRealTimeEvent(token.getCompanyId().toString(), RealtimeService.RealtimeEventType.MemberGroupsUpdateEvent, null);
        realtimeService.sendRealTimeEvent(token.getCompanyId().toString(), RealtimeService.RealtimeEventType.MembersUpdateEvent, null);
    }

    @Override
    public boolean checkForPermission(String password) {
        Employee employee = employeeRepository.get(token.getCompanyId(), token.getActiveTopUser().getUserId());
        if (employee == null) {
            throw new BlazeInvalidArgException("Employee", "Session is invalid.");
        }

        if (!securityUtil.checkPassword(password, employee.getPassword())) {
            throw new BlazeAuthException("Authentication", "Password do not match.");
        }

        boolean hasSettingsPerm = roleService.hasPermission(token.getCompanyId(), employee.getRoleId(), Role.Permission.WebSettingsManage);

        if (!hasSettingsPerm) {
            throw new BlazeAuthException("Permission", "You don't have permission.");
        }
        return true;
    }
}
