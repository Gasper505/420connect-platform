package com.fourtwenty.core.services.mgmt.impl;

import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.config.pdf.PdfGenerator;
import com.fourtwenty.core.domain.models.company.*;
import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.models.product.CompanyLicense;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.models.purchaseorder.*;
import com.fourtwenty.core.domain.models.transaction.TaxResult;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.managed.AmazonServiceManager;
import com.fourtwenty.core.rest.common.EmailRequest;
import com.fourtwenty.core.rest.dispensary.requests.inventory.ShipmentBillPaymentStatusRequest;
import com.fourtwenty.core.rest.dispensary.requests.inventory.ShipmentBillUpdateRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.POProductRequestResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.ShipmentBillRequestResult;
import com.fourtwenty.core.rest.purchaseorders.POCompleteRequest;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.mgmt.EmployeeService;
import com.fourtwenty.core.services.mgmt.POActivityService;
import com.fourtwenty.core.services.mgmt.PurchaseOrderService;
import com.fourtwenty.core.services.mgmt.ShipmentBillService;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.core.util.TextUtil;
import com.google.inject.Provider;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.assertj.core.util.Lists;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;

public class ShipmentBillServiceImpl extends AbstractAuthServiceImpl implements ShipmentBillService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ShipmentBillServiceImpl.class);
    private static final String BILL_NOT_FOUND = "Error! Shipment Bill not found";
    private static final String PO_NOT_FOUND = "Error! Please provide valid purchase order";
    private static final String SHIPMENT_BILL = "Shipment Bill";
    private static final String AMOUNT_NOT_ZERO = "Payment amount cannot be zero.";
    private static final String PAYMENT_TYPE_NOT_FOUND = "Payment type does not exist.";
    private static final String PURCHASE_ORDER = "Purchase Order";
    private static final String SHIPMENT_BILL_HTML_RESOURCE = "/shipmentBillPDF.html";

    private ShipmentBillRepository shipmentBillRepository;
    private VendorRepository vendorRepository;
    private PurchaseOrderRepository purchaseOrderRepository;
    private CompanyRepository companyRepository;
    private EmployeeService employeeService;
    private ProductRepository productRepository;
    private AmazonServiceManager amazonServiceManager;
    private ConnectConfiguration connectConfiguration;
    private ShopRepository shopRepository;
    private PurchaseOrderService purchaseOrderService;
    private ProductCategoryRepository productCategoryRepository;
    private POActivityService poActivityService;
    @Inject
    private AdjustmentRepository adjustmentRepository;
    @Inject
    public ShipmentBillServiceImpl(ShipmentBillRepository shipmentBillRepository,
                                   Provider<ConnectAuthToken> tokenProvider,
                                   VendorRepository vendorRepository,
                                   PurchaseOrderRepository purchaseOrderRepository, CompanyRepository companyRepository, EmployeeService employeeService, ProductRepository productRepository, AmazonServiceManager amazonServiceManager, ConnectConfiguration connectConfiguration, ShopRepository shopRepository, PurchaseOrderService purchaseOrderService, ProductCategoryRepository productCategoryRepository, POActivityService poActivityService) {
        super(tokenProvider);
        this.shipmentBillRepository = shipmentBillRepository;
        this.vendorRepository = vendorRepository;
        this.purchaseOrderRepository = purchaseOrderRepository;
        this.companyRepository = companyRepository;
        this.employeeService = employeeService;
        this.productRepository = productRepository;
        this.amazonServiceManager = amazonServiceManager;
        this.connectConfiguration = connectConfiguration;
        this.shopRepository = shopRepository;
        this.purchaseOrderService = purchaseOrderService;
        this.productCategoryRepository = productCategoryRepository;
        this.poActivityService = poActivityService;
    }

    /**
     * getShipmentBillById is used to get shipment bill info by id
     *
     * @param shipmentId : shipment bill id
     */
    @Override
    public ShipmentBillRequestResult getShipmentBillById(final String shipmentId) {
        ShipmentBillRequestResult dbShipmentBill = shipmentBillRepository.get(token.getCompanyId(), shipmentId, ShipmentBillRequestResult.class);
        if (dbShipmentBill == null) {
            throw new BlazeInvalidArgException(SHIPMENT_BILL, BILL_NOT_FOUND);
        }
        PurchaseOrder purchaseOrder = purchaseOrderRepository.getPOById(token.getCompanyId(), dbShipmentBill.getPurchaseOrderId());

        PurchaseOrder boPurchaseOrder = purchaseOrderRepository.getBOByPOId(token.getCompanyId(), purchaseOrder.getId());


        if (boPurchaseOrder != null) {
            dbShipmentBill.setBackOrderPOId(boPurchaseOrder.getId());
            dbShipmentBill.setBackOrder(boPurchaseOrder);
        }

        dbShipmentBill.setPurchaseOrder(purchaseOrder);
        dbShipmentBill.setCustomerType(purchaseOrder.getCustomerType());

        Vendor vendor = vendorRepository.getById(purchaseOrder.getVendorId());
        if (vendor == null) {
            throw new BlazeInvalidArgException("Vendor", "Vendor can't be empty");
        }
        dbShipmentBill.setVendor(vendor);

        preparePurchaseOrderResult(dbShipmentBill);

        return dbShipmentBill;
    }

    private void preparePurchaseOrderResult(ShipmentBillRequestResult shipmentBill) {
        List<POProductRequestResult> poProductRequestResultList = new ArrayList<>();
        if (shipmentBill.getPoProductRequest() != null) {
            List<ObjectId> productIds = new ArrayList<>();
            for (POProductRequest poProductRequest : shipmentBill.getPoProductRequest()) {
                productIds.add(new ObjectId(poProductRequest.getProductId()));
            }
            for (POProductRequest poProductRequest : shipmentBill.getPurchaseOrder().getPoProductRequestList()) {
                productIds.add(new ObjectId(poProductRequest.getProductId()));
            }

            for (POProductRequest poProductRequest : shipmentBill.getPurchaseOrder().getPoProductRequestList()) {
                productIds.add(new ObjectId(poProductRequest.getProductId()));
            }

            HashMap<String, Product> productMap = productRepository.listAsMap(token.getCompanyId(), productIds);

            HashMap<String, ProductCategory> productCategoryHashMap = productCategoryRepository.listAsMap(token.getCompanyId(), token.getShopId());
            Product product = null;
            for (POProductRequest productRequest : shipmentBill.getPoProductRequest()) {

                ProductCategory productCategory = null;
                if (!productRequest.getProductId().isEmpty()) {
                    product = productMap.get(productRequest.getProductId());
                    productCategory = productCategoryHashMap.get(product.getCategoryId());
                }

                POProductRequestResult requestResult = new POProductRequestResult();
                requestResult.setProduct(product);
                requestResult.setCompanyId(productRequest.getCompanyId());
                requestResult.setId(productRequest.getId());
                requestResult.setCreated(productRequest.getCreated());
                requestResult.setDeleted(productRequest.isDeleted());
                requestResult.setModified(productRequest.getModified());
                requestResult.setDirty(productRequest.isDirty());
                requestResult.setShopId(productRequest.getShopId());
                requestResult.setUpdated(productRequest.isUpdated());
                requestResult.setDeclineReason(productRequest.getDeclineReason());

                requestResult.setNotes(productRequest.getNotes());
                requestResult.setProductId(productRequest.getProductId());
                requestResult.setProductName(productRequest.getProductName());
                requestResult.setReceivedQuantity(productRequest.getReceivedQuantity());
                requestResult.setRequestQuantity(productRequest.getRequestQuantity());
                requestResult.setUnitPrice(productRequest.getUnitPrice());
                requestResult.setTrackTraceSystem(productRequest.getTrackTraceSystem());
                requestResult.setRequestStatus(productRequest.getRequestStatus());
                requestResult.setTotalCost(productRequest.getTotalCost());
                requestResult.setTrackingPackagesList(productRequest.getTrackingPackagesList());
                requestResult.setExciseTax(productRequest.getExciseTax());
                requestResult.setTotalExciseTax(productRequest.getTotalExciseTax());

                requestResult.setDiscount(productRequest.getDiscount());

                if (productRequest.getDiscount() != null && productRequest.getDiscount().doubleValue() > productRequest.getTotalCost().doubleValue()) {
                        throw new BlazeInvalidArgException("Product", "Discount cannot be greater than total Cost.");
                }
                BigDecimal finaTotalCost = productRequest.getTotalCost().subtract(productRequest.getDiscount() == null ? BigDecimal.ZERO : productRequest.getDiscount());
                requestResult.setFinalTotalCost(finaTotalCost);

                if (productCategory != null && product != null) {
                    boolean cannabis = productCategory.isCannabis();
                    if ((product.getCannabisType() == Product.CannabisType.DEFAULT && productCategory.isCannabis())
                            || (product.getCannabisType() != Product.CannabisType.CBD
                            && product.getCannabisType() != Product.CannabisType.NON_CANNABIS
                            && product.getCannabisType() != Product.CannabisType.DEFAULT)) {
                        cannabis = true;
                    }
                    requestResult.setCannabis(cannabis);
                }

                poProductRequestResultList.add(requestResult);

            }

            List<POProductRequest> poProductRequestList = shipmentBill.getPurchaseOrder().getPoProductRequestList();
            for (int i = 0; i < poProductRequestList.size(); i++) {
                POProductRequest poProductRequest = poProductRequestList.get(i);
                POProductRequestResult poProductRequestResult = new POProductRequestResult(poProductRequest);

                poProductRequestResult.setProduct(productMap.get(poProductRequest.getProductId()));

                Product poProduct = productMap.get(poProductRequest.getProductId());
                if (poProduct != null && productCategoryHashMap.get(poProduct.getCategoryId()) != null) {
                    ProductCategory productCategory = productCategoryHashMap.get(poProduct.getCategoryId());

                    boolean cannabis = false;
                    if (productCategory != null) {
                        cannabis = productCategory.isCannabis();
                        if ((poProduct.getCannabisType() == Product.CannabisType.DEFAULT && cannabis)
                                || (poProduct.getCannabisType() != Product.CannabisType.CBD
                                && poProduct.getCannabisType() != Product.CannabisType.NON_CANNABIS
                                && poProduct.getCannabisType() != Product.CannabisType.DEFAULT)) {
                            cannabis = true;
                        }
                    }

                    poProductRequestResult.setCannabis(cannabis);
                }
                poProductRequestList.set(i, poProductRequestResult);
            }
        }

        shipmentBill.setPoProductRequest(null);
        shipmentBill.setPoProductRequestResultList(poProductRequestResultList);

        List<ShipmentBillPayment> shipmentBillPayments = shipmentBill.getPaymentHistory();
        shipmentBill.setPaymentHistory(filterPaymentHistory(shipmentBillPayments, false));
    }

    /**
     * getShipmentBillByPoId is used to get shipment bill info by po id
     *
     * @param poId : purchase order id
     */
    @Override
    public ShipmentBill getShipmentBillByPoId(final String poId) {
        ShipmentBill shipmentBillByPoId = shipmentBillRepository.getShipmentBillByPoId(poId);
        if (shipmentBillByPoId == null) {
            throw new BlazeInvalidArgException(SHIPMENT_BILL, "Error ! Shipment bill not found for this purchase order");
        }
        shipmentBillByPoId.setPaymentHistory(filterPaymentHistory(shipmentBillByPoId.getPaymentHistory(), false));
        if (CollectionUtils.isNotEmpty(shipmentBillByPoId.getPaymentHistory())) {
            shipmentBillByPoId.getPaymentHistory().sort(Comparator.comparing(ShipmentBillPayment::getCreated));
        }
        if(shipmentBillByPoId.getGrandTotal().setScale(2, RoundingMode.HALF_EVEN).compareTo(shipmentBillByPoId.getAmountPaid().setScale(2,RoundingMode.HALF_EVEN)) > 0 && shipmentBillByPoId.getAmountPaid().doubleValue() > 0) {
            shipmentBillByPoId.setPaymentStatus(ShipmentBill.PaymentStatus.PARTIAL);
        }
        return shipmentBillByPoId;
    }

    /**
     * addShipmentBill is used to save all shipment billing information those are requested
     * by
     *
     * @param poId          : purchase order id
     * @param request       : ShipmentBillAddRequest's object
     * @param purchaseOrder
     */
    @Override
    public ShipmentBill addShipmentBill(final String poId, final POCompleteRequest request, PurchaseOrder purchaseOrder) {
        ShipmentBill dbShipmentBill;

        final ShipmentBill shipmentBill = new ShipmentBill();
        shipmentBill.setPurchaseOrderId(poId);
        shipmentBill.setVendorId(purchaseOrder.getVendorId());
        shipmentBill.setCreatedBy(token.getActiveTopUser().getUserId());
        if (request.getAsset() != null) {
            List<CompanyAsset> assetList = request.getAsset();
            for (CompanyAsset asset : assetList) {
                asset.prepare(token.getCompanyId());
            }

            shipmentBill.setAssets(assetList);
        }

        Set<ObjectId> productIds = new HashSet<>();
        BigDecimal totalCost = BigDecimal.ZERO;
        BigDecimal totalDiscount = BigDecimal.ZERO;
        if (request.getPoProductRequestList() != null) {
            List<POProductRequest> poProductRequestList = new ArrayList<>();
            for (POProductRequest poProductRequest : request.getPoProductRequestList()) {
                if (POProductRequest.RequestStatus.ACCEPTED == poProductRequest.getRequestStatus()) {
                    productIds.add(new ObjectId(poProductRequest.getProductId()));
                    poProductRequest.prepare(token.getCompanyId());


                    // recalculate unit price of the origal cost
                    double unitPrice = poProductRequest.getUnitPrice().doubleValue();
                    BigDecimal dbProductTotalCost = poProductRequest.getTotalCost();
                    if (dbProductTotalCost != null && poProductRequest.getRequestQuantity() != null && !poProductRequest.getRequestQuantity().equals(BigDecimal.ZERO)) {
                        unitPrice = dbProductTotalCost.doubleValue() / poProductRequest.getRequestQuantity().doubleValue();
                    }

                    BigDecimal productCost = new BigDecimal(poProductRequest.getReceivedQuantity().doubleValue() * unitPrice);

                    BigDecimal finalTotalCost = BigDecimal.ZERO;
                    BigDecimal discount = poProductRequest.getDiscount() == null ? BigDecimal.ZERO : poProductRequest.getDiscount();

                    poProductRequest.setTotalCost(productCost);

                    finalTotalCost = productCost.subtract(discount);
                    totalDiscount = totalDiscount.add(discount);

                    poProductRequest.setFinalTotalCost(finalTotalCost);

                    totalCost = new BigDecimal(totalCost.doubleValue() + finalTotalCost.doubleValue());
                    if (poProductRequest.getTrackingPackagesList() != null && poProductRequest.getTrackingPackagesList().size() > 0) {
                        for (TrackingPackages trackingPackages : poProductRequest.getTrackingPackagesList()) {
                            trackingPackages.prepare(token.getCompanyId());
                        }
                    }
                    poProductRequestList.add(poProductRequest);
                }
            }

            shipmentBill.setPoProductRequest(poProductRequestList);
        }

        shipmentBill.setTotalCost(totalCost);

        ArrayList<ObjectId> arrayList = Lists.newArrayList(productIds);
        HashMap<String, Product> productMap = new HashMap<>();
        List<ObjectId> productCategoryIds = new ArrayList<>();

        HashMap<String, Product> productHashMap = productRepository.listAsMap(token.getCompanyId(), arrayList);

        if (productHashMap == null) {
            throw new BlazeInvalidArgException("Product", "Product is not found");
        }

        for (Product product : productHashMap.values()) {
            productMap.put(product.getId(), product);
            productCategoryIds.add(new ObjectId(product.getCategoryId()));
        }

        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());
        if (shop == null) {
            throw new BlazeInvalidArgException("Shop", "Shop is not found");
        }

        Vendor vendor = vendorRepository.getById(purchaseOrder.getVendorId());
        if (vendor == null) {
            throw new BlazeInvalidArgException("Vendor", "Vendor not found");
        }

        TaxResult taxResult = null;
        if (Vendor.ArmsLengthType.ARMS_LENGTH.equals(purchaseOrder.getTransactionType()) && shop.isRetail()) {
            taxResult = purchaseOrderService.calculateArmsLengthExciseTax(shop, shipmentBill.getPoProductRequest(), productMap, productCategoryIds, false);
        }

        CompanyLicense companyLicense = vendor.getCompanyLicense(purchaseOrder.getLicenseId());
        if ((Vendor.CompanyType.CULTIVATOR.equals(companyLicense.getCompanyType()) || Vendor.CompanyType.MANUFACTURER.equals(companyLicense.getCompanyType())) && PurchaseOrder.FlowerSourceType.CULTIVATOR_DIRECT == purchaseOrder.getFlowerSourceType()) {
            taxResult = purchaseOrderService.calculateCultivationTax(taxResult, shipmentBill.getPoProductRequest(), purchaseOrder, productMap, productCategoryIds);
        }
        shipmentBill.setTaxResult(taxResult);

        BigDecimal grandTotal = new BigDecimal(0);
        grandTotal = grandTotal.add(shipmentBill.getTotalCost());
        if (shipmentBill.getTaxResult() != null) {
            grandTotal = grandTotal.add(shipmentBill.getTaxResult().getTotalExciseTax());
            if (taxResult != null && taxResult.getCultivationTaxResult() != null && taxResult.getCultivationTaxResult().getTotalCultivationTax() != null) {
                grandTotal = grandTotal.add(taxResult.getCultivationTaxResult().getTotalCultivationTax());
            }
        }
        totalDiscount = totalDiscount.add(purchaseOrder.getDiscount());
        shipmentBill.setDiscount(totalDiscount);
        shipmentBill.setFees(purchaseOrder.getFees());

        grandTotal = grandTotal.add(purchaseOrder.getFees());
        grandTotal = grandTotal.subtract(purchaseOrder.getDiscount());
        shipmentBill.setGrandTotal(grandTotal);

        shipmentBill.setShipmentBillNumber(createShipmentNumber(purchaseOrder.getPoNumber()));
        shipmentBill.setReceivedDate(request.getReceivedDate() != 0 ? request.getReceivedDate() : purchaseOrder.getReceivedDate());
        shipmentBill.setDeliveredBy(request.getDeliveredBy());
        shipmentBill.prepare(token.getCompanyId());
        shipmentBill.setShopId(token.getShopId());
        shipmentBill.setCreatedByEmployeeId(token.getActiveTopUser().getUserId());
        shipmentBill.setCompletedByEmployeeId(token.getActiveTopUser().getUserId());
        shipmentBill.setNotes(request.getNotes());
        shipmentBill.setReference(purchaseOrder.getReference());

        BigDecimal amountPaid = calculateAmountPaid(shipmentBill.getPaymentHistory());

        shipmentBill.setAmountPaid(amountPaid);

        shipmentBill.setReceivedByEmployeeId(request.getReceivedByEmployeeId());
        shipmentBill.setCompletedDate(DateTime.now().getMillis());
        shipmentBill.setCustomerType(request.getCustomerType());

        BigDecimal processedGrandTotal = purchaseOrderService.processAdjustments(purchaseOrder.getAdjustmentInfoList(), shipmentBill.getGrandTotal());
        BigDecimal adjustmentAmount = (shipmentBill.getGrandTotal().subtract(processedGrandTotal)).multiply(new BigDecimal(-1));
        shipmentBill.setAdjustmentAmount(adjustmentAmount);
        shipmentBill.setGrandTotal(processedGrandTotal);

        if (StringUtils.isBlank(purchaseOrder.getVendorId())) {
            throw new BlazeInvalidArgException("Vendor", "Vendor can't be empty");
        }

        shipmentBill.setVendorId(purchaseOrder.getVendorId());

        shipmentBill.setTermsAndCondition(request.getTermsAndCondition());

        if (shipmentBill.getGrandTotal().doubleValue() <= 0) {
            shipmentBill.setPaymentStatus(ShipmentBill.PaymentStatus.PAID);
        }
        dbShipmentBill = shipmentBillRepository.save(shipmentBill);
        return dbShipmentBill;

    }

    /**
     * Create bill number from PO number
     *
     * @param fromPONumber : PO number
     */
    private String createShipmentNumber(String fromPONumber) {
        return fromPONumber + "-SB";
    }

    private BigDecimal calculateAmountPaid(List<ShipmentBillPayment> paymentList) {
        BigDecimal amountPaid = BigDecimal.ZERO;

        if (paymentList != null && !paymentList.isEmpty()) {
            for (ShipmentBillPayment shipmentBillPayment : paymentList) {
                if (!shipmentBillPayment.isDeleted()) {
                    amountPaid = amountPaid.add(shipmentBillPayment.getAmountPaid());
                }
            }
        }

        return amountPaid;
    }

    /**
     * updateShipmentBill is used to update the all information of shipment bill
     * using these parameters.
     *
     * @param shipmentBillId : shipment bill id
     * @param shipmentBill   : shipment bill's object
     */
    @Override
    public ShipmentBill updateShipmentBill(final String shipmentBillId, ShipmentBill shipmentBill) {

        ShipmentBill dbShipmentBill1 = null;

        PurchaseOrder purchaseOrder = purchaseOrderRepository.get(token.getCompanyId(), shipmentBill.getPurchaseOrderId());
        if (purchaseOrder == null) {
            throw new BlazeInvalidArgException(SHIPMENT_BILL, PO_NOT_FOUND);
        }
        Vendor vendor = vendorRepository.getById(shipmentBill.getVendorId());
        if (vendor == null) {
            throw new BlazeInvalidArgException("Vendor", "Vendor does not exist");
        }

        final ShipmentBill shipmentBillById = shipmentBillRepository.get(token.getCompanyId(), shipmentBillId);
        if (shipmentBillById != null) {
            List<ShipmentBillPayment> shipmentBillPayments = filterPaymentHistory(shipmentBillById.getPaymentHistory(), true);

            // incoming payment status
            shipmentBillById.setPaymentStatus(shipmentBill.getPaymentStatus());

            if (shipmentBill.getPaymentHistory() != null) {
                for (ShipmentBillPayment shipmentBillPayment : shipmentBill.getPaymentHistory()) {
                    shipmentBillPayment.prepare();
                    shipmentBillPayments.add(shipmentBillPayment);
                }
                shipmentBillById.setPaymentHistory(shipmentBillPayments);
            }

            Set<ObjectId> productIds = new HashSet<>();
            BigDecimal totalCost = BigDecimal.ZERO;
            BigDecimal finalTotalCost = BigDecimal.ZERO;
            BigDecimal totalDiscount = BigDecimal.ZERO;
            if (shipmentBill.getPoProductRequest() != null) {
                List<POProductRequest> poProductRequestList = new ArrayList<>();
                for (POProductRequest poProductRequest : shipmentBill.getPoProductRequest()) {
                    productIds.add(new ObjectId(poProductRequest.getProductId()));
                    BigDecimal productCost = new BigDecimal(poProductRequest.getReceivedQuantity().doubleValue() * poProductRequest.getUnitPrice().doubleValue());
                    BigDecimal discount = poProductRequest.getDiscount() == null ? BigDecimal.ZERO : poProductRequest.getDiscount();

                    totalCost = new BigDecimal(totalCost.doubleValue() + productCost.doubleValue());
                    finalTotalCost = totalCost.subtract(discount);
                    totalDiscount = totalDiscount.add(discount);

                    poProductRequest.setTotalCost(productCost);
                    poProductRequest.setFinalTotalCost(finalTotalCost);
                    poProductRequest.prepare(token.getCompanyId());
                    poProductRequestList.add(poProductRequest);

                    for (TrackingPackages trackingPackages : poProductRequest.getTrackingPackagesList()) {
                        trackingPackages.prepare(token.getCompanyId());
                    }
                }

                shipmentBill.setPoProductRequest(poProductRequestList);
            }
            shipmentBillById.setCreatedBy(shipmentBill.getCreatedBy());
            shipmentBillById.setAssets(shipmentBill.getAssets());
            shipmentBillById.setPoProductRequest(shipmentBill.getPoProductRequest());
            shipmentBillById.setReceivedDate(shipmentBill.getReceivedDate());
            shipmentBillById.setDeliveredBy(shipmentBill.getDeliveredBy());
            shipmentBillById.setNotes(shipmentBill.getNotes());

            BigDecimal amountPaid = calculateAmountPaid(shipmentBillById.getPaymentHistory());


            ArrayList<ObjectId> arrayList = Lists.newArrayList(productIds);
            HashMap<String, Product> productMap = new HashMap<>();
            List<ObjectId> productCategoryIds = new ArrayList<>();

            HashMap<String, Product> productHashMap = productRepository.listAsMap(token.getCompanyId(), arrayList);

            if (productHashMap == null) {
                throw new BlazeInvalidArgException("Product", "Product is not found");
            }

            for (Product product : productHashMap.values()) {
                productMap.put(product.getId(), product);
                productCategoryIds.add(new ObjectId(product.getCategoryId()));
            }

            Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());
            if (shop == null) {
                throw new BlazeInvalidArgException("Shop", "Shop is not found");
            }

            TaxResult taxResult = null;
            if (Vendor.ArmsLengthType.ARMS_LENGTH.equals(purchaseOrder.getTransactionType()) && shop.isRetail()) {
                taxResult = purchaseOrderService.calculateArmsLengthExciseTax(shop, shipmentBill.getPoProductRequest(), productMap, productCategoryIds, false);
            }

            CompanyLicense companyLicense = vendor.getCompanyLicense(purchaseOrder.getLicenseId());
            if ((Vendor.CompanyType.CULTIVATOR.equals(companyLicense.getCompanyType()) || Vendor.CompanyType.MANUFACTURER.equals(companyLicense.getCompanyType())) && PurchaseOrder.FlowerSourceType.CULTIVATOR_DIRECT == purchaseOrder.getFlowerSourceType()) {
                taxResult = purchaseOrderService.calculateCultivationTax(taxResult, shipmentBill.getPoProductRequest(), purchaseOrder, productMap, productCategoryIds);
            }

            shipmentBillById.setTaxResult(taxResult);

            totalDiscount = totalDiscount.add(shipmentBill.getDiscount());
            shipmentBillById.setDiscount(totalDiscount);
            shipmentBillById.setFees(shipmentBill.getFees());

            BigDecimal grandTotal = new BigDecimal(0);
            grandTotal = grandTotal.add(shipmentBill.getTotalCost());
            grandTotal = grandTotal.add(shipmentBill.getFees());
            grandTotal = grandTotal.subtract(shipmentBill.getDiscount());
            if (shipmentBillById.getTaxResult() != null) {
                grandTotal = grandTotal.add(shipmentBillById.getTaxResult().getTotalExciseTax());
            }
            shipmentBillById.setGrandTotal(grandTotal);

            shipmentBillById.setAmountPaid(amountPaid);
            shipmentBillById.setTotalCost(finalTotalCost);

            shipmentBillById.setReceivedByEmployeeId(shipmentBill.getReceivedByEmployeeId());
            shipmentBillById.setCompletedDate(shipmentBill.getCompletedDate());
            shipmentBillById.setCustomerType(shipmentBill.getCustomerType());

            shipmentBillById.setVendorId(shipmentBill.getVendorId());

            shipmentBillById.setTermsAndCondition(shipmentBill.getTermsAndCondition());
            //process shipment bill
            BigDecimal processedGrandTotal = purchaseOrderService.processAdjustments(purchaseOrder.getAdjustmentInfoList(), shipmentBillById.getGrandTotal());
            BigDecimal adjustmentAmount = (shipmentBillById.getGrandTotal().subtract(processedGrandTotal)).multiply(new BigDecimal(-1));
            shipmentBillById.setAdjustmentAmount(adjustmentAmount);
            shipmentBillById.setGrandTotal(processedGrandTotal);
            if (shipmentBillById.getGrandTotal().doubleValue() <= 0) {
                shipmentBillById.setPaymentStatus(ShipmentBill.PaymentStatus.PAID);
            }
            dbShipmentBill1 = shipmentBillRepository.update(shipmentBillId, shipmentBillById);
            dbShipmentBill1.setPaymentHistory(filterPaymentHistory(dbShipmentBill1.getPaymentHistory(), false));
        } else {
            throw new BlazeInvalidArgException(SHIPMENT_BILL, BILL_NOT_FOUND);
        }
        return dbShipmentBill1;
    }

    /**
     * It updates payment in shipment bill
     *
     * @param shipmentBillId : shipment bill id
     * @param request        : ShipmentBillPayment's object with required information
     */
    @Override
    public ShipmentBill addShipmentBillPayment(String shipmentBillId, ShipmentBillPaymentStatusRequest request) {
        ShipmentBill dbShipmentBill;

        ShipmentBill shipmentBill = shipmentBillRepository.get(token.getCompanyId(), shipmentBillId);
        if (shipmentBill != null) {
            if (request.getAmountPaid() == null || request.getAmountPaid().doubleValue() <= 0) {
                throw new BlazeInvalidArgException(SHIPMENT_BILL, AMOUNT_NOT_ZERO);
            }
            if (request.getPaymentType() == null) {
                throw new BlazeInvalidArgException(SHIPMENT_BILL, PAYMENT_TYPE_NOT_FOUND);
            }
            PurchaseOrder purchaseOrder = purchaseOrderRepository.get(token.getCompanyId(), shipmentBill.getPurchaseOrderId());
            if (purchaseOrder == null) {
                throw new BlazeInvalidArgException(SHIPMENT_BILL, PO_NOT_FOUND);
            }
            List<ShipmentBillPayment> paymentHistory = shipmentBill.getPaymentHistory();
            if (paymentHistory == null) {
                paymentHistory = new ArrayList<>();
            }

            int count = paymentHistory.size();
            ShipmentBillPayment shipmentBillPayment = new ShipmentBillPayment();
            shipmentBillPayment.prepare(token.getCompanyId());
            shipmentBillPayment.setShopId(token.getShopId());
            shipmentBillPayment.setNotes(request.getNotes());
            shipmentBillPayment.setAmountPaid(request.getAmountPaid());
            shipmentBillPayment.setPaidDate((request.getPaidDate() == 0) ? DateTime.now().getMillis() : request.getPaidDate());
            shipmentBillPayment.setPaymentType(request.getPaymentType());
            shipmentBillPayment.setReferenceNo(request.getReferenceNo());

            // For quickBook need a reference no
            String paymentNumber = purchaseOrder.getPoNumber() + "-" + (++count) + "P";
            if (paymentNumber.length() > 11) {
                int length = paymentNumber.length();
                paymentNumber = paymentNumber.substring((length - 11), length);
            }

            shipmentBillPayment.setPaymentNo(paymentNumber);

            paymentHistory.add(shipmentBillPayment);

            shipmentBill.setPaymentHistory(paymentHistory);

            BigDecimal amountPaid = calculateAmountPaid(shipmentBill.getPaymentHistory());

            shipmentBill.setAmountPaid(amountPaid);

            getShipmentBillStatus(shipmentBill, amountPaid);

            dbShipmentBill = shipmentBillRepository.update(token.getCompanyId(), shipmentBillId, shipmentBill);

            dbShipmentBill.setPaymentHistory(filterPaymentHistory(dbShipmentBill.getPaymentHistory(), false));

            Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());
            if (shop != null && !shop.isRetail()) {
                if (dbShipmentBill.getPaymentStatus() == ShipmentBill.PaymentStatus.PAID) {
                    purchaseOrder.setPurchaseOrderPreviousStatus(purchaseOrder.getPurchaseOrderStatus());
                    purchaseOrder.setPurchaseOrderStatus(PurchaseOrder.PurchaseOrderStatus.Closed);
                }
            }
            purchaseOrder.setAmountPaid(amountPaid);

            purchaseOrderRepository.update(token.getCompanyId(), purchaseOrder.getId(), purchaseOrder);

            if (shop != null && !shop.isRetail() && purchaseOrder.getPurchaseOrderStatus() == PurchaseOrder.PurchaseOrderStatus.Closed) {
                poActivityService.addPOActivity(purchaseOrder.getId(), token.getActiveTopUser().getUserId(), "PO completed");
            }

        } else {
            throw new BlazeInvalidArgException(SHIPMENT_BILL, BILL_NOT_FOUND);
        }

        return dbShipmentBill;
    }

    /**
     * Get all shipment bill by company and shop id
     *
     * @param start : start number
     * @param limit : limit of search
     */
    @Override
    public SearchResult<ShipmentBillRequestResult> getAllShipmentBills(int start, int limit, PurchaseOrder.CustomerType customerType) {

        customerType = (customerType == null) ? PurchaseOrder.CustomerType.VENDOR : customerType;

        final SearchResult<ShipmentBillRequestResult> allShipmentBills = shipmentBillRepository.getAllShipmentBills(token.getCompanyId(), token.getShopId(), "{created:-1}", start, limit, customerType);
        prepareSearchRequestResult(allShipmentBills);
        return allShipmentBills;
    }

    private void prepareSearchRequestResult(SearchResult<ShipmentBillRequestResult> requestResultSearchResult) {
        if (requestResultSearchResult.getValues() != null) {
            for (ShipmentBillRequestResult requestResult : requestResultSearchResult.getValues()) {
                Vendor vendor = vendorRepository.getById(requestResult.getVendorId());
                PurchaseOrder purchaseOrder = purchaseOrderRepository.get(token.getCompanyId(), requestResult.getPurchaseOrderId());
                requestResult.setVendor(vendor);
                requestResult.setPurchaseOrder(purchaseOrder);
                List<ShipmentBillPayment> shipmentBillPayments = requestResult.getPaymentHistory();
                requestResult.setPaymentHistory(filterPaymentHistory(shipmentBillPayments, false));
            }
        }
    }

    /**
     * This method gets archived shipment bill list
     *
     * @param start : start
     * @param limit : limit
     */
    @Override
    public SearchResult<ShipmentBillRequestResult> getArchivedShipmentBills(int start, int limit) {
        SearchResult<ShipmentBillRequestResult> searchResult = shipmentBillRepository.getArchivedShipmentBills(token.getCompanyId(), token.getShopId(), "{archiveDate:-1}", start, limit);
        prepareSearchRequestResult(searchResult);
        return searchResult;
    }

    /**
     * This method mark shipment bill as archived
     *
     * @param shipmentBillId : shipment bill id
     */
    @Override
    public ShipmentBill archiveShipmentBill(String shipmentBillId) {
        ShipmentBill updatedShipmentBill;
        ShipmentBill dbShipmentBill = shipmentBillRepository.get(token.getCompanyId(), shipmentBillId);
        if (dbShipmentBill == null) {
            throw new BlazeInvalidArgException(SHIPMENT_BILL, BILL_NOT_FOUND);
        }

        dbShipmentBill.setArchive(true);
        dbShipmentBill.setArchiveDate(DateTime.now().getMillis());
        updatedShipmentBill = shipmentBillRepository.update(token.getCompanyId(), shipmentBillId, dbShipmentBill);
        updatedShipmentBill.setPaymentHistory(filterPaymentHistory(updatedShipmentBill.getPaymentHistory(), false));
        return updatedShipmentBill;
    }

    /**
     * This method email shipment bill to provided email address
     *
     * @param shipmentBillId : shipment bill id
     * @param emailRequest   : email id
     */
    @Override
    public void emailShipmentBillToAccounting(String shipmentBillId, EmailRequest emailRequest) {
        ShipmentBill dbShipmentBill = shipmentBillRepository.get(token.getCompanyId(), shipmentBillId);
        if (dbShipmentBill == null) {
            throw new BlazeInvalidArgException(SHIPMENT_BILL, BILL_NOT_FOUND);
        }

        PurchaseOrder purchaseOrder = purchaseOrderRepository.get(token.getCompanyId(), dbShipmentBill.getPurchaseOrderId());
        if (purchaseOrder == null) {
            throw new BlazeInvalidArgException("Purchase Order", PO_NOT_FOUND);
        }

        final Company company = companyRepository.getById(purchaseOrder.getCompanyId());
        if (company == null) {
            throw new BlazeInvalidArgException("Company", "Company not found");
        }

        Shop shop = shopRepository.getById(purchaseOrder.getShopId());
        String emailBody = createShipmentBillEmailBody(company, purchaseOrder, dbShipmentBill, shop);

        String fromEmail = (shop != null && StringUtils.isNotBlank(shop.getEmailAdress())) ? shop.getEmailAdress() : "support@blaze.me";

        amazonServiceManager.sendEmail(fromEmail, emailRequest.getEmail(), connectConfiguration.getAppName(), emailBody, null, shop.getEmailAdress(), shop.getName());
    }

    /**
     * It created shipment bill's email body
     *
     * @param company       : company object
     * @param purchaseOrder : purchase order
     * @param shipmentBill  : shipment bill
     * @param shop
     */
    private String createShipmentBillEmailBody(Company company, PurchaseOrder purchaseOrder, ShipmentBill shipmentBill, Shop shop) {
        try {
            String shipmentBillBody = "";

            InputStream inputStream = PurchaseOrderServiceImpl.class.getResourceAsStream("/shipmentBill.html");
            StringWriter writer = new StringWriter();
            try {
                IOUtils.copy(inputStream, writer, "UTF-8");
            } catch (IOException e) {
                LOGGER.error("Error in emailAccountingBody : " + e);
            }

            shipmentBillBody = writer.toString();
            Vendor vendor = vendorRepository.getById(purchaseOrder.getVendorId());
            String vendorName = getVendorSection(vendor.getName());
            shipmentBillBody = shipmentBillBody.replaceAll("==customerName==", vendorName);

            String timeZone = token.getRequestTimeZone();
           if (shop != null && StringUtils.isBlank(timeZone)) {
                timeZone = shop.getTimeZone();
            }
            Employee createByEmployee = employeeService.getEmployeeById(shipmentBill.getCreatedBy());
            if(shipmentBill.getGrandTotal().setScale(2, RoundingMode.HALF_EVEN).compareTo(shipmentBill.getAmountPaid().setScale(2,RoundingMode.HALF_EVEN)) > 0 && shipmentBill.getAmountPaid().doubleValue() > 0){
                shipmentBillBody = shipmentBillBody.replaceAll("==paymentStatus==", "Partial paid");
            } else {
                shipmentBillBody = shipmentBillBody.replaceAll("==paymentStatus==", shipmentBill.getPaymentStatus().toString());
            }
            shipmentBillBody = shipmentBillBody.replaceAll("==createdBy==", createByEmployee == null ? "" : createByEmployee.getFirstName() + " " + createByEmployee.getLastName());
            shipmentBillBody = shipmentBillBody.replaceAll("==receivedDate==", purchaseOrder.getReceivedDate() == null ? "" : TextUtil.toDateTime(purchaseOrder.getReceivedDate(), "MM/dd/yyyy hh:mm:ss a"));
            shipmentBillBody = shipmentBillBody.replaceAll("==deliveredBy==", shipmentBill.getDeliveredBy() == null ? "" : shipmentBill.getDeliveredBy());
            shipmentBillBody = shipmentBillBody.replaceAll("==completedDate==", TextUtil.toDateTime(purchaseOrder.getCompletedDate()));
            shipmentBillBody = shipmentBillBody.replaceAll("==billNumber==", shipmentBill.getShipmentBillNumber());
            shipmentBillBody = shipmentBillBody.replaceAll("==totalCost==", TextUtil.formatToTwoDecimalPoints(shipmentBill.getGrandTotal().doubleValue()));
            shipmentBillBody = shipmentBillBody.replaceAll("==subTotal==", TextUtil.formatToTwoDecimalPoints(shipmentBill.getTotalCost().doubleValue()));
            shipmentBillBody = shipmentBillBody.replaceAll("==poDate==", purchaseOrder.getPurchaseOrderDate() > 0 ? DateUtil.toDateTimeFormatted(purchaseOrder.getPurchaseOrderDate(), timeZone, "MM/dd/yyyy") : "N/A");
            shipmentBillBody = shipmentBillBody.replaceAll("==deliveryDate==", purchaseOrder.getDeliveryDate() > 0 ? DateUtil.toDateTimeFormatted(purchaseOrder.getDeliveryDate(), timeZone, "MM/dd/yyyy hh:mm a") : "N/A");

            StringBuilder paymentHistorySection = null;
            if (shipmentBill.getPaymentHistory() != null) {
                paymentHistorySection = new StringBuilder();
                int index = 0;
                for (ShipmentBillPayment shipmentBillPayment : shipmentBill.getPaymentHistory()) {
                    if (shipmentBillPayment.isDeleted()) {
                        continue;
                    }
                    try {
                        paymentHistorySection.append(preparePaymentHistorySection(++index, shipmentBillPayment));
                    } catch (Exception e) {
                        LOGGER.error("Exception while creating payment history section for email of shipment bill");
                    }
                }
            }
            shipmentBillBody = shipmentBillBody.replaceAll("==paymentHistory==", paymentHistorySection == null ? "" : Matcher.quoteReplacement(paymentHistorySection.toString()));
            shipmentBillBody = shipmentBillBody.replaceAll("==showPaymentHistory==", (paymentHistorySection == null) ? "display:none;" : "");

            boolean isCannabis = Boolean.FALSE;

            if (purchaseOrder.getPoProductRequestList() != null) {
                List<ObjectId> ids = new ArrayList<>();
                for (POProductRequest poProductRequest : purchaseOrder.getPoProductRequestList()) {
                    if (poProductRequest.getProductId() != null && ObjectId.isValid(poProductRequest.getProductId())) {
                        ids.add(new ObjectId(poProductRequest.getProductId()));
                    }

                }
                Iterable<Product> productList = productRepository.findProductsByIds(purchaseOrder.getCompanyId(), purchaseOrder.getShopId(), ids);
                for (Product product : productList) {
                    ProductCategory productCategory = product.getCategory();
                    if ((product.getCannabisType() == Product.CannabisType.DEFAULT && productCategory.isCannabis())
                            || (product.getCannabisType() != Product.CannabisType.CBD
                            && product.getCannabisType() != Product.CannabisType.NON_CANNABIS
                            && product.getCannabisType() != Product.CannabisType.DEFAULT)) {
                        isCannabis = Boolean.TRUE;
                    }
                }
            }

            if (purchaseOrder.getFees() != null && purchaseOrder.getFees().doubleValue() == 0) {
                shipmentBillBody = shipmentBillBody.replaceAll("==showFees==", TextUtil.textOrEmpty("display:none"));
            } else {
                shipmentBillBody = shipmentBillBody.replaceAll("==showFees==", TextUtil.textOrEmpty(""));
                shipmentBillBody = shipmentBillBody.replaceAll("==fees==", TextUtil.formatToTwoDecimalPoints(purchaseOrder.getFees()));
            }

            if (purchaseOrder.getDiscount() != null && purchaseOrder.getDiscount().doubleValue() == 0) {
                shipmentBillBody = shipmentBillBody.replaceAll("==showDiscount==", TextUtil.textOrEmpty("display:none"));
            } else {
                shipmentBillBody = shipmentBillBody.replaceAll("==showDiscount==", TextUtil.textOrEmpty(""));
                shipmentBillBody = shipmentBillBody.replaceAll("==discount==", TextUtil.formatToTwoDecimalPoints(purchaseOrder.getDiscount()));
            }

            boolean showExciseTax = Boolean.FALSE;
            if (shop.isEnableExciseTax() && shipmentBill.getTaxResult() != null && shipmentBill.getTaxResult().getTotalExciseTax().doubleValue() != 0) {
                if (isCannabis) { //
                    shipmentBillBody = shipmentBillBody.replaceAll("==exciseTaxMessage==", TextUtil.textOrEmpty("The cannabis excise taxes are included in the total amount of this invoice."));
                } else {
                    shipmentBillBody = shipmentBillBody.replaceAll("==showExciseTaxMessage==", TextUtil.textOrEmpty("display:none"));

                }
                if (ExciseTaxInfo.ExciseTaxType.PER_ITEM.equals(shop.getExciseTaxType())) {
                    showExciseTax = Boolean.TRUE;
                    shipmentBillBody = shipmentBillBody.replaceAll("==exciseTax==", TextUtil.toEscapeCurrency(shipmentBill.getTaxResult().getTotalExciseTax().doubleValue(), shop.getDefaultCountry()));
                } else {
                    shipmentBillBody = shipmentBillBody.replaceAll("==showExciseTax==", TextUtil.textOrEmpty("display:none"));
                }
            } else {
                shipmentBillBody = shipmentBillBody.replaceAll("==showExciseTax==", TextUtil.textOrEmpty("display:none"));
                shipmentBillBody = shipmentBillBody.replaceAll("==showExciseTaxMessage==", TextUtil.textOrEmpty("display:none"));
            }

            StringBuilder poProductRequestSection = null;
            StringBuilder trackPackageSection = null;
            if (purchaseOrder.getPoProductRequestList() != null) {
                poProductRequestSection = new StringBuilder();
                trackPackageSection = new StringBuilder();
                int index = 0;
                int trackIndex = 0;
                if (showExciseTax) {

                    for (POProductRequest poProductRequest : shipmentBill.getPoProductRequest()) {
                        try {
                            Product product = productRepository.get(token.getCompanyId(), poProductRequest.getProductId());
                            poProductRequestSection.append(preparePOCannabisProductRequest(++index, poProductRequest, product));
                            if (poProductRequest.getTrackingPackagesList() != null) {
                                for (TrackingPackages trackingPackages : poProductRequest.getTrackingPackagesList()) {
                                    trackPackageSection.append(prepareTrackPackagesSection(++trackIndex, trackingPackages, product));
                                }
                            }
                        } catch (Exception e) {
                            LOGGER.error("Exception while creating product request section for email of shipment bill");
                        }
                    }

                    shipmentBillBody = shipmentBillBody.replaceAll("==showNonCannabisProduct==", TextUtil.textOrEmpty("display:none"));
                } else {
                    for (POProductRequest poProductRequest : purchaseOrder.getPoProductRequestList()) {
                        try {
                            Product product = productRepository.get(token.getCompanyId(), poProductRequest.getProductId());
                            poProductRequestSection.append(preparePOProductRequestSection(++index, poProductRequest, product));
                            if (poProductRequest.getTrackingPackagesList() != null) {
                                for (TrackingPackages trackingPackages : poProductRequest.getTrackingPackagesList()) {
                                    trackPackageSection.append(prepareTrackPackagesSection(++trackIndex, trackingPackages, product));
                                }
                            }
                        } catch (Exception e) {
                            LOGGER.error("Exception while creating product request section for email of shipment bill");
                        }
                    }
                    shipmentBillBody = shipmentBillBody.replaceAll("==showCannabisProduct==", TextUtil.textOrEmpty("display:none"));
                }
            }

            shipmentBillBody = shipmentBillBody.replaceAll("==productRequest==", poProductRequestSection == null ? "" : Matcher.quoteReplacement(poProductRequestSection.toString()));
            shipmentBillBody = shipmentBillBody.replaceAll("==trackPackages==", trackPackageSection == null ? "" : Matcher.quoteReplacement(trackPackageSection.toString()));
            shipmentBillBody = shipmentBillBody.replaceAll("==showTrackPackages==", (trackPackageSection == null || StringUtils.isBlank(trackPackageSection.toString())) ? "display:none;" : "");

            shipmentBillBody = shipmentBillBody.replaceAll("==companyName==", (shop != null && StringUtils.isNotBlank(shop.getName())) ? shop.getName() : "");
            shipmentBillBody = shipmentBillBody.replaceAll("==companyEmail==", (shop != null && StringUtils.isNotBlank(shop.getEmailAdress())) ? shop.getEmailAdress() : "");
            shipmentBillBody = shipmentBillBody.replaceAll("==companyPhone==", (shop != null && StringUtils.isNotBlank(shop.getPhoneNumber())) ? shop.getPhoneNumber() : "");
            shipmentBillBody = shipmentBillBody.replaceAll("==preferredEmailColor==", company.getPreferredEmailColor());

            String logoURL = (shop != null && shop.getLogo() != null) ? shop.getLogo().getPublicURL() : "https://connect-assets.s3.amazonaws.com/email/logo.jpg";
            shipmentBillBody = shipmentBillBody.replaceAll("==logo==", logoURL);

            if (CollectionUtils.isEmpty(purchaseOrder.getAdjustmentInfoList())) {
                shipmentBillBody = shipmentBillBody.replaceAll("==showAdjustments==", TextUtil.textOrEmpty("display:none"));
            } else {
                List<ObjectId> objectIds = new ArrayList<>();
                for (AdjustmentInfo adjustmentInfo : purchaseOrder.getAdjustmentInfoList()) {
                    objectIds.add(new ObjectId(adjustmentInfo.getAdjustmentId()));
                }
                HashMap<String, Adjustment> infoHashMap = adjustmentRepository.listAsMap(token.getCompanyId(), objectIds);
                StringBuilder adjustmentInfoStr = new StringBuilder();

                for (AdjustmentInfo adjustmentInfo : purchaseOrder.getAdjustmentInfoList()) {
                    Adjustment adjustment = infoHashMap.get(adjustmentInfo.getAdjustmentId());
                    if (adjustment == null) {
                        continue;
                    }
                    String color = adjustmentInfo.isNegative() ? "red" : "black";
                    String amountStr = TextUtil.toEscapeCurrency(adjustmentInfo.getAmount().doubleValue(), shop.getDefaultCountry());
                    amountStr = adjustmentInfo.isNegative() ? "- {" + amountStr + "}" : amountStr;

                    adjustmentInfoStr.append("<tr><td width=\"330\" height=\"30\">" + Matcher.quoteReplacement(adjustment.getName()) + "</td>" +
                            "<td width=\"210\" height=\"30\" style =\"color:" + color + "\"><sup style=\"font-size:0.6em;\"></sup>" + amountStr + "</td></tr>");

                }
                shipmentBillBody = shipmentBillBody.replaceAll("==adjustmentInfo==", TextUtil.textOrEmpty(adjustmentInfoStr.toString()));
                shipmentBillBody = shipmentBillBody.replaceAll("==showAdjustments==", TextUtil.textOrEmpty(""));

            }
            return shipmentBillBody;
        } catch (Exception e) {
            LOGGER.error("Exception in email of shipment bill to accounting", e);
            return "Information is not completed";
        }
    }

    private String preparePOCannabisProductRequest(int index, POProductRequest poProductRequest, Product product) {
        String requestQuantity = poProductRequest.getRequestQuantity() + " " + (product == null ? "" : product.getCategory().getUnitType().toString());
        String receivedQuantity = poProductRequest.getReceivedQuantity() + " " + (product == null ? "" : product.getCategory().getUnitType().toString());

        String notes = poProductRequest.getNotes() == null ? "" : poProductRequest.getNotes();
        String trackTraceSystem = poProductRequest.getTrackTraceSystem() == null ? "" : poProductRequest.getTrackTraceSystem().toString();
        return "<tr>\n" +
                "<td width=\"50\" height=\"30\" class=\"col-1\">" + index + "</td>\n" +
                "<td width=\"330\" height=\"30\" class=\"col\">" + product.getName() + "</td>\n" +
                "<td width=\"330\" height=\"30\" class=\"col\">" + requestQuantity + "</td>\n" +
                "<td width=\"330\" height=\"30\" class=\"col\">" + receivedQuantity + "</td>\n" +
                "<td width=\"330\" height=\"30\" class=\"col\">$" + poProductRequest.getUnitPrice() + "</td>\n" +
                "<td width=\"330\" height=\"30\" class=\"col\">$" + poProductRequest.getTotalExciseTax() + "</td>\n" +
                "<td width=\"330\" height=\"30\" class=\"col\">" + poProductRequest.getRequestStatus() + "</td>\n" +
                "<td width=\"330\" height=\"30\" class=\"col\">" + trackTraceSystem + "</td>\n" +
                "<td width=\"330\" height=\"30\" class=\"col\">" + notes + "</td>\n" +
                "</tr>";
    }

    private String prepareTrackPackagesSection(int index, TrackingPackages trackingPackages, Product product) {
        String quantity = trackingPackages.getQuantity() + " " + (product == null ? "" : product.getCategory().getUnitType().toString());
        return "<tr>\n" +
                "<td width=\"50\" height=\"30\" class=\"col-1\">" + index + "</td>\n" +
                "<td width=\"330\" height=\"30\" class=\"col\">" + trackingPackages.getPackageLabel() + "</td>\n" +
                "<td width=\"330\" height=\"30\" class=\"col\">" + quantity + "</td>\n" +
                "</tr>";
    }

    private String preparePOProductRequestSection(int index, POProductRequest poProductRequest, Product product) {

        String requestQuantity = poProductRequest.getRequestQuantity() + " " + (product == null ? "" : product.getCategory().getUnitType().toString());
        String receivedQuantity = poProductRequest.getReceivedQuantity() + " " + (product == null ? "" : product.getCategory().getUnitType().toString());

        String notes = poProductRequest.getNotes() == null ? "" : poProductRequest.getNotes();
        String trackTraceSystem = poProductRequest.getTrackTraceSystem() == null ? "" : poProductRequest.getTrackTraceSystem().toString();
        return "<tr>\n" +
                "<td width=\"50\" height=\"30\" class=\"col-1\">" + index + "</td>\n" +
                "<td width=\"330\" height=\"30\" class=\"col\">" + product.getName() + "</td>\n" +
                "<td width=\"330\" height=\"30\" class=\"col\">" + requestQuantity + "</td>\n" +
                "<td width=\"330\" height=\"30\" class=\"col\">" + receivedQuantity + "</td>\n" +
                "<td width=\"330\" height=\"30\" class=\"col\">$" + poProductRequest.getUnitPrice() + "</td>\n" +
                "<td width=\"330\" height=\"30\" class=\"col\">" + poProductRequest.getRequestStatus() + "</td>\n" +
                "<td width=\"330\" height=\"30\" class=\"col\">" + trackTraceSystem + "</td>\n" +
                "<td width=\"330\" height=\"30\" class=\"col\">" + notes + "</td>\n" +
                "</tr>";
    }

    private String preparePaymentHistorySection(int index, ShipmentBillPayment shipmentBillPayment) {
        String notes = shipmentBillPayment.getNotes() == null ? "" : shipmentBillPayment.getNotes();
        return "<tr>\n" +
                "<td width=\"50\" height=\"30\" class=\"col-1\">" + index + "</td>\n" +
                "<td width=\"330\" height=\"30\" class=\"col\">$" + shipmentBillPayment.getAmountPaid() + "</td>\n" +
                "<td width=\"330\" height=\"30\" class=\"col\">" + TextUtil.toDateTime(shipmentBillPayment.getPaidDate()) + "</td>\n" +
                "<td width=\"330\" height=\"30\" class=\"col\">" + notes + "</td>\n" +
                "</tr>";
    }

    private String getCustomerCompanySection(final String name) {
        return "<tr>\n" +
                "                                <td width=\"330\" height=\"30\"\t>Customer Company Name</td>\n" +
                "                                <td width=\"210\" height=\"30\"><sup style=\"font-size:0.6em;\"></sup>" + name + "</td>\n" +
                "                            </tr>";
    }

    private String getVendorSection(final String name) {
        return "<tr>\n" +
                "                                <td width=\"330\" height=\"30\"\t>Vendor Name</td>\n" +
                "                                <td width=\"210\" height=\"30\"><sup style=\"font-size:0.6em;\"></sup>" + name + "</td>\n" +
                "                            </tr>\n";
    }

    /**
     * This method update shipment bill payment
     *
     * @param shipmentBillId : shipment bill id
     * @param paymentId      : payment id
     * @param request        :
     */
    @Override
    public ShipmentBill updateShipmentBillPayment(String shipmentBillId, String paymentId, ShipmentBillPaymentStatusRequest request) {

        ShipmentBill dbShipmentBill = shipmentBillRepository.get(token.getCompanyId(), shipmentBillId);

        if (dbShipmentBill == null) {
            throw new BlazeInvalidArgException(SHIPMENT_BILL, BILL_NOT_FOUND);
        }

        if (request.getAmountPaid() == null || request.getAmountPaid().doubleValue() <= 0) {
            throw new BlazeInvalidArgException(SHIPMENT_BILL, AMOUNT_NOT_ZERO);
        }
        if (request.getPaymentType() == null) {
            throw new BlazeInvalidArgException(SHIPMENT_BILL, PAYMENT_TYPE_NOT_FOUND);
        }

        PurchaseOrder purchaseOrder = purchaseOrderRepository.get(token.getCompanyId(), dbShipmentBill.getPurchaseOrderId());
        if (purchaseOrder == null) {
            throw new BlazeInvalidArgException(SHIPMENT_BILL, PO_NOT_FOUND);
        }

        ShipmentBillPayment shipmentBillPayment = null;

        if (dbShipmentBill.getPaymentHistory() != null && !dbShipmentBill.getPaymentHistory().isEmpty()) {
            for (ShipmentBillPayment billPayment : dbShipmentBill.getPaymentHistory()) {
                if (paymentId.equals(billPayment.getId())) {
                    shipmentBillPayment = billPayment;
                    break;
                }
            }
        }

        if (shipmentBillPayment == null) {
            throw new BlazeInvalidArgException(SHIPMENT_BILL, "Payment not found");
        }

        shipmentBillPayment.setPaymentType(request.getPaymentType());
        shipmentBillPayment.setPaidDate((request.getPaidDate() == 0) ? DateTime.now().getMillis() : request.getPaidDate());
        shipmentBillPayment.setAmountPaid(request.getAmountPaid());
        shipmentBillPayment.setNotes(request.getNotes());
        shipmentBillPayment.setReferenceNo(request.getReferenceNo());

        BigDecimal amountPaid = calculateAmountPaid(dbShipmentBill.getPaymentHistory());

        dbShipmentBill.setAmountPaid(amountPaid);

        getShipmentBillStatus(dbShipmentBill, amountPaid);

        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());
        if (shop != null && !shop.isRetail()) {
            if (dbShipmentBill.getPaymentStatus() == ShipmentBill.PaymentStatus.PAID) {
                purchaseOrder.setPurchaseOrderPreviousStatus(purchaseOrder.getPurchaseOrderStatus());
                purchaseOrder.setPurchaseOrderStatus(PurchaseOrder.PurchaseOrderStatus.Closed);
            }
        }

        purchaseOrder.setAmountPaid(amountPaid);

        purchaseOrderRepository.update(token.getCompanyId(), purchaseOrder.getId(), purchaseOrder);

        if (shop != null && !shop.isRetail() && purchaseOrder.getPurchaseOrderStatus() == PurchaseOrder.PurchaseOrderStatus.Closed) {
            poActivityService.addPOActivity(purchaseOrder.getId(), token.getActiveTopUser().getUserId(), "PO completed");
        }

        dbShipmentBill = shipmentBillRepository.update(token.getCompanyId(), shipmentBillId, dbShipmentBill);
        dbShipmentBill.setPaymentHistory(filterPaymentHistory(dbShipmentBill.getPaymentHistory(), false));

        return dbShipmentBill;
    }

    @Override
    public HashMap<String, ShipmentBill> getShipmentBillByPoAsMap(List<String> purchaseOrderIds) {
        HashMap<String, ShipmentBill> resultMap = new HashMap<>();
        if (purchaseOrderIds != null && !purchaseOrderIds.isEmpty()) {
            resultMap = shipmentBillRepository.listShipmentBillByPOAsMap(token.getCompanyId(), token.getShopId(), purchaseOrderIds);
            for (String key : resultMap.keySet()) {
                ShipmentBill shipmentBill = resultMap.get(key);
                shipmentBill.setPaymentHistory(filterPaymentHistory(shipmentBill.getPaymentHistory(), false));
                resultMap.put(key, shipmentBill);
            }
        }
        return resultMap;
    }

    @Override
    public void deleteShipmentBillPayment(String shipmentBillId, String paymentId) {
        ShipmentBill dbShipmentBill = shipmentBillRepository.get(token.getCompanyId(), shipmentBillId);

        if (dbShipmentBill == null) {
            throw new BlazeInvalidArgException("Shipment Bill", BILL_NOT_FOUND);
        }
        PurchaseOrder purchaseOrder = purchaseOrderRepository.get(token.getCompanyId(), dbShipmentBill.getPurchaseOrderId());
        if (purchaseOrder == null) {
            throw new BlazeInvalidArgException(SHIPMENT_BILL, PO_NOT_FOUND);
        }
        if (dbShipmentBill.getPaymentHistory() != null && !dbShipmentBill.getPaymentHistory().isEmpty()) {
            for (ShipmentBillPayment payment : dbShipmentBill.getPaymentHistory()) {
                if (payment.getId().equalsIgnoreCase(paymentId)) {
                    payment.setDeleted(true);
                    break;
                }
            }
        }
        BigDecimal amountPaid = calculateAmountPaid(dbShipmentBill.getPaymentHistory());
        dbShipmentBill.setAmountPaid(amountPaid);
        getShipmentBillStatus(dbShipmentBill, amountPaid);

        shipmentBillRepository.update(shipmentBillId, dbShipmentBill);

        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());
        if (shop != null && !shop.isRetail()) {
            if (dbShipmentBill.getPaymentStatus() == ShipmentBill.PaymentStatus.PAID) {
                purchaseOrder.setPurchaseOrderPreviousStatus(purchaseOrder.getPurchaseOrderStatus());
                purchaseOrder.setPurchaseOrderStatus(PurchaseOrder.PurchaseOrderStatus.Closed);
            }
        }

        purchaseOrder.setAmountPaid(amountPaid);

        purchaseOrderRepository.update(token.getCompanyId(), purchaseOrder.getId(), purchaseOrder);

        if (shop != null && !shop.isRetail() && purchaseOrder.getPurchaseOrderStatus() == PurchaseOrder.PurchaseOrderStatus.Closed) {
            poActivityService.addPOActivity(purchaseOrder.getId(), token.getActiveTopUser().getUserId(), "PO completed");
        }
    }

    /**
     * This is private method to filter all payments of shipment bill
     */
    private List<ShipmentBillPayment> filterPaymentHistory(List<ShipmentBillPayment> shipmentBillPayments, boolean filterBy) {
        List<ShipmentBillPayment> paymentHistory = new ArrayList<>();
        if (shipmentBillPayments != null && !shipmentBillPayments.isEmpty()) {
            for (ShipmentBillPayment payment : shipmentBillPayments) {
                if (payment.isDeleted() == filterBy) {
                    paymentHistory.add(payment);
                }
            }
        }
        return paymentHistory;
    }

    @Override
    public void closeShipmentBill(String shipmentBillId) {
        ShipmentBill dbShipmentBill = shipmentBillRepository.get(token.getCompanyId(), shipmentBillId);

        if (dbShipmentBill == null) {
            throw new BlazeInvalidArgException(SHIPMENT_BILL, BILL_NOT_FOUND);
        }

        PurchaseOrder purchaseOrder = purchaseOrderRepository.get(token.getCompanyId(), dbShipmentBill.getPurchaseOrderId());
        if (purchaseOrder == null) {
            throw new BlazeInvalidArgException(SHIPMENT_BILL, PO_NOT_FOUND);
        }

        if (dbShipmentBill.getPaymentStatus() == ShipmentBill.PaymentStatus.PAID && purchaseOrder.getPurchaseOrderStatus() == PurchaseOrder.PurchaseOrderStatus.Closed) {
            throw new BlazeInvalidArgException(SHIPMENT_BILL, "Shipment bill already paid.");
        }

        if (purchaseOrder.getPurchaseOrderStatus() == PurchaseOrder.PurchaseOrderStatus.Closed) {
            throw new BlazeInvalidArgException(SHIPMENT_BILL, "Purchase order already closed");
        }

        BigDecimal amountPaid = calculateAmountPaid(dbShipmentBill.getPaymentHistory());

        shipmentBillRepository.updatePaymentStatus(dbShipmentBill.getId(), ShipmentBill.PaymentStatus.PAID);
        purchaseOrder.setAmountPaid(amountPaid);
        purchaseOrder.setPurchaseOrderPreviousStatus(purchaseOrder.getPurchaseOrderStatus());
        purchaseOrder.setPurchaseOrderStatus(PurchaseOrder.PurchaseOrderStatus.Closed);
        purchaseOrder.setManualClose(true);
        purchaseOrderRepository.update(purchaseOrder.getId(), purchaseOrder);


    }

    /**
     * Override method to update received date for shipment bill
     *
     * @param shipmentBillId : shipment bill id
     * @param request        : request
     * @return : shipmentbill
     */
    @Override
    public ShipmentBill updateReceivedDate(String shipmentBillId, ShipmentBillUpdateRequest request) {
        ShipmentBill shipmentBill = shipmentBillRepository.getById(shipmentBillId);

        if (shipmentBill == null) {
            throw new BlazeInvalidArgException(SHIPMENT_BILL, BILL_NOT_FOUND);
        }

        if (request != null && request.getReceivedDate() != 0) {
            shipmentBillRepository.updateReceivedDate(shipmentBill.getId(), request.getReceivedDate());

            if (StringUtils.isNotBlank(shipmentBill.getPurchaseOrderId()) &&  ObjectId.isValid(shipmentBill.getPurchaseOrderId())) {
                purchaseOrderRepository.updateRecievedDate(shipmentBill.getPurchaseOrderId(), request.getReceivedDate());
            }
            // send updated date in response
            shipmentBill.setReceivedDate(request.getReceivedDate());

        }
        return shipmentBill;
    }

    @Override
    public void updatePurchaseIncompleteOrder(String purchaseOrderId) {
        if (StringUtils.isBlank(purchaseOrderId)) {
            throw new BlazeInvalidArgException(SHIPMENT_BILL, "Purchase Order Id cannot be blank.");
        }
        final PurchaseOrder purchaseOrder = purchaseOrderRepository.get(token.getCompanyId(), purchaseOrderId);
        if (purchaseOrder == null) {
            throw new BlazeInvalidArgException(SHIPMENT_BILL, PO_NOT_FOUND);
        }
        final ShipmentBill shipmentBill = shipmentBillRepository.getShipmentBillByPoId(purchaseOrderId);
        if (shipmentBill == null) {
            throw new BlazeInvalidArgException(SHIPMENT_BILL, BILL_NOT_FOUND);
        }
        BigDecimal amountPaid = calculateAmountPaid(shipmentBill.getPaymentHistory());

        if (shipmentBill.getGrandTotal().setScale(2, RoundingMode.HALF_EVEN).doubleValue() > amountPaid.setScale(2,RoundingMode.HALF_EVEN).doubleValue()) {
            shipmentBillRepository.updatePaymentStatus(shipmentBill.getId(), ShipmentBill.PaymentStatus.UNPAID);
        }

        PurchaseOrder.PurchaseOrderStatus purchaseOrderPreviousStatus = Objects.nonNull(purchaseOrder.getPurchaseOrderPreviousStatus()) ? purchaseOrder.getPurchaseOrderPreviousStatus() : PurchaseOrder.PurchaseOrderStatus.ReceivedShipment;
        purchaseOrderRepository.updatePurchaseOrderStatus(token.getCompanyId(), purchaseOrderId, purchaseOrderPreviousStatus, null);
    }

    /**
     *
     * @param shipmentBillId
     * @return
     */
    @Override
    public InputStream createPdfForShipmentBill(String shipmentBillId) {
        ShipmentBill shipmentBill = null;
        if (StringUtils.isNotEmpty(shipmentBillId)) {
            shipmentBill = shipmentBillRepository.get(token.getCompanyId(), shipmentBillId);
        } else {
            throw new BlazeInvalidArgException("Shipment Bill", BILL_NOT_FOUND);
        }
        if (shipmentBill == null) {
            throw new BlazeInvalidArgException(SHIPMENT_BILL, "Error ! Shipment bill not found for this purchase order");
        }
        final PurchaseOrder purchaseOrder = purchaseOrderRepository.get(token.getCompanyId(), shipmentBill.getPurchaseOrderId());
        if (purchaseOrder == null) {
            throw new BlazeInvalidArgException(PURCHASE_ORDER, PO_NOT_FOUND);
        }

        String body = getShipmentBillPdfHtml(purchaseOrder, shipmentBill);
        return new ByteArrayInputStream(PdfGenerator.exportToPdfBox(body, SHIPMENT_BILL_HTML_RESOURCE));
    }

    private String getShipmentBillPdfHtml(PurchaseOrder purchaseOrder, ShipmentBill shipmentBill) {

        try {
            Vendor vendor = vendorRepository.getById(shipmentBill.getVendorId());
            Shop shop = shopRepository.getById(shipmentBill.getShopId());
            CompanyLicense companyLicense = null;
            if (vendor != null) {
                companyLicense = vendor.getCompanyLicense(purchaseOrder.getLicenseId());
            }

            BigDecimal amountPaid = shipmentBill.getAmountPaid();
            BigDecimal totalAmount = shipmentBill.getGrandTotal();

            amountPaid = (amountPaid == null) ? new BigDecimal(0) : amountPaid;
            totalAmount = (totalAmount == null) ? new BigDecimal(0) : totalAmount;

            BigDecimal lineItemDiscount = new BigDecimal(0);
            BigDecimal shipmentBillDiscount = new BigDecimal(0);
            BigDecimal totalDiscount = new BigDecimal(0);
            BigDecimal fees = new BigDecimal(0);
            BigDecimal balanceDue = new BigDecimal(0);
            BigDecimal subTotal = new BigDecimal(0);
            if (totalAmount.doubleValue() > amountPaid.doubleValue()) {
                balanceDue = totalAmount.subtract(amountPaid);
            }

            for (POProductRequest productRequest : shipmentBill.getPoProductRequest()) {
                subTotal = subTotal.add(productRequest.getReceivedQuantity().multiply(productRequest.getUnitPrice()));
                lineItemDiscount = lineItemDiscount.add(productRequest.getDiscount());
            }
            shipmentBillDiscount = shipmentBill.getDiscount();
            shipmentBillDiscount = shipmentBillDiscount.subtract(lineItemDiscount);

            subTotal = subTotal.subtract(lineItemDiscount);
            fees = (fees == null) ? fees : shipmentBill.getFees();

            BigDecimal deliveryCharge = purchaseOrder.getDeliveryCharge();
            deliveryCharge = (deliveryCharge == null) ? new BigDecimal(0) : deliveryCharge;
            BigDecimal changeReturn = amountPaid.doubleValue() > totalAmount.doubleValue() ? amountPaid.subtract(totalAmount) : BigDecimal.ZERO;
            totalDiscount = (purchaseOrder.getTotalDiscount() == null) ? getTotalDiscount(purchaseOrder) : purchaseOrder.getTotalDiscount();

            InputStream inputStream = PurchaseOrderServiceImpl.class.getResourceAsStream(SHIPMENT_BILL_HTML_RESOURCE);
            StringWriter writer = new StringWriter();
            try {
                IOUtils.copy(inputStream, writer, "UTF-8");
            } catch (IOException ex) {
                LOGGER.error("Error! in po email", ex);
            }
            String paymentStatus = shipmentBill.getPaymentStatus().toString();
            String body = writer.toString();
            if(shipmentBill.getAmountPaid().doubleValue() > 0 && shipmentBill.getGrandTotal().setScale(2, RoundingMode.HALF_EVEN).compareTo(shipmentBill.getAmountPaid().setScale(2,RoundingMode.HALF_EVEN)) > 0){
                 paymentStatus = "Partial paid";
            }

            body = body.replaceAll("==blankSpace==", TextUtil.textOrEmpty("  "));
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM-dd-YYYY-HH-mm-ss");
            body = body.replaceAll("==title==", shipmentBill.getShipmentBillNumber() + "-" + simpleDateFormat.format(System.currentTimeMillis()));
            body = body.replaceAll("==shipmentBillNumber==", shipmentBill.getShipmentBillNumber());
            body = body.replaceAll("==paymentStatus==", paymentStatus);
            body = body.replaceAll("==companyName==", shop.getName());


            if (shop.getAddress() != null) {
                Address address = shop.getAddress();
                body = body.replaceAll("==address==", TextUtil.textOrEmpty(address.getAddress()));
                body = body.replaceAll("==city==", TextUtil.textOrEmpty(address.getCity()));
                body = body.replaceAll("==state==", TextUtil.textOrEmpty(address.getState()));
                body = body.replaceAll("==zipCode==", TextUtil.textOrEmpty(address.getZipCode()));
                body = body.replaceAll("==country==", TextUtil.textOrEmpty(address.getCountry()));
                body = body.replaceAll("==showCompanyAddress==", TextUtil.textOrEmpty(""));
            } else {
                body = body.replaceAll("==showCompanyAddress==", TextUtil.textOrEmpty("display:none;"));
            }

            if (shop != null && StringUtils.isNotBlank(shop.getLicense())) {
                body = body.replaceAll("==licenseNumber==", shop.getLicense());
            } else {
                body = body.replaceAll("==licenseNumber==", TextUtil.textOrEmpty(""));
            }

            BigDecimal ozTax = new BigDecimal(0);

            if (shipmentBill.getTaxResult() != null && shipmentBill.getTaxResult().getCultivationTaxResult() != null) {
                ozTax = shipmentBill.getTaxResult().getCultivationTaxResult().getTotalCultivationTax();
            }

            body = body.replaceAll("==showTotalTax==", TextUtil.textOrEmpty((ozTax != null && ozTax.doubleValue() <= 0) ? "display:none" : ""));
            body = body.replaceAll("==totalTax==", TextUtil.formatToTwoDecimalPoints(ozTax.doubleValue()));

            body = body.replaceAll("==showOZTax==", TextUtil.textOrEmpty((ozTax != null && ozTax.doubleValue() <= 0) ? "display:none" : ""));
            body = body.replaceAll("==ozTax==", TextUtil.formatToTwoDecimalPoints(ozTax.doubleValue()));


            if (purchaseOrder.getPoProductRequestList() != null) {
                StringBuilder section = new StringBuilder();
                List<ObjectId> productIds = new ArrayList<>();
                for (POProductRequest poProductRequest : purchaseOrder.getPoProductRequestList()) {
                    if (StringUtils.isNotBlank(poProductRequest.getProductId()) && ObjectId.isValid(poProductRequest.getProductId()))
                        productIds.add(new ObjectId(poProductRequest.getProductId()));
                }
                if (!productIds.isEmpty()) {
                    HashMap<String, Product> productHashMap = productRepository.findProductsByIdsAsMap(token.getCompanyId(), token.getShopId(), productIds);
                    int index = 0;
                    for (POProductRequest poProductRequest : shipmentBill.getPoProductRequest()) {
                        Product product = productHashMap.get(poProductRequest.getProductId());
                        if (product == null) {
                            continue;
                        }
                        section.append(getProductInfoSectionForPdf(++index, product, poProductRequest, purchaseOrder));

                    }
                    String sectionData = section.toString();
                    sectionData = sectionData.replaceAll("&", "&amp;");
                    body = body.replaceAll("==productInformation==", Matcher.quoteReplacement(sectionData));
                }
            }

            if (vendor != null) {
                String vendorName = vendor.getName();
                if (StringUtils.isNotBlank(vendor.getDbaName())) {
                    vendorName += "(" + vendor.getDbaName() + ")";
                }

                body = body.replaceAll("==billTo==", vendorName);

            } else {
                body = body.replaceAll("==billTo==", TextUtil.textOrEmpty(""));
            }

            StringBuilder billToLicenseNo = new StringBuilder();
            if (companyLicense != null) {
                billToLicenseNo.append((StringUtils.isNotBlank(companyLicense.getLicenseNumber()) ? companyLicense.getLicenseNumber() : "N/A"))
                        .append(" (").append(companyLicense.getCompanyType()).append(")");
            }

            body = body.replaceAll("==billToLicenseNumber==", Matcher.quoteReplacement(billToLicenseNo.toString()));

            if (vendor != null && vendor.getAddress() != null) {
                Address address = vendor.getAddress();
                body = body.replaceAll("==billToAddress==", TextUtil.textOrEmpty(address.getAddress()));
                body = body.replaceAll("==billToCity==", TextUtil.textOrEmpty(address.getCity()));
                body = body.replaceAll("==billToState==", TextUtil.textOrEmpty(address.getState()));
                body = body.replaceAll("==billToZipCode==", TextUtil.textOrEmpty(address.getZipCode()));
                body = body.replaceAll("==billToCountry==", TextUtil.textOrEmpty(address.getCountry()));
            } else {
                body = body.replaceAll("==billToAddress==", TextUtil.textOrEmpty(""));
                body = body.replaceAll("==billToCity==", TextUtil.textOrEmpty(""));
                body = body.replaceAll("==billToState==", TextUtil.textOrEmpty(""));
                body = body.replaceAll("==billToZipCode==", TextUtil.textOrEmpty(""));
                body = body.replaceAll("==billToCountry==", TextUtil.textOrEmpty(""));
            }

            String timeZone = token.getRequestTimeZone();
            if (StringUtils.isBlank(timeZone)) {
                timeZone = shop.getTimeZone();
            }

            body = body.replaceAll("==poDate==", TextUtil.toDate(DateUtil.toDateTime(purchaseOrder.getPurchaseOrderDate(), timeZone)));
            body = body.replaceAll("==term==", purchaseOrder.getPoPaymentTerms().toString());

            body = body.replaceAll("==subTotal==", TextUtil.formatToTwoDecimalPoints(subTotal));
            body = body.replaceAll("==discount==", TextUtil.formatToTwoDecimalPoints(shipmentBillDiscount));
            body = body.replaceAll("==fees==", TextUtil.formatToTwoDecimalPoints(fees));
            body = body.replaceAll("==totalDiscount==", TextUtil.formatToTwoDecimalPoints(totalDiscount));

            body = body.replaceAll("==receivedDate==", shipmentBill.getReceivedDate() == 0 ? "" : TextUtil.toDate(DateUtil.toDateTime(shipmentBill.getReceivedDate(), timeZone)));
            body = body.replaceAll("==deliveryCharge==", TextUtil.formatToTwoDecimalPoints(deliveryCharge));
            body = body.replaceAll("==paymentMade==", TextUtil.formatToTwoDecimalPoints(amountPaid));
            body = body.replaceAll("==total==", TextUtil.formatToTwoDecimalPoints(totalAmount));
            body = body.replaceAll("==balanceDue==", TextUtil.formatToTwoDecimalPoints(balanceDue));

            body = body.replaceAll("==showChangeReturn==", TextUtil.textOrEmpty((changeReturn.doubleValue() <= 0) ? "display:none" : ""));
            body = body.replaceAll("==changeReturn==", TextUtil.formatToTwoDecimalPoints(changeReturn));

            if (StringUtils.isBlank(shipmentBill.getNotes())) {
                body = body.replaceAll("==showNotes", "display:none");
                body = body.replaceAll("==notes==","");
            } else {
                body = body.replaceAll("==notes==", Matcher.quoteReplacement(shipmentBill.getNotes() == null ? "" : shipmentBill.getNotes().replaceAll("&", "&amp;")));
            }
            if (StringUtils.isBlank(purchaseOrder.getTermsAndCondition())) {
                body = body.replaceAll("==showTermsAndConditions", "display:none");
                body = body.replaceAll("==termsConditions==", "");
            } else {
                body = body.replaceAll("==termsConditions==", Matcher.quoteReplacement(StringUtils.isBlank(purchaseOrder.getTermsAndCondition()) ? " " : purchaseOrder.getTermsAndCondition().replaceAll("&", "&amp;")));
            }

            String logoURL = (shop.getLogo() != null && org.apache.commons.lang3.StringUtils.isNotBlank(shop.getLogo().getLargeURL())) ? shop.getLogo().getLargeURL() : "https://connect-assets.s3.amazonaws.com/email/logo.jpg";
            body = body.replaceAll("==logo==", logoURL);
            body = body.replaceAll("==QRCodeImage==", (purchaseOrder.getPoQrCodeUrl() == null) ? "" : purchaseOrder.getPoQrCodeUrl());

            if (purchaseOrder.getAdjustmentInfoList() == null || purchaseOrder.getAdjustmentInfoList().size() == 0) {
                body = body.replaceAll("==showAdjustments==", "display:none");
            } else {
                List<ObjectId> adjustmentIds = new ArrayList<>();
                for (AdjustmentInfo info : purchaseOrder.getAdjustmentInfoList()) {
                    adjustmentIds.add(new ObjectId(info.getAdjustmentId()));
                }

                HashMap<String, Adjustment> adjustmentMap = adjustmentRepository.listAsMap(token.getCompanyId(), adjustmentIds);

                StringBuilder adjustmentData = new StringBuilder();

                for (AdjustmentInfo info : purchaseOrder.getAdjustmentInfoList()) {
                    Adjustment adjustment = adjustmentMap.get(info.getAdjustmentId());
                    if (adjustment == null) {
                        continue;
                    }
                    adjustmentData.append(prepareAdjustmentInfo(adjustment, info.getAmount().doubleValue(), info.isNegative()));
                }

                String adjustments = adjustmentData.toString();
                adjustments = adjustments.replaceAll("&", "&amp;");
                body = body.replaceAll("==adjustmentList==", Matcher.quoteReplacement(adjustments));
                body = body.replaceAll("==showAdjustments==", " ");
            }

            return body;
        } catch (Exception ex) {
            LOGGER.error(PURCHASE_ORDER, ex);
            return "Information are not completed";
        }

    }

    private BigDecimal getTotalDiscount(PurchaseOrder purchaseOrder) {
        BigDecimal totalDiscount = BigDecimal.ZERO;

        for (POProductRequest poProductRequest : purchaseOrder.getPoProductRequestList()) {
            totalDiscount = totalDiscount.add(poProductRequest.getDiscount() == null ? BigDecimal.ZERO : poProductRequest.getDiscount());
        }

        return purchaseOrder.getDiscount().add(totalDiscount);

    }

    private String getProductInfoSectionForPdf(final int index, final Product product, final POProductRequest productRequest, PurchaseOrder purchaseOrder) {
        BigDecimal finalTotalCost = productRequest.getReceivedQuantity().multiply(productRequest.getUnitPrice());
        BigDecimal discount = productRequest.getDiscount() == null ? BigDecimal.ZERO : productRequest.getDiscount();
        if (finalTotalCost != null || finalTotalCost.doubleValue() != 0) {
            finalTotalCost = finalTotalCost.subtract(discount);
        }

         BigDecimal ozTax = new BigDecimal(0);

        if (productRequest != null && productRequest.getTotalCultivationTax() != null) {
            ozTax = productRequest.getTotalCultivationTax();
        }

        return "<tr>\n" +
                "<td style=\"width:10%;font-family:'Open Sans',Helvetica,Arial,sans-serif!important;text-align:center;vertical-align:middle;padding: 10px 5px 10px 5px;line-height:1.42857;border-top:1px solid #e7ecf1;font-size:12px;color:#5a5a5a;\">" + index + "</td>\n" +
                "<td style=\"width:60%;font-family:'Open Sans',Helvetica,Arial,sans-serif!important;text-align:left;vertical-align:middle;padding: 10px 5px 10px 5px;line-height:1.42857;border-top:1px solid #e7ecf1;font-size:12px;color:#5a5a5a;\">" + product.getName() + "</td>\n" +
                "<td style=\"width:10%;font-family:'Open Sans',Helvetica,Arial,sans-serif!important;text-align:center;vertical-align:middle;padding: 10px 5px 10px 5px;line-height:1.42857;border-top:1px solid #e7ecf1;font-size:12px;color:#5a5a5a;\"> " + TextUtil.formatToTwoDecimalPoints(productRequest.getRequestQuantity()) + product.getCustomGramType().getType() + "</td>\n" +
                "<td style=\"width:10%;font-family:'Open Sans',Helvetica,Arial,sans-serif!important;text-align:center;vertical-align:middle;padding: 10px 5px 10px 5px;line-height:1.42857;border-top:1px solid #e7ecf1;font-size:12px;color:#5a5a5a;\"> " + TextUtil.formatToTwoDecimalPoints(productRequest.getReceivedQuantity()) + product.getCustomGramType().getType() + "</td>\n" +
                "<td style=\"width:10%;font-family:'Open Sans',Helvetica,Arial,sans-serif!important;text-align:center;vertical-align:middle;padding: 10px 5px 10px 5px;line-height:1.42857;border-top:1px solid #e7ecf1;font-size:12px;color:#5a5a5a;\">$" + TextUtil.formatToTwoDecimalPoints(ozTax) + "</td>\n" +
                "<td style=\"width:10%;font-family:'Open Sans',Helvetica,Arial,sans-serif!important;text-align:center;vertical-align:middle;padding: 10px 5px 10px 5px;line-height:1.42857;border-top:1px solid #e7ecf1;font-size:12px;color:#5a5a5a;\">$" + TextUtil.formatToTwoDecimalPoints(productRequest.getDiscount() == null ? BigDecimal.ZERO : productRequest.getDiscount()) + "</td>\n" +
                "<td style=\"width:10%;font-family:'Open Sans',Helvetica,Arial,sans-serif!important;text-align:center;vertical-align:middle;padding: 10px 5px 10px 5px;line-height:1.42857;border-top:1px solid #e7ecf1;font-size:12px;color:#5a5a5a;\">$" + TextUtil.formatToTwoDecimalPoints(finalTotalCost) + "</td>\n" +
                "<td style=\"width:10%;font-family:'Open Sans',Helvetica,Arial,sans-serif!important;text-align:right;vertical-align:middle;padding: 10px 5px 10px 5px;line-height:1.42857;border-top:1px solid #e7ecf1;font-size:12px;color:#5a5a5a;\">" + productRequest.getRequestStatus() + "</td>\n" +
                "</tr>";
    }

    private String prepareAdjustmentInfo(Adjustment adjustment, double amount, boolean negative) {
        StringBuilder adjustmentData = new StringBuilder();
        adjustmentData.append("<div style=\"width:100%;float:left;background-color: rgb(238, 238, 238)\">\n");
        adjustmentData.append("<div style=\"width:50%;float:left;color: #5a5a5a;text-align: right;padding:8px;font-size:12px;font-family:'Open Sans',Helvetica,Arial,sans-serif!important;\">" + adjustment.getName() + "</div>");

        if (negative) {
            adjustmentData.append("<div style=\"width:35%;float:right;text-align: right;color: red;padding:8px;font-size:12px;font-family:'Open Sans',Helvetica,Arial,sans-serif!important;\"> -$" + TextUtil.formatToTwoDecimalPoints(amount) + "</div>");
        } else {
            adjustmentData.append("<div style=\"width:35%;float:right;text-align: right;color: #5a5a5a;padding:8px;font-size:12px;font-family:'Open Sans',Helvetica,Arial,sans-serif!important;\"> $" + TextUtil.formatToTwoDecimalPoints(amount) + "</div>");
        }
        adjustmentData.append("</div>");

        return adjustmentData.toString();
    }


    private void getShipmentBillStatus(ShipmentBill dbShipmentBill, BigDecimal amountPaid) {
        if (dbShipmentBill.getGrandTotal().setScale(2, RoundingMode.HALF_EVEN).compareTo(amountPaid.setScale(2,RoundingMode.HALF_EVEN)) <= 0) {
            dbShipmentBill.setPaymentStatus(ShipmentBill.PaymentStatus.PAID);
        } else if(dbShipmentBill.getGrandTotal().setScale(2, RoundingMode.HALF_EVEN).compareTo(dbShipmentBill.getAmountPaid().setScale(2,RoundingMode.HALF_EVEN)) > 0 && dbShipmentBill.getAmountPaid().doubleValue() > 0) {
            dbShipmentBill.setPaymentStatus(ShipmentBill.PaymentStatus.PARTIAL);
        } else {
            dbShipmentBill.setPaymentStatus(ShipmentBill.PaymentStatus.UNPAID);
        }
    }
}
