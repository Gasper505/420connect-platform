package com.fourtwenty.core.services.mgmt.impl;

import com.fourtwenty.core.domain.models.purchaseorder.Shipment;
import com.fourtwenty.core.domain.repositories.dispensary.ShipmentRepository;
import com.fourtwenty.core.rest.dispensary.requests.inventory.ShipmentAddRequest;
import com.fourtwenty.core.rest.dispensary.requests.inventory.ShipmentStatusUpdateRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.mgmt.ShipmentService;
import com.google.inject.Inject;
import com.google.inject.Provider;
import org.joda.time.DateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ShipmentServiceImpl extends AbstractAuthServiceImpl implements ShipmentService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ShipmentServiceImpl.class);

    @Inject
    ShipmentRepository shipmentRepository;

    @Inject
    public ShipmentServiceImpl(ShipmentRepository shipmentRepository, Provider<ConnectAuthToken> tokenProvider){
        super(tokenProvider);
        this.shipmentRepository = shipmentRepository;
    }

    @Override
    public Shipment addShipment(ShipmentAddRequest shipmentAddRequest) {
        Shipment shipment = prepareShipment(shipmentAddRequest);
        if(shipment != null){
            shipment.prepare(token.getCompanyId());
            shipment.setShopId(token.getShopId());

            Shipment dbShipment = shipmentRepository.save(shipment);
            return dbShipment;
        }
        return shipment;
    }

    @Override
    public Shipment addShipment(Shipment shipment) {
        return shipmentRepository.save(shipment);
    } 

    @Override
    public Shipment getShipmentById(String shipmentId) {
        Shipment shipment = shipmentRepository.get(token.getCompanyId(), shipmentId);
        return shipment;
    }

    @Override
    public SearchResult<Shipment> getAllShipment(int start, int limit, String searchTerm, Shipment.ShipmentSort sortOption) {
        String sortOptionStr = "";
        if(Shipment.ShipmentSort.CREATED_DATE == sortOption) {
            sortOptionStr = "{created: -1}";
        } else {
            sortOptionStr = "{modified: -1}";
        }

        return shipmentRepository.getAllShipment(token.getCompanyId(), token.getShopId(), start, limit, sortOptionStr);
    }

    @Override
    public SearchResult<Shipment> getShipmentListByStatus(int start, int limit, String status, String searchTerm, Shipment.ShipmentSort sortOption) {
        String sortOptionStr = "";
        if(Shipment.ShipmentSort.CREATED_DATE == sortOption) {
            sortOptionStr = "{created: -1}";
        } else {
            sortOptionStr = "{modified: -1}";
        }

        return shipmentRepository.listAllByStatus(token.getCompanyId(), token.getShopId() , start, limit, sortOptionStr, status);
    }

    @Override
    public Shipment updateShipmentStatus(String shipmentId, ShipmentStatusUpdateRequest shipmentStatusUpdateRequest) {
        Shipment shipment = shipmentRepository.get(token.getCompanyId(), shipmentId);
        if(shipment != null) {
            shipment.setShipmentStatus(shipmentStatusUpdateRequest.getStatus());
            if(shipmentStatusUpdateRequest.getStatus() == Shipment.ShipmentStatus.Accepted) {
                shipment.setAcceptedBy(token.getActiveTopUser().getUserId());
                shipment.setAcceptedDate(DateTime.now().getMillis());
            }
            shipment = shipmentRepository.save(shipment);
        }
        return shipment;
    }

    private Shipment prepareShipment(ShipmentAddRequest shipmentAddRequest) {
        Shipment shipment = new Shipment();
        shipment.setShippingManifestId(shipmentAddRequest.getShippingManifestId());
        shipment.setShippingManifestNo(shipmentAddRequest.getShippingManifestNo());
        shipment.setProductMetrcInfo(shipmentAddRequest.getProductMetrcInfo());
        shipment.setShipperInformation(shipmentAddRequest.getShipperInformation());
        shipment.setDeliveryDate(shipmentAddRequest.getDeliveryDate());
        shipment.setDeliveryTime(shipmentAddRequest.getDeliveryTime());
        shipment.setShippedDateTime(shipmentAddRequest.getShippedDateTime());
        shipment.setEstimatedArrival(shipmentAddRequest.getEstimatedArrival());
        shipment.setNotes(shipmentAddRequest.getNotes());
        shipment.setDistributorInformation(shipmentAddRequest.getDistributorInformation());
        shipment.setShipmentStatus(shipmentAddRequest.getShipmentStatus());
        return shipment;
    }

    @Override
    public List<Shipment>  getAvailableShipments() {
        return shipmentRepository.getAvailableShipment(token.getCompanyId(), token.getShopId());
    }

    @Override
    public Shipment rejectShipment(String shipmentId) {
        Shipment shipment = shipmentRepository.getById(shipmentId);
        if(shipment != null) {
            shipment.setShipmentStatus(Shipment.ShipmentStatus.Rejected);
            shipment = shipmentRepository.update(shipmentId, shipment);
        }
        return shipment;
    }
}