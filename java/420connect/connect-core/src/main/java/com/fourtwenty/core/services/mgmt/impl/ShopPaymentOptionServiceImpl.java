package com.fourtwenty.core.services.mgmt.impl;

import com.amazonaws.util.StringUtils;
import com.fourtwenty.core.domain.models.payment.ShopPaymentOption;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.repositories.payment.ShopPaymentOptionRepository;
import com.fourtwenty.core.event.BlazeEventBus;
import com.fourtwenty.core.event.linx.IsLinxEnabledEvent;
import com.fourtwenty.core.exceptions.BlazeOperationException;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.mgmt.ShopPaymentOptionService;
import com.google.common.collect.Lists;
import com.google.inject.Provider;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


public class ShopPaymentOptionServiceImpl extends AbstractAuthServiceImpl implements ShopPaymentOptionService {
    @Inject
    ShopPaymentOptionRepository shopPaymentOptionRepository;

    @Inject
    BlazeEventBus eventBus;


    @Inject
    public ShopPaymentOptionServiceImpl(Provider<ConnectAuthToken> token) {
        super(token);
    }


    @Override
    public List<ShopPaymentOption> getPaymentOptions() {
        // Get currently persisted shop payment option objects
        List<ShopPaymentOption> list =
                Lists.newArrayList(shopPaymentOptionRepository.listAllByShop(token.getCompanyId(), token.getShopId()));

        // Get list of persisted payment options
        List<Cart.PaymentOption> existingPaymentOptions = list
                .stream()
                .map(spo -> spo.getPaymentOption())
                .collect(Collectors.toList());

        // Filter Cart.PaymentOptions enum to only PaymentOptions not persisted
        // Create empty Shop payment options for missing persisted records (backwards compatibility)
        list.addAll(
                Arrays.stream(Cart.PaymentOption.values())
                        .filter(po -> !existingPaymentOptions.contains(po) && po != Cart.PaymentOption.Hypur && po != Cart.PaymentOption.None)
                        .map(po -> new ShopPaymentOption(po))
                        .collect(Collectors.toList()));

        return list;
    }

    @Override
    public void savePaymentOptions(List<ShopPaymentOption> shopPaymentOptions) {
        if (shopPaymentOptions != null) {

            // Linx Validation Check (Requires the linx account be configured and enabled)
            boolean tryingToEnableLinx = tryingToEnable(shopPaymentOptions, Cart.PaymentOption.Linx);
            if (tryingToEnableLinx) {
                IsLinxEnabledEvent isLinxEnabledEvent = new IsLinxEnabledEvent();
                isLinxEnabledEvent.setPayload(token.getCompanyId(), token.getShopId());
                eventBus.post(isLinxEnabledEvent);
                if (!isLinxEnabledEvent.getResponse().isEnabled()) {
                    throw new BlazeOperationException("You must configure and enable the linx account before you can enable the payment option.");
                }
            }
            List<ShopPaymentOption> paymentOptionsToUpdate = new ArrayList<>();
            List<ShopPaymentOption> paymentOptionsToSave = new ArrayList<>();
            List<ShopPaymentOption> dbPaymentList = Lists.newArrayList(shopPaymentOptionRepository.listAllByShop(token.getCompanyId(), token.getShopId()));
            List<Cart.PaymentOption> existingPaymentOptions = dbPaymentList.stream().map(spo -> spo.getPaymentOption()).collect(Collectors.toList());
            for (ShopPaymentOption paymentOption : shopPaymentOptions) {
                if (StringUtils.isNullOrEmpty(paymentOption.getId())) {
                        if (!existingPaymentOptions.contains(paymentOption.getPaymentOption())) {
                            paymentOption.prepare(token.getCompanyId());
                            paymentOption.setShopId(token.getShopId());
                            paymentOptionsToSave.add(paymentOption);
                        } else {
                            paymentOption.prepare(token.getCompanyId());
                            paymentOption.setShopId(token.getShopId());
                            paymentOptionsToUpdate.add(paymentOption);
                        }
                } else {
                    paymentOption.prepare(token.getCompanyId());
                    paymentOption.setShopId(token.getShopId());
                    paymentOptionsToUpdate.add(paymentOption);
                }
            }
            if (paymentOptionsToSave.size() > 0) {
                shopPaymentOptionRepository.save(paymentOptionsToSave);
            }
            for (ShopPaymentOption shopPaymentOption : paymentOptionsToUpdate) {
                shopPaymentOptionRepository.update(token.getCompanyId(), shopPaymentOption.getId(), shopPaymentOption);
            }

        }
    }

    private boolean tryingToEnable(List<ShopPaymentOption> shopPaymentOptions, Cart.PaymentOption paymentOption) {
        boolean tryingToEnable = shopPaymentOptions
                .stream()
                .filter(po -> po.getPaymentOption() == paymentOption && po.isEnabled())
                .findFirst()
                .isPresent();

        return tryingToEnable;
    }
}
