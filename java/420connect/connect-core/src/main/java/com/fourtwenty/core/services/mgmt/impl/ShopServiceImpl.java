package com.fourtwenty.core.services.mgmt.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fourtwenty.core.domain.models.company.*;
import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.models.plugins.MessagingPluginCompanySetting;
import com.fourtwenty.core.domain.models.plugins.MessagingPluginShopSetting;
import com.fourtwenty.core.domain.models.store.OnlineStoreInfo;
import com.fourtwenty.core.domain.models.thirdparty.MetrcAccount;
import com.fourtwenty.core.domain.models.thirdparty.MetrcFacilityAccount;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.domain.repositories.plugins.MessagingPluginRepository;
import com.fourtwenty.core.domain.repositories.thirdparty.MetrcAccountRepository;
import com.fourtwenty.core.exceptions.BlazeAuthException;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.jobs.DailySummaryEmail;
import com.fourtwenty.core.jobs.LowInventoryNotification;
import com.fourtwenty.core.rest.common.EmailNotificationRequest;
import com.fourtwenty.core.rest.common.EmailRequest;
import com.fourtwenty.core.rest.dispensary.requests.shop.ShopAddRequest;
import com.fourtwenty.core.rest.dispensary.requests.shop.ShopUpdateRequest;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.ListResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.shop.CustomShopResult;
import com.fourtwenty.core.rest.dispensary.results.shop.ShopResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.common.RealtimeService;
import com.fourtwenty.core.services.mgmt.DefaultDataService;
import com.fourtwenty.core.services.mgmt.ShopService;
import com.fourtwenty.core.services.twilio.TwilioMessageService;
import com.google.common.collect.Lists;
import com.google.inject.Provider;
import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.model.GeocodingResult;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import javax.inject.Inject;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;

/**
 * Created by mdo on 9/27/15.
 */
public class ShopServiceImpl extends AbstractAuthServiceImpl implements ShopService {
    private static final Log LOG = LogFactory.getLog(ShopServiceImpl.class);
    private static final String EMPLOYEE = "Employee";
    private static final String EMPLOYEE_NOT_FOUND = "Employee not found";
    private static final String ADD_SHOP_NOT_ALLOWED = "You don't have permission to add shop";
    private static final String SHOP = "Shop";
    private static final String SHOP_NOT_FOUND = "Shop not found";
    private static final String ERROR_UPDATE = "Error while reading update data";

    @Inject
    ShopRepository shopRepository;
    @Inject
    CompanyRepository companyRepository;
    @Inject
    CompanyFeaturesRepository companyFeaturesRepository;
    @Inject
    DefaultDataService defaultDataService;
    @Inject
    RealtimeService realtimeService;
    @Inject
    private TwilioMessageService twilioMessageService;
    @Inject
    MessagingPluginRepository messagingPluginRepository;
    @Inject
    MetrcAccountRepository metrcAccountRepository;
    @Inject
    TransactionRepository transactionRepository;
    @Inject
    private ExciseTaxInfoRepository exciseTaxInfoRepository;
    @Inject
    ProductRepository productRepository;
    @Inject
    private DailySummaryEmail dailySummaryEmail;
    @Inject
    private EmployeeRepository employeeRepository;
    @Inject
    private RoleRepository roleRepository;
    @Inject
    private LowInventoryNotification lowInventoryNotification;
    @Inject
    private MemberRepository memberRepository;

    @Inject
    public ShopServiceImpl(Provider<ConnectAuthToken> token) {
        super(token);
    }

    @Override
    public SearchResult<CustomShopResult> searchShops(CompanyFeatures.AppTarget appTarget) {

        SearchResult<CustomShopResult> items = null;
        if (appTarget != null) {
            items = shopRepository.findShopsByAppTarget(token.getCompanyId(), 0, Integer.MAX_VALUE, appTarget, CustomShopResult.class);
        }

        if (token.getAppTarget() == null || (items == null || items.getValues().isEmpty())) {
            items = shopRepository.findItems(token.getCompanyId(), 0, Integer.MAX_VALUE, CustomShopResult.class);
            //items = shopRepository.getAllShops(0, Integer.MAX_VALUE);
        }

        if (items != null && items.getValues() != null) {

            List<ExciseTaxInfo> exciseTaxInfoList = exciseTaxInfoRepository.list();
            for (CustomShopResult customShopResult : items.getValues()) {
                this.prepareCustomShopResult(customShopResult, exciseTaxInfoList);
            }
        }

        return items;
    }

    /**
     * This method prepares custom result for shop
     */
    private void prepareCustomShopResult(CustomShopResult customShopResult, List<ExciseTaxInfo> exciseTaxInfoList) {
        if (exciseTaxInfoList != null && customShopResult.getAddress() != null) {
            for (ExciseTaxInfo exciseTaxInfo : exciseTaxInfoList) {
                if (exciseTaxInfo.getState().equalsIgnoreCase(customShopResult.getAddress().getState()) &&
                        exciseTaxInfo.getCountry().equalsIgnoreCase(customShopResult.getAddress().getCountry())) {
                    if (exciseTaxInfo.getCannabisTaxType() == ExciseTaxInfo.CannabisTaxType.Cannabis) {
                        customShopResult.setExciseTaxInfo(exciseTaxInfo);
                    }
                    if (exciseTaxInfo.getCannabisTaxType() == ExciseTaxInfo.CannabisTaxType.NonCannabis) {
                        customShopResult.setNonCannabisTaxInfo(exciseTaxInfo);
                    }
                }
            }
        }
        if (customShopResult.getOnlineStoreInfo() != null && customShopResult.getOnlineStoreInfo().getCartMinimum() != null
                && customShopResult.getOnlineStoreInfo().getCartMinimum().doubleValue() > 0
                && customShopResult.getOnlineStoreInfo().getCartMinType() != null
                && customShopResult.getCartMinimums() == null) {
            CartMinimums minimums = new CartMinimums();
            minimums.prepare(token.getCompanyId());
            minimums.setMinimumsType(CartMinimums.MinimumsType.SIMPLE);

            customShopResult.setCartMinimums(minimums);
        }

        if (customShopResult.getMembersTag() == null || customShopResult.getMembersTag().size() == 0) {
            List<String> allMemberTags = memberRepository.getAllMemberTags(customShopResult.getCompanyId());
            customShopResult.setMembersTag(new HashSet<String>(allMemberTags));
        }

        // set default values for non cannabis taxes
        if (customShopResult.getNonCannabisTaxes() == null) {
            customShopResult.setNonCannabisTaxes(new NonCannabisTaxInfo());
        }

        if (customShopResult.getNonCannabisTaxes().getTaxInfo() == null) {
            customShopResult.getNonCannabisTaxes().setTaxInfo(new TaxInfo());
        }

        if (customShopResult.getNonCannabisTaxes().getTaxInfo() != null && StringUtils.isBlank(customShopResult.getNonCannabisTaxes().getTaxInfo().getId())) {
            TaxInfo taxInfo = customShopResult.getNonCannabisTaxes().getTaxInfo();
            taxInfo.prepare();
            customShopResult.getNonCannabisTaxes().setTaxInfo(taxInfo);
        }

        if (CollectionUtils.isEmpty(customShopResult.getNonCannabisTaxes().getTaxTables())) {
            List<ConsumerType> consumerTypes = Lists.newArrayList(ConsumerType.values());
            for (ConsumerType consumerType : consumerTypes) {
                CompoundTaxTable taxTable = new CompoundTaxTable();
                if (consumerType == ConsumerType.Other) {
                    continue;
                }
                taxTable.prepare(customShopResult.getCompanyId());
                taxTable.setShopId(customShopResult.getId());
                taxTable.setConsumerType(consumerType);
                taxTable.setName(consumerType.getDisplayName());
                taxTable.setCityTax(new CompoundTaxRate());
                taxTable.setCountyTax(new CompoundTaxRate());
                taxTable.setFederalTax(new CompoundTaxRate());
                taxTable.setStateTax(new CompoundTaxRate());

                taxTable.reset();

                taxTable.getCityTax().prepare(customShopResult.getCompanyId());
                taxTable.getCityTax().setShopId(customShopResult.getId());

                taxTable.getCountyTax().prepare(customShopResult.getCompanyId());
                taxTable.getCountyTax().setShopId(customShopResult.getId());

                taxTable.getFederalTax().prepare(customShopResult.getCompanyId());
                taxTable.getFederalTax().setShopId(customShopResult.getId());

                taxTable.getStateTax().prepare(customShopResult.getCompanyId());
                taxTable.getStateTax().setShopId(customShopResult.getId());

                customShopResult.getNonCannabisTaxes().getTaxTables().add(taxTable);
            }
        }
    }

    // Shops
    @Override
    public ListResult<Shop> list() {
        ListResult<Shop> shops = new ListResult<Shop>();
        shops.setValues(Lists.newArrayList(shopRepository.list(token.getCompanyId())));

        return shops;
    }

    @Override
    public DateSearchResult<ShopResult> getShops(long afterDate, long beforeDate) {
        DateSearchResult<ShopResult> shops = shopRepository.findItemsWithDate(token.getCompanyId(), afterDate, beforeDate, ShopResult.class);

        String stateCode = null;
        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());
        if (Objects.isNull(shop)) {
            throw new BlazeInvalidArgException("Shop", "Shop doesn't found for metrc.");
        }
        if (Objects.isNull(shop.getAddress())) {
            throw new BlazeInvalidArgException("Shop", "Shop address does not found for metrc");
        } else {
            if (StringUtils.isNotBlank(shop.getAddress().getState())) {
                stateCode = shop.getAddress().getState();
            }
        }


        if (StringUtils.isNotBlank(stateCode)) {
           MetrcAccount  metrcAccount = metrcAccountRepository.getMetrcAccount(token.getCompanyId(), stateCode);
            if (metrcAccount != null) {
                for (MetrcFacilityAccount facilityAccount : metrcAccount.getFacilities()) {
                    for (ShopResult shopResult : shops.getValues()) {
                        if (shopResult.getId().equals(facilityAccount.getShopId())) {
                            shopResult.setEnableMetrc(facilityAccount.isEnabled() && StringUtils.isNotBlank(facilityAccount.getFacLicense()));

                            LOG.info("Enable metrc: " + shopResult.isEnableMetrc());
                        }
                    }
                }
            }

        }

        MessagingPluginCompanySetting plugin = messagingPluginRepository.getCompanyPlugin(token.getCompanyId());
        if (plugin != null && plugin.getEnabled()) {
            for (ShopResult shopResult : shops.getValues()) {
                if (plugin.getMessagingSetting() != null) {
                    for (MessagingPluginShopSetting setting : plugin.getMessagingSetting()) {
                        if (shopResult.getId().equalsIgnoreCase(setting.getShopId())) {
                            shopResult.setEnableDeliveryMessaging(setting.getEnabled() != null ? setting.getEnabled().booleanValue() : false);
                        }
                    }
                }
            }
        }

        Iterable<ExciseTaxInfo> exciseTaxInfos = exciseTaxInfoRepository.list();
        assignExciseTax(shops.getValues(), exciseTaxInfos);
        return shops;
    }

    private void assignExciseTax(List<ShopResult> shops, Iterable<ExciseTaxInfo> exciseTaxInfos) {
        if (exciseTaxInfos != null) {
            for (ShopResult shop : shops) {
                if (shop != null && shop.getAddress() != null) {
                    if (shop.getAddress().getState() != null && shop.getAddress().getCountry() != null) {
                        for (ExciseTaxInfo exciseTaxInfo : exciseTaxInfos) {
                            if (shop.getAddress().getState().equalsIgnoreCase(exciseTaxInfo.getState())
                                    && shop.getAddress().getCountry().equalsIgnoreCase(exciseTaxInfo.getCountry())) {
                                shop.setExciseTaxInfo(exciseTaxInfo);
                                break;
                            }
                        }
                    }

                }
            }
        }
    }

    @Override
    public Shop addShop(ShopAddRequest request) {

        Employee employee = employeeRepository.get(token.getCompanyId(), token.getActiveTopUser().getUserId());
        if (employee == null) {
            throw new BlazeInvalidArgException(EMPLOYEE, EMPLOYEE_NOT_FOUND);
        }

        Role role = roleRepository.get(token.getCompanyId(), employee.getRoleId());
        if (role == null || !role.getName().equalsIgnoreCase("Admin")) {
            throw new BlazeInvalidArgException(EMPLOYEE, ADD_SHOP_NOT_ALLOWED);
        }


        CompanyFeatures companyFeatures = companyFeaturesRepository.getCompanyFeature(token.getCompanyId());
        if (companyFeatures == null) {
            throw new BlazeInvalidArgException("CompanyFeatures", "Unknown company features.");
        }
        int maxShop = companyFeatures.getMaxShop();
        long activeShopCount = shopRepository.count(token.getCompanyId());
        if (activeShopCount >= maxShop) {
            throw new BlazeInvalidArgException("Shop", "Maximum shop available has been reached.");
        }

        Company company = companyRepository.getById(token.getCompanyId());

        // Check for availableApps
        if (companyFeatures.getAvailableApps() == null || !companyFeatures.getAvailableApps().contains(request.getAppTarget())) {
            throw new BlazeAuthException("Company", company.getName() + " is not configured for " + request.getAppTarget());
        }

        Shop shop = new Shop();
        shop.prepare(token.getCompanyId());
        shop.setName(request.getName());
        shop.setActive(true);
        shop.setTimeZone(ConnectAuthToken.DEFAULT_REQ_TIMEZONE);
        shop.setShopType(request.getShopType());
        shop.setDefaultCountry(request.getDefaultCountry());
        shop.setOnlineStoreInfo(new OnlineStoreInfo());
        shop.setAppTarget(request.getAppTarget());
        Address address = new Address();
        address.prepare(token.getCompanyId());
        shop.setAddress(address);

        shopRepository.save(shop);
        defaultDataService.initializeDefaultShopData(shop, null, false, company);
        return shop;
    }

    @Override
    public CustomShopResult updateShop(String shopId, Shop shop) {
        String apiKey = "";
        //TODO:get the api key from whatever service. We could also just inject the GeoApiContext with Dep. injection.
        GeoApiContext apiContext = new GeoApiContext().setApiKey(apiKey);
        String address;
        GeocodingResult[] results;
        Shop dbShop = shopRepository.get(token.getCompanyId(), shopId);
        if (dbShop == null) {
            throw new BlazeInvalidArgException("Shop", "Shop does not exist.");
        }

        if (dbShop.getAddress() != null) {
            address = dbShop.getAddress().getAddress() + " " + dbShop.getAddress().getCity() + "," +
                    dbShop.getAddress().getState() + " " + dbShop.getAddress().getZipCode();
            try {
                //I figure its better to run this synchronous call in a try/catch block rather than choose to ignore exception
                //that way we can figure out which shops did not successfully get a lat/lon;
                results = GeocodingApi.geocode(apiContext, address).await();
                if (results.length > 0) {
                    dbShop.setLatitude(results[0].geometry.location.lat);
                    dbShop.setLongitude(results[0].geometry.location.lng);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        List<String> oldTagLists = new ArrayList<>();
        for (String dbProductTag : dbShop.getProductsTag()) {
            if (!shop.getProductsTag().contains(dbProductTag)) {
                oldTagLists.add(dbProductTag);
            }
        }

        if (oldTagLists.size() > 0) {
            productRepository.bulkRemoveProductTags(token.getCompanyId(), token.getShopId(), oldTagLists);
        }

        //members tag updated in shop
        List<String> oldMemberTagLists = new ArrayList<>();
        for (String dbMemberTag : dbShop.getMembersTag()) {
            if (!shop.getMembersTag().contains(dbMemberTag)) {
                oldMemberTagLists.add(dbMemberTag);
            }
        }

        if (oldMemberTagLists.size() > 0) {
            memberRepository.bulkRemoveMemberTags(token.getCompanyId(), token.getShopId(), oldMemberTagLists);
        }

        dbShop.setName(shop.getName());
        dbShop.setPhoneNumber(shop.getPhoneNumber());
        dbShop.setAddress(shop.getAddress());
        dbShop.setEmailAdress(shop.getEmailAdress());
        dbShop.setShopType(shop.getShopType());
        dbShop.setTaxInfo(shop.getTaxInfo());
        dbShop.setTaxOrder(shop.getTaxOrder());
        dbShop.setLicense(shop.getLicense());
        dbShop.setShowDeliveryQueue(shop.isShowDeliveryQueue());
        dbShop.setShowWalkInQueue(shop.isShowWalkInQueue());
        dbShop.setShowOnlineQueue(shop.isShowOnlineQueue());
        dbShop.setActive(shop.isActive());
        dbShop.setEnableDeliveryFee(shop.isEnableDeliveryFee());
        dbShop.setDeliveryFee(shop.getDeliveryFee());
        dbShop.setDefaultCountry(shop.getDefaultCountry());
        dbShop.setEnableSaleLogout(shop.isEnableSaleLogout());
        dbShop.setEnableBCCReceipt(shop.isEnableBCCReceipt());
        dbShop.setBccEmailAddress(shop.getBccEmailAddress());
        dbShop.setEnableGPSTracking(shop.isEnableGPSTracking());
        dbShop.setReceivingInventoryId(shop.getReceivingInventoryId());
        dbShop.setDefaultPinTimeout(shop.getDefaultPinTimeout());
        dbShop.setShowSpecialQueue(shop.isShowSpecialQueue());
        dbShop.setTaxRoundOffType(shop.getTaxRoundOffType());
        dbShop.setEmailList(shop.getEmailList());
        dbShop.setEnableLowInventoryEmail(shop.isEnableLowInventoryEmail());
        dbShop.setRestrictedViews(shop.isRestrictedViews());
        dbShop.setNumAllowActiveTrans(shop.getNumAllowActiveTrans());
        dbShop.setRequireValidRecDate(shop.isRequireValidRecDate());
        dbShop.setAllowAnonymousOrders(shop.isAllowAnonymousOrders());

        dbShop.setEmailMessage(shop.getEmailMessage());
        dbShop.setEnforceCashDrawers(shop.isEnforceCashDrawers());
        dbShop.setUseAssignedEmployee(shop.isUseAssignedEmployee());
        dbShop.setTermSalesOption(shop.getTermSalesOption());
        dbShop.setShowProductByAvailableQuantity(shop.isShowProductByAvailableQuantity());

        dbShop.setAutoCashDrawer(shop.isAutoCashDrawer());
        dbShop.setDefaultPinTimeout(shop.getDefaultPinTimeout());
        dbShop.setEnableDeliverySignature(shop.isEnableDeliverySignature());
        dbShop.setEnableCannabisLimit(shop.isEnableCannabisLimit());

        dbShop.setRoundOffType(shop.getRoundOffType());
        dbShop.setRoundUpMessage(shop.getRoundUpMessage());

        dbShop.setRestrictIncomingOrderNotifications(shop.isRestrictIncomingOrderNotifications());
        dbShop.setRestrictedNotificationTerminals(shop.getRestrictedNotificationTerminals());
        dbShop.setAppendBrandNameWM(shop.isAppendBrandNameWM());

        dbShop.setDeliveryFees(shop.getDeliveryFees());
        dbShop.setMarketingSources(shop.getMarketingSources());
        dbShop.setEnableTip(shop.isEnableTip());
        dbShop.setFlowerType(shop.getFlowerType());
        // New Taxes
        dbShop.setUseComplexTax(shop.isUseComplexTax());
        dbShop.setTaxTables(shop.getTaxTables());
        dbShop.setProductsTag(shop.getProductsTag());
        dbShop.setEnableDailySummaryEmail(shop.isEnableDailySummaryEmail());
        dbShop.setCheckDuplicateDl(shop.isCheckDuplicateDl());
        if (dbShop.getTaxTables() != null) {
            for (CompoundTaxTable taxTable : dbShop.getTaxTables()) {
                taxTable.prepare(token.getCompanyId());
                taxTable.setShopId(token.getShopId());
                taxTable.reset();
            }
        }
        if (dbShop.getTaxTables() == null || dbShop.getTaxTables().size() == 0) {
            List<CompoundTaxTable> taxTables = new ArrayList<>();
            dbShop.setTaxTables(taxTables);

            ConsumerType[] consumerTypes = ConsumerType.values();
            for (ConsumerType consumerType : consumerTypes) {
                if (consumerType == ConsumerType.Other) {
                    continue;
                }
                CompoundTaxTable taxTable = new CompoundTaxTable();
                taxTable.prepare(token.getCompanyId());
                taxTable.setShopId(token.getShopId());
                taxTable.setName(consumerType.getDisplayName());
                taxTable.setConsumerType(consumerType);
                taxTables.add(taxTable);
            }
        }

        if (dbShop.getDeliveryFees() == null) {
            dbShop.setDeliveryFees(new ArrayList<DeliveryFee>());
        } else {
            for (DeliveryFee fee : dbShop.getDeliveryFees()) {
                fee.prepare(token.getCompanyId());
                if (fee.getFee() == null) {
                    fee.setFee(new BigDecimal(0));
                }
                if (fee.getSubTotalThreshold() == null) {
                    fee.setSubTotalThreshold(new BigDecimal(0));
                }
            }

            // Order delivery fees
            dbShop.getDeliveryFees().sort(new DeliveryFee.DeliveryFeeComparator());
        }

        dbShop.setCartMinimums(shop.getCartMinimums());
        if (dbShop.getCartMinimums() == null) {
            dbShop.setCartMinimums(new CartMinimums());
            dbShop.getCartMinimums().prepare(token.getCompanyId());
        } else {
            dbShop.getCartMinimums().prepare(token.getCompanyId());
        }

        dbShop.setCreditCardFeeList(shop.getCreditCardFeeList());
        if (dbShop.getCreditCardFeeList() == null) {
            dbShop.setCreditCardFeeList(new ArrayList<CreditCardFee>());
        } else {
            for (CreditCardFee cardFee : dbShop.getCreditCardFeeList()) {
                cardFee.prepare(token.getCompanyId());
            }

            dbShop.getCreditCardFeeList().sort(new CreditCardFee.CreditCardFeeComparator());
        }

        dbShop.setOnlineStoreInfo(shop.getOnlineStoreInfo());
        if (dbShop.getOnlineStoreInfo() != null) {
            dbShop.getOnlineStoreInfo().prepare(token.getCompanyId());
            dbShop.getOnlineStoreInfo().setShopId(dbShop.getId());
        }
        if (dbShop.getTaxInfo() != null) {
            dbShop.getTaxInfo().prepare();
        }

        if (StringUtils.isNotEmpty(shop.getTimeZone())) {
            if (!shop.getTimeZone().equalsIgnoreCase(dbShop.getTimeZone())) {
                try {
                    // test if shop timezone is valid
                    DateTimeZone.forID(shop.getTimeZone());
                    dbShop.setTimeZone(shop.getTimeZone());
                } catch (Exception e) {
                    throw new BlazeInvalidArgException("Shop", "Invalid timezone.");
                }
            }
        }

        List<CompanyAsset> oldCompanyAssets = null;
        if (dbShop.getAssets() != null && dbShop.getAssets().size() > 0) {

            oldCompanyAssets = dbShop.getAssets();
        }
        List<CompanyAsset> requestCompanyAssets = null;
        if (shop.getAssets() != null && shop.getAssets().size() > 0) {

            requestCompanyAssets = shop.getAssets();
        }

        if (dbShop.getCheckoutType() != shop.getCheckoutType()) {
            // checkout type is different, let's make sure nothing is in the queue
            long count = transactionRepository.getAllActiveTransactionsCount(token.getCompanyId(), shopId);
            if (count > 0) {
                throw new BlazeInvalidArgException("TransferRequest", "Please clear all transactions from the queue before changing the Shop's checkout type.");
            }
        }
        dbShop.setCheckoutType(shop.getCheckoutType());

        List<CompanyAsset> newAssetsList = new ArrayList<>();
        final List<CompanyAsset> companyAssets = updateAssets(oldCompanyAssets, requestCompanyAssets, newAssetsList);
        dbShop.setAssets(companyAssets);
        dbShop.setLogo(shop.getLogo());
        dbShop.setEmailAttachment(shop.getEmailAttachment());

        dbShop.setEnableExciseTax(shop.isEnableExciseTax());
        dbShop.setExciseTaxType(shop.getExciseTaxType());

        dbShop.setReceiptInfo(shop.getReceiptInfo());
        dbShop.setEnablePinForCashDrawer(shop.isEnablePinForCashDrawer());
        dbShop.setEnableWooCommerce(shop.isEnableWooCommerce());
        dbShop.setEnableAgeLimit(shop.isEnableAgeLimit());
        dbShop.setEnableMedicinalAge(shop.isEnableMedicinalAge());
        if (shop.isEnableAgeLimit()) {
            if (shop.getAgeLimit() != 0) {
                dbShop.setAgeLimit(shop.getAgeLimit()); // For Adult Use
            } else {
                throw new BlazeInvalidArgException("Shop", "Enter valid age limit for Adult Use.");
            }
        }

        if (shop.isEnableMedicinalAge()) {
            if (shop.getMedicinalAge() != 0) {
                dbShop.setMedicinalAge(shop.getMedicinalAge()); // For MMIC/ThirdParty
            } else {
                throw new BlazeInvalidArgException("Shop", "Enter valid age limit for MMIC/ThirdParty.");
            }
        }

        dbShop.setProductPriceIncludeExciseTax(shop.isProductPriceIncludeExciseTax());
        dbShop.setNalExciseFromRetailCost(shop.isNalExciseFromRetailCost());
        dbShop.setAlExciseOnZeroPrice(shop.isAlExciseOnZeroPrice());
        dbShop.setEnableHarvestTax(shop.isEnableHarvestTax());
        dbShop.setLegalLanguage(shop.getLegalLanguage());
        dbShop.setOrderTags(shop.getOrderTags());
        dbShop.setMembersTag(shop.getMembersTag());

        dbShop.setPoTermCondition(shop.getPoTermCondition());
        dbShop.setPoNote(shop.getPoNote());
        dbShop.setInvoiceTermCondition(shop.getInvoiceTermCondition());
        dbShop.setInvoiceNote(shop.getInvoiceNote());
        dbShop.setEnableRefundNote(shop.isEnableRefundNote());
        dbShop.setDefaultHandOverMin(shop.getDefaultHandOverMin());
        dbShop.setEnableAutoPrintAtFulfillment(shop.isEnableAutoPrintAtFulfillment());
        dbShop.setNonCannabisTaxes(shop.getNonCannabisTaxes());
        dbShop.setDisableMedicalProductsForAdultUse(shop.isDisableMedicalProductsForAdultUse());
        dbShop.setEnableDiscountNotes(shop.isEnableDiscountNotes());
        dbShop.setEnableCartDiscountNotes(shop.isEnableCartDiscountNotes());
        dbShop.setEnableAfterTaxDiscountNotes(shop.isEnableAfterTaxDiscountNotes());
        dbShop.setEnableUnAssignedOnfleetOrder(shop.isEnableUnAssignedOnfleetOrder());

        realtimeService.sendRealTimeEvent(shop.getId(), RealtimeService.RealtimeEventType.ShopInfoUpdateEvent, null);
        Shop updatedShop = shopRepository.update(token.getCompanyId(), shopId, dbShop);
        CustomShopResult customShopResult = null;
        if (updatedShop != null) {
            customShopResult = shopRepository.get(token.getCompanyId(), updatedShop.getId(), CustomShopResult.class);
            if (customShopResult != null) {
                List<ExciseTaxInfo> exciseTaxInfoList = exciseTaxInfoRepository.list();
                this.prepareCustomShopResult(customShopResult, exciseTaxInfoList);
            }
        }
        return customShopResult;
    }

    private List<CompanyAsset> updateAssets(List<CompanyAsset> oldCompanyAssets, List<CompanyAsset> requestCompanyAssets, List<CompanyAsset> newAssetsList) {
        if (requestCompanyAssets != null) {
            for (CompanyAsset asset : requestCompanyAssets) {

                Boolean isPresent = Boolean.FALSE;
                if (oldCompanyAssets != null && oldCompanyAssets.size() > 0) {

                    for (CompanyAsset oldAsset : oldCompanyAssets) {
                        oldAsset.prepare();
                        if (oldAsset.getId().equalsIgnoreCase(asset.getId())) {
                            isPresent = Boolean.TRUE;
                            break;
                        }
                    }
                }

                if (!isPresent) {
                    asset.prepare();
                }
                newAssetsList.add(asset);
            }
        }


        return newAssetsList;
    }

    @Override
    public void sendDailySummaryDataByEmail(EmailNotificationRequest request, String shopId) {
        List<String> emailList = new ArrayList<>();
        if (request != null && StringUtils.isNotBlank(request.getEmail())) {
            emailList = Arrays.asList(request.getEmail().split("\\s*,\\s*"));
        }
        dailySummaryEmail.dataForDailySummary(false, emailList, StringUtils.isNotBlank(shopId) ? shopId : token.getShopId(), token.getRequestTimeZone(), request.getSummaryDate());
    }

    @Override
    public void sendLowInventoryNotificationByEmail(EmailRequest request) {
        List<String> emailList = new ArrayList<>();
        if (request != null && StringUtils.isNotBlank(request.getEmail())) {
            emailList = Arrays.asList(request.getEmail().split("\\s*,\\s*"));
        }
        lowInventoryNotification.dataForLowInventoryNotification(token.getShopId(), false, emailList);
    }

    @Override
    public Shop updateShopData(String shopId, ShopUpdateRequest request) {

        Shop shop = shopRepository.get(token.getCompanyId(), shopId);

        if (shop == null) {
            throw new BlazeInvalidArgException(SHOP, SHOP_NOT_FOUND);
        }

        Map<String, Object> shopInfoMap = request.getShopInfoMap();

        if (shopInfoMap == null || shopInfoMap.size() == 0) {
            throw new BlazeInvalidArgException(SHOP, "Please add some data to update");
        }


        Iterator<Map.Entry<String, Object>> iterator = shopInfoMap.entrySet().iterator();

        ObjectMapper objectMapper = new ObjectMapper();
        while (iterator.hasNext()) {

            Map.Entry<String, Object> entry = iterator.next();

            if ("taxTables".equals(entry.getKey())) {
                List<CompoundTaxTable> taxTables = null;
                try {
                    taxTables = objectMapper.readValue(objectMapper.writeValueAsString(entry.getValue()), new TypeReference<List<CompoundTaxTable>>() {
                    });
                } catch (Exception e) {
                    throw new BlazeInvalidArgException(SHOP, ERROR_UPDATE);
                }

                if (taxTables != null) {
                    for (CompoundTaxTable taxTable : taxTables) {
                        taxTable.prepare(token.getCompanyId());
                        taxTable.setShopId(token.getShopId());
                        taxTable.reset();
                    }

                    if (taxTables.size() == 0) {
                        ConsumerType[] consumerTypes = ConsumerType.values();
                        for (ConsumerType consumerType : consumerTypes) {
                            if (consumerType == ConsumerType.Other) {
                                continue;
                            }
                            CompoundTaxTable taxTable = new CompoundTaxTable();
                            taxTable.prepare(token.getCompanyId());
                            taxTable.setShopId(token.getShopId());
                            taxTable.setName(consumerType.getDisplayName());
                            taxTable.setConsumerType(consumerType);
                            taxTables.add(taxTable);
                        }
                    }
                }

                shopInfoMap.put("taxTables", taxTables);
            } else if ("deliveryFees".equals(entry.getKey())) {
                List<DeliveryFee> deliveryFees = null;
                try {
                    deliveryFees = objectMapper.readValue(objectMapper.writeValueAsString(entry.getValue()), new TypeReference<List<DeliveryFee>>() {
                    });
                } catch (Exception e) {
                    throw new BlazeInvalidArgException(SHOP, ERROR_UPDATE);
                }


                if (deliveryFees == null) {
                    deliveryFees = new ArrayList<DeliveryFee>();
                } else {
                    for (DeliveryFee fee : deliveryFees) {
                        fee.prepare(token.getCompanyId());
                        if (fee.getFee() == null) {
                            fee.setFee(new BigDecimal(0));
                        }
                        if (fee.getSubTotalThreshold() == null) {
                            fee.setSubTotalThreshold(new BigDecimal(0));
                        }
                    }

                    // Order delivery fees
                    deliveryFees.sort(new DeliveryFee.DeliveryFeeComparator());
                }

                shopInfoMap.put("deliveryFees", deliveryFees);
            } else if ("cartMinimums".equals(entry.getKey())) {
                CartMinimums cartMinimums = null;
                try {
                    cartMinimums = objectMapper.readValue(objectMapper.writeValueAsString(entry.getValue()), CartMinimums.class);
                } catch (IOException e) {
                    throw new BlazeInvalidArgException(SHOP, ERROR_UPDATE);
                }

                if (cartMinimums == null) {
                    cartMinimums = new CartMinimums();
                }
                cartMinimums.prepare(token.getCompanyId());

                try {
                    shopInfoMap.put("cartMinimums", cartMinimums);
                } catch (Exception e) {
                    throw new BlazeInvalidArgException(SHOP, ERROR_UPDATE);
                }
            } else if ("creditCardFeeList".equals(entry.getKey())) {
                List<CreditCardFee> creditCardFeeList = null;
                try {
                    creditCardFeeList = objectMapper.readValue(objectMapper.writeValueAsString(entry.getValue()), new TypeReference<List<CreditCardFee>>() {
                    });
                } catch (Exception e) {
                    throw new BlazeInvalidArgException(SHOP, ERROR_UPDATE);
                }

                if (creditCardFeeList == null) {
                    creditCardFeeList = new ArrayList<>();
                } else {
                    for (CreditCardFee cardFee : creditCardFeeList) {
                        cardFee.prepare(token.getCompanyId());
                    }
                }

                creditCardFeeList.sort(new CreditCardFee.CreditCardFeeComparator());

                shopInfoMap.put("creditCardFeeList", creditCardFeeList);
            } else if ("onlineStoreInfo".equals(entry.getKey())) {
                OnlineStoreInfo onlineStoreInfo = null;

                try {
                    onlineStoreInfo = objectMapper.readValue(objectMapper.writeValueAsString(entry.getValue()), OnlineStoreInfo.class);
                } catch (IOException e) {
                    throw new BlazeInvalidArgException(SHOP, ERROR_UPDATE);
                }

                if (onlineStoreInfo != null) {
                    onlineStoreInfo.prepare(token.getCompanyId());
                    onlineStoreInfo.setShopId(shopId);
                    shopInfoMap.put("onlineStoreInfo", onlineStoreInfo);
                }
            } else if ("taxInfo".equals(entry.getKey())) {
                TaxInfo taxInfo = null;
                try {
                    taxInfo = objectMapper.readValue(objectMapper.writeValueAsString(entry.getValue()), TaxInfo.class);
                } catch (IOException e) {
                    throw new BlazeInvalidArgException(SHOP, ERROR_UPDATE);
                }

                if (taxInfo != null) {
                    taxInfo.prepare();

                    shopInfoMap.put("taxInfo", taxInfo);
                }
            } else if ("timeZone".equals(entry.getKey())) {
                String timeZone = (String) entry.getValue();

                if (StringUtils.isNotEmpty(timeZone)) {
                    if (!shop.getTimeZone().equalsIgnoreCase(timeZone)) {
                        try {
                            DateTimeZone.forID(timeZone);

                        } catch (Exception e) {
                            throw new BlazeInvalidArgException("Shop", "Invalid timezone.");
                        }
                    }
                }
            } else if ("assets".equals(entry.getKey())) {
                List<CompanyAsset> oldCompanyAssets = null;
                if (shop.getAssets() != null && shop.getAssets().size() > 0) {

                    oldCompanyAssets = shop.getAssets();
                }
                List<CompanyAsset> assets = null;
                try {
                    assets = objectMapper.readValue(objectMapper.writeValueAsString(entry.getValue()), new TypeReference<List<CompanyAsset>>() {
                    });
                } catch (Exception e) {
                    throw new BlazeInvalidArgException(SHOP, ERROR_UPDATE);
                }
                List<CompanyAsset> requestCompanyAssets = null;
                if (assets != null && assets.size() > 0) {

                    requestCompanyAssets = shop.getAssets();
                }
                List<CompanyAsset> newAssetsList = new ArrayList<>();
                final List<CompanyAsset> companyAssets = updateAssets(oldCompanyAssets, requestCompanyAssets, newAssetsList);

                shopInfoMap.put("assets", companyAssets);
            } else if ("enableAgeLimit".equals(entry.getKey())) {
                Boolean enableAgeLimit = (Boolean) entry.getValue();
                Integer ageLimit = (Integer) shopInfoMap.get("ageLimit");
                if (enableAgeLimit) {
                    if (ageLimit != null || ageLimit != 0) {
                        throw new BlazeInvalidArgException("Shop", "Enter valid age limit for Adult Use.");
                    }
                }
            } else if ("enableMedicinalAge".equals(entry.getKey())) {
                Boolean enableMedicinalAge = (Boolean) entry.getValue();
                Integer medicinalAge = (Integer) shopInfoMap.get("medicinalAge");

                if (enableMedicinalAge) {
                    if (medicinalAge == null || medicinalAge != 0) {
                        throw new BlazeInvalidArgException("Shop", "Enter valid age limit for MMIC/ThirdParty.");
                    }
                }
            }
        }

        shopInfoMap.put("modified", DateTime.now().getMillis());

        shopRepository.updateShopInfo(token.getCompanyId(), shopId, shopInfoMap);

        return shopRepository.get(token.getCompanyId(), shopId);
    }

    @Override
    public List<ShopResult> getBlazeConnectionsByShopId(String shopId) {
        List<ShopResult> shops = new ArrayList<>();
        Shop shop = shopRepository.get(token.getCompanyId(), shopId);
        if(shop != null){
            Iterable<ShopResult> items = shopRepository.getAllShopsInList(shop.getBlazeConnections());
            shops = Lists.newArrayList(items);
        }
        
        return shops;
    }
}
