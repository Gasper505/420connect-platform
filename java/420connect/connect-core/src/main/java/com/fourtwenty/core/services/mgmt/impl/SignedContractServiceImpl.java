package com.fourtwenty.core.services.mgmt.impl;

import com.fourtwenty.core.domain.models.company.SignedContract;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.mgmt.SignedContractService;
import com.google.inject.Inject;
import com.google.inject.Provider;

import java.util.List;

/**
 * Created by Stephen Schmidt on 12/28/2015.
 */
public class SignedContractServiceImpl extends AbstractAuthServiceImpl implements SignedContractService {


    @Inject
    public SignedContractServiceImpl(Provider<ConnectAuthToken> token) {
        super(token);
    }

    @Override
    public List<SignedContract> getSignedContracts() {
        //return signedContractRepository.getSignedContracts(token.getCompanyId(), token.getShopId());
        return null;
    }

    @Override
    public SignedContract getSignedContractByMembership(String memberId, Boolean active) {
        //return signedContractRepository.getSignedContractByMembership(token.getCompanyId(),
        //        token.getShopId(), memberId, active);
        return null;
    }

}
