package com.fourtwenty.core.services.mgmt.impl;

import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.thirdparty.SpringBigInfo;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.domain.repositories.dispensary.SpringbigRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.mgmt.SpringBigService;
import com.fourtwenty.core.thirdparty.springbig.models.SpringMemberResponse;
import com.fourtwenty.core.thirdparty.springbig.services.SpringBigAPIService;
import com.google.common.collect.Lists;
import com.google.inject.Provider;
import org.apache.commons.lang.StringUtils;

import javax.inject.Inject;
import java.util.List;

public class SpringBigServiceImpl extends AbstractAuthServiceImpl implements SpringBigService {

    private static final String SHOP = "Shop";
    private static final String SPRINGBIG = "SpringBig";
    private static final String SHOP_NOT_FOUND = "Shop does not found";
    private static final String SPRINGBIG_NOT_FOUND = "SpringBig info does not found";
    private static final String SPRINGBIG_EXIST = "SpringBig info already exist for requested shop";

    @Inject
    private SpringbigRepository springbigRepository;
    @Inject
    private ShopRepository shopRepository;
    @Inject
    SpringBigAPIService springBigAPIService;

    @Inject
    public SpringBigServiceImpl(Provider<ConnectAuthToken> token) {
        super(token);
    }

    /**
     * Get springbig info by id
     *
     * @param id : id of springbig
     */
    @Override
    public SpringBigInfo getSpringInfoById(String id) {
        return springbigRepository.get(token.getCompanyId(), id);
    }

    /**
     * Gets list of springbig info by company
     *
     * @return : list of springbig info per shop
     */
    @Override
    public List<SpringBigInfo> getSpringBigInfoList() {
        return Lists.newArrayList(springbigRepository.list(token.getCompanyId()));
    }

    /**
     * Update springbig info by shop
     *
     * @param request : request data
     * @return : updated springbig object
     */
    @Override
    public SpringBigInfo updateSpringBigInfoByShop(SpringBigInfo request) {

        if (StringUtils.isEmpty(request.getShopId())) {
            throw new BlazeInvalidArgException(SHOP, SHOP_NOT_FOUND);
        }

        Shop shop = shopRepository.get(token.getCompanyId(), request.getShopId());
        if (shop == null) {
            throw new BlazeInvalidArgException(SHOP, SHOP_NOT_FOUND);
        }

        SpringBigInfo springBigInfo = springbigRepository.getInfoByShop(token.getCompanyId(), request.getShopId());

        if (springBigInfo == null) {
            throw new BlazeInvalidArgException(SPRINGBIG, SPRINGBIG_NOT_FOUND);
        }

        springBigInfo.prepare(token.getCompanyId());
        springBigInfo.setShopId(request.getShopId());
        springBigInfo.setActive(request.isActive());
        springBigInfo.setAuthToken(request.getAuthToken());
        springBigInfo.setEnv(request.getEnv());
        if (request.isActive()) {
            if (StringUtils.isBlank(request.getAuthToken())) {
                throw new BlazeInvalidArgException(SPRINGBIG, "Auth token needs to be specified.");
            }
        }

        return springbigRepository.update(token.getCompanyId(), springBigInfo.getId(), springBigInfo);
    }

    /**
     * Gets springbig info by shop
     *
     * @param shopId : shop id
     */
    @Override
    public SpringBigInfo getSpringBigInfoByShop(String shopId) {
        SpringBigInfo infoByShop = springbigRepository.getInfoByShop(token.getCompanyId(), shopId);

        if (infoByShop == null) {
            throw new BlazeInvalidArgException(SPRINGBIG, SPRINGBIG_NOT_FOUND);
        }

        return infoByShop;
    }

    /**
     * Create springbig object per shop
     *
     * @param request : springbig request
     */
    @Override
    public SpringBigInfo createSpringBigInfoByShop(SpringBigInfo request) {
        if (StringUtils.isEmpty(request.getShopId())) {
            throw new BlazeInvalidArgException(SHOP, SHOP_NOT_FOUND);
        }

        Shop shop = shopRepository.get(token.getCompanyId(), request.getShopId());
        if (shop == null) {
            throw new BlazeInvalidArgException(SHOP, SHOP_NOT_FOUND);
        }

        SpringBigInfo springBigInfo = springbigRepository.getInfoByShop(token.getCompanyId(), request.getShopId());

        if (springBigInfo != null) {
            throw new BlazeInvalidArgException(SPRINGBIG, SPRINGBIG_EXIST);
        }

        springBigInfo = new SpringBigInfo();

        springBigInfo.prepare(token.getCompanyId());
        springBigInfo.setShopId(request.getShopId());
        springBigInfo.setActive(request.isActive());
        springBigInfo.setAuthToken(request.getAuthToken());
        springBigInfo.setEnv(request.getEnv());

        return springbigRepository.save(springBigInfo);
    }


    @Override
    public SpringMemberResponse getSpringBigMemberById(String memberId) {
        SpringBigInfo springBigInfo = springbigRepository.getInfoByShop(token.getCompanyId(), token.getShopId());
        if (springBigInfo != null && springBigInfo.isActive()) {
            return springBigAPIService.getMemberByByMemberId(springBigInfo, memberId);
        }
        return null;
    }
}
