package com.fourtwenty.core.services.mgmt.impl;

//import com.amazonaws.services.cloudsearchdomain.model.SearchResult;
import com.amazonaws.util.CollectionUtils;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.loyalty.StorewideSale;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductPriceBreak;
import com.fourtwenty.core.domain.models.product.ProductPriceRange;
import com.fourtwenty.core.rest.dispensary.requests.promotions.StorewideSaleAddRequest;
import com.fourtwenty.core.rest.dispensary.requests.promotions.StorewideSaleRequest;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.domain.repositories.loyalty.StorewideSaleRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.StorewideSaleResult;
import com.fourtwenty.core.rest.store.results.ProductWithInfo;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.common.RealtimeService;
import com.fourtwenty.core.services.mgmt.StorewideSaleService;
import com.google.inject.Inject;
import com.google.inject.Provider;
import java.math.BigDecimal;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import java.util.*;

/**
 * Created on 9/10/19.
 */
public class StorewideSaleServiceImpl extends AbstractAuthServiceImpl implements StorewideSaleService {

    private static final Log LOG = LogFactory.getLog(StorewideSaleServiceImpl.class);
    @Inject
    TransactionRepository transactionRepository;
    @Inject
    ProductRepository productRepository;
    @Inject
    ShopRepository shopRepository;
    @Inject
    RealtimeService realtimeService;
    @Inject
    MemberRepository memberRepository;
    @Inject
    StorewideSaleRepository storewideSaleRepository;

    @Inject
    public StorewideSaleServiceImpl(Provider<ConnectAuthToken> tokenProvider) {
        super(tokenProvider);
    }

    @Override
    public StorewideSale addStorewideSale(StorewideSaleAddRequest request) {
        StorewideSale storewideSale = storewideSaleRepository.getStorewideSaleByName(token.getCompanyId(), token.getShopId(), request.getName());
        if (storewideSale != null) {
            throw new BlazeInvalidArgException("NAME", "Storewide Sale already exist with this name");
        }

        StorewideSale sale = new StorewideSale();
        sale.setName(request.getName());
        sale.prepare(token.getCompanyId());
        sale.setShopId(token.getShopId());
        sale.setDiscountAmt(request.getDiscountAmt());
        sale.setActive(request.isActive());
        sale.setDiscountType(request.getDiscountType());

        storewideSaleRepository.save(sale);
        realtimeService.sendRealTimeEvent(token.getShopId().toString(), RealtimeService.RealtimeEventType.StorewideSaleUpdateEvent, null);
        return sale;
    }

    @Override
    public StorewideSaleResult updateStorewideSale(String storewideSaleId, StorewideSaleRequest storewideSale) {
        StorewideSaleResult dbSale = storewideSaleRepository.get(token.getCompanyId(), storewideSaleId, StorewideSaleResult.class);

        if (dbSale == null) {
            throw new BlazeInvalidArgException("StorewideSale", "Storewide Sale not found.");
        }

        dbSale.setName(storewideSale.getName());
        dbSale.setActive(storewideSale.isActive());
        dbSale.setSaleDesc(storewideSale.getSaleDesc());
        dbSale.setDiscountAmt(storewideSale.getDiscountAmt());
        dbSale.setDiscountType(storewideSale.getDiscountType());
        dbSale.setBrandsIds(storewideSale.getBrandsIds());

        if (dbSale.getBrandsIds() == null) {
            dbSale.setBrandsIds(new LinkedHashSet<>());
        }

        storewideSaleRepository.update(token.getCompanyId(), dbSale.getId(), dbSale);
        realtimeService.sendRealTimeEvent(token.getShopId().toString(), RealtimeService.RealtimeEventType.StorewideSaleUpdateEvent, null);
        return dbSale;
    }

    @Override
    public void deleteStorewideSale(String storewideSaleId) {

        storewideSaleRepository.removeByIdSetState(token.getCompanyId(), storewideSaleId);
        realtimeService.sendRealTimeEvent(token.getShopId().toString(), RealtimeService.RealtimeEventType.StorewideSaleUpdateEvent, null);
    }

    @Override
    public StorewideSaleResult getStorewideSaleById(String storewideSaleId) {
        StorewideSaleResult storewideSaleResult = storewideSaleRepository.get(token.getCompanyId(), storewideSaleId, StorewideSaleResult.class);
        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());

        if (storewideSaleResult == null) {
            throw new BlazeInvalidArgException("StorewideSale", "Storewide Sale not found");
        }

        if (shop == null) {
            throw new BlazeInvalidArgException("Shop", "Shop is not found");
        }

        return storewideSaleResult;
    }

    @Override
    public DateSearchResult<StorewideSale> getStorewideSalesByDate(long afterDate, long beforeDate) {
        return storewideSaleRepository.findItemsWithDate(token.getCompanyId(), token.getShopId(), afterDate, beforeDate);
    }

    @Override
    public SearchResult<StorewideSale> getStorewideSales(int start, int limit) {
        return storewideSaleRepository.findItems(token.getCompanyId(), token.getShopId(), "{name:1}", start, limit);
    }

    private double getSalesPrice(double price, double discountAmount) {
        return price * (1 - discountAmount / 100);
    }

    @Override
    public StorewideSaleResult enableStorewideSale(String storewideSaleId) {
        StorewideSaleResult storewideSaleResult = storewideSaleRepository.get(token.getCompanyId(), storewideSaleId, StorewideSaleResult.class);
        int activeCount = storewideSaleRepository.getStorewideSalesActiveCount(token.getCompanyId(), token.getShopId());

        if (activeCount > 0) {
            throw new BlazeInvalidArgException("StorewideSale", "There is an active Storewide Sale already");
        }
        if (storewideSaleResult == null) {
            throw new BlazeInvalidArgException("StorewideSale", "Storewide Sale not found");
        }
        storewideSaleResult.setActive(true);
        storewideSaleRepository.update(storewideSaleId, storewideSaleResult);

        if (!CollectionUtils.isNullOrEmpty(storewideSaleResult.getBrandsIds())) {
            double discountedAmount = storewideSaleResult.getDiscountAmt().doubleValue();
            SearchResult<ProductWithInfo> products = new SearchResult<>();
            List<String> tags = new ArrayList<>();
            //Loop for all Brands
            for (String brandId : storewideSaleResult.getBrandsIds()) {
                products = productRepository.findProductsByBrand(token.getCompanyId(), token.getShopId(), tags, brandId,
                        "{name:1}", 0, 0, ProductWithInfo.class);
                if (!CollectionUtils.isNullOrEmpty(products.getValues())) {
                    //Loop for all Products
                    for (ProductWithInfo product : products.getValues()) {
                        //Loop for all Price Breaks
                        List<ProductPriceBreak> priceBreakList = new ArrayList<>();
                        if (!CollectionUtils.isNullOrEmpty(product.getPriceBreaks())) {
                            for (ProductPriceBreak priceBreak : product.getPriceBreaks()) {
                                double price = priceBreak.getPrice() != null ? priceBreak.getPrice().doubleValue() : 0;
                                double salesPrice = getSalesPrice(price, discountedAmount);
                                priceBreak.setSalePrice(new BigDecimal(salesPrice));
                                priceBreakList.add(priceBreak);
                            }
                        }
                        //Loop for all Price Ranges
                        List<ProductPriceRange> priceRangeList = new ArrayList<>();
                        if (!CollectionUtils.isNullOrEmpty(product.getPriceRanges())) {
                            for (ProductPriceRange priceRange : product.getPriceRanges()) {
                                double price = priceRange.getPrice() != null ? priceRange.getPrice().doubleValue() : 0;
                                double salesPrice = getSalesPrice(price, discountedAmount);
                                priceRange.setSalePrice(new BigDecimal(salesPrice));
                                priceRangeList.add(priceRange);
                            }
                        }
                        product.setPriceRanges(priceRangeList);
                        product.setPriceBreaks(priceBreakList);
                        productRepository.update(product.getId(), product);
                    }
                }
            }
        }else{
            throw new BlazeInvalidArgException("StorewideSale", "The Storewide Sale should have a brand at least");
        }

        return storewideSaleResult;
    }

    @Override
    public StorewideSaleResult disableStorewideSale(String storewideSaleId) {
        StorewideSaleResult storewideSaleResult = storewideSaleRepository.get(token.getCompanyId(), storewideSaleId, StorewideSaleResult.class);

        if (storewideSaleResult == null) {
            throw new BlazeInvalidArgException("StorewideSale", "Storewide Sale not found");
        }

        if (!storewideSaleResult.isActive()) {
            throw new BlazeInvalidArgException("StorewideSale", "Storewide is already inactive");
        }

        storewideSaleResult.setActive(false);
        storewideSaleRepository.update(storewideSaleId, storewideSaleResult);

        if (!CollectionUtils.isNullOrEmpty(storewideSaleResult.getBrandsIds())) {
            SearchResult<ProductWithInfo> products = new SearchResult<>();
            List<String> tags = new ArrayList<>();
            //Loop for all Brands
            for (String brandId : storewideSaleResult.getBrandsIds()) {
                products = productRepository.findProductsByBrand(token.getCompanyId(), token.getShopId(), tags, brandId,
                        "{name:1}", 0, 0, ProductWithInfo.class);
                if (!CollectionUtils.isNullOrEmpty(products.getValues())) {
                    //Loop for all Products
                    for (ProductWithInfo product : products.getValues()) {
                        List<ProductPriceBreak> priceBreakList = new ArrayList<>();
                        //Loop for all Price Breaks
                        if (!CollectionUtils.isNullOrEmpty(product.getPriceBreaks())) {
                            for (ProductPriceBreak priceBreak : product.getPriceBreaks()) {
                                priceBreak.setSalePrice(null);
                                priceBreakList.add(priceBreak);
                            }
                        }
                        //Loop for all Price Ranges
                        List<ProductPriceRange> priceRangeList = new ArrayList<>();
                        if (!CollectionUtils.isNullOrEmpty(product.getPriceRanges())) {
                            for (ProductPriceRange priceRange : product.getPriceRanges()) {
                                priceRange.setSalePrice(null);
                                priceRangeList.add(priceRange);
                            }
                        }
                        product.setPriceRanges(priceRangeList);
                        product.setPriceBreaks(priceBreakList);
                        productRepository.update(product.getId(), product);
                    }
                }
            }
        }

        return storewideSaleResult;
    }

    @Override
    public Product applyActiveStorewideSaleForProduct(Product product) {
        StorewideSale storewideSale = storewideSaleRepository.getActiveStorewideSale(token.getCompanyId(), token.getShopId());
        
        if (storewideSale != null) {
            double discountedAmount = storewideSale.getDiscountAmt().doubleValue();
            
            if (storewideSale.getBrandsIds().contains(product.getBrandId())) {
                //Loop for all Price Breaks
                List<ProductPriceBreak> priceBreakList = new ArrayList<>();
                if (!CollectionUtils.isNullOrEmpty(product.getPriceBreaks())) {
                    for (ProductPriceBreak priceBreak : product.getPriceBreaks()) {
                        double price = priceBreak.getPrice() != null ? priceBreak.getPrice().doubleValue() : 0;
                        double salesPrice = getSalesPrice(price, discountedAmount);
                        priceBreak.setSalePrice(new BigDecimal(salesPrice));
                        priceBreakList.add(priceBreak);
                    }
                }
                //Loop for all Price Ranges
                List<ProductPriceRange> priceRangeList = new ArrayList<>();
                if (!CollectionUtils.isNullOrEmpty(product.getPriceRanges())) {
                    for (ProductPriceRange priceRange : product.getPriceRanges()) {
                        double price = priceRange.getPrice() != null ? priceRange.getPrice().doubleValue() : 0;
                        double salesPrice = getSalesPrice(price, discountedAmount);
                        priceRange.setSalePrice(new BigDecimal(salesPrice));
                        priceRangeList.add(priceRange);
                    }
                }
                product.setPriceRanges(priceRangeList);
                product.setPriceBreaks(priceBreakList);
                productRepository.update(product.getId(), product);
            }
        }
        
        return product;
    }
}
