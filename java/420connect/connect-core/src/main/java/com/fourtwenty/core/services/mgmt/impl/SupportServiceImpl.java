package com.fourtwenty.core.services.mgmt.impl;

import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.repositories.dispensary.CompanyRepository;
import com.fourtwenty.core.domain.repositories.dispensary.EmployeeRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.managed.AmazonServiceManager;
import com.fourtwenty.core.rest.dispensary.requests.support.SupportSendRequest;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.mgmt.SupportService;
import com.google.inject.Inject;
import com.google.inject.Provider;
import org.apache.commons.io.IOUtils;
import org.joda.time.DateTime;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.regex.Pattern;

/**
 * Created by mdo on 8/10/16.
 */
public class SupportServiceImpl extends AbstractAuthServiceImpl implements SupportService {
    @Inject
    AmazonServiceManager amazonServiceManager;
    @Inject
    CompanyRepository companyRepository;
    @Inject
    EmployeeRepository employeeRepository;


    @Inject
    public SupportServiceImpl(Provider<ConnectAuthToken> token) {
        super(token);
    }


    @Override
    public void sendSupportEmail(SupportSendRequest request) {
        Company company = companyRepository.getById(token.getCompanyId());
        if (company == null) {
            throw new BlazeInvalidArgException("Company", "Company does not exist.");
        }
        Employee employee = employeeRepository.get(token.getCompanyId(), token.getActiveTopUser().getUserId());

        String body = getBody(employee, company, request.getBody());
        amazonServiceManager.sendEmail("support@blaze.me", "support@blaze.me", "Support email from: " + company.getName(), body, null, company.getEmail(), company.getName());
    }

    private String getBody(Employee employee, Company company, String body) {
        InputStream inputStream = ShopServiceImpl.class.getResourceAsStream("/support.html");
        StringWriter writer = new StringWriter();
        try {
            IOUtils.copy(inputStream, writer, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        String theString = writer.toString();

        theString = theString.replaceAll(Pattern.quote("{{name}}"), employee.getFirstName() + " " + employee.getLastName());
        theString = theString.replaceAll(Pattern.quote("{{companyId}}"), company.getId());
        theString = theString.replaceAll(Pattern.quote("{{date}}"), DateTime.now().toString());
        theString = theString.replaceAll(Pattern.quote("{{email}}"), company.getEmail());
        theString = theString.replaceAll(Pattern.quote("{{body}}"), body);

        return theString;
    }
}
