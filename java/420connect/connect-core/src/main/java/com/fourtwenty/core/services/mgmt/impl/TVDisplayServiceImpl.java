package com.fourtwenty.core.services.mgmt.impl;

import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.company.Role;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.company.TaxInfo;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.generic.CompanyUniqueSequence;
import com.fourtwenty.core.domain.models.plugins.TVDisplay;
import com.fourtwenty.core.domain.models.plugins.TVDisplayContent;
import com.fourtwenty.core.domain.models.plugins.TVPluginCompanySetting;
import com.fourtwenty.core.domain.models.product.Brand;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.models.views.MemberLimitedView;
import com.fourtwenty.core.domain.repositories.dispensary.BrandRepository;
import com.fourtwenty.core.domain.repositories.dispensary.CompanyRepository;
import com.fourtwenty.core.domain.repositories.dispensary.CompanyUniqueSequenceRepository;
import com.fourtwenty.core.domain.repositories.dispensary.EmployeeRepository;
import com.fourtwenty.core.domain.repositories.dispensary.MemberRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductCategoryRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.dispensary.RoleRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TVDisplayRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.domain.repositories.dispensary.UniqueSequenceRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.exceptions.BlazeOperationException;
import com.fourtwenty.core.rest.dispensary.requests.plugins.TVDisplayAddRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.PurchaseBrand;
import com.fourtwenty.core.rest.dispensary.results.inventory.PurchaseProduct;
import com.fourtwenty.core.rest.dispensary.results.tvdisplay.EmployeeStatsResponse;
import com.fourtwenty.core.rest.dispensary.results.tvdisplay.EmployeeStatsResult;
import com.fourtwenty.core.rest.dispensary.results.tvdisplay.SalesByBrand;
import com.fourtwenty.core.rest.dispensary.results.tvdisplay.SalesByCategory;
import com.fourtwenty.core.rest.marketing.TVDisplayResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.mgmt.TVDisplayService;
import com.fourtwenty.core.services.plugins.TVPluginService;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.core.util.NumberUtils;
import com.fourtwenty.core.util.RandomUtil;
import com.google.common.collect.Lists;
import com.google.inject.Provider;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * Created by decipher on 14/10/17 2:42 PM
 * Abhishek Samuel (Software Engineer)
 * abhishke.decipher@gmail.com
 * Decipher Zone Softwares
 * www.decipherzone.com
 */
public class TVDisplayServiceImpl extends AbstractAuthServiceImpl implements TVDisplayService {
    private static final Log LOG = LogFactory.getLog(TVDisplayServiceImpl.class);
    private static final String TV_NOT_FOUND = "TV display not found";
    private static final String NO_PRODUCT = "No Products found for this category";
    private static final String TV_DISPLAY = "TV Display";
    private static final String CATEGORY_NOT_FOUND = "Product category not found";
    private static final String TV_NOT_PUBLISHED = "TV display is not published";
    private static final String CONTENT_NOT_FOUND = "TV display content not found";
    private static final String SHOP = "Shop";
    private static final String SHOP_NOT_FOUND = "Shop not found";

    private TVDisplayRepository tvDisplayRepository;
    private UniqueSequenceRepository uniqueSequenceRepository;
    private CompanyUniqueSequenceRepository companyUniqueSequenceRepository;
    private MemberRepository memberRepository;
    private TransactionRepository transactionRepository;
    private ProductRepository productRepository;
    private TVPluginService tvPluginService;
    @Inject
    private ShopRepository shopRepository;
    @Inject
    private ProductCategoryRepository productCategoryRepository;
    @Inject
    private CompanyRepository companyRepository;
    @Inject
    private EmployeeRepository employeeRepository;
    @Inject
    private BrandRepository brandRepository;
    @Inject
    private RoleRepository roleRepository;
    @Inject
    public TVDisplayServiceImpl(Provider<ConnectAuthToken> tokenProvider, TVDisplayRepository tvDisplayRepository,
                                UniqueSequenceRepository uniqueSequenceRepository,
                                CompanyUniqueSequenceRepository companyUniqueSequenceRepository,
                                MemberRepository memberRepository,
                                TransactionRepository transactionRepository,
                                ProductRepository productRepository,
                                TVPluginService tvPluginService) {
        super(tokenProvider);
        this.tvDisplayRepository = tvDisplayRepository;
        this.uniqueSequenceRepository = uniqueSequenceRepository;
        this.companyUniqueSequenceRepository = companyUniqueSequenceRepository;
        this.memberRepository = memberRepository;
        this.transactionRepository = transactionRepository;
        this.productRepository = productRepository;
        this.tvPluginService = tvPluginService;
    }

    /**
     * This method add process of tv display
     *
     * @param request : request object for adding tv display
     */
    @Override
    public TVDisplay addTVDisplay(TVDisplayAddRequest request) {
        TVDisplay dbTVvDisplay;
        TVPluginCompanySetting tvPluginCompanySetting = tvPluginService.getCompanyTVPlugin();

        if (tvPluginCompanySetting != null && tvPluginCompanySetting.getEnabled()) {
            CompanyUniqueSequence companyUniqueSequence = companyUniqueSequenceRepository.getNewIdentifier(token.getCompanyId(), "TVDisplay");

            Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());
            String newIdentifier = RandomUtil.getIdentifier().toLowerCase(); // FIRESTICK BROWSERS ARE LOWER CASE
            while (true) {
                // let's make sure there's current identifier
                boolean exists = shopRepository.existsWithIdentifier(newIdentifier);
                if (exists) {
                    // get new identifier
                    newIdentifier = RandomUtil.getIdentifier().toLowerCase();
                } else {
                    shop.setShortIdentifier(newIdentifier);
                    shopRepository.setShopShortIdentifier(token.getCompanyId(), token.getShopId(), newIdentifier);
                    break;
                }
            }


            TVDisplay tvDisplay = new TVDisplay();
            tvDisplay.prepare(token.getCompanyId());
            tvDisplay.setShopId(token.getShopId());
            tvDisplay.setName(request.getName());
            tvDisplay.setTheme(TVDisplay.TVDisplayTheme.Light);
            tvDisplay.setOrientation(TVDisplay.TVDisplayOrientation.Landscape);
            tvDisplay.setColumn(TVDisplay.TVDisplayColumn.Column_2);
            tvDisplay.setRotationTime(1);
            tvDisplay.setPublished(false);
            tvDisplay.setTvNumber(companyUniqueSequence.getCount());
            tvDisplay.setIdentifier(newIdentifier);

            List<TVDisplayContent> tvDisplayContents = new ArrayList<>();
            tvDisplay.setContentList(tvDisplayContents);

            dbTVvDisplay = tvDisplayRepository.save(tvDisplay);

            return dbTVvDisplay;
        } else {
            throw new BlazeOperationException("TV Plugin is not enabled for your company.");
        }

    }

    /**
     * Update tv display setting by id
     *
     * @param tvDisplayId       : tv display id
     * @param incomingTVDisplay
     */
    @Override
    public TVDisplay updateTVDisplay(String tvDisplayId, TVDisplay incomingTVDisplay) {
        TVPluginCompanySetting tvPluginCompanySetting = tvPluginService.getCompanyTVPlugin();
        if (tvPluginCompanySetting != null && tvPluginCompanySetting.getEnabled()) {
            TVDisplay tvDisplay = tvDisplayRepository.get(token.getCompanyId(), tvDisplayId);

            if (tvDisplay != null) {
                if (incomingTVDisplay.isPublished() && incomingTVDisplay.getContentList().isEmpty()) {
                    throw new BlazeInvalidArgException("TV Display Content", "Please add content in TV display");
                }

                tvDisplay.setName(incomingTVDisplay.getName());
                tvDisplay.setTheme(incomingTVDisplay.getTheme());
                tvDisplay.setOrientation(incomingTVDisplay.getOrientation());
                tvDisplay.setColumn(incomingTVDisplay.getColumn());
                tvDisplay.setRotationTime(incomingTVDisplay.getRotationTime());
                tvDisplay.setPublished(incomingTVDisplay.isPublished());

                List<TVDisplayContent> tvDisplayContentList = updateTVDisplayContent(incomingTVDisplay.getContentList());

                tvDisplay.setContentList(tvDisplayContentList);

                tvDisplayRepository.update(token.getCompanyId(), tvDisplayId, tvDisplay);
                HashMap<String, ProductCategory> categoryHashMap = productCategoryRepository.listAsMap(tvDisplay.getCompanyId(), tvDisplay.getShopId());
                assignDependencies(tvDisplay, categoryHashMap);
                return tvDisplay;
            } else {
                throw new BlazeInvalidArgException(TV_DISPLAY, TV_NOT_FOUND);
            }
        } else {
            throw new BlazeInvalidArgException(TV_DISPLAY, "Cannot be update, Because Tv Plugin settings are not found for this company");
        }
    }

    private void assignDependencies(TVDisplay tvDisplay, final HashMap<String, ProductCategory> categoryHashMap) {
        for (TVDisplayContent content : tvDisplay.getContentList()) {
            content.setProductCategory(categoryHashMap.get(content.getProductCategoryId()));
            content.setSecondCategory(categoryHashMap.get(content.getSecondCategoryId()));
        }
    }

    /**
     * Create list of TVDisplayContent from db list
     *
     * @param requestContentList
     */
    private List<TVDisplayContent> updateTVDisplayContent(List<TVDisplayContent> requestContentList) {
        for (TVDisplayContent content : requestContentList) {
            content.prepare(token.getCompanyId());
            content.setShopId(token.getShopId());
        }
        return requestContentList;
    }

    /**
     * Fetch all tv display by company Id
     *
     * @param start
     * @param limit
     */
    @Override
    public SearchResult<TVDisplay> getAllTVDisplay(int start, int limit) {
        SearchResult<TVDisplay> results = tvDisplayRepository.findItems(token.getCompanyId(), token.getShopId(), start, limit);

        HashMap<String, ProductCategory> categoryHashMap = productCategoryRepository.listAsMap(token.getCompanyId(), token.getShopId());
        for (TVDisplay tvDisplay : results.getValues()) {
            assignDependencies(tvDisplay, categoryHashMap);
        }
        return results;
    }

    /**
     * This method fetch tv display setting for particular tv
     *
     * @param uniqueIdentifier : unique identifier
     * @param tvNo             : tv number
     */
    @Override
    public TVDisplayResult getTVDisplaySetting(String uniqueIdentifier, Long tvNo) {
        TVDisplayResult tvDisplay = tvDisplayRepository.getTVDisplayByIdentifierAndTVNo(uniqueIdentifier, tvNo, TVDisplayResult.class);
        if (tvDisplay == null) {
            throw new BlazeInvalidArgException(TV_DISPLAY, TV_NOT_FOUND);
        }
        HashMap<String, ProductCategory> categoryHashMap = productCategoryRepository.listAsMap(tvDisplay.getCompanyId(), tvDisplay.getShopId());
        assignDependencies(tvDisplay, categoryHashMap);

        Company company = companyRepository.getById(tvDisplay.getCompanyId());
        if (company != null) {
            if (company.isDeleted() || company.isActive() == false) {
                throw new BlazeInvalidArgException("Company", "Company is no longer active.");
            }
            // company should not be null but let's check anyways
            tvDisplay.setCompanyName(company.getName());
            tvDisplay.setCompanyLogoURL(company.getLogoURL());
        } else {
            throw new BlazeInvalidArgException("Company", "Company does not exist.");
        }
        Shop shop = shopRepository.get(company.getId(), tvDisplay.getShopId());
        if (shop == null) {
            throw new BlazeInvalidArgException(SHOP, SHOP_NOT_FOUND);
        }
        tvDisplay.setShopName(shop.getName());
        if (shop.getLogo() != null) {
            tvDisplay.setShopLogoURL(shop.getLogo().getPublicURL());
        }

        return tvDisplay;
    }

    /**
     * This method is used to get all walk in transactions and return member information
     * based on transaction
     *
     * @param uniqueIdentifier
     * @return
     */
    @Override
    public List<MemberLimitedView> getWaitListForAllActiveTransactions(final String uniqueIdentifier) {
        final TVDisplay tvDisplay = tvDisplayRepository.getTvDisplayByIndentifier(uniqueIdentifier);
        if (tvDisplay != null) {
            if (!tvDisplay.isPublished()) {
                throw new BlazeInvalidArgException("TVDisplay", "TVDisplay is not published.");
            }
            List<Transaction> transactionlist = transactionRepository.getWalkinTransations(tvDisplay.getShopId(), Transaction.QueueType.WalkIn);

            if (transactionlist != null) {
                List<ObjectId> objectIds = new ArrayList<>();
                LOG.info("Found trans count: " + transactionlist.size());
                for (Transaction transaction : transactionlist) {
                    if (ObjectId.isValid(transaction.getMemberId())) {
                        objectIds.add(new ObjectId(transaction.getMemberId()));
                    }
                }
                Iterable<MemberLimitedView> members = memberRepository.findItemsIn(tvDisplay.getCompanyId(), objectIds, MemberLimitedView.class);
                return Lists.newArrayList(members);
            } else {
                throw new BlazeInvalidArgException(TV_DISPLAY, "No transactions found for this Display");
            }
        }

        throw new BlazeInvalidArgException(TV_DISPLAY, "No Members found for these transactions");
    }

    /**
     * This method is used to get all product list based on category id for the current display.
     *
     * @param uniqueIdentifier
     * @param categoryId
     * @return
     */
    @Override
    public SearchResult<Product> getProductByCategoryAndIdentifier(final String uniqueIdentifier, final String categoryId, final int start, final int limit) {
        TVDisplay tvDisplay = tvDisplayRepository.getTvDisplayByIndentifier(uniqueIdentifier);
        if (tvDisplay != null) {
            if (!tvDisplay.isPublished()) {
                throw new BlazeInvalidArgException("TVDisplay", "TVDisplay is not published.");
            }
            if (tvDisplay.getContentList().size() > 0) {

                ProductCategory productCategory = productCategoryRepository.get(tvDisplay.getCompanyId(), categoryId);

                if (productCategory == null) {
                    throw new BlazeInvalidArgException(TV_DISPLAY, CATEGORY_NOT_FOUND);
                } else if (!productCategory.isActive()) {
                    SearchResult<Product> searchResult = new SearchResult<>();
                    searchResult.setTotal(0l);
                    return searchResult;
                }

                HashMap<String, ProductCategory> productCategoryMap = productCategoryRepository.listAsMap(tvDisplay.getCompanyId(), tvDisplay.getShopId());
                List<TVDisplayContent> tvDisplayContentList = tvDisplay.getContentList();
                for (TVDisplayContent tvDisplayContent : tvDisplayContentList) {
                    if (tvDisplayContent.getProductCategoryId().equalsIgnoreCase(categoryId)) {
                        SearchResult<Product> productList = productRepository.findActiveProductsByCategoryId(tvDisplay.getCompanyId(), tvDisplay.getShopId(), categoryId, "{name:1}", start, limit, Boolean.TRUE);
                        if (productList != null) {
                            prepareProductResult(productList.getValues(), productCategoryMap);
                            return productList;
                        } else {
                            throw new BlazeOperationException(NO_PRODUCT);
                        }
                    } else if (tvDisplayContent.getSecondCategoryId().equalsIgnoreCase(categoryId)) {
                        SearchResult<Product> productList = productRepository.findActiveProductsByCategoryId(tvDisplay.getCompanyId(), tvDisplay.getShopId(), categoryId, "{name:1}", start, limit, Boolean.TRUE);
                        if (productList != null) {
                            prepareProductResult(productList.getValues(), productCategoryMap);
                            return productList;
                        } else {
                            throw new BlazeOperationException(NO_PRODUCT);
                        }
                    } else if (tvDisplayContent.getSecondCategoryId().equalsIgnoreCase(categoryId)) {
                        SearchResult<Product> productList = productRepository.findActiveProductsByCategoryId(tvDisplay.getCompanyId(), tvDisplay.getShopId(), categoryId, "{name:1}", start, limit, Boolean.TRUE);
                        if (productList != null) {
                            prepareProductResult(productList.getValues(), productCategoryMap);
                            return productList;
                        } else {
                            throw new BlazeInvalidArgException(TV_DISPLAY, NO_PRODUCT);
                        }
                    }
                }
            } else {
                throw new BlazeInvalidArgException(TV_DISPLAY, "No Display contents found for this category");
            }

        }
        throw new BlazeInvalidArgException(TV_DISPLAY, "No TvDisplay found for this category");
    }

    private void prepareProductResult(List<Product> productList, HashMap<String, ProductCategory> productCategoryMap) {


        for (Product product : productList) {
            ProductCategory category = productCategoryMap.get(product.getCategoryId());

            if (category != null) {
                product.setCategory(category);
            }
        }

    }

    /**
     * Get TV display by id
     *
     * @param tvDisplayId : tv display if from which tv display needs to be fetch
     */
    @Override
    public TVDisplay getTVDisplayById(String tvDisplayId) {
        TVDisplay tvDisplay = tvDisplayRepository.get(token.getCompanyId(), tvDisplayId);

        if (tvDisplay == null) {
            throw new BlazeOperationException(TV_NOT_FOUND);
        }
        HashMap<String, ProductCategory> categoryHashMap = productCategoryRepository.listAsMap(tvDisplay.getCompanyId(), tvDisplay.getShopId());
        assignDependencies(tvDisplay, categoryHashMap);

        return tvDisplay;
    }

    /**
     * Delete TV display by id
     *
     * @param tvDisplayId : TV display id that need to be deleted
     */
    @Override
    public void deleteTVDisplay(String tvDisplayId) {
        TVDisplay tvDisplay = tvDisplayRepository.get(token.getCompanyId(), tvDisplayId);

        if (tvDisplay == null) {
            throw new BlazeInvalidArgException(TV_DISPLAY, TV_NOT_FOUND);
        }

        tvDisplayRepository.removeAndUnpublish(token.getCompanyId(), tvDisplayId);
    }

    /**
     * This method fetch transactions for tv content
     * @param uniqueIdentifier : tv's unique identifier
     * @param contentId : content id
     * @param start : start
     * @param limit : limit
     */
    @Override
    public Map<String, SearchResult<Transaction>> getTvDisplayTransactionsByContent(String uniqueIdentifier, String contentId, int start, int limit) {
        TVDisplay tvDisplay = tvDisplayRepository.getTvDisplayByIndentifier(uniqueIdentifier);
        Map<String, SearchResult<Transaction>> transactionMap = new HashMap<>();

        if (tvDisplay == null) {
            throw new BlazeInvalidArgException(TV_DISPLAY, TV_NOT_FOUND);
        }

        if (!tvDisplay.isPublished()) {
            throw new BlazeInvalidArgException(TV_DISPLAY, TV_NOT_PUBLISHED);
        }

        if (limit == 0) {
            limit = 200;
        }

        if (StringUtils.isBlank(contentId)) {
            return transactionMap;
        }

        TVDisplayContent tvDisplayContent = null;
        for (TVDisplayContent displayContent : tvDisplay.getContentList()) {
            if (displayContent.getId().equalsIgnoreCase(contentId)
                    && TVDisplayContent.TVDisplayContentType.FulfillmentQueue == displayContent.getTvDisplayContentType()) {
                tvDisplayContent = displayContent;
                break;
            }
        }

        if (tvDisplayContent == null) {
            throw new BlazeInvalidArgException(TV_DISPLAY, CONTENT_NOT_FOUND);
        }

        Transaction.QueueType queueType = tvDisplayContent.getQueueType();

        SearchResult<Transaction> resultFulfill = transactionRepository.getTransactionsByFulFillmentStepAndQueue(tvDisplay.getCompanyId(), tvDisplay.getShopId(), Transaction.FulfillmentStep.Fulfill, queueType, start, limit);
        SearchResult<Transaction> resultPrepare = transactionRepository.getTransactionsByFulFillmentStepAndQueue(tvDisplay.getCompanyId(), tvDisplay.getShopId(), Transaction.FulfillmentStep.Prepare, queueType, start, limit);

        if (resultFulfill.getValues() != null && resultFulfill.getValues().size() > 0) {
            this.prepareTransaction(resultFulfill, tvDisplay);
        }

        if (resultPrepare.getValues() != null && resultPrepare.getValues().size() > 0) {
            this.prepareTransaction(resultPrepare, tvDisplay);
        }

        transactionMap.put(Transaction.FulfillmentStep.Fulfill.toString(), resultFulfill);
        transactionMap.put(Transaction.FulfillmentStep.Prepare.toString(), resultPrepare);

        return transactionMap;
    }

    private void prepareTransaction(SearchResult<Transaction> result, TVDisplay tvDisplay) {
        Set<ObjectId> membersIds = new HashSet<>();
        Set<ObjectId> productIds = new HashSet<>();
        Set<ObjectId> employeeIds = new HashSet<>();

        for (Transaction transaction : result.getValues()) {
            if (StringUtils.isNotEmpty(transaction.getMemberId())) {
                membersIds.add(new ObjectId(transaction.getMemberId()));
            }

            if (StringUtils.isNotBlank(transaction.getAssignedEmployeeId())) {
                employeeIds.add(new ObjectId(transaction.getAssignedEmployeeId()));
            }
            for (OrderItem item : transaction.getCart().getItems()) {
                if (StringUtils.isNotEmpty(item.getProductId())) {
                    productIds.add(new ObjectId(item.getProductId()));
                }
            }
        }

        String projection = "{firstName:1, lastName:1}";
        HashMap<String, Member> memberHashMap = memberRepository.findMembers(tvDisplay.getCompanyId(), projection, Lists.newArrayList(membersIds));
        HashMap<String, Product> productHashMap = productRepository.findItemsInAsMap(tvDisplay.getCompanyId(), tvDisplay.getShopId(), Lists.newArrayList(productIds));
        HashMap<String, Employee> employeeHashMap = employeeRepository.findItemsInAsMap(tvDisplay.getCompanyId(), Lists.newArrayList(employeeIds));

        for (Transaction transaction : result.getValues()) {
            transaction.setMember(memberHashMap.get(transaction.getMemberId()));
            transaction.setAssignedEmployee(employeeHashMap.get(transaction.getAssignedEmployeeId()));
            for (OrderItem item : transaction.getCart().getItems()) {
                item.setProduct(productHashMap.get(item.getProductId()));
            }
        }
    }

    /**
     * Override method to get employee stats details.
     *
     * @param uniqueIdentifier : unique identifier
     * @param contentId        : tv display content id
     * @param start            : start
     * @param limit            : limit
     * @param timeZoneOffset   : timezoneoffset
     * @return : return employee stats details
     */
    @Override
    public EmployeeStatsResult getEmployeeStatsDetail(String uniqueIdentifier, String contentId, int start, int limit, int timeZoneOffset) {
        limit = (limit <= 0) ? 5 : limit;

        EmployeeStatsResult result = new EmployeeStatsResult(new ArrayList<>(), new ArrayList<>());

        TVDisplay tvDisplay = tvDisplayRepository.getTvDisplayByIndentifier(uniqueIdentifier);

        if (tvDisplay == null || tvDisplay.getContentList() == null || tvDisplay.getContentList().isEmpty()) {
            return result;
        }

        if (StringUtils.isBlank(contentId)) {
            return result;
        }

        TVDisplayContent tvDisplayContent = null;
        for (TVDisplayContent displayContent : tvDisplay.getContentList()) {
            if (displayContent.getId().equalsIgnoreCase(contentId)
                    && TVDisplayContent.TVDisplayContentType.EmployeeStats == displayContent.getTvDisplayContentType()) {
                tvDisplayContent = displayContent;
                break;
            }
        }

        if (tvDisplayContent == null) {
            throw new BlazeInvalidArgException(TV_DISPLAY, CONTENT_NOT_FOUND);
        }

        Shop shop = shopRepository.getById(tvDisplay.getShopId());
        if (timeZoneOffset == 0 && shop != null) {
            String timeZone = shop.getTimeZone();
            timeZoneOffset = DateUtil.getTimezoneOffsetInMinutes(timeZone);
        }

        long timeZoneStartDateMillis, timeZoneEndDateMillis;

        if (timeZoneOffset != 0) {
            timeZoneStartDateMillis = DateTime.now().withTimeAtStartOfDay().plusMinutes(timeZoneOffset).minusDays(1).getMillis();
            timeZoneEndDateMillis = DateTime.now().getMillis();
        } else {
           timeZoneStartDateMillis = DateUtil.nowUTC().withTimeAtStartOfDay().getMillis();
           timeZoneEndDateMillis = DateUtil.nowUTC().getMillis();
        }

        SearchResult<Transaction> transactionSearchResult = transactionRepository.getCompletedTransactionsByDate(tvDisplay.getCompanyId(), tvDisplay.getShopId(), 0, Integer.MAX_VALUE, timeZoneStartDateMillis, timeZoneEndDateMillis);

        if (transactionSearchResult == null || transactionSearchResult.getValues() == null || transactionSearchResult.getValues().isEmpty()) {
            return result;
        }

        prepareEmployeeDetails(tvDisplay, transactionSearchResult, result, start, limit);
        prepareSalesDetails(tvDisplay, transactionSearchResult, result);
        return result;
    }

    /**
     * Private method for prepare sales details
     *
     * @param tvDisplay               : tvDisplay
     * @param transactionSearchResult : transaction search result
     * @param result                  : employee stats result
     */
    private void prepareSalesDetails(TVDisplay tvDisplay, SearchResult<Transaction> transactionSearchResult, EmployeeStatsResult result) {

        int factor;

        Set<ObjectId> productIds = new HashSet<>();
        Set<ObjectId> categoryIds = new HashSet<>();
        Set<ObjectId> brandIds = new HashSet<>();
        Set<ObjectId> memberIds = new HashSet<>();
        Set<ObjectId> employeeIds = new HashSet<>();
        Set<ObjectId> roleIds = new HashSet<>();
        HashMap<String, SalesByCategory> saleByCategoryMap = new HashMap<>();
        HashMap<String, SalesByBrand> salesByBrandMap = new HashMap<>();
        HashMap<String, EmployeeStatsResult.SalesByBudTender> salesByBudTenderMap = new HashMap<>();
        HashMap<String, EmployeeStatsResult.SalesByMemberType> salesByMemberTypeMap = new HashMap<>();
        double totalSales = 0;

        for (Transaction transaction : transactionSearchResult.getValues()) {
            factor = 1;
            if (transaction.getTransType() == Transaction.TransactionType.Refund && transaction.getCart() != null && transaction.getCart() != null && transaction.getCart().getRefundOption() == Cart.RefundOption.Retail) {
                factor = -1;
            }

            totalSales += transaction.getCart().getTotal().doubleValue() * factor;

            if (transaction.getCart() == null || StringUtils.isBlank(transaction.getAssignedEmployeeId()) || transaction.getCart().getItems() == null || transaction.getCart().getItems().isEmpty()) {
                continue;
            }
            for (OrderItem item : transaction.getCart().getItems()) {
                if ((Transaction.TransactionType.Refund == transaction.getTransType() && Cart.RefundOption.Void == transaction.getCart().getRefundOption())
                        && OrderItem.OrderItemStatus.Refunded == item.getStatus()) {
                    continue;
                }

                productIds.add(new ObjectId(item.getProductId()));
            }
            if (StringUtils.isNotBlank(transaction.getMemberId()) && ObjectId.isValid(transaction.getMemberId())) {
                memberIds.add(new ObjectId(transaction.getMemberId()));
            }
            if (StringUtils.isNotBlank(transaction.getAssignedEmployeeId()) && ObjectId.isValid(transaction.getAssignedEmployeeId())) {
                employeeIds.add(new ObjectId(transaction.getAssignedEmployeeId()));
            }
        }

        HashMap<String, Product> productHashMap = productRepository.listAsMap(tvDisplay.getCompanyId(), Lists.newArrayList(productIds));
        HashMap<String, Member> memberHashMap = memberRepository.listAsMap(tvDisplay.getCompanyId(), Lists.newArrayList(memberIds));
        HashMap<String, Employee> employeeHashMap = employeeRepository.listAsMap(tvDisplay.getCompanyId(), Lists.newArrayList(employeeIds));

        for (Employee employee : employeeHashMap.values()) {
            if (StringUtils.isNotBlank(employee.getRoleId()) && ObjectId.isValid(employee.getRoleId())) {
                roleIds.add(new ObjectId(employee.getRoleId()));
            }
        }

        HashMap<String, Role> roleHashMap = roleRepository.listAsMap(tvDisplay.getCompanyId(), Lists.newArrayList(roleIds));

        for (Product product : productHashMap.values()) {
            if (StringUtils.isNotBlank(product.getCategoryId()) && ObjectId.isValid(product.getCategoryId())) {
                categoryIds.add(new ObjectId(product.getCategoryId()));
            }

            if (StringUtils.isNotBlank(product.getBrandId()) && ObjectId.isValid(product.getBrandId())) {
                brandIds.add(new ObjectId(product.getBrandId()));
            }
        }

        HashMap<String, ProductCategory> productCategoryHashMap = productCategoryRepository.listAsMap(tvDisplay.getCompanyId(), Lists.newArrayList(categoryIds));
        HashMap<String, Brand> brandHashMap = brandRepository.listAsMap(tvDisplay.getCompanyId(), Lists.newArrayList(brandIds));

        HashMap<String, Double> propRatioMap;
        Product product;
        ProductCategory category;
        Brand brand;
        Member member;
        Employee employee;
        Role role;
        double propCartDiscount;
        double propDeliveryFee;
        double propCCFee;
        double propAfterTaxDiscount;
        double finalCost, ratio;
        double gross, totalTax, postTax, postALExciseTax, postNALExciseTax, totalCityTax, totalStateTax, totalFedTax, subTotalAfterDiscount;
        int refundSales = 0;
        int onlineSales = 0;
        int totalSalesCount = 0;

        for (Transaction transaction : transactionSearchResult.getValues()) {
            totalSalesCount++;

            factor = 1;
            if (transaction.getTransType() == Transaction.TransactionType.Refund && transaction.getCart() != null && transaction.getCart() != null && transaction.getCart().getRefundOption() == Cart.RefundOption.Retail) {
                refundSales++;
                factor = -1;
            }

            double cartTotal = transaction.getCart().getTotal().doubleValue() * factor;
            if (StringUtils.isNotBlank(transaction.getConsumerCartId())) {
                onlineSales++;
            }

            member = memberHashMap.get(transaction.getMemberId());
            if (member != null && member.getConsumerType() != null) {
                EmployeeStatsResult.SalesByMemberType salesByMemberType = salesByMemberTypeMap.getOrDefault(member.getConsumerType().toString(), new EmployeeStatsResult.SalesByMemberType());
                salesByMemberType.setConsumerType(member.getConsumerType());
                salesByMemberType.setSalesCount(salesByMemberType.getSalesCount() + factor);
                salesByMemberTypeMap.put(member.getConsumerType().toString(), salesByMemberType);
            }

            employee = employeeHashMap.get(transaction.getAssignedEmployeeId());
            if (employee != null) {
                    EmployeeStatsResult.SalesByBudTender salesByBudTender = salesByBudTenderMap.getOrDefault(employee.getId(), new EmployeeStatsResult.SalesByBudTender());
                    salesByBudTender.setEmployeeId(employee.getId());
                    StringBuilder employeeName = new StringBuilder();
                    employeeName.append(employee.getFirstName().trim())
                            .append(StringUtils.isNotBlank(employee.getLastName()) ? " " + employee.getLastName().trim() : "");

                    salesByBudTender.setEmployeeName(employeeName.toString());
                    salesByBudTender.setSales(salesByBudTender.getSales().add(BigDecimal.valueOf(cartTotal)));
                    salesByBudTender.setVists(salesByBudTender.getVists() + 1);
                    salesByBudTender.setAverageSales(BigDecimal.valueOf(salesByBudTender.getVists() == 0 ? 0 : salesByBudTender.getSales().doubleValue() / salesByBudTender.getVists()));
                    salesByBudTenderMap.put(employee.getId(), salesByBudTender);
            }
            // Calculate discounts proportionally so we can apply taxes

            propRatioMap = new HashMap<>();

            if (transaction.getCart() == null || transaction.getCart().getItems() == null || transaction.getCart().getItems().isEmpty()) {
                continue;
            }
            double total = 0.0;
            for (OrderItem orderItem : transaction.getCart().getItems()) {
                product = productHashMap.get(orderItem.getProductId());
                if (product == null) {
                    continue;
                }
                if (product.isDiscountable()) {
                    total += NumberUtils.round(orderItem.getFinalPrice().doubleValue(), 6);
                }
            }

            // Calculate the ratio to be applied
            for (OrderItem orderItem : transaction.getCart().getItems()) {
                if ((Transaction.TransactionType.Refund == transaction.getTransType() && Cart.RefundOption.Void == transaction.getCart().getRefundOption())
                        && OrderItem.OrderItemStatus.Refunded == orderItem.getStatus()) {
                    continue;
                }
                product = productHashMap.get(orderItem.getProductId());
                if (product == null) {
                    continue;
                }

                propRatioMap.put(orderItem.getId(), 1d); // 100 %
                if (product.isDiscountable() && total > 0) {
                    finalCost = orderItem.getFinalPrice().doubleValue();
                    ratio = finalCost / total;

                    propRatioMap.put(orderItem.getId(), ratio);
                }

            }

            for (OrderItem item : transaction.getCart().getItems()) {

                if ((Transaction.TransactionType.Refund == transaction.getTransType() && Cart.RefundOption.Void == transaction.getCart().getRefundOption())
                        && OrderItem.OrderItemStatus.Refunded == item.getStatus()) {
                    continue;
                }

                product = productHashMap.get(item.getProductId());
                if (product == null) {
                    continue;
                }
                category = productCategoryHashMap.get(product.getCategoryId());
                brand = brandHashMap.get(product.getBrandId());

                if (category == null && brand == null) {
                    continue;
                }

                ratio = 1D;
                if (propRatioMap.containsKey(item.getId())) {
                    ratio = propRatioMap.get(item.getId());
                }

                propCartDiscount = transaction.getCart().getCalcCartDiscount() != null ? transaction.getCart().getCalcCartDiscount().doubleValue() * ratio : 0;
                propDeliveryFee = transaction.getCart().getDeliveryFee() != null ? transaction.getCart().getDeliveryFee().doubleValue() * ratio : 0;
                propCCFee = transaction.getCart().getCreditCardFee() != null ? transaction.getCart().getCreditCardFee().doubleValue() * ratio : 0;
                propAfterTaxDiscount = transaction.getCart().getAppliedAfterTaxDiscount() != null ? transaction.getCart().getAppliedAfterTaxDiscount().doubleValue() * ratio : 0;

                // calculated postTax
                postTax = postALExciseTax = postNALExciseTax = totalCityTax = totalStateTax = totalFedTax = 0;

                subTotalAfterDiscount = item.getFinalPrice().doubleValue() - propCartDiscount;
                if (item.getTaxResult() != null) {
                    postALExciseTax = item.getTaxResult().getTotalALPostExciseTax().doubleValue();
                    postNALExciseTax = item.getTaxResult().getTotalExciseTax().doubleValue();

                    if (item.getTaxOrder() == TaxInfo.TaxProcessingOrder.PostTaxed) {
                        postTax += item.getTaxResult().getTotalPostCalcTax().doubleValue() + postALExciseTax + postNALExciseTax;
                    }
                }
                if (postTax == 0 && ((item.getTaxTable() == null || !item.getTaxTable().isActive()) && item.getTaxInfo() != null)) {
                    TaxInfo taxInfo = item.getTaxInfo();
                    if (item.getTaxOrder() == TaxInfo.TaxProcessingOrder.PostTaxed) {
                        totalCityTax = subTotalAfterDiscount * taxInfo.getCityTax().doubleValue();
                        totalStateTax = subTotalAfterDiscount * taxInfo.getStateTax().doubleValue();
                        totalFedTax = subTotalAfterDiscount * taxInfo.getFederalTax().doubleValue();

                        postTax = totalCityTax + totalStateTax + totalFedTax;
                    }
                }

                if (postTax == 0) {
                    postTax = item.getCalcTax().doubleValue(); // - item.getCalcPreTax().doubleValue();
                }

                totalTax = postTax;

                gross = item.getFinalPrice().doubleValue() -
                        NumberUtils.round(propCartDiscount, 4) +
                        NumberUtils.round(propDeliveryFee, 4) +
                        NumberUtils.round(totalTax, 4) +
                        NumberUtils.round(propCCFee, 4) -
                        NumberUtils.round(propAfterTaxDiscount, 4);

                gross = (gross * factor);

                if (category != null) {
                    SalesByCategory salesByCategory = saleByCategoryMap.getOrDefault(category.getId(), new SalesByCategory());
                    salesByCategory.setCategoryId(category.getId());
                    salesByCategory.setCategoryName(category.getName());

                    salesByCategory.setSales(BigDecimal.valueOf(salesByCategory.getSales().doubleValue() + gross));

                    saleByCategoryMap.put(category.getId(), salesByCategory);
                }

                if (brand != null) {
                    SalesByBrand salesByBrand = salesByBrandMap.getOrDefault(brand.getId(), new SalesByBrand());
                    salesByBrand.setBrandId(brand.getId());
                    salesByBrand.setBrandName(brand.getName());
                    salesByBrand.setSales(BigDecimal.valueOf(salesByBrand.getSales().doubleValue() + gross));

                    salesByBrandMap.put(brand.getId(), salesByBrand);
                }

            }
        }

        for (EmployeeStatsResult.SalesByMemberType salesByMemberType : salesByMemberTypeMap.values()) {
            int sales = salesByMemberType.getSalesCount();
            salesByMemberType.setSalesPercent(totalSalesCount >= 0 ? BigDecimal.valueOf(sales * 100 / totalSalesCount) : BigDecimal.ZERO);
        }

        result.setOnlineOrders(onlineSales);
        result.setRefundSales(refundSales);
        result.setSalesByCategory(Lists.newArrayList(saleByCategoryMap.values()));
        result.setSalesByBrands(Lists.newArrayList(salesByBrandMap.values()));
        result.setSalesByConsumer(Lists.newArrayList(salesByMemberTypeMap.values()));
        result.setTotalSales(new BigDecimal(totalSales));
        result.setTotalVisits(transactionSearchResult.getTotal());
        result.setAverageSales(new BigDecimal((totalSales >= 0) ? totalSales / result.getTotalVisits() : 0));
        result.setBudTenderSale(Lists.newArrayList(salesByBudTenderMap.values()));
        result.setOnlineOrdersPercent(onlineSales >= 0 ? new BigDecimal(onlineSales * 100 / totalSalesCount) : BigDecimal.ZERO);
        result.setRefundSalesPercent(refundSales >= 0 ? new BigDecimal(refundSales * 100 / totalSalesCount) : BigDecimal.ZERO);
    }

    /**
     * Private method to prepare employee details
     *
     * @param tvDisplay               : tvDisplay
     * @param transactionSearchResult : transaction search result
     * @param result                  : employee stats result
     * @param start                   : start
     * @param limit                   : limit
     */
    private void prepareEmployeeDetails(TVDisplay tvDisplay, SearchResult<Transaction> transactionSearchResult, EmployeeStatsResult result, int start, int limit) {
        Set<ObjectId> productIds = new HashSet<>();
        Set<ObjectId> brandIds = new HashSet<>();
        Set<ObjectId> employeeIds = new HashSet<>();

        HashMap<String, Double> employeeProductQuantity = new HashMap<>();
        HashMap<String, Double> employeeBrandQuantity = new HashMap<>();

        HashMap<String, EmployeeStatsResponse> employeeProductMap = new HashMap<>();
        HashMap<String, EmployeeStatsResponse> employeeBrandMap = new HashMap<>();

        double totalSales = 0;

        for (Transaction transaction : transactionSearchResult.getValues()) {
            if (transaction.getCart() == null || StringUtils.isBlank(transaction.getAssignedEmployeeId()) || transaction.getCart().getItems() == null || transaction.getCart().getItems().isEmpty()) {
                continue;
            }

            int factor = 1;
            if (Transaction.TransactionType.Refund == transaction.getTransType() && Cart.RefundOption.Retail == transaction.getCart().getRefundOption()) {
                factor = -1;
            }

            employeeIds.add(new ObjectId(transaction.getAssignedEmployeeId()));

            for (OrderItem item : transaction.getCart().getItems()) {
                if ((Transaction.TransactionType.Refund == transaction.getTransType() && Cart.RefundOption.Void == transaction.getCart().getRefundOption())
                        && OrderItem.OrderItemStatus.Refunded == item.getStatus()) {
                    continue;
                }

                productIds.add(new ObjectId(item.getProductId()));
                String key = transaction.getAssignedEmployeeId() + "_" + item.getProductId();
                double quantity = employeeProductQuantity.getOrDefault(key, 0.0);
                employeeProductQuantity.put(key, quantity + (item.getQuantity().doubleValue() * factor));


            }
        }

        HashMap<String, Product> productHashMap = productRepository.listAsMap(tvDisplay.getCompanyId(), Lists.newArrayList(productIds));
        HashMap<String, Employee> employeeHashMap = employeeRepository.listAsMap(tvDisplay.getCompanyId(), Lists.newArrayList(employeeIds));
        List<Employee> employeeList = Lists.newArrayList(employeeHashMap.values());

        employeeList.sort(new Comparator<Employee>() {
            @Override
            public int compare(Employee e1, Employee e2) {
                StringBuilder employeeName1 = new StringBuilder();
                employeeName1.append(e1.getFirstName().trim())
                        .append(StringUtils.isNotBlank(e1.getLastName()) ? " " + e1.getLastName().trim() : "");
                StringBuilder employeeName2 = new StringBuilder();
                employeeName2.append(e2.getFirstName().trim())
                        .append(StringUtils.isNotBlank(e2.getLastName()) ? " " + e2.getLastName().trim() : "");
                return employeeName1.toString().compareTo(employeeName2.toString());

            }
        });
        start = (start >= employeeList.size()) ? employeeList.size() : start;
        limit = (limit <= employeeIds.size()) ? limit : employeeList.size();
        employeeList = employeeList.subList(start, limit);


        for (Product product : productHashMap.values()) {
            if (StringUtils.isNotBlank(product.getBrandId()) && ObjectId.isValid(product.getBrandId())) {
                brandIds.add(new ObjectId(product.getBrandId()));
            }
        }

        HashMap<String, Brand> brandHashMap = brandRepository.listAsMap(tvDisplay.getCompanyId(), Lists.newArrayList(brandIds));

        for (String key : employeeProductQuantity.keySet()) {
            String[] keyArr = StringUtils.split(key, "_");
            Product product = productHashMap.get(keyArr[1]);
            Employee employee = employeeHashMap.get(keyArr[0]);

            if (Objects.isNull(employee) || Objects.isNull(product)) {
                continue;
            }

            Brand brand = brandHashMap.get(product.getBrandId());

            EmployeeStatsResponse employeeStatsResponse = employeeProductMap.getOrDefault(employee.getId(), new EmployeeStatsResponse());
            List purchaseProducts = employeeStatsResponse.getResultList();

            purchaseProducts = (purchaseProducts == null) ? new ArrayList<>() : purchaseProducts;

            PurchaseProduct purchaseProduct = new PurchaseProduct();
            purchaseProduct.setProductId(product.getId());
            purchaseProduct.setProductName(product.getName());
            purchaseProduct.setQuantity((new BigDecimal(employeeProductQuantity.get(key)).setScale(2, BigDecimal.ROUND_UP)));
            purchaseProducts.add(purchaseProduct);

            if (brand != null) {
                double quantity = employeeBrandQuantity.getOrDefault(employee.getId() + "_" + brand.getId(), 0.0);
                employeeBrandQuantity.put(employee.getId() + "_" + brand.getId(), quantity + employeeProductQuantity.get(key));
            }
            employeeStatsResponse.setResultList(purchaseProducts);

            if (StringUtils.isBlank(employeeStatsResponse.getEmployeeName())) {
                StringBuilder employeeName = new StringBuilder();
                employeeName.append(employee.getFirstName())
                        .append(StringUtils.isNotBlank(employee.getLastName()) ? " " + employee.getLastName() : "");

                employeeStatsResponse.setEmployeeName(employeeName.toString());
            }

            employeeProductMap.put(employee.getId(), employeeStatsResponse);
        }

        for (String key : employeeBrandQuantity.keySet()) {
            String[] keyArr = StringUtils.split(key, "_");
            Brand brand = brandHashMap.get(keyArr[1]);
            Employee employee = employeeHashMap.get(keyArr[0]);

            if (Objects.isNull(employee) || Objects.isNull(brand)) {
                continue;
            }

            EmployeeStatsResponse employeeStatsResponse = employeeBrandMap.getOrDefault(employee.getId(), new EmployeeStatsResponse());
            List purchaseBrands = employeeStatsResponse.getResultList();

            purchaseBrands = (purchaseBrands == null) ? new ArrayList<>() : purchaseBrands;

            PurchaseBrand purchaseBrand = new PurchaseBrand();
            purchaseBrand.setBrandId(brand.getId());
            purchaseBrand.setBrandName(brand.getName());
            purchaseBrand.setQuantity((new BigDecimal(employeeBrandQuantity.get(key)).setScale(2, BigDecimal.ROUND_UP)));
            purchaseBrands.add(purchaseBrand);

            employeeStatsResponse.setResultList(purchaseBrands);

            if (StringUtils.isBlank(employeeStatsResponse.getEmployeeName())) {
                StringBuilder employeeName = new StringBuilder();
                employeeName.append(employee.getFirstName())
                        .append(StringUtils.isNotBlank(employee.getLastName()) ? " " + employee.getLastName() : "");

                employeeStatsResponse.setEmployeeName(employeeName.toString());
            }

            employeeBrandMap.put(employee.getId(), employeeStatsResponse);
        }

        List<EmployeeStatsResponse> employeeBrandList = new ArrayList<>();
        List<EmployeeStatsResponse> employeeProductList = new ArrayList<>();

        for (Employee employee : employeeList) {
            EmployeeStatsResponse employeeProduct = employeeProductMap.get(employee.getId());
            EmployeeStatsResponse employeeBrand = employeeBrandMap.get(employee.getId());


            if (employeeProduct != null) {
                List<PurchaseProduct> productList = employeeProduct.getResultList();

                productList.sort(new Comparator<PurchaseProduct>() {
                    @Override
                    public int compare(PurchaseProduct o1, PurchaseProduct o2) {
                        return o2.getQuantity().compareTo(o1.getQuantity());
                    }
                });
                employeeProduct.setResultList(productList.subList(0, productList.size() < 5 ? productList.size() : 5));
                employeeProductList.add(employeeProduct);
            }

            if (employeeBrand != null) {
                List<PurchaseBrand> brandList = employeeBrand.getResultList();

                brandList.sort(new Comparator<PurchaseBrand>() {
                    @Override
                    public int compare(PurchaseBrand o1, PurchaseBrand o2) {
                        return o2.getQuantity().compareTo(o1.getQuantity());
                    }
                });
                employeeBrand.setResultList(brandList.subList(0, brandList.size() < 5 ? brandList.size() : 5));
                employeeBrandList.add(employeeBrand);
            }

        }

        result.setBrandDetails(employeeBrandList);
        result.setProductDetails(employeeProductList);
    }
}
