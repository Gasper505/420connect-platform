package com.fourtwenty.core.services.mgmt.impl;

import com.fourtwenty.core.domain.models.thirdparty.ThirdPartyAccount;
import com.fourtwenty.core.domain.repositories.thirdparty.ThirdPartyAccountRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.rest.dispensary.requests.thirdparty.TPAccountAddRequest;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.mgmt.ThirdPartyAccountService;
import com.google.inject.Inject;
import com.google.inject.Provider;

/**
 * Created by mdo on 2/10/17.
 */
public class ThirdPartyAccountServiceImpl extends AbstractAuthServiceImpl implements ThirdPartyAccountService {
    @Inject
    ThirdPartyAccountRepository thirdPartyAccountRepository;

    @Inject
    public ThirdPartyAccountServiceImpl(Provider<ConnectAuthToken> tokenProvider) {
        super(tokenProvider);
    }

    @Override
    public ThirdPartyAccount addAccount(TPAccountAddRequest request) {
        ThirdPartyAccount acc = new ThirdPartyAccount();
        acc.prepare(token.getCompanyId());
        acc.setName(request.getName());
        acc.setActive(request.getStatus());
        acc.setAccountType(request.getAccountType());
        acc.setUsername(request.getUsername());
        acc.setPassword(request.getPassword());
        acc.setClientId(request.getClientId());
        acc.setClientSecret(request.getClientSecret());
        acc.setShopId(request.getShopId());
        acc.setLocationId(request.getLocationId());
        acc.setMerchantId(request.getMerchantId());
        acc.setToken(request.getToken());
        thirdPartyAccountRepository.save(acc);
        return acc;
    }

    @Override
    public ThirdPartyAccount getAccountById(String accountId) {
        ThirdPartyAccount account = thirdPartyAccountRepository.get(token.getCompanyId(), accountId);
        if (account == null) {
            throw new BlazeInvalidArgException("ThirdPartyAccount", "Account does not exist.");
        }
        return account;
    }

    @Override
    public SearchResult<ThirdPartyAccount> getThirdPartyAccounts() {
        return thirdPartyAccountRepository.findItems(token.getCompanyId(), 0, Integer.MAX_VALUE);
    }

    @Override
    public DateSearchResult<ThirdPartyAccount> getThirdPartyAccounts(long afterDate, long beforeDate) {
        return thirdPartyAccountRepository.findItemsWithDate(token.getCompanyId(), afterDate, beforeDate);
    }

    @Override
    public ThirdPartyAccount updateAccount(String accountId, ThirdPartyAccount account) {
        ThirdPartyAccount dbAccount = thirdPartyAccountRepository.get(token.getCompanyId(), accountId);
        if (dbAccount == null) {
            throw new BlazeInvalidArgException("ThirdPartyAccount", "Third Party Account does not exist.");
        }
        dbAccount.setAccountType(account.getAccountType());
        dbAccount.setActive(account.isActive());
        dbAccount.setPassword(account.getPassword());
        dbAccount.setUsername(account.getUsername());
        dbAccount.setName(account.getName());
        dbAccount.setClientId(account.getClientId());
        dbAccount.setClientSecret(account.getClientSecret());
        dbAccount.setShopId(account.getShopId());
        dbAccount.setLocationId(account.getLocationId());
        dbAccount.setMerchantId(account.getMerchantId());
        dbAccount.setToken(account.getToken());

        thirdPartyAccountRepository.update(token.getCompanyId(), dbAccount.getId(), dbAccount);
        return dbAccount;
    }

    @Override
    public void deleteThirdPartyAccount(String accountId) {
        thirdPartyAccountRepository.removeById(token.getCompanyId(), accountId);
    }

}
