package com.fourtwenty.core.services.mgmt.impl;

import com.fourtwenty.core.domain.models.thirdparty.ThirdPartyShopAccount;
import com.fourtwenty.core.domain.repositories.thirdparty.ThirdPartyShopAccountRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.rest.dispensary.requests.thirdparty.TPShopAccountAddRequest;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.mgmt.ThirdPartyShopAccountService;
import com.google.inject.Inject;
import com.google.inject.Provider;

/**
 * Created by mdo on 8/25/17.
 */
public class ThirdPartyShopAccountServiceImpl extends AbstractAuthServiceImpl implements ThirdPartyShopAccountService {

    @Inject
    ThirdPartyShopAccountRepository thirdPartyShopAccountRepository;

    @Inject
    public ThirdPartyShopAccountServiceImpl(Provider<ConnectAuthToken> tokenProvider) {
        super(tokenProvider);
    }

    @Override
    public ThirdPartyShopAccount addAccount(TPShopAccountAddRequest request) {

        ThirdPartyShopAccount thirdPartyShopAccount = new ThirdPartyShopAccount();
        thirdPartyShopAccount.setAccountType(request.getAccountType());
        thirdPartyShopAccount.setEnabled(true);
        thirdPartyShopAccount.setShopApiKey(request.getShopApiKey());
        thirdPartyShopAccount.setVerified(request.isVerified());
        thirdPartyShopAccount.setLastSyncDate(request.getLastSyncDate());
        thirdPartyShopAccount.setEntityISN(request.getEntityISN());

        thirdPartyShopAccountRepository.save(thirdPartyShopAccount);
        return thirdPartyShopAccount;
    }


    @Override
    public ThirdPartyShopAccount getAccountById(String accountId) {
        return thirdPartyShopAccountRepository.get(token.getCompanyId(), accountId);
    }

    @Override
    public ThirdPartyShopAccount getAccountByType(ThirdPartyShopAccount.ThirdPartyShopAccountType accountType) {
        ThirdPartyShopAccount account = thirdPartyShopAccountRepository.getTPShopAccount(token.getCompanyId(),
                token.getShopId(), accountType);
        if (account == null) {
            account = new ThirdPartyShopAccount();
            account.prepare(token.getCompanyId());
            account.setShopId(token.getShopId());

            thirdPartyShopAccountRepository.save(account);
        }

        return account;
    }

    @Override
    public DateSearchResult<ThirdPartyShopAccount> getThirdPartyShopAccounts(long afterDate, long beforeDate) {
        return thirdPartyShopAccountRepository.findItemsWithDate(token.getCompanyId(), afterDate, beforeDate);
    }

    @Override
    public SearchResult<ThirdPartyShopAccount> getThirdPartyShopAccounts() {

        return thirdPartyShopAccountRepository.findItems(token.getCompanyId(), token.getShopId(), 0, Integer.MAX_VALUE);
    }

    @Override
    public ThirdPartyShopAccount updateAccount(String accountId, ThirdPartyShopAccount account) {

        ThirdPartyShopAccount dbTthirdPartyShopAccount = thirdPartyShopAccountRepository.get(token.getCompanyId(), accountId);
        if (dbTthirdPartyShopAccount == null) {
            throw new BlazeInvalidArgException("ThirdPartyShopAccount", "Account does not exist.");
        }

        dbTthirdPartyShopAccount.setLastSyncDate(account.getLastSyncDate());
        dbTthirdPartyShopAccount.setShopApiKey(account.getShopApiKey());
        dbTthirdPartyShopAccount.setEnabled(account.isEnabled());
        dbTthirdPartyShopAccount.setAccountType(account.getAccountType());
        dbTthirdPartyShopAccount.setSettings(account.getSettings());
        dbTthirdPartyShopAccount.setVerified(account.isVerified());
        dbTthirdPartyShopAccount.setEntityISN(account.getEntityISN());

        return thirdPartyShopAccountRepository.update(token.getCompanyId(), accountId, dbTthirdPartyShopAccount);
    }

    @Override
    public void deleteThirdPartyShopAccount(String accountId) {
        thirdPartyShopAccountRepository.removeById(token.getCompanyId(), accountId);
    }
}
