package com.fourtwenty.core.services.mgmt.impl;

import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.company.Role;
import com.fourtwenty.core.domain.models.company.TimeCard;
import com.fourtwenty.core.domain.repositories.dispensary.EmployeeRepository;
import com.fourtwenty.core.domain.repositories.dispensary.RoleRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TimeCardRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.rest.dispensary.requests.auth.PinRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.TimeCardCompositeResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.mgmt.RoleService;
import com.fourtwenty.core.services.mgmt.TimeCardService;
import com.google.inject.Provider;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by mdo on 6/24/16.
 */
public class TimeCardServiceImpl extends AbstractAuthServiceImpl implements TimeCardService {
    @Inject
    TimeCardRepository timeCardRepository;
    @Inject
    EmployeeRepository employeeRepository;
    @Inject
    RoleService roleService;
    @Inject
    RoleRepository roleRepository;

    @Inject
    public TimeCardServiceImpl(Provider<ConnectAuthToken> token) {
        super(token);
    }


    @Override
    public SearchResult<TimeCardCompositeResult> getActiveTimeCards() {

        Iterable<TimeCard> timeCards = timeCardRepository.getActiveTimeCards(token.getCompanyId(), token.getShopId());

        List<ObjectId> objectIdList = new ArrayList<>();
        HashMap<String, TimeCard> timeCardHashMap = new HashMap<>();
        for (TimeCard timeCard : timeCards) {
            objectIdList.add(new ObjectId(timeCard.getEmployeeId()));
            timeCardHashMap.put(timeCard.getEmployeeId(), timeCard);
        }

        Iterable<Employee> clockedInEmployees = employeeRepository.findItemsIn(token.getCompanyId(), objectIdList, "{password:0,pin:0}");
        HashMap<String, Role> roleHashMap = roleRepository.listAsMap(token.getCompanyId());

        SearchResult<TimeCardCompositeResult> items = new SearchResult<>();
        for (Employee employee : clockedInEmployees) {
            TimeCardCompositeResult comp = new TimeCardCompositeResult();
            comp.setEmployee(employee);
            comp.setTimeCard(timeCardHashMap.get(employee.getId()));
            items.getValues().add(comp);
            employee.setRole(roleHashMap.get(employee.getRoleId()));
        }

        items.setTotal((long) items.getValues().size());
        return items;
    }

    @Override
    public SearchResult<TimeCardCompositeResult> getTimecardHistory(int start, int limit) {
        if (limit >= 200 || limit <= 0) {
            limit = 200;
        }
        Iterable<TimeCard> timeCards = timeCardRepository.listByShopSort(token.getCompanyId(), token.getShopId(), "{clockInTime:-1}", start, limit);

        List<ObjectId> objectIdList = new ArrayList<>();
        for (TimeCard timeCard : timeCards) {
            objectIdList.add(new ObjectId(timeCard.getEmployeeId()));
        }

        HashMap<String, Employee> employeeHashMap = employeeRepository.findItemsInAsMap(token.getCompanyId(), objectIdList);


        HashMap<String, Role> roleHashMap = roleRepository.listAsMap(token.getCompanyId());
        SearchResult<TimeCardCompositeResult> items = new SearchResult<>();
        for (TimeCard timeCard : timeCards) {
            TimeCardCompositeResult comp = new TimeCardCompositeResult();
            Employee employee = employeeHashMap.get(timeCard.getEmployeeId());
            comp.setEmployee(employee);
            comp.setTimeCard(timeCard);
            items.getValues().add(comp);
            if (employee != null) {
                employee.setRole(roleHashMap.get(employee.getRoleId()));
            }
        }

        items.setTotal((long) items.getValues().size());
        return items;
    }

    @Override
    public TimeCardCompositeResult updateTimecard(String timecardId, TimeCard timeCard) {
        TimeCard dbTimeCard = timeCardRepository.get(token.getCompanyId(), timecardId);
        if (dbTimeCard == null) {
            throw new BlazeInvalidArgException("Timecard", "Time card does not exist.");
        }

        Employee employee = employeeRepository.get(dbTimeCard.getCompanyId(), dbTimeCard.getEmployeeId());
        if (employee == null) {
            throw new BlazeInvalidArgException("Timecard", "Employee does not exist for this timecard.");
        }
        if (timeCard.getClockInTime() == null) {
            throw new BlazeInvalidArgException("Timecard", "clock in time should not be empty");
        }
        if (timeCard.getClockOutTime() == null) {
            throw new BlazeInvalidArgException("Timecard", "clock out time should not be empty");
        }

        if (timeCard.getClockInTime() >= timeCard.getClockOutTime()) {
            throw new BlazeInvalidArgException("TimeCard", "Clockout time must be greater than clock in time.");
        }

        dbTimeCard.setClockInTime(timeCard.getClockInTime());
        dbTimeCard.setClockOutTime(timeCard.getClockOutTime());
        dbTimeCard.setClockin(timeCard.isClockin());

        timeCardRepository.update(token.getCompanyId(), dbTimeCard.getId(), dbTimeCard);

        TimeCardCompositeResult comp = new TimeCardCompositeResult();
        comp.setEmployee(employee);
        comp.setTimeCard(dbTimeCard);

        return comp;
    }

    @Override
    public TimeCardCompositeResult clockIn(PinRequest request) {
        Employee employee = employeeRepository.getEmployeeByQuickPin(token.getCompanyId(), request.getPin());
        if (employee == null) {
            throw new BlazeInvalidArgException("EmployeeClockIn", "Invalid pin");
        }
        // make sure this employee is assigned to this shop
        if (employee.getShops() == null || !employee.getShops().contains(token.getShopId())) {
            throw new BlazeInvalidArgException("EmployeeClockIn", "Employee is not assigned to this shop.");
        }

        TimeCard timecard = timeCardRepository.getActiveTimeCard(token.getCompanyId(), token.getShopId(), employee.getId());

        if (timecard == null) {
            // Create and save
            timecard = new TimeCard();
            timecard.prepare(token.getCompanyId());
            timecard.setShopId(token.getShopId());
            timecard.setClockin(true);
            timecard.setClockInTime(DateTime.now().getMillis());
            timecard.setEmployeeId(employee.getId());
            timeCardRepository.save(timecard);

            employee.setTimecardId(timecard.getId());
            employeeRepository.setRecentTimecard(token.getCompanyId(), employee.getId(), timecard.getId());
        } else {
            throw new BlazeInvalidArgException("ClockIn", "Employee is already clocked in.");
        }
        TimeCardCompositeResult result = new TimeCardCompositeResult();
        result.setEmployee(employee);
        result.setTimeCard(timecard);
        return result;
    }

    @Override
    public TimeCardCompositeResult userClockIn() {
        String employeeId = token.getActiveTopUser().getUserId();
        Employee employee = employeeRepository.get(token.getCompanyId(), employeeId);
        if (employee == null) {
            throw new BlazeInvalidArgException("EmployeeClockIn", "Invalid employee.");
        }


        TimeCard timecard = timeCardRepository.getActiveTimeCard(token.getCompanyId(), token.getShopId(), employeeId);

        if (timecard == null) {
            // Create and save
            timecard = new TimeCard();
            timecard.prepare(token.getCompanyId());
            timecard.setShopId(token.getShopId());
            timecard.setClockin(true);
            timecard.setClockInTime(DateTime.now().getMillis());
            timecard.setEmployeeId(employeeId);
            timeCardRepository.save(timecard);

            employee.setTimecardId(timecard.getId());
            employeeRepository.setRecentTimecard(token.getCompanyId(), employeeId, timecard.getId());
        } else {
            throw new BlazeInvalidArgException("ClockIn", "You are already clocked in.");
        }
        TimeCardCompositeResult result = new TimeCardCompositeResult();
        result.setEmployee(employee);
        result.setTimeCard(timecard);
        return result;
    }

    @Override
    public void clockOut(String timeCardId, PinRequest request) {
        Employee employee = employeeRepository.getEmployeeByQuickPin(token.getCompanyId(), request.getPin());
        if (employee == null) {
            throw new BlazeInvalidArgException("EmployeeClockOut", "Invalid pin");
        }

        TimeCard dbTimeCard = timeCardRepository.get(token.getCompanyId(), timeCardId);

        // All good and well, let's log out
        if (dbTimeCard == null) {
            throw new BlazeInvalidArgException("EmployeeClockOut", "TimeCard doesn't exist.");
        }

        // Check if this user owns this timecard. If not, check if this user has admin permission
        if (!dbTimeCard.getEmployeeId().equalsIgnoreCase(employee.getId())) {
            // Check to see if this user is a manager by checking to see if he/she can access manager settings
            roleService.checkPermission(token.getCompanyId(), employee.getRoleId(), Role.Permission.iPadManageSettingsView);
        }

        dbTimeCard.setClockOutTime(DateTime.now().getMillis());
        dbTimeCard.setClockin(false);

        timeCardRepository.update(token.getCompanyId(), dbTimeCard.getId(), dbTimeCard);
    }

    @Override
    public void adminClockOut(String timeCardId) {
        TimeCard dbTimeCard = timeCardRepository.get(token.getCompanyId(), timeCardId);
        if (dbTimeCard == null) {
            throw new BlazeInvalidArgException("TimeCard", "TimeCard doesn't exist.");
        }
        Employee employee = employeeRepository.get(token.getCompanyId(), dbTimeCard.getEmployeeId());
        if (employee == null) {
            throw new BlazeInvalidArgException("Employee", "Employee doesn't exist for timecard.");
        }
        // Check to see if it's the logged in id
        if (!employee.getId().equalsIgnoreCase(token.getActiveTopUser().getUserId())) {
            // Don't match, check to see if this employee has permission to clockout
            Employee loggedInEmployee = employeeRepository.get(token.getCompanyId(), token.getActiveTopUser().getUserId());
            // Check to see if this user is a manager by checking to see if he/she can access manager settings
            roleService.checkPermission(token.getCompanyId(), loggedInEmployee.getRoleId(), Role.Permission.WebEmployeeManage);
        }
        dbTimeCard.setClockOutTime(DateTime.now().getMillis());
        dbTimeCard.setClockin(false);

        timeCardRepository.update(token.getCompanyId(), dbTimeCard.getId(), dbTimeCard);

    }

    @Override
    public void userClockOut() {
        TimeCard dbTimeCard = timeCardRepository.getActiveTimeCard(token.getCompanyId(), token.getShopId(), token.getActiveTopUser().getUserId());
        if (dbTimeCard == null) {
            throw new BlazeInvalidArgException("TimeCard", "TimeCard doesn't exist.");
        }
        dbTimeCard.setClockOutTime(DateTime.now().getMillis());
        dbTimeCard.setClockin(false);

        timeCardRepository.update(token.getCompanyId(), dbTimeCard.getId(), dbTimeCard);
    }
}
