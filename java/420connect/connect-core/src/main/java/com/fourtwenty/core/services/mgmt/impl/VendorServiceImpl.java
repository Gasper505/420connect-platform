package com.fourtwenty.core.services.mgmt.impl;

import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.domain.models.company.CompanyFeatures;
import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.models.generic.Note;
import com.fourtwenty.core.domain.models.product.Brand;
import com.fourtwenty.core.domain.models.product.CompanyLicense;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrder;
import com.fourtwenty.core.domain.repositories.dispensary.BrandRepository;
import com.fourtwenty.core.domain.repositories.dispensary.CompanyRepository;
import com.fourtwenty.core.domain.repositories.dispensary.EmployeeRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.domain.repositories.dispensary.VendorRepository;
import com.fourtwenty.core.event.BlazeEventBus;
import com.fourtwenty.core.event.vendor.VendorUpdateEvent;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.exceptions.BlazeQuickPinExistException;
import com.fourtwenty.core.rest.dispensary.requests.inventory.VendorAddRequest;
import com.fourtwenty.core.rest.dispensary.requests.inventory.VendorBulkUpdateRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.VendorResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.mgmt.VendorService;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.google.inject.Provider;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.EmailValidator;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Stephen Schmidt on 10/12/2015.
 */
public class VendorServiceImpl extends AbstractAuthServiceImpl implements VendorService {
    @Inject
    VendorRepository vendorRepository;
    @Inject
    ProductRepository productRepository;
    @Inject
    private BrandRepository brandRepository;
    @Inject
    private ShopRepository shopRepository;
    @Inject
    private EmployeeRepository employeeRepository;
    @Inject
    private CompanyRepository companyRepository;
    @Inject
    private BlazeEventBus eventBus;

    private static final String VENDOR = "Vendor";
    private static final String VENDOR_NOT_FOUND = "Vendor does not found";
    private static final String EMAIL_NOT_BLANK = "Email cannot be blank.";
    private static final String BULK_VENDOR_UPDATE = "Bulk Vendor Update";
    private static final String COMPANY_TYPE_NOT_FOUND = "Company type cannot be blank.";
    private static final String OWNER_NOT_EXIST = "Account owner isn't exist.";

    @Inject
    public VendorServiceImpl(Provider<ConnectAuthToken> token) {
        super(token);
    }

    @Override
    public Vendor addVendor(VendorAddRequest request) {
        Shop shop = shopRepository.getById(token.getShopId());

        if (StringUtils.isNotEmpty(request.getName())) {
            long count = vendorRepository.countVendorByVendorName(token.getCompanyId(), request.getName());
            if (count > 0) {
                throw new BlazeInvalidArgException("VendorAddRequest", "Vendor with this name already exist.");
            }
        }
        if (shop != null && !shop.isRetail()) {
            if (StringUtils.isNotBlank(request.getEmail()) && !EmailValidator.getInstance().isValid(request.getEmail())) {
                throw new BlazeQuickPinExistException("InvalidEmail", "Please specify a valid email.");
            }
        }

        Employee accountOwner = null;
        if (StringUtils.isNotBlank(request.getAccountOwnerId())) {
            accountOwner = employeeRepository.get(token.getCompanyId(), request.getAccountOwnerId());
            if (accountOwner == null) {
                throw new BlazeInvalidArgException(VENDOR, OWNER_NOT_EXIST);
            }
        }

        Vendor vendor = new Vendor();
        vendor.setCompanyId(token.getCompanyId());
        vendor.setName(request.getName());
        vendor.setDescription(request.getDescription());
        vendor.setEmail(request.getEmail());
        vendor.setPhone(request.getPhone());
        vendor.setAddress(request.getAddress());
        vendor.setActive(request.getActive());
        vendor.setLastName(request.getLastName());
        vendor.setFirstName(request.getFirstName());
        vendor.setLicenseNumber(request.getLicenseNumber());
        vendor.setWebsite(request.getWebsite());
        vendor.setFax(request.getFax());
        vendor.setActive(request.getActive());
        vendor.setAssets(request.getAssets());
        vendor.setConnectedShop(request.getConnectedShop());
        if (request.getAddress() != null) {
            request.getAddress().prepare();
        }
        vendor.setBrands(request.getBrands());
        vendor.setLicenseExpirationDate(request.getLicenseExpirationDate());
        vendor.setArmsLengthType(request.getArmsLengthType());

        vendor.setCompanyType(request.getCompanyType());
        if (request.getAdditionalAddressList() != null) {
            for (Address address : request.getAdditionalAddressList()) {
                address.prepare(token.getCompanyId());
            }
            vendor.setAdditionalAddressList(request.getAdditionalAddressList());
        }

        vendor.setLicenceType(request.getLicenceType());
        vendor.setRelatedEntity(request.isRelatedEntity());
        vendor.setMobileNumber(request.getMobileNumber());
        if (request.getNotes() != null) {
            for (Note note : request.getNotes()) {
                note.prepare();
                note.setWriterId(token.getActiveTopUser().getUserId());
                note.setWriterName(token.getActiveTopUser().getFirstName() + " " + token.getActiveTopUser().getLastName());
            }
            vendor.setNotes(request.getNotes());
        }
        vendor.setVendorType(request.getVendorType());
        vendor.setDbaName(request.getDbaName());

        updateCompanyLicense(shop, request.getCompanyLicenses(), request.getCompanyType(), request.getLicenceType(), request.getLicenseNumber(), request.getLicenseExpirationDate());

        vendor.setCompanyLicenses(request.getCompanyLicenses());
        vendor.setCreatedBy(token.getActiveTopUser().getUserId());
        vendor.setAccountOwnerId(accountOwner != null ? accountOwner.getId() : null);
        vendor.setContactPerson(request.getContactPerson());
        vendor.setSalesPerson(request.getSalesPerson());

        if (StringUtils.isNotBlank(request.getDefaultPaymentTerm())) {
            try {
                PurchaseOrder.POPaymentTerms defaultPaymentTerm = PurchaseOrder.POPaymentTerms.valueOf(request.getDefaultPaymentTerm());
                vendor.setDefaultPaymentTerm(defaultPaymentTerm.toString());
            } catch (Exception e) {
                throw new BlazeInvalidArgException(VENDOR, "Payment term is not valid");
            }
        } else {
            vendor.setDefaultPaymentTerm(StringUtils.EMPTY);
        }

        vendor.setDefaultContactId(request.getDefaultContactId());

        if (token.getGrowAppType().equals(ConnectAuthToken.GrowAppType.Operations)) {
            if (StringUtils.isBlank(request.getExternalId())) {
                throw new BlazeInvalidArgException("ExternalId", "External Id is missing");
            }
            vendor.setExternalId(request.getExternalId());
        }

        return vendorRepository.save(vendor);
    }

    @Override
    public Vendor sanitize(VendorAddRequest request) throws BlazeInvalidArgException {
        if (request == null || StringUtils.isBlank(request.getName())) {
            throw new BlazeInvalidArgException("VendorAddRequest", "Invalid arguments - vendor import requires name");
        }
        Shop shop = shopRepository.getById(token.getShopId());
        Vendor vendor = new Vendor();
        vendor.setImportId(request.getImportId());
        vendor.setCompanyId(token.getCompanyId());
        vendor.setName(request.getName());
        vendor.setDescription(request.getDescription());
        vendor.setEmail(request.getEmail());
        vendor.setPhone(request.getPhone());
        vendor.setAddress(request.getAddress());
        vendor.setActive(request.getActive());
        vendor.setFirstName(request.getContactFirstName());
        vendor.setLastName(request.getContactLastName());
        vendor.setLicenseNumber(request.getLicenseNumber());
        updateCompanyLicense(shop, request.getCompanyLicenses(), request.getCompanyType(), request.getLicenceType(), request.getLicenseNumber(), request.getLicenseExpirationDate());
        vendor.setCompanyLicenses(request.getCompanyLicenses());
        vendor.setFax(request.getFax());
        if (request.getAddress() != null) {
            request.getAddress().prepare();
        }
        if (CollectionUtils.isNotEmpty(request.getNotes())) {
            for (Note note : request.getNotes()) {
                note.prepare();
                note.setWriterId(token.getActiveTopUser().getUserId());
                note.setWriterName(token.getActiveTopUser().getFirstName() + " " + token.getActiveTopUser().getLastName());
            }
        }
        if (!shop.isRetail()) {
            vendor.setVendorType(Vendor.VendorType.BOTH);
            if (request.getVendorType() != null) {
                vendor.setVendorType(request.getVendorType());
            }
        } else {
            vendor.setVendorType(Vendor.VendorType.VENDOR);
        }
        vendor.setNotes(request.getNotes());
        return vendor;
    }

    @Override
    public Vendor updateVendor(String vendorId, Vendor vendor) {
        if (StringUtils.isBlank(vendorId) || !ObjectId.isValid(vendorId)) {
            throw new BlazeInvalidArgException("VendorAddRequest", "Invalid vendorId");
        }

        Vendor dbVendor = vendorRepository.get(token.getCompanyId(), vendorId);
        if (dbVendor == null) {
            throw new BlazeInvalidArgException("Vendor", "Vendor doesn't exist.");
        }

        if (StringUtils.isNotEmpty(vendor.getName()) && StringUtils.isNotBlank(dbVendor.getName()) && !dbVendor.getName().equals(vendor.getName())) {
            long count = vendorRepository.countVendorByVendorName(token.getCompanyId(), vendor.getName());
            if (count > 0) {
                throw new BlazeInvalidArgException("VendorAddRequest", "Vendor with this name already exist.");
            }
        }

        Shop shop = shopRepository.getById(token.getShopId());
        /*if (shop != null && !shop.isRetail()) {
            if (Vendor.VendorType.BOTH.equals(dbVendor.getVendorType()) && !Vendor.VendorType.BOTH.equals(vendor.getVendorType())) {
                throw new BlazeInvalidArgException(VENDOR, "Vendor with type BOTH are not allowed to update.");
            }
        }*/

        if (dbVendor.getVendorType() != vendor.getVendorType()) {
            VendorUpdateEvent event = new VendorUpdateEvent();
            event.setCompanyId(token.getCompanyId());
            event.setVendorId(vendorId);
            event.setReqVendorType(vendor.getVendorType());
            event.setDbVendorType(dbVendor.getVendorType());
            eventBus.post(event);
            event.getResponse();
        }

        Employee accountOwner = null;
        if (StringUtils.isNotBlank(vendor.getAccountOwnerId())) {
            accountOwner = employeeRepository.get(token.getCompanyId(), vendor.getAccountOwnerId());
            if (accountOwner == null) {
                throw new BlazeInvalidArgException(VENDOR, OWNER_NOT_EXIST);
            }
        }

        if (vendor.getAddress() != null) {
            vendor.getAddress().prepare();
        }


        if (vendor.getNotes() != null) {
            for (Note note : vendor.getNotes()) {
                if (note.getId() == null) {
                    note.setId(ObjectId.get().toString());
                    note.setWriterId(token.getActiveTopUser().getUserId());
                    note.setWriterName(token.getActiveTopUser().getFirstName() + " " + token.getActiveTopUser().getLastName());
                }
            }
        }

        dbVendor.setName(vendor.getName());
        dbVendor.setAddress(vendor.getAddress());
        dbVendor.setWebsite(vendor.getWebsite());
        dbVendor.setEmail(vendor.getEmail());
        dbVendor.setActive(vendor.getActive());
        dbVendor.setDescription(vendor.getDescription());
        dbVendor.setFax(vendor.getFax());
        dbVendor.setFirstName(vendor.getFirstName());
        dbVendor.setLastName(vendor.getLastName());
        dbVendor.setLicenseNumber(vendor.getLicenseNumber());
        dbVendor.setNotes(vendor.getNotes());
        dbVendor.setPhone(vendor.getPhone());
        dbVendor.setAssets(vendor.getAssets());
        dbVendor.setBackOrderEnabled(vendor.getBackOrderEnabled());
        dbVendor.setLicenseExpirationDate(vendor.getLicenseExpirationDate());
        dbVendor.setArmsLengthType(vendor.getArmsLengthType());
        if (CollectionUtils.isNotEmpty(dbVendor.getBrands())) {
            LinkedHashSet<String> brandList = dbVendor.getBrands();
            brandList.removeAll(vendor.getBrands());
            if (CollectionUtils.isNotEmpty(brandList)) {
                productRepository.updateBrandsInProduct(token.getCompanyId(), token.getShopId(), brandList, dbVendor.getId());
            }
        }
        dbVendor.setBrands(vendor.getBrands());

        dbVendor.setCompanyType(vendor.getCompanyType());
        if (vendor.getAdditionalAddressList() != null) {
            for (Address address : vendor.getAdditionalAddressList()) {
                address.prepare(token.getCompanyId());
            }
            dbVendor.setAdditionalAddressList(vendor.getAdditionalAddressList());
        }

        dbVendor.setLicenceType(vendor.getLicenceType());
        dbVendor.setRelatedEntity(vendor.isRelatedEntity());
        dbVendor.setMobileNumber(vendor.getMobileNumber());
        dbVendor.setVendorType(vendor.getVendorType());
        dbVendor.setCredits(vendor.getCredits());
        if (vendor.getNotes() != null) {
            for (Note note : vendor.getNotes()) {
                note.prepare();
            }
            dbVendor.setNotes(vendor.getNotes());
        }
        dbVendor.setDbaName(vendor.getDbaName());

        if (token.getGrowAppType().equals(ConnectAuthToken.GrowAppType.Operations)) {
            if (StringUtils.isBlank(vendor.getExternalId())) {
                throw new BlazeInvalidArgException("ExternalId", "External Id is missing");
            }
            dbVendor.setExternalId(vendor.getExternalId());
        }
        updateCompanyLicense(shop, vendor.getCompanyLicenses(), vendor.getCompanyType(), vendor.getLicenceType(), vendor.getLicenseNumber(), vendor.getLicenseExpirationDate());
        dbVendor.setCompanyLicenses(vendor.getCompanyLicenses());
        dbVendor.setAccountOwnerId(accountOwner != null ? accountOwner.getId() : null);

        dbVendor.setDefaultPaymentTerm(vendor.getDefaultPaymentTerm());
        dbVendor.setSalesPerson(vendor.getSalesPerson());
        if (StringUtils.isNotBlank(vendor.getDefaultPaymentTerm())) {
            try {
                PurchaseOrder.POPaymentTerms defaultPaymentTerm = PurchaseOrder.POPaymentTerms.valueOf(vendor.getDefaultPaymentTerm());
                dbVendor.setDefaultPaymentTerm(defaultPaymentTerm.toString());
            } catch (Exception e) {
                throw new BlazeInvalidArgException(VENDOR, "Payment term is not valid");
            }
        } else {
            dbVendor.setDefaultPaymentTerm(StringUtils.EMPTY);
        }
        dbVendor.setDefaultContactId(vendor.getDefaultContactId());
        dbVendor.setContactPerson(vendor.getContactPerson());
        return vendorRepository.update(token.getCompanyId(), vendorId, dbVendor);
    }

    @Override
    public List<Vendor> getAllVendors() {
        Iterable<Vendor> items = vendorRepository.list(token.getCompanyId());
        return Lists.newArrayList(items);
    }

    @Override
    public SearchResult<VendorResult> getVendors(String vendorId, int start, int limit, Vendor.VendorType vendorType, String term, List<Vendor.CompanyType> companyType) {

        if (limit <= 0 || limit > 1000) {
            limit = 1000;
        }
        if (companyType != null) {
            companyType.removeAll(Collections.singleton(null));
        }
        if (StringUtils.isNotEmpty(vendorId)) {
            SearchResult<VendorResult> vendorSearchResult = new SearchResult<>();
            VendorResult vendor = vendorRepository.get(token.getCompanyId(), vendorId, VendorResult.class);
            if (vendor != null) {
                HashMap<String, Brand> brandHashMap = prepareVendor(vendor);
                prepareVendorResult(vendor, brandHashMap);

                vendorSearchResult.getValues().add(vendor);
                vendorSearchResult.setTotal(1l);
            }
            return vendorSearchResult;
        } else if (StringUtils.isNotBlank(term)) {
            if (vendorType == null) {
                vendorType = Vendor.VendorType.VENDOR;
            }

            if (companyType != null && !companyType.isEmpty()) {
                SearchResult<VendorResult> result = vendorRepository.findItemsByCompanyTypeAndTerm(token.getCompanyId(), "{name:1}", start, limit, vendorType, companyType, term, VendorResult.class);
                prepareSearchResult(result);
                return result;
            }
            SearchResult<VendorResult> result = vendorRepository.findItemsByTerm(token.getCompanyId(), "{name:1}", start, limit, vendorType, term, VendorResult.class);
            prepareSearchResult(result);
            return result;
        } else {
            if (vendorType == null && (companyType == null || companyType.isEmpty())) {
                vendorType = Vendor.VendorType.VENDOR;
            }
            SearchResult<VendorResult> result = vendorRepository.findItemsByVendorAndCompanyType(token.getCompanyId(), "{name:1}", start, limit, vendorType, companyType, VendorResult.class);
            prepareSearchResult(result);
            return result;
        }
    }

    private void prepareSearchResult(SearchResult<VendorResult> result) {
        if (result != null && result.getValues() != null) {
            Set<ObjectId> brandIds = new HashSet<>();

            for (VendorResult vendorResult : result.getValues()) {
                if (vendorResult.getBrands() != null) {
                    for (String brandId : vendorResult.getBrands()) {
                        if (ObjectId.isValid(brandId)) {
                            brandIds.add(new ObjectId(brandId));
                        }
                    }
                }
            }

            HashMap<String, Brand> brandHashMap = brandRepository.listAsMap(token.getCompanyId(), new ArrayList<>(brandIds));

            for (VendorResult vendorResult : result.getValues()) {
                prepareVendorResult(vendorResult, brandHashMap);
            }
        }
    }

    private void prepareVendorResult(VendorResult vendorResult, HashMap<String, Brand> brandHashMap) {
        List<Brand> brandList = new ArrayList<>();

        if (vendorResult.getBrands() != null && brandHashMap != null) {
            for (String brandId : vendorResult.getBrands()) {
                Brand brand = brandHashMap.get(brandId);
                if (brand != null) {
                    brandList.add(brand);
                }
            }
        }

        vendorResult.setBrandList(brandList);
    }

    @Override
    public VendorResult getVendor(String vendorId) {
        VendorResult vendorResult = vendorRepository.get(token.getCompanyId(), vendorId, VendorResult.class);

        HashMap<String, Brand> brandHashMap = prepareVendor(vendorResult);
        prepareVendorResult(vendorResult, brandHashMap);

        return vendorResult;
    }

    private HashMap<String, Brand> prepareVendor(VendorResult vendorResult) {
        HashMap<String, Brand> brandHashMap = null;
        if (vendorResult.getBrands() != null) {
            List<ObjectId> brandIds = new ArrayList<>();
            for (String brandId : vendorResult.getBrands()) {
                if (ObjectId.isValid(brandId)) {
                    brandIds.add(new ObjectId(brandId));
                }
            }
            brandHashMap = brandRepository.listAsMap(token.getCompanyId(), brandIds);
        }

        return brandHashMap;
    }

    @Override
    public SearchResult<VendorResult> getVendorsByDate(long afterDate, long beforeDate) {
        SearchResult<VendorResult> results = vendorRepository.findItemsWithDate(token.getCompanyId(), afterDate, beforeDate, VendorResult.class);

        prepareSearchResult(results);

        return results;
    }

    @Override
    public void deleteVendor(String vendorId) {
        vendorRepository.removeByIdSetState(token.getCompanyId(), vendorId);
    }

    @Override
    public SearchResult<VendorResult> getAllVendorByCategoryId(String categoryId) {
        Iterable<Product> ids = productRepository.getVendorIdListByCategoryId(token.getCompanyId(), token.getShopId(), categoryId, "{vendorId:1}");
        Set<ObjectId> objIds = new HashSet<>();

        for (Product id : ids) {
            objIds.add(new ObjectId(id.getVendorId()));
        }

        Iterable<VendorResult> list = vendorRepository.findItemsIn(token.getCompanyId(), Lists.newArrayList(objIds), VendorResult.class);
        SearchResult<VendorResult> vendorSearchResult = new SearchResult<>();
        vendorSearchResult.setValues(Lists.newArrayList(list));

        prepareSearchResult(vendorSearchResult);

        return vendorSearchResult;
    }

    @Override
    public SearchResult<Brand> getBrandsByVendorId(String vendorId, int start, int limit, String term) {
        if (limit <= 0 || limit > 1000) {
            limit = 1000;
        }
        Vendor vendor = vendorRepository.get(token.getCompanyId(), vendorId);

        if (vendor == null) {
            throw new BlazeInvalidArgException(VENDOR, VENDOR_NOT_FOUND);
        }
        SearchResult<Brand> brands = new SearchResult<>();
        if (vendor.getBrands() != null && vendor.getBrands().size() > 0) {
            List<ObjectId> brandIds = new ArrayList<>();

            for (String brandId : vendor.getBrands()) {
                if (StringUtils.isNotBlank(brandId) && ObjectId.isValid(brandId)) {
                    brandIds.add(new ObjectId(brandId));
                }
            }
            if (StringUtils.isNotBlank(term)) {
                brands = brandRepository.getBrandsByIdAndTerm(token.getCompanyId(), brandIds, term, "{ name :1}", start, limit);
            } else {
                brands = brandRepository.getBrandsByIds(token.getCompanyId(), brandIds, "{name:1}", start, limit);
            }
        }
        return brands;
    }

    /**
     * @param vendorList : list of vendor ids in string
     * @implNote Check if list of vendor id's is valid or not
     */
    @Override
    public boolean checkValidVendors(List<String> vendorList) {

        List<ObjectId> vendorIds = new ArrayList<>();
        for (String id : vendorList) {
            if (StringUtils.isNotBlank(id) && ObjectId.isValid(id)) {
                vendorIds.add(new ObjectId(id));
            } else {
                return false;
            }
        }

        HashMap<String, Vendor> vendorHashMap = vendorRepository.listAsMap(token.getCompanyId(), vendorIds);

        if (vendorHashMap == null || vendorHashMap.size() < vendorList.size()) {
            return false;
        }

        return true;
    }

    /**
     * @param request {@link VendorBulkUpdateRequest}
     * @implNote Vendor Bulk Update.
     */
    @Override
    public void bulkVendorUpdates(VendorBulkUpdateRequest request) {
        List<String> vendorIds = request.getVendorId();
        if (CollectionUtils.isEmpty(vendorIds)) {
            throw new BlazeInvalidArgException(BULK_VENDOR_UPDATE, "Vendor Id does not exist.");
        }

        if (request.getOperationType() == null) {
            throw new BlazeInvalidArgException(BULK_VENDOR_UPDATE, "Operation type does not exist.");
        }

        List<ObjectId> vendorObjectIdList = new ArrayList<>();
        for (String vendorId : vendorIds) {
            vendorObjectIdList.add(new ObjectId(vendorId));
        }

        switch (request.getOperationType()) {
            case STATUS:
                vendorRepository.bulkUpdateStatus(token.getCompanyId(), vendorObjectIdList, request.isActive());
                break;

            case ENABLE_BACK_ORDER:
                vendorRepository.bulkUpdateBackOrderEnabled(token.getCompanyId(), vendorObjectIdList, request.isBackOrderEnabled());
                break;

            case TAX_TYPE:
                if (request.getArmsLengthType() == null) {
                    request.setArmsLengthType(Vendor.ArmsLengthType.ARMS_LENGTH);
                }
                vendorRepository.bulkUpdateTax(token.getCompanyId(), vendorObjectIdList, request.getArmsLengthType());
                break;

            case BRAND:
                if (CollectionUtils.isEmpty(request.getBrands())) {
                    throw new BlazeInvalidArgException(BULK_VENDOR_UPDATE, "Brand does not exist.");
                }
                vendorRepository.bulkUpdateBrands(token.getCompanyId(), vendorObjectIdList, request.getBrands());
                break;
        }
    }

    /**
     * Private method to update vendor license to support multiple licenses or create one default as RETAILER
     *
     * @param shop                  : shop
     * @param companyLicenses       : list of company licenses
     * @param companyType           : company type
     * @param licenseType           : license type
     * @param licenseNumber         : license number
     * @param licenseExpirationDate : license expiry date
     */
    private void updateCompanyLicense(Shop shop, List<CompanyLicense> companyLicenses, Vendor.CompanyType companyType, Vendor.LicenceType licenseType, String licenseNumber, long licenseExpirationDate) {
        if (CollectionUtils.isEmpty(companyLicenses) && StringUtils.isNotBlank(licenseNumber)) {
            CompanyLicense companyLicense = new CompanyLicense();
            companyLicense.prepare();
            companyLicense.setCompanyType(companyType == null ? Vendor.CompanyType.RETAILER : companyType);
            companyLicense.setLicenseType(licenseType);
            companyLicense.setLicenseNumber(licenseNumber);
            companyLicense.setLicenseExpirationDate(licenseExpirationDate);
            companyLicense.setToDefault(true);
            companyLicenses.add(companyLicense);
        } else if (CollectionUtils.isNotEmpty(companyLicenses)) {
            boolean defaultUpdate = false;
            String defaultLicenseId = "";

            if (shop != null && shop.getAppTarget() == CompanyFeatures.AppTarget.Retail) {
                CompanyLicense companyLicense = companyLicenses.get(0);
                if (companyLicense != null) {
                    companyLicense.setLicenseType(licenseType);
                    companyLicense.setCompanyType(companyType == null ? Vendor.CompanyType.RETAILER : companyType);
                    companyLicense.setLicenseNumber(licenseNumber);
                    companyLicense.setLicenseExpirationDate(licenseExpirationDate);
                }
            }
            for (CompanyLicense companyLicense : companyLicenses) {
                if (companyLicense.getCompanyType() == null) {
                    throw new BlazeInvalidArgException(VENDOR, COMPANY_TYPE_NOT_FOUND);
                }
                companyLicense.prepare();
                // Find if default license is updated.

                if (StringUtils.isBlank(defaultLicenseId)) {
                    defaultLicenseId = companyLicense.getId(); // default set to first
                }
                defaultUpdate = companyLicense.isToDefault();
                if (defaultUpdate) {
                    defaultLicenseId = companyLicense.getId(); // set to latest default
                }
            }

            for (CompanyLicense companyLicense : companyLicenses) {
                if (!companyLicense.getId().equals(defaultLicenseId)) {
                    companyLicense.setToDefault(false);
                } else {
                    companyLicense.setToDefault(true);
                }
            }
        }
    }

    @Override
    public Vendor getDefaultVendor(String companyId) {
        Vendor vendor = vendorRepository.getDefaultVendor(companyId);

        if (vendor == null) {
            Company company = companyRepository.getById(companyId);

            vendor = new Vendor();
            vendor.prepare(companyId);
            vendor.setToDefault(true);
            vendor.setName(company.getName() + "- Default");
            vendor.setCreatedBy(token.getActiveTopUser().getUserId());
            vendor.setActive(true);
            vendor.setEmail(company.getEmail());
            vendor.setPhone(company.getPhoneNumber());
            vendor.setAddress(company.getAddress());
            vendor.setDescription("Default vendor");
            vendor.setVendorType(Vendor.VendorType.BOTH);
            vendor.setCompanyType(Vendor.CompanyType.CULTIVATOR);
            vendor = vendorRepository.save(vendor);
        }

        return vendor;
    }
}
