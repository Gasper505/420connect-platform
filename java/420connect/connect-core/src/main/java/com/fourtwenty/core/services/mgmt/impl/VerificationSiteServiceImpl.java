package com.fourtwenty.core.services.mgmt.impl;

import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.exceptions.BlazeAuthException;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.mgmt.VerificationSiteService;
import com.google.inject.Inject;
import com.google.inject.Provider;
import net.sf.json.JSONObject;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Gaurav Saini on 16/5/17.
 */

public class VerificationSiteServiceImpl extends AbstractAuthServiceImpl implements VerificationSiteService {

    @Inject
    private ConnectConfiguration config;

    @Inject
    public VerificationSiteServiceImpl(Provider<ConnectAuthToken> tokenProvider) {
        super(tokenProvider);
    }

    public String getHelloMDPatient(String verificationCode) {

        CloseableHttpClient httpClient = HttpClientBuilder.create().build();
        HttpGet getRequest = new HttpGet("https://www.hellomd.com/api/v1/recommendation/" + verificationCode);
        getRequest.setHeader("Accept", "application/json");
        getRequest.setHeader("Content-Type", "application/json");

        String apiKey = config.getVerificationSiteConfig().getApiKey();
        String secretKey = config.getVerificationSiteConfig().getSecretKey();

        String binaryData = apiKey + ":" + secretKey;
        getRequest.setHeader("Authorization", "Basic " + Base64.encodeBase64String(binaryData.getBytes()));

        HttpResponse httpResponse = null;
        try {
            httpResponse = httpClient.execute(getRequest);
        } catch (IOException e) {
            e.printStackTrace();
        }
        int statusCode = httpResponse.getStatusLine().getStatusCode();

        StringBuilder builder = new StringBuilder();

        if (statusCode == 401) {
            throw new BlazeAuthException("Authentication", "api and secret Keys not valid.");
        }

        BufferedReader br = null;
        try {
            br = new BufferedReader(
                    new InputStreamReader((httpResponse.getEntity().getContent())));
        } catch (IOException e) {
            e.printStackTrace();
        }
        String output;
        try {
            while ((output = br.readLine()) != null) {

                builder.append(output);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (statusCode != 200) {
            JSONObject jsonObject = JSONObject.fromObject(builder.toString());
            throw new BlazeInvalidArgException("error_message", jsonObject.getString("error_message"));

        }

        return builder.toString();
    }
}