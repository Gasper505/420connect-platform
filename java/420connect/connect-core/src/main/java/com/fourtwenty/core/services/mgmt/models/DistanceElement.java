package com.fourtwenty.core.services.mgmt.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by mdo on 11/9/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DistanceElement {
    private ElementValue distance;
    private ElementValue duration;
    private String status;

    public ElementValue getDistance() {
        return distance;
    }

    public void setDistance(ElementValue distance) {
        this.distance = distance;
    }

    public ElementValue getDuration() {
        return duration;
    }

    public void setDuration(ElementValue duration) {
        this.duration = duration;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
