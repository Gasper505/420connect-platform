package com.fourtwenty.core.services.mgmt.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mdo on 11/9/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DistanceElementWrapper {
    private List<DistanceElement> elements = new ArrayList<>();

    public List<DistanceElement> getElements() {
        return elements;
    }

    public void setElements(List<DistanceElement> elements) {
        this.elements = elements;
    }
}
