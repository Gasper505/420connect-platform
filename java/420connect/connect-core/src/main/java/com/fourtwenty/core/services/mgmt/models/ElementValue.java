package com.fourtwenty.core.services.mgmt.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by mdo on 11/9/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ElementValue {
    private String text;
    private int value;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
