package com.fourtwenty.core.services.mgmt.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fourtwenty.core.domain.models.generic.BaseModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mdo on 11/9/17.
 * <p>
 * {
 * "destination_addresses" : [ "San Francisco, CA, USA" ],
 * "origin_addresses" : [ "639 Blackwood Ave, Clovis, CA 93619, USA" ],
 * "rows" : [
 * {
 * "elements" : [
 * {
 * "distance" : {
 * "text" : "200 mi",
 * "value" : 321090
 * },
 * "duration" : {
 * "text" : "3 hours 11 mins",
 * "value" : 11438
 * },
 * "status" : "OK"
 * }
 * ]
 * }
 * ],
 * "status" : "OK"
 * }
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class GoogleDistanceResult extends BaseModel {

    @JsonProperty("destination_addresses")
    private List<String> destinations = new ArrayList<>();
    @JsonProperty("origin_addresses")
    private List<String> origins = new ArrayList<>();
    private String status;
    private List<DistanceElementWrapper> rows = new ArrayList<>();

    public List<DistanceElementWrapper> getRows() {
        return rows;
    }

    public void setRows(List<DistanceElementWrapper> rows) {
        this.rows = rows;
    }

    public List<String> getDestinations() {
        return destinations;
    }

    public void setDestinations(List<String> destinations) {
        this.destinations = destinations;
    }

    public List<String> getOrigins() {
        return origins;
    }

    public void setOrigins(List<String> origins) {
        this.origins = origins;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
