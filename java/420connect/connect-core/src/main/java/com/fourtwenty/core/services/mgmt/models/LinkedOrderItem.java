package com.fourtwenty.core.services.mgmt.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.transaction.OrderItem;

import java.util.UUID;

/**
 * Created by mdo on 9/8/17.
 * <p>
 * This is to be used internally to calculate normal pricing with mix&match
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class LinkedOrderItem extends OrderItem {
    private String origOrderItemId;
    private boolean unlinked = false;

    public String getOrigOrderItemId() {
        return origOrderItemId;
    }

    public void setOrigOrderItemId(String origOrderItemId) {
        this.origOrderItemId = origOrderItemId;
    }

    public boolean isUnlinked() {
        return unlinked;
    }

    public void setUnlinked(boolean unlinked) {
        this.unlinked = unlinked;
    }

    public static LinkedOrderItem convertFromOrderItem(OrderItem orderItem, boolean isPreparingFulfillment) {
        LinkedOrderItem linkedOrderItem = new LinkedOrderItem();
        linkedOrderItem.prepare(orderItem.getCompanyId());
        linkedOrderItem.setOrigOrderItemId(orderItem.getId());
        linkedOrderItem.setOrderItemId(UUID.randomUUID().toString());
        linkedOrderItem.setQuantity(orderItem.getQuantity());
        linkedOrderItem.setRemarks(orderItem.getRemarks());
        linkedOrderItem.setProduct(orderItem.getProduct());
        linkedOrderItem.setProductId(orderItem.getProductId());
        linkedOrderItem.setCost(orderItem.getCost());
        linkedOrderItem.setUnitPrice(orderItem.getUnitPrice());
        linkedOrderItem.setDiscount(orderItem.getDiscount());
        linkedOrderItem.setDiscountType(orderItem.getDiscountType());
        linkedOrderItem.setFinalPrice(orderItem.getFinalPrice());
        linkedOrderItem.setStatus(orderItem.getStatus());
        linkedOrderItem.setQuantityLogs(orderItem.getQuantityLogs());
        linkedOrderItem.setMixMatched(orderItem.isMixMatched());
        linkedOrderItem.setPromotionReqs(orderItem.getPromotionReqs());
        linkedOrderItem.setPrepackageItemId(orderItem.getPrepackageItemId());
        linkedOrderItem.setBatchId(orderItem.getBatchId());
        linkedOrderItem.setPrepackage(orderItem.getPrepackage());
        linkedOrderItem.setPrepackageProductItem(orderItem.getPrepackageProductItem());
        linkedOrderItem.setIgnoreMixMatch(orderItem.isIgnoreMixMatch());
        linkedOrderItem.setOverridePrice(orderItem.getOverridePrice());
        linkedOrderItem.setFinalized(orderItem.isFinalized());
        linkedOrderItem.setFinalizedProductId(orderItem.getFinalizedProductId());
        return linkedOrderItem;
    }
}
