package com.fourtwenty.core.services.notification;


import com.fourtwenty.core.config.FcmConfig;
import com.fourtwenty.core.domain.models.transaction.FcmPayload;

public interface FcmService {
    String send(FcmConfig fcmConfig, FcmPayload body);
}
