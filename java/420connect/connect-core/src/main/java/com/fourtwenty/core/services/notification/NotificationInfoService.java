package com.fourtwenty.core.services.notification;

import com.fourtwenty.core.domain.models.notification.NotificationInfo;
import com.fourtwenty.core.rest.dispensary.requests.notification.NotificationInfoAddRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;

import java.util.List;

public interface NotificationInfoService {

    NotificationInfo addNotification(NotificationInfoAddRequest request);

    NotificationInfo updateNotification(String notificationId, NotificationInfo request);

    SearchResult<NotificationInfo> getNotification(String notificationId, String shopId, NotificationInfo.NotificationType type, int start, int limit);

    void deleteNotification(String notificationId);

    List<NotificationInfo> updateNotificationList(List<NotificationInfo> notificationInfos);

    List<NotificationInfo> getNotificationInfoList();
}
