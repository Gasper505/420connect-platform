package com.fourtwenty.core.services.notification.impl;

import com.fourtwenty.core.config.FcmConfig;
import com.fourtwenty.core.services.notification.FcmService;
import com.fourtwenty.core.util.JsonSerializer;
import com.fourtwenty.core.domain.models.transaction.FcmPayload;
import com.mdo.pusher.SimpleRestUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;

public class FcmServiceImpl implements FcmService {
    private static final Logger logUtil = LoggerFactory.getLogger(FcmServiceImpl.class);

    @Override
    public String send(FcmConfig fcmConfig, FcmPayload body) {

        String jsonBody = JsonSerializer.toJson(body);
        try {
            String messageResponse = SimpleRestUtil.postWithSsl(fcmConfig.getHost(), jsonBody, String.class, getHeaders(fcmConfig));
            logUtil.info("Message Send Successfully to " + body.getToken() + messageResponse + "Message  Info : "+ jsonBody);
            return  messageResponse;
        } catch (Exception e) {
            logUtil.info("Error in Message Send : " + e.getMessage());
        }
        return "";
    }

    private MultivaluedMap<String, Object> getHeaders(FcmConfig fcmConfig) {
        MultivaluedMap<String, Object> headers = new MultivaluedHashMap<>();
        headers.putSingle("Authorization", "key=" + fcmConfig.getSecret());
        headers.putSingle("Content-Type", MediaType.APPLICATION_JSON);
        return headers;
    }

}
