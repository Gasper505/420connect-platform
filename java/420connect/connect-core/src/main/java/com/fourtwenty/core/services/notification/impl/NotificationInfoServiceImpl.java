package com.fourtwenty.core.services.notification.impl;

import com.fourtwenty.core.domain.models.notification.NotificationInfo;
import com.fourtwenty.core.domain.repositories.notification.NotificationInfoRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.rest.dispensary.requests.notification.NotificationInfoAddRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.notification.NotificationInfoService;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.google.inject.Provider;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


public class NotificationInfoServiceImpl extends AbstractAuthServiceImpl implements NotificationInfoService {

    private static final String NOTIFICATION_INFO = "Notification Info";
    private static final String NOTIFICATION_TYPE_NOT_FOUND = "Notification type not found";
    private static final String NOTIFICATION_NOT_FOUND = "Notification Info not found";

    @Inject
    private NotificationInfoRepository notificationInfoRepository;

    @Inject
    public NotificationInfoServiceImpl(Provider<ConnectAuthToken> token) {
        super(token);
    }

    /**
     * Override method to create notification info for particular shop.
     *
     * @param request : request to create notification info
     * @return : return notification info
     */
    @Override
    public NotificationInfo addNotification(NotificationInfoAddRequest request) {
        if (request == null || request.getNotificationType() == null) {
            throw new BlazeInvalidArgException(NOTIFICATION_INFO, NOTIFICATION_TYPE_NOT_FOUND);
        }
        String shopId = StringUtils.isNotBlank(request.getShopId()) ? request.getShopId() : token.getShopId();
        NotificationInfo notificationInfo = notificationInfoRepository.getByShopAndType(token.getCompanyId(), shopId, request.getNotificationType());
        if (notificationInfo == null) {
            notificationInfo = new NotificationInfo();
            notificationInfo.prepare(token.getCompanyId());
            notificationInfo.setShopId(shopId);
            notificationInfo.setNotificationType(request.getNotificationType());
            notificationInfo.setLastSent(request.getLastSent());
            notificationInfo.setActive(request.isActive());
            notificationInfo.setDefaultEmail(request.getDefaultEmail());
            notificationInfo.setDefaultNumber(request.getDefaultNumber());
            notificationInfo = notificationInfoRepository.save(notificationInfo);
        } else {
            notificationInfo.setLastSent(request.getLastSent());
            notificationInfo.setActive(request.isActive());
            notificationInfo.setDefaultEmail(request.getDefaultEmail());
            notificationInfo.setDefaultNumber(request.getDefaultNumber());
            notificationInfo = notificationInfoRepository.update(token.getCompanyId(), notificationInfo.getId(), notificationInfo);
        }


        return notificationInfo;
    }

    /**
     * Override method to update notification info
     *
     * @param notificationId : notification id
     * @param request        : request to update notification info
     * @return : return updated notification info
     */
    @Override
    public NotificationInfo updateNotification(String notificationId, NotificationInfo request) {
        if (request == null || request.getNotificationType() == null) {
            throw new BlazeInvalidArgException(NOTIFICATION_INFO, NOTIFICATION_TYPE_NOT_FOUND);
        }
        NotificationInfo dbNotificationInfo = notificationInfoRepository.get(token.getCompanyId(), notificationId);
        if (dbNotificationInfo == null) {
            throw new BlazeInvalidArgException(NOTIFICATION_INFO, NOTIFICATION_NOT_FOUND);
        }
        dbNotificationInfo.setNotificationType(request.getNotificationType());
        dbNotificationInfo.setLastSent(request.getLastSent());
        dbNotificationInfo.setActive(request.isActive());
        dbNotificationInfo.setDefaultEmail(request.getDefaultEmail());
        dbNotificationInfo.setDefaultNumber(request.getDefaultNumber());
        return notificationInfoRepository.update(token.getCompanyId(), notificationId, dbNotificationInfo);
    }

    /**
     * Override method to get notification info by either id or shop
     *
     * @param notificationId : notification info id
     * @param shopId         : shopId
     * @param start          : start
     * @param limit          : limit
     * @return : return list of notification info
     */
    @Override
    public SearchResult<NotificationInfo> getNotification(String notificationId, String shopId, NotificationInfo.NotificationType type, int start, int limit) {
        SearchResult<NotificationInfo> result = new SearchResult<>();
        if (StringUtils.isNotBlank(notificationId)) {
            NotificationInfo dbNotificationInfo = notificationInfoRepository.get(token.getCompanyId(), notificationId);
            if (dbNotificationInfo == null) {
                throw new BlazeInvalidArgException(NOTIFICATION_INFO, NOTIFICATION_NOT_FOUND);
            }
            result.setValues(Lists.newArrayList(dbNotificationInfo));
            result.setSkip(start);
            result.setLimit(0);
            result.setTotal(1L);
        } else {
            limit = (limit == 0) ? Integer.MAX_VALUE : limit;
            shopId = (StringUtils.isNotBlank(shopId)) ? shopId : token.getShopId();
            if (type == null) {
                result = notificationInfoRepository.findItems(token.getCompanyId(), shopId, start, limit);
            } else {
                result = notificationInfoRepository.findItemsByType(token.getCompanyId(), shopId, type, start, limit);
            }
        }
        return result;
    }

    @Override
    public void deleteNotification(String notificationId) {
        if (StringUtils.isBlank(notificationId)) {
            throw new BlazeInvalidArgException(NOTIFICATION_INFO, NOTIFICATION_NOT_FOUND);
        }
        notificationInfoRepository.removeByIdSetState(token.getCompanyId(), notificationId);
    }

    /**
     * Override method to update or create notification info
     *
     * @param notificationInfos : list of notification infos
     * @return : return list of updated or saved notification info
     */
    @Override
    public List<NotificationInfo> updateNotificationList(List<NotificationInfo> notificationInfos) {
        List<NotificationInfo> returnList = new ArrayList<>();
        if (notificationInfos == null || notificationInfos.isEmpty()) {
            return returnList;
        }

        notificationInfos.forEach(info -> {
            if (StringUtils.isBlank(info.getId())) {
                info.prepare(token.getCompanyId());
            }
        });

        List<String> companyIds = notificationInfos
                .stream()
                .map(info -> info.getCompanyId())
                .distinct()
                .collect(Collectors.toList());

        List<NotificationInfo> existingNotificationInfo = new ArrayList<>();
        companyIds.forEach(companyId ->
                existingNotificationInfo.addAll(Lists.newArrayList(notificationInfoRepository.list(companyId))));

        for (NotificationInfo notificationInfo : notificationInfos) {
            NotificationInfo newNotificationInfo = existingNotificationInfo.stream()
                    .filter(info ->
                            info.getCompanyId().equals(notificationInfo.getCompanyId()) &&
                                    info.getShopId().equals(notificationInfo.getShopId()) &&
                                    info.getNotificationType() == notificationInfo.getNotificationType())
                    .findFirst()
                    .orElse(notificationInfo);

            if (notificationInfo == newNotificationInfo) {
                returnList.add(notificationInfoRepository.save(newNotificationInfo));
            } else {
                if (newNotificationInfo.getNotificationType() == null) {
                    continue;
                }
                newNotificationInfo.setActive(notificationInfo.isActive());
                newNotificationInfo.setDefaultEmail(StringUtils.isNotBlank(notificationInfo.getDefaultEmail()) ? notificationInfo.getDefaultEmail() : StringUtils.EMPTY);
                newNotificationInfo.setDefaultNumber(StringUtils.isNotBlank(notificationInfo.getDefaultNumber()) ? notificationInfo.getDefaultNumber() : StringUtils.EMPTY);
                newNotificationInfo.setLastSent(notificationInfo.getLastSent());
                returnList.add(notificationInfoRepository.update(newNotificationInfo.getId(), newNotificationInfo));
            }
        }
        return returnList;

    }

    /**
     * Get notification info list for current shop if info exists add that info else create new for all types
     *
     * @return : return notification info
     */
    @Override
    public List<NotificationInfo> getNotificationInfoList() {

        List<NotificationInfo.NotificationType> notificationTypes = Arrays.asList(NotificationInfo.NotificationType.values());

        List<NotificationInfo> notificationInfos = Lists.newArrayList(notificationInfoRepository.listByShop(token.getCompanyId(), token.getShopId()));

        List<NotificationInfo.NotificationType> savedInfoTypes = notificationInfos
                .stream()
                .map(info -> info.getNotificationType())
                .distinct()
                .collect(Collectors.toList());

        notificationInfos.addAll(notificationTypes.stream()
                .filter(type -> !savedInfoTypes.contains(type))
                .map(type -> {
                    NotificationInfo notificationInfo = new NotificationInfo();
                    notificationInfo.setCompanyId(token.getCompanyId());
                    notificationInfo.setShopId(token.getShopId());
                    notificationInfo.setNotificationType(type);
                    notificationInfo.setActive(true); // Default value is true
                    return notificationInfo;
                })
                .collect(Collectors.toList()));

        return notificationInfos;
    }
}
