package com.fourtwenty.core.services.onprem;

import com.fourtwenty.core.rest.onprem.DownloadRequest;
import com.fourtwenty.core.rest.onprem.DownloadResponse;
import com.fourtwenty.core.rest.onprem.OnPremUploadRequest;

public interface OnPremCloudSyncService {

    <T> DownloadResponse<T> sendEntityData(DownloadRequest downloadRequest);
    <T> DownloadResponse<T> sendEntityIndexes(DownloadRequest downloadRequest);

    void uploadOnPremChanges(String entityName, OnPremUploadRequest body);


}
