package com.fourtwenty.core.services.onprem;

public interface OnPremSyncService {
    void initialDownload();
    void syncWithCloud();

}
