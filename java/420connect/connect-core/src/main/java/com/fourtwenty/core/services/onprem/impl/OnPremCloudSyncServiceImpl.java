package com.fourtwenty.core.services.onprem.impl;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.domain.models.company.CompanyBaseModel;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.repositories.onprem.OnPremSyncRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.rest.onprem.DownloadRequest;
import com.fourtwenty.core.rest.onprem.DownloadResponse;
import com.fourtwenty.core.rest.onprem.OnPremUploadRequest;
import com.fourtwenty.core.security.tokens.OnPremToken;
import com.fourtwenty.core.services.AbstractOnPremServiceImpl;
import com.fourtwenty.core.services.onprem.OnPremCloudSyncService;
import com.fourtwenty.core.util.JsonSerializer;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.mongodb.*;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class OnPremCloudSyncServiceImpl extends AbstractOnPremServiceImpl implements OnPremCloudSyncService {

    private static final Logger LOGGER = LoggerFactory.getLogger(OnPremCloudSyncServiceImpl.class);

    private OnPremSyncRepository onPremSyncRepository;

    @Inject
    public OnPremCloudSyncServiceImpl(Provider<OnPremToken> tokenProvider, OnPremSyncRepository onPremSyncRepository) {
        super(tokenProvider);
        this.onPremSyncRepository = onPremSyncRepository;
    }

    public <T> DownloadResponse<T> sendEntityData(DownloadRequest downloadRequest) {
        OnPremToken onPremToken = getOnPremToken();
        Class<?> entityClass = null;

        try {
            entityClass = Class.forName(downloadRequest.getEntityName());
        } catch (ClassNotFoundException e) {
            throw new BlazeInvalidArgException("Entity", "Entity class seems to be incorrect");
        }

        List<T> data;
        if (Company.class.equals(entityClass)) {
            data = (List<T>) onPremSyncRepository.get(new ObjectId(onPremToken.getCompanyId()), entityClass);
        } else if (CompanyBaseModel.class.isAssignableFrom(entityClass)) {
            if (downloadRequest.getAfterDate() > 0) {
                data = (List<T>) onPremSyncRepository.get(onPremToken.getCompanyId(), entityClass, downloadRequest.getAfterDate());
            } else {
                data = (List<T>) onPremSyncRepository.get(onPremToken.getCompanyId(), entityClass);
            }
        } else {
            if (downloadRequest.getAfterDate() > 0) {
                data = (List<T>) onPremSyncRepository.get(entityClass, downloadRequest.getAfterDate());
            } else {
                data = (List<T>) onPremSyncRepository.get(entityClass);
            }
        }

        DownloadResponse<T> downloadResponse = new DownloadResponse<>();
        downloadResponse.setData(data);
        downloadResponse.setTotal(data.size());
        return downloadResponse;
    }

    @Override
    public <T> DownloadResponse<T> sendEntityIndexes(DownloadRequest downloadRequest) {
        OnPremToken onPremToken = new OnPremToken();
        Class<?> entityClass = null;
        try {
            entityClass = Class.forName(downloadRequest.getEntityName());
        } catch (ClassNotFoundException e) {
            throw new BlazeInvalidArgException("Entity", "Entity class seems to be incorrect");
        }
        ArrayList data = (ArrayList) onPremSyncRepository.getIndexes(onPremToken.getCompanyId(), entityClass);
        DownloadResponse<T> downloadResponse = new DownloadResponse<>();
        downloadResponse.setData(data);
        downloadResponse.setTotal(data.size());
        return downloadResponse;
    }


    @Override
    public void uploadOnPremChanges(String entityName, OnPremUploadRequest body) {

        if (body != null && body.getData().size() > 0) {
            final ObjectMapper mapper = new ObjectMapper();
            mapper.enable(DeserializationFeature.READ_ENUMS_USING_TO_STRING);
            Class<?> entityClass = null;
            try {
                entityClass = Class.forName(entityName);
            } catch (ClassNotFoundException e) {
                throw new BlazeInvalidArgException("Entity", "Entity class seems to be incorrect");
            }

            List<Member> members = new ArrayList<>();


            try {
                DBCollection mongoCollection = onPremSyncRepository.getMongoDBCollection(entityClass);

                // bulk writes
                BulkWriteOperation bulkWriteOperation = mongoCollection.initializeOrderedBulkOperation();
                for (Object dataEntry : body.getData()) {
                    LinkedHashMap map = ((LinkedHashMap) dataEntry);
                    /*if (Member.class.isAssignableFrom(entityClass)) {
                        try {
                            Member member = mapper.convertValue(map, Member.class);
                            if (member != null) {
                                members.add(member);
                            }
                        } catch (Exception e) {
                        }
                    }*/
                    JsonSerializer.convertIds(map);
                    BasicDBObject o = new BasicDBObject(map);

                    String entityIdStr = String.valueOf(map.get("_id"));

                    ObjectId entityId = new ObjectId(entityIdStr);
                    BulkWriteRequestBuilder bulkWriteRequestBuilder = bulkWriteOperation.find(new BasicDBObject("_id",entityId));

                    BulkUpdateRequestBuilder updateReq = bulkWriteRequestBuilder.upsert();
                    updateReq.replaceOne(o);

                }
                bulkWriteOperation.execute();
                LOGGER.info("Data was saved for {}", entityClass.getName());
            } catch (Exception e) {
                LOGGER.error("Error downloading.." + entityClass.getName(), e);
            }
        }
    }


}
