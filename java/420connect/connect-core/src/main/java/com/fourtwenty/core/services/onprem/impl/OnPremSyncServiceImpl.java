package com.fourtwenty.core.services.onprem.impl;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.config.PlatformModeConfiguration;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.interfaces.OnPremSyncable;
import com.fourtwenty.core.domain.models.company.CompanyBaseModel;
import com.fourtwenty.core.domain.models.onprem.OnPremSyncDetails;
import com.fourtwenty.core.domain.repositories.onprem.OnPremSyncDetailsRepository;
import com.fourtwenty.core.domain.repositories.onprem.OnPremSyncRepository;
import com.fourtwenty.core.rest.onprem.DownloadResponse;
import com.fourtwenty.core.rest.onprem.OnPremUploadRequest;
import com.fourtwenty.core.services.onprem.OnPremSyncService;
import com.fourtwenty.core.util.JsonSerializer;
import com.fourtwenty.core.util.SecurityUtil;
import com.google.common.collect.Lists;
import com.mdo.pusher.SimpleRestUtil;
import com.mongodb.*;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.jongo.MongoCollection;
import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;

public class OnPremSyncServiceImpl implements OnPremSyncService {
    private static final Logger LOGGER = LoggerFactory.getLogger(OnPremSyncServiceImpl.class);
    private static final String DOWNLOAD_URL = "api/v1/onprem/download/";
    private static final String UPLOAD_URL = "api/v1/onprem/upload/";

    private ConnectConfiguration configuration;
    private SecurityUtil securityUtil;
    private ConnectConfiguration connectConfiguration;
    private OnPremSyncDetailsRepository onPremSyncDetailsRepository;
    private OnPremSyncRepository onPremSyncRepository;

    @Inject
    public OnPremSyncServiceImpl(ConnectConfiguration configuration,
                                 SecurityUtil securityUtil,
                                 ConnectConfiguration connectConfiguration,
                                 OnPremSyncDetailsRepository onPremSyncDetailsRepository,
                                 OnPremSyncRepository onPremSyncRepository) {
        this.configuration = configuration;
        this.securityUtil = securityUtil;
        this.connectConfiguration = connectConfiguration;
        this.onPremSyncDetailsRepository = onPremSyncDetailsRepository;
        this.onPremSyncRepository = onPremSyncRepository;
    }



    public DownloadResponse downloadDataFromCloud(String companyId, String entityName, long afterDate, String shopId) {
        String signature = "";
        String url = "";
        signature = generateEncryptedSignature(companyId);
        url = configuration.getPlatformModeConfiguration().getCloudURL() + DOWNLOAD_URL + entityName + "?afterDate=" + afterDate;
        if (StringUtils.isNotBlank(shopId)) {
            url += "&shopId=" + shopId;
        }

        return SimpleRestUtil.get(url, DownloadResponse.class, setHeaders(companyId, signature));
    }

    public void uploadDataToCloud(String companyId, String entityName, OnPremUploadRequest uploadRequest) {
        String signature = "";
        String url = "";
        signature = generateEncryptedSignature(companyId);
        url = configuration.getPlatformModeConfiguration().getCloudURL() + UPLOAD_URL + entityName;


        try {
            SimpleRestUtil.post(url, uploadRequest,DownloadResponse.class, setHeaders(companyId, signature));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private String generateEncryptedSignature(String companyId) {
        return securityUtil.encryptByOnPremApiSecret(companyId, configuration.getPlatformModeConfiguration().getSecret(), configuration.getPlatformModeConfiguration().getIv());
    }



    private MultivaluedMap<String, Object> setHeaders(String companyId, String signature) {
        MultivaluedMap<String, Object> headers = new MultivaluedHashMap<>();
        headers.putSingle("companyId", companyId);
        headers.putSingle("signature", signature);
        return headers;
    }

    @Override
    public void initialDownload() {

        if (connectConfiguration.getPlatformModeConfiguration() == null
                || (connectConfiguration.getPlatformModeConfiguration().getMode() != PlatformModeConfiguration.PlatformMode.OnPrem
                || connectConfiguration.getPlatformModeConfiguration().isEnablePremSync() == false)) {
            return;
        }
        long count = onPremSyncDetailsRepository.count(connectConfiguration.getPlatformModeConfiguration().getCompanyId());
        if (count == 0) {
            try {
                downloadFromCloud();
            } catch (Exception e) {
                LOGGER.info("Error downloading from cloud", e);
            }
        }

    }

    private <T> DownloadResponse<T> downloadFromCloud() {
        LOGGER.info("Starting the download from Cloud");
        // 2b. If it's the first sync, then download the complete data and insert sync details
        Map<Class, DownloadResponse> responseMap = new HashMap<>();
        List<Class<? extends OnPremSyncable>> entities = getSyncableEntities();


        final ObjectMapper mapper = new ObjectMapper();
        mapper.enable(DeserializationFeature.READ_ENUMS_USING_TO_STRING);

        for (Class entity : entities) {
            try {
                LOGGER.info("Downloading data for " + entity.getName());
                DownloadResponse downloadResponse = downloadDataFromCloud(connectConfiguration.getPlatformModeConfiguration().getCompanyId(), entity.getCanonicalName(), 0, null);


                LOGGER.info("Saving downloaded data for {} entity with total {} records", entity.getName(), downloadResponse.getTotal());
                OnPremSyncDetails onPremSyncDetails = new OnPremSyncDetails();
                onPremSyncDetails.prepare(connectConfiguration.getPlatformModeConfiguration().getCompanyId());
                onPremSyncDetails.setEntity(entity.getCanonicalName());
                onPremSyncDetails.setSyncType(OnPremSyncDetails.SyncType.Pull);
                long syncTime = DateTime.now().getMillis();
                onPremSyncDetails.setLastSync(syncTime);
                onPremSyncDetails.getSyncRecords().put(syncTime, downloadResponse.getTotal());

                List<Object> entries = new ArrayList<>();
                for (Object dataEntry : downloadResponse.getData()) {
                    Object en = mapper.convertValue(dataEntry, entity);
                    entries.add(en);
                }
                onPremSyncRepository.save(entries, entity);

                onPremSyncDetailsRepository.save(onPremSyncDetails);
                LOGGER.info("Data was saved for {}", entity.getName());
            } catch (Exception e) {
                LOGGER.error("Error downloading.." + entity.getName(), e);
            }
        }

        return null;
    }


    @Override
    public void syncWithCloud() {
        LOGGER.info("Starting OnPrem Sync Job");
        // Only run if this is an onpremise server and we want to sync w/ the cloud
        if (connectConfiguration.getPlatformModeConfiguration() != null && connectConfiguration.getPlatformModeConfiguration().isEnablePremSync()) {
            String companyId = connectConfiguration.getPlatformModeConfiguration().getCompanyId();
            long count = onPremSyncDetailsRepository.count(companyId);

            if (count > 0) {
                // 2a. If it's not the first sync, then upload each entity's data one by one to cloud & update sync details individually
                Iterable<OnPremSyncDetails> list = onPremSyncDetailsRepository.list(companyId);
                Map<String, OnPremSyncDetails> onPremSyncDetailsMap = new HashMap<>();
                for (OnPremSyncDetails onPremSyncDetails : list) {
                    onPremSyncDetailsMap.put(onPremSyncDetails.getEntity(), onPremSyncDetails);
                }
                //Get all syncable entities
                List<Class<? extends OnPremSyncable>> entities = getSyncableEntities();
                int numParts = entities.size() / Runtime.getRuntime().availableProcessors();

                List<List<Class<? extends OnPremSyncable>>> partitions = Lists.partition(entities,numParts);

                ExecutorService service = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

                List<SyncRunner> futureList = new ArrayList<SyncRunner>();
                int i = 0;
                for (List<Class<? extends OnPremSyncable>> parts : partitions) {
                    SyncRunner myCallable = new SyncRunner((long)i);

                    myCallable.companyId = companyId;
                    myCallable.entities = parts;
                    myCallable.onPremSyncDetailsMap = onPremSyncDetailsMap;

                    futureList.add(myCallable);
                    i++;
                }


                LOGGER.info("Start Syncing w/ Cloud");
                try {
                    List<Future<Long>> futures = service.invokeAll(futureList);
                } catch (Exception err) {
                    err.printStackTrace();
                }
                LOGGER.info("Completed Syncing w/ Cloud");
                service.shutdown();


            }
        }
    }

    private int uploadLatestFromCloud(String companyId, Class entity, OnPremSyncDetails onPremSyncDetails) {
        // 2. Download changes from cloud
        //TODO:
        // Only upload if it's company based
        if (CompanyBaseModel.class.isAssignableFrom(entity)) {
            LOGGER.info("Updating data for " + entity.getName());
            MongoCollection mongoCollection = onPremSyncRepository.getCollection(entity);
            Iterable items = mongoCollection.find("{companyId:#,modified:{$gt:#}}",companyId,onPremSyncDetails.getLastSync()).as(entity);

            List arr = Lists.newArrayList(items);
            OnPremUploadRequest request = new OnPremUploadRequest();
            request.setData(arr);
            if (arr.size() > 0) {
                // there's data to uploaded
                uploadDataToCloud(companyId,entity.getName(),request);
                LOGGER.info("Uploading data for {} entity with total {} records", entity.getName(), arr.size());
            }

        }

        return 0;
    }

    private int downloadLatestFromCloud(String companyId, Class entity, OnPremSyncDetails onPremSyncDetails) {
        // 2. Download changes from cloud
        LOGGER.info("Downloading data for " + entity.getName());
        DownloadResponse downloadResponse = downloadDataFromCloud(companyId, entity.getCanonicalName(), onPremSyncDetails.getLastSync(), null);
        if (downloadResponse != null && downloadResponse.getTotal() > 0) {
            LOGGER.info("Saving downloaded data for {} entity with total {} records", entity.getName(), downloadResponse.getTotal());

            DBCollection mongoCollection = onPremSyncRepository.getMongoDBCollection(entity);

            // bulk writes
            BulkWriteOperation bulkWriteOperation = mongoCollection.initializeOrderedBulkOperation();
            for (Object dataEntry : downloadResponse.getData()) {
                LinkedHashMap map = ((LinkedHashMap) dataEntry);
                JsonSerializer.convertIds(map);
                BasicDBObject o = new BasicDBObject(map);

                String entityIdStr = String.valueOf(map.get("_id"));

                ObjectId entityId = new ObjectId(entityIdStr);
                BulkWriteRequestBuilder bulkWriteRequestBuilder = bulkWriteOperation.find(new BasicDBObject("_id",entityId));

                BulkUpdateRequestBuilder updateReq = bulkWriteRequestBuilder.upsert();
                updateReq.replaceOne(o);

            }
            bulkWriteOperation.execute();
            LOGGER.info("Data was saved for {}", entity.getName());
            return downloadResponse.getTotal();
        }
        return 0;
    }



    //Helper methods
    private boolean isOnPrem() {
        return configuration.getPlatformModeConfiguration() != null
                && configuration.getPlatformModeConfiguration().getMode() == PlatformModeConfiguration.PlatformMode.OnPrem;
    }


    private static List<Class<? extends OnPremSyncable>> getSyncableEntities() {
        Reflections reflections = new Reflections("com.fourtwenty.core.domain.models");
        Set<Class<? extends OnPremSyncable>> types = reflections.getSubTypesOf(OnPremSyncable.class);

        // remove any that does not have a collectionName
        types.removeIf(clazz -> {
            CollectionName collAnnot = clazz.getAnnotation(CollectionName.class);
            if (collAnnot == null) {
                return true;
            }

            return false;
        });

        return types.stream().collect(Collectors.toList());
    }


    enum SyncType {
        INITIAL,
        UPLOAD,
        DOWNLOAD
    }


    class SyncRunner implements Callable<Long> {
        Long id = 0L;
        List<Class<? extends OnPremSyncable>> entities;
        Map<String, OnPremSyncDetails> onPremSyncDetailsMap = new HashMap<>();
        String companyId;
        public SyncRunner(Long val) {
            this.id = val;
        }
        public Long call() {
            // Add your business logic
            //final ObjectMapper mapper = new ObjectMapper();
            //mapper.enable(DeserializationFeature.READ_ENUMS_USING_TO_STRING);
            for (Class entity : entities) {
                try {
                    if (onPremSyncDetailsMap.containsKey(entity.getCanonicalName())) {
                        OnPremSyncDetails onPremSyncDetails = onPremSyncDetailsMap.get(entity.getCanonicalName());

                        if (onPremSyncDetails != null) {
                            // 1. Upload current changes to cloud

                            CollectionName collAnnot = (CollectionName)entity.getAnnotation(CollectionName.class);

                            if (collAnnot.premSyncUp()) {
                                int totalUploaded = uploadLatestFromCloud(companyId, entity, onPremSyncDetails);
                            }

                            if (collAnnot.premSyncDown()) {
                                int downloaded = downloadLatestFromCloud(companyId, entity, onPremSyncDetails);
                            } else {
                                LOGGER.info("Skipping sync down..." + entity.getName());
                            }
                            // OnPremSyncDetails updated
                            long syncTime = DateTime.now().getMillis();
                            onPremSyncDetails.setLastSync(syncTime);
                            //onPremSyncDetails.getSyncRecords().put(syncTime,total);
                            onPremSyncDetailsRepository.update(companyId, onPremSyncDetails.getId(), onPremSyncDetails);

                        }
                    }
                } catch (Exception e) {
                    LOGGER.error("Error downloading.." + entity.getName(), e);
                }
            }

            LOGGER.info("Completed Task: " + id);
            return id;
        }
    }
}
