package com.fourtwenty.core.services.orders;

import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.rest.dispensary.requests.queues.EmployeeReassignRequest;
import com.fourtwenty.core.rest.dispensary.requests.queues.QueueAddMemberRequest;
import com.fourtwenty.core.rest.dispensary.requests.queues.TransactionDeleteRequest;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;

import javax.validation.Valid;
import java.io.InputStream;

public interface CommonPOSService {
    Transaction addToQueue(String companyId, String shopId, String currentEmployeeId, String queueName, QueueAddMemberRequest request);

    void deleteTransaction(String companyId, String shopId, String currentEmployeeId, String transactionId, TransactionDeleteRequest request);


    Transaction prepareCart(String companyId, String shopId, String terminalId, String currentEmployeeId, String transactionId, Transaction transaction);

    Transaction setTransactionToFulfill(String companyId, String shopId, String currentEmployeeId,Transaction transaction, String transactionId);
    Transaction markAsPaid(String companyId, String shopId, String transactionId, Cart.PaymentOption paymentOption);
    Transaction completeTransaction(String companyId, String shopId,
                                    String terminalId, String currentEmployeeId,
                                    String transactionId, Transaction transaction,
                                    boolean avoidCannabis,boolean fromBulk);

    Transaction signTransaction(String companyId, String shopId, String transactionId, String currentEmployeeId, InputStream inputStream,
                                String name,
                                final FormDataBodyPart body);

    Transaction holdTransaction(String companyId, String shopId, String terminalId,String currentEmployeeId, String transactionId, @Valid Transaction transaction);
    Transaction updateDeliveryAddress(String companyId, String shopId, String transactionId, Address request);
    Transaction reassignTransactionEmployee(String companyId, String shopId,String terminalId, String currentEmployeeId, String transactionId, @Valid EmployeeReassignRequest request);

    Transaction unassignTransaction(String companyId, String shopId, String transactionId);

    Transaction startTransaction(String companyId, String shopId,String currentEmployeeId, String transactionId);

    Transaction stopTransaction(String companyId, String shopId, String currentEmployeeId,String transactionId);

    Transaction finalizeTransaction(String companyId, String shopId, String transactionId, Transaction transaction, boolean finalize);

}
