package com.fourtwenty.core.services.orders.impl;

import com.esotericsoftware.kryo.Kryo;
import com.fourtwenty.core.domain.models.company.*;
import com.fourtwenty.core.domain.models.consumer.ConsumerUser;
import com.fourtwenty.core.domain.models.customer.Identification;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.customer.CustomerInfo;
import com.fourtwenty.core.domain.models.customer.Recommendation;
import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.models.generic.Note;
import com.fourtwenty.core.domain.models.generic.UniqueSequence;
import com.fourtwenty.core.domain.models.notification.NotificationInfo;
import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.models.store.ConsumerCart;
import com.fourtwenty.core.domain.models.tookan.EmployeeTookanInfo;
import com.fourtwenty.core.domain.models.tookan.TookanTeamInfo;
import com.fourtwenty.core.domain.models.transaction.*;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.domain.repositories.store.ConsumerCartRepository;
import com.fourtwenty.core.domain.repositories.store.ConsumerUserRepository;
import com.fourtwenty.core.event.BlazeEventBus;
import com.fourtwenty.core.event.transaction.CloverPaymentCardsForTransactionEvent;
import com.fourtwenty.core.event.transaction.MtracPaymentCardsForTransactionEvent;
import com.fourtwenty.core.event.transaction.ProcessPaymentCardsForTransactionEvent;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.managed.ElasticSearchManager;
import com.fourtwenty.core.managed.PartnerWebHookManager;
import com.fourtwenty.core.managed.PushNotificationManager;
import com.fourtwenty.core.rest.dispensary.requests.inventory.BulkInventoryTransferRequest;
import com.fourtwenty.core.rest.dispensary.requests.inventory.InventoryTransferRequest;
import com.fourtwenty.core.rest.dispensary.requests.queues.EmployeeReassignRequest;
import com.fourtwenty.core.rest.dispensary.requests.queues.QueueAddMemberRequest;
import com.fourtwenty.core.rest.dispensary.requests.queues.TransactionDeleteRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.store.webhooks.ConsumerOrderData;
import com.fourtwenty.core.rest.store.webhooks.TransactionData;
import com.fourtwenty.core.services.common.*;
import com.fourtwenty.core.services.global.CannabisLimitService;
import com.fourtwenty.core.services.mgmt.CartService;
import com.fourtwenty.core.services.orders.CommonPOSService;
import com.fourtwenty.core.services.tookan.TookanService;
import com.fourtwenty.core.thirdparty.onfleet.services.OnFleetService;
import com.fourtwenty.core.thirdparty.tookan.model.response.TookanTaskResult;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.core.util.NumberUtils;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.fourtwenty.core.domain.models.transaction.FcmPayload;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bson.types.ObjectId;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.joda.time.DateTime;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.*;

public class CommonPOSServiceImpl implements CommonPOSService {
    private static final String ONFLEET_OBJECT_ERROR = "Error while copying transaction property in OnFleet transaction's result.";
    private static final String TRANSACTION = "Transaction";
    private static final String TRANSACTION_LOCKED = "Transaction is locked, so you can't modify it.";
    private static final String TRANSACTION_NOT_FOUND = "Transaction does not exist.";
    private static final String EMPLOYEE = "Employee";
    private static final String EMPLOYEE_NOT_FOUND = "Employee does not exist.";
    private static final String TRANSACTION_LOCK_PERMISSION = "You don't have permission to perform lock/unlock transaction.";


    private static final Log LOG = LogFactory.getLog(CommonPOSServiceImpl.class);

    @Inject
    private MemberRepository memberRepository;
    @Inject
    private ShopRepository shopRepository;
    @Inject
    private RealtimeService realtimeService;
    @Inject
    private EmployeeRepository employeeRepository;
    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private UniqueSequenceRepository uniqueSequenceRepository;
    @Inject
    private ElasticSearchManager elasticSearchManager;
    @Inject
    private PushNotificationManager pushNotificationManager;
    @Inject
    private TerminalRepository terminalRepository;
    @Inject
    private CartService cartService;
    @Inject
    private TookanService tookanService;
    @Inject
    private OnFleetService onFleetService;
    @Inject
    private MemberGroupRepository memberGroupRepository;
    @Inject
    private ConsumerCartRepository consumerCartRepository;
    @Inject
    private QueuedTransactionRepository queuedTransactionRepository;
    @Inject
    private BackgroundJobService backgroundJobService;
    @Inject
    private ConsumerUserRepository consumerUserRepository;
    @Inject
    private PartnerWebHookManager partnerWebHookManager;
    @Inject
    private CannabisLimitService cannabisLimitService;
    @Inject
    private ProductRepository productRepository;
    @Inject
    private PrepackageRepository prepackageRepository;
    @Inject
    private InventoryRepository inventoryRepository;
    @Inject
    private ProductPrepackageQuantityRepository prepackageQuantityRepository;
    @Inject
    private CashDrawerSessionRepository cashDrawerSessionRepository;
    @Inject
    private BlazeEventBus eventBus;
    @Inject
    private ConsumerNotificationService consumerNotificationService;
    @Inject
    private DeliveryUtilService deliveryUtilService;
    @Inject
    private EmployeeNotificationService employeeNotificationService;
    @Inject
    private ProductCategoryRepository productCategoryRepository;
    @Inject
    private PrepackageProductItemRepository prepackageProductItemRepository;
    @Inject
    private ProductWeightToleranceRepository weightToleranceRepository;
    @Inject
    private BrandRepository brandRepository;

    @Override
    public Transaction addToQueue(final String companyId, final String shopId, String currentEmployeeId, final String queueName, QueueAddMemberRequest request) {
        if (queueName == null) {
            throw new BlazeInvalidArgException("QueueName", "Please specify a queue name.");
        }

        Member member = memberRepository.get(companyId, request.getMemberId());
        if (member == null) {
            throw new BlazeInvalidArgException("Member", "Member does not exist.");
        }

        if (member.isBanPatient() == true) {
            throw new BlazeInvalidArgException("Member", "Banned member cannot be added to queue");
        }

        if (member.getStatus() == Member.MembershipStatus.Inactive) {
            throw new BlazeInvalidArgException("Member", "Cannot add inactive member to queue.");
        }

        Transaction.QueueType queueType = Transaction.QueueType.None;
        try {
            queueType = Transaction.QueueType.valueOf(queueName);
        } catch (Exception e) {
            throw new BlazeInvalidArgException("Queue name", "Invalid queue name. Must be of: "
                    + Transaction.QueueType.Online.name() + ", " + Transaction.QueueType.Delivery.name() + ", " + Transaction.QueueType.WalkIn.name() + ", " + Transaction.QueueType.Special.name());
        }
        Shop shop = shopRepository.get(companyId, shopId);

        if (shop == null) {
            throw new BlazeInvalidArgException("Shop", "Shop does not exist.");
        }

        // Check if employee is valid
        Employee employee = null;
        if (StringUtils.isNotBlank(request.getEmployeeId())) {
            employee = employeeRepository.get(companyId, request.getEmployeeId());
            if (employee == null || employee.isDeleted()) {
                throw new BlazeInvalidArgException("Employee", "Employee does not exist.");
            }

            if (employee.isDisabled()) {
                throw new BlazeInvalidArgException("Employee", "Employee is inactive.");
            }

            if (request.isCreateTookanTask() && shop.isEnableTookan()) {
                this.createTookanTask(null, employee, member, request.getTookanTeamId(), false, false, queueType);
            }

        }


        //check member age limit
        int memberAge = 0;
        if (member.getDob() != null) {
            memberAge = DateUtil.getYearsBetweenTwoDates(member.getDob(), DateTime.now().getMillis());
        }

        if (shop.isEnableMedicinalAge() && (ConsumerType.MedicinalThirdParty.equals(member.getConsumerType()) || ConsumerType.MedicinalState.equals(member.getConsumerType()))) {
            int medicalAge = shop.getMedicinalAge();
            if (medicalAge > memberAge) {
                throw new BlazeInvalidArgException("Member", String.format("Member does not meet minimum age of %d.", shop.getMedicinalAge()));
            }
        }

        if (shop.isEnableAgeLimit() && ConsumerType.AdultUse.equals(member.getConsumerType())) {
            int adultAge = shop.getAgeLimit();
            if (adultAge > memberAge) {
                throw new BlazeInvalidArgException("Member", String.format("Member does not meet minimum age of %d.", shop.getAgeLimit()));
            }
        }


        if (shop.isRequireValidRecDate() && member.getConsumerType() != ConsumerType.AdultUse) {
            //TODO: FOR THE TIME BEING, THIS IS ENABLED FOR ALL. ENABLE THIS ONLY FOR MEDICINAL
            // check member rec
            if (!member.hasValidRecommendation()) {
                throw new BlazeInvalidArgException("Recommendation", "Member/patient is missing a recommendation or it has been expired.");
            }

        }

        long activeTransCount = transactionRepository.getActiveTransactionCount(shopId, member.getId());
        if (shop.getNumAllowActiveTrans() > activeTransCount) {

            //Create Sequence
            UniqueSequence sequence = uniqueSequenceRepository.getNewIdentifier(companyId, shopId, "TransactionPriority");

            //Start Transaction
            Transaction trans = new Transaction();
            trans.prepare(companyId);
            trans.setActive(true);
            trans.setTransNo("" + sequence.getCount());
            trans.setMemberId(request.getMemberId());
            trans.setShopId(shopId);
            trans.setQueueType(queueType);
            trans.setMemberId(member.getId());
            trans.setCheckinTime(DateTime.now().getMillis());
            trans.setSellerId(currentEmployeeId);
            trans.setCreatedById(currentEmployeeId);
            trans.setPriority(sequence.getCount());

            //Create Cart
            Cart cart = new Cart();
            cart.prepare(companyId);

            trans.setCart(cart);
            trans.setTransType(Transaction.TransactionType.Sale);
            trans.setTimeZone(shop.getTimeZone());
            trans.setAssignedEmployeeId(request.getEmployeeId());
            trans.setAssigned((StringUtils.isNotBlank(request.getEmployeeId())));
            trans.setFulfillmentStep(Transaction.FulfillmentStep.Prepare);
            trans.setMemo(request.getMemo());

            Set<String> orderTags = new HashSet<>();
            if (trans.getQueueType() == Transaction.QueueType.Delivery) {
                orderTags.add(trans.getQueueType().name());
            }

            if (member != null) {
                if (member.getAddress() != null) {
                    Address address = member.getAddress();
                    address.resetPrepare(companyId);
                    trans.setDeliveryAddress(address);
                }
                // check verified
                if (member.getIdentifications() != null) {
                    for (Identification identification : member.getIdentifications()) {
                        if (identification != null) {
                            if (identification.isVerified()) {
                                orderTags.add("ID Verified");
                                break;
                            }
                        }
                    }
                }

                // check verified
                if (member.getRecommendations() != null) {
                    for (Recommendation recommendation : member.getRecommendations()) {
                        if (recommendation != null) {
                            if (recommendation.isVerified()) {
                                orderTags.add("REC Verified");
                                break;
                            }
                        }
                    }
                }
            }
            trans.setOrderTags(orderTags);

            //Get Seller Id
            String sellerTerminalId = getSellerTerminalId(shop, currentEmployeeId,trans.getAssignedEmployeeId(), null);
            Terminal sellerTerminal = terminalRepository.getTerminalById(companyId, sellerTerminalId);

            trans.setCheckoutType((sellerTerminal != null && sellerTerminal.getCheckoutType() != null) ? sellerTerminal.getCheckoutType() : shop.getCheckoutType());

            trans.setTerminalId(sellerTerminalId);

            //Member Info
            final CustomerInfo customerInfo = getMemberGroup(companyId, trans.getMemberId());
            cartService.prepareCart(shop, trans, false, Transaction.TransactionStatus.Hold, false, customerInfo, true);


            if (trans.getQueueType() == Transaction.QueueType.Delivery) {
                if (shop != null) {
                    boolean enabledDelivery = false;
                    for (DeliveryFee deliveryFee : shop.getDeliveryFees()) {
                        if (deliveryFee.isEnabled()) {
                            enabledDelivery = true;
                            break;
                        }
                    }
                    cart.setEnableDeliveryFee(enabledDelivery);
                    // Always start with 0
                    cart.setDeliveryFee(new BigDecimal(0));
                }
            }
            trans.setPreparedBy(currentEmployeeId);
            trans.setCreateTookanTask(request.isCreateTookanTask());

            final Transaction dbTransaction = transactionRepository.save(trans);

            if (request.getCreateOnFleetTask() && shop.isEnableOnFleet()) {
                this.createTaskAtOnFleet(shop, employee, trans, request.getOnFleetTeamId(), member);
            }
            if (request.isCreateTookanTask() && shop.isEnableTookan()) {
                this.createTookanTask(trans, employee, member, request.getTookanTeamId(), false, true, queueType);
            }

            // ElasticSearch prereqs and upload
            dbTransaction.setMember(memberRepository.get(companyId, dbTransaction.getMemberId()));
            dbTransaction.setSeller(employeeRepository.get(companyId, dbTransaction.getSellerId()));
            elasticSearchManager.createOrUpdateIndexedDocument(dbTransaction);

            if (dbTransaction != null && employee != null) {
                if (employee.getAssignedTerminalId() != null) {
                    Terminal terminal = terminalRepository.get(employee.getCompanyId(), employee.getAssignedTerminalId());
                    if (terminal != null) {
                        String message = "You have been assigned to a member in " + queueType + "." + " Order Number : #" + dbTransaction.getTransNo();
                        String title = "New Order";
                            pushNotificationManager.sendPushNotification(message, Lists.newArrayList(terminal), 0, FcmPayload.Type.TASK, dbTransaction.getId(), dbTransaction.getTransNo(), title, FcmPayload.SubType.NEW_ORDER);
                    }
                }
            }

            memberRepository.updateModified(member.getId());
            realtimeService.sendRealTimeEvent(shopId, RealtimeService.RealtimeEventType.MembersUpdateEvent, null);
            realtimeService.sendRealTimeEvent(shopId, RealtimeService.RealtimeEventType.QueueUpdateEvent, null);
            return dbTransaction;
        } else {
            if (shop.getNumAllowActiveTrans() > 1) {
                throw new BlazeInvalidArgException("Transaction", String.format("Member has %s active orders in queue. Please complete current order(s) before accepting new one.", shop.getNumAllowActiveTrans()));
            } else {
                throw new BlazeInvalidArgException("Transaction", "Member is already in the queue.");
            }
        }
    }


    @Override
    public void deleteTransaction(String companyId,
                                  String shopId,
                                  String currentEmployeeId,
                                  String transactionId,
                                  TransactionDeleteRequest deleteRequest) {
        if (StringUtils.isBlank(transactionId)) {
            throw new BlazeInvalidArgException("TransactionId", "Invalid transactionId");
        }
        Transaction dbTransaction = transactionRepository.get(companyId, transactionId);
        if (dbTransaction == null) {
            throw new BlazeInvalidArgException("TransactionId", "Invalid transactionId");
        }
        if (dbTransaction.isActive()) {

            if (deleteRequest != null) {
                Note note = new Note();
                note.prepare();
                note.setMessage(deleteRequest.getReason());
                note.setWriterId(currentEmployeeId);

                Employee employee = employeeRepository.get(companyId, currentEmployeeId);
                if (employee != null) {
                    note.setWriterName(employee.getFirstName() + " " + employee.getLastName());
                } else {
                    note.setWriterName("Unknown");
                }
                dbTransaction.setDeleteNote(note);

            }

            if (dbTransaction.isPreparingFulfillment()) {
                dbTransaction.setActive(false);
                dbTransaction.setStatus(Transaction.TransactionStatus.Canceled);
                dbTransaction.setProcessedTime(DateTime.now().getMillis());
                dbTransaction.setCompletedTime(DateTime.now().getMillis());
                dbTransaction.setSellerId(currentEmployeeId);
                // clear and then update
                transactionRepository.update(companyId, transactionId, dbTransaction);
                realtimeService.sendRealTimeEvent(shopId, RealtimeService.RealtimeEventType.QueueUpdateEvent, null);
            } else {
                // add some simple checks
                // only check if it's not empty
                if (dbTransaction.getCart() != null && !dbTransaction.getCart().getItems().isEmpty()) {
                    if (dbTransaction.getQueueType() == Transaction.QueueType.WalkIn) {
                        if (dbTransaction.getStatus() == Transaction.TransactionStatus.InProgress) {
                            LOG.info("Transaction is in progress. Please put on hold before canceling.");
                            throw new BlazeInvalidArgException("Transaction", "Transaction is in progress. Please put on hold before canceling.");
                        } else if (dbTransaction.getStatus() == Transaction.TransactionStatus.Completed) {
                            dbTransaction.setActive(false);
                            LOG.info("Transaction is already completed.");
                            transactionRepository.deactivate(companyId, dbTransaction.getId());
                            return;
                        }
                    }
                }

                dbTransaction.setActive(false);
                dbTransaction.setProcessedTime(DateTime.now().getMillis());
                dbTransaction.setCompletedTime(DateTime.now().getMillis());
                // update first
                transactionRepository.update(companyId, transactionId, dbTransaction);
                // clear and then send to queue transaction to deduct amt
                //dbTransaction.getCart().getItems().clear();

                Shop shop = shopRepository.get(companyId, shopId);
                //queueTransactionJob(companyId,shopId,currentEmployeeId,transactionId, dbTransaction, Transaction.TransactionStatus.Canceled, shop);
                queueTransactionJob(companyId, shopId, currentEmployeeId, dbTransaction.getTerminalId(), transactionId, dbTransaction, Transaction.TransactionStatus.Canceled, shop);
            }

            if (StringUtils.isNotBlank(dbTransaction.getConsumerCartId())) {
                ConsumerCart consumerCart = consumerCartRepository.get(companyId, dbTransaction.getConsumerCartId());
                consumerCart.setCartStatus(ConsumerCart.ConsumerCartStatus.CanceledByDispensary);
                consumerCart.setTrackingStatus(ConsumerCart.ConsumerTrackingStatus.Declined);
                consumerCartRepository.update(companyId, consumerCart.getId(), consumerCart);
                updateConsumerOrderWebHook(companyId, shopId, consumerCart);
            }

            if (dbTransaction.isCreateTookanTask() && StringUtils.isNotBlank(dbTransaction.getTookanTaskId())) {
                tookanService.updateTaskStatus(companyId, shopId, dbTransaction, TookanTaskResult.TookanTaskStatus.CANCEL);
            }
        }
    }

    @Override
    public Transaction setTransactionToFulfill(String companyId, String shopId, String currentEmployeeId,Transaction transaction, String transactionId) {
        final Transaction dbTransaction = transactionRepository.get(companyId, transactionId);
        if (dbTransaction == null) {
            throw new BlazeInvalidArgException("Transaction", "Transaction does not exist.");
        }
        // loop through and set the qty to the prepareQty
        if (dbTransaction.isPreparingFulfillment()) {
            populatePrepareQty(transaction);
        }
        dbTransaction.setStatus(Transaction.TransactionStatus.Hold);
        dbTransaction.setFulfillmentStep(Transaction.FulfillmentStep.Fulfill);
        dbTransaction.setPreparedBy(currentEmployeeId);
        dbTransaction.setPreparedDate(DateTime.now().getMillis());

        Shop shop = shopRepository.get(companyId, shopId);

        final CustomerInfo customerInfo = getMemberGroup(dbTransaction.getCompanyId(), dbTransaction.getMemberId());
        cartService.prepareCart(shop, transaction, false, dbTransaction.getStatus(), false, customerInfo, false);

        // set the incoming cart
        dbTransaction.setCart(transaction.getCart());

        transactionRepository.update(transaction.getId(), dbTransaction);
        realtimeService.sendRealTimeEvent(shopId, RealtimeService.RealtimeEventType.QueueUpdateEvent, null);
        return dbTransaction;
    }

    @Override
    public Transaction markAsPaid(String companyId,String shopId,String transactionId, Cart.PaymentOption paymentOption) {
        Transaction dbTrans = transactionRepository.get(companyId,transactionId);
        LOG.info("Marking as paid: " + transactionId);
        if (dbTrans == null) {
            LOG.info("Error marking as paid: " + transactionId);
            throw new BlazeInvalidArgException(TRANSACTION, "Transaction does not exist.");
        }

        dbTrans.setPaid(true);
        dbTrans.setProcessedTime(DateTime.now().getMillis());
        if (paymentOption == null) {
            paymentOption = Cart.PaymentOption.Credit; // mark as paid defaults to Credit since we're marking things as paid
        }

        dbTrans.getCart().setPaymentOption(paymentOption);

        transactionRepository.markTransactionAsPaid(companyId,transactionId,dbTrans.getProcessedTime(), paymentOption);
        //realtimeService.sendRealTimeEvent(shopId, RealtimeService.RealtimeEventType.QueueUpdateEvent, null);
        LOG.info("Successful marking as paid: " + transactionId + " paymentOption: " + paymentOption);

        Transaction trans = new Kryo().copy(dbTrans);
        return trans;
    }

    @Override
    public Transaction updateDeliveryAddress(String companyId, String shopId, String transactionId, Address request) {
        Transaction dbTransaction = transactionRepository.getByShopAndId(companyId, shopId, transactionId);
        if (dbTransaction == null) {
            throw new BlazeInvalidArgException(TRANSACTION, TRANSACTION_NOT_FOUND);
        }

        if (request == null) {
            throw new BlazeInvalidArgException(TRANSACTION, "Request cannot be blank.");
        }
        if (dbTransaction.isActive() == false) {
            throw new BlazeInvalidArgException(TRANSACTION, "Transaction is no longer active.");
        }
        request.prepare(companyId);

        transactionRepository.updateDeliveryAddress(dbTransaction.getId(), request);
        dbTransaction.setDeliveryAddress(request);
        realtimeService.sendRealTimeEvent(shopId, RealtimeService.RealtimeEventType.QueueUpdateEvent, null);
        return dbTransaction;


    }

    @Override
    public Transaction completeTransaction(String companyId, String shopId,
                                           String terminalId, String currentEmployeeId,
                                           String transactionId, Transaction transaction,
                                           boolean avoidCannabis, boolean fromBulk) {
        Transaction dbTrans = transactionRepository.get(companyId, transactionId);
        if (dbTrans == null) {
            throw new BlazeInvalidArgException("TransactionId", "Transaction does not exist.");
        }
        Member member = null;
        if (StringUtils.isNotBlank(dbTrans.getMemberId())) {
            member = memberRepository.get(companyId, dbTrans.getMemberId());
            if (member == null) {
                throw new BlazeInvalidArgException("Member", "Member does not found");
            }

            if (member.isBanPatient()) {
                throw new BlazeInvalidArgException("Transaction", "Transaction can not be completed because member is banned");
            }
        }


        if (dbTrans.isLocked() && this.checkLockedOrder(dbTrans, transaction)) {
            throw new BlazeInvalidArgException(TRANSACTION, TRANSACTION_LOCKED);
        }

        if (transaction.getCart() != null && transaction.getCart().getPaymentOption().equals(Cart.PaymentOption.Split)) {
            transaction.getCart().setPaymentType(Cart.PaymentType.Split);

            if (transaction.getCart().getSplitPayment() == null) {
                throw new BlazeInvalidArgException("Transaction", "Split payment was not received. Please specify split payments.");
            }
        }

        if (transaction.getCart() != null && transaction.getCart().getPaymentOption() == Cart.PaymentOption.None) {
            throw new BlazeInvalidArgException("Transaction", "Please choose a valid payment option.");
        }




        if (dbTrans.isActive() == false) {
            // if it was canceled && there's a note, then just returned the canceled version
            // otherwise, let this continue to process
            if (!fromBulk) {
                // if incoming is from bulk, let's process it
                // otherwise, check if canceled
                if (dbTrans.getStatus() == Transaction.TransactionStatus.Canceled) {
                    if (dbTrans.getDeleteNote() != null && StringUtils.isNotBlank(dbTrans.getDeleteNote().getMessage())) {
                        // there was a reason it was canceled, so let's returned it
                        return dbTrans;
                    }
                } else {
                    return dbTrans;
                }
            }
        }

        if (dbTrans.getStatus() == Transaction.TransactionStatus.Completed) {
            return dbTrans;
        }
        if (transaction.getCart().getItems() == null || transaction.getCart().getItems().isEmpty()) {
            throw new BlazeInvalidArgException("Transaction", "Your cart is empty. Please try again.");
        }

        Shop shop = shopRepository.get(companyId, shopId);

        if (shop.isEnableCannabisLimit() && !avoidCannabis) {
            boolean status = cannabisLimitService.checkCannabisLimit(companyId, shop, dbTrans.getMemberId(), transaction.getCart(), null, false);
            if (!status) {
                throw new BlazeInvalidArgException("CannabisLimit", "State Cannabis Limit is reached.");
            }
        }

        String sellerTerminalId = getSellerTerminalId(shop, currentEmployeeId, dbTrans.getAssignedEmployeeId(), terminalId);

        Terminal sellerTerminal = terminalRepository.getTerminalById(companyId, sellerTerminalId);
        if (sellerTerminal == null || sellerTerminal.isDeleted() || sellerTerminal.isActive() == false) {

            throw new BlazeInvalidArgException("Terminal",
                    String.format("Terminal '%s' is no longer active.",
                            sellerTerminal == null ? "" : sellerTerminal.getName()));
        }


        dbTrans.setCheckoutType((sellerTerminal != null && sellerTerminal.getCheckoutType() != null) ? sellerTerminal.getCheckoutType() : shop.getCheckoutType());



        if (dbTrans.isPreparingFulfillment()) {
            throw new BlazeInvalidArgException("Transaction", "This transaction is currently in preparing mode.");
        }

        // assume its cache if no payment option is set
        if (transaction.getCart() != null && transaction.getCart().getPaymentOption() == null) {
            transaction.getCart().setPaymentOption(Cart.PaymentOption.Cash);
        }

        // Check Available
        checkAvailableInventory(transaction, dbTrans, "complete", null, sellerTerminalId, Boolean.FALSE);

        if (shop != null) {
            // make sure the current terminal has a cash drawer
            if (shop.isEnforceCashDrawers()) {
                long nowMillis = DateTime.now().getMillis();
                String todayDate = DateUtil.toDateFormatted(nowMillis, shop.getTimeZone());
                String todayFormatted = DateUtil.toDateFormatted(nowMillis, shop.getTimeZone(), "yyyyMMdd");

                CashDrawerSession dbLogResult = cashDrawerSessionRepository.getCashDrawerForDate(companyId,
                        shop.getId(), sellerTerminalId, todayFormatted);

                if (dbLogResult == null) {
                    throw new BlazeInvalidArgException("Terminal",
                            String.format("Please create a cash drawer for today's date, %s, for terminal '%s' before completing your sale.",
                                    todayDate,
                                    sellerTerminal.getName()));
                } else if (dbLogResult != null && dbLogResult.getStatus() == CashDrawerSession.CashDrawerLogStatus.Closed) {

                    throw new BlazeInvalidArgException("Terminal",
                            String.format("Please open a cash drawer for today's date, %s, for terminal '%s' before completing your sale.",
                                    todayDate,
                                    sellerTerminal.getName()));
                }
            }


        }

        dbTrans.setSellerId(currentEmployeeId);
        dbTrans.setSellerTerminalId(sellerTerminalId);
        if (dbTrans.getStartTime() == null || dbTrans.getStartTime() == 0) {
            if (transaction.getStartTime() == null || transaction.getStartTime() <= 0) {
                dbTrans.setStartTime(dbTrans.getCreated());
            } else {
                dbTrans.setStartTime(transaction.getStartTime());
            }
        }
        dbTrans.setEndTime(DateTime.now().getMillis());
        dbTrans.setStatus(Transaction.TransactionStatus.Completed);
        dbTrans.setLoc(transaction.getLoc());
        dbTrans.setActive(false);
        dbTrans.setTimeZone(shop.getTimeZone());
        dbTrans.setRouting(false);

        if ((dbTrans.getDeliveryAddress() == null || !dbTrans.getDeliveryAddress().isValid())
                && member != null && member.getAddress() != null) {
            Address address = member.getAddress();
            address.resetPrepare(companyId);
            dbTrans.setDeliveryAddress(address);
        }

        if (StringUtils.isBlank(dbTrans.getAssignedEmployeeId())) {
            dbTrans.setAssignedEmployeeId(currentEmployeeId);
        }
        dbTrans.setAssigned(true);

        if (dbTrans.getStartRouteDate() == null || dbTrans.getStartRouteDate() == 0) {
            dbTrans.setStartRouteDate(dbTrans.getCreated());
        }
        if (dbTrans.getEndRouteDate() == null || dbTrans.getEndRouteDate() == 0) {
            dbTrans.setEndRouteDate(DateTime.now().getMillis());
        }

        if (transaction.getMemberSignature() != null) {
            dbTrans.setMemberSignature(transaction.getMemberSignature());
        }

        if (dbTrans.isPaid() == false) {
            // If it's already paid, th
            if (transaction.getProcessedTime() != null && transaction.getProcessedTime() != 0) {
                dbTrans.setProcessedTime(transaction.getProcessedTime());
            } else {
                dbTrans.setProcessedTime(DateTime.now().getMillis());
            }
            dbTrans.setCompletedTime(dbTrans.getProcessedTime());
        } else {
            // set completed time if it's paid, leave processedTime alone
            if (transaction.getProcessedTime() != null && transaction.getProcessedTime() != 0) {
                dbTrans.setCompletedTime(transaction.getProcessedTime());
            } else {
                dbTrans.setCompletedTime(DateTime.now().getMillis());
            }
        }

        dbTrans.setPaid(true);

        // create new note
        if (transaction.getNote() != null && StringUtils.isNotEmpty(transaction.getNote().getMessage())) {
            Note note = new Note();
            note.setId(ObjectId.get().toString());
            note.setMessage(transaction.getNote().getMessage());
            dbTrans.setNote(note);
        }


        final CustomerInfo customerInfo = getMemberGroup(dbTrans.getCompanyId(), dbTrans.getMemberId());
        try {
            cartService.prepareCart(shop, dbTrans, false, Transaction.TransactionStatus.InProgress, false, customerInfo, false); // current transaction is on hold
        } catch (Exception ex) {
            LOG.warn("Error in dbTransaction no " + dbTrans.getTransNo() + " : " + ex.getMessage());
        }
        cartService.prepareCart(shop, transaction, false, Transaction.TransactionStatus.Completed, false, customerInfo, false); // new transaction is completed


        String consumerUserId = null;
        if (StringUtils.isNotBlank(transaction.getConsumerCartId())) {
            ConsumerCart consumerCart = consumerCartRepository.get(companyId, transaction.getConsumerCartId());
            if (transaction.getCart() != null) {
                consumerCart.setCart(transaction.getCart());
            }
            consumerCart.setCartStatus(ConsumerCart.ConsumerCartStatus.Completed);
            consumerCart.setCompleted(true);
            consumerCart.setCompletedTime(DateTime.now().getMillis());
            consumerCart.setTrackingStatus(ConsumerCart.ConsumerTrackingStatus.Delivered);
            consumerCartRepository.update(companyId, consumerCart.getId(), consumerCart);
            realtimeService.sendRealTimeEvent(shopId,
                    RealtimeService.RealtimeEventType.ConsumerCartsUpdate, null);

            ConsumerUser consumerUser = consumerUserRepository.getById(consumerCart.getConsumerId());
            //if (ConsumerCart.TransactionSource.WooCommerce != consumerCart.getTransactionSource()) {
                consumerNotificationService.sendUpdateOrderdNotification(consumerCart, consumerUser, shop, NotificationInfo.NotificationType.Consumer_Update_Order);
            //}
            if (consumerUser != null) {
                consumerUserId = consumerCart.getConsumerId();
            }
            updateConsumerOrderWebHook(companyId, shopId, consumerCart);
        }

        if (dbTrans.getQueueType() == Transaction.QueueType.Delivery) {
            double mileage = deliveryUtilService.calculateMileageByTransaction(companyId,shopId,transaction);
            dbTrans.setMileage(new BigDecimal(mileage));
        }
        dbTrans.setMileageCalculated(true);


        // Process Loyalty Card Payments (ie Linx, etc) [Need support for split payment?]
        if (transaction.isPayingWithPaymentCard()) {
            processPaymentOption(dbTrans, transaction);
        }


        transactionRepository.update(companyId, dbTrans.getId(), dbTrans);


        dbTrans.setMember(memberRepository.get(companyId, dbTrans.getMemberId()));
        if (transaction.getCart() != null) {
            dbTrans.setCart(transaction.getCart());
        }

        // if we're completing, make sure the final transaction has everything fulfilled
        for (OrderItem item : transaction.getCart().getItems()) {
            item.setFulfilled(true);
        }

        //queueTransactionJob(transactionId, transaction, Transaction.TransactionStatus.Completed, shop);
        queueTransactionJob(companyId, shopId, currentEmployeeId, terminalId, transactionId, transaction, Transaction.TransactionStatus.Completed, shop);


        final TransactionData transactionData = new TransactionData();
        transactionData.setId(transaction.getId());
        transactionData.setShopId(transaction.getShopId());
        transactionData.setTerminalId(transaction.getSellerTerminalId());
        transactionData.setCreated(transaction.getCreated());
        transactionData.setTransactionNo(transaction.getTransNo());
        if (member != null) {
            StringBuilder sb = new StringBuilder();
            transactionData.setMemberName(sb.append(member.getFirstName()).append(" ").append(member.getLastName()).toString());
            transactionData.setMemberId(member.getId());
            transactionData.setLoyaltyPoints(member.getLoyaltyPoints());
        }

        if (transaction.getConsumerCartId() != null && ObjectId.isValid(transaction.getConsumerCartId())) {
            transactionData.setConsumerCartId(transaction.getConsumerCartId());
            transactionData.setConsumerUserId(consumerUserId);
            ConsumerUser consumerUser = consumerUserRepository.get(companyId, consumerUserId);
            if (consumerUser != null) {
                transactionData.setMembershipAccepted(consumerUser.isAccepted());
            }
        }

        if (transaction.getProcessedTime() == null) {
            transactionData.setProcessedTime(dbTrans.getProcessedTime());
        } else {
            transactionData.setProcessedTime(transaction.getProcessedTime());
        }

        transactionData.setTransactionType(transaction.getTransType());
        transactionData.setQueueType(transaction.getQueueType());
        transactionData.setFulfillmentStep(transaction.getFulfillmentStep());
        if (StringUtils.isNotBlank(transaction.getSellerId())) {
            Employee employee = employeeRepository.get(companyId, transaction.getSellerId());
            if (employee != null) {
                StringBuilder sb = new StringBuilder();
                transactionData.setSellerName(sb.append(employee.getFirstName()).append(" ").append(employee.getLastName()).toString());
            }
        }
        transactionData.setPaymentOption(transaction.getCart().getPaymentOption());
        transactionData.setPaymentType(transaction.getCart().getPaymentType());
        transactionData.setDiscount(transaction.getCart().getDiscount() != null ? transaction.getCart().getDiscount() : BigDecimal.ZERO);
        transactionData.setTotalTax(transaction.getCart().getTotalCalcTax() != null ? transaction.getCart().getTotalCalcTax() : BigDecimal.ZERO);
        transactionData.setTotal(transaction.getCart().getTotal() != null ? transaction.getCart().getTotal() : BigDecimal.ZERO);
        transactionData.setPromoCode(transaction.getCart().getPromoCode() != null ? transaction.getCart().getPromoCode() : "");

        partnerWebHookManager.completeTransactionWebHook(companyId, shopId, transactionData);
        /*Company company = companyRepository.getById(companyId);
        SpringBigInfo springBigInfo = springbigRepository.getInfoByShop(companyId, shopId);
        if(company != null && company.isEnableSpringBig() && springBigInfo != null && springBigInfo.isActive()) {
            springBigManager.createVisit(transaction, member);
        }*/

        if (dbTrans.isCreateTookanTask() && StringUtils.isNotBlank(dbTrans.getTookanTaskId())) {
            tookanService.updateTaskStatus(companyId, shopId, dbTrans, TookanTaskResult.TookanTaskStatus.SUCCESSFUL);
        }
        if (fromBulk) {
            transactionRepository.update(companyId, dbTrans.getId(), dbTrans);
        }
        return dbTrans;
    }

    @Override
    public Transaction prepareCart(String companyId, String shopId, String terminalId, String currentEmployeeId, String transactionId, Transaction transaction) {
        Transaction dbTrans = transactionRepository.get(companyId, transactionId);
        if (dbTrans == null) {
            throw new BlazeInvalidArgException("TransactionId", "Transaction does not exist.");
        }

        if (transaction.getQueueType().equals("Delivery")) {

        }

        Shop shop = shopRepository.get(companyId, shopId);
        if (shop.isEnableCannabisLimit()) {
            boolean status = cannabisLimitService.checkCannabisLimit(companyId, shop, dbTrans.getMemberId(), transaction.getCart(), null, false);
            if (!status) {
                throw new BlazeInvalidArgException("CannabisLimit", "State Cannabis Limit is reached.");
            }
        }

        final CustomerInfo customerInfo = getMemberGroup(companyId, dbTrans.getMemberId());
        cartService.prepareCart(shop, transaction, false, Transaction.TransactionStatus.InProgress, true, customerInfo, false);
        queuePrepareTransactionJob(terminalId, dbTrans.getId(), transaction,
                Transaction.TransactionStatus.InProgress, shop,
                currentEmployeeId, false, null);
        return transaction;
    }


    @Override
    public Transaction holdTransaction(String companyId, String shopId, String terminalId, String currentEmployeeId, String transactionId, Transaction incomingTrans) {
        Transaction dbTrans = transactionRepository.get(companyId, transactionId);
        LOG.info("Hold transaction: " + transactionId);
        if (dbTrans == null) {
            throw new BlazeInvalidArgException("TransactionId", "Transaction does not exist: " + transactionId);
        }
        if (!dbTrans.isActive()) {
            return dbTrans;
        }
        if (dbTrans.getStatus() == Transaction.TransactionStatus.Completed) {
            dbTrans.setActive(false);
            transactionRepository.deactivate(companyId, dbTrans.getId());
            return dbTrans;
        }

        if (dbTrans.isFulfillingFulfillment()) {
            if (incomingTrans.getCart().getItems().size() == 0
                    && dbTrans.getCart().getItems().size() > 0) {
                return dbTrans;
            }
        }

        if (dbTrans.isLocked() && this.checkLockedOrder(dbTrans, incomingTrans)) {
            throw new BlazeInvalidArgException(TRANSACTION, TRANSACTION_LOCKED);
        }

        Shop shop = shopRepository.get(companyId, shopId);

        String sellerTerminalId = getSellerTerminalId(shop, currentEmployeeId, dbTrans.getAssignedEmployeeId(), terminalId);

        if (!incomingTrans.isPreparingFulfillment()) {
            checkAvailableInventory(incomingTrans, dbTrans, "hold", null, sellerTerminalId, Boolean.FALSE);
        }
        // create new note
        if (incomingTrans.getNote() != null && StringUtils.isNotEmpty(incomingTrans.getNote().getMessage())) {
            Note note = new Note();
            note.setId(ObjectId.get().toString());
            note.setMessage(incomingTrans.getNote().getMessage());
            dbTrans.setNote(note);
        }


        Transaction.TransactionStatus newStatus = Transaction.TransactionStatus.Hold;
        if (incomingTrans.getCart().getItems().size() == 0) {
            newStatus = Transaction.TransactionStatus.Queued; // Go back to the queue
            dbTrans.setStartTime(0L);
        }
        dbTrans.setSellerTerminalId(sellerTerminalId);
        dbTrans.setSellerId(currentEmployeeId);
        dbTrans.setStatus(newStatus);
        dbTrans.setTimeZone(shop.getTimeZone());

        dbTrans.setPickUpDate(incomingTrans.getPickUpDate());
        dbTrans.setDeliveryDate(incomingTrans.getDeliveryDate());

        dbTrans.setPickUpDate(incomingTrans.getPickUpDate());
        dbTrans.setDeliveryDate(incomingTrans.getDeliveryDate());
        dbTrans.setCompleteAfter(incomingTrans.getCompleteAfter());

        if (dbTrans.isPaid() == false && incomingTrans.isPaid()) {
            dbTrans.setPaid(incomingTrans.isPaid());
            dbTrans.setProcessedTime(DateTime.now().getMillis());
        }

        dbTrans.setMemo(incomingTrans.getMemo());
        if (incomingTrans.getCart() != null && incomingTrans.getCart().getPaymentOption() == null) {
            incomingTrans.getCart().setPaymentOption(Cart.PaymentOption.None);
        }

        final CustomerInfo customerInfo = getMemberGroup(dbTrans.getCompanyId(), dbTrans.getMemberId());
        //cartService.prepareCart(shop, dbTrans, false, newStatus, false, customerInfo, false);
        cartService.prepareCart(shop, incomingTrans, false, newStatus, false, customerInfo, false);

        // If it's preparing, just save it (we're not taking it from the inventory)
        if (dbTrans.isPreparingFulfillment()) {
            // we're reparing to just save it
            dbTrans.setCart(incomingTrans.getCart());
            dbTrans.setStatus(newStatus);
            populatePrepareQty(dbTrans);
            transactionRepository.update(companyId, dbTrans.getId(), dbTrans);
        } else {
            transactionRepository.update(companyId, dbTrans.getId(), dbTrans);
            //queueTransactionJob(transactionId, incomingTrans, newStatus, shop);
            queueTransactionJob(companyId, shopId, currentEmployeeId, terminalId, transactionId, incomingTrans, newStatus, shop);
        }

        dbTrans.setCart(incomingTrans.getCart());
        dbTrans.setMember(memberRepository.get(companyId, dbTrans.getMemberId()));
        if (dbTrans.isCreateTookanTask() && StringUtils.isNotBlank(dbTrans.getTookanTaskId())) {
            tookanService.updateTaskStatus(companyId, shopId, dbTrans, null);
        }

        if (StringUtils.isNotBlank(dbTrans.getAssignedEmployeeId())) {
            Employee employee = employeeRepository.getById(dbTrans.getAssignedEmployeeId());
            dbTrans.setAssignedEmployee(employee);
        }
        LOG.info("Hold successful: " + transactionId);
        return dbTrans;
    }

    @Override
    public Transaction reassignTransactionEmployee(String companyId, String shopId, String terminalId, String currentEmployeeId, String transactionId, EmployeeReassignRequest request) {
        Shop shop = shopRepository.get(companyId, shopId);
        if (shop == null) {
            throw new BlazeInvalidArgException("Shop", "Shop does not exist");
        }

        Transaction dbTrans = transactionRepository.get(companyId, transactionId);
        if (dbTrans == null) {
            throw new BlazeInvalidArgException("TransactionId", "Transaction does not exist.");
        }

        Employee employee = null;
        // Check if employee is valid
        Transaction.TransactionStatus newStatus = Transaction.TransactionStatus.Hold;
        if (StringUtils.isNotEmpty(request.getEmployeeId())) {
            employee = employeeRepository.get(companyId, request.getEmployeeId());
            if (employee == null || employee.isDeleted()) {
                throw new BlazeInvalidArgException("Employee", "Employee does not exist.");
            }

            if (employee.isDisabled()) {
                throw new BlazeInvalidArgException("Employee", "Employee does is inactive.");
            }

            if (request.isCreateTookanTask() && shop.isEnableTookan()) {
                Member member = memberRepository.get(companyId, dbTrans.getMemberId());
                dbTrans.setCreateTookanTask(request.isCreateTookanTask());
                createTookanTask(dbTrans, employee, member, request.getTookanTeamId(), true, false, dbTrans.getQueueType());
            }

            Terminal terminal = terminalRepository.get(companyId, employee.getAssignedTerminalId());

            if (terminal != null && terminal.getCheckoutType() != dbTrans.getCheckoutType()) {
                throw new BlazeInvalidArgException("Employee", "Cannot reassign transaction terminal checkout type is not equivalent to transaction checkout type.");
            }

            if (StringUtils.isNotBlank(request.getOverrideInventoryId())) {
                dbTrans.setOverrideInventoryId(request.getOverrideInventoryId());
            }

            if (terminal != null || StringUtils.isNotBlank(request.getOverrideInventoryId())) {
                if (!request.isTransferItems()) {
                    // if we're transfering inventory, then do not check available inventory
                    if (terminal != null) {
                        terminalId = terminal.getId();
                        dbTrans.setSellerTerminalId(terminal.getId());
                    }
                    checkAvailableInventory(dbTrans, dbTrans, "reassign", employee, terminalId, Boolean.FALSE);
                }
            } else if (!dbTrans.isPreparingFulfillment()) {
                throw new BlazeInvalidArgException("Employee", "Employee does not have an assigned terminal.");
            }

            dbTrans.setSellerId(request.getEmployeeId());
            dbTrans.setStatus(newStatus);
            dbTrans.setTimeZone(shop.getTimeZone());

            /* If order is not assigned previosly update consumer cart order*/
            if (StringUtils.isNotBlank(dbTrans.getConsumerCartId())) {
                ConsumerCart dbConsumerCart = consumerCartRepository.get(companyId, dbTrans.getConsumerCartId());
                if (dbConsumerCart != null) {
                    dbConsumerCart.setEmployeeName(employee.getFirstName() + " " + ((employee.getLastName() != null && employee.getLastName().length() > 0) ? employee.getLastName().substring(0, 1) : ""));
                    dbConsumerCart.setAssignedEmployeeId(employee.getId());
                    consumerCartRepository.update(companyId, dbConsumerCart.getId(), dbConsumerCart);
                }
            }

            employeeNotificationService.sendAssignedOrderNotification(dbTrans, employee.getId());
        }
        dbTrans.setAssignedEmployeeId(request.getEmployeeId());
        dbTrans.setAssigned((StringUtils.isNotBlank(request.getEmployeeId())));
        dbTrans.setCreateOnfleetTask(request.getCreateOnfleetTask());


        // check if employee's terminal has the correct inventory
        transactionRepository.update(companyId, dbTrans.getId(), dbTrans);

        String transferInventoryId = "";
        // if we're transferring items,
        if (request.isTransferItems() && dbTrans.getCart() != null && dbTrans.getCart().getItems().size() > 0) {

            Terminal terminal = terminalRepository.getById(employee.getAssignedTerminalId());
            if (terminal != null) {
                transferInventoryId = terminal.getAssignedInventoryId();
            }


            UniqueSequence sequence = uniqueSequenceRepository.getNewIdentifier(companyId, shopId, "TransactionPriority");

            Transaction trans = new Kryo().copy(dbTrans);
            trans.setId(null);
            trans.setTransNo("" + sequence.getCount());
            trans.setActive(false);
            trans.setCompanyId(companyId);
            trans.setShopId(shopId);
            trans.setQueueType(Transaction.QueueType.None);
            trans.setStatus(Transaction.TransactionStatus.Void);
            trans.setCheckinTime(DateTime.now().getMillis());
            trans.setSellerId(currentEmployeeId);

            trans.setPriority(sequence.getCount());
            trans.setStartTime(DateTime.now().getMillis());
            trans.setEndTime(DateTime.now().getMillis());
            trans.setTransType(Transaction.TransactionType.Transfer);

            if (trans.getCart() != null) {
                trans.getCart().setTotal(new BigDecimal(0));
                trans.getCart().setChangeDue(new BigDecimal(0));
                trans.getCart().setCashReceived(new BigDecimal(0));
                trans.getCart().setSubTotal(new BigDecimal(0));
                trans.getCart().setTaxResult(new TaxResult());
                trans.getCart().setTotalCalcTax(new BigDecimal(0));


                BulkInventoryTransferRequest transferRequest = new BulkInventoryTransferRequest();
                transferRequest.setFromShopId(shopId);
                transferRequest.setToShopId(shopId);

                for (OrderItem orderItem : trans.getCart().getItems()) {
                    orderItem.setFinalPrice(new BigDecimal(0));
                    orderItem.setCost(new BigDecimal(0));
                    orderItem.setCalcTax(new BigDecimal(0));
                    orderItem.setUnitPrice(new BigDecimal(0));


                    if (orderItem.getQuantityLogs().size() > 0) {
                        QuantityLog quantityLog = orderItem.getQuantityLogs().get(0);


                        if (!quantityLog.getInventoryId().equalsIgnoreCase(transferInventoryId)) {
                            InventoryTransferRequest trequest = new InventoryTransferRequest();
                            trequest.setFromBatchId(orderItem.getBatchId());
                            trequest.setFromInventoryId(quantityLog.getInventoryId()); // old inventory
                            trequest.setToInventoryId(transferInventoryId); // new inventory
                            trequest.setTransferAmount(orderItem.getQuantity());
                            trequest.setPrepackageItemId(orderItem.getPrepackageItemId());
                            transferRequest.getTransfers().add(trequest);
                        }
                    }
                }

                // set the transfer requests
                if (transferRequest.getTransfers().size() > 0) {
                    trans.setTransferRequest(transferRequest);
                }
            }


            // Add note
            Note note = new Note();
            note.prepare();
            note.setWriterId(currentEmployeeId);
            note.setWriterName("System");
            note.setMessage(String.format("Re-assigning to '%s %s' with transfer inventory.", employee.getFirstName(), employee.getLastName()));
            trans.setNote(note);


            // ony create the transfer if there's something we're transfering
            if (trans.getTransferRequest() != null) {
                transactionRepository.save(trans);
            }


            dbTrans = transactionRepository.save(trans);
        }


        final CustomerInfo customerInfo = getMemberGroup(dbTrans.getCompanyId(),dbTrans.getMemberId());
        cartService.prepareCart(shop, dbTrans, false, Transaction.TransactionStatus.Hold, false, customerInfo, false);

        // If it's preparing, just save it (we're not taking it from the inventory)
        if (dbTrans.isPreparingFulfillment()) {
            // we're reparing to just save it
            dbTrans.setCart(dbTrans.getCart());
            dbTrans.setStatus(newStatus);
            populatePrepareQty(dbTrans);
            transactionRepository.update(companyId, dbTrans.getId(), dbTrans);
        } else {
            transactionRepository.update(companyId, dbTrans.getId(), dbTrans);
            //queueTransactionJob(transactionId, dbTrans, newStatus, shop, dbTrans.getSellerId(), request.isTransferItems(), transferInventoryId);
            queueTransactionJob(companyId,shopId,currentEmployeeId,terminalId,transactionId,dbTrans,newStatus,shop,request.isTransferItems(),transferInventoryId);
        }


        if (shop.isEnableOnFleet() && dbTrans.getCreateOnfleetTask()) {

            String employeeWorkerId = "";
            EmployeeOnFleetInfo employeeOnFleetInfo = onFleetService.getEmployeeOnfFleetInfoByShop(employee, shop.getId());
            if (employeeOnFleetInfo != null) {
                employeeWorkerId = employeeOnFleetInfo.getOnFleetWorkerId();
            }

            Member member = memberRepository.get(companyId, dbTrans.getMemberId());
            onFleetService.reAssignTask(shop, dbTrans, employeeWorkerId, request.getOnFleetTeamId(), employee, employeeOnFleetInfo, member);

        }
        if (request.isCreateTookanTask() && shop.isEnableTookan()) {
            Member member = memberRepository.get(companyId, dbTrans.getMemberId());
            dbTrans.setCreateTookanTask(request.isCreateTookanTask());
            createTookanTask(dbTrans, employee, member, request.getTookanTeamId(), true, true, dbTrans.getQueueType());
        }

        realtimeService.sendRealTimeEvent(shopId, RealtimeService.RealtimeEventType.QueueUpdateEvent, null);
        // NOTE: SLOPPY WAY OF DOING THIS BUT IT'S CONVENIENT. HAHA.
        SearchResult<Transaction> result = new SearchResult<>();
        result.getValues().add(dbTrans);
        assignDependentObjects(companyId,shopId,result, true, true);
        return dbTrans;
    }



    @Override
    public Transaction unassignTransaction(String companyId, String shopId, String transactionId) {
        return null;
    }

    @Override
    public Transaction startTransaction(String companyId, String shopId, String currentEmployeeId, String transactionId) {
        return null;
    }

    @Override
    public Transaction stopTransaction(String companyId, String shopId, String currentEmployeeId, String transactionId) {
        return null;
    }


    @Override
    public Transaction signTransaction(String companyId, String shopId, String transactionId, String currentEmployeeId, InputStream inputStream, String name, FormDataBodyPart body) {
        return null;
    }





    /*
    Helper Methods
     */

    private void assignDependentObjects(String companyId, String shopId, SearchResult<Transaction> transactions, boolean assignedProducts, boolean assignEmployees) {
        Set<ObjectId> membersIds = new HashSet<>();
        Set<ObjectId> productIds = new HashSet<>();
        Set<ObjectId> employeeIds = new HashSet<>();
        Set<String> productWithPrepackages = new HashSet<>();

        for (Transaction transaction : transactions.getValues()) {
            if (StringUtils.isNotEmpty(transaction.getMemberId())) {
                membersIds.add(new ObjectId(transaction.getMemberId()));
            }
            if (assignEmployees && ObjectId.isValid(transaction.getSellerId())) {
                employeeIds.add(new ObjectId(transaction.getSellerId()));
            }
            if (StringUtils.isNotEmpty(transaction.getAssignedEmployeeId()) && ObjectId.isValid(transaction.getAssignedEmployeeId())) {
                employeeIds.add(new ObjectId(transaction.getAssignedEmployeeId()));
            }
            if (assignedProducts) {
                for (OrderItem item : transaction.getCart().getItems()) {
                    productIds.add(new ObjectId(item.getProductId()));

                    if (StringUtils.isNotEmpty(item.getPrepackageItemId())
                            && ObjectId.isValid(item.getPrepackageItemId())) {
                        productWithPrepackages.add(item.getProductId());
                    }
                }
            }
        }

        HashMap<String, Member> memberHashMap = memberRepository.findItemsInAsMap(companyId, Lists.newArrayList(membersIds));
        HashMap<String, Product> productHashMap = new HashMap<>();
        HashMap<String, Employee> employeeHashMap = new HashMap<>();
        HashMap<String, Prepackage> prepackageHashMap = new HashMap<>();
        HashMap<String, PrepackageProductItem> prepackageProductItemHashMap = new HashMap<>();
        HashMap<String, ProductWeightTolerance> weightToleranceHashMap = weightToleranceRepository.listAsMap(companyId);

        HashMap<String, Terminal> terminalHashMap = terminalRepository.listAllAsMap(companyId, shopId);
        HashMap<String, ProductCategory> categoryHashMap = productCategoryRepository.listAllAsMap(companyId, shopId);
        HashMap<String, Brand> brandHashMap = brandRepository.listAllAsMap(companyId);

        if (assignedProducts) {
            productHashMap = productRepository.findItemsInAsMap(companyId, shopId, Lists.newArrayList(productIds));

            List<String> pids = Lists.newArrayList(productWithPrepackages);
            prepackageHashMap = prepackageRepository.getPrepackagesForProductsAsMap(companyId, shopId, pids);
            prepackageProductItemHashMap = prepackageProductItemRepository.getPrepackagesForProductsAsMap(companyId, shopId, pids);
        }
        if (assignEmployees) {
            employeeHashMap = employeeRepository.findItemsInAsMap(companyId, Lists.newArrayList(employeeIds));
        }

        for (Transaction transaction : transactions.getValues()) {
            if (StringUtils.isNotEmpty(transaction.getMemberId())) {
                transaction.setMember(memberHashMap.get(transaction.getMemberId()));
            }
            if (assignedProducts) {
                for (OrderItem item : transaction.getCart().getItems()) {
                    item.setProduct(productHashMap.get(item.getProductId()));
                    if (item.getProduct() != null) {
                        item.getProduct().setCategory(categoryHashMap.get(item.getProduct().getCategoryId()));
                        item.getProduct().setBrand(brandHashMap.get(item.getProduct().getBrandId()));
                    }

                    if (StringUtils.isNotEmpty(item.getPrepackageItemId())) {
                        PrepackageProductItem prepackageProductItem = prepackageProductItemHashMap.get(item.getPrepackageItemId());
                        item.setPrepackageProductItem(prepackageProductItem);
                        if (prepackageProductItem != null) {
                            Prepackage prepackage = prepackageHashMap.get(prepackageProductItem.getPrepackageId());
                            item.setPrepackage(prepackage);
                            item.setPrepackageProductItem(prepackageProductItem);

                            if (prepackage != null) {
                                ProductWeightTolerance weightTolerance = weightToleranceHashMap.get(prepackage.getToleranceId());
                                if (weightTolerance != null) {
                                    prepackage.setName(weightTolerance.getName());
                                    prepackage.setUnitValue(weightTolerance.getUnitValue());
                                }
                            }
                        }
                    }
                }
            }
            if (assignEmployees) {
                Employee employee = employeeHashMap.get(transaction.getSellerId());
                if (employee != null) {
                    employee.setPassword(null);
                    employee.setPin(null);
                    transaction.setSeller(employee);
                }


                Employee assignedEmp = employeeHashMap.get(transaction.getAssignedEmployeeId());
                if (assignedEmp != null) {
                    assignedEmp.setPassword(null);
                    assignedEmp.setPin(null);
                    transaction.setAssignedEmployee(assignedEmp);
                }
            }
            // assign terminals
            transaction.setSellerTerminal(terminalHashMap.get(transaction.getSellerTerminalId()));
            transaction.setAssigned(StringUtils.isNotBlank(transaction.getAssignedEmployeeId()));
        }

    }

    private void populatePrepareQty(Transaction transaction) {
        if (transaction != null) {
            for (OrderItem orderItem : transaction.getCart().getItems()) {
                orderItem.setPreparedQty(orderItem.getQuantity());
            }
        }
    }

    // Check Inventory Available
    private void checkAvailableInventory(final Transaction inTransaction, final Transaction dbTransaction, String action, Employee employee, String sellerTerminalId, Boolean checkInAllInventory) {
        final String companyId = dbTransaction.getCompanyId();
        final String shopId = dbTransaction.getShopId();

        //Get list of associated products
        List<ObjectId> productIds = new ArrayList<>();
        List<String> productIdsStr = new ArrayList<>();
        for (OrderItem orderItem : inTransaction.getCart().getItems()) {
            if (orderItem.getStatus() == OrderItem.OrderItemStatus.Refunded) {
                continue;
            }
            if (orderItem.getProductId() != null && ObjectId.isValid(orderItem.getProductId())) {
                productIds.add(new ObjectId(orderItem.getProductId()));
                productIdsStr.add(orderItem.getProductId());
            }
        }
        Iterable<Product> products = productRepository.list(dbTransaction.getCompanyId(), productIds);
        Iterable<ProductPrepackageQuantity> prepackageQuantities = prepackageQuantityRepository.getQuantitiesForProducts(companyId, shopId, productIdsStr);

        // For each product in list
        // Sum available for quantity in dbTransaction for product
        // Sum newamount in InTransaction for product
        // new quantity = available + oldAmount - newAmount
        Inventory inventory = null;
        final String terminalId = sellerTerminalId; //employee == null ? null : employee.getAssignedTerminalId();

        Terminal terminal = terminalRepository.get(companyId, terminalId);

        boolean useOverrideInventory = Boolean.FALSE;
        if (terminal != null) {
            String assignedInventoryId = terminal.getAssignedInventoryId();
            if (assignedInventoryId != null && StringUtils.isBlank(inTransaction.getOverrideInventoryId())) {
                inventory = inventoryRepository.get(companyId, assignedInventoryId);
            } else if (StringUtils.isNotBlank(inTransaction.getOverrideInventoryId())) {
                inventory = inventoryRepository.get(companyId, inTransaction.getOverrideInventoryId());
                useOverrideInventory = Boolean.TRUE;
            }
        }
        if (inventory == null && !checkInAllInventory) {
            throw new BlazeInvalidArgException("Transaction", "Inventory is not assigned.");
        }

        for (Product product : products) {
            double available = 0d;
            int prepackagesAvailable = 0;
            for (ProductQuantity productQuantity : product.getQuantities()) {
                if (!checkInAllInventory && productQuantity.getInventoryId().equalsIgnoreCase(inventory.getId())) {
                    available = productQuantity.getQuantity().doubleValue();
                    break;
                } else if (checkInAllInventory) {
                    available += productQuantity.getQuantity().doubleValue();
                }
            }

            for (ProductPrepackageQuantity prepackageQuantity : prepackageQuantities) {
                if (!checkInAllInventory && prepackageQuantity.getProductId().equalsIgnoreCase(product.getId())
                        && prepackageQuantity.getInventoryId().equalsIgnoreCase(inventory.getId())) {
                    prepackagesAvailable += prepackageQuantity.getQuantity();
                } else if (checkInAllInventory && prepackageQuantity.getProductId().equalsIgnoreCase(product.getId())) {
                    prepackagesAvailable += prepackageQuantity.getQuantity();
                }
            }


            // get old amount
            double oldAmount = 0d;
            int prepackageOldAmount = 0;
            if (dbTransaction != null) {
                for (OrderItem orderItem : dbTransaction.getCart().getItems()) {
                    if (orderItem.getQuantityLogs().size() > 0) {
                        for (QuantityLog log : orderItem.getQuantityLogs()) {
                            if (!checkInAllInventory && log.getInventoryId().equalsIgnoreCase(inventory.getId())) {
                                if (StringUtils.isNotBlank(log.getPrepackageItemId())) {
                                    prepackageOldAmount += orderItem.getQuantity().intValue();
                                } else {
                                    oldAmount += orderItem.getQuantity().doubleValue();
                                }
                            } else if (checkInAllInventory) {
                                if (StringUtils.isNotBlank(log.getPrepackageItemId())) {
                                    prepackageOldAmount += orderItem.getQuantity().intValue();
                                } else {
                                    oldAmount += orderItem.getQuantity().doubleValue();
                                }
                            }
                        }

                    }/* else {
                        if (product.getId().equalsIgnoreCase(orderItem.getProductId())) {
                            if (BLStringUtils.isNotBlank(orderItem.getPrepackageItemId())) {
                                prepackageOldAmount += orderItem.getQuantity().intValue();

                            } else {
                                oldAmount += orderItem.getQuantity().doubleValue();
                            }
                        }
                    }*/
                }
            }

            double newAmount = 0d;
            int prepackageNewAmount = 0;
            for (OrderItem orderItem : inTransaction.getCart().getItems()) {
                if (orderItem.getStatus() == OrderItem.OrderItemStatus.Refunded) {
                    continue;
                }
                if (product.getId().equalsIgnoreCase(orderItem.getProductId())) {
                    if (StringUtils.isNotBlank(orderItem.getPrepackageItemId())) {
                        prepackageNewAmount += orderItem.getQuantity().intValue();
                    } else {
                        newAmount += orderItem.getQuantity().doubleValue();
                    }
                }
            }

            double newAvailable = available + oldAmount - newAmount;
            int prepackageNewAvailable = prepackagesAvailable + prepackageOldAmount - prepackageNewAmount;
            double rounded = NumberUtils.round(newAvailable, 2);
            LOG.info(String.format("RAW: rounded: %.3f, available: %.3f, oldAmount: %.3f, newAmount: %.3f", rounded, available, oldAmount, newAmount));
            LOG.info(String.format("Pre" +
                            "packages: newAvailable: %d, preAvailable: %d, preOldAmount: %d, preNewAmount: %d",
                    prepackageNewAvailable, prepackagesAvailable, prepackageOldAmount, prepackageNewAmount));
            if (rounded < 0 || prepackageNewAvailable < 0) {
                String msg = StringUtils.EMPTY;
                if (checkInAllInventory) {
                    msg = String.format("Not have enough stock for product '%s' to %s transaction.", product.getName(), action);
                } else {
                    msg = String.format("Inventory '%s' does not have enough stock for product '%s' to %s transaction.",
                            inventory.getName(), product.getName(), action);
                }

                if (!useOverrideInventory && employee != null) {
                    msg = String.format("Employee '%s' does not have enough stock for product '%s' in his/her inventory '%s'.",
                            employee.getFirstName() + " " + employee.getLastName(), product.getName(), inventory.getName());
                }
                throw new BlazeInvalidArgException("Transaction", msg);
            }
        }
    }

    private boolean checkLockedOrder(Transaction dbTrans, Transaction transaction) {

        if (dbTrans.getCart().getItems().size() != transaction.getCart().getItems().size()) {
            return true;
        } else {
            for (OrderItem item : dbTrans.getCart().getItems()) {
                for (OrderItem orderItem : transaction.getCart().getItems()) {
                    if (item.getId().equalsIgnoreCase(orderItem.getId())) {
                        if (item.getQuantity().doubleValue() != orderItem.getQuantity().doubleValue()) {
                            return true;
                        }
                        break;
                    }
                }
            }
        }

        return false;
    }

    private String getSellerTerminalId(Shop shop, String currentEmployeeId, String assignedEmployeeId, String terminalId) {
        String sellerTerminalId = terminalId;

        if (shop != null) {
            Employee employee = null;
            if (shop.getTermSalesOption() == Shop.TerminalSalesOption.AssignedEmployeeTerminal) {
                employee = employeeRepository.get(shop.getCompanyId(), assignedEmployeeId);

            } else if (shop.getTermSalesOption() == Shop.TerminalSalesOption.SellerAssignedTerminal) {
                employee = employeeRepository.get(shop.getCompanyId(), currentEmployeeId);
            }
            if (employee != null && StringUtils.isNotBlank(employee.getAssignedTerminalId())) {
                return employee.getAssignedTerminalId();
            }
        }


        // Get default terminal?
        return sellerTerminalId;
    }


    private CustomerInfo getMemberGroup(final String companyId, final String memberId) {
        Member member = memberRepository.get(companyId, memberId);
        CustomerInfo customerInfo = new CustomerInfo();
        if (member != null) {
            MemberGroup group = memberGroupRepository.get(companyId, member.getMemberGroupId());
            customerInfo.setMemberGroup(group);
            customerInfo.setConsumerType(member.getConsumerType());
        }
        customerInfo.setPatient(member);
        return customerInfo;
    }


    public void queueTransactionJob(String companyId,
                                    String shopId,
                                    String employeeId,
                                    String terminalId,
                                    String transactionId,
                                    Transaction transaction,
                                    Transaction.TransactionStatus status, Shop shop) {
        //queueTransactionJob(companyId,shopId,transactionId, transaction, status, shop, employeeId, false, "");

        queueTransactionJob(companyId, shopId, employeeId, terminalId, transactionId, transaction, status, shop, false, "");
    }

    private void queueRefundJob(String companyId,
                                String shopId,
                                String transactionId,
                                RefundTransactionRequest refundTransactionRequest,
                                String sellerId,
                                String terminalId) {

        QueuedTransaction queuedTransaction = new QueuedTransaction();
        queuedTransaction.setShopId(shopId);
        queuedTransaction.setCompanyId(companyId);
        queuedTransaction.setId(ObjectId.get().toString());
        if (refundTransactionRequest.isWithInventory()) {
            queuedTransaction.setPendingStatus(Transaction.TransactionStatus.RefundWithInventory);
        } else {
            queuedTransaction.setPendingStatus(Transaction.TransactionStatus.RefundWithoutInventory);
        }
        queuedTransaction.setStatus(QueuedTransaction.QueueStatus.Pending);
        queuedTransaction.setQueuedTransType(QueuedTransaction.QueuedTransactionType.RefundRequest);
        queuedTransaction.setSellerId(sellerId);
        queuedTransaction.setMemberId(null);
        queuedTransaction.setTerminalId(terminalId); // User the current terminal
        queuedTransaction.setTransactionId(transactionId);
        queuedTransaction.setRequestTime(DateTime.now().getMillis());

        queuedTransaction.setReassignTransfer(false);
        queuedTransaction.setNewTransferInventoryId(null);

        queuedTransactionRepository.save(queuedTransaction);

        backgroundJobService.addTransactionJob(companyId, shopId, queuedTransaction.getId());
    }

    private void queueTransactionJob(String companyId,
                                     String shopId,
                                     String employeeId,
                                     String terminalId,
                                     String transactionId,
                                     Transaction transaction,
                                     Transaction.TransactionStatus status,
                                     Shop shop,
                                     boolean transferItems,
                                     String transferInventoryId) {
        String sellerTerminalId = getSellerTerminalId(shop, employeeId,transaction.getAssignedEmployeeId(), terminalId);

        QueuedTransaction queuedTransaction = new QueuedTransaction();
        queuedTransaction.setShopId(shopId);
        queuedTransaction.setCompanyId(companyId);
        queuedTransaction.setId(ObjectId.get().toString());
        queuedTransaction.setCart(transaction.getCart());
        queuedTransaction.setPendingStatus(status);
        queuedTransaction.setStatus(QueuedTransaction.QueueStatus.Pending);
        queuedTransaction.setQueuedTransType(QueuedTransaction.QueuedTransactionType.SalesTrans);
        queuedTransaction.setSellerId(employeeId);
        queuedTransaction.setMemberId(transaction.getMemberId());
        queuedTransaction.setTerminalId(sellerTerminalId); // User the current terminal
        queuedTransaction.setTransactionId(transactionId);
        queuedTransaction.setRequestTime(DateTime.now().getMillis());

        queuedTransaction.setReassignTransfer(transferItems);
        queuedTransaction.setNewTransferInventoryId(transferInventoryId);

        queuedTransactionRepository.save(queuedTransaction);

        backgroundJobService.addTransactionJob(companyId, shopId, queuedTransaction.getId());
    }


    // PARTNERS

    /**
     * This is private method for update consumer order web hook
     *
     * @param dbConsumerCart
     */
    private void updateConsumerOrderWebHook(String companyId, String shopId, ConsumerCart dbConsumerCart) {
        final ConsumerOrderData consumerOrderData = new ConsumerOrderData();
        StringBuilder memberName = new StringBuilder();
        StringBuilder consumerName = new StringBuilder();
        if (StringUtils.isNotBlank(dbConsumerCart.getMemberId())) {
            Member member = memberRepository.getById(dbConsumerCart.getMemberId());
            if (member == null) {
                member = memberRepository.getMemberWithConsumerId(companyId, dbConsumerCart.getConsumerId());
            }
            if (member != null) {
                memberName.append(member.getFirstName())
                        .append((member.getMiddleName() == null) ? " " : member.getMiddleName())
                        .append((member.getLastName() == null) ? "" : member.getLastName());
                consumerOrderData.setLoyaltyPoints(member.getLoyaltyPoints());
                consumerOrderData.setMemberId(member.getId());
            }
        }
        if (StringUtils.isNotBlank(dbConsumerCart.getConsumerId())) {
            ConsumerUser consumerUser = consumerUserRepository.getById(dbConsumerCart.getConsumerId());
            if (consumerUser != null) {
                consumerName.append(consumerUser.getFirstName())
                        .append((consumerUser.getMiddleName() == null) ? " " : consumerUser.getMiddleName())
                        .append((consumerUser.getLastName() == null) ? "" : consumerUser.getLastName());
                consumerOrderData.setMembershipAccepted(consumerUser.isAccepted());
                consumerOrderData.setConsumerId(consumerUser.getId());
            }
        }
        consumerOrderData.setConsumerName(consumerName.toString());
        consumerOrderData.setConsumerOrderId(dbConsumerCart.getId());
        consumerOrderData.setConsumerOrderNo(dbConsumerCart.getOrderNo());
        consumerOrderData.setOrderStatus(dbConsumerCart.getCartStatus());
        consumerOrderData.setOrderTime(dbConsumerCart.getOrderPlacedTime());
        consumerOrderData.setMemberName(memberName.toString());
        consumerOrderData.setCart(dbConsumerCart.getCart());
        try {
            partnerWebHookManager.updateConsumerOrderHook(companyId, shopId, consumerOrderData);
        } catch (Exception e) {
            LOG.warn("Update consumer web hook failed for shop id :" + shopId + " and company id:" + companyId, e);
        }
    }

    private void queuePrepareTransactionJob(String terminalId,
                                            String transactionId,
                                            Transaction transaction,
                                            Transaction.TransactionStatus status,
                                            Shop shop,
                                            String sellerId,
                                            boolean transferItems, String transferInventoryId) {
        String sellerTerminalId = getSellerTerminalId(shop, sellerId,transaction.getAssignedEmployeeId(), terminalId);

        QueuedTransaction queuedTransaction = new QueuedTransaction();
        queuedTransaction.setShopId(transaction.getShopId());
        queuedTransaction.setCompanyId(transaction.getCompanyId());
        queuedTransaction.setId(ObjectId.get().toString());
        queuedTransaction.setCart(transaction.getCart());
        queuedTransaction.setPendingStatus(status);
        queuedTransaction.setStatus(QueuedTransaction.QueueStatus.Prepare);
        queuedTransaction.setSellerId(sellerId);
        queuedTransaction.setMemberId(transaction.getMemberId());
        queuedTransaction.setTerminalId(sellerTerminalId); // User the current terminal
        queuedTransaction.setTransactionId(transactionId);
        queuedTransaction.setRequestTime(0);

        queuedTransaction.setReassignTransfer(transferItems);
        queuedTransaction.setNewTransferInventoryId(transferInventoryId);

        queuedTransactionRepository.save(queuedTransaction);
    }

    /*
     * TOOKAN AND ONFLEET
     */

    //TOOKAN
    private void createTookanTask(Transaction transaction, Employee employee, Member member, String teamId, boolean isReassign, boolean isCreate, Transaction.QueueType queueType) {

        EmployeeTookanInfo employeeTookanInfo = null;

        if (employee != null) {

            if (employee.getTookanInfoList() == null || employee.getTookanInfoList().isEmpty()) {
                throw new BlazeInvalidArgException("Tookan Task", "Employee is not linked with tookan.");
            }
            for (EmployeeTookanInfo tookanInfo : employee.getTookanInfoList()) {
                if (tookanInfo.getShopId().equals(transaction.getShopId())) {
                    employeeTookanInfo = tookanInfo;
                }
            }
            if (employeeTookanInfo == null) {
                throw new BlazeInvalidArgException("Tookan Task", "Employee is not linked with tookan.");
            }
            if (StringUtils.isNotBlank(teamId)) {
                TookanTeamInfo teamInfo = tookanService.getTookanTeamInfo(transaction.getCompanyId(), transaction.getShopId(), teamId);
                if (teamInfo == null) {
                    throw new BlazeInvalidArgException("Tookan Task", "Team does not exist.");
                }
                boolean isTeamAssigned = tookanService.checkTeamAssignment(transaction.getShopId(), teamId, employeeTookanInfo);
                if (!isTeamAssigned) {
                    LOG.info("Team :" + teamInfo.getTeamName() + " does not assigned to employee :" + employee.getFirstName());
                    throw new BlazeInvalidArgException("Tookan Task", "Team :" + teamInfo.getTeamName() + " does not assigned to employee :" + employee.getFirstName());
                }
            }
        }
        if (queueType == Transaction.QueueType.WalkIn && member == null) {
            throw new BlazeInvalidArgException("Tookan Task", "Member does not found");
        }

        if (isCreate) {
            tookanService.createTookanTask(transaction, employee, member, teamId, (employeeTookanInfo != null) ? employeeTookanInfo.getTookanAgentId() : "", isReassign);
        }
    }


    private void createTaskAtOnFleet(Shop shop, Employee employee, Transaction trans, String onFleetTeamId, Member member) {
        String employeeWorkerId = "";
        EmployeeOnFleetInfo employeeOnFleetInfo = onFleetService.getEmployeeOnfFleetInfoByShop(employee, trans.getShopId());
        if (employeeOnFleetInfo != null) {
            employeeWorkerId = employeeOnFleetInfo.getOnFleetWorkerId();
        }

        onFleetService.createOnFleetTask(shop, trans, employeeWorkerId, onFleetTeamId, employee, employeeOnFleetInfo, member, false);
    }

    /**
     * This method is used for transaction  payment  with Linux, Clover, Mtrac payment type.
     * @param dbTrans
     * @param transaction
     * @return
     */
    private Transaction processPaymentOption(Transaction dbTrans, Transaction transaction) {
        if (transaction.getCart().getPaymentOption() == Cart.PaymentOption.Linx) {
            ProcessPaymentCardsForTransactionEvent processLoyaltyCards = new ProcessPaymentCardsForTransactionEvent();
            processLoyaltyCards.setPayload(dbTrans, transaction);
            eventBus.post(processLoyaltyCards);
            processLoyaltyCards.getResponse();
            dbTrans = processLoyaltyCards.getDbTransaction();
        } else if (transaction.getCart().getPaymentOption() == Cart.PaymentOption.Clover) {
            CloverPaymentCardsForTransactionEvent processLoyaltyCards = new CloverPaymentCardsForTransactionEvent();
            processLoyaltyCards.setPayload(dbTrans, transaction);
            eventBus.post(processLoyaltyCards);
            processLoyaltyCards.getResponse();
            dbTrans = processLoyaltyCards.getDbTransaction();
        } else if (transaction.getCart().getPaymentOption() == Cart.PaymentOption.Mtrac) {
            MtracPaymentCardsForTransactionEvent processLoyaltyCards = new MtracPaymentCardsForTransactionEvent();
            processLoyaltyCards.setPayload(dbTrans, transaction);
            eventBus.post(processLoyaltyCards);
            processLoyaltyCards.setResponseTimeout(30000);
            processLoyaltyCards.getResponse();
            dbTrans = processLoyaltyCards.getDbTransaction();
        }

        return dbTrans;
    }

    @Override
    public Transaction finalizeTransaction(String companyId, String shopId, String transactionId, Transaction incomingTransaction, boolean finalize) {
        Transaction transaction = transactionRepository.get(companyId, transactionId);
        LOG.info("Finalizing transaction: " + transactionId);
        if (transaction == null) {
            LOG.error("Error finalizing transaction: " + transactionId);
            throw new BlazeInvalidArgException("Transaction", "Transaction does not exist.");
        }

        if (incomingTransaction != null) {
            transaction = incomingTransaction;
        }

        Shop shop = shopRepository.get(companyId, shopId);

        //If status is true, the selected order should be finalized
        if(finalize) {
            cartService.finalizeAllOrderItems(shop, transaction);
        } else {
            cartService.unfinalizeAllOrderItems(shop, transaction);
        }

        final Transaction dbTransaction = transactionRepository.save(transaction);
        LOG.info("Successful finalizing transaction: " + transactionId);
        Transaction trans = new Kryo().copy(dbTransaction);
        return trans;
    }

}
