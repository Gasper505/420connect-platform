package com.fourtwenty.core.services.partners;

import com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrder;
import com.fourtwenty.core.rest.dispensary.requests.inventory.PurchaseOrderAddRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.PurchaseOrderItemResult;

public interface CommonPurchaseOrderService {

    PurchaseOrder addPurchaseOrder(String companyId, String shopId, String currentEmployeeId, PurchaseOrderAddRequest request);

    PurchaseOrder preparePurchaseOrder(String companyId, String shopId, String currentEmployeeId, PurchaseOrderAddRequest request);

    PurchaseOrder updatePurchaseOrder(String companyId, String shopId, String currentEmployeeId, String purchaseOrderId, PurchaseOrder request);

    PurchaseOrderItemResult getPurchaseOrderById(String companyId, String shopId, String purchaseOrderId);

    SearchResult<PurchaseOrderItemResult> getAllPurchaseOrderByDates(String companyId, String shopId, long startDate, long endDate, int start, int limit);
}
