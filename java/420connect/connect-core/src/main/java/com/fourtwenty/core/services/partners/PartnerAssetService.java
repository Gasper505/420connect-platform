package com.fourtwenty.core.services.partners;

import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.rest.dispensary.results.common.AssetStreamResult;

import java.io.InputStream;

public interface PartnerAssetService {
    CompanyAsset uploadAssetPrivate(InputStream inputStream, String name, CompanyAsset.AssetType type, String mimeType);

    AssetStreamResult getAssetStream(String assetKey, boolean handleError);
}
