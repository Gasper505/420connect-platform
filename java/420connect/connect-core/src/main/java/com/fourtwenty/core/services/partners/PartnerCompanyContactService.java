package com.fourtwenty.core.services.partners;

import com.fourtwenty.core.domain.models.company.CompanyContact;
import com.fourtwenty.core.rest.dispensary.requests.partners.PartnerCompanyContactRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.company.CompanyContactResult;

public interface PartnerCompanyContactService {

    CompanyContact createCompanyContact(PartnerCompanyContactRequest companyContact);

    CompanyContact updateCompanyContact(String contactId, PartnerCompanyContactRequest companyContact);

    CompanyContactResult getCompanyContactById(String contactId);

    SearchResult<CompanyContact> getCompanyContacts(String startDate, String endDate, int skip, int limit);
}
