package com.fourtwenty.core.services.partners;

import com.fourtwenty.core.domain.models.loyalty.LoyaltyReward;
import com.fourtwenty.core.domain.models.loyalty.Promotion;
import com.fourtwenty.core.rest.dispensary.requests.promotions.PartnerAddPromotionRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.company.LoyaltyMemberReward;


public interface PartnerLoyaltyService {
    SearchResult<LoyaltyReward> getRewards(int start, int limit);

    Promotion findPromotion(String promoCode);
    Promotion addPromotion(PartnerAddPromotionRequest request);
    Promotion updatePromotion(String promotionId, Promotion aPromotion);
    void deletePromotion(String promotionId);
    SearchResult<Promotion> getPromotions(int start, int limit);
    //Get loyaltyrewards for members
    SearchResult<LoyaltyMemberReward> getRewardsForMembers(String memberId);

}
