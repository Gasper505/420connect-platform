package com.fourtwenty.core.services.partners;

import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.rest.consumer.results.MemberLimit;
import com.fourtwenty.core.rest.dispensary.requests.company.MembershipAddRequest;
import com.fourtwenty.core.rest.dispensary.requests.company.MembershipUpdateRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;

public interface PartnerMemberService {
    Member getMemberById(String memberId);
    Member addMember(MembershipAddRequest addRequest);
    Member updateMembership(String memberId, MembershipUpdateRequest updateRequest);
    Member getMemberByEmail(String email);

    SearchResult<Member> getMemberList(long startDate, long endDate, int skip, int limit);

    HashMap<Product.CannabisType, BigDecimal> getMemberLimit(String memberId);
    MemberLimit getRemainingMemberLimit(String memberId);

    ArrayList<Member> getMembersByLicenceNo(String licenceNumber);
}
