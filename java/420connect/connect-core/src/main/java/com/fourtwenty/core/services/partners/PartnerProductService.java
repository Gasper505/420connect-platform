package com.fourtwenty.core.services.partners;

import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.rest.dispensary.requests.inventory.ProductAddRequest;
import com.fourtwenty.core.rest.dispensary.requests.partners.PartnerProductUpdateRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;

public interface PartnerProductService {
    Product getProductById(String productId);

    Product addProduct(ProductAddRequest addRequest);

    Product updateProduct(String productId, PartnerProductUpdateRequest updateRequest);

    SearchResult<Product> getProducts(String startDate, String endDate, int skip, int limit);
    SearchResult<Product> getProducts(long startDate, long endDate, int skip, int limit);
}
