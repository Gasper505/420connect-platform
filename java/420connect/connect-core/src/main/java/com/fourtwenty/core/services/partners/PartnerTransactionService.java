package com.fourtwenty.core.services.partners;

import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.rest.dispensary.requests.partners.PartnerEmployeeReassignRequest;
import com.fourtwenty.core.rest.dispensary.requests.partners.PartnerQueueAddMemberRequest;
import com.fourtwenty.core.rest.dispensary.requests.partners.PartnerTransactionDeleteRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;

import java.io.InputStream;


public interface PartnerTransactionService {

    SearchResult<Transaction> getAllTransaction(String startDate, String endDate,int skip, int limit);
    SearchResult<Transaction> getActiveTransactions();

    Transaction getTransactionById(String transactionId);

    Transaction addToQueue(String queueName, PartnerQueueAddMemberRequest request);

    void deleteTransaction(String transactionId, PartnerTransactionDeleteRequest request);
    Transaction setTransactionToFulfill(Transaction transaction, String transactionId);
    Transaction completeTransaction(String transactionId, Transaction transaction);

    Transaction markAsPaid(String transactionId, Cart.PaymentOption paymentOption);
    Transaction prepareCart(String transactionId, Transaction transaction);


    Transaction signTransaction(String transactionId, InputStream inputStream,
                                       String name,
                                       final FormDataBodyPart body);

    Transaction holdTransaction(String transactionId, Transaction transaction);
    Transaction reassignTransactionEmployee(String transactionId, PartnerEmployeeReassignRequest request);

    Transaction unassignTransaction(String transactionId);

    Transaction startTransaction(String transactionId);

    Transaction stopTransaction(String transactionId);

    SearchResult<Transaction> getCompletedTransactionsForMember(String memberId, int start, int limit);

    SearchResult<Transaction> getMemberActiveTransactions(String memberId);

    Transaction updateDeliveryAddress(String transactionId,Address request);

    Transaction finalizeTransaction(String transactionId, Transaction transaction, boolean finalize);
}
