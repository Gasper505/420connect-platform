package com.fourtwenty.core.services.partners;

import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.rest.dispensary.requests.inventory.VendorAddRequest;
import com.fourtwenty.core.rest.dispensary.requests.partners.PartnerVendorUpdateRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.VendorResult;

public interface PartnerVendorService {

    VendorResult getVendor(String vendorId);



    Vendor addVendor(VendorAddRequest request);

    Vendor updateVendor(String vendorId, PartnerVendorUpdateRequest vendor);

    SearchResult<Vendor> getVendors(String startDate, String endDate, int skip, int limit);
}
