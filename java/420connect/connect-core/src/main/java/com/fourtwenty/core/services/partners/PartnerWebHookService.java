package com.fourtwenty.core.services.partners;

import com.fourtwenty.core.domain.models.partner.PartnerWebHook;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.partners.request.PartnerWebHookAddRequest;

public interface PartnerWebHookService {
    PartnerWebHook createPartnerWebHooks(PartnerWebHookAddRequest request);

    PartnerWebHook updatePartnerWebHooks(String webHookId, PartnerWebHook request);

    PartnerWebHook getPartnerWebHookById(String webHookId);

    SearchResult<PartnerWebHook> getPartnerWebHook(PartnerWebHook.PartnerWebHookType webHookType, int start, int limit, String shopId);

    void deletePartnerWebHook(String id);
}
