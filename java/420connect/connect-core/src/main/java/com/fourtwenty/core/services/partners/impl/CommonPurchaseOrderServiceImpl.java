package com.fourtwenty.core.services.partners.impl;

import com.fourtwenty.core.domain.models.company.*;
import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.models.generic.Asset;
import com.fourtwenty.core.domain.models.product.CompanyLicense;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.models.purchaseorder.POActivity;
import com.fourtwenty.core.domain.models.purchaseorder.POProductRequest;
import com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrder;
import com.fourtwenty.core.domain.models.transaction.CultivationTaxResult;
import com.fourtwenty.core.domain.models.transaction.TaxResult;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.rest.dispensary.requests.inventory.POProductAddRequest;
import com.fourtwenty.core.rest.dispensary.requests.inventory.PurchaseOrderAddRequest;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.common.UploadFileResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.POProductRequestResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.PurchaseOrderItemResult;
import com.fourtwenty.core.services.global.CompanyUniqueSequenceService;
import com.fourtwenty.core.services.partners.CommonPurchaseOrderService;
import com.fourtwenty.core.services.taxes.TaxCalulationService;
import com.fourtwenty.core.services.thirdparty.AmazonS3Service;
import com.fourtwenty.core.util.QrCodeUtil;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.*;

public class CommonPurchaseOrderServiceImpl implements CommonPurchaseOrderService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CommonPurchaseOrderServiceImpl.class);

    private static final String PO_NOT_FOUND = "Error! Purchase order not found.";
    private static final String PURCHASE_ORDER = "PurchaseOrder";
    private static final String CUSTOM_DATE = "Custom date";
    private static final String NOT_VALID_PRODUCT = "Please provide valid products.";
    private static final String NOT_CUSTOM_DATE = "Please provide custom date.";
    private static final String SHOP_NOT_FOUND = "Shop does not found";
    private static final String SHOP = "Shop";
    private static final String VENDOR_NOT_FOUND = "Vendor does not found";
    private static final String QR_CODE_ERROR = "Error while creating QR code";
    private static final String PO_NOT_ALLOWED_FOR_CUSTOMER = "Purchase Order not allowed for customer";
    private static final String ACTIVITY_LOG = "Purchase Order Activity Log";

    @Inject
    private ShopRepository shopRepository;
    @Inject
    private VendorRepository vendorRepository;
    @Inject
    private PurchaseOrderRepository purchaseOrderRepository;
    @Inject
    private ProductRepository productRepository;
    @Inject
    private ExciseTaxInfoRepository exciseTaxInfoRepository;
    @Inject
    private ProductCategoryRepository productCategoryRepository;
    @Inject
    private CultivationTaxInfoRepository cultivationTaxInfoRepository;
    @Inject
    private POActivityRepository poActivityRepository;
    @Inject
    private CompanyAssetRepository companyAssetRepository;
    @Inject
    private AmazonS3Service amazonS3Service;
    @Inject
    private EmployeeRepository employeeRepository;
    @Inject
    private CompanyRepository companyRepository;
    @Inject
    private CompanyUniqueSequenceService companyUniqueSequenceService;
    @Inject
    private TaxCalulationService taxCalulationService;
    /**
     * Override method to add po
     *
     * @param companyId         : companyId
     * @param shopId            : shopId
     * @param currentEmployeeId : currentEmployeeId
     * @param request           : request
     * @return: purchase order
     */
    @Override
    public PurchaseOrder addPurchaseOrder(String companyId, String shopId, String currentEmployeeId, PurchaseOrderAddRequest request) {
        PurchaseOrder purchaseOrder = preparePurchaseOrder(companyId, shopId, currentEmployeeId, request);

        if (purchaseOrder != null) {
            purchaseOrder.prepare(companyId);
            purchaseOrder.setShopId(shopId);
            long ordinal = getNextSequence(companyId);
            purchaseOrder.setPoNumber(ordinal + "-N");

            PurchaseOrder dbPurchaseOrder = purchaseOrderRepository.save(purchaseOrder);


            if (dbPurchaseOrder != null) {
                addPOActivity(companyId, shopId, dbPurchaseOrder.getId(), currentEmployeeId, "Created PO");
                UploadFileResult result;
                File file;
                try {
                    InputStream inputStream = QrCodeUtil.getQrCode(dbPurchaseOrder.getPoNumber());
                    file = QrCodeUtil.stream2file(inputStream, ".png");
                    String keyName = dbPurchaseOrder.getId() + "-" + dbPurchaseOrder.getPoNumber().replaceAll(" ", "");
                    result = amazonS3Service.uploadFilePublic(file, keyName, ".png");
                    if (Objects.nonNull(result) && StringUtils.isNotBlank(result.getUrl())) {
                        CompanyAsset asset = new CompanyAsset();
                        asset.prepare(companyId);
                        asset.setName("PurchaseOrder");
                        asset.setKey(result.getKey());
                        asset.setActive(true);
                        asset.setType(Asset.AssetType.Photo);
                        asset.setPublicURL(result.getUrl());
                        asset.setSecured(false);
                        companyAssetRepository.save(asset);
                        dbPurchaseOrder.setPoQrCodeAsset(asset);
                        dbPurchaseOrder.setPoQrCodeUrl(result.getUrl());
                        purchaseOrderRepository.update(dbPurchaseOrder.getId(), dbPurchaseOrder);
                    }
                } catch (IOException ex) {
                    LOGGER.error(QR_CODE_ERROR);
                }
            }
            return dbPurchaseOrder;
        }

        return purchaseOrder;

    }

    /**
     * Override method to prepare po
     *
     * @param companyId         : companyId
     * @param shopId            : shopId
     * @param currentEmployeeId : currentEmployeeId
     * @param request           : request
     * @return: purchase order
     */
    @Override
    public PurchaseOrder preparePurchaseOrder(String companyId, String shopId, String currentEmployeeId, PurchaseOrderAddRequest request) {
        Shop shop = shopRepository.get(companyId, shopId);
        if (shop == null) {
            throw new BlazeInvalidArgException(SHOP, SHOP_NOT_FOUND);
        }

        if (request.getPoProductAddRequestList() == null
                || request.getPoProductAddRequestList().size() == 0) {
            throw new BlazeInvalidArgException(PURCHASE_ORDER, NOT_VALID_PRODUCT);
        }

        if (PurchaseOrder.POPaymentTerms.CUSTOM_DATE.toString().equals(request.getPoPaymentTerms()) &&
                request.getCustomTermDate() == 0) {
            throw new BlazeInvalidArgException(CUSTOM_DATE, NOT_CUSTOM_DATE);
        }

        PurchaseOrder purchaseOrder = new PurchaseOrder();
        purchaseOrder.setCreatedByEmployeeId(currentEmployeeId);
        purchaseOrder.setNotes(request.getNotes());
        purchaseOrder.setPoPaymentTerms(PurchaseOrder.POPaymentTerms.valueOf(request.getPoPaymentTerms()));
        purchaseOrder.setPoPaymentOptions(PurchaseOrder.POPaymentOptions.toPOPaymentOptions(request.getPaymentType()));
        purchaseOrder.setPurchaseOrderStatus(PurchaseOrder.PurchaseOrderStatus.InProgress);
        purchaseOrder.setCustomTermDate(request.getCustomTermDate());

        List<POProductRequest> poProductRequests = new ArrayList<>();
        Map<String, Object> poProductRequestResult = createPOProductRequest(companyId, shopId, request.getPoProductAddRequestList(), poProductRequests);
        BigDecimal totalAmount = (BigDecimal) poProductRequestResult.get("totalAmount");
        Map<String, Product> productMap = (Map<String, Product>) poProductRequestResult.get("products");
        List<ObjectId> productCategoryIds = (List<ObjectId>) poProductRequestResult.get("productCategoryList");
        BigDecimal totalDiscount = (BigDecimal) poProductRequestResult.get("totalDiscount");

        purchaseOrder.setPoProductRequestList(poProductRequests);
        purchaseOrder.setTotalCost(totalAmount);
        purchaseOrder.setGrandTotal(totalAmount);
        purchaseOrder.setReference(request.getReference());
        if (request.getDiscount().doubleValue() > purchaseOrder.getTotalCost().doubleValue()) {
            throw new BlazeInvalidArgException(PURCHASE_ORDER, "Discount cannot be greater than total amount.");
        }

        purchaseOrder.setCustomerType(request.getCustomerType());

        if (StringUtils.isBlank(request.getVendorId())) {
            throw new BlazeInvalidArgException(PURCHASE_ORDER, VENDOR_NOT_FOUND);
        }

        Vendor vendor = vendorRepository.getById(request.getVendorId());
        if (vendor == null) {
            throw new BlazeInvalidArgException(PURCHASE_ORDER, VENDOR_NOT_FOUND);
        }
        if (vendor.getVendorType().equals(Vendor.VendorType.CUSTOMER)) {
            throw new BlazeInvalidArgException(PURCHASE_ORDER, PO_NOT_ALLOWED_FOR_CUSTOMER);
        }

        CompanyLicense companyLicense = null;
        if (StringUtils.isNotBlank(request.getLicenseId())) {
            companyLicense = vendor.getCompanyLicense(request.getLicenseId());
            if (companyLicense == null) {
                throw new BlazeInvalidArgException(PURCHASE_ORDER, "Company License not found");
            }
        } else {
            companyLicense = new CompanyLicense();
            companyLicense.setCompanyType(Vendor.CompanyType.RETAILER);
        }

        purchaseOrder.setVendorId(request.getVendorId());
        purchaseOrder.setLicenseId(companyLicense.getId());
        purchaseOrder.setVendorId(request.getVendorId());
        if (request.getTransactionType() == null && vendor.getArmsLengthType() != null) {
            purchaseOrder.setTransactionType(vendor.getArmsLengthType());
        } else {
            purchaseOrder.setTransactionType(request.getTransactionType());
        }

        purchaseOrder.setDiscount(request.getDiscount());
        purchaseOrder.setFees(request.getFees());
        purchaseOrder.setTransactionType(request.getTransactionType());
        purchaseOrder.setFlowerSourceType(request.getFlowerSourceType());

        TaxResult taxResult = null;
        if (Vendor.ArmsLengthType.ARMS_LENGTH.equals(purchaseOrder.getTransactionType()) && shop.isRetail()) {
            taxResult = calculateArmsLengthExciseTax(companyId, shop, purchaseOrder.getPoProductRequestList(), productMap, productCategoryIds);
        }
        //OZ tax calculation
        CultivationTaxResult totalCultivationTax = null;
        if ((Vendor.CompanyType.CULTIVATOR.equals(vendor.getCompanyType()) || Vendor.CompanyType.MANUFACTURER.equals(vendor.getCompanyType())) && PurchaseOrder.FlowerSourceType.CULTIVATOR_DIRECT == purchaseOrder.getFlowerSourceType()) {
            totalCultivationTax = this.calculateOzTax(companyId, shop, purchaseOrder, productMap, productCategoryIds);
        }
        if (taxResult == null) {
            taxResult = new TaxResult();
            taxResult.prepare();
        }
        BigDecimal cultivationTax = new BigDecimal(0);
        if (totalCultivationTax != null) {
            cultivationTax = totalCultivationTax.getTotalCultivationTax();
        }
        taxResult.setCultivationTaxResult(totalCultivationTax == null ? new CultivationTaxResult() : totalCultivationTax);
        purchaseOrder.setTaxResult(taxResult);

        BigDecimal grandTotal = new BigDecimal(0);
        BigDecimal totalTax = new BigDecimal(0);
        if (cultivationTax.doubleValue() > 0) {
            grandTotal = grandTotal.add(cultivationTax);
        }
        grandTotal = grandTotal.add(purchaseOrder.getFees());
        grandTotal = grandTotal.subtract(purchaseOrder.getDiscount());
        grandTotal = grandTotal.add(purchaseOrder.getTotalCost());
        if (purchaseOrder.getTaxResult() != null) {
            totalTax = purchaseOrder.getTaxResult().getTotalExciseTax();
            if (purchaseOrder.getTaxResult().getCultivationTaxResult() != null) {
                totalTax = purchaseOrder.getTaxResult().getTotalExciseTax().add(purchaseOrder.getTaxResult().getCultivationTaxResult().getTotalCultivationTax());
            }

            grandTotal = grandTotal.add(purchaseOrder.getTaxResult().getTotalExciseTax());

            if (purchaseOrder.getCustomerType() == PurchaseOrder.CustomerType.CUSTOMER_COMPANY) {
                applyDeliveryCharge(shop, purchaseOrder);
            }
        }
        purchaseOrder.setGrandTotal(grandTotal.add(purchaseOrder.getDeliveryCharge()));
        purchaseOrder.setTotalDiscount(totalDiscount.add(purchaseOrder.getDiscount()));

        purchaseOrder.setTotalTax(totalTax);
        purchaseOrder.setDeliveryAddress(request.getDeliveryAddress());
        purchaseOrder.setDeliveryTime(request.getDeliveryTime());
        purchaseOrder.setTermsAndCondition(request.getTermsAndCondition());
        purchaseOrder.setDeliveryDate(request.getDeliveryDate());

        purchaseOrder.setPurchaseOrderDate(request.getPurchaseOrderDate());

        purchaseOrder.setDueDate(purchaseOrder.getPODueDate());

        return purchaseOrder;

    }

    /**
     * Override mpurchaseOrderIdethod to update po
     *
     * @param companyId         : companyId
     * @param shopId            : shopId
     * @param currentEmployeeId : currentEmployeeId
     * @param purchaseOrderId   : purchaseorderId
     * @param request           : request
     * @return : purchaseOrder
     */
    @Override
    public PurchaseOrder updatePurchaseOrder(String companyId, String shopId, String currentEmployeeId, String purchaseOrderId, PurchaseOrder request) {

        PurchaseOrder updatedPurchaseOrder;
        HashMap<String, Product> productMap = new HashMap<>();
        List<ObjectId> productCategoryIds = new ArrayList<>();
        Product product;
        if (request.getPoProductRequestList().size() > 0) {
            for (POProductRequest poProductRequest : request.getPoProductRequestList()) {

                if (!poProductRequest.getProductId().isEmpty()) {
                    product = productRepository.get(companyId, poProductRequest.getProductId());
                    if (product == null) {
                        throw new BlazeInvalidArgException("Product", "Product does not exist");
                    }
                    productMap.put(product.getId(), product);
                    productCategoryIds.add(new ObjectId(product.getCategoryId()));
                }
            }
        }


        PurchaseOrder dbPurchaseOrder = this.getPurchaseOrderById(companyId, shopId, purchaseOrderId);

        Shop shop = shopRepository.get(companyId, shopId);
        if (shop == null) {
            throw new BlazeInvalidArgException(SHOP, SHOP_NOT_FOUND);
        }

        if (request.getPoProductRequestList() == null || request.getPoProductRequestList().size() == 0) {
            throw new BlazeInvalidArgException(PURCHASE_ORDER, NOT_VALID_PRODUCT);
        }

        if (PurchaseOrder.POPaymentTerms.CUSTOM_DATE.equals(request.getPoPaymentTerms()) &&
                request.getCustomTermDate() == 0) {
            throw new BlazeInvalidArgException(CUSTOM_DATE, NOT_CUSTOM_DATE);
        }

        if (dbPurchaseOrder != null && dbPurchaseOrder.getPurchaseOrderStatus().equals(PurchaseOrder.PurchaseOrderStatus.InProgress)
                && request.getPoProductRequestList() != null) {

            dbPurchaseOrder.prepare(companyId);
            dbPurchaseOrder.setShopId(shopId);
            dbPurchaseOrder.setCreatedByEmployeeId(currentEmployeeId);
            dbPurchaseOrder.setVendorId(request.getVendorId());
            dbPurchaseOrder.setNotes(request.getNotes());
            dbPurchaseOrder.setPoPaymentTerms(request.getPoPaymentTerms());
            dbPurchaseOrder.setPoPaymentOptions(request.getPoPaymentOptions());
            dbPurchaseOrder.setCustomTermDate(dbPurchaseOrder.getCustomTermDate());
            dbPurchaseOrder.setCustomerType(request.getCustomerType());

            if (StringUtils.isBlank(request.getVendorId())) {
                throw new BlazeInvalidArgException(PURCHASE_ORDER, "Vendor can't be empty");
            }
            Vendor vendor = vendorRepository.getById(request.getVendorId());
            if (vendor == null) {
                throw new BlazeInvalidArgException(PURCHASE_ORDER, VENDOR_NOT_FOUND);
            }
            if (vendor.getVendorType().equals(Vendor.VendorType.CUSTOMER)) {
                throw new BlazeInvalidArgException(PURCHASE_ORDER, PO_NOT_ALLOWED_FOR_CUSTOMER);
            }

            CompanyLicense companyLicense = null;
            if (StringUtils.isNotBlank(request.getLicenseId())) {
                companyLicense = vendor.getCompanyLicense(request.getLicenseId());
                if (companyLicense == null) {
                    throw new BlazeInvalidArgException(PURCHASE_ORDER, "Company License not found");
                }
            } else {
                companyLicense = new CompanyLicense();
                companyLicense.setCompanyType(Vendor.CompanyType.RETAILER);
            }

            dbPurchaseOrder.setVendorId(request.getVendorId());
            dbPurchaseOrder.setLicenseId(companyLicense.getId());

            if (request.getTransactionType() == null && vendor.getArmsLengthType() != null) {
                dbPurchaseOrder.setTransactionType(vendor.getArmsLengthType());
            } else {
                dbPurchaseOrder.setTransactionType(request.getTransactionType());
            }

            dbPurchaseOrder.setTransactionType(request.getTransactionType());

            HashMap<String, BigDecimal> infoMap = processIncomingProductRequest(companyId, shopId, request.getPoProductRequestList());
            BigDecimal totalAmount = infoMap.get("totalAmount");
            BigDecimal totalItemDiscount = infoMap.get("totalDiscount");

            dbPurchaseOrder.setPoProductRequestList(request.getPoProductRequestList());
            dbPurchaseOrder.setTotalCost(totalAmount);
            dbPurchaseOrder.setReference(request.getReference());
            if (request.getDiscount().doubleValue() > dbPurchaseOrder.getTotalCost().doubleValue()) {
                throw new BlazeInvalidArgException(PURCHASE_ORDER, "Discount cannot be greater than total amount.");
            }

            //Reset calc taxes
            dbPurchaseOrder.setGrandTotal(totalAmount);
            dbPurchaseOrder.setTotalCalcTax(new BigDecimal(0));
            dbPurchaseOrder.setTotalPreCalcTax(new BigDecimal(0));

            TaxResult taxResult = null;
            if (Vendor.ArmsLengthType.ARMS_LENGTH.equals(dbPurchaseOrder.getTransactionType()) && CompanyFeatures.AppTarget.Distribution != shop.getAppTarget()) {
                taxResult = calculateArmsLengthExciseTax(companyId, shop, dbPurchaseOrder.getPoProductRequestList(), productMap, productCategoryIds);
            }
            //OZ tax calculation
            CultivationTaxResult totalCultivationTax = null;
            if (Vendor.CompanyType.CULTIVATOR.equals(vendor.getCompanyType()) || Vendor.CompanyType.MANUFACTURER.equals(vendor.getCompanyType())) {
                if (PurchaseOrder.FlowerSourceType.CULTIVATOR_DIRECT == request.getFlowerSourceType()) {
                    totalCultivationTax = this.calculateOzTax(companyId, shop, dbPurchaseOrder, productMap, productCategoryIds);
                }
            }

            if (taxResult == null) {
                taxResult = new TaxResult();
            }
            BigDecimal cultivationTax = new BigDecimal(0);
            if (totalCultivationTax != null) {
                cultivationTax = totalCultivationTax.getTotalCultivationTax();
            }

            taxResult.setCultivationTaxResult(totalCultivationTax == null ? new CultivationTaxResult() : totalCultivationTax);
            dbPurchaseOrder.setDiscount(request.getDiscount());
            dbPurchaseOrder.setFees(request.getFees());
            dbPurchaseOrder.setTaxResult(taxResult);
            dbPurchaseOrder.setTotalDiscount(totalItemDiscount.add(dbPurchaseOrder.getDiscount()));

            BigDecimal grandTotal = new BigDecimal(0);
            BigDecimal totalTax = new BigDecimal(0);
            if (cultivationTax.doubleValue() > 0) {
                grandTotal = grandTotal.add(cultivationTax);
            }
            grandTotal = grandTotal.add(dbPurchaseOrder.getTotalCost());
            if (dbPurchaseOrder.getTaxResult() != null) {
                totalTax = dbPurchaseOrder.getTaxResult().getTotalExciseTax();
                if (dbPurchaseOrder.getTaxResult().getCultivationTaxResult() != null) {
                    totalTax = dbPurchaseOrder.getTaxResult().getTotalExciseTax().add(dbPurchaseOrder.getTaxResult().getCultivationTaxResult().getTotalCultivationTax());
                }
                grandTotal = grandTotal.add(dbPurchaseOrder.getTaxResult().getTotalExciseTax());

                if (PurchaseOrder.CustomerType.CUSTOMER_COMPANY == dbPurchaseOrder.getCustomerType()) {
                    applyDeliveryCharge(shop, dbPurchaseOrder);
                }
            }

            grandTotal = grandTotal.add(dbPurchaseOrder.getFees());
            grandTotal = grandTotal.subtract(dbPurchaseOrder.getDiscount());
            dbPurchaseOrder.setGrandTotal(grandTotal.add(dbPurchaseOrder.getDeliveryCharge()));

            dbPurchaseOrder.setTotalTax(totalTax);
            dbPurchaseOrder.setDeliveryAddress(request.getDeliveryAddress());
            dbPurchaseOrder.setDeliveryTime(request.getDeliveryTime());
            dbPurchaseOrder.setTermsAndCondition(request.getTermsAndCondition());
            dbPurchaseOrder.setDeliveryDate(request.getDeliveryDate());

            dbPurchaseOrder.setManagerReceiveSignature(request.getManagerReceiveSignature());

            dbPurchaseOrder.setPurchaseOrderDate(request.getPurchaseOrderDate());
            dbPurchaseOrder.setDueDate(request.getPODueDate());

            updatedPurchaseOrder = purchaseOrderRepository.update(companyId, dbPurchaseOrder.getId(), dbPurchaseOrder);

            if (updatedPurchaseOrder != null) {
                addPOActivity(companyId, shopId, updatedPurchaseOrder.getId(), currentEmployeeId, "Updated PO");
            }
        } else {
            throw new BlazeInvalidArgException(PURCHASE_ORDER, PO_NOT_FOUND + " or purchase order's status is not InProgress");
        }

        return updatedPurchaseOrder;
    }

    /**
     * Override method to get po by id
     *
     * @param companyId       : companyId
     * @param shopId          : shopId
     * @param purchaseOrderId : purchaseOrderId
     * @return : purchaseOrder
     */
    @Override
    public PurchaseOrderItemResult getPurchaseOrderById(String companyId, String shopId, String purchaseOrderId) {

        PurchaseOrderItemResult purchaseOrder = purchaseOrderRepository.getPOById(companyId, purchaseOrderId);

        if (purchaseOrder == null) {
            throw new BlazeInvalidArgException(PURCHASE_ORDER, PO_NOT_FOUND);
        }

        LinkedHashSet<ObjectId> employeeIds = new LinkedHashSet<>();
        LinkedHashSet<ObjectId> vendorIds = new LinkedHashSet<>();
        LinkedHashSet<ObjectId> productsIds = new LinkedHashSet<>();

        this.getInfoForPurchaseOrder(purchaseOrder, employeeIds, vendorIds, productsIds);

        HashMap<String, Employee> employeeMap = employeeRepository.listAsMap(companyId, Lists.newArrayList(employeeIds));
        HashMap<String, Vendor> vendorMap = vendorRepository.listAsMap(companyId, Lists.newArrayList(vendorIds));
        HashMap<String, Product> productMap = productRepository.listAsMap(companyId, Lists.newArrayList(productsIds));

        preparePurchaseOrderResult(companyId, shopId, purchaseOrder, employeeMap, vendorMap, productMap);

        return purchaseOrder;
    }

    /**
     * Override method to get po by dates
     *
     * @param companyId : companyId
     * @param shopId    : shopId
     * @param startDate : startDate
     * @param endDate   : endDate
     * @param start     : start
     * @param limit     : limit
     * @return : purchase order result
     */
    @Override
    public SearchResult<PurchaseOrderItemResult> getAllPurchaseOrderByDates(String companyId, String shopId, long startDate, long endDate, int start, int limit) {
        DateSearchResult<PurchaseOrderItemResult> result = purchaseOrderRepository.findItemsWithDateAndLimit(companyId, shopId, startDate, endDate, start, limit, PurchaseOrderItemResult.class);
        if (result.getValues() != null) {

            HashMap<String, Object> preparedResult = this.getInfoForPurchaseOrderList(companyId, result.getValues());
            HashMap<String, Employee> employeeMap = (HashMap<String, Employee>) preparedResult.get("Employee");
            HashMap<String, Vendor> vendorMap = (HashMap<String, Vendor>) preparedResult.get("Vendor");
            HashMap<String, Product> productMap = (HashMap<String, Product>) preparedResult.get("Product");

            for (PurchaseOrderItemResult purchaseOrderItemResult : result.getValues()) {
                preparePurchaseOrderResult(companyId, shopId, purchaseOrderItemResult, employeeMap, vendorMap, productMap);
            }
        }
        return result;
    }

    /**
     * Private method to calculate arms length excise tax
     *
     * @param companyId            : companyId
     * @param shop                 : shopId
     * @param poProductRequestList : poProducyRequestList
     * @param productMap           : productMap
     * @param productCategoryIds   : productCategoryId
     * @return : Taxresult
     */
    private TaxResult calculateArmsLengthExciseTax(String companyId, Shop shop, List<POProductRequest> poProductRequestList, Map<String, Product> productMap, List<ObjectId> productCategoryIds) {

        ExciseTaxInfo exciseTaxInfo = getExciseTaxInfoForShop(shop);

        double totalExciseTax = 0.0;
        if (shop.isEnableExciseTax() && exciseTaxInfo != null) {
            double stateMarkUp = exciseTaxInfo.getStateMarkUp().doubleValue();
            stateMarkUp = 1 + (stateMarkUp / 100);
            double stateExciseTax = exciseTaxInfo.getExciseTax().doubleValue();

            HashMap<String, ProductCategory> categoryHashMap = productCategoryRepository.listAsMap(companyId, productCategoryIds);

            for (POProductRequest poProductRequest : poProductRequestList) {
                Product product = productMap.get(poProductRequest.getProductId());
                poProductRequest.setExciseTax(new BigDecimal(0));
                poProductRequest.setTotalExciseTax(new BigDecimal(0));
                if (product != null && categoryHashMap.containsKey(product.getCategoryId())) {
                    ProductCategory productCategory = categoryHashMap.get(product.getCategoryId());

                    if ((product.getCannabisType() == Product.CannabisType.DEFAULT && productCategory.isCannabis())
                            || (product.getCannabisType() != Product.CannabisType.CBD
                            && product.getCannabisType() != Product.CannabisType.NON_CANNABIS
                            && product.getCannabisType() != Product.CannabisType.DEFAULT)) {
                        double unitProductExciseTax = (poProductRequest.getUnitPrice().doubleValue() * stateMarkUp * stateExciseTax) / 100;

                        double quantity = poProductRequest.getRequestQuantity().doubleValue();

                        double productRequestTax = unitProductExciseTax * quantity;
                        totalExciseTax = totalExciseTax + productRequestTax;

                        poProductRequest.setExciseTax(new BigDecimal(unitProductExciseTax));
                        poProductRequest.setTotalExciseTax(new BigDecimal(productRequestTax));
                    }
                }
            }
        }

        TaxResult taxResult = new TaxResult();
        taxResult.prepare();

        taxResult.setTotalExciseTax(new BigDecimal(totalExciseTax));

        return taxResult;
    }

    /**
     * Private method to get excise tax info
     *
     * @param shop : shop
     * @return : ExciseTaxInfo
     */
    private ExciseTaxInfo getExciseTaxInfoForShop(Shop shop) {
        String state = "";
        String country = "";
        Address address = shop.getAddress();
        if (address != null) {
            country = address.getCountry();
            state = address.getState();
        }
        return exciseTaxInfoRepository.getExciseTaxInfoByState(state, country);
    }

    /* Helper Private Methods*/

    /**
     * Creates PoProductRequests from PoProductAddRequestList
     *
     * @param poProductAddRequestList : list of poProductAddRequest
     * @param poProductRequests       : list of poProductRequests
     */
    private Map<String, Object> createPOProductRequest(String companyId, String shopId, List<POProductAddRequest> poProductAddRequestList, List<POProductRequest> poProductRequests) {
        Map<String, Object> productReqResult = new HashMap<>();
        List<ObjectId> productCategoryList = new ArrayList<>();
        productReqResult.put("totalAmount", 0);
        productReqResult.put("products", new ArrayList<>());
        productReqResult.put("productCategoryList", productCategoryList);

        HashMap<String, Product> productMap = new HashMap<>();
        BigDecimal totalAmount = BigDecimal.ZERO;
        BigDecimal totalDiscount = BigDecimal.ZERO;
        for (POProductAddRequest poProductAddRequest : poProductAddRequestList) {
            Product product = null;

            if (!poProductAddRequest.getProductId().isEmpty()) {
                product = productRepository.get(companyId, poProductAddRequest.getProductId());
            }

            if (product != null) {
                productMap.put(product.getId(), product);
                productCategoryList.add(new ObjectId(product.getCategoryId()));

                POProductRequest poProductRequest = new POProductRequest();
                poProductRequest.prepare(companyId);
                poProductRequest.setShopId(shopId);
                poProductRequest.setNotes(poProductAddRequest.getNotes());
                poProductRequest.setProductId(poProductAddRequest.getProductId());
                poProductRequest.setProductName(product.getName());
                poProductRequest.setRequestQuantity(poProductAddRequest.getRequestQuantity());
                poProductRequest.setDiscount(poProductAddRequest.getDiscount());

                BigDecimal unitPrice = BigDecimal.ZERO;
                BigDecimal totalCost = BigDecimal.ZERO;
                BigDecimal finalTotalCost = BigDecimal.ZERO;

                if (poProductAddRequest.getTotalCost() != null) {
                    totalCost = poProductAddRequest.getTotalCost();
                }
                if (poProductAddRequest.getRequestQuantity() != null && !poProductAddRequest.getRequestQuantity().equals(BigDecimal.ZERO)) {
                    unitPrice = BigDecimal.valueOf(totalCost.doubleValue() / poProductAddRequest.getRequestQuantity().doubleValue());
                }
                if (poProductAddRequest.getDiscount() != null) {
                    finalTotalCost = totalCost.subtract(poProductAddRequest.getDiscount());
                    totalDiscount = totalDiscount.add(poProductAddRequest.getDiscount());
                }
                poProductRequest.setUnitPrice(unitPrice);
                poProductRequest.setTotalCost(totalCost);

                poProductRequest.setFinalTotalCost(finalTotalCost);
                totalAmount = totalAmount.add(finalTotalCost);

                poProductRequests.add(poProductRequest);
            } else {
                throw new BlazeInvalidArgException("Product", "Product does not exist for this shop.");
            }
        }

        productReqResult.put("totalAmount", totalAmount);
        productReqResult.put("products", productMap);
        productReqResult.put("productCategoryList", productCategoryList);
        productReqResult.put("totalDiscount", totalDiscount);
        return productReqResult;
    }

    /**
     * Private method to generate purchase order unique number
     *
     * @return : purchaseorder number
     */
    private long getNextSequence(String companyId) {
        return companyUniqueSequenceService.getNewIdentifier(companyId, PURCHASE_ORDER, purchaseOrderRepository.count(companyId));
    }

    /**
     * Private method to calculate oz/harvest/cultivation tax
     *
     * @param shop               : shop
     * @param purchaseOrder      : purchase order
     * @param productMap         : product map
     * @param productCategoryIds : product categoryId
     * @return : taxResult
     */
    private CultivationTaxResult calculateOzTax(String companyId, Shop shop, PurchaseOrder purchaseOrder, Map<String, Product> productMap, List<ObjectId> productCategoryIds) {
        HashMap<String, ProductCategory> categoryHashMap = productCategoryRepository.listAsMap(companyId, productCategoryIds);
        return taxCalulationService.calculateOzTax(companyId, shop.getId(), purchaseOrder, productMap, categoryHashMap, true);
    }

    /**
     * Private method to calculate delivery fees
     *
     * @param shop          : shop
     * @param purchaseOrder : purchaseOrder
     */
    private void applyDeliveryCharge(Shop shop, PurchaseOrder purchaseOrder) {

        if (shop == null || purchaseOrder == null) {
            return;
        }

        BigDecimal totalAmount = purchaseOrder.getTotalCost();
        if (totalAmount == null) {
            totalAmount = new BigDecimal(0);
        }

        List<DeliveryFee> deliveryFees = shop.getDeliveryFees();
        deliveryFees.sort(new DeliveryFee.DeliveryFeeComparator());

        for (DeliveryFee deliveryFee : deliveryFees) {
            boolean enabled = deliveryFee.isEnabled();

            if (enabled && totalAmount.compareTo(deliveryFee.getSubTotalThreshold()) >= 0) {
                purchaseOrder.setEnableDeliveryCharge(true);
                purchaseOrder.setDeliveryCharge(deliveryFee.getFee());
            }

        }
    }

    /**
     * Private method to process incoming product request
     *
     * @param companyId         : companyId
     * @param shopId            : shopId
     * @param poProductRequests : poProductRequest
     * @return: info map
     */
    private HashMap<String, BigDecimal> processIncomingProductRequest(String companyId, String shopId, List<POProductRequest> poProductRequests) {
        BigDecimal totalAmount = BigDecimal.ZERO;
        BigDecimal totalDiscount = BigDecimal.ZERO;

        for (POProductRequest poProductRequest : poProductRequests) {
            poProductRequest.prepare(companyId); // always prepare
            poProductRequest.setShopId(shopId);

            if (StringUtils.isBlank(poProductRequest.getProductId())) {
                throw new BlazeInvalidArgException("ProductRequest", "Please select a valid product.");
            }

            Product product = productRepository.get(companyId, poProductRequest.getProductId());
            if (product == null) {
                throw new BlazeInvalidArgException("Product", "Product does not exist.");
            }
            if (!product.getShopId().equalsIgnoreCase(shopId)) {
                throw new BlazeInvalidArgException("Product", "Please choose a product from the same shop.");
            }
            poProductRequest.setProductName(product.getName());

            BigDecimal unitPrice = BigDecimal.ZERO;
            BigDecimal totalCost = poProductRequest.getTotalCost();
            BigDecimal quantity = poProductRequest.getRequestQuantity();

            totalCost = (totalCost == null) ? BigDecimal.ZERO : totalCost;

            if (quantity.doubleValue() <= 0) {
                throw new BlazeInvalidArgException("Quantity", "Quantity should be greater than 0.");
            }
            if (totalCost != null && poProductRequest.getRequestQuantity() != null && !poProductRequest.getRequestQuantity().equals(BigDecimal.ZERO)) {
                unitPrice = BigDecimal.valueOf(totalCost.doubleValue() / quantity.doubleValue());
            }
            poProductRequest.setUnitPrice(unitPrice);
            poProductRequest.setTotalCost(totalCost);

            totalAmount = totalAmount.add(totalCost);
            totalDiscount = totalDiscount.add(poProductRequest.getDiscount());
        }
        HashMap<String, BigDecimal> returnMap = new HashMap<>();
        returnMap.put("totalAmount", totalAmount);
        returnMap.put("totalDiscount", totalDiscount);

        return returnMap;
    }

    /**
     * Private method to get info of purchase order
     *
     * @param purchaseOrder : purchaseOrder
     * @param employeeIds   : employeeIds
     * @param vendorIds     : vendorIds
     * @param productsIds   : productIds
     */
    private void getInfoForPurchaseOrder(PurchaseOrderItemResult purchaseOrder, LinkedHashSet<ObjectId> employeeIds, LinkedHashSet<ObjectId> vendorIds, LinkedHashSet<ObjectId> productsIds) {
        if (StringUtils.isNotBlank(purchaseOrder.getApprovedBy()) && ObjectId.isValid(purchaseOrder.getApprovedBy())) {
            employeeIds.add(new ObjectId(purchaseOrder.getApprovedBy()));
        }
        if (StringUtils.isNotBlank(purchaseOrder.getVendorId()) && ObjectId.isValid(purchaseOrder.getVendorId())) {
            vendorIds.add(new ObjectId(purchaseOrder.getVendorId()));
        }

        if (purchaseOrder.getPoProductRequestList() != null) {
            for (POProductRequest request : purchaseOrder.getPoProductRequestList()) {
                if (StringUtils.isNotBlank(request.getProductId()) && ObjectId.isValid(request.getProductId())) {
                    productsIds.add(new ObjectId(request.getProductId()));
                }
            }
        }
    }

    /**
     * Private method to prepare po result
     *
     * @param companyId   : companyId
     * @param shopId      : shopId
     * @param orderResult : result
     * @param employeeMap : employeeMap
     * @param vendorMap   : vendorMap
     * @param productMap  : productMap
     */
    private void preparePurchaseOrderResult(String companyId, String shopId, PurchaseOrderItemResult orderResult, HashMap<String, Employee> employeeMap, HashMap<String, Vendor> vendorMap, HashMap<String, Product> productMap) {

        List<POProductRequestResult> poProductRequestResultList = new ArrayList<>();
        if (!orderResult.getPoProductRequestList().isEmpty()) {
            Product product = null;
            POProductRequestResult requestResult;
            for (POProductRequest productRequest : orderResult.getPoProductRequestList()) {

                if (!productRequest.getProductId().isEmpty()) {
                    product = productMap.get(productRequest.getProductId());
                }

                if (product == null) {
                    continue;
                }
                requestResult = new POProductRequestResult();
                requestResult.setProduct(product);
                requestResult.setCompanyId(productRequest.getCompanyId());
                requestResult.setId(productRequest.getId());
                requestResult.setCreated(productRequest.getCreated());
                requestResult.setDeleted(productRequest.isDeleted());
                requestResult.setModified(productRequest.getModified());
                requestResult.setDirty(productRequest.isDirty());
                requestResult.setShopId(productRequest.getShopId());
                requestResult.setUpdated(productRequest.isUpdated());

                requestResult.setDeclineReason(productRequest.getDeclineReason());
                requestResult.setNotes(productRequest.getNotes());
                requestResult.setProductId(productRequest.getProductId());
                requestResult.setProductName(productRequest.getProductName());
                requestResult.setReceivedQuantity(productRequest.getReceivedQuantity());
                requestResult.setRequestQuantity(productRequest.getRequestQuantity());
                requestResult.setUnitPrice(productRequest.getUnitPrice());
                requestResult.setTrackTraceSystem(productRequest.getTrackTraceSystem());
                requestResult.setRequestStatus(productRequest.getRequestStatus());
                requestResult.setTotalCost(productRequest.getTotalCost());
                requestResult.setTrackingPackagesList(productRequest.getTrackingPackagesList());
                requestResult.setExciseTax(productRequest.getExciseTax());
                requestResult.setTotalExciseTax(productRequest.getTotalExciseTax());
                requestResult.setTotalCultivationTax(productRequest.getTotalCultivationTax());
                requestResult.setReceiveBatchStatus(productRequest.getReceiveBatchStatus());
                requestResult.setBatchAddDetails(productRequest.getBatchAddDetails());

                requestResult.setDiscount(productRequest.getDiscount());
                requestResult.setFinalTotalCost(productRequest.getFinalTotalCost());
                poProductRequestResultList.add(requestResult);

            }
        }

        orderResult.setPoProductRequestList(null);
        orderResult.setPoProductRequestResultList(poProductRequestResultList);
        SearchResult<POActivity> poActivitySearchResult = getAllPOActivityList(companyId, shopId, 0, Integer.MAX_VALUE, orderResult.getId());
        if (poActivitySearchResult != null && !poActivitySearchResult.getValues().isEmpty()) {
            orderResult.setPoActivityLog(poActivitySearchResult.getValues());
        }
        Company company = companyRepository.getById(orderResult.getCompanyId());
        String logoURL = company.getLogoURL() != null ? company.getLogoURL() : "https://connect-assets.s3.amazonaws.com/email/logo.jpg";
        orderResult.setCompanyLogo(logoURL);

        Vendor vendor = vendorMap.get(orderResult.getVendorId());
        orderResult.setVendor(vendor);

        Employee employee = null;

        if (orderResult.getApprovedBy() != null) {
            employee = employeeMap.get(orderResult.getApprovedBy());
        }

        orderResult.setApprovedByMember(employee);

    }

    /**
     * Private method to get po info
     *
     * @param companyId               : companyId
     * @param purchaseOrderItemResult : po list
     * @return: info map
     */
    private HashMap<String, Object> getInfoForPurchaseOrderList(String companyId, List<PurchaseOrderItemResult> purchaseOrderItemResult) {

        HashMap<String, Object> result = new HashMap<>();
        LinkedHashSet<ObjectId> employeeIds = new LinkedHashSet<>();
        LinkedHashSet<ObjectId> vendorIds = new LinkedHashSet<>();
        LinkedHashSet<ObjectId> productsIds = new LinkedHashSet<>();

        for (PurchaseOrderItemResult purchaseOrder : purchaseOrderItemResult) {

            this.getInfoForPurchaseOrder(purchaseOrder, employeeIds, vendorIds, productsIds);
        }

        HashMap<String, Employee> employeeMap = employeeRepository.listAsMap(companyId, Lists.newArrayList(employeeIds));
        HashMap<String, Vendor> vendorMap = vendorRepository.listAsMap(companyId, Lists.newArrayList(vendorIds));
        HashMap<String, Product> productMap = productRepository.listAsMap(companyId, Lists.newArrayList(productsIds));

        result.put("Employee", employeeMap);
        result.put("Vendor", vendorMap);
        result.put("Product", productMap);

        return result;
    }

    /**
     * Private method to add po activity
     *
     * @param companyId       : companyId
     * @param shopId          : shopId
     * @param purchaseOrderId : purchaseOrderId
     * @param employeeId      : employeeId
     * @param log             : log message
     * @return : PoActivity
     */
    private POActivity addPOActivity(String companyId, String shopId, String purchaseOrderId, String employeeId, String log) {
        if (StringUtils.isNotBlank(purchaseOrderId)) {
            POActivity poActivity = new POActivity();
            poActivity.prepare(companyId);
            poActivity.setShopId(shopId);
            poActivity.setEmployeeId(employeeId);
            poActivity.setLog(log);
            poActivity.setPurchaseOrderId(purchaseOrderId);
            poActivity.setActivityType(POActivity.ActivityType.NORMAL_LOG);
            return poActivityRepository.save(poActivity);
        }

        return null;
    }

    /**
     * This method gets list of POActivity
     *
     * @param start           : start of search
     * @param limit           : limit of search
     * @param purchaseOrderId : purchase order id from which PO activity needs to be find
     * @return List of PO activity by purchase order id (PO id)
     */
    private SearchResult<POActivity> getAllPOActivityList(String companyId, String shopId, int start, int limit, String purchaseOrderId) {
        if (StringUtils.isBlank(purchaseOrderId)) {
            throw new BlazeInvalidArgException(ACTIVITY_LOG, "Purchase order cannot be blank");
        }
        if (limit == 0) {
            limit = Integer.MAX_VALUE;
        }
        return poActivityRepository.findItemsByPurchaseOrderId(companyId, shopId, start, limit, "{created:1}", purchaseOrderId);
    }
}
