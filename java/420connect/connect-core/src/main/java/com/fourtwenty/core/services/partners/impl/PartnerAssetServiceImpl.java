package com.fourtwenty.core.services.partners.impl;

import com.fourtwenty.core.config.pdf.PdfGenerator;
import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.models.generic.Asset;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.domain.repositories.dispensary.CompanyAssetRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductCategoryRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.exceptions.BlazeAuthException;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.rest.dispensary.results.common.AssetStreamResult;
import com.fourtwenty.core.rest.dispensary.results.common.UploadFileResult;
import com.fourtwenty.core.security.tokens.StoreAuthToken;
import com.fourtwenty.core.services.AbstractStoreServiceImpl;
import com.fourtwenty.core.services.imaging.ImageProcessorService;
import com.fourtwenty.core.services.partners.PartnerAssetService;
import com.fourtwenty.core.services.thirdparty.AmazonS3Service;
import com.google.inject.Inject;
import com.google.inject.Provider;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

public class PartnerAssetServiceImpl extends AbstractStoreServiceImpl implements PartnerAssetService {
    
    private static final Log LOG = LogFactory.getLog(PartnerAssetServiceImpl.class);

    @Inject
    private AmazonS3Service amazonS3Service;
    @Inject
    private ImageProcessorService imageProcessorService;
    @Inject
    private CompanyAssetRepository companyAssetRepository;
    @Inject
    private ProductCategoryRepository categoryRepository;
    @Inject
    private ProductRepository productRepository;

    @Inject
    public PartnerAssetServiceImpl(Provider<StoreAuthToken> tokenProvider) {
        super(tokenProvider);
    }

    @Override
    public CompanyAsset uploadAssetPrivate(InputStream inputStream, String name, CompanyAsset.AssetType assetType, String mimeType) {
        if (inputStream == null || StringUtils.isBlank(name)) {
            throw new BlazeInvalidArgException("Form", "Invalid name or stream");
        }

        String extension = getAssetExtension(name, assetType, mimeType);

        try {
            File file = stream2file(inputStream, extension);

            String keyName = UUID.randomUUID().toString();
            String key = amazonS3Service.uploadFile(file, keyName, extension);

            CompanyAsset asset = new CompanyAsset();
            preparePrivateImageUrl(asset, extension, file, keyName);

            asset.prepare(storeToken.getCompanyId());
            asset.setName(name);
            asset.setKey(key);
            asset.setActive(true);
            asset.setType(assetType);
            companyAssetRepository.save(asset);
            return asset;
        } catch (IOException e) {
            e.printStackTrace();
            throw new BlazeInvalidArgException("Blaze", "Cannot upload file.");
        }
    }

    @Override
    public AssetStreamResult getAssetStream(String assetKey, boolean handleError) {
        AssetStreamResult result = null;
        try {
            result = getCompanyAssetStream(assetKey);

        } catch (Exception e) {
            LOG.error("Error while getting asset :" + assetKey + "");
        }
        if (handleError && result == null) {
            byte[] noDocumentPdf = PdfGenerator.getNoDocumentPdf();
            result = new AssetStreamResult();
            result.setStream(noDocumentPdf != null ? new ByteArrayInputStream(noDocumentPdf) : null);
            result.setContentType("application/pdf");
            result.setKey(assetKey);
            CompanyAsset asset = new CompanyAsset();
            asset.setType(Asset.AssetType.Document);
            result.setAsset(asset);
        } else if (result == null) {
            throw new BlazeInvalidArgException("CompanyAsset", "Invalid asset");
        }

        return result;
    }

    private AssetStreamResult getCompanyAssetStream(String assetKey) {
        if (assetKey != null && assetKey.startsWith("420default-")) {
            AssetStreamResult result = amazonS3Service.downloadFile(assetKey, true);
            if (result != null) {
                result.setAsset(null);
                return result;
            }
            throw new BlazeInvalidArgException("Asset", "Invalid asset.");
        }

        String companyId = null;
        if (storeToken != null && storeToken.isValid() && StringUtils.isNotEmpty(storeToken.getCompanyId())) {
            companyId = storeToken.getCompanyId();
        }

        if (companyId == null) {
            throw new BlazeAuthException("Authorization", "Invalid companyId");
        }

        CompanyAsset asset = companyAssetRepository.getAssetByKey(companyId, assetKey);
        if (asset == null) {
            // Find by category
            ProductCategory productCategory = categoryRepository.getCategoryByPhotoKey(companyId, assetKey);
            asset = (productCategory != null) ? productCategory.getPhoto() : null;

            if (asset == null) {
                Product product = productRepository.findProductWithAssetKey(companyId, assetKey);
                if (product != null) {
                    for (CompanyAsset companyAsset : product.getAssets()) {
                        if (companyAsset.getKey().equalsIgnoreCase(assetKey)) {
                            asset = companyAsset;
                            break;
                        }
                    }
                }
            }
        }

        if (asset != null) {
            LOG.info("Accessing image: " + asset.getKey() + " secured: " + asset.getKey() + " purl: " + asset.getPublicURL());
            if (asset.getPublicURL() != null && asset.getPublicURL().contains("420default-")) {
                asset.setKey("420default-" + asset.getName());
            }
        } else {
            throw new BlazeInvalidArgException("CompanyAsset", "Invalid asset");

        }

        AssetStreamResult result = amazonS3Service.downloadFile(assetKey, asset.isSecured());
        result.setAsset(asset);
        return result;
    }

    private void preparePrivateImageUrl(CompanyAsset asset, String extension, File file, String keyName) {
        if (extension.equalsIgnoreCase(".jpg") ||
                extension.equalsIgnoreCase(".jpeg") ||
                extension.equalsIgnoreCase(".png")) {

            ImageProcessorService.ImageProcessingResult imageProcessingResult = imageProcessorService.processImage(file, file.getName());

            if (imageProcessingResult != null) {

                if (imageProcessingResult.small != null) {
                    UploadFileResult resizeResult = amazonS3Service.uploadFilePrivate(imageProcessingResult.small.inputStream,
                            imageProcessingResult.small.objectMetadata,
                            keyName + "-" + imageProcessingResult.small.size, extension);
                    if (resizeResult != null) {
                        asset.setThumbURL(resizeResult.getUrl());
                    }
                }

                if (imageProcessingResult.medium != null) {
                    UploadFileResult resizeResult = amazonS3Service.uploadFilePrivate(imageProcessingResult.medium.inputStream,
                            imageProcessingResult.medium.objectMetadata,
                            keyName + "-" + imageProcessingResult.medium.size, extension);
                    if (resizeResult != null) {
                        asset.setMediumURL(resizeResult.getUrl());
                    }
                }

                if (imageProcessingResult.large != null) {
                    UploadFileResult resizeResult = amazonS3Service.uploadFilePrivate(imageProcessingResult.large.inputStream,
                            imageProcessingResult.large.objectMetadata,
                            keyName + "-" + imageProcessingResult.large.size, extension);

                    if (resizeResult != null) {
                        asset.setLargeURL(resizeResult.getUrl());
                    }
                }

                if (imageProcessingResult.largeX2 != null) {
                    UploadFileResult resizeResult = amazonS3Service.uploadFilePrivate(imageProcessingResult.largeX2.inputStream,
                            imageProcessingResult.largeX2.objectMetadata,
                            keyName + "-" + imageProcessingResult.largeX2.size, extension);
                    if (resizeResult != null) {
                        asset.setLargeX2URL(resizeResult.getUrl());
                    }
                }
            }
        }
    }

    private String getAssetExtension(String name, CompanyAsset.AssetType assetType, String mimeType) {
        String extension = FilenameUtils.getExtension(name); // returns "exe"
        if (StringUtils.isBlank(extension) && assetType == Asset.AssetType.Document) {
            extension = ".pdf";
        } else if (StringUtils.isBlank(extension) && assetType == Asset.AssetType.Photo) {
            extension = ".jpeg";
            if (StringUtils.isNotBlank(mimeType) && mimeType.equalsIgnoreCase("image/png")) {
                extension = ".png";
            }
        } else {
            extension = "." + extension;
        }
        return extension;
    }

    private static File stream2file(InputStream in, String extension) throws IOException {
        final File tempFile = File.createTempFile(UUID.randomUUID().toString(), extension);
        tempFile.deleteOnExit();
        try (FileOutputStream out = new FileOutputStream(tempFile)) {
            IOUtils.copy(in, out);
        }
        return tempFile;
    }

}
