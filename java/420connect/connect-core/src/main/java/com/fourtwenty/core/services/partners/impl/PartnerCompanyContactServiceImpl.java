package com.fourtwenty.core.services.partners.impl;

import com.fourtwenty.core.domain.models.company.CompanyContact;
import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.repositories.dispensary.EmployeeRepository;
import com.fourtwenty.core.rest.dispensary.requests.partners.PartnerCompanyContactRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.company.CompanyContactResult;
import com.fourtwenty.core.security.tokens.StoreAuthToken;
import com.fourtwenty.core.services.AbstractStoreServiceImpl;
import com.fourtwenty.core.services.mgmt.CommonCompanyContactService;
import com.fourtwenty.core.services.partners.PartnerCompanyContactService;
import com.google.inject.Inject;
import com.google.inject.Provider;
import org.apache.commons.lang3.StringUtils;

public class PartnerCompanyContactServiceImpl extends AbstractStoreServiceImpl implements PartnerCompanyContactService {

    @Inject
    private EmployeeRepository employeeRepository;
    @Inject
    private CommonCompanyContactService commonCompanyContactService;

    @Inject
    public PartnerCompanyContactServiceImpl(Provider<StoreAuthToken> tokenProvider) {
        super(tokenProvider);
    }

    @Override
    public CompanyContact createCompanyContact(PartnerCompanyContactRequest companyContact) {
        String employeeName = this.getEmployeeInfo(companyContact.getEmployeeId());
        return commonCompanyContactService.createCompanyContact(storeToken.getCompanyId(), storeToken.getShopId(), companyContact, companyContact.getEmployeeId(), employeeName);
    }

    @Override
    public CompanyContact updateCompanyContact(String contactId, PartnerCompanyContactRequest companyContact) {
        String employeeName = this.getEmployeeInfo(companyContact.getEmployeeId());
        return commonCompanyContactService.updateCompanyContact(storeToken.getCompanyId(), storeToken.getShopId(), contactId, companyContact, companyContact.getEmployeeId(), employeeName);
    }

    @Override
    public CompanyContactResult getCompanyContactById(String contactId) {
        return commonCompanyContactService.getCompanyContactById(storeToken.getCompanyId(), contactId);
    }

    @Override
    public SearchResult<CompanyContact> getCompanyContacts(String startDate, String endDate, int skip, int limit) {
        limit = (limit <= 0 || limit > 100) ? 100 : limit;
        skip = (skip <= 0) ? 0 : skip;
        return commonCompanyContactService.getCompanyContacts(storeToken.getCompanyId(), storeToken.getShopId(), startDate, endDate, skip, limit);
    }

    private String getEmployeeInfo(String employeeId) {
        Employee employee = employeeRepository.get(storeToken.getCompanyId(), employeeId);
        String employeeName = StringUtils.EMPTY;
        if (employee != null) {
            employeeName = employee.getFirstName() + " " + employee.getLastName();
        }
        return employeeName;
    }
}
