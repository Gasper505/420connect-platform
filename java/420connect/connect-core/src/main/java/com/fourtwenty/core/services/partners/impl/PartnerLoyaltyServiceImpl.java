package com.fourtwenty.core.services.partners.impl;

import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.loyalty.LoyaltyReward;
import com.fourtwenty.core.domain.models.loyalty.Promotion;
import com.fourtwenty.core.domain.models.loyalty.PromotionRule;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.repositories.dispensary.MemberRepository;
import com.fourtwenty.core.domain.repositories.dispensary.PromotionRepository;
import com.fourtwenty.core.domain.repositories.loyalty.LoyaltyRewardRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.rest.dispensary.requests.promotions.PartnerAddPromotionRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.company.LoyaltyMemberReward;
import com.fourtwenty.core.security.tokens.StoreAuthToken;
import com.fourtwenty.core.services.AbstractStoreServiceImpl;
import com.fourtwenty.core.services.common.RealtimeService;
import com.fourtwenty.core.services.partners.PartnerLoyaltyService;
import com.fourtwenty.core.util.TextUtil;
import com.google.common.collect.Sets;
import com.google.inject.Inject;
import com.google.inject.Provider;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

public class PartnerLoyaltyServiceImpl extends AbstractStoreServiceImpl implements PartnerLoyaltyService {

    @Inject
    LoyaltyRewardRepository rewardRepository;
    @Inject
    private PromotionRepository promotionRepository;
    @Inject
    private RealtimeService realtimeService;
    @Inject
    private MemberRepository memberRepository;

    @Inject
    public PartnerLoyaltyServiceImpl(Provider<StoreAuthToken> tokenProvider) {
        super(tokenProvider);
    }

    @Override
    public SearchResult<LoyaltyReward> getRewards(int start, int limit) {
        if (limit >= 200 || limit <= 0) {
            limit = 200;
        }
        return rewardRepository.findItems(storeToken.getCompanyId(), storeToken.getShopId(), "{points:1}", start, limit);
    }


    @Override
    public Promotion findPromotion(String promoCode) {
        if (StringUtils.isBlank(promoCode)) {
            throw new BlazeInvalidArgException("NAME", "No promotion exists with this promo code.");
        }
        return promotionRepository.getPromotionByPromoCode(storeToken.getCompanyId(),storeToken.getShopId(),promoCode);
    }

    /**
     * Override method to add promotions
     *
     * @param promotion : request
     * @return : promotion
     */
    @Override
    public Promotion addPromotion(PartnerAddPromotionRequest promotion) {

        Promotion dbPromo = new Promotion();
        dbPromo.prepare(storeToken.getCompanyId());
        dbPromo.setShopId(storeToken.getShopId());
        if (StringUtils.isNotBlank(promotion.getName())) {
            Promotion promotionByName = promotionRepository.getPromotionByName(storeToken.getCompanyId(), storeToken.getShopId(), promotion.getName());
            if (promotionByName != null) {
                throw new BlazeInvalidArgException("NAME", "A promotion with this name already exists");
            }
        }

        if (promotion.getPromoCodes() == null) {
            promotion.setPromoCodes(Sets.newHashSet());
        }

        // clean incoming promocodes
        HashSet<String> promoCodes = Sets.newHashSet();

        for (String promoCode : promotion.getPromoCodes()) {
            if (promoCode != null) {
                promoCodes.add(promoCode.trim());
            }
        }
        promotion.setPromoCodes(promoCodes);

        if (!promotion.getPromoCodes().isEmpty()) {
            List<Promotion> promotionList = promotionRepository.getPromotionByPromoCode(storeToken.getCompanyId(), storeToken.getShopId(), promotion.getPromoCodes())
                    .stream()
                    .collect(Collectors.toList());
            if (!promotionList.isEmpty() && promotionList.stream().anyMatch(p -> !Collections.disjoint(promotion.getPromoCodes(), p.getPromoCodes()))) {
                throw new BlazeInvalidArgException("Promotion", "Promo code is in used by another promotion.");
            }
        }


        dbPromo.setName(promotion.getName());
        dbPromo.setActive(promotion.isActive());
        if (promotion.getStartDate() == null) {
            dbPromo.setStartDate(0L);
        } else {
            dbPromo.setStartDate(promotion.getStartDate());
        }
        if (promotion.getEndDate() == null) {
            dbPromo.setEndDate(0L);
        } else {
            dbPromo.setEndDate(promotion.getEndDate());
        }
        dbPromo.setPromoDesc(promotion.getPromoDesc());
        dbPromo.setConsumerFacing(promotion.isConsumerFacing());
        dbPromo.setDiscountAmt(promotion.getDiscountAmt());
        dbPromo.setDiscountType(promotion.getDiscountType());
        dbPromo.setPromoCodeRequired(promotion.isPromoCodeRequired());
        dbPromo.setPromoCodes(promotion.getPromoCodes());
        dbPromo.setRules(promotion.getRules());
        dbPromo.setMon(promotion.isMon());
        dbPromo.setTues(promotion.isTues());
        dbPromo.setWed(promotion.isWed());
        dbPromo.setThur(promotion.isThur());
        dbPromo.setFri(promotion.isFri());
        dbPromo.setSat(promotion.isSat());
        dbPromo.setSun(promotion.isSun());
        dbPromo.setPromotionType(promotion.getPromotionType());
        dbPromo.setDiscountUnitType(promotion.getDiscountUnitType());
        dbPromo.setStackable(promotion.isStackable());

        dbPromo.setEnableBOGO(promotion.isEnableBOGO());

        dbPromo.setEnableDayDuration(promotion.isEnableDayDuration());

        if (promotion.isEnableDayDuration()) {
            dbPromo.setDayStartTime(promotion.getDayStartTime());
            dbPromo.setDayEndTime(promotion.getDayEndTime());
        } else {
            dbPromo.setDayStartTime(0);
            dbPromo.setDayEndTime(0);
        }

        dbPromo.setPromotionType(promotion.getPromotionType());
        dbPromo.setEnableMaxAvailable(promotion.isEnableMaxAvailable());
        dbPromo.setMaxAvailable(promotion.getMaxAvailable());
        dbPromo.setMaxCashValue(promotion.getMaxCashValue());

        dbPromo.setEnableLimitPerCustomer(promotion.isEnableLimitPerCustomer());
        dbPromo.setLimitPerCustomer(promotion.getLimitPerCustomer());

        dbPromo.setRestrictMemberGroups(promotion.isRestrictMemberGroups());
        dbPromo.setMemberGroupIds(promotion.getMemberGroupIds());
        dbPromo.setScalable(promotion.isScalable());

        if (dbPromo.getMemberGroupIds() == null) {
            dbPromo.setMemberGroupIds(new ArrayList<String>());
        }

        if (dbPromo.getRules() == null) {
            dbPromo.setRules(new ArrayList<PromotionRule>());
        }
        for (PromotionRule criteria : dbPromo.getRules()) {
            criteria.prepare(storeToken.getCompanyId());
            //BACKWARD COMPATIBILITY
            if (criteria.getProductIds() != null && StringUtils.isNotBlank(criteria.getProductId())) {
                criteria.getProductIds().add(criteria.getProductId());
            }

            if (criteria.getCategoryIds() != null && StringUtils.isNotBlank(criteria.getCategoryId())) {
                criteria.getCategoryIds().add(criteria.getCategoryId());
            }

            if (criteria.getVendorIds() != null && StringUtils.isNotBlank(criteria.getVendorId())) {
                criteria.getVendorIds().add(criteria.getVendorId());
            }
        }


        promotionRepository.save(dbPromo);
        realtimeService.sendRealTimeEvent(storeToken.getShopId(), RealtimeService.RealtimeEventType.PromotionUpdateEvent, null);
        return dbPromo;
    }

    @Override
    public Promotion updatePromotion(String promotionId, Promotion promotion) {
        Promotion dbPromo = promotionRepository.get(storeToken.getCompanyId(), promotionId);

        if (dbPromo == null) {
            throw new BlazeInvalidArgException("Promotion", "Promotion not found.");
        }

        if (!dbPromo.getName().equals(promotion.getName())) {
            Promotion promotionByName = promotionRepository.getPromotionByName(storeToken.getCompanyId(), storeToken.getShopId(), promotion.getName());
            if (promotionByName != null) {
                throw new BlazeInvalidArgException("NAME", "Promotion is already exist with this name");
            }
        }

        if (promotion.getPromoCodes() == null) {
            promotion.setPromoCodes(Sets.newHashSet());
        }

        // clean incoming promocodes
        HashSet<String> promoCodes = Sets.newHashSet();

        for (String promoCode : promotion.getPromoCodes()) {
            if (promoCode != null) {
                promoCodes.add(promoCode.trim());
            }
        }
        promotion.setPromoCodes(promoCodes);

        if (!promotion.getPromoCodes().isEmpty()) {
            List<Promotion> promotionList = promotionRepository.getPromotionByPromoCode(storeToken.getCompanyId(), storeToken.getShopId(), promotion.getPromoCodes())
                    .stream()
                    .filter(p -> !promotionId.equalsIgnoreCase(p.getId()))
                    .collect(Collectors.toList());
            if (!promotionList.isEmpty() && promotionList.stream().anyMatch(p -> !Collections.disjoint(promotion.getPromoCodes(), p.getPromoCodes()))) {
                throw new BlazeInvalidArgException("Promotion", "Promo code is in used by another promotion.");
            }
        }


        dbPromo.setName(promotion.getName());
        dbPromo.setActive(promotion.isActive());
        if (promotion.getStartDate() == null) {
            dbPromo.setStartDate(0L);
        } else {
            dbPromo.setStartDate(promotion.getStartDate());
        }
        if (promotion.getEndDate() == null) {
            dbPromo.setEndDate(0L);
        } else {
            dbPromo.setEndDate(promotion.getEndDate());
        }
        dbPromo.setPromoDesc(promotion.getPromoDesc());
        dbPromo.setConsumerFacing(promotion.isConsumerFacing());
        dbPromo.setDiscountAmt(promotion.getDiscountAmt());
        dbPromo.setDiscountType(promotion.getDiscountType());
        dbPromo.setPromoCodeRequired(promotion.isPromoCodeRequired());
        dbPromo.setPromoCodes(promotion.getPromoCodes());
        dbPromo.setRules(promotion.getRules());
        dbPromo.setMon(promotion.isMon());
        dbPromo.setTues(promotion.isTues());
        dbPromo.setWed(promotion.isWed());
        dbPromo.setThur(promotion.isThur());
        dbPromo.setFri(promotion.isFri());
        dbPromo.setSat(promotion.isSat());
        dbPromo.setSun(promotion.isSun());
        dbPromo.setPromotionType(promotion.getPromotionType());
        dbPromo.setDiscountUnitType(promotion.getDiscountUnitType());
        dbPromo.setStackable(promotion.isStackable());
        dbPromo.setLowestPriceFirst(promotion.isLowestPriceFirst());
        dbPromo.setEnableBOGO(promotion.isEnableBOGO());

        dbPromo.setEnableDayDuration(promotion.isEnableDayDuration());

        if (promotion.isEnableDayDuration()) {
            dbPromo.setDayStartTime(promotion.getDayStartTime());
            dbPromo.setDayEndTime(promotion.getDayEndTime());
        } else {
            dbPromo.setDayStartTime(0);
            dbPromo.setDayEndTime(0);
        }

        dbPromo.setPromotionType(promotion.getPromotionType());
        dbPromo.setEnableMaxAvailable(promotion.isEnableMaxAvailable());
        dbPromo.setMaxAvailable(promotion.getMaxAvailable());
        dbPromo.setMaxCashValue(promotion.getMaxCashValue());

        dbPromo.setEnableLimitPerCustomer(promotion.isEnableLimitPerCustomer());
        dbPromo.setLimitPerCustomer(promotion.getLimitPerCustomer());

        dbPromo.setRestrictMemberGroups(promotion.isRestrictMemberGroups());
        dbPromo.setMemberGroupIds(promotion.getMemberGroupIds());
        dbPromo.setScalable(promotion.isScalable());

        if (dbPromo.getMemberGroupIds() == null) {
            dbPromo.setMemberGroupIds(new ArrayList<String>());
        }

        if (dbPromo.getRules() == null) {
            dbPromo.setRules(new ArrayList<PromotionRule>());
        }
        for (PromotionRule criteria : dbPromo.getRules()) {
            criteria.prepare(storeToken.getCompanyId());
            //BACKWARD COMPATIBILITY
            if (criteria.getProductIds() != null && StringUtils.isNotBlank(criteria.getProductId())) {
                criteria.getProductIds().add(criteria.getProductId());
            }

            if (criteria.getCategoryIds() != null && StringUtils.isNotBlank(criteria.getCategoryId())) {
                criteria.getCategoryIds().add(criteria.getCategoryId());
            }

            if (criteria.getVendorIds() != null && StringUtils.isNotBlank(criteria.getVendorId())) {
                criteria.getVendorIds().add(criteria.getVendorId());
            }
        }

        promotionRepository.update(storeToken.getCompanyId(), promotionId, dbPromo);
        realtimeService.sendRealTimeEvent(storeToken.getShopId(), RealtimeService.RealtimeEventType.PromotionUpdateEvent, null);
        return dbPromo;
    }

    @Override
    public void deletePromotion(String promotionId) {

        promotionRepository.removeByIdSetState(storeToken.getCompanyId(), promotionId);
        realtimeService.sendRealTimeEvent(storeToken.getShopId().toString(), RealtimeService.RealtimeEventType.PromotionUpdateEvent, null);
    }

    /**
     * Override method to get promotion list.
     *
     * @param start : start
     * @param limit : limit
     * @return : promotion list
     */
    @Override
    public SearchResult<Promotion> getPromotions(int start, int limit) {
        start = start < 0 ? 0 : start;
        limit = (limit <= 0 || limit > 200) ? 200 : limit;
        return promotionRepository.findItems(storeToken.getCompanyId(), storeToken.getShopId(), "{name:1}", start, limit);
    }

    @Override
    public SearchResult<LoyaltyMemberReward> getRewardsForMembers(String memberId) {
        Member member = memberRepository.get(storeToken.getCompanyId(), memberId);
        return getRewardsForMembers(storeToken.getCompanyId(), storeToken.getShopId(), member);
    }

    private SearchResult<LoyaltyMemberReward> getRewardsForMembers(String companyId, String shopId, Member member) {
        Iterable<LoyaltyReward> rewards = rewardRepository.getPublishedRewards(companyId, shopId);

        SearchResult<LoyaltyMemberReward> memberRewards = new SearchResult<>();

        for (LoyaltyReward reward : rewards) {
            if (reward.isDeleted()) {
                continue;
            }
            if (member != null && reward.getPoints() > member.getLoyaltyPoints().doubleValue()) {
               continue;
            }

            LoyaltyMemberReward memberReward = new LoyaltyMemberReward();

            memberReward.setRewardId(reward.getId());
            memberReward.setRewardName(reward.getName());
            memberReward.setRewardDescription(reward.getDetail());
            memberReward.setAvailableDate(reward.getStartDate());
            memberReward.setPointsRequired(reward.getPoints());
            memberReward.setRewardState(LoyaltyMemberReward.MemberRewardState.Locked);
            if (member != null) {
                memberReward.setMemberId(member.getId());
                if (reward.getPoints() <= member.getLoyaltyPoints().doubleValue()) {
                    memberReward.setRewardState(LoyaltyMemberReward.MemberRewardState.Available);
                }
            }

            memberRewards.getValues().add(memberReward);

            // Configure discount
            String discountInfo = "";
            if (reward.getDiscountType() == OrderItem.DiscountType.Cash) {
                discountInfo = TextUtil.toCurrency(reward.getDiscountAmt().doubleValue(), null) + " off";
            } else {
                // handle percentage%
                discountInfo = String.format("%.2f%% off %s", reward.getDiscountAmt().doubleValue(), reward.getDiscountUnitType().weightName);
            }

            memberReward.setDiscountInfo(discountInfo);
        }
        memberRewards.setTotal((long) memberRewards.getValues().size());

        return memberRewards;
    }
}
