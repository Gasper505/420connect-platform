package com.fourtwenty.core.services.partners.impl;

import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.models.company.MemberGroup;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.company.SignedContract;
import com.fourtwenty.core.domain.models.customer.*;
import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.models.generic.Note;
import com.fourtwenty.core.domain.models.global.StateCannabisLimit;
import com.fourtwenty.core.domain.models.partner.PartnerWebHook;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.managed.ElasticSearchManager;
import com.fourtwenty.core.managed.PartnerWebHookManager;
import com.fourtwenty.core.rest.consumer.results.MemberLimit;
import com.fourtwenty.core.rest.dispensary.requests.company.MembershipAddRequest;
import com.fourtwenty.core.rest.dispensary.requests.company.MembershipUpdateRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.company.MemberResult;
import com.fourtwenty.core.rest.store.webhooks.MemberData;
import com.fourtwenty.core.security.tokens.StoreAuthToken;
import com.fourtwenty.core.services.AbstractStoreServiceImpl;
import com.fourtwenty.core.services.common.RealtimeService;
import com.fourtwenty.core.services.global.CannabisLimitService;
import com.fourtwenty.core.services.mgmt.MemberActivityService;
import com.fourtwenty.core.services.partners.PartnerMemberService;
import com.fourtwenty.core.util.EntityFieldUtil;
import com.google.inject.Inject;
import com.google.inject.Provider;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.*;

public class PartnerMemberServiceImpl extends AbstractStoreServiceImpl implements PartnerMemberService {
    private static final Logger LOG = LoggerFactory.getLogger(PartnerMemberServiceImpl.class);
    private static final String MEMBER = "Member";
    private static final String MEMBER_NOT_FOUND = "Member doesn't exist with this id";

    @Inject
    MemberRepository memberRepository;
    @Inject
    CareGiverRepository careGiverRepository;
    @Inject
    MemberGroupRepository memberGroupRepository;
    @Inject
    ShopRepository shopRepository;
    @Inject
    private PartnerWebHookManager partnerWebHookManager;
    @Inject
    private MemberActivityService memberActivityService;
    @Inject
    private ElasticSearchManager elasticSearchManager;
    @Inject
    RealtimeService realtimeService;
    @Inject
    CompanyAssetRepository companyAssetRepository;
    @Inject
    DoctorRepository doctorRepository;
    @Inject
    EmployeeRepository employeeRepository;
    @Inject
    private CannabisLimitService cannabisLimitService;

    @Inject
    public PartnerMemberServiceImpl(Provider<StoreAuthToken> tokenProvider) {
        super(tokenProvider);
    }


    @Override
    public Member getMemberById(String memberId) {
        if (memberId == null) {
            throw new BlazeInvalidArgException("MembershipId", "Member Id cannot be null");
        }
        MemberResult member = memberRepository.get(storeToken.getCompanyId(), memberId, MemberResult.class);
        return member;
    }

    @Override
    public Member getMemberByEmail(String email) {
        Iterable<Member> members = memberRepository.getMemberByEmail(storeToken.getCompanyId(), email);
        for (Member m : members) {
            return m;
        }
        return null;
    }

    @Override
    public Member addMember(MembershipAddRequest addRequest) {
        if (StringUtils.isNotEmpty(addRequest.getEmail())) {
            long count = 0;
            count = memberRepository.countMemberByEmail(storeToken.getCompanyId(), addRequest.getEmail().toLowerCase());

            if (count > 0) {
                throw new BlazeInvalidArgException("MembershipAddRequest", "Member with this email already exist for this shop.");
            }
        }

        Member member = addOrUpdateMember(null, addRequest, null);

        callWebHook(member, false);
        return member;
    }


    @Override
    public Member updateMembership(String memberId, MembershipUpdateRequest member) {
        if (memberId == null || !ObjectId.isValid(memberId)) {
            throw new BlazeInvalidArgException("MemberId", "Invalid member id");
        }

        Shop shop = shopRepository.get(storeToken.getCompanyId(), storeToken.getShopId());
        if (shop == null) {
            throw new BlazeInvalidArgException("Shop", "Shop does not found");
        }


        Member dbMember = memberRepository.get(storeToken.getCompanyId(), memberId);
        if (dbMember == null) {
            throw new BlazeInvalidArgException("Member", "Member doesn't exist with this id");
        }

        member.setId(memberId);
        if (member.getNotes() != null) {
            for (Note note : member.getNotes()) {
                if (note.getId() == null) {
                    note.prepare();
                    note.setWriterName("Partner API");
                }
            }
        }

        prepareDataToSave(member);


        HashMap<String, MemberGroup> memberGroupHashMap = new HashMap<>();
        Iterable<MemberGroup> groups = memberGroupRepository.list(storeToken.getCompanyId());
        MemberGroup defaultGroup = null;
        for (MemberGroup memberGroup : groups) {
            memberGroupHashMap.put(memberGroup.getId(), memberGroup);
            if (memberGroup.isDefaultGroup()) {
                defaultGroup = memberGroup;
            }
        }


        MemberGroup memberGroup = memberGroupHashMap.get(member.getMemberGroupId());
        if (memberGroup != null) {
            dbMember.setMemberGroupId(memberGroup.getId());
            dbMember.setMemberGroup(memberGroup);
        } else if (defaultGroup != null) {
            dbMember.setMemberGroupId(defaultGroup.getId());
            dbMember.setMemberGroup(defaultGroup);
        }

        if (dbMember.getAddresses() != null) {
            for (Address address : dbMember.getAddresses()) {
                address.prepare(storeToken.getCompanyId());
            }

        }

        dbMember.setFirstName(member.getFirstName());
        dbMember.setLastName(member.getLastName());
        dbMember.setMiddleName(member.getMiddleName());
        dbMember.setAddress(member.getAddress());
        dbMember.setNotes(member.getNotes());
        dbMember.setStatus(member.getStatus());
        dbMember.setContracts(member.getContracts());
        dbMember.setIdentifications(member.getIdentifications());
        dbMember.setPreferences(member.getPreferences());
        dbMember.setRecommendations(member.getRecommendations());
        dbMember.setDob(member.getDob());
        dbMember.setEmail(member.getEmail());
        dbMember.setEmailOptIn(member.isEmailOptIn());
        dbMember.setTextOptIn(member.isTextOptIn());
        dbMember.setMedical(member.isMedical());
        dbMember.setPrimaryPhone(member.getPrimaryPhone());
        dbMember.setSex(member.getSex());
        dbMember.setStartDate(member.getStartDate());

        if (StringUtils.isNotBlank(member.getMarketingSource())) {
            LinkedHashSet<String> marketingSources = shop.getMarketingSources();
            if (marketingSources == null) {
                marketingSources = new LinkedHashSet<>();
            }
            if (!marketingSources.contains(member.getMarketingSource())) {
                marketingSources.add(member.getMarketingSource());
                shopRepository.updateMarketingSources(storeToken.getCompanyId(), storeToken.getShopId(), marketingSources);
            }
        }
        dbMember.setMarketingSource(member.getMarketingSource());
        dbMember.setEnableLoyalty(member.isEnableLoyalty());
        dbMember.setEnabledCareGiver(member.isEnabledCareGiver());
        dbMember.setAddresses(member.getAddresses());
        dbMember.setTags(member.getTags());

        List<String> careGivers = member.getCareGivers();
        HashMap<String, CareGiver> careGiverHashMap = careGiverRepository.listAllAsMap(storeToken.getCompanyId());
        if (careGivers != null) {
            for (String careGiver : careGivers) {
                CareGiver careGiverDetails = careGiverHashMap.get(careGiver);
                if (careGiverDetails == null) {
                    throw new BlazeInvalidArgException("CareGiver", "Care Giver doesn't found");
                }
            }
        } else {
            careGivers = new ArrayList<>();
        }
        dbMember.setCareGivers(careGivers);

        dbMember.setConsumerType(member.getConsumerType());

        if (StringUtils.isNotEmpty(member.getEmail())) {
            dbMember.setEmail(member.getEmail().toLowerCase());
        }
        String memberGroupName = "";
        if (dbMember.getMemberGroup() != null) {
            memberGroupName = dbMember.getMemberGroup().getName();
        }

        String search = String.format("%s %s %s %s %s %s %s",
                EntityFieldUtil.value(dbMember.getFirstName()),
                EntityFieldUtil.value(dbMember.getMiddleName()),
                EntityFieldUtil.value(dbMember.getLastName()),
                EntityFieldUtil.value((dbMember.getAddress() == null) ? "" : dbMember.getAddress().getAddress()),
                EntityFieldUtil.value(dbMember.getEmail()),
                EntityFieldUtil.value(dbMember.getPrimaryPhone()),
                EntityFieldUtil.value(memberGroupName));

        dbMember.setSearchText(search.toLowerCase());

        if (member.getLoyaltyPoints() == null) {
            member.setLoyaltyPoints(new BigDecimal(0));
        }


        if (dbMember.getAddress() == null) {
            Address address = new Address();
            address.prepare(storeToken.getCompanyId());
            dbMember.setAddress(address);
        }

        dbMember.setRecommendationExpired(getRecommendationExpired(member));

        elasticSearchManager.createOrUpdateIndexedDocument(dbMember);
        Member m = memberRepository.update(storeToken.getCompanyId(), memberId, dbMember);

        //update member tags in shop
        if (member.getTags() != null && member.getTags().size() > 0) {
            Set<String> shopMembersTag = member.getTags();
            for (String requestTag : member.getTags())
                if (!shopMembersTag.contains(requestTag)) {
                    shopMembersTag.add(requestTag);
                }
            shopRepository.updateMemberTags(storeToken.getCompanyId(), shop.getId(), shopMembersTag);
        }


        realtimeService.sendRealTimeEvent(storeToken.getShopId().toString(), RealtimeService.RealtimeEventType.MembersUpdateEvent, null);

        callWebHook(dbMember, true);

        return m;
    }

    

    public Member addOrUpdateMember(Member member, MembershipAddRequest addRequest, SignedContract signedContract) {
        boolean shouldUpdate = false;

        Shop shop = shopRepository.get(storeToken.getCompanyId(), storeToken.getShopId());
        if (shop == null) {
            throw new BlazeInvalidArgException("Shop", "Shop does not found");
        }
        if (member == null) {
            member = new Member();
            member.setId(addRequest.getId());
            if (StringUtils.isNotEmpty(addRequest.getEmail())) {
                member.setEmail(addRequest.getEmail().toLowerCase());
            }
            member.setStartDate(DateTime.now().getMillis());
            // This is an add
            member.setLoyaltyPoints(addRequest.getLoyaltyPoints());
            member.setLifetimePoints(addRequest.getLoyaltyPoints());
        } else {
            shouldUpdate = true;
        }
        if (member.getStartDate() == null || member.getStartDate() == 0) {
            member.setStartDate(DateTime.now().getMillis());
        }

        HashMap<String, MemberGroup> memberGroupHashMap = new HashMap<>();
        Iterable<MemberGroup> groups = memberGroupRepository.list(storeToken.getCompanyId());
        MemberGroup defaultGroup = null;
        for (MemberGroup memberGroup : groups) {
            memberGroupHashMap.put(memberGroup.getId(), memberGroup);
            if (memberGroup.isDefaultGroup()) {
                defaultGroup = memberGroup;
            }
        }

        if (member.getAddresses() != null) {
            for (Address address : member.getAddresses()) {
                address.prepare(storeToken.getCompanyId());
            }
        }

        MemberGroup memberGroup = memberGroupHashMap.get(addRequest.getMemberGroupId());
        if (memberGroup != null) {
            member.setMemberGroupId(memberGroup.getId());
            member.setMemberGroup(memberGroup);
        } else if (defaultGroup != null) {
            member.setMemberGroupId(defaultGroup.getId());
            member.setMemberGroup(defaultGroup);
        }

        if (member.getMemberGroup() == null) {
            throw new BlazeInvalidArgException("MemberGroup", "Invalid membership level");
        }

        //set fields
        member.setCompanyId(storeToken.getCompanyId());
        member.setShopId(storeToken.getShopId());
        member.setEmail(addRequest.getEmail());
        member.setFirstName(addRequest.getFirstName());
        member.setLastName(addRequest.getLastName());
        member.setMiddleName(addRequest.getMiddleName());
        member.setAddress(addRequest.getAddress());
        member.setAddresses(addRequest.getAddresses());
        member.setDob(addRequest.getDob());
        if (StringUtils.isNotBlank(addRequest.getMarketingSource())) {
            LinkedHashSet<String> marketingSources = shop.getMarketingSources();
            if (marketingSources == null) {
                marketingSources = new LinkedHashSet<>();
            }
            if (!marketingSources.contains(addRequest.getMarketingSource())) {
                marketingSources.add(addRequest.getMarketingSource());
                shopRepository.updateMarketingSources(storeToken.getCompanyId(), storeToken.getShopId(), marketingSources);
            }
        }
        member.setMarketingSource(addRequest.getMarketingSource());
        member.setEnableLoyalty(addRequest.isEnableLoyalty());
        if (member.getAddress() != null && member.getAddress().getCompanyId() != null) {
            member.getAddress().resetPrepare(storeToken.getCompanyId());
        }

        if (StringUtils.isNotEmpty(member.getEmail())) {
            member.setEmail(member.getEmail().toLowerCase());
        }

        member.setPrimaryPhone(addRequest.getPrimaryPhone());
        member.setTextOptIn(addRequest.isTextOptIn());
        member.setEmailOptIn(addRequest.isEmailOptIn());
        member.setMedical(addRequest.isMedical());
        member.setSex(addRequest.getSex());
        member.setStartDate(addRequest.getStartDate());
        member.setConsumerType(addRequest.getConsumerType());
        if (member.getStartDate() == 0) {
            member.setStartDate(DateTime.now().getMillis());
        }

        String search = String.format("%s %s %s %s %s %s %s",
                EntityFieldUtil.value(addRequest.getFirstName()),
                EntityFieldUtil.value(addRequest.getMiddleName()),
                EntityFieldUtil.value(addRequest.getLastName()),
                EntityFieldUtil.value((addRequest.getAddress() == null) ? "" : addRequest.getAddress().getAddress()),
                EntityFieldUtil.value(addRequest.getEmail()),
                EntityFieldUtil.value(addRequest.getPrimaryPhone()),
                EntityFieldUtil.value(member.getMemberGroup().getName()));
        member.setSearchText(search.toLowerCase());

        List<String> careGivers = addRequest.getCareGivers();
        HashMap<String, CareGiver> careGiverHashMap = careGiverRepository.listAllAsMap(storeToken.getCompanyId());
        if (careGivers != null) {
            for (String careGiver : careGivers) {
                CareGiver careGiverDetails = careGiverHashMap.get(careGiver);
                if (careGiverDetails == null) {
                    throw new BlazeInvalidArgException("CareGiver", "Care Giver doesn't found");
                }
            }
        }
        member.setCareGivers(careGivers);
        member.setTags(addRequest.getTags());


        // Identifications and recommendations
        member.setIdentifications(addRequest.getIdentifications());
        member.setRecommendations(addRequest.getRecommendations());

        member.setRecommendationExpired(getRecommendationExpired(member));

        member.setStatus(addRequest.getStatus());

        if (signedContract != null) {
            member.getContracts().add(signedContract);
        }

        member.setStatus(addRequest.getStatus());
        member.setEnabledCareGiver(addRequest.isEnabledCareGiver());

        prepareDataToSave(member);
        //save entity
        StringBuilder logMessage = new StringBuilder();
        if (shouldUpdate) {

            memberRepository.update(storeToken.getCompanyId(), member.getId(), member);
            logMessage.append("Member updated by - ");
        } else {
            memberRepository.save(member);
            logMessage.append("Member created by - ");
        }

        //update member tags in shop
        if (member.getTags() != null && member.getTags().size() > 0) {
            LinkedHashSet<String> shopMembersTag = member.getTags();
            for (String requestTag : member.getTags()) {
                if (!shopMembersTag.contains(requestTag)) {
                    shopMembersTag.add(requestTag);
                }
            }
            shopRepository.updateMemberTags(storeToken.getCompanyId(), storeToken.getShopId(), shopMembersTag);
        }

        elasticSearchManager.createOrUpdateIndexedDocument(member);

        logMessage.append("Partner API: ");


        realtimeService.sendRealTimeEvent(storeToken.getShopId().toString(), RealtimeService.RealtimeEventType.MembersUpdateEvent, null);
        return member;
    }





    //Helper
    private void prepareDataToSave(Member member) {
        // set start Date
        if (member.getStartDate() == null || member.getStartDate() == 0) {
            member.setStartDate(DateTime.now().getMillis());
        }
        if (member.getAddress() != null) {
            member.getAddress().prepare();
        }

        if (member.getIdentifications() != null) {
            for (Identification identification : member.getIdentifications()) {
                identification.prepare(storeToken.getCompanyId());
                if (identification.getFrontPhoto() != null) {
                    CompanyAsset backAsset = companyAssetRepository.getById(identification.getFrontPhoto().getId());
                    identification.setFrontPhoto(backAsset);
                }

                // Refresh assets just in case
                List<CompanyAsset> assets = new ArrayList<>();
                for (CompanyAsset asset : identification.getAssets()) {
                    CompanyAsset backAsset = companyAssetRepository.getById(asset.getId());
                    assets.add(backAsset);

                    if (identification.getFrontPhoto() == null) {
                        identification.setFrontPhoto(backAsset);
                    }
                }
                if (assets.size() == 0 && identification.getFrontPhoto() != null) {
                    assets.add(identification.getFrontPhoto());
                } else if (assets.size() > 0) {
                    identification.setFrontPhoto(assets.get(0));
                }
                identification.setAssets(assets);
            }
        }
        if (member.getRecommendations() != null) {
            for (Recommendation recommendation : member.getRecommendations()) {
                recommendation.prepare(storeToken.getCompanyId());
                if (recommendation.getFrontPhoto() != null && recommendation.getFrontPhoto().getId() != null) {
                    CompanyAsset backAsset = companyAssetRepository.getById(recommendation.getFrontPhoto().getId());
                    recommendation.setFrontPhoto(backAsset);
                }
                // Refresh assets just in case
                List<CompanyAsset> assets = new ArrayList<>();
                for (CompanyAsset asset : recommendation.getAssets()) {
                    CompanyAsset backAsset = companyAssetRepository.getById(asset.getId());
                    assets.add(backAsset);

                    if (recommendation.getFrontPhoto() == null) {
                        recommendation.setFrontPhoto(backAsset);
                    }
                }

                if (assets.size() == 0 && recommendation.getFrontPhoto() != null) {
                    assets.add(recommendation.getFrontPhoto());
                } else if (assets.size() > 0) {
                    recommendation.setFrontPhoto(assets.get(0));
                }
                recommendation.setAssets(assets);

                if (StringUtils.isNotBlank(recommendation.getDoctorId()) && ObjectId.isValid(recommendation.getDoctorId())) {
                    Doctor dbDoctor = doctorRepository.get(storeToken.getCompanyId(), recommendation.getDoctorId());
                    if (dbDoctor != null) {
                        recommendation.setDoctorId(dbDoctor.getId());
                        recommendation.setDoctor(dbDoctor);
                    }
                } else if (recommendation.getDoctor() != null) {
                    Doctor doctor = recommendation.getDoctor();
                    Doctor dbDoctor = null; //doctorRepository.getDoctorByLicense(storeToken.getCompanyId(), doctor.getLicense());

                    if (doctor.getId() != null) {
                        dbDoctor = doctorRepository.get(storeToken.getCompanyId(), doctor.getId());
                    } else if (StringUtils.isNotEmpty(doctor.getLicense())) {
                        dbDoctor = doctorRepository.getDoctorByLicense(storeToken.getCompanyId(), doctor.getLicense());
                    }

                    if (dbDoctor == null) {
                        doctor.setId(ObjectId.get().toString());
                        doctor.setActive(true);
                        doctorRepository.save(doctor);
                    } else {
                        if (!dbDoctor.complete()) {
                            dbDoctor.setFirstName(doctor.getFirstName());
                            dbDoctor.setLastName(doctor.getLastName());
                            dbDoctor.setLicense(doctor.getLicense());
                            dbDoctor.setWebsite(doctor.getWebsite());
                            dbDoctor.setPhoneNumber(doctor.getPhoneNumber());
                            doctor.setActive(true);
                            doctorRepository.update(storeToken.getCompanyId(), dbDoctor.getId(), dbDoctor);
                        }
                        doctor = dbDoctor;
                    }
                    recommendation.setDoctorId(doctor.getId());
                    recommendation.setDoctor(doctor);
                }
            }
        }


        if (member.getNotes() != null) {
            for (Note note : member.getNotes()) {
                note.prepare();
                if (note.getWriterId() == null) {
                    note.setWriterName("Partner API");
                }
            }
        }

        if (member.getContracts() != null) {
            for (SignedContract signedContract : member.getContracts()) {
                signedContract.prepare(storeToken.getCompanyId());
                if (signedContract.getMembershipId() == null) {
                    signedContract.setShopId(storeToken.getShopId());
                    signedContract.setMembershipId(member.getId());
                    signedContract.setSignedDate(DateTime.now().getMillis());
                }
            }
        }
    }




    /**
     * Private method to update recommendation expired.
     *
     * @param member
     * @return
     */
    private boolean getRecommendationExpired(Member member) {
        boolean isRecExpired = true;
        if (member.getRecommendations() != null && !member.getRecommendations().isEmpty()) {
            long currentDate = DateTime.now().getMillis();
            for (Recommendation recommendation : member.getRecommendations()) {
                if (recommendation.getExpirationDate() != null && recommendation.getExpirationDate() >= currentDate) {
                    isRecExpired = false;
                    break;
                }
            }
        }
        return isRecExpired;
    }



    /**
     * This is private method for web hook call
     *
     * @param member   : details of member
     * @param isUpdate : false (if new member is added) and true (if member update)
     */
    private void callWebHook(Member member, boolean isUpdate) {
        if (member == null) {
            return;
        }

        try {
            final MemberData memberData = new MemberData();

            StringBuilder name = new StringBuilder();
            name.append(StringUtils.isNotBlank(member.getFirstName()) ? member.getFirstName() : "").append(" ");
            name.append(StringUtils.isNotBlank(member.getMiddleName()) ? member.getMiddleName() : "").append(" ");
            name.append(StringUtils.isNotBlank(member.getLastName()) ? member.getLastName() : "");
            memberData.setId(member.getId());
            memberData.setCreated(member.getCreated());
            memberData.setModified(member.getModified());
            memberData.setMemberName(name.toString());
            memberData.setFirstName(member.getFirstName());
            memberData.setLastName(member.getLastName());
            memberData.setPhoneNumber(member.getPrimaryPhone());
            memberData.setEmail(member.getEmail());
            memberData.setSex(member.getSex());
            memberData.setAddress(member.getAddress());
            memberData.setDob(member.getDob());
            memberData.setMemberGroupId(member.getMemberGroupId());
            memberData.setStatus(member.getStatus());
            memberData.setConsumerType(member.getConsumerType());
            memberData.setTextOptIn(member.isTextOptIn());
            memberData.setEmailOptIn(member.isEmailOptIn());

            PartnerWebHook.PartnerWebHookType webHookType = (isUpdate) ? PartnerWebHook.PartnerWebHookType.UPDATE_MEMBER : PartnerWebHook.PartnerWebHookType.NEW_MEMBER;
            partnerWebHookManager.memberWebHook(storeToken.getCompanyId(), storeToken.getShopId(), memberData, webHookType);

        } catch (Exception e) {
            LOG.error(((isUpdate) ? "Update" : "Create") + " member web hook failed for shopId :" + storeToken.getShopId() + ",company id:" + storeToken.getCompanyId());
        }

    }

    /**
     * This method fetches list of members
     * @param startDate : start date
     * @param endDate : end date
     * @param skip : skip
     * @param limit : limit
     * @return List of member {@link SearchResult<Member>}
     */
    @Override
    public SearchResult<Member> getMemberList(long startDate, long endDate, int skip, int limit) {

        limit = (limit <= 0 || limit > 100) ? 100 : limit;
        skip = (skip <= 0) ? 0 : skip;

        return memberRepository.getMemberList(storeToken.getCompanyId(), startDate, endDate, skip, limit, "{modified:-1}");
    }

    /**
     * This member fetches member limit
     * @param memberId : member id
     */
    @Override
    public HashMap<Product.CannabisType, BigDecimal> getMemberLimit(String memberId) {
        if (StringUtils.isBlank(memberId)) {
            throw new BlazeInvalidArgException(MEMBER, "Member id can't be empty");
        }

        MemberResult member = memberRepository.get(storeToken.getCompanyId(), memberId, MemberResult.class);

        if (member == null) {
            throw new BlazeInvalidArgException(MEMBER, MEMBER_NOT_FOUND);
        }

        Shop shop = shopRepository.get(storeToken.getCompanyId(), storeToken.getShopId());
        if (shop == null) {
            throw new BlazeInvalidArgException("Shop", "Shop doesn't exist");
        }

        HashMap<Product.CannabisType, BigDecimal> userLimit = new HashMap<>();
        StateCannabisLimit stateCannabisLimit = null;
        if (shop.isEnableCannabisLimit()) {
            if (StringUtils.isNotBlank(shop.getAddress().getState())) {
                stateCannabisLimit = cannabisLimitService.getCannabisLimitByConsumerTypeAndState(storeToken.getCompanyId(),shop.getAddress().getState(), member.getConsumerType());
            }

            HashMap<Product.CannabisType, BigDecimal> userCurrentCannabisLimit = cannabisLimitService.userCurrentCannabisLimit(storeToken.getCompanyId(), shop, member, null, stateCannabisLimit, null, null, false);
            if (userCurrentCannabisLimit.size() == 0 && userCurrentCannabisLimit.isEmpty()) {
                HashMap<Product.CannabisType, BigDecimal> cannabisTypeMap = new HashMap<>();
                cannabisTypeMap.put(Product.CannabisType.CONCENTRATE, new BigDecimal(0));
                cannabisTypeMap.put(Product.CannabisType.NON_CONCENTRATE, new BigDecimal(0));
                cannabisTypeMap.put(Product.CannabisType.PLANT, new BigDecimal(0));
                cannabisTypeMap.put(Product.CannabisType.DRY_LEAF, new BigDecimal(0));
                cannabisTypeMap.put(Product.CannabisType.OIL, new BigDecimal(0));
                cannabisTypeMap.put(Product.CannabisType.EXTRACT, new BigDecimal(0));
                cannabisTypeMap.put(Product.CannabisType.DRY_FLOWER, new BigDecimal(0));
                cannabisTypeMap.put(Product.CannabisType.EDIBLE, new BigDecimal(0));
                cannabisTypeMap.put(Product.CannabisType.KIEF, new BigDecimal(0));
                cannabisTypeMap.put(Product.CannabisType.LIQUID, new BigDecimal(0));
                cannabisTypeMap.put(Product.CannabisType.SUPPOSITORY, new BigDecimal(0));
                cannabisTypeMap.put(Product.CannabisType.TINCTURE, new BigDecimal(0));
                cannabisTypeMap.put(Product.CannabisType.TOPICAL, new BigDecimal(0));
                userLimit = cannabisLimitService.prepareMemberDataForCannabis(member, cannabisTypeMap);

            } else {
                userLimit = cannabisLimitService.prepareMemberDataForCannabis(member, userCurrentCannabisLimit);
            }
        }
        return userLimit;
    }

    @Override
    public MemberLimit getRemainingMemberLimit(String memberId) {
        if (StringUtils.isBlank(memberId)) {
            throw new BlazeInvalidArgException(MEMBER, "Member id can't be empty");
        }

        MemberResult member = memberRepository.get(storeToken.getCompanyId(), memberId, MemberResult.class);

        if (member == null) {
            throw new BlazeInvalidArgException(MEMBER, MEMBER_NOT_FOUND);
        }

        Shop shop = shopRepository.get(storeToken.getCompanyId(), storeToken.getShopId());
        if (shop == null) {
            throw new BlazeInvalidArgException("Shop", "Shop doesn't exist");
        }

        HashMap<Product.CannabisType, BigDecimal> userLimit = new HashMap<>();
        StateCannabisLimit stateCannabisLimit = null;
        if (shop.isEnableCannabisLimit()) {
            if (StringUtils.isNotBlank(shop.getAddress().getState())) {
                stateCannabisLimit = cannabisLimitService.getCannabisLimitByConsumerTypeAndState(storeToken.getCompanyId(),shop.getAddress().getState(), member.getConsumerType());
            }

            HashMap<Product.CannabisType, BigDecimal> userCurrentCannabisLimit = cannabisLimitService.userCurrentCannabisLimit(storeToken.getCompanyId(), shop, member, null, stateCannabisLimit, null, null, false);
            if (userCurrentCannabisLimit.size() > 0) {
                userLimit = cannabisLimitService.prepareMemberDataForCannabis(member, userCurrentCannabisLimit);
            }
        }
        if (stateCannabisLimit == null) {
            throw new BlazeInvalidArgException("Shop", "Shop location is not enabled for member limits.");
        }


        /*

            } else if (cannabisType.getType() == 1) {
                cannabisDetail = Product.CannabisType.NON_CONCENTRATE;
            } else if (cannabisType.getType() == 2) {
                cannabisDetail = Product.CannabisType.PLANT;
            } else if (cannabisType.getType() == 3) {
                cannabisDetail = Product.CannabisType.SEEDS;
            } else if (cannabisType.getType() == 4) {
                cannabisDetail = Product.CannabisType.THC;*/
        BigDecimal concentrates = calcRemaining(stateCannabisLimit.getConcentrates(), userLimit.get(Product.CannabisType.CONCENTRATE));
        BigDecimal nonconcentrates = calcRemaining(stateCannabisLimit.getNonConcentrates(), userLimit.get(Product.CannabisType.NON_CONCENTRATE));
        BigDecimal plants = calcRemaining(stateCannabisLimit.getPlants(), userLimit.get(Product.CannabisType.PLANT));
        BigDecimal seeds = calcRemaining(stateCannabisLimit.getSeeds(), userLimit.get(Product.CannabisType.SEEDS));
        BigDecimal thc = calcRemaining(stateCannabisLimit.getThc(), userLimit.get(Product.CannabisType.THC));

        MemberLimit memberLimit = new MemberLimit();


        memberLimit.setConsumerType(stateCannabisLimit.getConsumerType());
        memberLimit.setDuration(stateCannabisLimit.getDuration());
        memberLimit.setState(stateCannabisLimit.getState());
        memberLimit.setConcentrates(concentrates);
        memberLimit.setNonConcentrates(nonconcentrates);
        memberLimit.setPlants(plants);
        memberLimit.setSeeds(seeds);
        memberLimit.setThc(thc);
        return memberLimit;
    }

    private BigDecimal calcRemaining(BigDecimal startValue, BigDecimal usageValue) {
        if (startValue == null || startValue.doubleValue() < 0) {
            return new BigDecimal(-1);
        }

        if (usageValue == null) {
            return startValue;
        }
        return startValue.subtract(usageValue);
    }

    @Override
    public ArrayList<Member> getMembersByLicenceNo(String licenceNumber) {
        return memberRepository.getMembersByLicenceNo(storeToken.getCompanyId(), licenceNumber, "{modified:-1}");
    }
}
