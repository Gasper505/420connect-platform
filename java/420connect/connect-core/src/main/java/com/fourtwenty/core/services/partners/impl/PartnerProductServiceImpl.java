package com.fourtwenty.core.services.partners.impl;

import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.repositories.dispensary.EmployeeRepository;
import com.fourtwenty.core.rest.dispensary.requests.inventory.ProductAddRequest;
import com.fourtwenty.core.rest.dispensary.requests.partners.PartnerProductUpdateRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.tokens.StoreAuthToken;
import com.fourtwenty.core.services.AbstractStoreServiceImpl;
import com.fourtwenty.core.services.mgmt.CommonProductService;
import com.fourtwenty.core.services.partners.PartnerProductService;
import com.google.inject.Inject;
import com.google.inject.Provider;
import org.apache.commons.lang3.StringUtils;

public class PartnerProductServiceImpl extends AbstractStoreServiceImpl implements PartnerProductService {

    @Inject
    private CommonProductService productService;
    @Inject
    private EmployeeRepository employeeRepository;

    @Inject
    public PartnerProductServiceImpl(Provider<StoreAuthToken> tokenProvider) {
        super(tokenProvider);
    }

    @Override
    public Product getProductById(String productId) {
        return productService.getProductById(storeToken.getCompanyId(), storeToken.getShopId(), productId);
    }

    @Override
    public Product addProduct(ProductAddRequest addRequest) {
        return productService.addProduct(storeToken.getCompanyId(), storeToken.getShopId(), addRequest);
    }

    @Override
    public Product updateProduct(String productId, PartnerProductUpdateRequest request) {
        Employee employee = employeeRepository.get(storeToken.getCompanyId(), request.getEmployeeId());
        String employeeId = StringUtils.EMPTY;
        String employeeName = StringUtils.EMPTY;
        if (employee != null) {
            employeeId = employee.getId();
            employeeName = employee.getFirstName() + " " + employee.getLastName();
        }
        return productService.updateProduct(storeToken.getCompanyId(), storeToken.getShopId(), productId, request, Boolean.TRUE, employeeId, employeeName);
    }

    @Override
    public SearchResult<Product> getProducts(String startDate, String endDate, int skip, int limit) {
        limit = (limit <= 0 || limit > 100) ? 100 : limit;
        skip = (skip <= 0) ? 0 : skip;
        return productService.getProducts(storeToken.getCompanyId(), storeToken.getShopId(), startDate, endDate, skip, limit);
    }

    @Override
    public SearchResult<Product> getProducts(long startDate, long endDate, int skip, int limit) {

        limit = (limit <= 0 || limit > 100) ? 100 : limit;
        skip = (skip <= 0) ? 0 : skip;

        return productService.getProducts(storeToken.getCompanyId(), storeToken.getShopId(), startDate, endDate, skip, limit);
    }
}
