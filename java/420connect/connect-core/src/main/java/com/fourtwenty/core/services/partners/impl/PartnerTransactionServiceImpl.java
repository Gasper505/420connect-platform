package com.fourtwenty.core.services.partners.impl;

import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.developer.DeveloperKeyRepository;
import com.fourtwenty.core.domain.repositories.dispensary.CompanyRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.rest.dispensary.requests.partners.PartnerEmployeeReassignRequest;
import com.fourtwenty.core.rest.dispensary.requests.partners.PartnerQueueAddMemberRequest;
import com.fourtwenty.core.rest.dispensary.requests.partners.PartnerTransactionDeleteRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.tokens.StoreAuthToken;
import com.fourtwenty.core.services.AbstractStoreServiceImpl;
import com.fourtwenty.core.services.orders.CommonPOSService;
import com.fourtwenty.core.services.partners.PartnerTransactionService;
import com.fourtwenty.core.util.DateUtil;
import com.google.common.collect.Lists;
import com.google.inject.Provider;
import org.apache.commons.lang3.StringUtils;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import javax.inject.Inject;
import java.io.InputStream;
import java.util.List;

public class PartnerTransactionServiceImpl extends AbstractStoreServiceImpl implements PartnerTransactionService {

    @Inject
    CommonPOSService commonPOSService;
    @Inject
    ShopRepository shopRepository;
    @Inject
    CompanyRepository companyRepository;
    @Inject
    DeveloperKeyRepository developerKeyRepository;
    @Inject
    TransactionRepository transactionRepository;

    @Override
    public Transaction addToQueue(String queueName, PartnerQueueAddMemberRequest request) {

        return commonPOSService.addToQueue(storeToken.getCompanyId(),storeToken.getShopId(),request.getEmployeeId(),queueName,request);
    }

    @Override
    public void deleteTransaction(String transactionId, PartnerTransactionDeleteRequest request) {
        commonPOSService.deleteTransaction(storeToken.getCompanyId(),storeToken.getShopId(),request.getEmployeeId(),transactionId,request);
    }


    @Override
    public Transaction completeTransaction(String transactionId, Transaction transaction) {
        return commonPOSService.completeTransaction(storeToken.getCompanyId(),storeToken.getShopId(),transaction.getTerminalId(),transaction.getSellerId(),
                transactionId,transaction,false,false);
    }

    @Override
    public Transaction setTransactionToFulfill(Transaction transaction, String transactionId) {
        return commonPOSService.setTransactionToFulfill(storeToken.getCompanyId(),storeToken.getShopId(),transaction.getSellerId(),transaction,transactionId);
    }

    @Override
    public Transaction prepareCart(String transactionId, Transaction transaction) {
        return commonPOSService.prepareCart(storeToken.getCompanyId(),storeToken.getShopId(),transaction.getTerminalId(),
                transaction.getSellerId(),transactionId,transaction);
    }

    @Override
    public Transaction markAsPaid(String transactionId, Cart.PaymentOption paymentOption) {
        return commonPOSService.markAsPaid(storeToken.getCompanyId(),storeToken.getShopId(),transactionId, paymentOption);
    }

    @Override
    public Transaction signTransaction(String transactionId, InputStream inputStream, String name, FormDataBodyPart body) {
        return null;
    }

    @Override
    public Transaction holdTransaction(String transactionId, Transaction transaction) {
        return commonPOSService.holdTransaction(storeToken.getCompanyId(),storeToken.getShopId(),transaction.getTerminalId(),transaction.getSellerId(),transactionId,transaction);
    }

    @Override
    public Transaction reassignTransactionEmployee(String transactionId, PartnerEmployeeReassignRequest request) {
        //return commonPOSService.reassignTransactionEmployee(storeToken.getCompanyId(),storeToken.getShopId(),request.getCurrentTerminalId(),request.getCurrentEmployeeId(),request
        return null;
    }

    @Override
    public Transaction updateDeliveryAddress(String transactionId, Address request) {
        return commonPOSService.updateDeliveryAddress(storeToken.getCompanyId(),storeToken.getShopId(),transactionId,request);
    }

    @Override
    public Transaction unassignTransaction(String transactionId) {
        return null;
    }

    @Override
    public Transaction startTransaction(String transactionId) {
        return null;
    }

    @Override
    public Transaction stopTransaction(String transactionId) {
        return null;
    }

    @Inject
    public PartnerTransactionServiceImpl(Provider<StoreAuthToken> tokenProvider) {
        super(tokenProvider);
    }

    @Override
    public SearchResult<Transaction> getAllTransaction(String startDate, String endDate, int skip, int limit) {
        SearchResult<Transaction> transactions = new SearchResult<>();
        if (!storeToken.isEnableSales()) {
            return transactions;
        }
        Shop shop = shopRepository.get(storeToken.getCompanyId(), storeToken.getShopId());
        if (shop == null) {
            throw new BlazeInvalidArgException("Shop", "Shop is not found.");
        }

        Company company = companyRepository.getById(storeToken.getCompanyId());
        if (company == null) {
            throw new BlazeInvalidArgException("Company", "Company is not found.");
        }

        DateTimeFormatter formatter = DateTimeFormat.forPattern("MM/dd/yyyy");




        if (limit <= 0 || limit > 100) {
            limit = 100;
        }

        if (skip < 0) {
            skip = 0;
        }

        List<Transaction.TransactionStatus> statuses = Lists.newArrayList(Transaction.TransactionStatus.Completed, Transaction.TransactionStatus.RefundWithInventory, Transaction.TransactionStatus.RefundWithoutInventory);

        List<Transaction.TransactionType> types = Lists.newArrayList(Transaction.TransactionType.Sale,Transaction.TransactionType.Refund);

        if (StringUtils.isBlank(endDate) || StringUtils.isBlank(startDate)) {

            transactions = transactionRepository.getAllTransactionByStatusAndTypeLimit(storeToken.getCompanyId(), storeToken.getShopId(),
                    statuses, types,
                    skip, limit);
        } else {
            DateTime endDateTime;
            if (StringUtils.isNotBlank(endDate)) {
                endDateTime = formatter.parseDateTime(endDate).plusDays(1).minusSeconds(1);
            } else {
                endDateTime = DateUtil.nowUTC().plusDays(1).minusSeconds(1);
            }

            DateTime startDateTime;
            if (StringUtils.isNotBlank(startDate)) {
                startDateTime = formatter.parseDateTime(startDate).withTimeAtStartOfDay();
            } else {
                startDateTime = DateUtil.nowUTC().withTimeAtStartOfDay();
            }
            String timeZone = shop.getTimeZone();
            int timeZoneOffset = DateUtil.getTimezoneOffsetInMinutes(timeZone);

            long timeZoneStartDateMillis = startDateTime.minusMinutes(timeZoneOffset).getMillis();
            long timeZoneEndDateMillis = endDateTime.minusMinutes(timeZoneOffset).getMillis();

            int days = DateUtil.getDaysBetweenTwoDates(timeZoneStartDateMillis, timeZoneEndDateMillis);

            transactions = transactionRepository.getAllTransactionByStatusAndType(storeToken.getCompanyId(), storeToken.getShopId(),
                    statuses, types,
                    timeZoneStartDateMillis, timeZoneEndDateMillis,
                    skip, limit);
        }
        return transactions;
    }

    @Override
    public SearchResult<Transaction> getActiveTransactions() {
        return transactionRepository.getActiveTransactions(storeToken.getCompanyId(),storeToken.getShopId());
    }

    @Override
    public Transaction getTransactionById(String transactionId) {
        Transaction transaction = transactionRepository.get(storeToken.getCompanyId(), transactionId);
        if (transaction == null) {
            throw new BlazeInvalidArgException("Transaction", "Transaction does not exist.");
        }
        return transaction;
    }

    /**
     * Override method to get completed transactions for member id
     *
     * @param memberId : memberId
     * @param start    : start
     * @param limit    : limit
     * @return : search result
     */
    @Override
    public SearchResult<Transaction> getCompletedTransactionsForMember(String memberId, int start, int limit) {
        limit = (limit <= 0 || limit > 100) ? 100 : limit;
        start = (start <= 0) ? 0 : start;
        return transactionRepository.getTransactionsForMember(storeToken.getCompanyId(), storeToken.getShopId(), memberId, start, limit);
    }

    /**
     * Override method to get active transactions for member
     *
     * @param memberId : member id
     * @return
     */
    @Override
    public SearchResult<Transaction> getMemberActiveTransactions(String memberId) {
        return transactionRepository.getActiveTransactionsForMember(storeToken.getCompanyId(), storeToken.getShopId(), memberId);
    }

    @Override
    public Transaction finalizeTransaction(String transactionId, Transaction transaction, boolean finalize) {
        return commonPOSService.finalizeTransaction(storeToken.getCompanyId(), storeToken.getShopId(), transactionId, transaction, finalize);
    }
}
