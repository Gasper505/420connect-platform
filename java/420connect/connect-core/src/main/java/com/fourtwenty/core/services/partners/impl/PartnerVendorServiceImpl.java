package com.fourtwenty.core.services.partners.impl;

import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.repositories.dispensary.EmployeeRepository;
import com.fourtwenty.core.rest.dispensary.requests.inventory.VendorAddRequest;
import com.fourtwenty.core.rest.dispensary.requests.partners.PartnerVendorUpdateRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.VendorResult;
import com.fourtwenty.core.security.tokens.StoreAuthToken;
import com.fourtwenty.core.services.AbstractStoreServiceImpl;
import com.fourtwenty.core.services.mgmt.CommonVendorService;
import com.fourtwenty.core.services.partners.PartnerVendorService;
import com.google.inject.Inject;
import com.google.inject.Provider;
import org.apache.commons.lang3.StringUtils;

public class PartnerVendorServiceImpl extends AbstractStoreServiceImpl implements PartnerVendorService {

    @Inject
    private CommonVendorService vendorService;
    @Inject
    private EmployeeRepository employeeRepository;

    @Inject
    public PartnerVendorServiceImpl(Provider<StoreAuthToken> token) {
        super(token);
    }

    @Override
    public VendorResult getVendor(String vendorId) {
        return vendorService.getVendor(storeToken.getCompanyId(), vendorId);
    }

    @Override
    public Vendor addVendor(VendorAddRequest request) {
        Employee employee = employeeRepository.get(storeToken.getCompanyId(), request.getEmployeeId());
        String employeeId = StringUtils.EMPTY;
        if (employee != null) {
            employeeId = employee.getId();
        }
        return vendorService.addVendor(storeToken.getCompanyId(), storeToken.getShopId(), request, employeeId);
    }

    @Override
    public Vendor updateVendor(String vendorId, PartnerVendorUpdateRequest vendor) {
        Employee employee = employeeRepository.get(storeToken.getCompanyId(), vendor.getEmployeeId());
        String employeeId = StringUtils.EMPTY;
        String employeeName = StringUtils.EMPTY;
        if (employee != null) {
            employeeId = employee.getId();
            employeeName = employee.getFirstName() + " " + employee.getLastName();
        }
        return vendorService.updateVendor(storeToken.getCompanyId(), storeToken.getShopId(), vendorId, vendor, Boolean.TRUE, employeeId, employeeName);
    }

    @Override
    public SearchResult<Vendor> getVendors(String startDate, String endDate, int skip, int limit) {
        return vendorService.getVendors(storeToken.getCompanyId(), storeToken.getShopId(), startDate, endDate, skip, limit);
    }
}
