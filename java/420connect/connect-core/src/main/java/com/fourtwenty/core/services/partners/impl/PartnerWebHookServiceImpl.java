package com.fourtwenty.core.services.partners.impl;

import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.partner.PartnerWebHook;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.domain.repositories.partner.PartnerWebHookRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.managed.PartnerWebHookManager;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.partners.request.PartnerWebHookAddRequest;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.partners.PartnerWebHookService;
import com.google.inject.Provider;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;


public class PartnerWebHookServiceImpl extends AbstractAuthServiceImpl implements PartnerWebHookService {

    private static final String PARTNER_WEBHOOK = "PARTNER Web Hooks";
    private static final String PARTNER_WEBHOOK_NOT_FOUND = "PARTNER webhook does not found";
    private static final String SHOP_NOT_FOUND = "Shop does not found";
    private static final String SHOP = "Shop";
    private static final String URL_NOT_FOUND = "Url cannot be blank";

    @Inject
    private PartnerWebHookRepository partnerWebHookRepository;
    @Inject
    private ShopRepository shopRepository;
    @Inject
    private PartnerWebHookManager partnerWebHookManager;

    @Inject
    public PartnerWebHookServiceImpl(Provider<ConnectAuthToken> token) {
        super(token);
    }

    /**
     * This is override method to create web hook for partner
     *
     * @param request
     * @return
     */
    @Override
    public PartnerWebHook createPartnerWebHooks(PartnerWebHookAddRequest request) {
        if (request.getWebHookUrls().size() == 0) {
            throw new BlazeInvalidArgException(PARTNER_WEBHOOK, URL_NOT_FOUND);
        }

        String shopId = request.getShopId();
        if (StringUtils.isBlank(shopId)) {
            shopId = token.getShopId();
        }
        Shop shop = shopRepository.getById(shopId);
        if (shop == null) {
            throw new BlazeInvalidArgException(SHOP, SHOP_NOT_FOUND);
        }
        //Check partner web hook if already available
        PartnerWebHook partnerWebHook = partnerWebHookRepository.getByWebHookType(token.getCompanyId(), shopId, request.getWebHookType());
        if (partnerWebHook == null) {
            //if not create new for shop and web hook type
            partnerWebHook = new PartnerWebHook();
            partnerWebHook.prepare(token.getCompanyId());
            partnerWebHook.setShopId(shopId);
            partnerWebHook.setWebHookType(request.getWebHookType());
            partnerWebHook.setUrl(request.getUrl());
            partnerWebHook.setWebHookUrls(request.getWebHookUrls());
            return partnerWebHookRepository.save(partnerWebHook);
        } else {
            //else update existing
            partnerWebHook.setWebHookType(request.getWebHookType());
            partnerWebHook.setUrl(request.getUrl());
            partnerWebHook.setWebHookUrls(request.getWebHookUrls());
            return partnerWebHookRepository.update(partnerWebHook.getId(), partnerWebHook);
        }
    }

    /**
     * This is overrided method to update partner web hook
     *
     * @param webHookId
     * @param request
     * @return
     */
    @Override
    public PartnerWebHook updatePartnerWebHooks(String webHookId, PartnerWebHook request) {

        PartnerWebHook partnerWebHook = partnerWebHookRepository.get(token.getCompanyId(), webHookId);
        if (partnerWebHook == null) {
            throw new BlazeInvalidArgException(PARTNER_WEBHOOK, PARTNER_WEBHOOK_NOT_FOUND);
        }
        String shopId = request.getShopId();
        if (StringUtils.isBlank(shopId)) {
            shopId = token.getShopId();
        }
        Shop shop = shopRepository.getById(shopId);
        if (shop == null) {
            throw new BlazeInvalidArgException(SHOP, SHOP_NOT_FOUND);
        }

        partnerWebHook.setShopId(shopId);
        partnerWebHook.setUrl(request.getUrl());
        partnerWebHook.setWebHookType(request.getWebHookType());
        partnerWebHook.setWebHookUrls(request.getWebHookUrls());
        return partnerWebHookRepository.update(webHookId, partnerWebHook);
    }

    /**
     * This is overrided method to get partner web hook by id
     *
     * @param webHookId
     * @return
     */
    @Override
    public PartnerWebHook getPartnerWebHookById(String webHookId) {
        PartnerWebHook partnerWebHook = partnerWebHookRepository.get(token.getCompanyId(), webHookId);
        if (partnerWebHook == null) {
            throw new BlazeInvalidArgException(PARTNER_WEBHOOK, PARTNER_WEBHOOK_NOT_FOUND);
        }
        if (StringUtils.isNotBlank(partnerWebHook.getUrl())) {
            partnerWebHookManager.updateWebHook(partnerWebHook);
        }
        return partnerWebHook;
    }

    /**
     * This is overrided method to get all partner web hook
     *
     * @param start
     * @param limit
     * @return
     */
    @Override
    public SearchResult<PartnerWebHook> getPartnerWebHook(PartnerWebHook.PartnerWebHookType webHookType, int start, int limit, String shopId) {
        if (limit == 0) {
            limit = Integer.MAX_VALUE;
        }
        if (webHookType != null && StringUtils.isBlank(shopId)) {
            return partnerWebHookRepository.findItemsByWebHookType(token.getCompanyId(), webHookType, "{modified:-1}", start, limit, PartnerWebHook.class);
        } else if (webHookType != null && StringUtils.isNotBlank(shopId)) {
            return partnerWebHookRepository.findItemsByWebHookTypeWithShop(token.getCompanyId(), shopId, webHookType, "{modified:-1}", start, limit);
        } else if (StringUtils.isBlank(shopId)) {
            return partnerWebHookRepository.findItemsWithSort(token.getCompanyId(), "{modified:-1}", start, limit);
        } else {
            return partnerWebHookRepository.findItems(token.getCompanyId(), shopId, "{modified:-1}", start, limit);
        }
    }

    @Override
    public void deletePartnerWebHook(String id) {
        PartnerWebHook webHook = partnerWebHookRepository.get(token.getCompanyId(), id);
        if (webHook == null) {
            throw new BlazeInvalidArgException(PARTNER_WEBHOOK, PARTNER_WEBHOOK_NOT_FOUND);
        }

        partnerWebHookRepository.removeById(id);
    }

}
