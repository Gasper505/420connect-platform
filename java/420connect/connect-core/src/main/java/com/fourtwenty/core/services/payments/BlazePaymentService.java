package com.fourtwenty.core.services.payments;

import com.braintreegateway.Result;
import com.braintreegateway.Transaction;
import com.fourtwenty.core.domain.models.payment.Payment;
import com.fourtwenty.core.domain.models.payment.PaymentComponent;
import com.fourtwenty.core.domain.models.payment.PaymentProviderDetails;
import com.fourtwenty.core.rest.payments.BraintreeBuyRequest;
import com.fourtwenty.core.rest.payments.BraintreeTokenResult;
import com.fourtwenty.core.rest.payments.PaymentReceipt;
import com.fourtwenty.core.rest.payments.SmsBuyRequest;
import com.fourtwenty.core.rest.payments.stripe.StripeSMSBuyRequest;
import com.fourtwenty.core.rest.payments.wepay.WepayCreditCardCheckoutRequest;

/**
 * Created by mdo on 8/27/17.
 */
public interface BlazePaymentService {

    BraintreeTokenResult createBraintreeToken();

    Result<Transaction> checkoutSMSBraintree(BraintreeBuyRequest buyRequest);

    Payment checkoutSMSWepay(WepayCreditCardCheckoutRequest wepayCreditCardCheckoutRequest);

    Payment checkoutSMSStripe(StripeSMSBuyRequest stripeSMSBuyRequest);

    PaymentReceipt getPurchaseReceipt(String purchaseReferenceId, PaymentComponent paymentComponent);

    Payment checkoutSMSPayment(SmsBuyRequest smsBuyRequest);

    PaymentProviderDetails getDefaultPaymentProvider();
}
