package com.fourtwenty.core.services.payments;

/**
 * Created on 24/10/17 1:47 AM by Raja Dushyant Vashishtha
 * Sr. Software Developer
 * email : rajad@decipherzone.com
 * www.decipherzone.com
 */
public final class StripeConversionService {

    private static final double MULTIPLIER = 100.00;

    private StripeConversionService() {
    }

    public static long toStripe(final double val) {
        return (long) (val * MULTIPLIER);
    }

    public static double toBlaze(final long val) {
        return ((double) val) / MULTIPLIER;
    }
}
