package com.fourtwenty.core.services.payments;

import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.rest.dispensary.requests.company.StripeCustomerRequest;
import com.fourtwenty.core.rest.dispensary.requests.company.CompanyEmployeeSeatRequest;
import com.fourtwenty.core.rest.dispensary.requests.company.StripeSourceRequest;
import com.fourtwenty.core.rest.dispensary.requests.company.StripeSubscriptionSourceRequest;
import java.util.List;
import com.stripe.model.Customer;
import com.stripe.model.Subscription;
import com.stripe.model.Source;

public interface StripeService {
    
    Company createCustomerFromCompany(Company company);

    List<Company> createCustomerAllCompany();

    String getStripeCustomerPayments(String customerId);

    String getCompanyStripeCustomer(String companyId);

    Customer updateStripeCustomerAddress(StripeCustomerRequest request);

    Subscription updateStripeSubscriptionEmployeeSeat(CompanyEmployeeSeatRequest request);

    void updateSubscriptionEmployeeSeat(String companyId);

    Customer attachCustomerSource(StripeSourceRequest request);

    Customer setCustomerDefaultSource(StripeSourceRequest request);

    Source removeCustomerSource(StripeSourceRequest request);

    Subscription updateSubscriptionDefaultPayment(StripeSubscriptionSourceRequest request);
}

