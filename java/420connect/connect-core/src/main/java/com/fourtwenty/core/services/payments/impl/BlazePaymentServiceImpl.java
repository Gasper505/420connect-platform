package com.fourtwenty.core.services.payments.impl;

import com.braintreegateway.*;
import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.domain.models.App;
import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.credits.CreditSaleLineItem;
import com.fourtwenty.core.domain.models.payment.*;
import com.fourtwenty.core.domain.repositories.dispensary.AppRepository;
import com.fourtwenty.core.domain.repositories.dispensary.CompanyRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.domain.repositories.payment.PaymentRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.exceptions.BlazeOperationException;
import com.fourtwenty.core.managed.AmazonServiceManager;
import com.fourtwenty.core.rest.features.SMSBuyRequest;
import com.fourtwenty.core.rest.payments.BraintreeBuyRequest;
import com.fourtwenty.core.rest.payments.BraintreeTokenResult;
import com.fourtwenty.core.rest.payments.PaymentReceipt;
import com.fourtwenty.core.rest.payments.SmsBuyRequest;
import com.fourtwenty.core.rest.payments.stripe.StripePaymentRequest;
import com.fourtwenty.core.rest.payments.stripe.StripeSMSBuyRequest;
import com.fourtwenty.core.rest.payments.wepay.WepayCreditCardCheckoutRequest;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.credits.CreditService;
import com.fourtwenty.core.services.payments.BlazePaymentService;
import com.fourtwenty.core.services.payments.StripeConversionService;
import com.fourtwenty.core.util.TextUtil;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.stripe.exception.StripeException;
import com.stripe.model.Charge;
import com.stripe.model.Token;
import com.stripe.net.RequestOptions;
import com.wepay.exception.WePayException;
import com.wepay.model.Checkout;
import com.wepay.model.data.CheckoutData;
import com.wepay.model.data.CheckoutFindData;
import com.wepay.model.data.HostedCheckoutData;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by mdo on 8/27/17.
 */
public class BlazePaymentServiceImpl extends AbstractAuthServiceImpl implements BlazePaymentService {

    private static final Log LOG = LogFactory.getLog(BlazePaymentServiceImpl.class);

    @Inject
    private CreditService creditService;

    @Inject
    private ConnectConfiguration configuration;
    @Inject
    private PaymentRepository paymentRepository;
    @Inject
    private CompanyRepository companyRepository;
    @Inject
    private AmazonServiceManager amazonServiceManager;
    @Inject
    private ShopRepository shopRepository;

    private AppRepository appRepository;

    private static BraintreeGateway gateway = new BraintreeGateway(
            Environment.SANDBOX,
            "trr48nyn2jvqzppd",
            "48tws82gtqhn2ck5",
            "2cdb4c722e0f2c61f613143f7e275cc2"
    );

    @Inject
    public BlazePaymentServiceImpl(Provider<ConnectAuthToken> tokenProvider, AppRepository appRepository) {
        super(tokenProvider);
        this.appRepository = appRepository;
    }


    @Override
    public BraintreeTokenResult createBraintreeToken() {
        BraintreeTokenResult result = new BraintreeTokenResult();
        String token = gateway.clientToken().generate();
        result.setClientToken(token);
        return result;
    }

    @Override
    public Result<Transaction> checkoutSMSBraintree(BraintreeBuyRequest buyRequest) {
        double cost = buyRequest.getQuantity() * getApp().getSmsCost().doubleValue() / 100;

        TransactionRequest request = new TransactionRequest()
                .amount(BigDecimal.valueOf(cost))
                .paymentMethodNonce(buyRequest.getNonce())
                .options()
                .submitForSettlement(true)
                .done();

        Result<Transaction> result = gateway.transaction().sale(request);
        if (result.isSuccess()) {
            com.fourtwenty.core.rest.features.SMSBuyRequest smsBuyRequest = new com.fourtwenty.core.rest.features.SMSBuyRequest();
            smsBuyRequest.setAmount(buyRequest.getQuantity());
            creditService.buySMSCredit(smsBuyRequest);
        } else {
            throw new BlazeInvalidArgException("Braintree", "Unable to process payment.");
        }
        return result;
    }


    @Override
    public Payment checkoutSMSWepay(WepayCreditCardCheckoutRequest wepayCreditCardCheckoutRequest) {

        // Verify creditcardid to be new
//        if (!this.validWepayCheckout(wepayCreditCardCheckoutRequest.getCreditCardId())) {
//            throw new BlazeInvalidArgException("Wepay", "Seems a duplicate checkout");
//        }

//        final PMCreditCardData pmCreditCardData = new PMCreditCardData();
//        pmCreditCardData.id = wepayCreditCardCheckoutRequest.getCreditCardId();
//
//        final PaymentMethodData paymentMethodData = new PaymentMethodData();
//        paymentMethodData.type = wepayCreditCardCheckoutRequest.getPaymentType();
//        paymentMethodData.creditCard = pmCreditCardData;

        final CheckoutData checkoutData = new CheckoutData();
        checkoutData.accountId = configuration.getWepayConfiguration().getAccountId();
        checkoutData.amount = wepayCreditCardCheckoutRequest.getAmount();
        checkoutData.shortDescription = wepayCreditCardCheckoutRequest.getShortDescription();
        checkoutData.type = wepayCreditCardCheckoutRequest.getType();
        checkoutData.currency = wepayCreditCardCheckoutRequest.getCurrency();
//        checkoutData.paymentMethod = paymentMethodData;

        // This is to make sure repeated checkouts are not longer valid.
//        checkoutData.referenceId = String.valueOf(wepayCreditCardCheckoutRequest.getCreditCardId());

        Payment payment = new Payment();
        payment.setAmount(wepayCreditCardCheckoutRequest.getAmount().doubleValue());
        payment.setPaymentComponent(PaymentComponent.SMS_MARKETING);
        payment.setPaymentProvider(PaymentProvider.WEPAY);
        payment.setPaymentRecurringType(PaymentRecurringType.ONE_TIME);
        payment.setCompanyId(token.getCompanyId());
        payment.setShopId(token.getShopId());
        payment.setPaymentDate(DateTime.now().getMillis());

        paymentRepository.save(payment);
        checkoutData.referenceId = payment.getId();
        checkoutData.hostedCheckout = new HostedCheckoutData();
        checkoutData.hostedCheckout.redirectUri = configuration.getAppWebsiteURL() + "/marketing?pstatus=success";
        checkoutData.hostedCheckout.fallbackUri = configuration.getAppWebsiteURL() + "/marketing?pstatus=error";
        Checkout checkout;
        try {
            checkout = Checkout.create(checkoutData, configuration.getWepayConfiguration().getAccessToken());
            payment.setCheckoutUrl(checkout.getCheckoutUri());
            payment.setPaymentStatus(PaymentStatus.SUCCESS);
            payment.setPaymentTransactionId(checkout.getCheckoutId().toString());
        } catch (WePayException | IOException e) {
            LOG.warn("Unable to create wepay checkout", e);
            payment.setPaymentStatus(PaymentStatus.FAILURE);
            payment.setFailureReason(e.getMessage());
            throw new BlazeInvalidArgException("Wepay", e.getMessage());
        }
        // Authorized is default status set by wepay when checkout is successful & released (this is only with credit card tokenize payment method)
        if (!"new".equals(checkout.getState())) {
            payment.setPaymentStatus(PaymentStatus.FAILURE);
            payment.setFailureReason("Transaction is not released");
            throw new BlazeInvalidArgException("Wepay", "Transaction is not released");
        }

        com.fourtwenty.core.rest.features.SMSBuyRequest smsBuyRequest = new com.fourtwenty.core.rest.features.SMSBuyRequest();
        smsBuyRequest.setAmount(wepayCreditCardCheckoutRequest.getQuantity());
        creditService.buySMSCredit(smsBuyRequest);

        payment.setRequestId(checkout.getReferenceId());
        paymentRepository.update(token.getCompanyId(), payment.getId(), payment);

        if (payment.getPaymentStatus().equals(PaymentStatus.FAILURE)) {
            // Throwing exception to make sure it is handled as an error at front-end
            throw new BlazeInvalidArgException("Wepay", "Unable to process [" + payment.getFailureReason() + "]");
        }

        return payment;
    }

    private Boolean validWepayCheckout(final long referenceId) {
        CheckoutFindData checkoutFindData = new CheckoutFindData();
        checkoutFindData.accountId = configuration.getWepayConfiguration().getAccountId();
        checkoutFindData.referenceId = String.valueOf(referenceId);
        try {
            Checkout[] checkouts = Checkout.find(checkoutFindData, configuration.getWepayConfiguration().getAccessToken());
            if (checkouts == null || checkouts.length == 0) {
                return Boolean.TRUE;
            }
        } catch (WePayException | IOException e) {
            LOG.warn("Unable to find wepay checkout", e);
        }

        return Boolean.FALSE;
    }

    @Override
    public Payment checkoutSMSStripe(final StripeSMSBuyRequest stripeSMSBuyRequest) {

        stripeSMSBuyRequest.setPaymentComponent(PaymentComponent.SMS_MARKETING);
        stripeSMSBuyRequest.setPaymentRecurringType(PaymentRecurringType.ONE_TIME);

        // Amount will be in cents so converting it to dollar for further use
        final double amount = StripeConversionService.toBlaze((long) (stripeSMSBuyRequest.getQuantity() * getApp().getSmsCost().doubleValue()));
        stripeSMSBuyRequest.setAmount(amount);

        final Payment payment = processStripePayment(stripeSMSBuyRequest);

        if (payment.getPaymentStatus().equals(PaymentStatus.FAILURE)) {
            throw new BlazeInvalidArgException("SMS", "Unable to process request [" + payment.getFailureReason() + "]");
        }

        SMSBuyRequest smsBuyRequest = new com.fourtwenty.core.rest.features.SMSBuyRequest();
        smsBuyRequest.setAmount(stripeSMSBuyRequest.getQuantity());
        final CreditSaleLineItem creditSaleLineItem = creditService.buySMSCredit(smsBuyRequest);

        payment.setPurchaseReferenceId(creditSaleLineItem.getId());
        paymentRepository.update(payment.getId(), payment);

        Shop shop = shopRepository.getById(payment.getShopId());
        this.sendReceiptEmail(payment.getPurchaseReferenceId(), payment.getPaymentComponent(), shop);
        return payment;
    }

    private Payment processStripePayment(StripePaymentRequest purchaseRequest) {
        final RequestOptions requestOptions = new RequestOptions.RequestOptionsBuilder().setApiKey(this.configuration.getStripeConfiguration().getApiSecret()).build();

        // We are throwing exception for invalid token so no need to wrap it in another IF
        validateStripeToken(purchaseRequest, requestOptions);

        final Map<String, Object> chargeMap = new HashMap<>();
        chargeMap.put("amount", StripeConversionService.toStripe(purchaseRequest.getAmount()));
        chargeMap.put("currency", purchaseRequest.getCurrency());
        chargeMap.put("source", purchaseRequest.getToken());

        Payment payment = new Payment();
        payment.setPaymentRecurringType(purchaseRequest.getPaymentRecurringType());
        payment.setPaymentComponent(purchaseRequest.getPaymentComponent());
        payment.setPaymentProvider(PaymentProvider.STRIPE);
        payment.setAmount(purchaseRequest.getAmount());
        payment.setCompanyId(token.getCompanyId());
        payment.setShopId(token.getShopId());
        payment.setPaymentDate(DateTime.now().getMillis());

        try {
            // As per Stripe, if charge is unsuccessful then it will throw an exception
            final Charge charge = Charge.create(chargeMap, requestOptions);
            payment.setPaymentTransactionId(charge.getId());
            payment.setPaymentStatus(PaymentStatus.SUCCESS);
        } catch (StripeException e) {
            LOG.warn("Unable to process stripe payment", e);
            payment.setPaymentStatus(PaymentStatus.FAILURE);
            payment.setFailureReason(e.getMessage());
            payment.setRequestId(e.getRequestId());
        }

        paymentRepository.save(payment);

        if (payment.getPaymentStatus().equals(PaymentStatus.FAILURE)) {
            // Throwing exception to make sure it is handled as an error at front-end
            throw new BlazeInvalidArgException("Stripe", "Unable to process [" + payment.getFailureReason() + "]");
        }

        return payment;

    }

    private Boolean validateStripeToken(final StripePaymentRequest purchaseRequest, final RequestOptions requestOptions) {
        try {
            Token token = Token.retrieve(purchaseRequest.getToken(), requestOptions);
            if (token.getUsed()) {
                throw new BlazeInvalidArgException("Stripe", "Token is already used");
            }

            return Boolean.TRUE;

        } catch (StripeException e) {
            LOG.error("Unable to validate stripe token", e);
            throw new BlazeInvalidArgException("Stripe", "Unable to validate token [" + e.getMessage() + "]");
        }
    }

    @Override
    public PaymentReceipt getPurchaseReceipt(final String purchaseReferenceId, final PaymentComponent paymentComponent) {
        final Payment payment = paymentRepository.getPayment(token.getCompanyId(), token.getShopId(), purchaseReferenceId, paymentComponent);

        if (payment == null) {
            throw new BlazeInvalidArgException("Purchase Receipt", "Purchase information can not be found");
        }

        if (payment.getPaymentComponent().equals(PaymentComponent.SMS_MARKETING)) {
            final CreditSaleLineItem creditSalesItem = creditService.getCreditSalesItem(token.getCompanyId(), purchaseReferenceId);

            final PaymentReceipt paymentReceipt = new PaymentReceipt();
            paymentReceipt.setCost(creditSalesItem.getCost());
            paymentReceipt.setPaymentComponent(paymentComponent);
            paymentReceipt.setPaymentProvider(payment.getPaymentProvider());
            paymentReceipt.setPurchaseDate(creditSalesItem.getPurchaseDate());
            paymentReceipt.setQuantity(creditSalesItem.getAmountBought());
            paymentReceipt.setUnitCost(getApp().getSmsCost());
            paymentReceipt.setPaymentTransactionId(payment.getPaymentTransactionId());

            return paymentReceipt;
        } else {
            throw new BlazeOperationException("Operation not implemented");
        }

    }

    @Override
    public Payment checkoutSMSPayment(SmsBuyRequest smsBuyRequest) {
        App app = getApp();
        PaymentProvider paymentProvider = app.getDefaultSmsPaymentProvider();

        switch (paymentProvider) {
            case WEPAY:
                return checkoutSMSWepay(smsBuyRequest.getWepayCreditCardCheckoutRequest());
            case STRIPE:
                return checkoutSMSStripe(smsBuyRequest.getStripeSMSBuyRequest());
            default:
                throw new BlazeInvalidArgException("Payment", "No default payment provider selected");
        }

    }

    @Override
    public PaymentProviderDetails getDefaultPaymentProvider() {
        PaymentProviderDetails providerDetails = new PaymentProviderDetails();
        App app = getApp();
        if (PaymentProvider.WEPAY.equals(app.getDefaultSmsPaymentProvider())) {

            providerDetails.setPaymentProvider(PaymentProvider.WEPAY);
//            providerDetails.setWepayClientKey(configuration.getWepayConfiguration().getClientId());

        } else if (PaymentProvider.STRIPE.equals(app.getDefaultSmsPaymentProvider())) {

            providerDetails.setPaymentProvider(PaymentProvider.STRIPE);
            providerDetails.setStripePublishKey(configuration.getStripeConfiguration().getApiKey());

        } else {
            throw new BlazeInvalidArgException("Payment", "No default payment provider selected");
        }

        return providerDetails;
    }

    private void sendReceiptEmail(final String purchaseReferenceId, final PaymentComponent paymentComponent, Shop shop) {
        final PaymentReceipt paymentReceipt = getPurchaseReceipt(purchaseReferenceId, paymentComponent);
        final Company company = companyRepository.getById(token.getCompanyId());

        String companyName = company.getName();
        String unitCost = "";
        String paymentComponentInfo = "";
        if (paymentComponent.equals(PaymentComponent.SMS_MARKETING)) {
            paymentComponentInfo = "SMS Credits";
            unitCost = paymentReceipt.getUnitCost() + " cents/unit";
        } else {
            throw new BlazeOperationException("Operation not implemented");
        }

        StringBuilder body = new StringBuilder();
        body.append("Dear ").append(companyName).append("<br/>").append("<br/>")
                .append("Your payment for your online order placed on ").append(TextUtil.toDateTime(paymentReceipt.getPurchaseDate(), "MM/dd/yyyy hh:mm a")).append(" has been processed (order reference number ").append(paymentReceipt.getPaymentTransactionId()).append(").").append("<br/>").append("<br/>")
                .append("An invoice has been prepared for ").append(companyName).append(" by Blaze Retail for ").append(paymentComponentInfo).append("<br/>").append("<br/>")
                .append("Payment Transaction Id : ").append(paymentReceipt.getPaymentTransactionId()).append("<br/>")
                .append("Total Cost : ").append(TextUtil.formatToTwoDecimalPoints(paymentReceipt.getCost().doubleValue())).append("<br/>")
                .append("Payment Component : ").append(paymentComponentInfo).append("<br/>")
                .append("Quantity : ").append(String.valueOf(paymentReceipt.getQuantity())).append("<br/>")
                .append("Per unit cost : ").append(unitCost).append("<br/>")
                .append("Payment Date : ").append(TextUtil.toDateTime(paymentReceipt.getPurchaseDate(), "MM/dd/yyyy hh:mm a")).append("<br/>").append("<br/>")
                .append("THANK YOU VERY MUCH FOR CHOOSING OUR PRODUCT").append("<br/>").append("<br/>")
                .append("You are receiving this because you are a current subscriber,or have bought from our website").append("<br/>");

        amazonServiceManager.sendEmail("support@blaze.me", company.getEmail(), getApp().getName(), body.toString(), null, null, "Blaze Support");

    }

    private App getApp() {
        return appRepository.getAppByName(App.BLAZE_APP_NAME);
    }

}
