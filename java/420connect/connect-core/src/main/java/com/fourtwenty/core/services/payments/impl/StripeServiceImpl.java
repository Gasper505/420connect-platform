package com.fourtwenty.core.services.payments.impl;

import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.domain.models.company.CompanyFeatures;
import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.repositories.dispensary.CompanyFeaturesRepository;
import com.fourtwenty.core.domain.repositories.dispensary.CompanyRepository;
import com.fourtwenty.core.domain.repositories.dispensary.EmployeeRepository;
import com.fourtwenty.core.rest.dispensary.requests.company.StripeCustomerRequest;
import com.fourtwenty.core.rest.dispensary.requests.company.CompanyEmployeeSeatRequest;
import com.fourtwenty.core.rest.dispensary.requests.company.StripeSourceRequest;
import com.fourtwenty.core.rest.dispensary.requests.company.StripeSubscriptionSourceRequest;

import com.amazonaws.util.CollectionUtils;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.services.payments.StripeService;
import com.google.inject.Inject;
import com.stripe.Stripe;
import com.stripe.exception.APIConnectionException;
import com.stripe.exception.AuthenticationException;
import com.stripe.exception.StripeException;
import com.stripe.model.Customer;
import com.stripe.model.Subscription;
import com.stripe.model.Source;
import com.stripe.net.RequestOptions;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.json.JSONArray;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.HashMap;
import java.util.Map;
import java.net.*;
import java.net.HttpURLConnection;
import java.io.BufferedReader;
import java.io.InputStreamReader;




public class StripeServiceImpl implements StripeService {

    private static final Logger LOGGER = LoggerFactory.getLogger(StripeServiceImpl.class);

    @Inject
    private ConnectConfiguration configuration;
    @Inject
    private CompanyRepository companyRepository;
    @Inject
    private CompanyFeaturesRepository companyFeaturesRepository;
    @Inject
    EmployeeRepository employeeRepository;

    private RequestOptions getRequestOptions(){
        String apiKey = this.configuration.getStripeConfiguration().getApiSecret();
        if (apiKey==null){
            throw new BlazeInvalidArgException("Stripe", "Api Key not set.");
        }

        return new RequestOptions.RequestOptionsBuilder().setApiKey(apiKey).build();
    }

    public Company createCustomerFromCompany(Company company){
        String apiKey = this.configuration.getStripeConfiguration().getApiSecret();
        if (apiKey==null){
            throw new BlazeInvalidArgException("Stripe", "Api Key not set.");
        }

        final RequestOptions requestOptions = new RequestOptions.RequestOptionsBuilder().setApiKey(apiKey).build();

        Map<String, Object> customerParams = new HashMap<String, Object>();
        
        //Set company address
        Map<String, Object> addressParams = new HashMap<String, Object>();
        Address address = company.getAddress();
        addressParams.put("line1", !address.getAddress().isEmpty() ? address.getAddress() : null);
        addressParams.put("city", !address.getCity().isEmpty() ? address.getCity() : null);
        addressParams.put("country", !address.getCountry().isEmpty() ? address.getCountry() : null);
        addressParams.put("line2", !address.getAddressLine2().isEmpty() ? address.getAddressLine2() : null);
        addressParams.put("postal_code", !address.getZipCode().isEmpty() ? address.getZipCode() : null);
        addressParams.put("state", !address.getState().isEmpty() ? address.getState() : null);

        customerParams.put("address", addressParams);
        customerParams.put("name", company.getName());
        customerParams.put("email", isValidEmail(company.getEmail())? company.getEmail() : null);
        customerParams.put("phone", StringUtils.isNotBlank(company.getPhoneNumber()) ? company.getPhoneNumber() : null);
        customerParams.put("balance", 0);


        CompanyFeatures companyFeatures = companyFeaturesRepository.getCompanyFeature(company.getId());

        Map<String, Object> metaData = new HashMap<String, Object>();
        metaData.put("companyId",company.getId());
        if (companyFeatures != null) {
            metaData.put("maxTerminals",companyFeatures.getMaxTerminals());
            metaData.put("maxEmployees",companyFeatures.getMaxEmployees());
            metaData.put("maxShops",companyFeatures.getMaxShop());
            metaData.put("maxInventories",companyFeatures.getMaxInventories());
            metaData.put("smsCredits",companyFeatures.getSmsMarketingCredits());
            //metaData.put("availableApps",companyFeatures.getAvailableApps());
            //metaData.put("availableQueues",companyFeatures.getAvailableQueues().stream().fla);
        }




        customerParams.put("metadata",metaData);

        try {
            Customer customer = Customer.create(customerParams, requestOptions);
            if (customer != null) {
                company.setStripeCustomer(customer);
                company.setStripeCustomerId(customer.getId());
            }
        } catch (AuthenticationException e){
            String message = "Authentication with Stripe's API failed";
            LOGGER.info(message);
            throw new BlazeInvalidArgException("Stripe", message);
        }catch (APIConnectionException e) {
            String message = "Network communication with Stripe failed";
            LOGGER.info(message);
            throw new BlazeInvalidArgException("Stripe", message);
        }catch (StripeException e) {
            String message = "Unable to create stripe customer for company "+ company.getName() + "; " + e.getMessage();
            LOGGER.info(message);
            throw new BlazeInvalidArgException("Stripe", message);
        }
        return company;
    }

    private boolean isValidEmail(String email) {
        String regex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        return email.matches(regex);
    }

    public List<Company> createCustomerAllCompany(){
        final List<Company> list = companyRepository.listNonDeleted();
        List<Company> companyList = new ArrayList<>();

        int updates = 0;
        for (Company company : list) {
            if (company.isActive() && !company.isDeleted()) {
                if (StringUtils.isBlank(company.getStripeCustomerId())) {
                    try {
                        company = createCustomerFromCompany(company);
                        companyRepository.update(company.getId(), company);
                        updates++;
                        companyList.add(company);
                    } catch (Exception e) {
                        LOGGER.info("Unable to create customer: " + company.getName() + " - " + company.getId() + " - " + company.getEmail());
                    }
                }
            }
        }
        LOGGER.info("{} companies updated", updates);
        return companyList;
    }

    private final String baseStripeUrl= "https://api.stripe.com";

    String getHttpResponse(String stringUrl,String httpMethod, String apiKey){
        try{
            URL url = new URL(stringUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("Authorization","Bearer "+apiKey);
            conn.setRequestProperty("Content-Type","application/json");
            conn.setRequestMethod(httpMethod);

            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String output;
            StringBuffer response = new StringBuffer();
            while ((output = in.readLine()) != null) {
                response.append(output);
            }

            in.close();
            // printing result from response
            return response.toString();

        }catch(Exception ex){
        }
        return null;
    }

    public String getStripeCustomerPayments(String customerId){
        String stringUrl = baseStripeUrl+"/v1/payment_intents";
        stringUrl+="?customer="+customerId;
        stringUrl+="&expand[0]=data.invoice";
        String apiKey = this.configuration.getStripeConfiguration().getApiSecret();
        if (apiKey==null){
            throw new BlazeInvalidArgException("Stripe", "Api Key not set.");
        }
        return getHttpResponse(stringUrl,"GET",apiKey);
    }

    public String getCompanyStripeCustomer(String companyId){
        Customer customer = null;
        RequestOptions requestOptions = getRequestOptions();
        Company company = companyRepository.getById(companyId);
        if (company!=null){
            customer = company.getStripeCustomer();
            if(customer != null){
                String stringUrl = baseStripeUrl+"/v1/customers/" + customer.getId();
                stringUrl += "?expand[]=subscriptions.data.plan.product";
                String apiKey = this.configuration.getStripeConfiguration().getApiSecret();
                if (apiKey==null){
                    throw new BlazeInvalidArgException("Stripe", "Api Key not set.");
                }
                return getHttpResponse(stringUrl,"GET",apiKey);
            }else{
                throw new BlazeInvalidArgException("Stripe", "Customer Stripe not found.");
            }
        }
        return null;
    }

    public Customer updateStripeCustomerAddress(StripeCustomerRequest request){
        RequestOptions requestOptions = getRequestOptions();
        Company company = companyRepository.getById(request.getCompanyId());
        Customer customer = company.getStripeCustomer();
        if(customer != null){
            try {
                String apiKey = this.configuration.getStripeConfiguration().getApiSecret();
                if (apiKey==null){
                    throw new BlazeInvalidArgException("Stripe", "Api Key not set.");
                }
                customer = Customer.retrieve(customer.getId(),requestOptions);
                Map<String, Object> addressParams = new HashMap<String, Object>();
                Address address = request.getAddress();
                addressParams.put("line1", !address.getAddress().isEmpty() ? address.getAddress() : null);
                addressParams.put("city", !address.getCity().isEmpty() ? address.getCity() : null);
                addressParams.put("country", !address.getCountry().isEmpty() ? address.getCountry() : null);
                addressParams.put("line2", !address.getAddressLine2().isEmpty() ? address.getAddressLine2() : null);
                addressParams.put("postal_code", !address.getZipCode().isEmpty() ? address.getZipCode() : null);
                addressParams.put("state", !address.getState().isEmpty() ? address.getState() : null);
                Map<String, Object> customerParams = new HashMap<String, Object>();
                customerParams.put("address", addressParams);
                customerParams.put("name", request.getName());
                customerParams.put("email", isValidEmail(request.getEmail())?request.getEmail() : null);
                customerParams.put("phone", request.getPhone());
                customer.update(customerParams,requestOptions);
                return customer;
            } catch (AuthenticationException e){
                String message = "Authentication with Stripe's API failed";
                LOGGER.info(message);
                throw new BlazeInvalidArgException("Stripe", message);
            }catch (APIConnectionException e) {
                String message = "Network communication with Stripe failed";
                LOGGER.info(message);
                throw new BlazeInvalidArgException("Stripe", message);
            }catch (StripeException e) {
                String message = "Unable to update Stripe Customer Address for company "+ company.getName();
                LOGGER.info(message);
                throw new BlazeInvalidArgException("Stripe", message);
            }
        }else{
            throw new BlazeInvalidArgException("Stripe", "Customer Stripe not found.");
        }
    }

    public Subscription updateStripeSubscriptionEmployeeSeat(CompanyEmployeeSeatRequest request){
        RequestOptions requestOptions = getRequestOptions();
        String apiKey = this.configuration.getStripeConfiguration().getApiSecret();
        if (apiKey==null){
            throw new BlazeInvalidArgException("Stripe", "Api Key not set.");
        }
        try {
            Subscription subscription = Subscription.retrieve(request.getSubscriptionId(),requestOptions);
            if (subscription!=null){
                Map<String, Object> item = new HashMap<String, Object>();
                item.put("quantity",request.getQuantity());
                item.put("id",request.getSubscriptionItemId());
                List<Object> items = new ArrayList<>();
                items.add(item);

                Map<String, Object> params = new HashMap<String, Object>();
                params.put("items",items);

                subscription.update(params,requestOptions);
                return subscription;
            }else{
                throw new BlazeInvalidArgException("Stripe", "Subscription not found.");
            }
        } catch (AuthenticationException e){
            String message = "Authentication with Stripe's API failed";
            LOGGER.info(message);
            throw new BlazeInvalidArgException("Stripe", message);
        }catch (APIConnectionException e) {
            String message = "Network communication with Stripe failed";
            LOGGER.info(message);
            throw new BlazeInvalidArgException("Stripe", message);
        }catch (StripeException e) {
            String message = "Unable to update Employee Count Subscription";
            LOGGER.info(message);
            throw new BlazeInvalidArgException("Stripe", message);
        }
    }

    public void updateSubscriptionEmployeeSeat(String companyId){
        Customer customer = null;
        RequestOptions requestOptions = getRequestOptions();
        Company company = companyRepository.getById(companyId);
        if (company!=null){
            customer = company.getStripeCustomer();
            if(customer != null){
                String customerResponse = getCompanyStripeCustomer(companyId);
                JSONObject jsonObject = new JSONObject(customerResponse);
                String planId = "";
                String subscriptionItemId = "";
                if (jsonObject!=null){
                    //If object customer has more than a subscription
                    if (!jsonObject.isNull("subscriptions")){
                        JSONObject jsonSubscriptions = jsonObject.getJSONObject("subscriptions");
                        JSONArray jsonData = jsonSubscriptions.getJSONArray("data");
                        //Loop every subscription
                        for (Iterator<Object> subscriptionItr=jsonData.iterator(); subscriptionItr.hasNext();){
                            JSONObject jsonElement = (JSONObject) subscriptionItr.next();
                            //Check for plan that has metadada isEmployeeSeat
                            if (!jsonElement.isNull("plan")){
                                JSONObject jsonPlan = jsonElement.getJSONObject("plan");
                                if (!jsonPlan.isNull("product")){
                                    JSONObject jsonProduct = jsonPlan.getJSONObject("product");
                                    if (!jsonProduct.isNull("metadata")){
                                        JSONObject jsonMetadata = jsonProduct.getJSONObject("metadata");
                                        if (!jsonMetadata.isNull("isEmployeeSeat")){
                                            String isEmployeeSeat = jsonMetadata.getString("isEmployeeSeat");
                                            if (Boolean.parseBoolean(isEmployeeSeat)){
                                                planId = jsonPlan.getString("id");
                                            }
                                        }
                                    }
                                }
                            }
                            if (StringUtils.isNotBlank(planId)){
                                //Get Subscription Item that has planId
                                if (!jsonElement.isNull("items")){
                                    JSONObject jsonItems = jsonElement.getJSONObject("items");
                                    JSONArray jsonItemData = jsonItems.getJSONArray("data");
                                    for (Iterator<Object> itemsItr=jsonItemData.iterator(); itemsItr.hasNext();){
                                        JSONObject jsonItem = (JSONObject) itemsItr.next();
                                        if (!jsonItem.isNull("plan")){
                                            JSONObject jsonItemPlan = jsonItem.getJSONObject("plan");
                                            String planItemId = jsonItemPlan.getString("id");
                                            if (planItemId.equals(planId)){
                                                subscriptionItemId = jsonItem.getString("id");
                                            }
                                        }
                                    }
                                }
                            }
                            if (StringUtils.isNotBlank(subscriptionItemId)){
                                List<Employee> employees = employeeRepository.getEmployees(companyId);
                                Integer employeeCount = !CollectionUtils.isNullOrEmpty(employees)?employees.size():0;

                                CompanyEmployeeSeatRequest request = new CompanyEmployeeSeatRequest();
                                request.setCompanyId(companyId);
                                request.setSubscriptionId(jsonElement.getString("id"));
                                request.setSubscriptionItemId(subscriptionItemId);
                                request.setQuantity(employeeCount);
                                updateStripeSubscriptionEmployeeSeat(request);
                            }
                        }
                    }
                }
            }else{
               LOGGER.info("Stripe", "Customer Stripe not found.");
            }
        }
    }

    @Override
    public Customer attachCustomerSource(StripeSourceRequest request) {
        RequestOptions requestOptions = getRequestOptions();
        String apiKey = this.configuration.getStripeConfiguration().getApiSecret();
        if (apiKey == null) {
            throw new BlazeInvalidArgException("Stripe", "Api Key not set.");
        }
        try {
            Stripe.apiKey = apiKey;
            Customer customer = Customer.retrieve(request.getCustomerId(), requestOptions);
            Map<String, Object> params = new HashMap<>();
            params.put("source", request.getSourceId());
            customer.getSources().create(params);
            return customer;
        } catch (AuthenticationException e) {
            String message = "Authentication with Stripe's API failed";
            LOGGER.info(message);
            throw new BlazeInvalidArgException("Stripe", message);
        } catch (APIConnectionException e) {
            String message = "Network communication with Stripe failed";
            LOGGER.info(message);
            throw new BlazeInvalidArgException("Stripe", message);
        } catch (StripeException e) {
            String message = "Unable to attach payment source";
            LOGGER.info(message);
            throw new BlazeInvalidArgException("Stripe", message);
        }
    }

    @Override
    public Customer setCustomerDefaultSource(StripeSourceRequest request){
        RequestOptions requestOptions = getRequestOptions();
        String apiKey = this.configuration.getStripeConfiguration().getApiSecret();
        if (apiKey == null) {
            throw new BlazeInvalidArgException("Stripe", "Api Key not set.");
        }
        try {
            Stripe.apiKey = apiKey;
            Customer customer = Customer.retrieve(request.getCustomerId());
            Map<String, Object> customerParams = new HashMap<String, Object>();

            Map<String, Object> params = new HashMap<String, Object>();
            params.put("customer", request.getCustomerId());

            customerParams.put("default_source", request.getSourceId());
            customer.update(customerParams);
            return customer;

        } catch (AuthenticationException e) {
            String message = "Authentication with Stripe's API failed";
            LOGGER.info(message);
            throw new BlazeInvalidArgException("Stripe", message);
        } catch (APIConnectionException e) {
            String message = "Network communication with Stripe failed";
            LOGGER.info(message);
            throw new BlazeInvalidArgException("Stripe", message);
        } catch (StripeException e) {
            String message = "Unable to set default payment source";
            LOGGER.info(message);
            throw new BlazeInvalidArgException("Stripe", message);
        }
    }

    @Override
    public Source removeCustomerSource(StripeSourceRequest request){
        String apiKey = this.configuration.getStripeConfiguration().getApiSecret();
        if (apiKey == null) {
            throw new BlazeInvalidArgException("Stripe", "Api Key not set.");
        }
        try {
            Stripe.apiKey = apiKey;
            Customer customer = Customer.retrieve(request.getCustomerId());

            if (customer.getDefaultSource().equals(request.getSourceId())){
                throw new BlazeInvalidArgException("Stripe", "Unable to remove the default payment source");
            }
            //Set Default Source for Subscriptions with the selected source
            if (!CollectionUtils.isNullOrEmpty(request.getSubscriptionIds())){
                for (String subscriptionId : request.getSubscriptionIds()){
                    Subscription subscription = Subscription.retrieve(subscriptionId);
                    Map<String, Object> subscriptionParams = new HashMap<String, Object>();
                    subscriptionParams.put("default_source",null);
                    subscription.update(subscriptionParams);
                }
            }

            Source source = Source.retrieve(request.getSourceId());
            source.detach();
            return source;

        } catch (AuthenticationException e) {
            String message = "Authentication with Stripe's API failed";
            LOGGER.info(message);
            throw new BlazeInvalidArgException("Stripe", message);
        } catch (APIConnectionException e) {
            String message = "Network communication with Stripe failed";
            LOGGER.info(message);
            throw new BlazeInvalidArgException("Stripe", message);
        } catch (StripeException e) {
            String message = "Unable to remove payment source";
            LOGGER.info(message);
            throw new BlazeInvalidArgException("Stripe", message);
        }
    }

    @Override
    public Subscription updateSubscriptionDefaultPayment(StripeSubscriptionSourceRequest request){
        String apiKey = this.configuration.getStripeConfiguration().getApiSecret();
        if (apiKey == null) {
            throw new BlazeInvalidArgException("Stripe", "Api Key not set.");
        }
        try {
            Stripe.apiKey = apiKey;
            Subscription subscription = Subscription.retrieve(request.getSubscriptionId());

            Map<String, Object> subscriptionParams = new HashMap<>();
            subscriptionParams.put("default_source",request.getSourceId());
            subscription.update(subscriptionParams);
            return subscription;

        } catch (AuthenticationException e) {
            String message = "Authentication with Stripe's API failed";
            LOGGER.info(message);
            throw new BlazeInvalidArgException("Stripe", message);
        } catch (APIConnectionException e) {
            String message = "Network communication with Stripe failed";
            LOGGER.info(message);
            throw new BlazeInvalidArgException("Stripe", message);
        } catch (StripeException e) {
            String message = "Unable to update subscription payment source";
            LOGGER.info(message);
            throw new BlazeInvalidArgException("Stripe", message);
        }
    }
}