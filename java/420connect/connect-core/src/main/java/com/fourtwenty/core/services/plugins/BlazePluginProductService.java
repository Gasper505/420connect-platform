package com.fourtwenty.core.services.plugins;

import com.fourtwenty.core.domain.models.plugins.BlazePluginProduct;

import java.util.List;

public interface BlazePluginProductService {
    List<BlazePluginProduct> getAllBlazePlugins();

    Boolean checkPlugin(String pluginId);

    BlazePluginProduct getPlugin(String pluginId);
}
