package com.fourtwenty.core.services.plugins;

import com.fourtwenty.core.domain.models.plugins.KioskPluginCompanySetting;
import com.fourtwenty.core.rest.plugins.KioskPluginCompanySettingRequest;

public interface KioskPluginService {
    KioskPluginCompanySetting getCompanyConsumerKioskPlugin();

    KioskPluginCompanySetting updateCompanyKioskPlugin(KioskPluginCompanySettingRequest companyPluginSettingRequest);
}
