package com.fourtwenty.core.services.plugins;

import com.fourtwenty.core.domain.models.plugins.MessagingPluginCompanySetting;
import com.fourtwenty.core.domain.models.plugins.MessagingPluginShopSetting;
import com.fourtwenty.core.rest.plugins.MessagingPluginCompanySettingRequest;

public interface MessagingPluginService {
    MessagingPluginCompanySetting getCompanyMessagingPlugin();

    MessagingPluginCompanySetting updateMessagingPluginSetting(MessagingPluginCompanySettingRequest companyPluginSettingRequest);

    MessagingPluginShopSetting getMessagingPluginShopSetting(String shopId);
}
