package com.fourtwenty.core.services.plugins;

import com.fourtwenty.core.domain.models.plugins.TVPluginCompanySetting;
import com.fourtwenty.core.rest.plugins.TVPluginCompanySettingRequest;

public interface TVPluginService {

    TVPluginCompanySetting getCompanyTVPlugin();

    TVPluginCompanySetting updateTVPluginSetting(TVPluginCompanySettingRequest companyPluginSettingRequest);

}
