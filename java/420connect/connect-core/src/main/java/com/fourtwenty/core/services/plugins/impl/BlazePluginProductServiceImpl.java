package com.fourtwenty.core.services.plugins.impl;

import com.fourtwenty.core.domain.models.plugins.BlazePluginProduct;
import com.fourtwenty.core.domain.repositories.plugins.BlazePluginProductRepository;
import com.fourtwenty.core.services.plugins.BlazePluginProductService;

import javax.inject.Inject;
import java.util.List;

public class BlazePluginProductServiceImpl implements BlazePluginProductService {

    private BlazePluginProductRepository blazePluginProductRepository;

    @Inject
    public BlazePluginProductServiceImpl(BlazePluginProductRepository blazePluginProductRepository) {
        this.blazePluginProductRepository = blazePluginProductRepository;
    }

    @Override
    public List<BlazePluginProduct> getAllBlazePlugins() {
        return blazePluginProductRepository.listNonDeleted();
    }

    @Override
    public Boolean checkPlugin(final String pluginId) {
        return getPlugin(pluginId) != null;
    }

    @Override
    public BlazePluginProduct getPlugin(final String pluginId) {
        return blazePluginProductRepository.getPlugin(pluginId);
    }

}