package com.fourtwenty.core.services.plugins.impl;

import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.plugins.KioskPluginCompanySetting;
import com.fourtwenty.core.domain.models.plugins.KioskPluginShopSetting;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.domain.repositories.plugins.KioskPluginRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.rest.plugins.KioskPluginCompanySettingRequest;
import com.fourtwenty.core.rest.plugins.KioskPluginShopSettingRequest;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.plugins.BlazePluginProductService;
import com.fourtwenty.core.services.plugins.KioskPluginService;
import com.google.common.collect.Lists;
import com.google.inject.Provider;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

public class KioskPluginServiceImpl extends AbstractAuthServiceImpl implements KioskPluginService {

    private static final Logger LOGGER = LoggerFactory.getLogger(KioskPluginServiceImpl.class);

    private KioskPluginRepository companyKioskPluginRepository;
    private BlazePluginProductService blazePluginProductService;
    private ShopRepository shopRepository;

    @Inject
    public KioskPluginServiceImpl(Provider<ConnectAuthToken> tokenProvider, KioskPluginRepository companyKioskPluginRepository, BlazePluginProductService blazePluginProductService, ShopRepository shopRepository) {
        super(tokenProvider);
        this.companyKioskPluginRepository = companyKioskPluginRepository;
        this.blazePluginProductService = blazePluginProductService;
        this.shopRepository = shopRepository;
    }

    @Override
    public KioskPluginCompanySetting getCompanyConsumerKioskPlugin() {
        KioskPluginCompanySetting kioskPluginCompanySetting = companyKioskPluginRepository.getCompanyPlugin(token.getCompanyId());
        if (kioskPluginCompanySetting == null) {
            kioskPluginCompanySetting = new KioskPluginCompanySetting();
            kioskPluginCompanySetting.setCompanyId(token.getCompanyId());
            kioskPluginCompanySetting.setEnabled(Boolean.FALSE);
            kioskPluginCompanySetting.setKioskSetting(Lists.<KioskPluginShopSetting>newArrayList());
            kioskPluginCompanySetting.prepare();
            this.companyKioskPluginRepository.save(kioskPluginCompanySetting);
        }
        return kioskPluginCompanySetting;
    }

    @Override
    public KioskPluginCompanySetting updateCompanyKioskPlugin(final KioskPluginCompanySettingRequest companyPluginSettingRequest) {
        /*final KioskPluginCompanySetting consumerKioskPlugin = companyKioskPluginRepository.get(token.getCompanyId(), companyPluginSettingRequest.getId());
        if (consumerKioskPlugin == null) {
            LOGGER.warn("Cannot find plugin setting for company and will not be created since we are already creating default when getting for company");
            throw new BlazeInvalidArgException("plugins", "Company plugin settings not available");
        }

        // Check if valid plugin for messaging
        final BlazePluginProduct blazePluginProduct = this.blazePluginProductService.getPlugin(companyPluginSettingRequest.getPluginId());
        if (blazePluginProduct == null) {
            throw new BlazeInvalidArgException("plugins", "Specified plugin is not found.");
        }
        if (!BlazePluginProduct.PluginType.CONSUMER_KIOSK.equals(blazePluginProduct.getPluginType())) {
            throw new BlazeInvalidArgException("plugins", "Specified plugin is not for Consumer Kiosk");
        }*/

        KioskPluginCompanySetting consumerKioskPlugin = null;
        Iterable<KioskPluginCompanySetting> pluginCompanySettings = companyKioskPluginRepository.list(token.getCompanyId());
        ArrayList<KioskPluginCompanySetting> kioskPluginCompanySettings = Lists.newArrayList(pluginCompanySettings);
        if (kioskPluginCompanySettings.isEmpty()) {
            consumerKioskPlugin = new KioskPluginCompanySetting();
            consumerKioskPlugin.setCompanyId(token.getCompanyId());
            consumerKioskPlugin.setEnabled(Boolean.FALSE);
            consumerKioskPlugin.setKioskSetting(new ArrayList<>());
            consumerKioskPlugin.prepare();
            this.companyKioskPluginRepository.save(consumerKioskPlugin);
        } else {
            for (KioskPluginCompanySetting setting : kioskPluginCompanySettings) {
                if (!setting.isDeleted()) {
                    consumerKioskPlugin = setting; break;
                }
            }
        }


//        consumerKioskPlugin.setPluginId(companyPluginSettingRequest.getPluginId());
        consumerKioskPlugin.setPluginId(StringUtils.EMPTY);
        consumerKioskPlugin.setEnabled(companyPluginSettingRequest.getEnabled());

        if (companyPluginSettingRequest.getShopPluginSettingRequests() != null) {

            // Check if available devices are allowed
            /*if (blazePluginProduct.getAvailableDevices() < companyPluginSettingRequest.getShopPluginSettingRequests().size()) {
                throw new BlazeInvalidArgException("plugins", "Exceeded available devices");
            }*/

            // Validate shop plugin settings
            final List<KioskPluginShopSetting> kioskSetting = consumerKioskPlugin.getKioskSetting();
            final List<KioskPluginShopSettingRequest> shopPluginSettingRequests = companyPluginSettingRequest.getShopPluginSettingRequests();

            for (KioskPluginShopSettingRequest shopPluginSettingRequest : shopPluginSettingRequests) {

                // Shopid is compulsory
                if (shopPluginSettingRequest.getShopId() == null) {
                    throw new BlazeInvalidArgException("plugins", "Shop id can not be null");
                }

                KioskPluginShopSetting shopSetting =  findShopSetting(kioskSetting, shopPluginSettingRequest.getShopId());

                shopSetting = (shopSetting == null) ? new KioskPluginShopSetting() : shopSetting;

                // Make sure that shop exists
                final Shop shop = shopRepository.get(token.getCompanyId(), shopPluginSettingRequest.getShopId());
                if (shop == null) {
                    throw new BlazeInvalidArgException("plugins", "Shop id is incorrect");
                } else if (shop.isDeleted()) {
                    // shop is deleted better to remove shop settings
                    final KioskPluginShopSetting deleteEligibleShopSetting = findShopSetting(kioskSetting, shopPluginSettingRequest.getShopId());
                    if (deleteEligibleShopSetting != null)
                        kioskSetting.remove(deleteEligibleShopSetting);
                    continue;
                }

                shopSetting.prepare(token.getCompanyId());
                shopSetting.setShopId(shopPluginSettingRequest.getShopId());
                shopSetting.setDeviceName(shopPluginSettingRequest.getDeviceName());
                shopSetting.setBackgroundColor(shopPluginSettingRequest.getBackgroundColor());
                shopSetting.setWelcomeText(shopPluginSettingRequest.getWelcomeText());
                shopSetting.setBackgroundImage(shopPluginSettingRequest.getBackgroundImage());
                shopSetting.setButtonTextColor(shopPluginSettingRequest.getButtonTextColor());
                shopSetting.setThemeColor(shopPluginSettingRequest.getThemeColor());

                if (kioskSetting.contains(shopSetting)) {
                    // Update persisted object
                    final int index = kioskSetting.indexOf(shopSetting);
                    kioskSetting.get(index).setEnabled(shopPluginSettingRequest.getEnabled());
                } else {
                    shopSetting.setEnabled(shopPluginSettingRequest.getEnabled());
                    kioskSetting.add(shopSetting);
                } /*else {
                    throw new BlazeInvalidArgException("plugins", "Exceeded available devices");
                }*/

                /* else if (blazePluginProduct.getAvailableDevices() > kioskSetting.size()) {
                    // save
                    shopSetting.setEnabled(shopPluginSettingRequest.getEnabled());
                    kioskSetting.add(shopSetting);
                } */
            }
        }

        this.companyKioskPluginRepository.update(token.getCompanyId(), consumerKioskPlugin.getId(), consumerKioskPlugin);
        return consumerKioskPlugin;
    }

    private KioskPluginShopSetting findShopSetting(final List<KioskPluginShopSetting> kioskSetting, final String shopId) {
        for (KioskPluginShopSetting kioskPluginShopSetting : kioskSetting) {
            if (kioskPluginShopSetting.getShopId().equals(shopId)) {
                return kioskPluginShopSetting;
            }
        }
        return null;
    }

}
