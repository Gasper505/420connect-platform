package com.fourtwenty.core.services.plugins.impl;

import com.fourtwenty.core.domain.models.App;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.plugins.BlazePluginProduct;
import com.fourtwenty.core.domain.models.plugins.MessagingPluginCompanySetting;
import com.fourtwenty.core.domain.models.plugins.MessagingPluginShopSetting;
import com.fourtwenty.core.domain.repositories.dispensary.AppRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.domain.repositories.plugins.MessagingPluginRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.rest.plugins.MessagingPluginCompanySettingRequest;
import com.fourtwenty.core.rest.plugins.MessagingPluginShopSettingRequest;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.plugins.BlazePluginProductService;
import com.fourtwenty.core.services.plugins.MessagingPluginService;
import com.fourtwenty.core.services.twilio.TwilioMessageService;
import com.google.common.collect.Lists;
import com.google.inject.Provider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.List;

public class MessagingPluginServiceImpl extends AbstractAuthServiceImpl implements MessagingPluginService {
    private static final Logger LOGGER = LoggerFactory.getLogger(MessagingPluginServiceImpl.class);

    private MessagingPluginRepository messagingPluginRepository;
    private BlazePluginProductService blazePluginProductService;
    private App app;
    private TwilioMessageService twilioMessageService;
    private ShopRepository shopRepository;

    @Inject
    public MessagingPluginServiceImpl(final AppRepository appRepository, Provider<ConnectAuthToken> tokenProvider, MessagingPluginRepository messagingPluginRepository, BlazePluginProductService blazePluginProductService, TwilioMessageService twilioMessageService, ShopRepository shopRepository) {
        super(tokenProvider);
        this.messagingPluginRepository = messagingPluginRepository;
        this.blazePluginProductService = blazePluginProductService;
        app = appRepository.getAppByName(App.BLAZE_APP_NAME);
        this.twilioMessageService = twilioMessageService;
        this.shopRepository = shopRepository;
    }

    @Override
    public MessagingPluginCompanySetting getCompanyMessagingPlugin() {
        MessagingPluginCompanySetting companyPlugin = messagingPluginRepository.getCompanyPlugin(token.getCompanyId());
        if (companyPlugin == null) {
            companyPlugin = new MessagingPluginCompanySetting();
            companyPlugin.setCompanyId(token.getCompanyId());
            companyPlugin.setEnabled(Boolean.FALSE);
            companyPlugin.setMessagingSetting(Lists.<MessagingPluginShopSetting>newArrayList());
            companyPlugin.prepare();
            messagingPluginRepository.save(companyPlugin);
        }

        return companyPlugin;
    }

    @Override
    public MessagingPluginCompanySetting updateMessagingPluginSetting(final MessagingPluginCompanySettingRequest companyPluginSettingRequest) {
        final MessagingPluginCompanySetting messagingPluginCompanySetting = messagingPluginRepository.get(token.getCompanyId(), companyPluginSettingRequest.getId());
        if (messagingPluginCompanySetting == null) {
            LOGGER.info("Cannot find plugin setting for company and will not be created since we are already creating default when getting for company");
            throw new BlazeInvalidArgException("plugins", "Company plugin settings not available");
        }

        // Check if valid plugin for messaging
        if (!BlazePluginProduct.PluginType.MESSAGING.equals(this.blazePluginProductService.getPlugin(companyPluginSettingRequest.getPluginId()).getPluginType())) {
            throw new BlazeInvalidArgException("plugins", "Specified plugin is not for Messaging");
        }


        messagingPluginCompanySetting.setPluginId(companyPluginSettingRequest.getPluginId());
        messagingPluginCompanySetting.setEnabled(companyPluginSettingRequest.getEnabled());

        if (companyPluginSettingRequest.getShopPluginSettingRequests() != null) {
            // Validate shop plugin settings
            final List<MessagingPluginShopSetting> messagingSetting = messagingPluginCompanySetting.getMessagingSetting();
            final List<MessagingPluginShopSettingRequest> shopPluginSettingRequests = companyPluginSettingRequest.getShopPluginSettingRequests();

            for (MessagingPluginShopSettingRequest shopPluginSettingRequest : shopPluginSettingRequests) {

                // Shopid is compulsory
                if (shopPluginSettingRequest.getShopId() == null) {
                    throw new BlazeInvalidArgException("plugins", "Shop id can not be null");
                }

                final MessagingPluginShopSetting shopSetting = new MessagingPluginShopSetting();

                // Make sure that shop exists
                final Shop shop = shopRepository.get(token.getCompanyId(), shopPluginSettingRequest.getShopId());
                if (shop == null || shop.isDeleted()) {
                    // shop is deleted hence don't do anything for it
                    // TODO clean up might be required later on to free up twilio resource if any
                    throw new BlazeInvalidArgException("plugins", "Shop id is incorrect");
                }

                shopSetting.setCompanyId(token.getCompanyId());
                shopSetting.setShopId(shopPluginSettingRequest.getShopId());

                if (messagingSetting.contains(shopSetting)) {
                    final int index = messagingSetting.indexOf(shopSetting);
                    messagingSetting.get(index).setEnabled(shopPluginSettingRequest.getEnabled());

                    // Update persisted object but do not update twilio number if already there as it is unchangeable
                    if (!app.isFakeTwilio() && shopPluginSettingRequest.getEnabled() && messagingSetting.get(index).getTwilioPhoneNumber() == null) {
//                        messagingSetting.get(index).setTwilioPhoneNumber(twilioMessageService.findAndCreatePhoneNumber(shop.getAddress().getCountry(), shop.getAddress().getState()));
                    }

                } else {
                    // save but fetch a twilio number first if enabling
                    if (!app.isFakeTwilio() && shopPluginSettingRequest.getEnabled()) {
//                        shopSetting.setTwilioPhoneNumber(twilioMessageService.findAndCreatePhoneNumber(shop.getAddress().getCountry(), shop.getAddress().getState()));
                    }
                    shopSetting.setEnabled(shopPluginSettingRequest.getEnabled());
                    messagingSetting.add(shopSetting);
                }
            }
        }

        this.messagingPluginRepository.update(token.getCompanyId(), messagingPluginCompanySetting.getId(), messagingPluginCompanySetting);
        return messagingPluginCompanySetting;
    }

    @Override
    public MessagingPluginShopSetting getMessagingPluginShopSetting(final String shopId) {
        return messagingPluginRepository.getMessagingPluginShopSetting(token.getCompanyId(), shopId);
    }

}
