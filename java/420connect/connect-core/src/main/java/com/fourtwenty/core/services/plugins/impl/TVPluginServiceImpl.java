package com.fourtwenty.core.services.plugins.impl;

import com.fourtwenty.core.domain.models.plugins.BlazePluginProduct;
import com.fourtwenty.core.domain.models.plugins.TVPluginCompanySetting;
import com.fourtwenty.core.domain.repositories.plugins.TVPluginRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.rest.plugins.TVPluginCompanySettingRequest;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.plugins.BlazePluginProductService;
import com.fourtwenty.core.services.plugins.TVPluginService;
import com.google.inject.Inject;
import com.google.inject.Provider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TVPluginServiceImpl extends AbstractAuthServiceImpl implements TVPluginService {

    private static final Logger LOGGER = LoggerFactory.getLogger(TVPluginServiceImpl.class);

    private TVPluginRepository tvPluginRepository;
    private BlazePluginProductService blazePluginProductService;

    @Inject
    public TVPluginServiceImpl(Provider<ConnectAuthToken> tokenProvider, TVPluginRepository tvPluginRepository, BlazePluginProductService blazePluginProductService) {
        super(tokenProvider);
        this.tvPluginRepository = tvPluginRepository;
        this.blazePluginProductService = blazePluginProductService;
    }

    @Override
    public TVPluginCompanySetting getCompanyTVPlugin() {
        TVPluginCompanySetting companyPlugin = tvPluginRepository.getCompanyPlugin(token.getCompanyId());
        if (companyPlugin == null) {
            companyPlugin = new TVPluginCompanySetting();
            companyPlugin.setCompanyId(token.getCompanyId());
            companyPlugin.setEnabled(Boolean.FALSE);
            companyPlugin.prepare();
            tvPluginRepository.save(companyPlugin);
        }

        return companyPlugin;
    }

    @Override
    public TVPluginCompanySetting updateTVPluginSetting(TVPluginCompanySettingRequest companyPluginSettingRequest) {
        final TVPluginCompanySetting tvPluginCompanySetting = tvPluginRepository.get(token.getCompanyId(), companyPluginSettingRequest.getId());
        if (tvPluginCompanySetting == null) {
            LOGGER.info("Cannot find plugin setting for company and will not be created since we are already creating default when getting for company");
            throw new BlazeInvalidArgException("plugins", "Company plugin settings not available");
        }

        // Check if valid plugin for messaging
        if (!BlazePluginProduct.PluginType.TV.equals(this.blazePluginProductService.getPlugin(companyPluginSettingRequest.getPluginId()).getPluginType())) {
            throw new BlazeInvalidArgException("plugins", "Specified plugin is not for TV");
        }

        tvPluginCompanySetting.setPluginId(companyPluginSettingRequest.getPluginId());
        tvPluginCompanySetting.setEnabled(companyPluginSettingRequest.getEnabled());

        this.tvPluginRepository.update(token.getCompanyId(), tvPluginCompanySetting.getId(), tvPluginCompanySetting);
        return tvPluginCompanySetting;
    }
}
