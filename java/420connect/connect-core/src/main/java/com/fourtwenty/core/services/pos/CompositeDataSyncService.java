package com.fourtwenty.core.services.pos;

import com.fourtwenty.core.rest.dispensary.results.common.CompositeInventorySyncResult;
import com.fourtwenty.core.rest.dispensary.results.common.CompositeProductInfoSyncResult;
import com.fourtwenty.core.rest.dispensary.results.common.CompositeShopSyncResult;

/**
 * Created by mdo on 5/26/18.
 */
public interface CompositeDataSyncService {
    CompositeShopSyncResult getShopData(long afterDate);

    CompositeProductInfoSyncResult getProductInfoSyncData(long afterDate);

    CompositeInventorySyncResult getInventorySyncData(long afterDate);
}
