package com.fourtwenty.core.services.pos;

import com.fourtwenty.core.domain.models.customer.BaseMember;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.views.MemberLimitedViewResult;
import com.fourtwenty.core.rest.dispensary.requests.company.MembershipUpdateRequest;
import com.fourtwenty.core.rest.dispensary.requests.company.POSMembershipAddMember;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.company.MemberResult;

/**
 * Created by mdo on 12/13/15.
 */
public interface POSMembershipService {
    SearchResult<Member> getMembershipsForCompanyDate(long beforeDate, long afterDate, int start, int limit);

    Member sanitize(BaseMember baseMember);

    Member addMembership(POSMembershipAddMember addRequest);

    Member updateMember(String memberId, MembershipUpdateRequest member);

    SearchResult<MemberLimitedViewResult> getMembersForDoctorId(String doctorId, String term, int start, int limit);

    SearchResult<MemberLimitedViewResult> getMembershipsForActiveShop(String term, int start, int limit,long beforeDate, long afterDate);

    MemberResult getMembership(String memberId);
}
