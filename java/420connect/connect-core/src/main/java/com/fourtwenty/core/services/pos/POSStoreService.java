package com.fourtwenty.core.services.pos;

import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.shop.ConsumerCartResult;

/**
 * Created by mdo on 6/7/17.
 */
public interface POSStoreService {
    DateSearchResult<ConsumerCartResult> getConsumerCarts(long afterDate, long beforeDate);
}
