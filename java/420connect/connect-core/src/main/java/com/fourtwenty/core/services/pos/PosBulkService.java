package com.fourtwenty.core.services.pos;

import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.rest.dispensary.requests.company.POSMembershipAddMember;
import com.fourtwenty.core.rest.dispensary.requests.queues.QueueAddMemberBulkRequest;
import com.fourtwenty.core.rest.dispensary.requests.shop.BulkAssetResponse;
import com.fourtwenty.core.rest.dispensary.requests.shop.BulkShopRequest;
import com.fourtwenty.core.rest.dispensary.requests.shop.BulkShopResponse;
import com.fourtwenty.core.rest.dispensary.requests.transaction.TransactionBulkRequest;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;

import java.io.InputStream;
import java.util.List;

public interface PosBulkService {
    BulkShopResponse syncData(BulkShopRequest request);

    List<Member> addMember(List<POSMembershipAddMember> request);

    List<Transaction> addToQueue(List<QueueAddMemberBulkRequest> request);

    List<Transaction> processTransaction(List<TransactionBulkRequest> request);

    List<BulkAssetResponse> uploadPhoto(InputStream inputStream, List<String> name, List<String> id, List<String> type, List<String> signed_type, List<FormDataBodyPart> body);


}
