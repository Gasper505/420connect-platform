package com.fourtwenty.core.services.pos;

import com.fourtwenty.core.domain.models.company.Terminal;
import com.fourtwenty.core.rest.dispensary.requests.terminals.TerminalAddRequest;
import com.fourtwenty.core.rest.dispensary.requests.terminals.TerminalSettingsUpdateRequest;
import com.fourtwenty.core.rest.dispensary.requests.terminals.TerminalUpdateNameRequest;
import com.fourtwenty.core.rest.dispensary.requests.terminals.UpdateTerminalLocationRequest;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;

/**
 * Created by mdo on 10/9/15.
 */
public interface TerminalService {
    DateSearchResult<Terminal> getTerminals(long afterDate, long beforeDate);

    SearchResult<Terminal> getAvailableTerminals(String shopId);

    Terminal addTerminal(String shopId, TerminalAddRequest request);

    void updateTerminalName(String shopId, String terminalId, TerminalUpdateNameRequest request);

    void deleteTerminal(String terminalId);

    void updateTerminalSettings(TerminalSettingsUpdateRequest request);

    void updateCurrentTerminalLocation(UpdateTerminalLocationRequest request);

    void refreshTerminalLocation(String terminalId);

    SearchResult<Terminal> getShopAvailableTerminals(String shopId, int start, int limit);
}

