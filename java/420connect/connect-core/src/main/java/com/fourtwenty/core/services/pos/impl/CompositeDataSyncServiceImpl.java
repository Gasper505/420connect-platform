package com.fourtwenty.core.services.pos.impl;

import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.domain.repositories.loyalty.LoyaltyRewardRepository;
import com.fourtwenty.core.domain.repositories.thirdparty.ThirdPartyAccountRepository;
import com.fourtwenty.core.rest.dispensary.results.common.CompositeInventorySyncResult;
import com.fourtwenty.core.rest.dispensary.results.common.CompositeProductInfoSyncResult;
import com.fourtwenty.core.rest.dispensary.results.common.CompositeShopSyncResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.inventory.BatchQuantityService;
import com.fourtwenty.core.services.mgmt.*;
import com.fourtwenty.core.services.pos.CompositeDataSyncService;
import com.fourtwenty.core.services.pos.POSMembershipService;
import com.fourtwenty.core.services.store.ConsumerCartService;
import com.google.inject.Inject;
import com.google.inject.Provider;
import org.joda.time.DateTime;

/**
 * Created by mdo on 5/26/18.
 * <p>
 * Purpose: Combine data sync calls so we can reduce database calls as well as server calls
 */
public class CompositeDataSyncServiceImpl extends AbstractAuthServiceImpl implements CompositeDataSyncService {

    @Inject
    private ShopService shopService;
    @Inject
    private QueuePOSService queuePOSService;
    @Inject
    private POSMembershipService memberService;
    @Inject
    private ConsumerCartService consumerCartService;
    @Inject
    private ProductService productService;
    @Inject
    private BatchQuantityService batchQuantityService;
    @Inject
    private ProductCategoryService productCategoryService;
    @Inject
    private PrepackageProductService prepackageService;
    @Inject
    private PromotionService promotionService;
    @Inject
    private BarcodeService barcodeService;
    @Inject
    private LoyaltyRewardService loyaltyRewardService;
    @Inject
    private CashDrawerService cashDrawerService;
    @Inject
    private InventoryService inventoryService;
    @Inject
    private MemberGroupService memberGroupService;
    @Inject
    private ContractService contractService;
    @Inject
    private DoctorService doctorService;
    @Inject
    private EmployeeService employeeService;
    @Inject
    private VendorService vendorService;
    @Inject
    private ThirdPartyAccountService thirdPartyAccountService;


    @Inject
    TransactionRepository transactionRepository;
    @Inject
    ProductWeightToleranceRepository weightToleranceRepository;
    @Inject
    ProductRepository productRepository;
    @Inject
    BatchQuantityRepository batchQuantityRepository;
    @Inject
    ProductCategoryRepository categoryRepository;
    @Inject
    MemberGroupPricesRepository memberGroupPricesRepository;
    @Inject
    PrepackageRepository prepackageRepository;
    @Inject
    PrepackageProductItemRepository prepackageProductItemRepository;
    @Inject
    PromotionRepository promotionRepository;
    @Inject
    ProductPrepackageQuantityRepository prepackageQuantityRepository;
    @Inject
    BarcodeItemRepository barcodeItemRepository;
    @Inject
    InventoryRepository inventoryRepository;
    @Inject
    ProductBatchRepository batchRepository;
    @Inject
    VendorRepository vendorRepository;
    @Inject
    LoyaltyRewardRepository loyaltyRewardRepository;
    @Inject
    DoctorRepository doctorRepository;
    @Inject
    EmployeeRepository employeeRepository;
    @Inject
    ThirdPartyAccountRepository thirdPartyAccountRepository;


    @Inject
    public CompositeDataSyncServiceImpl(Provider<ConnectAuthToken> tokenProvider) {
        super(tokenProvider);
    }

    @Override
    public CompositeInventorySyncResult getInventorySyncData(long afterDate) {
        final CompositeInventorySyncResult compositeInventorySyncResult = new CompositeInventorySyncResult();

        compositeInventorySyncResult.setTransactions(transactionRepository.findItemsAfter(token.getCompanyId(), token.getShopId(), afterDate));
        compositeInventorySyncResult.setWeightTolerance(weightToleranceRepository.findItemsAfter(token.getCompanyId(), afterDate));
        compositeInventorySyncResult.setProducts(productRepository.findItemsAfter(token.getCompanyId(), token.getShopId(), afterDate));
        compositeInventorySyncResult.setBatchQuantities(batchQuantityRepository.findItemsAfter(token.getCompanyId(), token.getShopId(), afterDate));

        compositeInventorySyncResult.setProductCategories(categoryRepository.findItemsAfter(token.getCompanyId(), token.getShopId(), afterDate));
        compositeInventorySyncResult.setMemberGroupPrices(memberGroupPricesRepository.findItemsAfter(token.getCompanyId(), token.getShopId(), afterDate));


        compositeInventorySyncResult.setPrepackageItems(prepackageProductItemRepository.findItemsAfter(token.getCompanyId(), token.getShopId(), afterDate));
        compositeInventorySyncResult.setPromotions(promotionRepository.findItemsAfter(token.getCompanyId(), token.getShopId(), afterDate));

        compositeInventorySyncResult.setPrepackages(prepackageRepository.findItemsAfter(token.getCompanyId(), token.getShopId(), afterDate));
        compositeInventorySyncResult.setPrepackageQuantities(prepackageQuantityRepository.findItemsAfter(token.getCompanyId(), token.getShopId(), afterDate));

        compositeInventorySyncResult.setBarcodes(barcodeItemRepository.findItemsAfter(token.getCompanyId(), token.getShopId(), afterDate));
        compositeInventorySyncResult.setProductBatches(batchRepository.findItemsAfter(token.getCompanyId(), token.getShopId(), afterDate));
        compositeInventorySyncResult.setVendors(vendorRepository.findItemsAfter(token.getCompanyId(), afterDate));

        return compositeInventorySyncResult;
    }

    @Override
    public CompositeShopSyncResult getShopData(long afterDate) {
        long currentDate = DateTime.now().getMillis();

        final CompositeShopSyncResult compositeShopSyncResult = new CompositeShopSyncResult();

        compositeShopSyncResult.setShops(shopService.getShops(afterDate, currentDate));
        compositeShopSyncResult.setTransactions(transactionRepository.findItemsAfter(token.getCompanyId(), token.getShopId(), afterDate));

        compositeShopSyncResult.setMembers(memberService.getMembershipsForCompanyDate(0, afterDate, 0, 0));

        compositeShopSyncResult.setConsumerCarts(consumerCartService.getConsumerCartByDate(afterDate, currentDate));

        compositeShopSyncResult.setWeightTolerance(weightToleranceRepository.findItemsAfter(token.getCompanyId(), afterDate));

        compositeShopSyncResult.setProducts(productRepository.findItemsAfter(token.getCompanyId(), token.getShopId(), afterDate));
        compositeShopSyncResult.setBatchQuantities(batchQuantityRepository.findItemsAfter(token.getCompanyId(), token.getShopId(), afterDate));

        compositeShopSyncResult.setProductCategories(categoryRepository.findItemsAfter(token.getCompanyId(), token.getShopId(), afterDate));
        compositeShopSyncResult.setMemberGroupPrices(memberGroupPricesRepository.findItemsAfter(token.getCompanyId(), token.getShopId(), afterDate));

        compositeShopSyncResult.setPrepackageItems(prepackageProductItemRepository.findItemsAfter(token.getCompanyId(), token.getShopId(), afterDate));
        compositeShopSyncResult.setPromotions(promotionRepository.findItemsAfter(token.getCompanyId(), token.getShopId(), afterDate));
        compositeShopSyncResult.setPrepackages(prepackageRepository.findItemsAfter(token.getCompanyId(), token.getShopId(), afterDate));

        compositeShopSyncResult.setPrepackageQuantities(prepackageQuantityRepository.findItemsAfter(token.getCompanyId(), token.getShopId(), afterDate));

        compositeShopSyncResult.setBarcodes(barcodeItemRepository.findItemsAfter(token.getCompanyId(), token.getShopId(), afterDate));


        compositeShopSyncResult.setLoyaltyRewards(loyaltyRewardRepository.findItemsAfter(token.getCompanyId(), token.getShopId(), afterDate));


        compositeShopSyncResult.setCashDrawers(cashDrawerService.getCashDrawersByDate(afterDate, currentDate));
        compositeShopSyncResult.setProductBatches(batchRepository.findItemsAfter(token.getCompanyId(), token.getShopId(), afterDate));

        compositeShopSyncResult.setMemberGroups(memberGroupService.getMemberGroupsByDate(afterDate, currentDate));

        compositeShopSyncResult.setContracts(contractService.getCompanyContracts(afterDate, currentDate));

        compositeShopSyncResult.setDoctors(doctorRepository.findItemsAfter(token.getCompanyId(), afterDate));

        compositeShopSyncResult.setEmployees(employeeRepository.findItemsAfter(token.getCompanyId(), afterDate));
        compositeShopSyncResult.setVendors(vendorRepository.findItemsAfter(token.getCompanyId(), afterDate));
        compositeShopSyncResult.setThirdPartyAccounts(thirdPartyAccountRepository.findItemsAfter(token.getCompanyId(), afterDate));
        return compositeShopSyncResult;
    }

    @Override
    public CompositeProductInfoSyncResult getProductInfoSyncData(long afterDate) {
        final CompositeProductInfoSyncResult result = new CompositeProductInfoSyncResult();

        result.setProducts(productRepository.findItemsAfter(token.getCompanyId(), token.getShopId(), afterDate));
        result.setMemberGroupPrices(memberGroupPricesRepository.findItemsAfter(token.getCompanyId(), token.getShopId(), afterDate));
        result.setBatchQuantities(batchQuantityRepository.findItemsAfter(token.getCompanyId(), token.getShopId(), afterDate));
        result.setPrepackageQuantities(prepackageQuantityRepository.findItemsAfter(token.getCompanyId(), token.getShopId(), afterDate));
        result.setWeightTolerance(weightToleranceRepository.findItemsAfter(token.getCompanyId(), afterDate));
        result.setPromotions(promotionRepository.findItemsAfter(token.getCompanyId(), token.getShopId(), afterDate));
        result.setPrepackages(prepackageRepository.findItemsAfter(token.getCompanyId(), token.getShopId(), afterDate));
        result.setPrepackageItems(prepackageProductItemRepository.findItemsAfter(token.getCompanyId(), token.getShopId(), afterDate));
        result.setBarcodes(barcodeItemRepository.findItemsAfter(token.getCompanyId(), token.getShopId(), afterDate));
        result.setProductBatches(batchRepository.findItemsAfter(token.getCompanyId(), token.getShopId(), afterDate));
        result.setLoyaltyRewards(loyaltyRewardRepository.findItemsAfter(token.getCompanyId(), token.getShopId(), afterDate));


        return result;

    }
}
