package com.fourtwenty.core.services.pos.impl;

import com.amazonaws.util.CollectionUtils;
import com.fourtwenty.core.domain.models.company.*;
import com.fourtwenty.core.domain.models.customer.*;
import com.fourtwenty.core.domain.models.generic.Note;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.models.views.MemberLimitedView;
import com.fourtwenty.core.domain.models.views.MemberLimitedViewResult;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.rest.dispensary.requests.company.MembershipUpdateRequest;
import com.fourtwenty.core.rest.dispensary.requests.company.POSMembershipAddMember;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.company.MemberResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.common.BackgroundJobService;
import com.fourtwenty.core.services.mgmt.MemberService;
import com.fourtwenty.core.services.mgmt.RoleService;
import com.fourtwenty.core.services.mgmt.SignedContractService;
import com.fourtwenty.core.services.pos.POSMembershipService;
import com.fourtwenty.core.util.DateUtil;
import com.google.common.collect.Lists;
import com.google.inject.Provider;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.util.*;

/**
 * Created by mdo on 12/13/15.
 */
public class POSMembershipServiceImpl extends AbstractAuthServiceImpl implements POSMembershipService {
    private static final int NUM_MEM_FETCH = 15;
    private static final int MAX_LIMIT = 5000;

    @Inject
    MemberRepository memberRepository;
    @Inject
    MemberService membershipService;
    @Inject
    MemberGroupRepository memberGroupRepository;
    @Inject
    CompanyAssetRepository companyAssetRepository;
    @Inject
    ContractRepository contractRepository;
    @Inject
    BackgroundJobService backgroundJobService;
    @Inject
    DoctorRepository doctorRepository;
    @Inject
    RoleService roleService;
    @Inject
    TransactionRepository transactionRepository;
    @Inject
    SignedContractService signedContractService;
    @Inject
    CompanyRepository companyRepository;
    @Inject
    private ShopRepository shopRepository;

    @Inject
    public POSMembershipServiceImpl(Provider<ConnectAuthToken> token) {
        super(token);
    }


    @Override
    public SearchResult<Member> getMembershipsForCompanyDate(long beforeDate, long afterDate, int start, int limit) {
        SearchResult<Member> memberSearchResult = null;

        int startLimit;
        int endLimit;
        if (start != 0) {
            startLimit = start;
        } else {
            startLimit = 0;
        }

        if (limit <= 0 || limit >= MAX_LIMIT) {
            endLimit = MAX_LIMIT;
        } else {
            endLimit = limit;
        }


        if (roleService.canAccessRestricted(token.getCompanyId(), token.getShopId(), token.getActiveTopUser().getUserId())) {
            if (afterDate > 0) {

                // override dates, always return modified in last 10 minutes
                long tenMinsAgo = DateTime.now().minusMinutes(10).getMillis();
                if (afterDate > tenMinsAgo) {
                    afterDate = tenMinsAgo;
                }

                if (token.getMembersShareOption() == Company.CompanyMembersShareOption.Shared) {
                    memberSearchResult = memberRepository.findItemsAfterLimit(token.getCompanyId(), afterDate,MAX_LIMIT);
                } else {
                    memberSearchResult = memberRepository.findItemsAfterLimit(token.getCompanyId(), token.getShopId(), afterDate,MAX_LIMIT);
                }


                // Add more data
                if (memberSearchResult.getTotal() <= 10) {
                    Set<ObjectId> memberIds = new HashSet<ObjectId>();
                    Iterable<Transaction> transactions = transactionRepository.getAllActiveTransactions(token.getCompanyId(), token.getShopId());
                    for (Transaction t : transactions) {
                        if (t != null && StringUtils.isNotBlank(t.getMemberId()) && ObjectId.isValid(t.getMemberId())) {
                            memberIds.add(new ObjectId(t.getMemberId()));
                        }
                    }
                    if (memberIds.size() <= NUM_MEM_FETCH) {
                        for (Member m : memberSearchResult.getValues()) {
                            memberIds.removeIf((id) -> id.toString().equalsIgnoreCase(m.getId()));
                        }
                        Iterable<Member> members = memberRepository.list(token.getCompanyId(), Lists.newArrayList(memberIds));
                        List<Member> items = Lists.newArrayList(members);

                        memberSearchResult.getValues().addAll(items);
                        memberSearchResult.setTotal(memberSearchResult.getTotal() + items.size());
                    }
                }
            } else {
                if (token.getMembersShareOption() == Company.CompanyMembersShareOption.Shared) {
                    memberSearchResult = memberRepository.findItemsWithDateAndLimit(token.getCompanyId(), afterDate, beforeDate, startLimit, endLimit);
                } else {
                    memberSearchResult = memberRepository.findItemsWithDateAndLimit(token.getCompanyId(), token.getShopId(), afterDate, beforeDate, startLimit, endLimit);
                }
            }
        } else {
            DateSearchResult<Transaction> trans = null;
            if (afterDate > 0) {
                DateTime after = DateTime.now().minusWeeks(1);
                trans = transactionRepository.getAllTransactionsAssigned(token.getCompanyId(), token.getShopId(), token.getActiveTopUser().getUserId(),
                        after.getMillis(), DateTime.now().getMillis(), startLimit, endLimit);

            } else {
                DateTime after = DateTime.now().minusWeeks(2);
                trans = transactionRepository.getAllTransactionsAssigned(token.getCompanyId(), token.getShopId(), token.getActiveTopUser().getUserId(),
                        after.getMillis(), DateTime.now().getMillis(), startLimit, endLimit);
            }

            List<ObjectId> objectIdList = new ArrayList<>();
            for (Transaction transaction : trans.getValues()) {
                if (StringUtils.isNotBlank(transaction.getMemberId())) {
                    objectIdList.add(new ObjectId(transaction.getMemberId()));
                }
            }

            Iterable<Member> members = memberRepository.list(token.getCompanyId(), objectIdList);
            memberSearchResult = new SearchResult<>();
            memberSearchResult.setValues(Lists.newArrayList(members));
            memberSearchResult.setTotal((long) memberSearchResult.getValues().size());
            memberSearchResult.setSkip(0);
            memberSearchResult.setLimit(memberSearchResult.getTotal().intValue());
        }

        membershipService.prepareDataForView(memberSearchResult, false);

        return memberSearchResult;
    }


    @Override
    public Member addMembership(POSMembershipAddMember addRequest) {
        Member member = memberRepository.get(token.getCompanyId(), addRequest.getId());

        // Id doesn't exist so it means we're trying to create a new member. check for existing email
        // StringUtils.isNotEmpty(addRequest.getEmail()) ? addRequest.getEmail().toLowerCase() : "";

        if (StringUtils.isNotBlank(addRequest.getEmail())) {
            String incomingEmail = addRequest.getEmail().toLowerCase().trim();
            if (member == null || !incomingEmail.equalsIgnoreCase(member.getEmail())) {
                long membercount = 0;
                if (token.getMembersShareOption() == Company.CompanyMembersShareOption.Shared) {
                    membercount = memberRepository.countMemberByEmail(token.getCompanyId(), incomingEmail);
                } else {
                    membercount = memberRepository.countMemberByEmail(token.getCompanyId(), token.getShopId(), incomingEmail);
                }

                //if (membercount > 0) {
                //    throw new BlazeInvalidArgException("New Member", "Member with this email already exist for this shop.");
                //}
            }
            addRequest.setEmail(incomingEmail);
        }

        if (member != null) {
            if (member.getIdentifications() == null) {
                member.setIdentifications(new ArrayList<Identification>());
            }

            if (addRequest.getIdentifications() == null) {
                addRequest.setIdentifications(new ArrayList<Identification>());
            }
        }

        // HOT-FIX: override in case memberId is the same
        /*
        if (member != null) {
            /// drivers license, dob, and first,last name are different, than different people
            if (member.getDob() != null && member.getDob() != addRequest.getDob()) {
                String oldName = member.getFirstName() + "_" + member.getLastName();
                String newName = addRequest.getFirstName() + "_" + addRequest.getLastName();

                // if name are different
                if (!oldName.equalsIgnoreCase(newName)) {
                    // check if id are different
                    String oldLicense = "";
                    String newLicense = "";

                    if (member.getIdentifications().size() > 0) {
                        Identification oldID = member.getIdentifications().get(0);

                        if (oldID != null && BLStringUtils.isNotBlank(oldID.getLicenseNumber())) {
                            oldLicense = oldID.getLicenseNumber();
                        }
                    }


                    Identification newId = addRequest.getIdentifications().get(0);
                    if (newId == null && BLStringUtils.isNotBlank(newId.getLicenseNumber())) {
                        newLicense = newId.getLicenseNumber();
                    }

                    // everything is different or license is empty, so set member = null to add a new member
                    if (!oldLicense.equalsIgnoreCase(newLicense)
                            || (BLStringUtils.isNotBlank(oldLicense) && BLStringUtils.isNotBlank(newLicense))) {
                        member = null;
                    }
                }
            }

        }*/
        Company company = companyRepository.getById(token.getCompanyId());
        if (company != null) {
            addRequest.setEnableLoyalty(company.isEnableLoyalty());
        }

        //Create Signed Contract
        SignedContract signedContract = null;

        // If shop contract id is exist and any of the asset ID exist then new agreement is added and add agreement code will run
        // Edit agreement feature does not exist
        if (addRequest.getShopContractId() != null && (addRequest.getSignatureAssetId() != null || addRequest.getEmployeeSignatureAssetId() != null || addRequest.getWitnessSignatureAssetId() != null)) {
            CompanyAsset companyAsset = companyAssetRepository.get(token.getCompanyId(), addRequest.getSignatureAssetId());
            CompanyAsset employeeSignature = companyAssetRepository.get(token.getCompanyId(), addRequest.getEmployeeSignatureAssetId());
            CompanyAsset witnessSignature = companyAssetRepository.get(token.getCompanyId(), addRequest.getWitnessSignatureAssetId());

            signedContract = new SignedContract();
            signedContract.prepare(token.getCompanyId());
            signedContract.setShopId(token.getShopId());
            signedContract.setContractId(addRequest.getShopContractId());
            signedContract.setSignaturePhoto(companyAsset);
            signedContract.setSignedDate(DateTime.now().getMillis());
            signedContract.setWitnessSignaturePhoto(witnessSignature);
            signedContract.setEmployeeSignaturePhoto(employeeSignature);
            signedContract.setEmployeeName(addRequest.getEmployeeName());
            signedContract.setWitnessName(addRequest.getWitnessName());
            if (member != null) {
                signedContract.setMembershipId(member.getId());
            }
        }

        List<Note> noteList = new ArrayList<Note>();
        if (addRequest.getNotes() != null && addRequest.getNotes().size() > 0) {
            for (Note note : addRequest.getNotes()) {
                noteList.add(note);
            }
            member.setNotes(noteList);
        }


        Member newMember = membershipService.addOrUpdateMember(member, addRequest, signedContract);

        // Sign the Contract
        return newMember;
    }

    @Override
    public Member updateMember(String memberId, MembershipUpdateRequest member) {
        return membershipService.updateMembership(memberId, member);
    }


    @Override
    public Member sanitize(BaseMember baseMember) {
        // let's add the member
        Member member = new Member();
        //set fields
        baseMember.setCompanyId(token.getCompanyId());
        member.setCompanyId(token.getCompanyId());
        member.setShopId(token.getShopId());
        //member.setCustomer(baseMember);
        member.setStatus(Member.MembershipStatus.Active);

        return member;
    }

    @Override
    public SearchResult<MemberLimitedViewResult> getMembersForDoctorId(String doctorId, String term, int start, int limit) {
        if (start < 0) start = 0;
        if (limit <= 0 || limit > 500) {
            limit = 200;
        }

        SearchResult<Member> memberSearchResult;

        if (token.getMembersShareOption() == Company.CompanyMembersShareOption.Shared) {
            memberSearchResult = StringUtils.isBlank(term) ?
                    memberRepository.getMembersWithDoctorId(token.getCompanyId(), doctorId, start, limit) :
                    memberRepository.getMembersWithDoctorIdWithShop(token.getCompanyId(), doctorId, term, start, limit);
        } else {
            memberSearchResult = StringUtils.isBlank(term) ?
                    memberRepository.getMembersWithDoctorIdWithShop(token.getCompanyId(), token.getShopId(), doctorId, start, limit)  :
                    memberRepository.getMembersWithDoctorIdWithShop(token.getCompanyId(), token.getShopId(), doctorId, term, start, limit);

        }


        return prepareDataForView(memberSearchResult);
    }

    @Override
    public SearchResult<MemberLimitedViewResult> getMembershipsForActiveShop(String term, int start, int limit,long beforeDate, long afterDate) {
        if (start < 0) start = 0;
        if (limit <= 0 || limit > MAX_LIMIT) {
            limit = MAX_LIMIT;
        }

        SearchResult<Member> memberSearchResult;

        if (!roleService.canAccessRestricted(token.getCompanyId(), token.getShopId(), token.getActiveTopUser().getUserId())) {
            Iterable<Transaction> transactions = transactionRepository.getAllActiveTransactionsForAssignedEmployee(token.getCompanyId(), token.getShopId(), token.getActiveTopUser().getUserId());

            List<ObjectId> memberIds = new ArrayList<>();

            for (Transaction transaction : transactions) {
                if (StringUtils.isBlank(transaction.getMemberId()) && !ObjectId.isValid(transaction.getMemberId())) {
                    continue;
                }
                memberIds.add(new ObjectId(transaction.getMemberId()));
            }
            memberSearchResult = memberRepository.findItemsWithSort(token.getCompanyId(), memberIds, "{firstName:1}", start, limit);

        } else {
            if (token.getMembersShareOption() == Company.CompanyMembersShareOption.Shared) {
                memberSearchResult = (StringUtils.isBlank(term)) ?
                        memberRepository.findItemsWithDateAndLimit(token.getCompanyId(), afterDate, beforeDate, start, limit) :
                        memberRepository.searchMemberships(token.getCompanyId(), term, start, limit);
            } else {
                memberSearchResult = (StringUtils.isBlank(term)) ?
                        memberRepository.findItemsWithDateAndLimit(token.getCompanyId(), token.getShopId(), afterDate, beforeDate, start, limit) :
                        memberRepository.searchMembershipsWithShop(token.getCompanyId(), token.getShopId(), term, start, limit);
            }

        }

        SearchResult<MemberLimitedViewResult> result = prepareDataForView(memberSearchResult);
        result.getValues().sort(Comparator.comparing(MemberLimitedView::getFirstName));
        return result;
    }

    @Override
    public MemberResult getMembership(String memberId) {
        return membershipService.getMembership(memberId,false,false);
    }

    private SearchResult<MemberLimitedViewResult> prepareDataForView(SearchResult<Member> memberSearchResult) {
        HashMap<String, Doctor> doctorHashMap = doctorRepository.listAllAsMap(token.getCompanyId());
        List<Contract> contracts = contractRepository.getActiveContracts(token.getCompanyId(), token.getShopId());
        HashMap<String, MemberGroup> memberGroupHashMap = memberGroupRepository.listAsMap(token.getCompanyId());
        SearchResult<Transaction> activeTransactions = transactionRepository.getActiveTransactions(token.getCompanyId(), token.getShopId());

        HashMap<String, Integer> memberTransCount = new HashMap<>();

        for (Transaction transaction : activeTransactions.getValues()) {
            memberTransCount.put(transaction.getMemberId(), memberTransCount.getOrDefault(transaction.getMemberId(), 0) + 1);
        }

        Contract activeContract = null;
        if (contracts.size() > 0) {
            activeContract = contracts.get(0);
        }

        long currentDate = DateTime.now().getMillis();
        long in30DaysDate = DateTime.now().plusDays(30).getMillis();
        SearchResult<MemberLimitedViewResult> limitedViewSearchResult = new SearchResult<>();

        for (Member member : memberSearchResult.getValues()) {
            boolean isRecExpired = true;
            boolean isAgreementExpired = false;
            long recExpiryLeft = 0;

            if (member.getAddress() != null) {
                member.getAddress().clean();
            }
            MemberLimitedViewResult memberLimitedView = new MemberLimitedViewResult();
            memberLimitedView.setId(member.getId());

            if (!CollectionUtils.isNullOrEmpty(member.getIdentifications())) {
                for (Identification identification : member.getIdentifications()) {

                    if (memberLimitedView.getFrontPhoto() == null) {
                        memberLimitedView.setFrontPhoto(identification.getFrontPhoto());
                    }

                }
            }

            String licenseNumber = "";
            if (member.getIdentifications().isEmpty()) {
                member.getExpStatuses().add("DL Missing");
            } else if (!member.getIdentifications().isEmpty()) {
                Identification lastId = member.getIdentifications().get(member.getIdentifications().size() - 1);
                if (lastId.getExpirationDate() == null) {
                    member.getExpStatuses().add("DL Expiration date missing.");
                } else if (lastId.getExpirationDate() < in30DaysDate) {
                    member.getExpStatuses().add("DL Expires: " + DateFormatUtils.format(lastId.getExpirationDate(), "MM/dd/yyyy"));
                }
                licenseNumber = lastId.getLicenseNumber();
            }

            if (!CollectionUtils.isNullOrEmpty(member.getRecommendations())) {
                for (Recommendation recommendation : member.getRecommendations()) {
                    recommendation.setDoctor(doctorHashMap.get(recommendation.getDoctorId()));

                    if (recommendation.getExpirationDate() != null && recommendation.getExpirationDate() >= currentDate) {
                        recExpiryLeft = DateUtil.getStandardDaysBetweenTwoDates(currentDate, recommendation.getExpirationDate());
                        isRecExpired = false;
                    }
                }
            }

            if (member.isMedical() && CollectionUtils.isNullOrEmpty(member.getRecommendations())) {
                member.getExpStatuses().add("Recommendation: Missing");
            } else if (member.isMedical() && !CollectionUtils.isNullOrEmpty(member.getRecommendations())) {
                Recommendation rec = member.getRecommendations().get(member.getRecommendations().size() - 1);

                if (Objects.isNull(rec.getExpirationDate())) {
                    member.getExpStatuses().add("Rec Expiration date missing.");
                } else if (rec.getExpirationDate() < in30DaysDate) {
                    member.getExpStatuses().add("Rec Expires: " + DateFormatUtils.format(rec.getExpirationDate(), "MM/dd/yyyy"));
                }

            }


            // active and required
            String agreeExp = "";
            if (activeContract != null && activeContract.isRequired()) {
                if (CollectionUtils.isNullOrEmpty(member.getContracts())) {
                    agreeExp = "New Required Agreement";
                    isAgreementExpired = true;
                    for (SignedContract signedContract : member.getContracts()) {
                        if (signedContract != null && StringUtils.isNotBlank(signedContract.getContractId())) {
                            if (signedContract.getContractId().equalsIgnoreCase(activeContract.getId())) {
                                // agreement match.
                                isAgreementExpired = false;
                                agreeExp = "";
                                break;
                            }
                        }
                    }
                } else {
                    isAgreementExpired = true;
                    agreeExp = "Agreement Missing";
                }
            }

            if (StringUtils.isNotEmpty(agreeExp)) {
                member.getExpStatuses().add(agreeExp);
            }

            memberLimitedView.setId(member.getId());
            memberLimitedView.setCompanyId(member.getCompanyId());
            memberLimitedView.setCreated(member.getCreated());
            memberLimitedView.setModified(member.getModified());
            memberLimitedView.setFirstName(member.getFirstName());
            memberLimitedView.setLastName(member.getLastName());
            memberLimitedView.setPrimaryPhone(member.getPrimaryPhone());
            memberLimitedView.setExpStatuses(member.getExpStatuses());
            memberLimitedView.setEmail(member.getEmail());
            memberLimitedView.setLoyaltyPoints(member.getLoyaltyPoints());
            memberLimitedView.setMedical(member.isMedical());
            memberLimitedView.setStartDate(member.getStartDate());
            memberLimitedView.setLicenseNumber(licenseNumber);
            memberLimitedView.setStatus(member.getStatus());
            memberLimitedView.setBan(member.isBanPatient());
            memberLimitedView.setAgreementExpired(isAgreementExpired);
            memberLimitedView.setRecommendationExpired(isRecExpired);
            memberLimitedView.setRecommendationExpiryLeft(recExpiryLeft);
            memberLimitedView.setLastVisitDate(member.getLastVisitDate());
            memberLimitedView.setEmail(member.getEmail());
            memberLimitedView.setConsumerType(member.getConsumerType());
            memberLimitedView.setDeleted(member.isDeleted());
            memberLimitedView.setPrimaryPhone(member.getPrimaryPhone());
            memberLimitedView.setStartDate(member.getStartDate());
            memberLimitedView.setDob(member.getDob());
            memberLimitedView.setIdentifications(member.getIdentifications());

            memberLimitedView.setActiveTransaction(memberTransCount.getOrDefault(member.getId(), 0));
            memberLimitedView.setInQueue(memberLimitedView.getActiveTransaction() > 0);

            MemberGroup memberGroup = memberGroupHashMap.get(member.getMemberGroupId());
            if (memberGroup != null) {
                memberLimitedView.setMemberGroupName(memberGroup.getName());
            }

            limitedViewSearchResult.getValues().add(memberLimitedView);
        }

        limitedViewSearchResult.setTotal(memberSearchResult.getTotal());

        return limitedViewSearchResult;
    }

}
