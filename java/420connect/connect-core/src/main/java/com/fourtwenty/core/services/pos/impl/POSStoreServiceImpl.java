package com.fourtwenty.core.services.pos.impl;

import com.fourtwenty.core.domain.models.consumer.ConsumerUser;
import com.fourtwenty.core.domain.models.store.ConsumerCart;
import com.fourtwenty.core.domain.repositories.store.ConsumerCartRepository;
import com.fourtwenty.core.domain.repositories.store.ConsumerUserRepository;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.shop.ConsumerCartResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.pos.POSStoreService;
import com.google.inject.Provider;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by mdo on 6/7/17.
 */
public class POSStoreServiceImpl extends AbstractAuthServiceImpl implements POSStoreService {

    @Inject
    ConsumerCartRepository consumerCartRepository;
    @Inject
    ConsumerUserRepository consumerUserRepository;

    @Inject
    public POSStoreServiceImpl(Provider<ConnectAuthToken> tokenProvider) {
        super(tokenProvider);
    }

    @Override
    public DateSearchResult<ConsumerCartResult> getConsumerCarts(long afterDate, long beforeDate) {

        // only get online orders within 2 weeks
        DateTime after = DateTime.now().minusWeeks(2);
        if (afterDate < after.getMillis()) {
            afterDate = after.getMillis();
        }

        DateSearchResult<ConsumerCartResult> results = consumerCartRepository.fetchOrders(token.getCompanyId(), token.getShopId(),
                afterDate, beforeDate, ConsumerCartResult.class);


        List<ObjectId> consumerIds = new ArrayList<>();
        for (ConsumerCart consumerCart : results.getValues()) {
            if (consumerCart.getConsumerId() != null && ObjectId.isValid(consumerCart.getConsumerId())) {
                consumerIds.add(new ObjectId(consumerCart.getConsumerId()));
            }
        }

        HashMap<String, ConsumerUser> consumerUserHashMap = consumerUserRepository.findItemsInAsMap(consumerIds);
        // show agree or non-agree
        for (ConsumerCartResult consumerCart : results.getValues()) {

            ConsumerUser cu = consumerUserHashMap.get(consumerCart.getConsumerId());
            if (cu != null) {
                consumerCart.setMembershipAccepted(false);
                consumerCart.setConsumerUser(cu);
                cu.setPassword(null);
                consumerCart.setMembershipAccepted(cu.isAccepted());
            }
        }
        return results;
    }

}
