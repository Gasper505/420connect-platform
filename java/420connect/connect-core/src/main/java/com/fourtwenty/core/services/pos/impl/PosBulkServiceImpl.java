package com.fourtwenty.core.services.pos.impl;

import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.rest.dispensary.requests.company.MembershipUpdateRequest;
import com.fourtwenty.core.rest.dispensary.requests.company.POSMembershipAddMember;
import com.fourtwenty.core.rest.dispensary.requests.queues.QueueAddMemberBulkRequest;
import com.fourtwenty.core.rest.dispensary.requests.shop.BulkAssetResponse;
import com.fourtwenty.core.rest.dispensary.requests.shop.BulkShopRequest;
import com.fourtwenty.core.rest.dispensary.requests.shop.BulkShopResponse;
import com.fourtwenty.core.rest.dispensary.requests.transaction.TransactionBulkRequest;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.mgmt.AssetService;
import com.fourtwenty.core.services.mgmt.QueuePOSService;
import com.fourtwenty.core.services.pos.POSMembershipService;
import com.fourtwenty.core.services.pos.PosBulkService;
import com.google.inject.Provider;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class PosBulkServiceImpl extends AbstractAuthServiceImpl implements PosBulkService {

    private static final Logger LOG = LoggerFactory.getLogger(PosBulkServiceImpl.class);

    @Inject
    POSMembershipService membershipService;

    @Inject
    QueuePOSService queuePOSService;

    @Inject
    AssetService assetService;

    @com.google.inject.Inject
    public PosBulkServiceImpl(Provider<ConnectAuthToken> tokenProvider) {
        super(tokenProvider);
    }

    @Override
    public List<Member> addMember(List<POSMembershipAddMember> addMemberlst) {
        List<Member> members = new ArrayList<>();
        for (POSMembershipAddMember addMember : addMemberlst) {
            addMember.setTextOptIn(true); // auto opt-in
            addMember.setEmailOptIn(true); // auto opt-in
            members.add(membershipService.addMembership(addMember));
        }
        return members;
    }

    @Override
    public List<Transaction> addToQueue(List<QueueAddMemberBulkRequest> queueAddMemberLst) {
        List<Transaction> transactions = new ArrayList<>();
        for (QueueAddMemberBulkRequest queueAddMember : queueAddMemberLst) {
            transactions.add(queuePOSService.addToQueue(queueAddMember.getTransID(), queueAddMember.getQueueName(), queueAddMember.getQueueAddMemberRequest()));
        }
        return transactions;
    }

    @Override
    public List<Transaction> processTransaction(List<TransactionBulkRequest> transactionlst) {
        List<Transaction> transactions = new ArrayList<>();
        for (TransactionBulkRequest transaction : transactionlst) {
            transactions.add(queuePOSService.completeTransaction(transaction.getTransactionId(), transaction.getTransaction(), false));
        }
        return transactions;
    }

    @Override
    public List<BulkAssetResponse> uploadPhoto(InputStream inputStream, List<String> name, List<String> id, List<String> type, List<String> signed_type, List<FormDataBodyPart> body) {
        List<BulkAssetResponse> bulkAssetResponseList = new ArrayList<>();
        for (int i = 0; i < body.size(); i++) {
            String mimeType = body.get(i).getMediaType().toString();
            InputStream is = body.get(i).getEntityAs(InputStream.class);

            BulkAssetResponse assetResponse = new BulkAssetResponse();
            if (type.get(i).equals(BulkAssetResponse.MEMBER) || type.get(i).equals(BulkAssetResponse.MEMBER_UPDATE)) {
                assetResponse.setCompanyAsset(assetService.uploadAssetPrivate(is, name.get(i), CompanyAsset.AssetType.Photo, mimeType));
                assetResponse.setSigned_type(signed_type.get(i));

            } else if (type.get(i).equals(BulkAssetResponse.TRANSACTION)) {
                assetResponse.setCompanyAsset(assetService.uploadAssetPublic(is, name.get(i), CompanyAsset.AssetType.Photo, mimeType));
            }

            assetResponse.setType(type.get(i));
            assetResponse.setId(id.get(i));
            bulkAssetResponseList.add(assetResponse);
        }

        return bulkAssetResponseList;
    }

    @Override
    public BulkShopResponse syncData(BulkShopRequest bulkShopRequests) {
        BulkShopResponse bulkShopResponse = new BulkShopResponse();

        // Add Members
        List<Member> members = new ArrayList<>();
        List<POSMembershipAddMember> addMemberlst = bulkShopRequests.getMembers();
        for (POSMembershipAddMember addMember : addMemberlst) {
            addMember.setTextOptIn(true); // auto opt-in
            addMember.setEmailOptIn(true); // auto opt-in
            try {
                members.add(membershipService.addMembership(addMember));
            } catch (Exception e) {
                LOG.error(String.format("Token: %s, Add Member: %s", token.getCompanyId(), e.getMessage()), e);
                bulkShopResponse.addMemberError(addMember, e.getMessage() == null ? "Internal Server Error" : e.getMessage());
            }
        }


        // Run membership updates
        List<MembershipUpdateRequest> updateMembersList = bulkShopRequests.getUpdatedMembers();
        for (MembershipUpdateRequest member : updateMembersList) {
            try {
                members.add(membershipService.updateMember(member.getId(), member));
            } catch (Exception e) {
                LOG.error(String.format("Token: %s, Update Member: %s", token.getCompanyId(), e.getMessage()), e);
                bulkShopResponse.addUpdateMemberError(member, e.getMessage() == null ? "Internal Server Error" : e.getMessage());
            }
        }

        bulkShopResponse.setMembers(members);


        // Add member to queue
        List<QueueAddMemberBulkRequest> queueAddMemberLst = new ArrayList();
        List<Transaction> queuetransactions = new ArrayList<>();
        for (QueueAddMemberBulkRequest queueAddMember : bulkShopRequests.getQueueAddMembers()) {
            try {
                queuetransactions.add(queuePOSService.handleAddToQueueForBulk(queueAddMember.getTransID(), queueAddMember.getQueueName(), queueAddMember.getQueueAddMemberRequest()));
                queueAddMemberLst.add(queueAddMember);
            } catch (Exception e) {
                LOG.error(String.format("Token: %s, Add to Queue: %s", token.getCompanyId(), e.getMessage()), e);
                bulkShopResponse.addQueueMemberError(queueAddMember, e.getMessage() == null ? "Internal Server Error" : e.getMessage());
            }
        }
        bulkShopResponse.setQueueAddMemberLst(queueAddMemberLst);

        // Update transactions
        List<TransactionBulkRequest> transactionlst = bulkShopRequests.getTransactions();
        List<Transaction> saletransactions = new ArrayList<>();
        for (TransactionBulkRequest transaction : transactionlst) {
            if (transaction.getTransaction() != null) {
                try {
                    if (transaction.getTransaction().getStatus() == Transaction.TransactionStatus.Completed) {
                        saletransactions.add(queuePOSService.completeTransaction(transaction.getTransactionId(), transaction.getTransaction(), true));
                    } else if (transaction.getTransaction().getStatus() == Transaction.TransactionStatus.Hold) {
                        saletransactions.add(queuePOSService.holdTransaction(transaction.getTransactionId(), transaction.getTransaction()));
                    }
                } catch (Exception e) {
                    LOG.error(String.format("Token: %s, Complete/Hold Trans: %s", token.getCompanyId(), e.getMessage()), e);
                    bulkShopResponse.addTransactionError(transaction, e.getMessage() == null ? "Internal Server Error" : e.getMessage());
                }
            }
        }
        bulkShopResponse.setTransactionlst(saletransactions);

        return bulkShopResponse;
    }
}
