package com.fourtwenty.core.services.pos.impl;


import com.fourtwenty.core.domain.models.company.*;
import com.fourtwenty.core.domain.models.product.Inventory;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.managed.PushNotificationManager;
import com.fourtwenty.core.rest.dispensary.requests.terminals.*;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.common.RealtimeService;
import com.fourtwenty.core.services.mgmt.RoleService;
import com.fourtwenty.core.services.pos.TerminalService;
import com.google.common.collect.Lists;
import com.google.inject.Provider;
import com.fourtwenty.core.domain.models.transaction.FcmPayload;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.util.UUID;

/**
 * Created by mdo on 10/9/15.
 */
public class TerminalServiceImpl extends AbstractAuthServiceImpl implements TerminalService {
    @Inject
    TerminalRepository terminalRepository;
    @Inject
    InventoryRepository inventoryRepository;
    @Inject
    EmployeeRepository employeeRepository;
    @Inject
    RoleService roleService;
    @Inject
    CompanyFeaturesRepository companyFeaturesRepository;
    @Inject
    RealtimeService realtimeService;
    @Inject
    TerminalLocationRepository terminalLocationRepository;
    @Inject
    PushNotificationManager pushNotificationManager;
    @Inject
    ShopRepository shopRepository;
    @Inject
    TimeCardRepository timeCardRepository;
    @Inject
    private TransactionRepository transactionRepository;


    @Inject
    public TerminalServiceImpl(Provider<ConnectAuthToken> token) {
        super(token);
    }

    @Override
    public SearchResult<Terminal> getAvailableTerminals(String shopId) {
        SearchResult<Terminal> terminalList = terminalRepository.findItems(token.getCompanyId(), 0, Integer.MAX_VALUE);

        // There should not be that many terminals
        for (Terminal terminal : terminalList.getValues()) {
            // top 5
            Iterable<TerminalLocation> locations = terminalLocationRepository.getTermLocations(token.getCompanyId(),
                    terminal.getShopId(),
                    terminal.getId(),
                    5);
            terminal.setTerminalLocations(Lists.newArrayList(locations));
        }

        return terminalList;
    }

    @Override
    public DateSearchResult<Terminal> getTerminals(long afterDate, long beforeDate) {
        DateSearchResult<Terminal> results = terminalRepository.findItemsWithDate(token.getCompanyId(), afterDate, beforeDate);

        // There should not be that many terminals
        for (Terminal terminal : results.getValues()) {
            // top 5
            Iterable<TerminalLocation> locations = terminalLocationRepository.getTermLocations(token.getCompanyId(),
                    terminal.getShopId(),
                    terminal.getId(),
                    5);
            terminal.setTerminalLocations(Lists.newArrayList(locations));
        }

        return results;
    }

    @Override
    public Terminal addTerminal(String shopId, TerminalAddRequest request) {
        if (request == null
                || StringUtils.isBlank(request.getName())) {
            throw new BlazeInvalidArgException("TerminalAddRequest", "Invalid arguments");
        }

        // Make sure we can add more terminals
        CompanyFeatures companyFeatures = companyFeaturesRepository.getCompanyFeature(token.getCompanyId());
        long count = terminalRepository.count(token.getCompanyId());

        if (companyFeatures.getMaxTerminals() <= count) {
            throw new BlazeInvalidArgException("Terminal", "Max terminals reached.");
        }

        Terminal dbTerminal = terminalRepository.getTerminalByName(token.getCompanyId(), request.getName());
        if (dbTerminal != null) {
            throw new BlazeInvalidArgException("Terminal", "Terminal already exist with this name.");
        }

        Inventory inventory = null;
        boolean active = true;
        if (request instanceof TerminalAdminAddRequest) {
            TerminalAdminAddRequest addRequest = (TerminalAdminAddRequest) request;
            inventory = inventoryRepository.get(token.getCompanyId(), addRequest.getAssignedInventoryId());
            active = addRequest.isActive();
            // Override shop if one exist
            if (StringUtils.isNotEmpty(addRequest.getShopId())) {
                shopId = addRequest.getShopId();

                boolean exists = shopRepository.exist(shopId);
                if (!exists) {
                    throw new BlazeInvalidArgException("AddTerminal", "Invalid shop id.");
                }
            }
        }


        if (inventory == null) {
            inventory = inventoryRepository.getInventory(token.getCompanyId(), shopId, Inventory.SAFE);
        }

        Shop shop = shopRepository.get(token.getCompanyId(), shopId);
        Terminal terminal = new Terminal();
        terminal.prepare();
        terminal.setCompanyId(token.getCompanyId());
        terminal.setShopId(shopId);
        terminal.setActive(active);
        terminal.setAppVersion(request.getAppVersion());
        terminal.setDeviceId(request.getDeviceId());
        terminal.setDeviceModel(request.getDeviceModel());
        terminal.setDeviceToken(request.getDeviceToken());
        terminal.setDeviceVersion(request.getDeviceVersion());
        terminal.setDeviceName(request.getDeviceName());
        terminal.setName(request.getName());
        terminal.setCheckoutType(request.getCheckoutType());
        terminal.setOnPremEnabled(request.isOnPremEnabled());
        /*if (terminal.getCheckoutType() == null && shop != null) {
            terminal.setCheckoutType(shop.getCheckoutType());
        }*/
        if (shop != null) {
            terminal.setCheckoutType(shop.getCheckoutType());
        }
        if (terminal.getCheckoutType() == null) {
            terminal.setCheckoutType(Shop.ShopCheckoutType.Direct);
        }

        if (inventory != null) {
            terminal.setAssignedInventoryId(inventory.getId());
        }

        // generate a random deviceId
        if (StringUtils.isBlank(terminal.getDeviceId())) {
            terminal.setDeviceId(UUID.randomUUID().toString());
        }


        terminalRepository.save(terminal);
        // Notify that inventory has been updated
        realtimeService.sendRealTimeEvent(shopId,
                RealtimeService.RealtimeEventType.TerminalsUpdateEvent, null);
        return terminal;
    }


    @Override
    public void updateTerminalName(String shopId, String terminalId, TerminalUpdateNameRequest request) {
        Boolean isNameAlreadyExist = false;
        if (request == null
                || StringUtils.isBlank(request.getName())) {
            throw new BlazeInvalidArgException("TerminalUpdateRequest", "Invalid arguments");
        }
        Terminal dbTerminal = terminalRepository.get(token.getCompanyId(), terminalId);
        if (dbTerminal == null) {
            throw new BlazeInvalidArgException("Terminal", "Terminal does not exist.");
        }

        //check updated name is duplicate or not
        if (!request.getName().equals(dbTerminal.getName())) {
            isNameAlreadyExist = terminalRepository.isNameAlreadyExist(request.getName());
        }

        if (isNameAlreadyExist) {
            throw new BlazeInvalidArgException("Terminal", "Terminal already exist with this name.");
        }

        dbTerminal.setName(request.getName());
        dbTerminal.setActive(request.isActive());

        Shop shop = shopRepository.get(token.getCompanyId(), request.getShopId());
        if (shop != null) {
            dbTerminal.setShopId(shop.getId());
        }

        Inventory inventory = inventoryRepository.get(token.getCompanyId(), request.getAssignedInventoryId());
        if (inventory != null) {
            dbTerminal.setAssignedInventoryId(inventory.getId());
        }
        dbTerminal.setCvAccountId(request.getCvAccountId());
        if (dbTerminal.getCheckoutType() != request.getCheckoutType()) {
            // checkout type is different, let's make sure nothing is in the queue
            long count = transactionRepository.getAllActiveTransactionsCountForTerminal(token.getCompanyId(), shopId, dbTerminal.getId());
            if (count > 0) {
                throw new BlazeInvalidArgException("TransferRequest", "Please clear all transactions from the queue before changing the checkout type.");
            }
        }
        //dbTerminal.setCheckoutType(request.getCheckoutType());
        dbTerminal.setOnPremEnabled(request.isOnPremEnabled());

        dbTerminal.setCheckoutType(request.getCheckoutType());
        /*if (shop != null) {
            dbTerminal.setCheckoutType(shop.getCheckoutType());
        }*/

        terminalRepository.update(token.getCompanyId(), terminalId, dbTerminal);
        // Notify that inventory has been updated
        realtimeService.sendRealTimeEvent(shopId,
                RealtimeService.RealtimeEventType.TerminalsUpdateEvent, null);
    }


    @Override
    public void deleteTerminal(String terminalId) {
        Terminal dbTerminal = terminalRepository.get(token.getCompanyId(), terminalId);
        if (dbTerminal == null) {
            throw new BlazeInvalidArgException("Terminal", "Invalid terminal id.");
        }

        String curName = dbTerminal.getName();
        String newName = String.format("%s [DELETED] - %s", curName, DateTime.now().toString());
        dbTerminal.setName(newName);
        dbTerminal.setActive(false);
        dbTerminal.setDeleted(true);
        terminalRepository.update(token.getCompanyId(), dbTerminal.getId(), dbTerminal);
    }

    @Override
    public void updateTerminalSettings(TerminalSettingsUpdateRequest request) {
        if (request == null
                || StringUtils.isBlank(request.getTerminalId())
                || StringUtils.isBlank(request.getInventoryId())) {
            throw new BlazeInvalidArgException("Request", "Missing TerminalId or InventoryId");
        }

        Terminal terminal = terminalRepository.get(token.getCompanyId(), request.getTerminalId());
        if (terminal == null) {
            throw new BlazeInvalidArgException("Terminal", "Invalid terminal id.");
        }
        Inventory inventory = inventoryRepository.get(token.getCompanyId(), request.getInventoryId());
        if (inventory == null) {
            throw new BlazeInvalidArgException("Inventory", "Invalid inventory.");
        }

        Employee employee = employeeRepository.get(token.getCompanyId(), token.getActiveTopUser().getUserId());
        roleService.checkPermission(token.getCompanyId(), employee.getRoleId(), Role.Permission.iPadManageSettingsView);

        Terminal oldTerminal = terminalRepository.getTerminalByDeviceId(token.getCompanyId(), request.getDeviceId());
        if (oldTerminal != null) {
            oldTerminal.setDeviceId(UUID.randomUUID().toString());
            terminalRepository.update(token.getCompanyId(), oldTerminal.getId(), oldTerminal);
        }


        terminal.setAssignedInventoryId(inventory.getId());
        terminal.setDeviceId(request.getDeviceId());

        terminalRepository.update(token.getCompanyId(), terminal.getId(), terminal);

        // Notify that inventory has been updated
        realtimeService.sendRealTimeEvent(token.getShopId().toString(),
                RealtimeService.RealtimeEventType.TerminalsUpdateEvent, null);
    }

    @Override
    public void updateCurrentTerminalLocation(UpdateTerminalLocationRequest request) {
        Terminal terminal = terminalRepository.get(token.getCompanyId(), token.getTerminalId());
        if (terminal == null) {
            throw new BlazeInvalidArgException("Terminal", "Terminal does not exist.");
        }
        if (request == null) {
            throw new BlazeInvalidArgException("Terminal", "Request is invalid.");
        }
        TerminalLocation terminalLocation = new TerminalLocation();
        terminalLocation.prepare(token.getCompanyId());
        terminalLocation.setTerminalId(terminal.getId());

        // get the current employee from the terminal. If it doesn't exist, get the current employee of the logged in employee
        // terminal.currentEmployeeId will be updated when the employee signs into the terminal using the pin
        // If another employee signs in to the same terminal, we'll automatically change the location to the new employee
        // The other employee would have been kicked out of the app.
        String employeeId = terminal.getCurrentEmployeeId();
        if (StringUtils.isBlank(employeeId)) {
            employeeId = token.getActiveTopUser().getUserId();
        }

        terminalLocation.setEmployeeId(employeeId);

        if (StringUtils.isNotBlank(terminalLocation.getEmployeeId())) {
            TimeCard timecard = timeCardRepository.getActiveTimeCard(token.getCompanyId(), token.getShopId(), employeeId);
            if (timecard != null) {
                terminalLocation.setTimeCardId(timecard.getId());
            }
        }

        terminalLocation.setName(request.getName());
        terminalLocation.setDeviceId(request.getDeviceId());
        terminalLocation.setShopId(token.getShopId());
        terminalLocation.setLoc(Lists.newArrayList(request.getLongitude(), request.getLatitude()));
        terminalLocationRepository.save(terminalLocation);

        if (StringUtils.isNotBlank(terminalLocation.getEmployeeId())) {
            employeeRepository.updateEmployeeLocation(token.getCompanyId(), employeeId, terminalLocation);
        }
    }


    @Override
    public void refreshTerminalLocation(String terminalId) {
        Terminal terminal = terminalRepository.get(token.getCompanyId(), terminalId);
        if (terminal == null) {
            throw new BlazeInvalidArgException("Terminal", "Terminal does not exist.");
        }
            String termId =   String.format("TerminalId : %s", terminalId);
            String title = "Inventory Updated";
            pushNotificationManager.sendRemotePushNotification("Inventory updated.", terminal, FcmPayload.Type.TASK, termId, title, FcmPayload.SubType.UPDATE);
    }

    @Override
    public SearchResult<Terminal> getShopAvailableTerminals(String shopId, int start, int limit) {
        if (StringUtils.isBlank(shopId)) {
            shopId = token.getShopId();
        }

        return terminalRepository.findItems(token.getCompanyId(), shopId, start, limit);
    }
}
