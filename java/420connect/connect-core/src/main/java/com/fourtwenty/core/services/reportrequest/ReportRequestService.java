package com.fourtwenty.core.services.reportrequest;

import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.models.reportrequest.ReportRequest;
import com.fourtwenty.core.reporting.ReportType;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.common.AssetStreamResult;

import java.io.InputStream;

public interface ReportRequestService {

    ReportRequest generateReportRequest(String companyId, String shopId, String employeeId, String requestId, InputStream stream, String fileName, String extension);

    SearchResult<ReportRequest> getReportRequestList(String companyId, String shopId, String employeeId, int start, int limit);

    ReportRequest addReportRequest(String companyId, String shopId, String employeeId, ReportType reportType, CompanyAsset asset, ReportRequest.RequestStatus status, Long startDate, Long endDate);

    ReportRequest updateRequestStatus(String companyId, String shopId, String requestId, ReportRequest.RequestStatus status, CompanyAsset asset);

    AssetStreamResult downloadReport(String companyId, String shopId, String employeeId, String generatedKey);
}
