package com.fourtwenty.core.services.reportrequest.impl;

import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.models.generic.Asset;
import com.fourtwenty.core.domain.models.reportrequest.ReportRequest;
import com.fourtwenty.core.domain.repositories.reportrequest.ReportRequestRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.reporting.ReportType;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.common.AssetStreamResult;
import com.fourtwenty.core.services.reportrequest.ReportRequestService;
import com.fourtwenty.core.services.thirdparty.AmazonS3Service;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

public class ReportRequestServiceImpl implements ReportRequestService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReportRequestServiceImpl.class);

    private static final String REPORT_REQUEST = "Report Request";
    private static final String REPORT_TYPE_NOT_BLANK = "Report Type cannot be blank";
    private static final String EMPLOYEE_NOT_BLANK = "Requesting employee is not found.";

    @Inject
    private ReportRequestRepository reportRequestRepository;
    @Inject
    private AmazonS3Service amazonS3Service;

    @Override
    public ReportRequest generateReportRequest(String companyId, String shopId, String employeeId, String requestId, InputStream stream, String fileName, String extension) {
        try {
            String keyName = UUID.randomUUID().toString();
            File file = stream2file(stream, extension);
            String key = amazonS3Service.uploadFile(file, keyName, extension);
            CompanyAsset asset = new CompanyAsset();
            asset.prepare(companyId);
            asset.setName(fileName + extension);
            asset.setKey(key);
            asset.setActive(true);
            asset.setType(Asset.AssetType.Document);

            return updateRequestStatus(companyId, shopId, requestId, ReportRequest.RequestStatus.Completed, asset);

        } catch (Exception e) {
            LOGGER.info(String.format("Error in file upload %s for request: %s", fileName, requestId));
        }

        return null;
    }

    @Override
    public SearchResult<ReportRequest> getReportRequestList(String companyId, String shopId, String employeeId, int start, int limit) {
        SearchResult<ReportRequest> result;

        limit = (limit == 0) ? 200 : limit;

        if (StringUtils.isNotBlank(employeeId)) {
            result = reportRequestRepository.listAllByEmployee(companyId, shopId, employeeId, start, limit);
        } else {
            result = reportRequestRepository.findItems(companyId, shopId, start, limit);
        }

        return result;
    }

    @Override
    public ReportRequest updateRequestStatus(String companyId, String shopId, String requestId, ReportRequest.RequestStatus status, CompanyAsset asset) {
        ReportRequest reportRequest = reportRequestRepository.getById(requestId);

        if (reportRequest == null) {
            LOGGER.info(String.format("Report request for Id: %s not found", requestId));
            return null;
        }

        reportRequest.setStatus(status);
        if (status == ReportRequest.RequestStatus.Completed && asset != null) {
            reportRequest.setAsset(asset);
        }

        return reportRequestRepository.update(companyId, requestId, reportRequest);
    }

    @Override
    public AssetStreamResult downloadReport(String companyId, String shopId, String employeeId, String requestId) {
        ReportRequest reportRequest = reportRequestRepository.get(companyId, requestId);
        if (reportRequest == null) {
            throw new BlazeInvalidArgException(REPORT_REQUEST, "Report request is not found.");
        }
        if (reportRequest.getAsset() == null) {
            throw new BlazeInvalidArgException(REPORT_REQUEST, String.format("Report for %s is not found.", reportRequest.getReportType().name()));
        }
        String generatedKey = reportRequest.getAsset().getKey();
        AssetStreamResult result = amazonS3Service.downloadFile(generatedKey, true);
        if (result == null) {
            throw new BlazeInvalidArgException("Asset", "Invalid asset key.");
        }
        result.setKey(StringUtils.isNotBlank(reportRequest.getAsset().getName()) ? reportRequest.getAsset().getName() : reportRequest.getReportType().name() + ".csv");
        return result;
    }

    @Override
    public ReportRequest addReportRequest(String companyId, String shopId, String employeeId, ReportType reportType, CompanyAsset asset, ReportRequest.RequestStatus status, Long startDate, Long endDate) {
        if (reportType == null) {
            throw new BlazeInvalidArgException(REPORT_REQUEST, REPORT_TYPE_NOT_BLANK);
        }
        if (StringUtils.isBlank(employeeId)) {
            throw new BlazeInvalidArgException(REPORT_REQUEST, EMPLOYEE_NOT_BLANK);
        }

        ReportRequest reportRequest = new ReportRequest();
        reportRequest.prepare(companyId);
        reportRequest.setShopId(shopId);
        reportRequest.setEmployeeId(employeeId);
        reportRequest.setRequestDate(new DateTime().getMillis());
        reportRequest.setReportType(reportType);
        reportRequest.setAsset(asset);
        reportRequest.setStatus(status);
        reportRequest.setReqStartDate(startDate);
        reportRequest.setReqEndDate(endDate);

        return reportRequestRepository.save(reportRequest);
    }

    private static File stream2file(InputStream in, String extension) throws IOException {
        final File tempFile = File.createTempFile(UUID.randomUUID().toString(), extension);
        tempFile.deleteOnExit();
        try (FileOutputStream out = new FileOutputStream(tempFile)) {
            IOUtils.copy(in, out);
        }
        return tempFile;
    }
}
