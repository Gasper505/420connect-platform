package com.fourtwenty.core.services.sms;

import com.fourtwenty.core.domain.models.marketing.MarketingJobLog;
import com.fourtwenty.core.services.sms.impl.IncomingSMSServiceImpl;

public interface IncomingSMSService {
    enum TwilioMessageStatus {
        Sent,
        Delivered,
        Undelivered,
        Failed;

        public static TwilioMessageStatus convert(String incomingMessage) {
            if (incomingMessage != null) {
                if (incomingMessage.equalsIgnoreCase(Sent.name())) {
                    return Sent;
                } else if (incomingMessage.equalsIgnoreCase(Delivered.name())) {
                    return Delivered;
                } else if (incomingMessage.equalsIgnoreCase(Undelivered.name())) {
                    return Undelivered;
                } else if (incomingMessage.equalsIgnoreCase(Failed.name())) {
                    return Failed;
                }
            }
            return Sent;
        }
    }

    MarketingJobLog.MSentStatus receiveSMSMessage(final String message, final String number, final String shopNumber);

    IncomingSMSServiceImpl.TwilioMessageStatus receiveSMSStatus(final String messageStatus, final String number, final String shopNumber);
}
