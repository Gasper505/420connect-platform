package com.fourtwenty.core.services.sms.impl;

import com.fourtwenty.core.domain.models.company.BouncedNumber;
import com.fourtwenty.core.domain.models.company.CompanyBounceNumber;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.marketing.MarketingJobLog;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.services.sms.IncomingSMSService;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bson.types.ObjectId;
import org.eclipse.jetty.util.StringUtil;

import java.util.Arrays;
import java.util.List;

public class IncomingSMSServiceImpl implements IncomingSMSService {
    private static final Log LOG = LogFactory.getLog(IncomingSMSServiceImpl.class);


//    Sent        Twilio did not receive updated delivery information for your message, or it was sent from the older /SMS/Message REST API Resource, which does not support message delivery information updates.
//    Delivered   Twilio has received confirmation of message delivery from the upstream carrier, (and, where available, the destination handset).
//    Undelivered	Twilio has received a delivery receipt indicating that the message was not delivered. This can happen for a number of reasons including carrier content filtering, availability of the destination handset, etc.
//    Failed


    public static final List<String> OUTOUT_MESSAGES = Arrays.asList(new String[]{"STOP", "UNSUBSCRIBE", "REMOVE"});
    public static final List<String> OUTIN_MESSAGES = Arrays.asList(new String[]{"UNSTOP", "START"});
    @Inject
    ShopRepository shopRepository;
    @Inject
    private CompanyBounceNumberRepository companyBounceNumberRepository;
    @Inject
    private BouncedNumberRepository bouncedNumberRepository;
    @Inject
    private MarketingJobLogRepository marketingJobLogRepository;
    @Inject
    private MemberRepository memberRepository;

    @Override
    public MarketingJobLog.MSentStatus receiveSMSMessage(String message, String _toNumber, String _fromNumber) {
        if (StringUtil.isNotBlank(message)) {
            message = message.toUpperCase();
            final String shopNumber = _toNumber;
            final String memberNumber = _fromNumber;

            // toNumber is the shopNumber
            Shop shop = shopRepository.getShopByTwilioNumber(shopNumber);
            LOG.info("Shop: " + (shop != null ? shop.getName() : "unknown shop"));

            LOG.info(String.format("receiveSMSMessage - shopNumber: %s, memberNumber: %s, message: %s", shopNumber, memberNumber, message));
            //+16502001867
            //INFO  [2018-10-20 03:10:34,127] com.fourtwenty.core.services.sms.impl.IncomingSMSServiceImpl: shopNumber: +16502001867, memberNumber: +15592307500, message: STOP
            if (isOptIn(message)) {
                // remove this from bounce numbers
                LOG.info("opting in..." + memberNumber);
                bouncedNumberRepository.deleteByNumber(memberNumber);
                if (shop != null) {
                    LOG.info("Searching cbn");
                    CompanyBounceNumber companyBounceNumber = companyBounceNumberRepository.getBouncedNumber(shop.getCompanyId(), memberNumber);
                    LOG.info("cbn: " + companyBounceNumber);
                    if (companyBounceNumber != null && companyBounceNumber.getStatus() != CompanyBounceNumber.BounceNumberStatus.OPT_IN) {
                        companyBounceNumber.setStatus(CompanyBounceNumber.BounceNumberStatus.OPT_IN);
                        companyBounceNumberRepository.update(companyBounceNumber.getId(), companyBounceNumber);
                    } else {
                        LOG.info("Could not find company bounced number...");
                    }

                    MarketingJobLog log = marketingJobLogRepository.getLog(shop.getCompanyId(), shopNumber, memberNumber);
                    if (log != null) {
                        ObjectId memberId = new ObjectId(log.getMemberId());
                        memberRepository.bulkUpdateTextOptIn(shop.getCompanyId(), Lists.newArrayList(memberId), true);


                        log.setStatus(MarketingJobLog.MSentStatus.OPT_IN);
                        marketingJobLogRepository.update(log.getCompanyId(), log.getId(), log);
                    } else {
                        LOG.info("no marketing log found...");
                    }
                }

                return MarketingJobLog.MSentStatus.OPT_IN;
            } else if (isOptOut(message)) {
                // Add this to bounce number

                LOG.info("opting out..." + memberNumber);
                BouncedNumber bouncedNumber = bouncedNumberRepository.getBouncedNumber(memberNumber);
                if (bouncedNumber == null) {
                    bouncedNumber = new BouncedNumber();
                    bouncedNumber.prepare();
                    bouncedNumber.setNumber(memberNumber);
                    bouncedNumber.setReason(MarketingJobLog.MSentStatus.OPT_OUT.name());
                    bouncedNumberRepository.save(bouncedNumber);
                } else {
                    if (bouncedNumber.isDeleted()) {
                        bouncedNumber.setDeleted(false);
                        bouncedNumber.setReason(MarketingJobLog.MSentStatus.OPT_OUT.name());
                        bouncedNumberRepository.update(bouncedNumber.getId(), bouncedNumber);
                    }
                }

                if (shop != null) {
                    LOG.info("Searching cbn");
                    CompanyBounceNumber companyBounceNumber = companyBounceNumberRepository.getBouncedNumber(shop.getCompanyId(), memberNumber);
                    LOG.info("cbn: " + companyBounceNumber);
                    if (companyBounceNumber == null) {
                        companyBounceNumber = new CompanyBounceNumber();
                        companyBounceNumber.prepare(shop.getCompanyId());
                        companyBounceNumber.setNumber(memberNumber);
                        companyBounceNumber.setStatus(CompanyBounceNumber.BounceNumberStatus.OPT_OUT);
                        companyBounceNumberRepository.save(companyBounceNumber);
                    } else if (companyBounceNumber.getStatus() != CompanyBounceNumber.BounceNumberStatus.OPT_OUT) {
                        companyBounceNumber.setStatus(CompanyBounceNumber.BounceNumberStatus.OPT_OUT);
                        companyBounceNumberRepository.update(companyBounceNumber.getId(), companyBounceNumber);
                    }


                    MarketingJobLog log = marketingJobLogRepository.getLog(shop.getCompanyId(), shopNumber, memberNumber);
                    if (log != null) {
                        ObjectId memberId = new ObjectId(log.getMemberId());
                        LOG.info("opting out...memberId: " + log.getMemberId());
                        memberRepository.bulkUpdateTextOptIn(shop.getCompanyId(), Lists.newArrayList(memberId), false);


                        log.setStatus(MarketingJobLog.MSentStatus.OPT_OUT);
                        marketingJobLogRepository.update(log.getCompanyId(), log.getId(), log);
                    } else {
                        LOG.info("no marketing log found...");
                    }
                }

                return MarketingJobLog.MSentStatus.OPT_OUT;
            }

        }

        return MarketingJobLog.MSentStatus.UNKNOWN_ERROR;
    }

    private static boolean isOptOut(String message) {
        for (String token : OUTOUT_MESSAGES) {
            if (message.contains(token)) {
                return true;
            }
        }
        return false;
    }

    private static boolean isOptIn(String message) {
        for (String token : OUTIN_MESSAGES) {
            if (message.contains(token)) {
                return true;
            }
        }
        return false;
    }


    @Override
    public TwilioMessageStatus receiveSMSStatus(final String messageStatus, final String _toNumber, final String _fromNumber) {

        final String shopNumber = _fromNumber;
        final String memberNumber = _toNumber;
        try {
            LOG.info(String.format("receiveSMSStatus - shopNumber: %s, memberNumber: %s, message: %s", shopNumber, memberNumber, messageStatus));
            TwilioMessageStatus twilioStatus = TwilioMessageStatus.convert(messageStatus);

            Shop shop = shopRepository.getShopByTwilioNumber(shopNumber);
            if (shop != null) {
                // Find latest marketingjob log with this number
                //UPDATE THE MARKING JOB LOG BASED ON THE TWILIO MESSAGE STATUS
                if (twilioStatus != TwilioMessageStatus.Sent) {
                    // Initial State is Sent

                    MarketingJobLog log = marketingJobLogRepository.getLog(shop.getCompanyId(), shopNumber, memberNumber);
                    if (log != null && log.getStatus() != MarketingJobLog.MSentStatus.DELIVERED) {
                        switch (twilioStatus) {
                            case Sent:
                                log.setStatus(MarketingJobLog.MSentStatus.SENT);
                                break;
                            case Delivered:
                                log.setStatus(MarketingJobLog.MSentStatus.DELIVERED);
                                break;
                            case Failed:
                                log.setStatus(MarketingJobLog.MSentStatus.FAILED);
                                break;
                            case Undelivered:
                                log.setStatus(MarketingJobLog.MSentStatus.UNDELIVERED);
                                break;
                        }
                        LOG.info("receivedSMSStatus: updating to - " + log.getStatus());
                        marketingJobLogRepository.update(log.getCompanyId(), log.getId(), log);
                    } else {
                        LOG.info("no marketing log found...");
                    }
                }
            }

            return twilioStatus;
        } catch (Exception e) {

        }
        return TwilioMessageStatus.Undelivered;
    }

}
