package com.fourtwenty.core.services.store;

import com.fourtwenty.core.domain.models.consumer.ConsumerUser;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.store.ConsumerPasswordReset;
import com.fourtwenty.core.rest.consumer.requests.ConsumerLoginRequest;
import com.fourtwenty.core.rest.consumer.requests.ConsumerRegisterRequest;
import com.fourtwenty.core.rest.consumer.results.ConsumerAuthResult;
import com.fourtwenty.core.rest.dispensary.requests.auth.ConsumerPasswordRequest;
import com.fourtwenty.core.rest.dispensary.requests.auth.PasswordResetRequest;
import com.fourtwenty.core.rest.dispensary.requests.auth.PasswordResetUpdateRequest;

/**
 * Created by mdo on 12/19/16.
 */
public interface ConsumerAuthService {
    ConsumerAuthResult login(ConsumerLoginRequest request);

    ConsumerAuthResult register(ConsumerRegisterRequest request);

    ConsumerAuthResult renewSession();

    ConsumerAuthResult getCurrentSession();

    // Password Reset
    ConsumerPasswordReset requestPasswordReset(PasswordResetRequest request);
    ConsumerUser getConsumerOrCreateFromProfile(PasswordResetRequest request);

    void updatePassword(PasswordResetUpdateRequest request);

    void resetConsumerPassword(ConsumerPasswordRequest request);

    ConsumerUser checkMemberExistenceByEmail(String email);

    ConsumerUser createConsumerUserFromMember(Member member);
}
