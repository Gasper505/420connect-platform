package com.fourtwenty.core.services.store;

import com.fourtwenty.core.domain.models.store.ConsumerCart;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.shop.ConsumerCartResult;
import com.fourtwenty.core.rest.store.requests.ProductCostRequest;
import com.fourtwenty.core.rest.store.requests.WooCartPrepareRequest;
import com.fourtwenty.core.rest.store.requests.WooCartRequest;
import com.fourtwenty.core.rest.store.results.ProductCostResult;
import com.fourtwenty.core.rest.store.results.WooComCartResult;

/**
 * Created by mdo on 5/10/17.
 */
public interface ConsumerCartService {
    ConsumerCartResult prepareConsumerCart(ConsumerCartResult incomingCart);

    enum ConsumerOrderSearchStatus {
        All,
        Open,
        Canceled,
        Completed
    }

    ConsumerCart getCartById(String consumerCartId);

    ConsumerCart getCurrentActiveCart(String sessionId);

    ConsumerCart prepareCart(ConsumerCart order);

    ConsumerCart updateCart(String consumerOrderId, ConsumerCart order);

    ConsumerCart cancelCart(String consumerOrderId);

    ConsumerCart submitWooOrder(WooCartRequest wooCartRequest);

    SearchResult<ConsumerCart> getLastOrders(int start, int limit);

    // Get cart by id
    ConsumerCartResult getCartByPublicKey(String publicKey);

    //By submitting cart, we are placing the consumer order
    ConsumerCart submitCart(String consumerOrderId, ConsumerCart incomingCart, ConsumerCart.TransactionSource transactionSource);


    SearchResult<ConsumerCart> getOrderHistory(int start, int limit);

    WooComCartResult prepareWoocommerceInfo(WooCartPrepareRequest cartPrepareRequest, String sessionId);

    // ProductCost
    ProductCostResult findCost(ProductCostRequest request);

    DateSearchResult<ConsumerCart> getConsumerCartByDate(final long afterDate, final long beforeDate);

    ConsumerCartResult finalizeItemCart(String transactionId, String orderItemId, ConsumerCartResult incomingCart);

    ConsumerCartResult unfinalizeItemCart(String transactionId, String orderItemId, ConsumerCartResult incomingCart);

    ConsumerCartResult finalizeAllOrderItems(String transactionId, ConsumerCartResult incomingCart);

    ConsumerCart finalizeItemCart(String orderItemId, ConsumerCart incomingCart);

    ConsumerCart unfinalizeItemCart(String orderItemId, ConsumerCart incomingCart);

    ConsumerCart finalizeAllOrderItems(ConsumerCart incomingCart);

    }
