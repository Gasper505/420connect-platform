package com.fourtwenty.core.services.store;

import com.fourtwenty.core.domain.models.consumer.ConsumerUser;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.rest.consumer.requests.ConsumerCreateRequest;
import com.fourtwenty.core.rest.consumer.requests.ConsumerLoginRequest;
import com.fourtwenty.core.rest.consumer.results.ConsumerAuthResult;
import com.fourtwenty.core.rest.dispensary.requests.consumeruser.request.ConsumerUserAddRequest;
import com.fourtwenty.core.rest.dispensary.requests.consumeruser.response.ConsumerUserImportResult;
import com.fourtwenty.core.rest.dispensary.results.ConsumerUserResult;
import com.fourtwenty.core.rest.dispensary.results.common.AssetStreamResult;
import com.fourtwenty.core.rest.store.requests.ContractSignRequest;

import java.io.InputStream;
import java.util.List;

/**
 * Created by mdo on 5/17/17.
 */
public interface ConsumerUserService {

    ConsumerAuthResult login(ConsumerLoginRequest request);
    ConsumerUser createNewUser(ConsumerCreateRequest createRequest);
    ConsumerUser createConsumerUserFromMember(Member member);

    ConsumerUser getCurrentUser();

    ConsumerUser signContract(ContractSignRequest request);

    ConsumerUser updateUser(ConsumerUser user, boolean updateEmail);

    ConsumerUser updateDLPhoto(InputStream stream, String fileName);

    ConsumerUser updateRecPhoto(InputStream stream, String fileName);

    AssetStreamResult getConsumerAssetStream(String assetKey, String assetToken);

    ConsumerUser findUser(String email, String phoneNumber, String country);

    ConsumerUser getUserById(String consumerUserId);

    void updateCorrespondingMembers(ConsumerUser consumerUser);

    ConsumerUserResult getConsumerByEmailId(String email);


    ConsumerUserImportResult parseConsumerDataImport(List<ConsumerUserAddRequest> requestList);

    void updateMemberWithEmailAndDob();
}
