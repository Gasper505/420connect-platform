package com.fourtwenty.core.services.store;

import com.fourtwenty.core.domain.models.views.MemberLimitedView;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;

public interface MemberUserService {
    SearchResult<MemberLimitedView> getMembershipsForActiveShop(int start, int limit);
}
