package com.fourtwenty.core.services.store;

import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.company.Terminal;
import com.fourtwenty.core.domain.models.customer.Doctor;
import com.fourtwenty.core.domain.models.partner.PartnerWebHook;
import com.fourtwenty.core.domain.models.payment.ShopPaymentOption;
import com.fourtwenty.core.domain.models.product.Inventory;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.company.EmployeeResult;
import com.fourtwenty.core.rest.partners.request.PartnerWebHookAddRequest;
import com.fourtwenty.core.rest.store.results.StoreInfoResult;

import java.util.List;

/**
 * Created by mdo on 5/9/17.
 */
public interface StoreInfoService {
    StoreInfoResult getStoreInfo();

    SearchResult<Doctor> searchDoctor(String term);

    SearchResult<Terminal> getActiveTerminalByShop(int start, int limit);

    SearchResult<Inventory> getActiveInventoriesByShop(int start, int limit);

    List<PartnerWebHook> createPartnerWebHookForPlugIn(List<PartnerWebHookAddRequest> request);

    SearchResult<EmployeeResult> searchEmployees(String currentEmployeeId, int start, int limit);

    Employee getEmployeeById(String employeeId);

    List<ShopPaymentOption> getPaymentOptions();

}
