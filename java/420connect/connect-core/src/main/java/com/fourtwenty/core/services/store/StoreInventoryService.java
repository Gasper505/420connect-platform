package com.fourtwenty.core.services.store;

import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.rest.dispensary.results.BrandResult;
import com.fourtwenty.core.rest.dispensary.requests.partners.PartnerInventoryTransferHistory;
import com.fourtwenty.core.rest.dispensary.requests.partners.PartnerInventoryTransferStatusRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.store.results.ProductWithInfo;

import java.util.List;

/**
 * Created by mdo on 4/21/17.
 */
public interface StoreInventoryService {
    SearchResult<ProductCategory> getStoreCategories();

    SearchResult<ProductWithInfo> getAllProducts(String categoryId, String strain, String term, List<String> tags, String vendorId, int start, int limit, String brandId);

    ProductWithInfo getProductById(String productId);

    SearchResult<Product> getProductsByTerminalId(String terminalId, int start, int limit);
    SearchResult<Product> getProductsByDates(long afterDate, long beforeDate);;

    SearchResult<ProductWithInfo> getAllProductsByDates(String categoryId, String strain, List<String> tags, String vendorId, long afterDate, long beforeDate);

    SearchResult<ProductBatch> getAllBatches(String batchId, String productId, ProductBatch.BatchStatus status, int start, int limit, String term);
    SearchResult<ProductBatch> getAllBatches(long afterDate, long beforeDate, int start, int limit);

    List<BatchQuantity> getQuantityInfo(String productId);

    SearchResult<BrandResult> getAllBrands(String term, int start, int limit);

    InventoryTransferHistory addTransferHistory(PartnerInventoryTransferHistory request);

    InventoryTransferHistory updateTransferHistory(String historyId, PartnerInventoryTransferHistory request);

    InventoryTransferHistory updateInventoryTransferStatus(String historyId, PartnerInventoryTransferStatusRequest request, boolean isAccept);
}
