package com.fourtwenty.core.services.store;

import com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrder;
import com.fourtwenty.core.rest.dispensary.requests.partners.purchaseorder.PartnerPurchaseOrderAddRequest;
import com.fourtwenty.core.rest.dispensary.requests.partners.purchaseorder.PartnerPurchaseOrderUpdateRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.PurchaseOrderItemResult;

public interface StorePurchaseOrderService {
    PurchaseOrder addPurchaseOrder(PartnerPurchaseOrderAddRequest request);

    PurchaseOrder preparePurchaseOrder(PartnerPurchaseOrderAddRequest request);

    PurchaseOrder updatePurchaseOrder(String purchaseOrderId, PartnerPurchaseOrderUpdateRequest request);

    PurchaseOrderItemResult getPurchaseOrderById(String purchaseOrderId);

    SearchResult<PurchaseOrderItemResult> getAllPurchaseOrder(String startDate, String endDate, int start, int limit);

}
