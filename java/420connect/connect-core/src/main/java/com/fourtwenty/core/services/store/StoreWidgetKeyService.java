package com.fourtwenty.core.services.store;

import com.fourtwenty.core.domain.models.store.StoreWidgetKey;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;

/**
 * Created by mdo on 4/17/17.
 */
public interface StoreWidgetKeyService {

    StoreWidgetKey createStoreKey();

    StoreWidgetKey getCurrentStoreKey();

    SearchResult<StoreWidgetKey> getStoreKeys();
}
