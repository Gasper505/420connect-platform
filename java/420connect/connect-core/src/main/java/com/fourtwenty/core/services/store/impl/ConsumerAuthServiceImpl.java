package com.fourtwenty.core.services.store.impl;

import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.consumer.ConsumerUser;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.models.store.ConsumerPasswordReset;
import com.fourtwenty.core.domain.repositories.dispensary.CompanyAssetRepository;
import com.fourtwenty.core.domain.repositories.dispensary.CompanyRepository;
import com.fourtwenty.core.domain.repositories.dispensary.DoctorRepository;
import com.fourtwenty.core.domain.repositories.dispensary.MemberRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.domain.repositories.store.ConsumerPasswordResetRepository;
import com.fourtwenty.core.domain.repositories.store.ConsumerUserRepository;
import com.fourtwenty.core.exceptions.BlazeAuthException;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.managed.AmazonServiceManager;
import com.fourtwenty.core.rest.consumer.requests.ConsumerLoginRequest;
import com.fourtwenty.core.rest.consumer.requests.ConsumerRegisterRequest;
import com.fourtwenty.core.rest.consumer.results.ConsumerAuthResult;
import com.fourtwenty.core.rest.dispensary.requests.auth.ConsumerPasswordRequest;
import com.fourtwenty.core.rest.dispensary.requests.auth.PasswordResetRequest;
import com.fourtwenty.core.rest.dispensary.requests.auth.PasswordResetUpdateRequest;
import com.fourtwenty.core.security.tokens.ConsumerAssetAccessToken;
import com.fourtwenty.core.security.tokens.StoreAuthToken;
import com.fourtwenty.core.services.AbstractStoreServiceImpl;
import com.fourtwenty.core.services.mgmt.MemberService;
import com.fourtwenty.core.services.mgmt.impl.ShopServiceImpl;
import com.fourtwenty.core.services.store.ConsumerAuthService;
import com.fourtwenty.core.util.JsonSerializer;
import com.fourtwenty.core.util.SecurityUtil;
import com.google.inject.Inject;
import com.google.inject.Provider;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.UUID;
import java.util.regex.Pattern;

/**
 * Created by mdo on 12/19/16.
 */
public class ConsumerAuthServiceImpl extends AbstractStoreServiceImpl implements ConsumerAuthService {

    private static final Logger LOG = LoggerFactory.getLogger(ConsumerAuthServiceImpl.class);

    @Inject
    ConsumerUserRepository consumerUserRepository;
    @Inject
    SecurityUtil securityUtil;
    @javax.inject.Inject
    ConnectConfiguration connectConfiguration;
    @javax.inject.Inject
    ConsumerPasswordResetRepository passwordResetRepository;
    @javax.inject.Inject
    ShopRepository shopRepository;
    @javax.inject.Inject
    AmazonServiceManager amazonServiceManager;
    @Inject
    CompanyRepository companyRepository;
    @Inject
    private MemberRepository memberRepository;
    @Inject
    private CompanyAssetRepository companyAssetRepository;
    @Inject
    private DoctorRepository doctorRepository;
    @Inject
    private MemberService memberService;

    private static final String PASSWORD_RESET = "Password Reset";
    private static final String CONSUMER_NOT_BLANK = "Consumer cannot be blank.";
    private static final String CONSUMER_NOT_FOUND = "Consumer not found.";
    private static final String CONSUMER = "Consumer User";
    private static final String ACCOUNT_EXIST = "Account already exists.";
    private static final String INVALID_CREDENTIALS = "Invalid user credentials.";

    @Inject
    public ConsumerAuthServiceImpl(Provider<StoreAuthToken> tokenProvider) {
        super(tokenProvider);
    }

    @Override
    public ConsumerAuthResult login(ConsumerLoginRequest request) {

        ConsumerUser consumerUser = consumerUserRepository.getConsumerByEmail(storeToken.getCompanyId(), request.getEmail().toLowerCase().trim(), ConsumerUser.class);

        if (consumerUser == null) {
            this.checkMemberExistenceByEmail(request.getEmail());
        }

        if (consumerUser == null) {
            throw new BlazeAuthException("Authentication", INVALID_CREDENTIALS);
        }

        if (!securityUtil.checkPassword(request.getPassword(), consumerUser.getPassword())) {
            throw new BlazeAuthException("Authentication", "Invalid user credentials.");
        }


        consumerUser.setPassword(null);
        ConsumerAuthResult authResult = new ConsumerAuthResult();
        StoreAuthToken storeAuthToken = createNewToken(
                consumerUser.getId());

        ConsumerAssetAccessToken assetAccessToken = createConsumerAssetToken(consumerUser.getId());

        authResult.setAccessToken(getNewEncryptedToken(storeAuthToken));
        authResult.setAssetAccessToken(getNewEncryptedAssetToken(assetAccessToken));
        authResult.setUser(consumerUser);
        authResult.setLoginTime(storeAuthToken.getLoginDate());
        authResult.setExpirationTime(storeAuthToken.getExpirationDate());
        authResult.setSessionId(UUID.randomUUID().toString());
        return authResult;

    }

    /**
     * This method checks weather any member exist with same email
     * @param emailId : requested email id
     */
    @Override
    public ConsumerUser checkMemberExistenceByEmail(String emailId) {
        if (emailId == null) {
            return null;
        }
        String email = emailId.trim().toLowerCase();


        Iterable<Member> memberByEmail = memberRepository.getMemberByEmail(storeToken.getCompanyId(), email);

        Member member = null;
        for (Member dbMember : memberByEmail) {
            member = dbMember;
            break;
        }

        //If dispensary has member profile created, but not corresponding member created
        //If member profile exist, and no consumer user exist
        if (member != null) {
            if (StringUtils.isNotBlank(member.getConsumerUserId())) {
                //
                ConsumerUser user = consumerUserRepository.get(storeToken.getCompanyId(),member.getConsumerUserId());
                // if same email, then return
                if (user.getEmail() != null && user.getEmail().equalsIgnoreCase(email)) {
                    return user;
                }
            }
            // create a brand new consumer user with incoming email
            ConsumerUser consumerUser = this.createConsumerUserFromMember(member);
            memberRepository.updateConsumerUserId(storeToken.getCompanyId(), consumerUser.getId(), member.getId());
            return consumerUser;
        }
        return null;
    }

    @Override
    public ConsumerAuthResult register(ConsumerRegisterRequest request) {

        ConsumerUser consumerUser = consumerUserRepository.getConsumerByEmail(storeToken.getCompanyId(), request.getEmail().toLowerCase().trim(), ConsumerUser.class);

        if (consumerUser == null) {
            ConsumerUser user = this.checkMemberExistenceByEmail(request.getEmail());

            if (user != null) {
                throw new BlazeInvalidArgException(CONSUMER, ACCOUNT_EXIST);
            }
        }

        if (consumerUser != null) {
            throw new BlazeAuthException("Authentication", "Email is in use.");
        }

        consumerUser = this.createConsumerUser(request);

        consumerUser.setPassword(null);
        /// Create auth
        ConsumerAuthResult authResult = new ConsumerAuthResult();
        StoreAuthToken storeAuthToken = createNewToken(
                consumerUser.getId());

        ConsumerAssetAccessToken assetAccessToken = createConsumerAssetToken(consumerUser.getId());
        authResult.setAccessToken(getNewEncryptedToken(storeAuthToken));
        authResult.setAssetAccessToken(getNewEncryptedAssetToken(assetAccessToken));
        authResult.setUser(consumerUser);
        authResult.setLoginTime(storeAuthToken.getLoginDate());
        authResult.setExpirationTime(storeAuthToken.getExpirationDate());
        authResult.setSessionId(UUID.randomUUID().toString());
        return authResult;
    }

    private ConsumerUser createConsumerUser(ConsumerRegisterRequest request) {
        LOG.info("Info of consumer registration : " + request.getEmail() + "\ttext opt in : " + request.isEmailOptIn() + "\temail opt in : " + request.isEmailOptIn());

        Shop shop = shopRepository.get(storeToken.getCompanyId(), storeToken.getShopId());
        String cpn = request.getPhoneNumber();
        if (shop != null) {
            cpn = AmazonServiceManager.cleanPhoneNumber(request.getPhoneNumber(), shop);
        }

        String json = JsonSerializer.toJson(request);
        LOG.info("Incoming JSON: " + json);

        ConsumerUser consumerUser = new ConsumerUser();
        consumerUser.prepare(storeToken.getCompanyId());
        consumerUser.setId(ObjectId.get().toString());
        consumerUser.setEmail(request.getEmail().toLowerCase().trim());
        consumerUser.setFirstName(request.getFirstName());
        consumerUser.setLastName(request.getLastName());
        consumerUser.setPrimaryPhone(request.getPhoneNumber());
        consumerUser.setMarketingSource(request.getMarketingSource());
        consumerUser.setDob(request.getDob());
        consumerUser.setActive(true);
        consumerUser.setSourceCompanyId(storeToken.getCompanyId());
        consumerUser.setCpn(cpn);
        String encryptPassword = securityUtil.encryptPassword(request.getPassword());
        consumerUser.setPassword(encryptPassword);
        consumerUser.setEmailOptIn(request.isEmailOptIn());
        consumerUser.setTextOptIn(request.isTextOptIn());
        consumerUser.setMedical(request.isMedical());
        consumerUser.setConsumerType(request.getConsumerType());
        consumerUser.setSex(request.getSex());

        return consumerUserRepository.save(consumerUser);
    }

    /**
     * Create consumer user from existing member
     * @param member : existing member
     */
    @Override
    public ConsumerUser createConsumerUserFromMember(Member member) {
        ConsumerUser consumerUser = new ConsumerUser();

        consumerUser.prepare(storeToken.getCompanyId());
        consumerUser.setEmail(member.getEmail().toLowerCase().trim());
        consumerUser.setFirstName(member.getFirstName());
        consumerUser.setLastName(member.getLastName());
        consumerUser.setMiddleName(member.getMiddleName());
        consumerUser.setPrimaryPhone(member.getPrimaryPhone());
        consumerUser.setMarketingSource(member.getMarketingSource());
        consumerUser.setDob(member.getDob());
        consumerUser.setActive(true);
        consumerUser.setSourceCompanyId(storeToken.getCompanyId());
        consumerUser.setEmailOptIn(member.isEmailOptIn());
        consumerUser.setTextOptIn(member.isTextOptIn());
        consumerUser.setMedical(member.isMedical());
        consumerUser.setConsumerType(member.getConsumerType());
        consumerUser.setSex(member.getSex());
        consumerUser.setMemberId(member.getId());



        Shop shop = shopRepository.get(storeToken.getCompanyId(), storeToken.getShopId());
        String cpn = member.getPrimaryPhone();
        if (shop != null) {
            cpn = AmazonServiceManager.cleanPhoneNumber(member.getPrimaryPhone(), shop);
        }
        Address address = member.getAddress();
        if (address != null) {
            address.prepare(storeToken.getCompanyId());
            consumerUser.setAddress(address);
            if (shop != null && shop.getAddress() != null && shop.getAddress().getCountry() != null) {
                address.setCountry(shop.getAddress().getCountry());
            }
        }
        consumerUser.setCpn(cpn);

        memberService.updateConsumerUserInfo(consumerUser, member);

        return consumerUserRepository.save(consumerUser);
    }

    @Override
    public ConsumerAuthResult getCurrentSession() {
        ConsumerUser consumerUser = consumerUserRepository.getById(storeToken.getConsumerId());
        if (consumerUser == null) {
            throw new BlazeAuthException("Authentication", "User is not valid.");
        }


        consumerUser.setPassword(null);

        ConsumerAuthResult authResult = new ConsumerAuthResult();
        StoreAuthToken storeAuthToken = createNewToken(
                consumerUser.getId());

        ConsumerAssetAccessToken assetAccessToken = createConsumerAssetToken(consumerUser.getId());

        storeAuthToken.setExpirationDate(storeToken.getExpirationDate());
        storeAuthToken.setLoginDate(storeToken.getLoginDate());
        authResult.setAccessToken(getNewEncryptedToken(storeAuthToken));
        authResult.setAssetAccessToken(getNewEncryptedAssetToken(assetAccessToken));
        authResult.setUser(consumerUser);
        authResult.setLoginTime(storeAuthToken.getLoginDate());
        authResult.setExpirationTime(storeAuthToken.getExpirationDate());
        authResult.setSessionId(UUID.randomUUID().toString());
        return authResult;
    }


    @Override
    public ConsumerAuthResult renewSession() {
        ConsumerUser consumerUser = consumerUserRepository.getById(storeToken.getConsumerId());
        if (consumerUser == null) {
            throw new BlazeAuthException("Consumer", "Consumer does not exist.");
        }

        consumerUser.setPassword(null);

        ConsumerAuthResult authResult = new ConsumerAuthResult();
        StoreAuthToken storeAuthToken = createNewToken(storeToken.getConsumerId());

        authResult.setAccessToken(getNewEncryptedToken(storeAuthToken));
        authResult.setUser(consumerUser);
        authResult.setLoginTime(storeAuthToken.getLoginDate());
        authResult.setExpirationTime(storeAuthToken.getExpirationDate());
        authResult.setSessionId(UUID.randomUUID().toString());
        return authResult;
    }


    @Override
    public ConsumerPasswordReset requestPasswordReset(PasswordResetRequest request) {
        ConsumerUser consumerUser = consumerUserRepository.getConsumerByEmail(storeToken.getCompanyId(), request.getEmail().toLowerCase(), ConsumerUser.class);

        if (consumerUser == null) {
            consumerUser = this.checkMemberExistenceByEmail(request.getEmail());
        }

        if (consumerUser == null) {
            throw new BlazeAuthException("Authentication", "This email does not exist in our system.");
        }
        ConsumerPasswordReset passwordReset = new ConsumerPasswordReset();
        passwordReset.setConsumerUserId(consumerUser.getId());
        passwordReset.setResetCode(securityUtil.getNextResetCode());
        passwordReset.setExpirationDate(DateTime.now().plusDays(2).getMillis());
        passwordReset.setExpired(false);

        // set all previous password reset to expired
        passwordResetRepository.setPasswordExpired(consumerUser.getId());
        passwordResetRepository.save(passwordReset);

        Shop shop = shopRepository.get(storeToken.getCompanyId(), storeToken.getShopId());

        String body = getPasswordEmailBody(shop, consumerUser, passwordReset);
        amazonServiceManager.sendEmail("support@blaze.me", consumerUser.getEmail(),
                connectConfiguration.getAppName() + " Email Reset", body, null, null, shop.getEmailAdress(),
                shop.getName());
        return passwordReset;
    }


    @Override
    public ConsumerUser getConsumerOrCreateFromProfile(PasswordResetRequest request) {
        ConsumerUser consumerUser = consumerUserRepository.getConsumerByEmail(storeToken.getCompanyId(), request.getEmail().toLowerCase(), ConsumerUser.class);

        if (consumerUser == null) {
            consumerUser = this.checkMemberExistenceByEmail(request.getEmail());
        }

        if (consumerUser == null) {
            throw new BlazeAuthException("Authentication", "This email does not exist in our system.");
        }
        consumerUser.setPassword(null);
        return consumerUser;
    }

    @Override
    public void updatePassword(PasswordResetUpdateRequest request) {

        ConsumerPasswordReset passwordReset = passwordResetRepository.getPasswordReset(request.getResetCode());
        if (passwordReset == null) {
            throw new BlazeInvalidArgException("PasswordReset", "Invalid password reset.");
        }
        if (DateTime.now().isAfter(passwordReset.getExpirationDate())) {
            throw new BlazeInvalidArgException("PasswordReset", "This password reset code has expired.");
        }

        if (passwordReset.isExpired()) {
            throw new BlazeInvalidArgException("PasswordReset", "This password reset code has expired.");
        }
        ConsumerUser consumerUser = consumerUserRepository.getById(passwordReset.getConsumerUserId());
        if (consumerUser == null) {
            throw new BlazeAuthException("Consumer", "Consumer does not exist.");
        }

        if (StringUtils.isBlank(request.getPassword()) && (request.getPassword().length() < 4)) {
            throw new BlazeAuthException(PASSWORD_RESET, "Password is too short.");
        }

        consumerUser.setPassword(securityUtil.encryptPassword(request.getPassword()));
        consumerUser.prepare(storeToken.getCompanyId());
        consumerUserRepository.update(consumerUser.getId(), consumerUser);

        // set all previous password reset to expired
        passwordResetRepository.setPasswordExpired(consumerUser.getId());
    }

    private String getPasswordEmailBody(Shop shop, ConsumerUser consumerUser, ConsumerPasswordReset passwordReset) {
        InputStream inputStream = ShopServiceImpl.class.getResourceAsStream("/consumer_passwordreset.html");
        StringWriter writer = new StringWriter();
        try {
            IOUtils.copy(inputStream, writer, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        String theString = writer.toString();
        String resetContent;
        String optionType;
        // TODO: just to make this backward compatible and will remove this check later on
        if (StringUtils.isEmpty(shop.getOnlineStoreInfo().getWebsiteUrl())) {
            resetContent = ("" +
                    "<table class=\"body-action\" align=\"center\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n" +
                    "    <tr>\n" +
                    "        <td align=\"center\">\n" +
                    "        " + passwordReset.getResetCode() + "\n" +
                    "        </td>\n" +
                    "    </tr>\n" +
                    "</table>");
            optionType = "code";
        } else {
            resetContent = ("" +
                    "<table class=\"body-action\" align=\"center\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n" +
                    "    <tr>\n" +
                    "        <td align=\"center\">\n" +
                    "        <a href=\"" + shop.getOnlineStoreInfo().getWebsiteUrl() + "?blazerc=" + passwordReset.getResetCode() + "\">Click here to reset</a>\n" +
                    "        </td>\n" +
                    "    </tr>\n" +
                    "</table>");
            optionType = "link";
        }

        theString = theString.replaceAll(Pattern.quote("{{name}}"), consumerUser.getFirstName());
        theString = theString.replaceAll(Pattern.quote("{{action_url}}"), "");
        theString = theString.replaceAll(Pattern.quote("{{option_type}}"), optionType);
        theString = theString.replaceAll(Pattern.quote("{{reset_content}}"), resetContent);
        theString = theString.replaceAll(Pattern.quote("{{dispensary_name}}"), shop.getName());


        return theString;
    }

    // Private methods
    private StoreAuthToken createNewToken(String consumerUserId) {
        StoreAuthToken newToken = new StoreAuthToken();
        newToken.setCompanyId(storeToken.getCompanyId());
        newToken.setShopId(storeToken.getShopId());
        newToken.setConsumerId(consumerUserId);
        newToken.setLoginDate(DateTime.now().getMillis());
        newToken.setExpirationDate(DateTime.now().plusMinutes(connectConfiguration.getAuthTTL()).getMillis());
        return newToken;
    }

    // Private methods
    private ConsumerAssetAccessToken createConsumerAssetToken(String consumerUserId) {
        ConsumerAssetAccessToken newToken = new ConsumerAssetAccessToken();
        newToken.setConsumerId(consumerUserId);
        newToken.setSignedDate(DateTime.now().getMillis());
        newToken.setExpirationDate(DateTime.now().plusMinutes(connectConfiguration.getAuthTTL()).getMillis());
        return newToken;
    }

    private String getNewEncryptedToken(StoreAuthToken authToken) {
        return securityUtil.createStoreToken(authToken);
    }

    private String getNewEncryptedAssetToken(ConsumerAssetAccessToken assetAccessToken) {
        return securityUtil.createConsumerAssetAccessToken(assetAccessToken);
    }

    @Override
    public void resetConsumerPassword(ConsumerPasswordRequest request) {
        if (request == null) {
            throw new BlazeInvalidArgException(PASSWORD_RESET, "Request cannot be blank.");
        }
        if (StringUtils.isBlank(request.getConsumerId())) {
            throw new BlazeInvalidArgException(PASSWORD_RESET, CONSUMER_NOT_BLANK);
        }

        ConsumerUser consumerUser = consumerUserRepository.getById(request.getConsumerId());
        if (consumerUser == null) {
            throw new BlazeInvalidArgException(PASSWORD_RESET, CONSUMER_NOT_FOUND);
        }

        consumerUser.prepare(storeToken.getCompanyId());
        consumerUser.setPassword(securityUtil.encryptPassword(request.getPassword()));
        consumerUserRepository.update(consumerUser.getId(), consumerUser);
        passwordResetRepository.setPasswordExpired(consumerUser.getId());

    }
}
