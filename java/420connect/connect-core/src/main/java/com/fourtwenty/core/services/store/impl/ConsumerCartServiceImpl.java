package com.fourtwenty.core.services.store.impl;

import com.esotericsoftware.kryo.Kryo;
import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.domain.models.company.*;
import com.fourtwenty.core.domain.models.consumer.ConsumerUser;
import com.fourtwenty.core.domain.models.customer.CustomerInfo;
import com.fourtwenty.core.domain.models.customer.Identification;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.generic.UniqueSequence;
import com.fourtwenty.core.domain.models.loyalty.LoyaltyReward;
import com.fourtwenty.core.domain.models.loyalty.Promotion;
import com.fourtwenty.core.domain.models.loyalty.PromotionReq;
import com.fourtwenty.core.domain.models.loyalty.PromotionReqLog;
import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.models.store.ConsumerCart;
import com.fourtwenty.core.domain.models.store.OnlineStoreInfo;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.domain.repositories.loyalty.LoyaltyRewardRepository;
import com.fourtwenty.core.domain.repositories.store.ConsumerCartRepository;
import com.fourtwenty.core.domain.repositories.store.ConsumerUserRepository;
import com.fourtwenty.core.event.BlazeEventBus;
import com.fourtwenty.core.event.orders.IncomingOrderEvent;
import com.fourtwenty.core.exceptions.BlazeAuthException;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.managed.PartnerWebHookManager;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.shop.ConsumerCartResult;
import com.fourtwenty.core.rest.store.requests.OrderItemRequest;
import com.fourtwenty.core.rest.store.requests.ProductCostRequest;
import com.fourtwenty.core.rest.store.requests.WooCartPrepareRequest;
import com.fourtwenty.core.rest.store.requests.WooCartRequest;
import com.fourtwenty.core.rest.store.results.ProductCostResult;
import com.fourtwenty.core.rest.store.results.WooComCartResult;
import com.fourtwenty.core.rest.store.webhooks.ConsumerOrderData;
import com.fourtwenty.core.security.tokens.StoreAuthToken;
import com.fourtwenty.core.services.AbstractStoreServiceImpl;
import com.fourtwenty.core.services.common.ConsumerNotificationService;
import com.fourtwenty.core.services.common.EmployeeNotificationService;
import com.fourtwenty.core.services.common.RealtimeService;
import com.fourtwenty.core.services.global.CannabisLimitService;
import com.fourtwenty.core.services.mgmt.CartService;
import com.fourtwenty.core.services.mgmt.QueuePOSService;
import com.fourtwenty.core.services.mgmt.ShopService;
import com.fourtwenty.core.services.store.ConsumerCartService;
import com.fourtwenty.core.services.store.ConsumerUserService;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.core.util.NumberUtils;
import com.fourtwenty.core.util.TextUtil;
import com.github.slugify.Slugify;
import com.google.common.collect.Lists;
import com.google.inject.Provider;
import com.fourtwenty.core.domain.models.transaction.FcmPayload;
import joptsimple.internal.Strings;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.*;

/**
 * Created by mdo on 5/10/17.
 */
public class ConsumerCartServiceImpl extends AbstractStoreServiceImpl implements ConsumerCartService {
    private static final Log LOGGER = LogFactory.getLog(ConsumerCartServiceImpl.class);
    private static final String QUANTITY_NOT_AVAILABLE = "We are out of stock for '%s'.";
    private static final String PRODUCT_QUANTITY = "Product Quantity";

    @Inject
    ConnectConfiguration configuration;
    @Inject
    ConsumerCartRepository consumerCartRepository;
    @Inject
    ShopService shopService;
    @Inject
    ShopRepository shopRepository;
    @Inject
    ConsumerUserRepository consumerUserRepository;
    @Inject
    MemberRepository memberRepository;
    @Inject
    MemberGroupRepository memberGroupRepository;
    @Inject
    CompanyAssetRepository companyAssetRepository;
    @Inject
    ProductRepository productRepository;
    @Inject
    MemberGroupPricesRepository memberGroupPricesRepository;
    @Inject
    ProductCategoryRepository categoryRepository;
    @Inject
    ProductWeightToleranceRepository weightToleranceRepository;
    @Inject
    TransactionRepository transactionRepository;
    @Inject
    EmployeeRepository employeeRepository;
    @Inject
    RealtimeService realtimeService;
    @Inject
    UniqueSequenceRepository uniqueSequenceRepository;
    @Inject
    ConsumerNotificationService consumerNotificationService;
    @Inject
    QueuePOSService queuePOSService;
    @Inject
    CartService cartService;
    @Inject
    EmployeeNotificationService employeeNotificationService;
    @Inject
    private ProductPrepackageQuantityRepository prepackageQuantityRepository;
    @Inject
    private CannabisLimitService cannabisLimitService;
    @Inject
    CompanyRepository companyRepository;
    @Inject
    private PartnerWebHookManager partnerWebHookManager;
    @Inject
    private ConsumerUserService consumerUserService;
    @Inject
    private ProductCategoryRepository productCategoryRepository;
    @Inject
    private BlazeEventBus eventBus;
    @Inject
    private LoyaltyRewardRepository rewardRepository;

    @Inject
    public ConsumerCartServiceImpl(Provider<StoreAuthToken> tokenProvider) {
        super(tokenProvider);
    }


    @Override
    public ConsumerCart getCurrentActiveCart(String sessionId) {
        // do this for validate
        if (StringUtils.isBlank(sessionId)) {
            sessionId = UUID.randomUUID().toString();
        }

        ConsumerCart unauthCart = consumerCartRepository.getCurrentInProgressCartWithSessionId(storeToken.getCompanyId(),
                storeToken.getShopId(),
                sessionId);

        // If authenticated, then return the authenticated user
        if (storeToken.isAuthenticated()) {
            // Make sure it's not a different user's cart


            ConsumerCart currentOrder = consumerCartRepository.getCurrentInProgressCart(storeToken.getCompanyId(),
                    storeToken.getShopId(),
                    storeToken.getConsumerId());

            if (unauthCart != null && StringUtils.isNotBlank(unauthCart.getConsumerId())
                    && !unauthCart.getConsumerId().equalsIgnoreCase(storeToken.getConsumerId())) {
                // if the incoming unauth cart is from a different user, just return the active user's cart
                if (currentOrder == null) {
                    currentOrder = createNewEmptyCart();
                    currentOrder.setConsumerId(storeToken.getConsumerId());
                    currentOrder.setSessionId(UUID.randomUUID().toString());
                    consumerCartRepository.save(currentOrder);
                }
                assignedDependencies(currentOrder);
                return currentOrder;
            } else {
                if (currentOrder == null) {
                    // Create a new consumer order
                    if (unauthCart == null) {
                        unauthCart = createNewEmptyCart();
                        unauthCart.setSessionId(sessionId);
                    }

                    currentOrder = unauthCart;
                    currentOrder.setConsumerId(storeToken.getConsumerId());

                    consumerCartRepository.save(currentOrder);
                } else {
                    // merge cart
                    currentOrder = mergeCart(currentOrder, unauthCart);
                    // update accordingly
                    consumerCartRepository.update(storeToken.getCompanyId(), currentOrder.getId(), currentOrder);
                }
            }


            assignedDependencies(currentOrder);
            return currentOrder;
        } else if (unauthCart == null) {
            unauthCart = createNewEmptyCart();
            unauthCart.setSessionId(sessionId);
            consumerCartRepository.save(unauthCart);
        }
        assignedDependencies(unauthCart);
        return unauthCart;
    }

    private ConsumerCart mergeCart(final ConsumerCart authCart, final ConsumerCart unauthCart) {
        ConsumerCart pendingCart = (new Kryo()).copy(authCart);
        if (unauthCart == null) {
            return pendingCart;
        }

        for (OrderItem orderItem : unauthCart.getCart().getItems()) {
            boolean shouldAdd = true;
            for (OrderItem authOrder : pendingCart.getCart().getItems()) {
                if (authOrder.getOrderItemId() == null
                        && authOrder.getId() != null
                        && authCart.getId().equalsIgnoreCase(orderItem.getId())) {
                    shouldAdd = false;
                } else if (authOrder.getOrderItemId() != null
                        && authOrder.getOrderItemId().equalsIgnoreCase(orderItem.getOrderItemId())) {
                    shouldAdd = false;
                }
            }
            if (shouldAdd) {
                pendingCart.getCart().getItems().add(orderItem);
            }
        }
        // Prepare the cart
        try {
            Cart cart = prepareCartHelper(pendingCart);
            pendingCart.setCart(cart);
        } catch (Exception e) {
            // There was an issue with the cart, let's just return it
            LOGGER.error("Error processing cart", e);
        }
        return pendingCart;
    }

    private void assignedDependencies(ConsumerCart consumerCart) {
        HashMap<String, ProductCategory> productCategoryHashMap = categoryRepository.listAllAsMap(storeToken.getCompanyId(), storeToken.getShopId());

        if (consumerCart != null && consumerCart.getCart() != null && consumerCart.getCart().getItems() != null) {
            for (OrderItem orderItem : consumerCart.getCart().getItems()) {
                if (orderItem.getProduct() != null) {
                    orderItem.getProduct().setCategory(productCategoryHashMap.get(orderItem.getProduct().getCategoryId()));
                }
            }
        }

    }

    @Override
    public ConsumerCart getCartById(String consumerCartId) {
        ConsumerCart dbCart = consumerCartRepository.get(storeToken.getCompanyId(), consumerCartId);
        if (dbCart == null) {
            throw new BlazeInvalidArgException("ConsumerCart", "Consumer Cart does not exist.");
        }
        if (dbCart.getConsumerId() != null && !dbCart.getConsumerId().equalsIgnoreCase(storeToken.getConsumerId())) {
            throw new BlazeInvalidArgException("ConsumerCart", "Consumer Cart does not exist for this user.");
        }
        return dbCart;
    }

    @Override
    public ConsumerCart prepareCart(ConsumerCart incomingCart) {
        if (incomingCart == null) {
            incomingCart = createNewEmptyCart();
        }
        Cart preparedCart = prepareCartHelper(incomingCart);
        incomingCart.setCart(preparedCart);
        return incomingCart;
    }

    @Override
    public ConsumerCartResult prepareConsumerCart(ConsumerCartResult incomingCart) {
        return this.prepareNonWidgetConsumerCart(incomingCart);
    }

    private ConsumerCartResult prepareNonWidgetConsumerCart(ConsumerCartResult incomingCart) {
        if (incomingCart == null) {
            throw new BlazeInvalidArgException("ConsumerCart", "ConsumerCart does not exist.");
        }

        Shop shop = shopRepository.get(incomingCart.getCompanyId(), incomingCart.getShopId());

        if (shop == null) {
            throw new BlazeInvalidArgException("ConsumerCart", "Shop not found.");
        }

        Transaction transaction = transactionRepository.get(incomingCart.getCompanyId(), incomingCart.getTransactionId());

        if (transaction == null) {
            transaction = new Transaction();
        }

        transaction.setCart(incomingCart.getCart());
        Transaction.QueueType queueType = incomingCart.getPickupType() == ConsumerCart.ConsumerOrderPickupType.Delivery ? Transaction.QueueType.Delivery : Transaction.QueueType.WalkIn;
        transaction.setQueueType(queueType);

        ConsumerUser consumerUser = consumerUserRepository.getById(incomingCart.getConsumerId());
        Company company = companyRepository.getById(incomingCart.getCompanyId());

        Member member = null;
        if (consumerUser != null) {
            member = memberRepository.get(company.getId(), incomingCart.getMemberId());

            incomingCart.setAccepted(consumerUser.isAccepted());
            incomingCart.setMemberId(consumerUser.getMemberId());
            incomingCart.setDeliveryAddress(incomingCart.getDeliveryAddress() == null ? consumerUser.getAddress() : incomingCart.getDeliveryAddress());
            /*for (ConsumerMemberStatus consumerMemberStatus : consumerUser.getMemberStatuses()) {
                if (consumerMemberStatus.getShopId().equalsIgnoreCase(shop.getId())
                        && consumerMemberStatus.getMemberId() != null) {
                    incomingCart.setMembershipAccepted(consumerMemberStatus.isAccepted());
                    incomingCart.setMemberId(consumerMemberStatus.getMemberId());
                    break;
                }
            }*/

            consumerUser.setPassword(null);
        }

        if (member != null) {
            transaction.setMemberId(member.getId());
            incomingCart.setDeliveryAddress(incomingCart.getDeliveryAddress() == null ? consumerUser.getAddress() : incomingCart.getDeliveryAddress());
        }

        transaction.setDeliveryAddress(incomingCart.getDeliveryAddress());


        final CustomerInfo customerInfo = getMemberGroup(member, consumerUser, shop);
        if (incomingCart.getDeliveryAddress() != null && StringUtils.isNotBlank(incomingCart.getDeliveryAddress().getZipCode())) {
            //update zipcode from selected delivery address
            customerInfo.setZipCode(incomingCart.getDeliveryAddress().getZipCode());
        }
        cartService.prepareCart(shop, transaction, false, Transaction.TransactionStatus.InProgress, true, customerInfo, false);

        incomingCart.setCart(transaction.getCart());

        HashSet<ObjectId> productIds = new LinkedHashSet<>();
        if (incomingCart.getCart() != null && incomingCart.getCart().getItems() != null) {
            for (OrderItem orderItem : incomingCart.getCart().getItems()) {
                if (orderItem.getProductId() != null && ObjectId.isValid(orderItem.getProductId())) {
                    productIds.add(new ObjectId(orderItem.getProductId()));
                }
            }
        }
        HashMap<String, Product> productHashMap = productRepository.findItemsInAsMap(company.getId(), Lists.newArrayList(productIds));
        HashMap<String, ProductCategory> categoryHashMap = productCategoryRepository.listAsMap(company.getId(), shop.getId());

        if (incomingCart.getCart() != null && incomingCart.getCart().getItems() != null) {
            for (OrderItem orderItem : incomingCart.getCart().getItems()) {
                Product product = productHashMap.get(orderItem.getProductId());
                if (product != null) {
                    product.setCategory(categoryHashMap.get(product.getCategoryId()));
                }
                orderItem.setProduct(product);
            }
        }

        incomingCart.setConsumerUser(consumerUser);
        incomingCart.setMember(member);

        return incomingCart;
    }

    private ConsumerCart createNewEmptyCart() {
        ConsumerCart incomingCart = new ConsumerCart();
        incomingCart.prepare(storeToken.getCompanyId());
        incomingCart.setShopId(storeToken.getShopId());
        incomingCart.setCartStatus(ConsumerCart.ConsumerCartStatus.InProgress);
        incomingCart.setPickupType(ConsumerCart.ConsumerOrderPickupType.Delivery);
        incomingCart.setTrackingStatus(ConsumerCart.ConsumerTrackingStatus.NotStarted);
        incomingCart.setSource(storeToken.getSource());
        incomingCart.setTransactionId(null);

        Cart cart = new Cart();
        cart.setId(ObjectId.get().toString());
        cart.setCompanyId(storeToken.getCompanyId());
        incomingCart.setCart(cart);
        return incomingCart;
    }

    @Override
    public ConsumerCart updateCart(String consumerOrderId, ConsumerCart incomingCart) {
        return updateCart(consumerOrderId, incomingCart, ConsumerCart.TransactionSource.Widget);
    }

    private ConsumerCart updateCart(String consumerOrderId, ConsumerCart incomingCart, ConsumerCart.TransactionSource transactionSource) {
        ConsumerCart dbCart = consumerCartRepository.get(storeToken.getCompanyId(), consumerOrderId);
        if (dbCart == null) {
            throw new BlazeInvalidArgException("ConsumerCart", "Consumer Cart does not exist.");
        }
        Cart cart = incomingCart.getCart();
        if (StringUtils.isNotBlank(incomingCart.getRewardName())) {
            String [] rewardNames = incomingCart.getRewardName().split(",");
            HashMap<String, String> errorMap = new HashMap<>();
            List<String> slugList = new ArrayList<>();
            setSlugListandHashMap(rewardNames, errorMap, slugList);
            Iterable<LoyaltyReward> loyaltyRewardIter = rewardRepository.getRewardByNameList(storeToken.getCompanyId(), storeToken.getShopId(), slugList);
            for (LoyaltyReward loyaltyReward : loyaltyRewardIter) {
                if (loyaltyReward != null) {
                    errorMap.remove(loyaltyReward.getName());
                    cart.getPromotionReqs().add(new PromotionReq(null, loyaltyReward.getId(),loyaltyReward.getFinalizedDiscountId()));
                }
            }
            for (Map.Entry<String,String> entry : errorMap.entrySet()) {
                if (entry.getValue() == null)
                    errorMap.replace(entry.getKey(), String.format("%s reward does not exists.",  entry.getKey()));
            }
            incomingCart.setRewardErrorMap(errorMap);
        }
        if (dbCart.isCompleted()) {
            // if it's completed and is not currently in placed, then this cart cannot be altered.
            /*
             */
            if (dbCart.getCartStatus() == ConsumerCart.ConsumerCartStatus.Accepted
                    || dbCart.getCartStatus() == ConsumerCart.ConsumerCartStatus.Completed
                    || dbCart.getCartStatus() == ConsumerCart.ConsumerCartStatus.Declined
                    || dbCart.getCartStatus() == ConsumerCart.ConsumerCartStatus.CanceledByDispensary
                    || dbCart.getCartStatus() == ConsumerCart.ConsumerCartStatus.CanceledByConsumer) {
                // It's just placed so users can continue to update
                throw new BlazeInvalidArgException("ConsumerCart", "This order has already been completed or is being processed.");
            }
        }
        // Force set to delivery if shop only have delivery enabled
        final Shop shop = shopRepository.get(storeToken.getCompanyId(), storeToken.getShopId());
        if (shop != null) {
            if (incomingCart.getPickupType() != ConsumerCart.ConsumerOrderPickupType.Delivery) {
                if (!shop.isShowSpecialQueue() && shop.isShowDeliveryQueue()) {
                    incomingCart.setPickupType(ConsumerCart.ConsumerOrderPickupType.Delivery);
                }
            }
        }

        // if it's still active or currently "placed

        dbCart.setCart(incomingCart.getCart());

        if (storeToken.isAuthenticated()) {

            if (StringUtils.isNotBlank(dbCart.getConsumerId())
                    && !dbCart.getConsumerId().equalsIgnoreCase(storeToken.getConsumerId())) {
                // if the incoming unauth cart is from a different user, throw an error
                throw new BlazeInvalidArgException("ConsumerCart", "This cart is not owned by the current user.");
            }
            // If the user is authenticatd, then merge and set the consumer id accordingly
            ConsumerCart currentOrder = null;

            if (dbCart.isCompleted()) {
                currentOrder = dbCart;
            } else {
                currentOrder = consumerCartRepository.getCurrentInProgressCart(storeToken.getCompanyId(),
                        storeToken.getShopId(),
                        storeToken.getConsumerId());
            }
            if (currentOrder == null) {
                // Convert the incoming cart to a consumer cart
                dbCart.setConsumerId(storeToken.getConsumerId());
            } else {
                // Merge the two cart
                if (dbCart.getId().equalsIgnoreCase(currentOrder.getId())) {
                    // it's the same cart so we should just update it
                    Cart preparedCart = prepareCartHelper(incomingCart, transactionSource == ConsumerCart.TransactionSource.WooCommerce);
                    currentOrder.setCart(preparedCart);
                } else {
                    // else merge
                    currentOrder.getCart().setPromotionReqs(incomingCart.getCart().getPromotionReqs());
                    currentOrder.setRewardErrorMap(incomingCart.getRewardErrorMap());
                    currentOrder.setRewardName("");
                    currentOrder = mergeCart(currentOrder, dbCart);
                }
                currentOrder.setPickupType(incomingCart.getPickupType());

                if (!dbCart.getPickupType().equals(ConsumerCart.ConsumerOrderPickupType.Delivery)) {
                    dbCart.getCart().setDeliveryFee(new BigDecimal(0));
                }

                currentOrder.setMemo(incomingCart.getMemo());
                currentOrder.setPickUpDate(incomingCart.getPickUpDate());
                currentOrder.setDeliveryDate(incomingCart.getDeliveryDate());
                currentOrder.setCompleteAfter(incomingCart.getCompleteAfter());
                currentOrder.setDeliveryAddress(incomingCart.getDeliveryAddress());
                currentOrder.setOrderTags(incomingCart.getOrderTags());
                consumerCartRepository.update(storeToken.getCompanyId(), currentOrder.getId(), currentOrder);

                assignedDependencies(currentOrder);
                return currentOrder;
            }
        } else {
            Cart preparedCart = prepareCartHelper(incomingCart, transactionSource == ConsumerCart.TransactionSource.WooCommerce);
            dbCart.setCart(preparedCart);
            // set consumer info if it's blank
            if (StringUtils.isBlank(dbCart.getConsumerId())) {
                dbCart.setConsumerId(incomingCart.getConsumerId());
                dbCart.setMemberId(incomingCart.getMemberId());
            }
            dbCart.getCart().setPromotionReqs(incomingCart.getCart().getPromotionReqs());
            dbCart.setRewardErrorMap(incomingCart.getRewardErrorMap());
            dbCart.setRewardName("");
        }

        dbCart.setPickupType(incomingCart.getPickupType());
        dbCart.setCompleteAfter(incomingCart.getCompleteAfter());

        dbCart.setMemo(incomingCart.getMemo());
        dbCart.setPickUpDate(incomingCart.getPickUpDate());
        dbCart.setCompleteAfter(incomingCart.getCompleteAfter());
        dbCart.setOrderTags(incomingCart.getOrderTags());
        if (!dbCart.getPickupType().equals(ConsumerCart.ConsumerOrderPickupType.Delivery)) {
            dbCart.getCart().setDeliveryFee(new BigDecimal(0));
        }
        dbCart.setDeliveryDate(incomingCart.getDeliveryDate());
        dbCart.setDeliveryAddress(incomingCart.getDeliveryAddress());
        consumerCartRepository.update(storeToken.getCompanyId(), dbCart.getId(), dbCart);

        assignedDependencies(dbCart);
        return dbCart;
    }

    @Override
    public ConsumerCart cancelCart(String consumerOrderId) {
        ConsumerCart dbCart = consumerCartRepository.get(storeToken.getCompanyId(), consumerOrderId);

        if (dbCart == null) {
            throw new BlazeInvalidArgException("ConsumerCart", "Consumer Cart does not exist.");
        }

        if (!dbCart.isCompleted()) {
            // if this cart is not completed, then we cannot cancel
            throw new BlazeInvalidArgException("ConsumerCart", "This order is still in progressed.");
        } else {
            // if it's completed and is not currently in placed, then this cart cannot be altered.
            if (dbCart.getCartStatus() == ConsumerCart.ConsumerCartStatus.Accepted
                    || dbCart.getCartStatus() == ConsumerCart.ConsumerCartStatus.Completed
                    || dbCart.getCartStatus() == ConsumerCart.ConsumerCartStatus.Declined
                    || dbCart.getCartStatus() == ConsumerCart.ConsumerCartStatus.CanceledByDispensary
                    || dbCart.getCartStatus() == ConsumerCart.ConsumerCartStatus.CanceledByConsumer) {
                // It's just placed so users can continue to update
                throw new BlazeInvalidArgException("ConsumerCart", "This order has already been completed or is being processed.");
            }
        }

        if (!storeToken.isAuthenticated()) {
            throw new BlazeInvalidArgException("ConsumerCart", "User is not authenticated or unknown user.");
        }

        if (StringUtils.isNotBlank(dbCart.getConsumerId())
                && !dbCart.getConsumerId().equalsIgnoreCase(storeToken.getConsumerId())) {
            // if the incoming unauth cart is from a different user, throw an error
            throw new BlazeInvalidArgException("ConsumerCart", "This order is not owned by the current user.");
        }

        dbCart.setCartStatus(ConsumerCart.ConsumerCartStatus.CanceledByConsumer);
        dbCart.setCanceledTime(DateTime.now().getMillis());

        consumerCartRepository.update(storeToken.getCompanyId(), dbCart.getId(), dbCart);
        return dbCart;
    }

    @Override
    public ConsumerCart submitCart(String consumerOrderId, ConsumerCart incomingCart, ConsumerCart.TransactionSource transactionSource) {

        ConsumerCart dbCart = consumerCartRepository.get(storeToken.getCompanyId(), consumerOrderId);
        if (dbCart == null) {
            throw new BlazeInvalidArgException("ConsumerCart", "Consumer Cart does not exist.");
        }
        if (dbCart.isCompleted()) {
            throw new BlazeInvalidArgException("ConsumerCart", "This cart has been previously completed/canceled.");
        }

        if (incomingCart.getCart().getItems().size() == 0) {
            throw new BlazeInvalidArgException("ConsumerCart", "Cart is empty.");
        }

        ConsumerUser consumerUser = consumerUserRepository.getById(dbCart.getConsumerId());
        if (consumerUser == null) {
            throw new BlazeAuthException("Authorization", "User could not be found.");
        }
        if (!consumerUser.getId().equalsIgnoreCase(dbCart.getConsumerId())) {
            throw new BlazeAuthException("Authorization", "User cart does not exits for this user.");
        }


        Shop shop = shopRepository.get(storeToken.getCompanyId(), storeToken.getShopId());
        if (shop == null || shop.isActive() == false) {
            throw new BlazeInvalidArgException("Shop", "This shop is no longer active.");
        }


        if (shop.isEnableMedicinalAge() && (ConsumerType.MedicinalThirdParty.equals(consumerUser.getConsumerType()) || ConsumerType.MedicinalState.equals(consumerUser.getConsumerType()))) {

            if (consumerUser.getDob() == null) {
                throw new BlazeInvalidArgException("ConsumerUser", "Please update consumer user's dob");
            }
            int userAge = DateUtil.getYearsBetweenTwoDates(consumerUser.getDob(), DateTime.now().getMillis());

            int medicalAge = shop.getMedicinalAge();
            if (medicalAge > userAge) {
                throw new BlazeInvalidArgException("Member", String.format("Member does not meet minimum age of %d.", shop.getMedicinalAge()));
            }
        }

        if (shop.isEnableAgeLimit() && ConsumerType.AdultUse.equals(consumerUser.getConsumerType())) {
            if (consumerUser.getDob() == null) {
                throw new BlazeInvalidArgException("ConsumerUser", "Please update consumer user's dob");
            }
            int userAge = DateUtil.getYearsBetweenTwoDates(consumerUser.getDob(), DateTime.now().getMillis());

            int adultAge = shop.getAgeLimit();
            if (adultAge > userAge) {
                throw new BlazeInvalidArgException("Member", String.format("Member does not meet minimum age of %d.", shop.getAgeLimit()));
            }
        }




        Member member = null;
        // find member by status if any
        if (StringUtils.isNotBlank(consumerUser.getMemberId())) {
            member = memberRepository.get(storeToken.getCompanyId(), consumerUser.getMemberId());
        }
        /*if (consumerUser.getMemberStatuses() != null) {
            Company company = companyRepository.getById(storeToken.getCompanyId());
            String memberId = null;
            if (company.getMembersShareOption() == Company.CompanyMembersShareOption.Shared) {
                memberId = consumerUser.getMemberIdForCompany(storeToken.getCompanyId());
            } else {
                memberId = consumerUser.getMemberIdForShop(storeToken.getShopId());
            }
            member = memberRepository.get(storeToken.getCompanyId(), memberId);
        }*/

        if (member == null) {
            // Create new member
            Iterable<Member> membersByEmail = memberRepository.getMemberByEmail(storeToken.getCompanyId(),
                    consumerUser.getEmail().toLowerCase());
            for (Member m : membersByEmail) {
                for (Identification identification : m.getIdentifications()) {
                    if (StringUtils.isNotBlank(identification.getLicenseNumber())
                            && identification.getLicenseNumber().equalsIgnoreCase(consumerUser.getDlNo())) {
                        member = m;
                        identification.setExpirationDate(consumerUser.getDlExpiration());
                        identification.setState(consumerUser.getDlState());

                        if (consumerUser.getDlPhoto() != null) {
                            CompanyAsset dlPhoto = consumerUser.getDlPhoto().toCompanyAsset(storeToken.getCompanyId());
                            try {
                                companyAssetRepository.save(dlPhoto);
                                identification.setFrontPhoto(dlPhoto);

                                List<CompanyAsset> companyAssets = identification.getAssets();
                                if (companyAssets == null) {
                                    companyAssets = new ArrayList<>();
                                }
                                companyAssets.add(dlPhoto);
                                identification.setAssets(companyAssets);
                            } catch (Exception e) {
                                // Ignore
                            }
                        }
                        break;
                    }
                }
            }
        }

        if (shop.isEnableCannabisLimit()) {
            boolean status = cannabisLimitService.checkCannabisLimit(storeToken.getCompanyId(), shop, dbCart.getConsumerId(), null, incomingCart, true);
            if (!status) {
                throw new BlazeInvalidArgException("CannabisLimit", "State Cannabis Limit is reached.");
            }
        }



        Cart preparedCart = prepareCartHelper(incomingCart, transactionSource == ConsumerCart.TransactionSource.WooCommerce);
        dbCart.setCart(preparedCart);
        dbCart.setCartStatus(ConsumerCart.ConsumerCartStatus.Placed);
        dbCart.setCompleted(true);
        dbCart.setTrackingStatus(ConsumerCart.ConsumerTrackingStatus.Placed);
        dbCart.setOrderPlacedTime(DateTime.now().getMillis());
        dbCart.setPickupType(incomingCart.getPickupType());
        dbCart.setTransactionSource(transactionSource);
        dbCart.setDeliveryDate(incomingCart.getDeliveryDate());
        dbCart.setDeliveryAddress(incomingCart.getDeliveryAddress() == null ? consumerUser.getAddress() : incomingCart.getDeliveryAddress());
        // Force set to delivery if shop only have delivery enabled
        dbCart.setOrderTags(incomingCart.getOrderTags());
        dbCart.setMemo(incomingCart.getMemo());



        if (shop != null) {
            if (dbCart.getPickupType() != ConsumerCart.ConsumerOrderPickupType.Delivery) {
                if (!shop.isShowSpecialQueue() && shop.isShowDeliveryQueue()) {
                    dbCart.setPickupType(ConsumerCart.ConsumerOrderPickupType.Delivery);
                }
            }

            if (shop.getOnlineStoreInfo() != null) {
                if (dbCart.getPickupType() == ConsumerCart.ConsumerOrderPickupType.Delivery) {
                    if (shop.getOnlineStoreInfo().isEnableDeliveryAreaRestrictions()
                            && shop.getOnlineStoreInfo().getRestrictedZipCodes() != null
                            && shop.getOnlineStoreInfo().getRestrictedZipCodes().size() > 0) {

                        if (dbCart.getDeliveryAddress() == null
                                || StringUtils.isBlank(dbCart.getDeliveryAddress().getZipCode())
                                || !shop.getOnlineStoreInfo().getRestrictedZipCodes().contains(dbCart.getDeliveryAddress().getZipCode().trim())) {
                            throw new BlazeInvalidArgException("ConsumerCart", "The store does not deliver to this zipcode.");
                        }
                    }
                }

                // check if online POS is enabled
                if (!shop.getOnlineStoreInfo().isEnableOnlinePOS()) {
                    throw new BlazeInvalidArgException("ConsumerCart", "The store does not accept orders at this time.");
                }
            }

            dbCart.setEta(shop.getOnlineStoreInfo().getDefaultETA());

            // Check Cart Minimum
            checkCartByMinimum(shop, dbCart, consumerUser);
        }

        // all tests are passed, create consumer cart no
        UniqueSequence sequence = uniqueSequenceRepository.getNewIdentifier(storeToken.getCompanyId(), storeToken.getShopId(), "ConsumerCartOrderNo");
        dbCart.setOrderNo("" + sequence.getCount());


        // Only change user if dbCart's consumerUserId is blank
        if (StringUtils.isNotBlank(dbCart.getConsumerId())) {
            dbCart.setConsumerId(storeToken.getConsumerId());
        }

        if (member != null) {
            dbCart.setMemberId(member.getId());

            if (!consumerUser.isAccepted() || !member.getId().equalsIgnoreCase(consumerUser.getMemberId())) {
                consumerUser.setAccepted(Boolean.TRUE);
                consumerUser.setAcceptedDate(DateTime.now().getMillis());
                consumerUserRepository.update(storeToken.getCompanyId(), consumerUser.getId(), consumerUser);
            }

            if (consumerUser.getAddress() != null
                    && StringUtils.isNotBlank(consumerUser.getAddress().getZipCode())) {
                member.setConsumerUserId(consumerUser.getId());
                member.setAddress(consumerUser.getAddress());
                member.getAddress().setId(ObjectId.get().toString());
                member.getAddress().setCompanyId(storeToken.getCompanyId());

                member.setConsumerUserId(consumerUser.getId());
                memberRepository.update(storeToken.getCompanyId(), member.getId(), member);
                realtimeService.sendRealTimeEvent(storeToken.getShopId().toString(), RealtimeService.RealtimeEventType.MembersUpdateEvent, null);
            }
        }


        dbCart.setPublicKey(UUID.randomUUID().toString());
        consumerCartRepository.update(storeToken.getCompanyId(), dbCart.getId(), dbCart);


        consumerNotificationService.sendNewOrderSubmittedNotification(dbCart, consumerUser, shop);


        //
        // -- POST EVENTS
        //

        // Send messages to terminals
        if (shop.isRestrictIncomingOrderNotifications()) {
            String title = "New order";
            employeeNotificationService.sendMessageToSpecifiedTerminals(shop.getCompanyId(), shop.getId(), shop.getRestrictedNotificationTerminals(), String.format("You've received a new online order from %s %s. Order Number : #%s", consumerUser.getFirstName(), consumerUser.getLastName(), dbCart.getOrderNo()), FcmPayload.Type.ONLINE_ORDER, dbCart.getTransactionId(), dbCart.getTransNo(), title, FcmPayload.SubType.NEW_ORDER, null);
        } else {
            String title = "New order";
            employeeNotificationService.sendMessageToAllTerminals(shop.getCompanyId(), shop.getId(),
                    String.format("You've received a new online order from %s %s. Order Number : #%s", consumerUser.getFirstName(), consumerUser.getLastName(), dbCart.getOrderNo()), FcmPayload.Type.ONLINE_ORDER, dbCart.getTransactionId(), dbCart.getTransNo(), title, FcmPayload.SubType.NEW_ORDER);
        }

        final ConsumerOrderData consumerOrderData = new ConsumerOrderData();
        consumerOrderData.setConsumerName(consumerUser.getFirstName() + " " + consumerUser.getLastName());
        consumerOrderData.setConsumerOrderNo(dbCart.getOrderNo());
        consumerOrderData.setConsumerOrderId(dbCart.getId());
        if (member != null)
            consumerOrderData.setMemberName(member.getFirstName() + " " + member.getLastName());
        consumerOrderData.setOrderTime(dbCart.getOrderPlacedTime());
        consumerOrderData.setOrderStatus(dbCart.getCartStatus());
        consumerOrderData.setCart(dbCart.getCart());

        try {
            partnerWebHookManager.newConsumerOrderHook(storeToken.getCompanyId(), storeToken.getShopId(), consumerOrderData);
        } catch (Exception e) {
            LOGGER.warn("new consumer order webhook failed for shopId:" + storeToken.getShopId() + " companyId:" + storeToken.getCompanyId(), e);
        }



        realtimeService.sendRealTimeEvent(storeToken.getShopId(),
                RealtimeService.RealtimeEventType.ConsumerCartsUpdate, null);

        // send event notifications for incoming orders
        IncomingOrderEvent event = new IncomingOrderEvent(consumerUser,dbCart,shop);
        eventBus.post(event);
        return dbCart;
    }

    /**
     * This method checks minimum amount for a cart
     *
     * @param shop         : Object of shop
     * @param consumerCart : cart's object
     * @param user         : member's object
     * @implNote : This method checks if cartMinimums present in shop other wise it check for OnlineStoreInfo's minimum (Previous implementation)
     * After getting cart minimum it checks weather total amount (sub total or sub total with discount) is greater than minimum amount or not otherwise throws an error
     */
    private void checkCartByMinimum(Shop shop, ConsumerCart consumerCart, ConsumerUser user) {
        if (shop != null && shop.getOnlineStoreInfo() != null) {
            int cartMin = 0;
            OnlineStoreInfo storeInfo = shop.getOnlineStoreInfo();

            CartMinimums shopCartMinimum = shop.getCartMinimums();

            if (shopCartMinimum == null) {
                return;
            }

            // Get the cart minimum
            if (shopCartMinimum.getMinimumsType() == CartMinimums.MinimumsType.SIMPLE) {
                cartMin = (int) (this.getSimpleCartMinimum(shopCartMinimum, storeInfo) * 100);
            } else if (consumerCart.getPickupType() == ConsumerCart.ConsumerOrderPickupType.Delivery) {
                if (shopCartMinimum.getMinimumsType() == CartMinimums.MinimumsType.ZIP_CODE) {
                    String zipCode = "";
                    if (user.getAddress() != null && StringUtils.isNotBlank(user.getAddress().getZipCode())) {
                        zipCode = user.getAddress().getZipCode();
                    }
                    if (StringUtils.isBlank(zipCode)) {
                        boolean zipEnabled = cartMinimumZipCodesEnabled(shopCartMinimum.getCartMinimumDetails());
                        if (zipEnabled) {
                            throw new BlazeInvalidArgException("Delivery Address", String.format("Address is required for delivery orders."));
                        }
                    }

                    // fetch cart mins
                    BigDecimal minimum = this.getCartMinimumByZipCode(shopCartMinimum.getCartMinimumDetails(), zipCode);

                    if (minimum.doubleValue() < 0) {
                        minimum = new BigDecimal(0);
                    }
                    cartMin = (int) (minimum.doubleValue() * 100);
                } else if (shopCartMinimum.getMinimumsType() == CartMinimums.MinimumsType.DISTANCE) {
                    // get cart minimum on distance
                }
            }

            if (cartMin > 0) {
                // get the total to check
                BigDecimal cartWithDiscounts = consumerCart.getCart().getSubTotalDiscount();
                if (storeInfo.getCartMinType() == OnlineStoreInfo.StoreCartMinimumType.Subtotal) {
                    cartWithDiscounts = consumerCart.getCart().getSubTotal();
                }

                int totalCost = (int) (cartWithDiscounts.doubleValue() * 100);
                LOGGER.info(String.format("Total Cost: %d,  Cart Min: %d", totalCost, cartMin));
                if (cartMin > totalCost) {
                    throw new BlazeInvalidArgException("Cart Minimum", String.format("Minimum cart amount %s is required.",
                            TextUtil.toCurrency(cartMin / 100d, shop.getDefaultCountry())));
                }
            }
        }
    }

    private double getSimpleCartMinimum(CartMinimums shopCartMinimum, OnlineStoreInfo storeInfo) {
        double minimum;

        // If shop's cart minimum is available then use it otherwise use online store's cart minimum
        if (shopCartMinimum.getCartMinimumDetails() != null && shopCartMinimum.getCartMinimumDetails().size() > 0) {
            CartMinimumDetails cartMinimumDetails = shopCartMinimum.getCartMinimumDetails().stream().findFirst().get();
            minimum = cartMinimumDetails.getMinimum().doubleValue();
        } else {
            minimum = storeInfo.getCartMinimum().doubleValue();
        }

        return minimum;
    }

    private BigDecimal getCartMinimumByZipCode(Set<CartMinimumDetails> cartMinimumDetails, String zipCode) {

        if (cartMinimumDetails != null) {
            for (CartMinimumDetails cartMinimumDetail : cartMinimumDetails) {
                if (cartMinimumDetail.getEnabled() && cartMinimumDetail.getMinimum().doubleValue() > 0
                        && cartMinimumDetail.getZipCodes() != null && cartMinimumDetail.getZipCodes().contains(zipCode)) {
                    return cartMinimumDetail.getMinimum();
                }
            }
        }

        return new BigDecimal(0);
    }

    private boolean cartMinimumZipCodesEnabled(Set<CartMinimumDetails> cartMinimumDetails) {

        if (cartMinimumDetails != null) {
            for (CartMinimumDetails cartMinimumDetail : cartMinimumDetails) {
                if (cartMinimumDetail.getEnabled() && cartMinimumDetail.getMinimum().doubleValue() > 0) {
                    return true;
                }
            }
        }

        return false;
    }


    @Override
    public SearchResult<ConsumerCart> getOrderHistory(int start, int limit) {

        if (limit > 100) {
            limit = 100;
        }
        if (start < 0) {
            start = 0;
        }

        SearchResult<ConsumerCart> orders = consumerCartRepository.getCompletedOrders(storeToken.getCompanyId(),
                storeToken.getShopId(),
                storeToken.getConsumerId(),
                true,
                start,
                limit);

        List<ObjectId> transIds = new ArrayList<>();
        for (ConsumerCart consumerCart : orders.getValues()) {
            if (StringUtils.isNotBlank(consumerCart.getTransactionId())) {
                transIds.add(new ObjectId(consumerCart.getTransactionId()));
            }
        }
        HashMap<String, Transaction> transactions = transactionRepository.findItemsInAsMap(storeToken.getCompanyId(), transIds);
        List<ObjectId> employeeIds = new ArrayList<>();
        List<ObjectId> productIds = new ArrayList<>();
        for (Transaction transaction : transactions.values()) {
            if (StringUtils.isNotBlank(transaction.getAssignedEmployeeId())) {
                employeeIds.add(new ObjectId(transaction.getAssignedEmployeeId()));
            }
        }

        for (ConsumerCart consumerCart : orders.getValues()) {
            if (consumerCart.getCart() != null) {
                for (OrderItem orderItem : consumerCart.getCart().getItems()) {
                    if (StringUtils.isNotBlank(orderItem.getProductId())) {
                        productIds.add(new ObjectId(orderItem.getProductId()));
                    }
                }
            }
        }

        HashMap<String, Employee> employeeHashMap = employeeRepository.findItemsInAsMap(storeToken.getCompanyId(), employeeIds);
        HashMap<String, Product> productHashMap = productRepository.findProductsByIdsAsMap(storeToken.getCompanyId(), storeToken.getShopId(), productIds);
        HashMap<String, ProductCategory> productCategoryHashMap = categoryRepository.listAllAsMap(storeToken.getCompanyId(), storeToken.getShopId());

        for (ConsumerCart consumerCart : orders.getValues()) {
            if (StringUtils.isNotBlank(consumerCart.getTransactionId())) {
                Transaction transaction = transactions.get(consumerCart.getTransactionId());
                if (transaction != null) {
                    Employee employee = employeeHashMap.get(transaction.getAssignedEmployeeId());
                    consumerCart.setEmployeeName((employee != null ? employee.getFirstName() : ""));
                }
            }
            if (consumerCart.getCart() != null) {
                for (OrderItem orderItem : consumerCart.getCart().getItems()) {
                    orderItem.setProduct(productHashMap.get(orderItem.getProductId()));
                    if (orderItem.getProduct() != null) {
                        orderItem.getProduct().setCategory(productCategoryHashMap.get(orderItem.getProduct().getCategoryId()));
                    }
                }
            }
        }

        return orders;
    }

    @Override
    public WooComCartResult prepareWoocommerceInfo(WooCartPrepareRequest cartPrepareRequest, String sessionId) {

        List<OrderItemRequest> productCostRequests = cartPrepareRequest.getProductCostRequests();
        if (productCostRequests == null) {
            productCostRequests = Lists.newArrayList();
        }

        final ConsumerCart consumerCart = this.getCurrentActiveCart(sessionId);

        final Cart cart = consumerCart.getCart();
        cart.getItems().clear();


        List<String> productIds = new ArrayList<>();
        List<ObjectId> productObjIds = new ArrayList<>();

        for (OrderItemRequest productCostRequest : productCostRequests) {
            if (StringUtils.isNotBlank(productCostRequest.getProductId()) && ObjectId.isValid(productCostRequest.getProductId())) {
                productIds.add(productCostRequest.getProductId());
                productObjIds.add(new ObjectId(productCostRequest.getProductId()));
            }
        }

        final HashMap<String, Product> productHashMap = productRepository.listAsMap(storeToken.getCompanyId(), productObjIds);

        for (OrderItemRequest productCostRequest : productCostRequests) {

            if (productHashMap.get(productCostRequest.getProductId()) == null) {
                throw new BlazeInvalidArgException("Product id", "Product id " + productCostRequest.getProductId() + " is invalid");
            }

            final OrderItem order = new OrderItem();
            order.prepare(storeToken.getCompanyId());
            order.setStatus(OrderItem.OrderItemStatus.Active);
            order.setCost(new BigDecimal(0));
            order.setFinalPrice(new BigDecimal(0));
            order.setProductId(productCostRequest.getProductId());
            order.setUseUnitQty(productCostRequest.isUseUnitQty());
            order.setWeightKey(productCostRequest.getWeightKey());

            if (order.isUseUnitQty()) {
                order.setUnitQty(productCostRequest.getQuantity().intValue());
            } else {
                order.setQuantity(productCostRequest.getQuantity());
            }
            cart.getItems().add(order);

        }

        consumerCart.setPickupType(cartPrepareRequest.getPickupType());

        cart.setPaymentOption(cartPrepareRequest.getPaymentOption());

        cart.setPromoCode(StringUtils.EMPTY);
        cart.getPromotionReqLogs().clear();
        cart.getPromotionReqs().clear();
        if (StringUtils.isNotBlank(cartPrepareRequest.getPromoCode())) {
            cart.setPromoCode(cartPrepareRequest.getPromoCode());
        }

        if (StringUtils.isNotBlank(cartPrepareRequest.getRewardName())) {
            String [] rewardNames = cartPrepareRequest.getRewardName().split(",");
            HashMap<String, String> errorMap = new HashMap<>();
            List<String> slugList = new ArrayList<>();
            setSlugListandHashMap(rewardNames, errorMap, slugList);
            Iterable<LoyaltyReward> loyaltyReward = rewardRepository.getRewardByNameList(storeToken.getCompanyId(), storeToken.getShopId(), slugList);
            for (LoyaltyReward loyaltyRewardList : loyaltyReward) {
                if (loyaltyRewardList != null) {
                    errorMap.remove(loyaltyRewardList.getName());
                    cart.getPromotionReqs().add(new PromotionReq(null, loyaltyRewardList.getId(),loyaltyRewardList.getFinalizedDiscountId()));
                }
            }
            for (Map.Entry<String,String> entry : errorMap.entrySet()) {
                if (entry.getValue() == null)
                    errorMap.replace(entry.getKey(), String.format("%s reward does not exists.",  entry.getKey()));
            }
            consumerCart.setRewardErrorMap(errorMap);
        }

        if (cartPrepareRequest.getDeliveryAddress() != null) {
            consumerCart.setDeliveryAddress(cartPrepareRequest.getDeliveryAddress());
        }
        consumerCart.setDeliveryDate(cartPrepareRequest.getDeliveryDate());


        final Cart preparedCart = this.prepareCartHelper(consumerCart, true);

        final WooComCartResult wooComCartResult = new WooComCartResult();
        wooComCartResult.setDiscount(preparedCart.getDiscount());
        wooComCartResult.setSubTotalDiscount(preparedCart.getSubTotalDiscount());
        wooComCartResult.setCalcCartDiscount(preparedCart.getCalcCartDiscount());
        wooComCartResult.setTotalDiscount(preparedCart.getTotalDiscount());

        wooComCartResult.setTax(preparedCart.getTax());
        wooComCartResult.setTaxResult(preparedCart.getTaxResult());
        wooComCartResult.setTotalPreCalcTax(preparedCart.getTotalPreCalcTax());
        wooComCartResult.setTotalCalcTax(preparedCart.getTotalCalcTax());

        wooComCartResult.setSubTotal(preparedCart.getSubTotal());
        wooComCartResult.setTotal(preparedCart.getTotal());
        wooComCartResult.setEnableDeliveryFee(preparedCart.isEnableDeliveryFee());
        wooComCartResult.setDeliveryFee(preparedCart.getDeliveryFee());
        wooComCartResult.setPromoCode(preparedCart.getPromoCode());

        wooComCartResult.setPointSpent(preparedCart.getPointSpent());
        wooComCartResult.setPaymentOption(preparedCart.getPaymentOption());
        wooComCartResult.setCreditCardFee(preparedCart.getCreditCardFee());

        if (CollectionUtils.isNotEmpty(preparedCart.getPromotionReqs())) {
            StringBuilder wooRewardName = new StringBuilder("");
            ArrayList<String> rewardName = new ArrayList<>();
            List<ObjectId> rewardIds = new ArrayList<>();
            for (PromotionReq promotionReq : preparedCart.getPromotionReqLogs()) {
                if (promotionReq.getRewardId() != null && ObjectId.isValid(promotionReq.getRewardId()))
                    rewardIds.add(new ObjectId(promotionReq.getRewardId()));
                }
            Iterable<LoyaltyReward> loyaltyReward = rewardRepository.findItemsIn(storeToken.getCompanyId(), storeToken.getShopId(), rewardIds);
            for (LoyaltyReward reward : loyaltyReward) {
                if (reward != null)
                    rewardName.add(reward.getName());
                }
            for (int i = 0; i < rewardName.size(); i++) {
                if (StringUtils.isNotBlank(wooRewardName))
                    wooRewardName.append(", ");
                wooRewardName.append(rewardName.get(i));
            }
             wooComCartResult.setRewardName(wooRewardName.toString());
        }
        for (OrderItem orderItem : preparedCart.getItems()) {

            final WooComCartResult.WooComCartItem wooComCartItem = new WooComCartResult.WooComCartItem();
            wooComCartItem.setCalcDiscount(orderItem.getCalcDiscount());
            wooComCartItem.setCost(orderItem.getCost());
            wooComCartItem.setDiscount(orderItem.getDiscount());
            wooComCartItem.setDiscountedQty(orderItem.getDiscountedQty());
            wooComCartItem.setDiscountType(orderItem.getDiscountType());
            wooComCartItem.setFinalPrice(orderItem.getFinalPrice());
            wooComCartItem.setOrigQuantity(orderItem.getOrigQuantity());
            wooComCartItem.setProductId(orderItem.getProductId());
            wooComCartItem.setQuantity(orderItem.getQuantity());
            wooComCartItem.setUnitPrice(orderItem.getUnitPrice());
            wooComCartItem.setUseUnitQty(orderItem.isUseUnitQty());
            wooComCartItem.setWeightKey(orderItem.getWeightKey());
            wooComCartItem.setRequestQuantity(BigDecimal.valueOf(orderItem.getUnitQty()));

            wooComCartResult.getItems().add(wooComCartItem);
        }

        wooComCartResult.setConsumerCartId(consumerCart.getId());
        wooComCartResult.setSessionId(consumerCart.getSessionId());
        wooComCartResult.setRewardErrorMap(consumerCart.getRewardErrorMap());

        final Shop shop = shopRepository.get(storeToken.getCompanyId(), storeToken.getShopId());

        // Force set to delivery if shop only have delivery enabled
        if (shop != null) {
            if (consumerCart.getPickupType() != ConsumerCart.ConsumerOrderPickupType.Delivery) {
                if (!shop.isShowSpecialQueue() && shop.isShowDeliveryQueue()) {
                    consumerCart.setPickupType(ConsumerCart.ConsumerOrderPickupType.Delivery);
                }
            }
        }

        consumerCartRepository.update(storeToken.getCompanyId(), consumerCart.getId(), consumerCart);

        if (shop.isShowDeliveryQueue() && shop.getOnlineStoreInfo().isEnableDelivery()) {
            wooComCartResult.getPickupTypes().add(ConsumerCart.ConsumerOrderPickupType.Delivery);
        }

        if (shop.isShowSpecialQueue() && shop.getOnlineStoreInfo().isEnableStorePickup()) {
            wooComCartResult.getPickupTypes().add(ConsumerCart.ConsumerOrderPickupType.Pickup);
        }


        wooComCartResult.setPickUpAddress(shop.getAddress());
        wooComCartResult.setDeliveryAddress(consumerCart.getDeliveryAddress());
        wooComCartResult.setDeliveryDate(consumerCart.getDeliveryDate());
        wooComCartResult.setMemberGroup(consumerCart.getMemberGroup());
        wooComCartResult.setErrorMsg(consumerCart.getErrorMsg());
        return wooComCartResult;
    }

    private Cart prepareCartHelper(ConsumerCart consumerCart) {
        return prepareCartHelper(consumerCart, false);
    }

    @Override
    public ProductCostResult findCost(ProductCostRequest request) {
        Product product = productRepository.get(storeToken.getCompanyId(), request.getProductId());
        if (product == null) {
            throw new BlazeInvalidArgException("ProductCost", "Product does not exist.");
        }
        ProductCategory category = categoryRepository.get(storeToken.getCompanyId(), product.getCategoryId());
        if (category == null) {
            throw new BlazeInvalidArgException("Product", "Unknown product category.");
        }

        MemberGroupPrices memberGroupPrices = null;
        if (StringUtils.isNotBlank(storeToken.getConsumerId())) {
            Member member = memberRepository.getMemberWithConsumerId(storeToken.getCompanyId(), storeToken.getConsumerId());
            if (member != null) {
                memberGroupPrices = memberGroupPricesRepository.getPricesForProductGroup(storeToken.getCompanyId(),
                        storeToken.getShopId(),
                        product.getId(),
                        member.getMemberGroupId(),
                        MemberGroupPrices.class);
            }
        }


        double cost = 0d;
        double calcQuantity = request.getQuantity().doubleValue();
        if (category.getUnitType() == ProductCategory.UnitType.units) {
            BigDecimal productPrice = product.getUnitPriceWithPriceBreaks(request.getQuantity().intValue(), memberGroupPrices, null);
            cost = productPrice.doubleValue() * calcQuantity;
        } else {
            HashMap<String, ProductWeightTolerance> toleranceHashMap = weightToleranceRepository.listAsMap(storeToken.getCompanyId());
            // Gram type
            List<ProductPriceRange> priceRanges = product.getPriceRanges();
            if (memberGroupPrices != null && memberGroupPrices.isEnabled()) {
                if (product.getPriceRanges().size() > 0) {
                    priceRanges = memberGroupPrices.getPriceRanges();
                }
            }
            priceRanges.sort(new Comparator<ProductPriceRange>() {
                @Override
                public int compare(ProductPriceRange o1, ProductPriceRange o2) {
                    Integer one = new Integer(o1.getPriority());
                    Integer two = new Integer(o2.getPriority());
                    return one.compareTo(two) * -1;
                }
            });

            ProductPriceRange priceRange = null;
            for (ProductPriceRange productPriceRange : priceRanges) {
                ProductWeightTolerance tolerance = toleranceHashMap.get(productPriceRange.getWeightToleranceId());
                if (tolerance != null) {
                    if (request.getQuantity().doubleValue() >= tolerance.getStartWeight().doubleValue()) {
                        priceRange = productPriceRange;
                        break;
                    }
                }
            }
            // does not match, get the cheapest one
            if (priceRange == null) {
                if (priceRanges.size() > 0) {
                    // get last item
                    priceRange = priceRanges.get(priceRanges.size() - 1);
                }
            }

            if (priceRange != null) {
                ProductWeightTolerance tolerance = toleranceHashMap.get(priceRange.getWeightToleranceId());
                double rangePrice = priceRange.getPrice() != null ? priceRange.getPrice().doubleValue() : 0;
                double toleranceUnitPrice = tolerance != null && tolerance.getWeightValue() != null ? tolerance.getWeightValue().doubleValue() : 0;

                if (calcQuantity >= tolerance.getStartWeight().doubleValue()
                        && calcQuantity <= tolerance.getEndWeight().doubleValue()) {
                    if (priceRange.getPrice() != null) {
                        cost = rangePrice;
                    }
                } else if (toleranceUnitPrice > 0) {
                    cost = calcQuantity * (rangePrice / toleranceUnitPrice);
                } else {
                    cost = 0;
                }
            } else {
                cost = product.getUnitPrice().doubleValue() * calcQuantity;
            }
        }
        ProductCostResult result = new ProductCostResult();
        result.setProductId(request.getProductId());
        result.setQuantity(request.getQuantity());
        result.setCost(new BigDecimal(cost));

        return result;
    }

    @Override
    public ConsumerCart submitWooOrder(final WooCartRequest wooCartRequest) {

        if (storeToken.isAuthenticated() && StringUtils.isNotBlank(storeToken.getConsumerId())) {
            // Consumer user might not be available on this cart, so checking with session id and consumer cart id
            final ConsumerCart consumerCart = consumerCartRepository.getCurrentInProgressCartWithSessionId(storeToken.getCompanyId(), storeToken.getShopId(), wooCartRequest.getSessionId());

            if (consumerCart != null && consumerCart.getId().equals(wooCartRequest.getConsumerCartId())) {

                // Valid cart & valid consumer user
                final ConsumerUser consumerUser = consumerUserRepository.getById(storeToken.getConsumerId());

                if (consumerUser == null) {
                    throw new BlazeAuthException("Authentication", "Consumer user is not available");
                }
                String email = wooCartRequest.getEmail();
                if (email != null) {
                    email = email.toLowerCase().trim();
                }

                consumerUser.setAddress(wooCartRequest.getAddress());
                consumerUser.setPrimaryPhone(wooCartRequest.getPrimaryPhone());
                consumerUser.setEmail(email);
                consumerUser.setNotificationType(wooCartRequest.getNotificationType());
                consumerUser.setPassword(null);
                consumerUserService.updateUser(consumerUser, Boolean.FALSE);



                consumerCart.setMemo(wooCartRequest.getMemo());
                consumerCart.setPickupType(wooCartRequest.getPickupType());
                consumerCart.getCart().setPromoCode(wooCartRequest.getPromoCode());
                consumerCart.getCart().getItems().clear();
                consumerCart.getCart().getPromotionReqLogs().clear();
                consumerCart.getCart().getPromotionReqs().clear();
                consumerCart.getCart().setPaymentOption(wooCartRequest.getPaymentOption());

                if (StringUtils.isNotBlank(wooCartRequest.getRewardName())) {
                    String [] rewardNames = wooCartRequest.getRewardName().split(",");
                    HashMap<String, String> errorMap = new HashMap<>();
                    List<String> slugList = new ArrayList<>();
                    setSlugListandHashMap(rewardNames, errorMap, slugList);
                    Iterable<LoyaltyReward> loyaltyReward = rewardRepository.getRewardByNameList(storeToken.getCompanyId(), storeToken.getShopId(), slugList);
                    for (LoyaltyReward loyaltyRewardList : loyaltyReward) {
                        if (loyaltyRewardList != null) {
                            errorMap.remove(loyaltyRewardList.getName());
                            consumerCart.getCart().getPromotionReqs().add(new PromotionReq(null, loyaltyRewardList.getId(), loyaltyRewardList.getFinalizedDiscountId()));
                        }
                    }
                    for (Map.Entry<String,String> entry : errorMap.entrySet()) {
                        if (entry.getValue() == null)
                            errorMap.replace(entry.getKey(), String.format("%s reward does not exists.",  entry.getKey()));
                    }
                    consumerCart.setRewardErrorMap(errorMap);
                }
                final HashMap<String, Product> productHashMap = productRepository.listAsMap(storeToken.getCompanyId(), storeToken.getShopId());
                for (OrderItemRequest productCostRequest : wooCartRequest.getProductCostRequests()) {

                    if (productHashMap.get(productCostRequest.getProductId()) == null) {
                        throw new BlazeInvalidArgException("Product id", "Product id " + productCostRequest.getProductId() + " is invalid");
                    }

                    final OrderItem order = new OrderItem();
                    order.prepare(storeToken.getCompanyId());
                    order.setStatus(OrderItem.OrderItemStatus.Active);
                    order.setCost(new BigDecimal(0));
                    order.setFinalPrice(new BigDecimal(0));
                    order.setProductId(productCostRequest.getProductId());

                    order.setUseUnitQty(productCostRequest.isUseUnitQty());
                    order.setWeightKey(productCostRequest.getWeightKey());

                    if (order.isUseUnitQty()) {
                        order.setUnitQty(productCostRequest.getQuantity().intValue());
                    } else {
                        order.setQuantity(productCostRequest.getQuantity());
                    }

                    consumerCart.getCart().getItems().add(order);

                }

                final Shop shop = shopRepository.get(storeToken.getCompanyId(), storeToken.getShopId());
                // Force set to delivery if shop only have delivery enabled
                if (shop != null) {
                    if (consumerCart.getPickupType() != ConsumerCart.ConsumerOrderPickupType.Delivery) {
                        if (!shop.isShowSpecialQueue() && shop.isShowDeliveryQueue()) {
                            consumerCart.setPickupType(ConsumerCart.ConsumerOrderPickupType.Delivery);
                        }
                    }
                }

                consumerCart.setCart(prepareCartHelper(consumerCart, true));
                consumerCart.setConsumerId(storeToken.getConsumerId());
                consumerCart.setDeliveryDate(wooCartRequest.getDeliveryDate());
                consumerCart.setDeliveryAddress(wooCartRequest.getDeliveryAddress());
                consumerCartRepository.update(storeToken.getCompanyId(), consumerCart.getId(), consumerCart);

                if (wooCartRequest.getPlaceOrder())
                    return submitCart(consumerCart.getId(), consumerCart, ConsumerCart.TransactionSource.WooCommerce);
                else
                    return updateCart(consumerCart.getId(), consumerCart, ConsumerCart.TransactionSource.WooCommerce);

            } else {
                throw new BlazeInvalidArgException("Cart", "This cart does not belong to this session or already completed");
            }

        } else {
            throw new BlazeAuthException("Authentication", "Request is not authenticated");
        }
    }

    @Override
    public SearchResult<ConsumerCart> getLastOrders(int start, int limit) {
        if (limit > 200) {
            limit = 200;
        }
        if (start < 0) {
            start = 0;
        }

        SearchResult<ConsumerCart> orders = consumerCartRepository.getAllOrders(storeToken.getCompanyId(), storeToken.getShopId(), storeToken.getConsumerId(), start, limit);

        List<ObjectId> transIds = new ArrayList<>();
        for (ConsumerCart consumerCart : orders.getValues()) {
            if (StringUtils.isNotBlank(consumerCart.getTransactionId())) {
                transIds.add(new ObjectId(consumerCart.getTransactionId()));
            }
        }
        HashMap<String, Transaction> transactions = transactionRepository.findItemsInAsMap(storeToken.getCompanyId(), transIds);
        List<ObjectId> employeeIds = new ArrayList<>();
        List<ObjectId> productIds = new ArrayList<>();
        for (Transaction transaction : transactions.values()) {
            employeeIds.add(new ObjectId(transaction.getAssignedEmployeeId()));
        }

        for (ConsumerCart consumerCart : orders.getValues()) {
            if (consumerCart.getCart() != null) {
                for (OrderItem orderItem : consumerCart.getCart().getItems()) {
                    if (StringUtils.isNotBlank(orderItem.getProductId())) {
                        productIds.add(new ObjectId(orderItem.getProductId()));
                    }
                }
            }
        }

        HashMap<String, Employee> employeeHashMap = employeeRepository.findItemsInAsMap(storeToken.getCompanyId(), employeeIds);
        HashMap<String, Product> productHashMap = productRepository.findProductsByIdsAsMap(storeToken.getCompanyId(), storeToken.getShopId(), productIds);
        HashMap<String, ProductCategory> productCategoryHashMap = categoryRepository.listAllAsMap(storeToken.getCompanyId(), storeToken.getShopId());

        for (ConsumerCart consumerCart : orders.getValues()) {
            if (StringUtils.isNotBlank(consumerCart.getTransactionId())) {
                Transaction transaction = transactions.get(consumerCart.getTransactionId());
                if (transaction != null) {
                    Employee employee = employeeHashMap.get(transaction.getAssignedEmployeeId());
                    consumerCart.setEmployeeName(employee.getFirstName());
                }
            }
            if (consumerCart.getCart() != null) {
                for (OrderItem orderItem : consumerCart.getCart().getItems()) {
                    orderItem.setProduct(productHashMap.get(orderItem.getProductId()));
                    if (orderItem.getProduct() != null) {
                        orderItem.getProduct().setCategory(productCategoryHashMap.get(orderItem.getProduct().getCategoryId()));
                    }
                }
            }
        }

        return orders;
    }

    @Override
    public ConsumerCartResult getCartByPublicKey(String publicKey) {
        ConsumerCartResult consumerCart = consumerCartRepository.getConsumerCart(publicKey);

        if (consumerCart == null) {
            throw new BlazeInvalidArgException("ConsumerCart", "Consumer cart does not exist");
        }

        ConsumerUser consumerUser = consumerUserRepository.getById(consumerCart.getConsumerId(), "{firstName:1,lastName:1,address:1}");

        Transaction transaction = transactionRepository.get(consumerCart.getCompanyId(), consumerCart.getTransactionId());
        Shop shop = shopRepository.get(consumerCart.getCompanyId(), consumerCart.getShopId());
        consumerCart.setShop(shop);
        consumerCart.setTransaction(transaction);
        consumerCart.setConsumerUser(consumerUser);

        // set result
        if (consumerCart.getCart() != null && consumerCart.getCart().getItems().size() > 0) {
            List<ObjectId> objectIds = new ArrayList<>();
            for (OrderItem orderItem : consumerCart.getCart().getItems()) {
                if (orderItem.getProductId() != null) {
                    objectIds.add(new ObjectId(orderItem.getProductId()));
                }
            }

            HashMap<String, Product> productHashMap = productRepository.findProductsByIdsAsMap(consumerCart.getCompanyId(), consumerCart.getShopId(), objectIds);
            HashMap<String, ProductCategory> productCategoryHashMap = categoryRepository.listAllAsMap(consumerCart.getCompanyId(), consumerCart.getShopId());

            for (OrderItem orderItem : consumerCart.getCart().getItems()) {
                orderItem.setProduct(productHashMap.get(orderItem.getProductId()));
                if (orderItem.getProduct() != null) {
                    orderItem.getProduct().setCategory(productCategoryHashMap.get(orderItem.getProduct().getCategoryId()));
                }
            }
        }

        return consumerCart;
    }

    private Cart prepareCartHelper(ConsumerCart cart, boolean newCart) {
        if (cart == null) {
            throw new BlazeInvalidArgException("ConsumerCart", "ConsumerCart does not exist.");
        }
        Shop shop = shopRepository.get(storeToken.getCompanyId(), storeToken.getShopId());
        if (shop == null) {
            throw new BlazeInvalidArgException("Shop", "Invalid shop.");
        }

        Transaction transaction = new Transaction();
        transaction.setCart(cart.getCart());
        Transaction.QueueType queueType = cart.getPickupType() == ConsumerCart.ConsumerOrderPickupType.Delivery ? Transaction.QueueType.Delivery : Transaction.QueueType.WalkIn;
        transaction.setQueueType(queueType);

        ConsumerUser consumerUser = consumerUserRepository.getById(storeToken.getConsumerId());

        Member member = null;
        if (consumerUser != null) {
            member = memberRepository.get(storeToken.getCompanyId(), consumerUser.getMemberId());
            cart.setDeliveryAddress(cart.getDeliveryAddress() == null ? consumerUser.getAddress() : cart.getDeliveryAddress());
        } else if (StringUtils.isNotBlank(cart.getMemberId())) {
            ConsumerCart dbCart = consumerCartRepository.get(storeToken.getCompanyId(),cart.getId());
            if (dbCart != null && StringUtils.isBlank(dbCart.getConsumerId())) {
                // db consumer user did not exist
                member = memberRepository.get(storeToken.getCompanyId(), cart.getMemberId());
                // consumer doesn't exist
                if (member != null) {
                    // create consumer
                    if (StringUtils.isNotBlank(member.getConsumerUserId())) {
                        consumerUser = consumerUserRepository.getById(member.getConsumerUserId());
                    }
                    if (consumerUser == null) {
                        // create a corresponding consumer
                        consumerUser = consumerUserService.createConsumerUserFromMember(member);
                        memberRepository.updateConsumerUserId(storeToken.getCompanyId(),consumerUser.getId(),member.getId());
                    }

                    if (consumerUser != null) {
                        cart.setConsumerId(consumerUser.getId());
                    }
                }
            }
        }

        if (member != null) {
            cart.setMemberId(member.getId());
            transaction.setMemberId(member.getId());
            cart.setDeliveryAddress(cart.getDeliveryAddress() == null ? member.getAddress() : cart.getDeliveryAddress());

            if (!cart.getCart().getPromotionReqs().isEmpty()) {
                LinkedHashSet<PromotionReq> updatedPromotionReqSet = new LinkedHashSet<>();
                List<ObjectId> rewardIds = new ArrayList<>();
                for (PromotionReq promotionReq : cart.getCart().getPromotionReqs()) {
                    if (promotionReq.getRewardId() != null && ObjectId.isValid(promotionReq.getRewardId()))
                        rewardIds.add(new ObjectId(promotionReq.getRewardId()));
                }
                Iterable<LoyaltyReward> loyaltyReward = rewardRepository.findItemsIn(storeToken.getCompanyId(), storeToken.getShopId(), rewardIds);
                HashMap<String, String> errorMap = cart.getRewardErrorMap();
                ArrayList<String> rewardName = new ArrayList<>();
                for (LoyaltyReward reward : loyaltyReward) {
                    if (reward != null) {
                        if (NumberUtils.round(reward.getPoints(), 1) > NumberUtils.round(member.getLoyaltyPoints().doubleValue(), 1)) {
                                if (errorMap != null) {
                                    errorMap.put(reward.getName(),String.format("Member does not have enough points for reward %s.",  reward.getName()));
                                } else {
                                    cart.setErrorMsg("Member does not have enough points");
                                }
                            } else {
                                rewardName.add(reward.getName());
                                updatedPromotionReqSet.add(new PromotionReq(null, reward.getId(),reward.getFinalizedDiscountId()));
                                cart.setErrorMsg("");
                            }
                    }
                }
                StringBuilder finalRewardName = new StringBuilder("");
                for (int i = 0; i < rewardName.size(); i++) {
                    if (StringUtils.isNotBlank(finalRewardName))
                        finalRewardName.append(", ");
                    finalRewardName.append(rewardName.get(i));
                }
                cart.setRewardName(finalRewardName.toString());
                cart.setRewardErrorMap(errorMap);
                cart.getCart().getPromotionReqs().clear();
                cart.getCart().getPromotionReqs().addAll(updatedPromotionReqSet);
            }
        }

        if (member == null && !cart.getCart().getPromotionReqs().isEmpty()) {
            cart.setErrorMsg("Rewards can only be applied to registered members only.");
        }

        transaction.setDeliveryAddress(cart.getDeliveryAddress());

        final CustomerInfo customerInfo = getMemberGroup(member, consumerUser, shop);
        // Clear promotions request
        resetAllDiscounts(cart.getCart());

        cartService.prepareCart(shop, transaction, false, Transaction.TransactionStatus.InProgress, true, customerInfo,  newCart);

        if (cart.getCart() != null && cart.getCart().getItems() != null && shop.getOnlineStoreInfo() != null && shop.getOnlineStoreInfo().isEnableInventory()) {
            this.checkAvailableInventory(shop.getOnlineStoreInfo().getActiveInventoryId(), cart.getCart(), shop.getOnlineStoreInfo().getEnableInventoryType());
        }

        if (newCart && customerInfo != null && customerInfo.getMemberGroup() != null) {
            MemberGroup memberGroup = customerInfo.getMemberGroup();
            if (memberGroup != null && !memberGroup.isDeleted() && memberGroup.isActive()) {
                if (memberGroup.isEnablePromotion() && StringUtils.isNotBlank(memberGroup.getPromotionId())) {
                    PromotionReqLog memberGroupPromotion = transaction.getCart().getPromotionReqLogs().stream().filter(promotionReqLog -> memberGroup.getPromotionId().equalsIgnoreCase(promotionReqLog.getPromotionId())).findFirst().orElse(null);
                    if (memberGroupPromotion != null) {
                        cart.setMemberGroup(new WooComCartResult.WooMemberGroupResult(memberGroup.getName(), memberGroupPromotion.getAmount(), OrderItem.DiscountType.Cash));
                    }
                } else if (!memberGroup.isEnablePromotion()) {
                    cart.setMemberGroup(new WooComCartResult.WooMemberGroupResult(memberGroup.getName(), memberGroup.getDiscount(), memberGroup.getDiscountType()));
                }
            }
        }
        return transaction.getCart();
    }

    private void checkAvailableInventory(String inventoryId, Cart cart, OnlineStoreInfo.EnableInventoryType type) {
        List<ObjectId> productIds = new ArrayList<>();
        List<String> strProductIds = new ArrayList<>();
        for (OrderItem orderItem : cart.getItems()) {
            productIds.add(new ObjectId(orderItem.getProductId()));
            strProductIds.add(orderItem.getProductId());
        }

        HashMap<String, Product> productMap = productRepository.listAsMap(storeToken.getCompanyId(), productIds);
        HashMap<String, ProductPrepackageQuantity> prepackageQuantityMap = prepackageQuantityRepository.getQuantitiesForProductsAsMap(storeToken.getCompanyId(), storeToken.getShopId(), strProductIds);

        Product productItem = null;
        for (OrderItem orderItem : cart.getItems()) {
            productItem = productMap.get(orderItem.getProductId());

            BigDecimal quantity = null;
            if (StringUtils.isNotBlank(orderItem.getPrepackageItemId())) {
                ProductPrepackageQuantity prepackageQuantity = prepackageQuantityMap.get(orderItem.getPrepackageItemId());

                if (prepackageQuantity == null || prepackageQuantity.getQuantity() < orderItem.getQuantity().intValue()) {
                    throw new BlazeInvalidArgException(PRODUCT_QUANTITY, String.format(QUANTITY_NOT_AVAILABLE, productItem.getName()));
                }

            } else {

                if (type.equals(OnlineStoreInfo.EnableInventoryType.All)) {
                    for (ProductQuantity productQuantity : productItem.getQuantities()) {
                        if (quantity == null) {
                            quantity = new BigDecimal(0);
                        }
                        quantity = quantity.add(productQuantity.getQuantity());
                    }
                } else if (StringUtils.isNotBlank(inventoryId)) {

                    for (ProductQuantity productQuantity : productItem.getQuantities()) {
                        if (productQuantity.getInventoryId().equalsIgnoreCase(inventoryId)) {
                            quantity = productQuantity.getQuantity();
                            break;
                        }
                    }
                }

                if (quantity == null || quantity.doubleValue() == 0.0 || quantity.doubleValue() < orderItem.getQuantity().doubleValue()) {
                    throw new BlazeInvalidArgException(PRODUCT_QUANTITY, String.format(QUANTITY_NOT_AVAILABLE, productItem.getName()));
                }
            }
        }

    }

    private void resetAllDiscounts(Cart cart) {
        if (cart != null) {
            cart.setDiscount(new BigDecimal(0));
            cart.setDiscountType(OrderItem.DiscountType.Cash);

            Iterable<ProductWeightTolerance> tolerances = weightToleranceRepository.list(storeToken.getCompanyId());
            HashMap<String, ProductWeightTolerance> weightToleranceHashMap = new HashMap<>();
            for (ProductWeightTolerance tolerance : tolerances) {
                weightToleranceHashMap.put(tolerance.getWeightKey().name(), tolerance);
            }

            boolean isProduct = false;
            PromotionReqLog reqLog = null;
            if (cart.getPromotionReqLogs() != null) {
                for (PromotionReqLog rl : cart.getPromotionReqLogs()) {
                    if (rl.getPromotionType() == Promotion.PromotionType.Product) {
                        isProduct = true;
                        reqLog = rl;
                        break;
                    }
                }
            }

            boolean found = false;
            for (OrderItem orderItem : cart.getItems()) {
                orderItem.setDiscount(new BigDecimal(0));
                orderItem.setDiscountType(OrderItem.DiscountType.Cash);

                if (orderItem.isUseUnitQty()) {
                    double qtyValue = orderItem.getUnitQty();

                    if (orderItem.getWeightKey() != null) {
                        ProductWeightTolerance tolerance = weightToleranceHashMap.get(orderItem.getWeightKey().name());
                        if (tolerance != null) {
                            qtyValue = tolerance.getUnitValue().doubleValue() * qtyValue;
                            qtyValue = NumberUtils.round(qtyValue, 2);
                        }
                    }
                    orderItem.setQuantity(new BigDecimal(qtyValue));
                }


                // must at least find 1
                if (isProduct && orderItem.getPromotionReqs() != null && found == false) {
                    for (PromotionReq orderLog : orderItem.getPromotionReqs()) {
                        if (StringUtils.isNotBlank(orderLog.getPromotionId()) && reqLog.getPromotionId().equalsIgnoreCase(orderLog.getPromotionId())) {
                            found = true;
                        } else if (StringUtils.isNotBlank(orderLog.getRewardId()) && reqLog.getRewardId().equalsIgnoreCase(orderLog.getRewardId())) {
                            found = true;
                        }
                    }
                }
            }

            if (isProduct && found == false) {
                // must have previous promo logs, eg, it was previously applied
                if (cart.getPromotionReqLogs() != null && cart.getPromotionReqLogs().size() > 0) {
                    cart.getPromotionReqLogs().clear();
                    cart.getPromotionReqs().clear();
                    cart.setPromoCode(Strings.EMPTY); // clear promoCode
                }
            }

            // clear promo requests if items are empty
            if (cart.getItems().size() == 0 && cart.getPromotionReqLogs() != null) {
                cart.getPromotionReqLogs().clear();
                cart.getPromotionReqs().clear();
                cart.setPromoCode(Strings.EMPTY); // clear promoCode
            }
            //cart.setPromotionReqs(new LinkedHashSet<PromotionReq>());

            // Remove if rewardId is not specified. Leave promos with rewardIds alone
            cart.getPromotionReqs().removeIf((req) -> StringUtils.isBlank(req.getRewardId()));
        }
    }

    private CustomerInfo getMemberGroup(Member member, ConsumerUser consumerUser, Shop shop) {
        CustomerInfo customerInfo = new CustomerInfo();
        if (member != null) {
            MemberGroup group = memberGroupRepository.get(storeToken.getCompanyId(), member.getMemberGroupId());
            customerInfo.setMemberGroup(group);
            customerInfo.setConsumerType(member.getConsumerType());
            customerInfo.setPatient(member);

        } else if (consumerUser != null) {
            customerInfo.setConsumerType(consumerUser.getConsumerType());
            customerInfo.setPatient(consumerUser);
            if (consumerUser.getAddress() != null) {
                customerInfo.setZipCode(consumerUser.getAddress().getZipCode());
            }
        }

        if (shop != null) {
            if (shop.getShopType() == Shop.ShopType.Recreational) {
                customerInfo.setConsumerType(ConsumerType.AdultUse);
            } else if (shop.getShopType() == Shop.ShopType.Medicinal) {
                if (customerInfo.getConsumerType() == ConsumerType.AdultUse) {
                    customerInfo.setConsumerType(ConsumerType.MedicinalThirdParty); // force medicinal third party
                }
            }
        }
        LOGGER.info("CustomerInfo: " + customerInfo);
        return customerInfo;
    }

    @Override
    public DateSearchResult<ConsumerCart> getConsumerCartByDate(long afterDate, long beforeDate) {
        return consumerCartRepository.findItemsWithDateNonDeleted(storeToken.getCompanyId(), storeToken.getShopId(), afterDate, beforeDate);
    }

    public ConsumerCartResult finalizeItemCart(String transactionId, String orderItemId, ConsumerCartResult incomingCart) {
        if (incomingCart == null) {
            throw new BlazeInvalidArgException("Cart", "Cart does not exist");
        }

        Transaction trans = transactionRepository.get(incomingCart.getCompanyId(), transactionId);
        if (trans == null) {
            throw new BlazeInvalidArgException("Transaction", "Transaction does not exist for id " + transactionId);
        }

        trans.setCart(incomingCart.getCart());
        Shop shop = shopRepository.get(incomingCart.getCompanyId(), incomingCart.getShopId());

        cartService.finalizeOrderItem(shop, trans, orderItemId);

        return incomingCart;
    }

    public ConsumerCartResult unfinalizeItemCart(String transactionId, String orderItemId, ConsumerCartResult incomingCart) {
        if (incomingCart == null) {
            throw new BlazeInvalidArgException("Cart", "Cart does not exist");
        }

        Transaction trans = transactionRepository.get(incomingCart.getCompanyId(), transactionId);
        if (trans == null) {
            throw new BlazeInvalidArgException("Transaction", "Transaction does not exist for id " + transactionId);
        }

        trans.setCart(incomingCart.getCart());

        cartService.unfinalizeOrderItem(trans, orderItemId);

        return incomingCart;
    }

    public ConsumerCartResult finalizeAllOrderItems(String transactionId, ConsumerCartResult incomingCart) {
        if (incomingCart == null) {
            throw new BlazeInvalidArgException("Cart", "Cart does not exist");
        }

        Transaction trans = transactionRepository.get(incomingCart.getCompanyId(), transactionId);
        if (trans == null) {
            throw new BlazeInvalidArgException("Transaction", "Transaction does not exist for id " + transactionId);
        }

        trans.setCart(incomingCart.getCart());
        Shop shop = shopRepository.get(incomingCart.getCompanyId(), incomingCart.getShopId());

        cartService.finalizeAllOrderItems(shop, trans);

        return incomingCart;
    }

    public ConsumerCart finalizeItemCart(String orderItemId, ConsumerCart incomingCart) {
        if (incomingCart == null) {
            incomingCart = createNewEmptyCart();
        }

        Cart cart = prepareCartHelper(incomingCart);
        Transaction transaction = new Transaction();
        transaction.setCart(cart);
        Shop shop = shopRepository.get(storeToken.getCompanyId(), storeToken.getShopId());

        cartService.finalizeOrderItem(shop, transaction, orderItemId);
        incomingCart.setCart(cart);

        return incomingCart;
    }

    public ConsumerCart unfinalizeItemCart(String orderItemId, ConsumerCart incomingCart) {
        if (incomingCart == null) {
            incomingCart = createNewEmptyCart();
        }

        Cart cart = prepareCartHelper(incomingCart);
        Transaction transaction = new Transaction();
        transaction.setCart(cart);

        cartService.unfinalizeOrderItem(transaction, orderItemId);
        incomingCart.setCart(cart);

        return incomingCart;
    }

    public ConsumerCart finalizeAllOrderItems(ConsumerCart incomingCart) {
        if (incomingCart == null) {
            incomingCart = createNewEmptyCart();
        }

        Cart cart = prepareCartHelper(incomingCart);
        Transaction transaction = new Transaction();
        transaction.setCart(cart);
        Shop shop = shopRepository.get(storeToken.getCompanyId(), storeToken.getShopId());

        cartService.finalizeAllOrderItems(shop, transaction);
        incomingCart.setCart(cart);

        return incomingCart;
    }

    /**
     * This method is used to set SlugList and HashMap<name,null> for given String array of names.
     * @param rewardNames
     * @param errorMap
     * @param slugList
     */
    private void setSlugListandHashMap(String [] rewardNames,HashMap<String, String> errorMap,List<String> slugList) {
        errorMap.clear();
        for (String key : rewardNames)
            errorMap.put(key, null);
        for (String rewardName : rewardNames)
            slugList.add((new Slugify()).slugify(rewardName.trim()));
        return;
    }
}
