package com.fourtwenty.core.services.store.impl;

import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.domain.models.company.*;
import com.fourtwenty.core.domain.models.consumer.ConsumerAsset;
import com.fourtwenty.core.domain.models.consumer.ConsumerUser;
import com.fourtwenty.core.domain.models.customer.Doctor;
import com.fourtwenty.core.domain.models.customer.Identification;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.customer.Recommendation;
import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.models.generic.Asset;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.domain.repositories.store.ConsumerCartRepository;
import com.fourtwenty.core.domain.repositories.store.ConsumerUserRepository;
import com.fourtwenty.core.event.BlazeEventBus;
import com.fourtwenty.core.event.member.MemberEmailAndDobUpdateEvent;
import com.fourtwenty.core.exceptions.BlazeAuthException;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.managed.AmazonServiceManager;
import com.fourtwenty.core.managed.ElasticSearchManager;
import com.fourtwenty.core.managed.PartnerWebHookManager;
import com.fourtwenty.core.rest.consumer.requests.ConsumerCreateRequest;
import com.fourtwenty.core.rest.consumer.requests.ConsumerLoginRequest;
import com.fourtwenty.core.rest.consumer.results.ConsumerAuthResult;
import com.fourtwenty.core.rest.dispensary.requests.consumeruser.request.ConsumerUserAddRequest;
import com.fourtwenty.core.rest.dispensary.requests.consumeruser.response.ConsumerUserImportResult;
import com.fourtwenty.core.rest.dispensary.results.ConsumerUserResult;
import com.fourtwenty.core.rest.dispensary.results.common.AssetStreamResult;
import com.fourtwenty.core.rest.store.requests.ContractSignRequest;
import com.fourtwenty.core.rest.store.webhooks.ConsumerUserData;
import com.fourtwenty.core.security.tokens.ConsumerAssetAccessToken;
import com.fourtwenty.core.security.tokens.StoreAuthToken;
import com.fourtwenty.core.services.AbstractStoreServiceImpl;
import com.fourtwenty.core.services.common.RealtimeService;
import com.fourtwenty.core.services.store.ConsumerAuthService;
import com.fourtwenty.core.services.store.ConsumerUserService;
import com.fourtwenty.core.services.thirdparty.AmazonS3Service;
import com.fourtwenty.core.util.SecurityUtil;
import com.google.common.collect.Lists;
import com.google.inject.Provider;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * Created by mdo on 5/17/17.
 */
public class ConsumerUserServiceImpl extends AbstractStoreServiceImpl implements ConsumerUserService {
    private static final Log LOG = LogFactory.getLog(ConsumerUserServiceImpl.class);
    private static final String CONSUMER_USER = "Consumer User";
    private static final String CONSUMER_USER_NOT_FOUND = "Consumer user not found";

    private static final String PASSWORD_RESET = "Password Reset";
    private static final String CONSUMER_NOT_BLANK = "Consumer cannot be blank.";
    private static final String CONSUMER_NOT_FOUND = "Consumer not found.";
    private static final String CONSUMER = "Consumer User";
    private static final String INVALID_CREDENTIALS = "Invalid user credentials.";
    private static final String ACCOUNT_EXIST = "Account already exists.";
    private static final String EMAIL_IN_USE = "User with this email already exists.";

    @Inject
    ConnectConfiguration connectConfiguration;

    @Inject
    ConsumerUserRepository consumerUserRepository;
    @Inject
    SecurityUtil securityUtil;
    @Inject
    AmazonS3Service amazonS3Service;
    @Inject
    ContractRepository contractRepository;
    @Inject
    CompanyAssetRepository companyAssetRepository;
    @Inject
    MemberRepository memberRepository;
    @Inject
    DoctorRepository doctorRepository;
    @Inject
    ShopRepository shopRepository;
    @Inject
    private PartnerWebHookManager partnerWebHookManager;
    @Inject
    private BlazeEventBus blazeEventBus;
    @Inject
    private ConsumerAuthService consumerAuthService;
    @Inject
    private MemberGroupRepository memberGroupRepository;
    @Inject
    private ElasticSearchManager elasticSearchManager;
    @Inject
    private RealtimeService realtimeService;
    @Inject
    private ConsumerCartRepository consumerCartRepository;
    @Inject
    private TransactionRepository transactionRepository;

    @Inject
    private BlazeEventBus eventBus;


    @Inject
    public ConsumerUserServiceImpl(Provider<StoreAuthToken> tokenProvider) {
        super(tokenProvider);
    }

    private static File stream2file(InputStream in, String extension) throws IOException {
        final File tempFile = File.createTempFile(UUID.randomUUID().toString(), extension);
        tempFile.deleteOnExit();
        try (FileOutputStream out = new FileOutputStream(tempFile)) {
            IOUtils.copy(in, out);
        }
        return tempFile;
    }

    @Override
    public ConsumerUser findUser(String email, String phoneNumber, String country) {
        ConsumerUser consumerUser = null;
        if (StringUtils.isNotBlank(email)) {
            consumerUser = consumerUserRepository.getConsumerByEmail(storeToken.getCompanyId(), email.toLowerCase().trim(), ConsumerUser.class);
        } else if (StringUtils.isNotBlank(phoneNumber)) {
            String cpn = AmazonServiceManager.cleanPhoneNumberCountry(phoneNumber, country);
            consumerUser = consumerUserRepository.getConsumerByCPN(storeToken.getCompanyId(), cpn); // cpn is Phone Number
        }

        if (consumerUser == null && StringUtils.isNotBlank(email)) {
            Member member = memberRepository.getMemberByEmailAddress(storeToken.getCompanyId(), email.toLowerCase().trim());
            if (member != null) {
                consumerUser = consumerAuthService.createConsumerUserFromMember(member);
            }
        }

        if (consumerUser == null) {
            throw new BlazeAuthException("Authentication", "User does not exist.");
        }
        consumerUser.setPassword(null);
        return consumerUser;
    }

    @Override
    public ConsumerUser getUserById(String consumerUserId) {
        ConsumerUser consumerUser = consumerUserRepository.getById(consumerUserId);
        if (consumerUser == null) {
            throw new BlazeAuthException("Authentication", "User does not exist.");
        }
        consumerUser.setPassword(null);
        return consumerUser;
    }

    @Override
    public ConsumerUser getCurrentUser() {
        ConsumerUser consumerUser = consumerUserRepository.getById(storeToken.getConsumerId());
        if (consumerUser == null) {
            throw new BlazeAuthException("Authentication", "User is not valid.");
        }
        consumerUser.setPassword(null);
        return consumerUser;
    }

    @Override
    public ConsumerAuthResult login(ConsumerLoginRequest request) {
        ConsumerUser consumerUser = consumerUserRepository.getConsumerByEmail(storeToken.getCompanyId(), request.getEmail().toLowerCase().trim(), ConsumerUser.class);

        if (consumerUser == null) {
            this.checkMemberExistenceByEmail(request.getEmail());
        }

        if (consumerUser == null) {
            throw new BlazeAuthException("Authentication", INVALID_CREDENTIALS);
        }

        if (!securityUtil.checkPassword(request.getPassword(), consumerUser.getPassword())) {
            throw new BlazeAuthException("Authentication", "Invalid user credentials.");
        }


        consumerUser.setPassword(null);
        ConsumerAuthResult authResult = new ConsumerAuthResult();
        StoreAuthToken storeAuthToken = createNewToken(
                consumerUser.getId());

        ConsumerAssetAccessToken assetAccessToken = createConsumerAssetToken(consumerUser.getId());

        authResult.setAccessToken(getNewEncryptedToken(storeAuthToken));
        authResult.setAssetAccessToken(getNewEncryptedAssetToken(assetAccessToken));
        authResult.setUser(consumerUser);
        authResult.setLoginTime(storeAuthToken.getLoginDate());
        authResult.setExpirationTime(storeAuthToken.getExpirationDate());
        authResult.setSessionId(UUID.randomUUID().toString());
        return authResult;
    }

    @Override
    public ConsumerUser createNewUser(ConsumerCreateRequest request) {
        ConsumerUser consumerUser = consumerUserRepository.getConsumerByEmail(storeToken.getCompanyId(), request.getEmail().toLowerCase().trim(), ConsumerUser.class);

        if (consumerUser == null) {
            ConsumerUser user = consumerAuthService.checkMemberExistenceByEmail(request.getEmail().toLowerCase().trim());

            if (user != null) {
                throw new BlazeInvalidArgException(CONSUMER_USER, ACCOUNT_EXIST);
            }
        }

        if (consumerUser != null) {
            throw new BlazeAuthException("Authentication", "Email is in use.");
        }

        Shop shop = shopRepository.get(storeToken.getCompanyId(), storeToken.getShopId());
        String cpn = request.getPhoneNumber();
        if (shop != null) {
            cpn = AmazonServiceManager.cleanPhoneNumber(request.getPhoneNumber(), shop);
        }

        consumerUser = new ConsumerUser();
        consumerUser.prepare(storeToken.getCompanyId());
        consumerUser.setId(ObjectId.get().toString());
        consumerUser.setEmail(request.getEmail().toLowerCase().trim());
        consumerUser.setFirstName(request.getFirstName());
        consumerUser.setLastName(request.getLastName());
        consumerUser.setPrimaryPhone(request.getPhoneNumber());
        consumerUser.setMarketingSource(request.getMarketingSource());
        consumerUser.setDob(request.getDob());
        consumerUser.setActive(true);
        consumerUser.setSourceCompanyId(storeToken.getCompanyId());
        consumerUser.setCpn(cpn);

        if (StringUtils.isBlank(request.getPassword())) {
            String encryptPassword = securityUtil.encryptPassword(securityUtil.getNextPassCode());
            consumerUser.setPassword(encryptPassword);
        } else {
            String encryptPassword = securityUtil.encryptPassword(request.getPassword());
            consumerUser.setPassword(encryptPassword);
        }

        if (request.getAgreement() != null) {
            ConsumerAsset asset = new ConsumerAsset();
            CompanyAsset companyAsset = request.getAgreement();
            asset.prepare();
            asset.setActive(companyAsset.isActive());
            asset.setName(companyAsset.getName());
            asset.setKey(companyAsset.getKey());
            asset.setType(companyAsset.getType());
            consumerUser.setDlPhoto(asset);
        }
        if (StringUtils.isNotBlank(request.getContractId())) {
            Contract contract = contractRepository.get(storeToken.getCompanyId(), request.getContractId());
            if (contract != null) {
                List<SignedContract> userSignedContract = new ArrayList<>();
                SignedContract signedContract = new SignedContract();
                signedContract.prepare(storeToken.getCompanyId());
                signedContract.setShopId(storeToken.getShopId());
                signedContract.setSignedDigitally(true);
                signedContract.setSignedDate(DateTime.now().getMillis());
                signedContract.setContractId(contract.getId());
                signedContract.setConsumerId(consumerUser.getId());
                userSignedContract.add(signedContract);
                consumerUser.setSignedContracts(userSignedContract);
            }
        }

        consumerUserRepository.save(consumerUser);

        consumerUser.setPassword(null);

        final ConsumerUserData consumerUserData = new ConsumerUserData();
        consumerUserData.setId(consumerUser.getId());
        consumerUserData.setCreated(consumerUser.getCreated());
        consumerUserData.setModified(consumerUser.getModified());
        consumerUserData.setConsumerEmail(consumerUser.getEmail());
        consumerUserData.setConsumerFirstName(consumerUser.getFirstName());
        consumerUserData.setConsumerLastName(consumerUser.getLastName());
        consumerUserData.setConsumerPhoneNumber(consumerUser.getPrimaryPhone());
        consumerUserData.setRegisterTime(consumerUser.getCreated());

        try {
            partnerWebHookManager.newConsumerUserHook(storeToken.getCompanyId(), storeToken.getShopId(), consumerUserData);
        } catch (Exception e) {
            LOG.warn("new consumer user webhook failed for shopId:" + storeToken.getShopId() + " companyId:" + storeToken.getCompanyId(), e);
        }

        return consumerUser;
    }

    @Override
    public ConsumerUser signContract(ContractSignRequest request) {
        ConsumerUser consumerUser = consumerUserRepository.getById(storeToken.getConsumerId());
        if (consumerUser == null) {
            throw new BlazeAuthException("Authentication", "User is not valid.");
        }

        // Find contract
        Contract contract = contractRepository.get(storeToken.getCompanyId(), request.getContractId());
        if (contract == null) {
            throw new BlazeInvalidArgException("Contract", "Could not find contract.");
        }
        // Check if this contract has already been sign
        boolean newContract = true;
        for (SignedContract signedContract : consumerUser.getSignedContracts()) {
            if (signedContract.getContractId().equalsIgnoreCase(contract.getId())) {
                newContract = false;
                break;
            }
        }

        if (newContract) {
            SignedContract signedContract = new SignedContract();
            signedContract.prepare(storeToken.getCompanyId());
            signedContract.setShopId(storeToken.getShopId());
            signedContract.setSignedDigitally(true);
            signedContract.setSignedDate(DateTime.now().getMillis());
            signedContract.setContractId(contract.getId());
            signedContract.setConsumerId(consumerUser.getId());
            consumerUser.getSignedContracts().add(signedContract);
            consumerUser.prepare(storeToken.getCompanyId());

            consumerUserRepository.update(consumerUser.getId(), consumerUser);
        }

        consumerUser.setPassword(null);
        return consumerUser;
    }

    @Override
    public ConsumerUser updateUser(ConsumerUser user, boolean updateEmail) {
        ConsumerUser dbConsumerUser = consumerUserRepository.getById(storeToken.getConsumerId());
        if (dbConsumerUser == null) {
            throw new BlazeAuthException("Authentication", "User is not valid.");
        }

        if (StringUtils.isNotBlank(user.getPassword())) {

            String encryptPassword = securityUtil.encryptPassword(user.getPassword());
            dbConsumerUser.setPassword(encryptPassword);
        }

        Shop shop = shopRepository.get(storeToken.getCompanyId(), storeToken.getShopId());
        String cpn = user.getPrimaryPhone();
        if (shop != null) {
            cpn = AmazonServiceManager.cleanPhoneNumber(user.getPrimaryPhone(), shop);
        }

        dbConsumerUser.prepare(storeToken.getCompanyId());
        dbConsumerUser.setFirstName(user.getFirstName());
        dbConsumerUser.setLastName(user.getLastName());
        dbConsumerUser.setCpn(cpn);
        //dbConsumerUser.setEmail(user.getEmail()); -- Disable changing email
        //Updated for BR-1477: Lifted and chilli's customer needs to update their email addresses so allowing update of email address
        if (updateEmail) {
            ConsumerUser dbConsumerUserByEmail = consumerUserRepository.getConsumerByEmail(storeToken.getCompanyId(), user.getEmail().toLowerCase().trim(), ConsumerUser.class);

            if (dbConsumerUserByEmail == null) {
                ConsumerUser consumerUserByEmail = this.checkMemberExistenceByEmail(user.getEmail());

                if (consumerUserByEmail != null) {
                    throw new BlazeInvalidArgException(CONSUMER, ACCOUNT_EXIST);
                }
            }

            if (dbConsumerUserByEmail != null) {
                throw new BlazeAuthException(CONSUMER, EMAIL_IN_USE);
            }

            dbConsumerUser.setEmail(user.getEmail());
        }
        dbConsumerUser.setPrimaryPhone(user.getPrimaryPhone());

        dbConsumerUser.setAddress(user.getAddress());
        dbConsumerUser.setDob(user.getDob());
        dbConsumerUser.setSex(user.getSex());
        dbConsumerUser.setNotificationType(user.getNotificationType());
        dbConsumerUser.setTextOptIn(user.isTextOptIn());
        dbConsumerUser.setEmailOptIn(user.isEmailOptIn());
        dbConsumerUser.setMarketingSource(user.getMarketingSource());

        if (dbConsumerUser.getAddress() != null) {
            dbConsumerUser.getAddress().prepare();
            if ((shop != null && shop.getAddress() != null) && StringUtils.isNotBlank(shop.getAddress().getCountry())) {
                dbConsumerUser.getAddress().setCountry(shop.getAddress().getCountry());
            }
        }

        dbConsumerUser.setDlNo(user.getDlNo());
        dbConsumerUser.setDlExpiration(user.getDlExpiration());
        dbConsumerUser.setDlState(user.getDlState());

        dbConsumerUser.setRecNo(user.getRecNo());
        dbConsumerUser.setRecIssueDate(user.getRecIssueDate());
        dbConsumerUser.setRecExpiration(user.getRecExpiration());
        dbConsumerUser.setVerificationPhone(user.getVerificationPhone());
        dbConsumerUser.setVerificationWebsite(user.getVerificationWebsite());

        // Doctor
        dbConsumerUser.setDoctorFirstName(user.getDoctorFirstName());
        dbConsumerUser.setDoctorLastName(user.getDoctorLastName());
        dbConsumerUser.setDoctorLicense(user.getDoctorLicense());


        if (user.getDlPhoto() != null) {
            dbConsumerUser.setDlPhoto(user.getDlPhoto());
            if (dbConsumerUser.getDlPhoto() != null) {
                dbConsumerUser.getDlPhoto().prepare();
            }
        }
        if (user.getRecPhoto() != null) {
            dbConsumerUser.setRecPhoto(user.getRecPhoto());
        }

        ConsumerType consumerType = null;
        if (dbConsumerUser.getRecPhoto() != null) {
            dbConsumerUser.getRecPhoto().prepare();
        }

        if (dbConsumerUser.getConsumerType() == ConsumerType.AdultUse
            && dbConsumerUser.getRecPhoto() != null && StringUtils.isNotBlank(dbConsumerUser.getRecNo())) {
            // force third party if it was adult use but has rec info
            consumerType = ConsumerType.MedicinalThirdParty;
            dbConsumerUser.setMedical(true);
        }

        if (user.getConsumerType() == null) {
            dbConsumerUser.setConsumerType(consumerType);
        } else {
            dbConsumerUser.setConsumerType(user.getConsumerType());
        }

        // only do this if we're updating info
        try {
            updateCorrespondingMembers(dbConsumerUser);
        } catch (Exception e) {
            LOG.error("Error updating member: " + e.getMessage(), e);
        }

        boolean isFound = false;
        if (user != null && user.getAddress() != null) {
            if (dbConsumerUser.getAddresses() != null && dbConsumerUser.getAddresses().size() > 0) {
                for (Address address : dbConsumerUser.getAddresses()) {
                    if ((address.getAddressString().equalsIgnoreCase(user.getAddress().getAddressString()))) {
                        isFound = true;
                    }
                }
            }
            if (!isFound) {
                dbConsumerUser.getAddresses().add(user.getAddress());
            }
        }
        consumerUserRepository.update(dbConsumerUser.getId(), dbConsumerUser);

        dbConsumerUser.setPassword(null);
        return dbConsumerUser;
    }

    @Override
    public void updateCorrespondingMembers(ConsumerUser dbConsumerUser) {
        LOG.info("Updating member info member id : " + dbConsumerUser.getMemberId());


        Member member = null;
        if (StringUtils.isNotBlank(dbConsumerUser.getMemberId())) {
            member = memberRepository.get(storeToken.getCompanyId(), dbConsumerUser.getMemberId());
        }

        if (member == null) {
            return;
        }

            /*List<ObjectId> memberIds = new ArrayList<>();
            for (ConsumerMemberStatus memberStatus : dbConsumerUser.getMemberStatuses()) {
                if (memberStatus.isAccepted()) {
                    if (StringUtils.isNotBlank(memberStatus.getMemberId()) && ObjectId.isValid(memberStatus.getMemberId())) {
                        memberIds.add(new ObjectId(memberStatus.getMemberId()));
                    }
                }
            }*/

//            LOG.info("Found members: " + memberIds.size());
//            Iterable<Member> members = memberRepository.findItemsIn(memberIds);
//            for (Member member : members) {

        LOG.info("Member updating...: " + member.getId());

        String companyId = member.getCompanyId();
        Address address = member.getAddress();
        if (address == null) {
            member.setAddress(address);
        }
        if (dbConsumerUser.getAddress() != null) {
            address.setAddress(dbConsumerUser.getAddress().getAddress());
            address.setCity(dbConsumerUser.getAddress().getCity());
            address.setAddressLine2(dbConsumerUser.getAddress().getAddressLine2());
            address.setState(dbConsumerUser.getAddress().getState());
            address.setCountry(dbConsumerUser.getAddress().getCountry());
            address.setZipCode(dbConsumerUser.getAddress().getZipCode());
        }

        Identification identification = null;

        if (member.getIdentifications().size() == 0) {
            identification = new Identification();
            identification.prepare(storeToken.getCompanyId());
            member.setIdentifications(new ArrayList<Identification>());
            member.getIdentifications().add(identification);
        } else {
            // Find matching drivers license or get the first one
            for (Identification id : member.getIdentifications()) {
                if (StringUtils.isNotBlank(id.getLicenseNumber()) && StringUtils.isNotBlank(dbConsumerUser.getDlNo())
                        && id.getLicenseNumber().equalsIgnoreCase(dbConsumerUser.getDlNo())) {
                    identification = id;
                    break;
                }
            }
            // if no match, just use the first one
            if (identification == null) {
                identification = member.getIdentifications().get(0);
            }
        }

        identification.setCompanyId(companyId);
        identification.setExpirationDate(dbConsumerUser.getDlExpiration());
        identification.setState(dbConsumerUser.getDlState());
        identification.setLicenseNumber(dbConsumerUser.getDlNo());
        if (dbConsumerUser.getDlPhoto() != null) {
            CompanyAsset dlPhoto = companyAssetRepository.getAssetByKey(companyId, dbConsumerUser.getDlPhoto().getKey());
            if (dlPhoto == null) {
                dlPhoto = dbConsumerUser.getDlPhoto().toCompanyAsset(companyId);
                companyAssetRepository.save(dlPhoto);

                identification.setFrontPhoto(dlPhoto);

                List<CompanyAsset> companyAssets = new ArrayList<>();

                companyAssets.add(dlPhoto);
                identification.setAssets(companyAssets);
            } else {
                identification.setFrontPhoto(dlPhoto);
                List<CompanyAsset> companyAssets = new ArrayList<>();
                companyAssets.add(dlPhoto);
                identification.setAssets(companyAssets);
            }
        }
        Recommendation recommendation = null;
        if (member.getRecommendations().size() > 0) {
            // Recommendation
            for (Recommendation rec : member.getRecommendations()) {
                if (StringUtils.isNotBlank(rec.getRecommendationNumber()) && StringUtils.isNotBlank(dbConsumerUser.getRecNo())
                        && rec.getRecommendationNumber().equalsIgnoreCase(dbConsumerUser.getRecNo())) {
                    recommendation = rec;
                    break;
                }
            }
            if (recommendation == null) {
                recommendation = member.getRecommendations().get(0);
            }
        } else {
            recommendation = new Recommendation();
            recommendation.prepare(storeToken.getCompanyId());
            member.setRecommendations(new ArrayList<Recommendation>());
            member.getRecommendations().add(recommendation);
        }

        recommendation.setState(dbConsumerUser.getDlState());

        if (dbConsumerUser.getRecPhoto() != null) {

            CompanyAsset recPhoto = companyAssetRepository.getAssetByKey(companyId, dbConsumerUser.getRecPhoto().getKey());
            if (recPhoto == null) {
                recPhoto = dbConsumerUser.getRecPhoto().toCompanyAsset(companyId);
                companyAssetRepository.save(recPhoto);
                recommendation.setFrontPhoto(recPhoto);


                List<CompanyAsset> companyAssets = new ArrayList<>();

                companyAssets.add(recPhoto);
                recommendation.setAssets(companyAssets);
            } else {
                recommendation.setFrontPhoto(recPhoto);
                List<CompanyAsset> companyAssets = new ArrayList<>();

                companyAssets.add(recPhoto);
                recommendation.setAssets(companyAssets);
            }
        }
        recommendation.setRecommendationNumber(dbConsumerUser.getRecNo());
        recommendation.setExpirationDate(dbConsumerUser.getRecExpiration());
        recommendation.setIssueDate(dbConsumerUser.getRecIssueDate());
        recommendation.setVerifyPhoneNumber(dbConsumerUser.getVerificationPhone());
        recommendation.setVerifyWebsite(dbConsumerUser.getVerificationWebsite());


            // Doctor
            createOrUpdateDoctor(recommendation, dbConsumerUser);
        member.setTextOptIn(dbConsumerUser.isTextOptIn());
        member.setEmailOptIn(dbConsumerUser.isEmailOptIn());
        member.setFirstName(dbConsumerUser.getFirstName());
        member.setLastName(dbConsumerUser.getLastName());
        member.setMiddleName(dbConsumerUser.getMiddleName());
        member.setDob(dbConsumerUser.getDob());
        member.setPrimaryPhone(dbConsumerUser.getPrimaryPhone());
        member.setMedical(dbConsumerUser.isMedical());
        member.setConsumerType(dbConsumerUser.getConsumerType());
        member.setMarketingSource(dbConsumerUser.getMarketingSource());
        member.setSex(dbConsumerUser.getSex());

        boolean isFound = false;
        if (member.getAddresses() != null && member.getAddresses().size() > 0) {
            for (Address address1 : member.getAddresses()) {
                if (dbConsumerUser.getAddress().getAddressString().equals(address1.getAddressString())) {
                    isFound = true;
                }
            }
            if (!isFound) {
                memberRepository.updateMemberAddress(companyId, member.getId(), dbConsumerUser.getAddress());
            }
        }

        LOG.info("Member updated: " + member.getId());
        memberRepository.update(member.getCompanyId(), member.getId(), member);
    }

    @Override
    public ConsumerUserResult getConsumerByEmailId(String email) {
        if (StringUtils.isBlank(email)) {
            throw new BlazeInvalidArgException(CONSUMER_USER, CONSUMER_USER_NOT_FOUND);
        }
        ConsumerUserResult consumer = consumerUserRepository.getConsumerByEmail(storeToken.getCompanyId(), email.toLowerCase().trim(), ConsumerUserResult.class);

        Member member = null;
        if (consumer == null) {
            member = memberRepository.getMemberByEmailAddress(storeToken.getCompanyId(), email.toLowerCase());
            if (member != null) {
                consumerAuthService.createConsumerUserFromMember(member);
                consumer = consumerUserRepository.getConsumerByEmail(storeToken.getCompanyId(), email.toLowerCase().trim(), ConsumerUserResult.class);
            }
        } else {
            // String memberIdForShop = consumer.getMemberIdForShop(storeToken.getShopId());
            String memberIdForShop = consumer.getMemberId();
            member = memberRepository.get(storeToken.getCompanyId(), memberIdForShop);
        }

        if (consumer == null) {
            throw new BlazeInvalidArgException(CONSUMER_USER, CONSUMER_USER_NOT_FOUND);
        }

        consumer.setPassword(null);

        consumer.setMember(member);

        return consumer;
    }

    private Recommendation createOrUpdateDoctor(Recommendation recommendation, ConsumerUser consumerUser) {

        // Doctor
        if (StringUtils.isNotBlank(consumerUser.getDoctorLicense())) {
            Doctor doctor = doctorRepository.getDoctorByLicense(recommendation.getCompanyId(), consumerUser.getDoctorLicense());
            if (doctor != null) {
                recommendation.setDoctor(doctor);
                recommendation.setDoctorId(doctor.getId());
                consumerUser.setDoctorFirstName(doctor.getFirstName());
                consumerUser.setDoctorLastName(doctor.getLastName());
                if (StringUtils.isNotBlank(doctor.getWebsite())) {
                    consumerUser.setVerificationWebsite(doctor.getWebsite());
                }
                if (StringUtils.isNotBlank(doctor.getPhoneNumber())) {
                    consumerUser.setVerificationPhone(doctor.getPhoneNumber());
                }
            } else {
                // Create a new doctor
                doctor = new Doctor();
                doctor.setCompanyId(recommendation.getCompanyId());
                doctor.setFirstName(consumerUser.getDoctorFirstName());
                doctor.setLastName(consumerUser.getDoctorLastName());
                doctor.setLicense(consumerUser.getDoctorLicense());
                doctor.setActive(true);
                doctor.setWebsite(consumerUser.getVerificationWebsite());
                doctor.setPhoneNumber(consumerUser.getVerificationPhone());
                doctorRepository.save(doctor);
                recommendation.setDoctor(doctor);
                recommendation.setDoctorId(doctor.getId());
            }
        }
        return recommendation;
    }

    @Override
    public ConsumerUser updateDLPhoto(InputStream inputStream, String fileName) {
        ConsumerUser dbConsumerUser = consumerUserRepository.getById(storeToken.getConsumerId());
        if (dbConsumerUser == null) {
            throw new BlazeAuthException("Authentication", "User is not valid.");
        }

        if (inputStream == null || StringUtils.isBlank(fileName)) {
            throw new BlazeInvalidArgException("Form", "Invalid name or stream");
        }

        String ext2 = FilenameUtils.getExtension(fileName); // returns "exe"
        ext2 = "." + ext2;

        ConsumerAsset asset = new ConsumerAsset();
        if (ext2.equalsIgnoreCase(".pdf")) {
            asset.setType(Asset.AssetType.Document);
        } else {
            asset.setType(Asset.AssetType.Photo);
        }

        try {
            File file = stream2file(inputStream, ext2);
            String key = amazonS3Service.uploadFile(file, UUID.randomUUID().toString(), ext2);
            asset.prepare();
            asset.setName(fileName);
            asset.setKey(key);
            asset.setActive(true);

            dbConsumerUser.setDlPhoto(asset);

            consumerUserRepository.update(dbConsumerUser.getId(), dbConsumerUser);

            if (StringUtils.isNotBlank(dbConsumerUser.getMemberId())) {
                this.updateMemberDlPhoto(dbConsumerUser);
            }

            dbConsumerUser.setPassword(null);
        } catch (Exception e) {
            throw new BlazeInvalidArgException("Upload", "Cannot upload document.");
        }


        return dbConsumerUser;
    }

    private void updateMemberDlPhoto(ConsumerUser consumerUser) {
        Member member = memberRepository.get(storeToken.getCompanyId(), consumerUser.getMemberId());
        if (member == null) {
            return;
        }

        Identification identification = null;

        if (member.getIdentifications() == null || member.getIdentifications().size() == 0) {
            identification = new Identification();
            identification.prepare(storeToken.getCompanyId());
            identification.setLicenseNumber(consumerUser.getDlNo());
            identification.setState(consumerUser.getDlState());
            identification.setExpirationDate(consumerUser.getDlExpiration());

            member.setIdentifications(new ArrayList<Identification>());

            member.getIdentifications().add(identification);
        } else {
            // Find matching drivers license or get the first one
            for (Identification id : member.getIdentifications()) {
                if (StringUtils.isNotBlank(id.getLicenseNumber()) && StringUtils.isNotBlank(consumerUser.getDlNo())
                    && id.getLicenseNumber().equalsIgnoreCase(consumerUser.getDlNo())) {
                    identification = id;
                    break;
                }
            }
            // if no match, just use the first one
            if (identification == null) {
                identification = member.getIdentifications().get(0);
            }

            //
        }

        if (consumerUser.getDlPhoto() != null) {
            CompanyAsset dlPhoto = companyAssetRepository.getAssetByKey(member.getCompanyId(), consumerUser.getDlPhoto().getKey());
            if (dlPhoto == null) {
                dlPhoto = consumerUser.getDlPhoto().toCompanyAsset(member.getCompanyId());
                companyAssetRepository.save(dlPhoto);

                identification.setFrontPhoto(dlPhoto);

                List<CompanyAsset> companyAssets = identification.getAssets();
                if (companyAssets == null) {
                    companyAssets = new ArrayList<>();
                }
                companyAssets.add(dlPhoto);
                identification.setAssets(companyAssets);

                memberRepository.update(member.getCompanyId(), member.getId(), member);
            } else {
                identification.setFrontPhoto(dlPhoto);
                List<CompanyAsset> companyAssets = new ArrayList<>();
                companyAssets.add(dlPhoto);
                identification.setAssets(companyAssets);
                memberRepository.update(member.getCompanyId(), member.getId(), member);
            }
        }
    }

    @Override
    public ConsumerUser updateRecPhoto(InputStream inputStream, String fileName) {
        ConsumerUser dbConsumerUser = consumerUserRepository.getById(storeToken.getConsumerId());
        if (dbConsumerUser == null) {
            throw new BlazeAuthException("Authentication", "User is not valid.");
        }

        if (inputStream == null || StringUtils.isBlank(fileName)) {
            throw new BlazeInvalidArgException("Form", "Invalid name or stream");
        }

        String ext2 = FilenameUtils.getExtension(fileName); // returns "exe"
        ext2 = "." + ext2;
        ConsumerAsset asset = new ConsumerAsset();

        if (ext2.equalsIgnoreCase(".pdf")) {
            asset.setType(Asset.AssetType.Document);
        } else {
            asset.setType(Asset.AssetType.Photo);
        }

        try {
            File file = stream2file(inputStream, ext2);
            String key = amazonS3Service.uploadFile(file, UUID.randomUUID().toString(), ext2);
            asset.prepare();
            asset.setName(fileName);
            asset.setKey(key);
            asset.setActive(true);
            dbConsumerUser.setRecPhoto(asset);
            dbConsumerUser.setMedical(true);
            dbConsumerUser.prepare(storeToken.getCompanyId());

            consumerUserRepository.update(dbConsumerUser.getId(), dbConsumerUser);

            dbConsumerUser.setPassword(null);

            if (StringUtils.isNotBlank(dbConsumerUser.getMemberId())) {
                this.updateMemberRecPhoto(dbConsumerUser);
            }
        } catch (Exception e) {
            throw new BlazeInvalidArgException("Upload", "Cannot upload document.");
        }

        return dbConsumerUser;
    }

    private void updateMemberRecPhoto(ConsumerUser consumerUser) {
        Member member = memberRepository.get(storeToken.getCompanyId(), consumerUser.getMemberId());
        if (member == null) {
            return;
        }

        Recommendation recommendation = null;
        if (member.getRecommendations() == null || member.getRecommendations().size() > 0) {
            recommendation = new Recommendation();
            recommendation.prepare(storeToken.getCompanyId());
            recommendation.setRecommendationNumber(consumerUser.getRecNo());
            recommendation.setState(consumerUser.getDlState());
            recommendation.setExpirationDate(consumerUser.getRecExpiration());
            recommendation.setIssueDate(consumerUser.getRecIssueDate());
            member.setRecommendations(new ArrayList<Recommendation>());
            member.getRecommendations().add(recommendation);
        } else {
            for (Recommendation rec : member.getRecommendations()) {
                if (StringUtils.isNotBlank(rec.getRecommendationNumber()) && StringUtils.isNotBlank(consumerUser.getRecNo())
                    && rec.getRecommendationNumber().equalsIgnoreCase(consumerUser.getRecNo())) {
                    recommendation = rec;
                    break;
                }
            }
            if (recommendation == null) {
                recommendation = member.getRecommendations().get(0);
            }
        }

        if (consumerUser.getRecPhoto() != null) {

            CompanyAsset recPhoto = companyAssetRepository.getAssetByKey(member.getCompanyId(), consumerUser.getRecPhoto().getKey());
            if (recPhoto == null) {
                recPhoto = consumerUser.getRecPhoto().toCompanyAsset(member.getCompanyId());
                companyAssetRepository.save(recPhoto);
                recommendation.setFrontPhoto(recPhoto);


                List<CompanyAsset> companyAssets = recommendation.getAssets();
                if (companyAssets == null) {
                    companyAssets = new ArrayList<>();
                }
                companyAssets.add(recPhoto);
                recommendation.setAssets(companyAssets);

                memberRepository.update(member.getCompanyId(), member.getId(), member);
            } else {
                recommendation.setFrontPhoto(recPhoto);
                List<CompanyAsset> companyAssets = new ArrayList<>();

                companyAssets.add(recPhoto);
                recommendation.setAssets(companyAssets);
                memberRepository.update(member.getCompanyId(), member.getId(), member);
            }
        }
    }

    @Override
    public AssetStreamResult getConsumerAssetStream(String assetKey, String assetToken) {

        String consumerId = null;
        if (assetToken != null) {
            try {
                ConsumerAssetAccessToken assetAccessToken = securityUtil.decryptConsumerAssetAccessToken(assetToken);
                consumerId = assetAccessToken.getConsumerId();
            } catch (Exception e) {
                // ignore
            }
        } else {
            consumerId = storeToken.getConsumerId();
        }
        ConsumerUser dbConsumerUser = consumerUserRepository.getById(consumerId);

        if (dbConsumerUser == null) {
            throw new BlazeAuthException("Authentication", "User is not valid.");
        }

        ConsumerAsset consumerAsset = null;
        if (StringUtils.isBlank(assetKey)) {
            throw new BlazeInvalidArgException("AssetKey", "Unknown asset key.");
        }
        // look at dl image
        if (dbConsumerUser.getDlPhoto() != null
                && assetKey.equalsIgnoreCase(dbConsumerUser.getDlPhoto().getKey())) {
            consumerAsset = dbConsumerUser.getDlPhoto();
        }

        // look at rec image
        if (dbConsumerUser.getRecPhoto() != null
                && assetKey.equalsIgnoreCase(dbConsumerUser.getRecPhoto().getKey())) {
            consumerAsset = dbConsumerUser.getRecPhoto();
        }

        if (consumerAsset == null) {
            // look at company asset
            CompanyAsset companyAsset = companyAssetRepository.getAssetByKey(storeToken.getCompanyId(), assetKey);
            if (companyAsset != null) {
                AssetStreamResult result = amazonS3Service.downloadFile(companyAsset.getKey(), true);
                result.setAsset(companyAsset);
                return result;
            }
        }

        if (consumerAsset == null) {
            throw new BlazeInvalidArgException("AssetKey", "Unknown asset key.");
        }
        AssetStreamResult result = amazonS3Service.downloadFile(consumerAsset.getKey(), true);
        result.setAsset(consumerAsset);
        return result;

    }

    @Override
    public ConsumerUserImportResult parseConsumerDataImport(List<ConsumerUserAddRequest> requestList) {

        if (requestList == null || requestList.isEmpty()) {
            return new ConsumerUserImportResult();
        }

        Shop shop = shopRepository.get(storeToken.getCompanyId(), storeToken.getShopId());
        if (shop == null) {
            throw new BlazeInvalidArgException("Blaze", "Shop does not exist.");
        }

        List<ConsumerUser> consumers = new ArrayList<>();
        Set<String> emailIds = new HashSet<>();
        long currentDate = DateTime.now().getMillis();
        for (ConsumerUserAddRequest consumerUserAddRequest : requestList) {
            ConsumerUser consumerUser = new ConsumerUser();
            consumerUser.prepare(storeToken.getCompanyId());
            consumerUser.setSourceCompanyId(storeToken.getCompanyId());
            consumerUser.setEmail(consumerUserAddRequest.getEmail());
            consumerUser.setFirstName(consumerUserAddRequest.getFirstName());
            consumerUser.setLastName(consumerUserAddRequest.getLastName());
            consumerUser.setPrimaryPhone(consumerUserAddRequest.getPhoneNo());
            consumerUser.setAccepted(Boolean.TRUE);
            consumerUser.setAcceptedDate(currentDate);
            consumerUser.setSource(ConsumerUser.RegistrationSource.WooCommerce);
            emailIds.add(consumerUserAddRequest.getEmail());
            consumers.add(consumerUser);
        }

        return importConsumerUser(consumers, emailIds);
    }

    private ConsumerUserImportResult importConsumerUser(List<ConsumerUser> requestUser, Set<String> emailIds) {
        HashMap<String, ConsumerUser> dbConsumerUser = new HashMap<>();
        Iterable<ConsumerUser> consumerUsers = consumerUserRepository.getConsumerByEmails(storeToken.getCompanyId(), emailIds);
        for (ConsumerUser consumerUser : consumerUsers) {
            dbConsumerUser.put(consumerUser.getEmail(), consumerUser);
        }

        HashMap<String, Member> memberEmailIds = new HashMap<>();

        Iterable<Member> members = memberRepository.getMembersByEmails(storeToken.getCompanyId(), emailIds);
        for (Member member : members) {
            memberEmailIds.put(member.getEmail(), member);
        }

        List<ConsumerUser> consumerUserList = new ArrayList<>();
        List<Member> memberList = new ArrayList<>();

        ConsumerUserImportResult consumerUserImportResult = new ConsumerUserImportResult();
        List<ConsumerUser> addFailed = new ArrayList<>();

        Set<String> requestConsumerEmailIds = new HashSet<>();
        for (ConsumerUser consumerUser : requestUser) {
            if (requestConsumerEmailIds.contains(consumerUser.getEmail()) || dbConsumerUser.containsKey(consumerUser.getEmail()) || memberEmailIds.containsKey(consumerUser.getEmail())) {
                addFailed.add(consumerUser);
            } else {
                try {
                    Member member = this.createMemberForConsumerUser(consumerUser);
                    memberList.add(member);
                    consumerUser.setMemberId(member.getId());
                    consumerUserList.add(consumerUser);
                    requestConsumerEmailIds.add(consumerUser.getEmail());
                } catch (BlazeInvalidArgException e) {
                    addFailed.add(consumerUser);
                }
            }
        }

        List<List<ConsumerUser>> partition = Lists.partition(consumerUserList, 1000);
        for (List<ConsumerUser> users : partition) {
            if (!users.isEmpty()) {
                consumerUserRepository.save(users);
            }
        }

        List<List<Member>> membersPartition = Lists.partition(memberList, 1000);
        for (List<Member> list : membersPartition) {
            if (!memberList.isEmpty()) {
                memberRepository.save(list);
                elasticSearchManager.createOrUpdateIndexedDocuments(list, list.size());
                realtimeService.sendRealTimeEvent(storeToken.getShopId(), RealtimeService.RealtimeEventType.MembersUpdateEvent, null);
            }
        }


        consumerUserImportResult.setAddFailed(addFailed);
        consumerUserImportResult.setAddSuccess(consumerUserList);
        return consumerUserImportResult;
    }

    private Member createMemberForConsumerUser(ConsumerUser consumerUser) {

        Member member = new Member();

        member.prepare(storeToken.getCompanyId());
        member.setShopId(storeToken.getShopId());
        member.setFirstName(consumerUser.getFirstName());
        member.setLastName(consumerUser.getLastName());
        member.setPrimaryPhone(consumerUser.getPrimaryPhone());
        member.setEmail(consumerUser.getEmail());
        member.setConsumerUserId(consumerUser.getId());
        member.setStatus(Member.MembershipStatus.Active);

        MemberGroup defaultGroup = memberGroupRepository.getDefaultMemberGroup(storeToken.getCompanyId());
        if (defaultGroup != null) {
            member.setMemberGroupId(defaultGroup.getId());
            member.setMemberGroup(defaultGroup);
        }
        return member;
    }



    //Private
    /**
     * This method checks weather any member exist with same email
     * @param emailId : requested email id
     */
    private ConsumerUser checkMemberExistenceByEmail(String emailId) {

        Iterable<Member> memberByEmail = memberRepository.getMemberByEmail(storeToken.getCompanyId(), emailId);

        Member member = null;
        for (Member dbMember : memberByEmail) {
            member = dbMember;
            break;
        }

        //If dispensary has member profile created, but not corresponding member created
        //If member profile exist, and no consumer user exist
        if (member != null && StringUtils.isBlank(member.getConsumerUserId())) {
            ConsumerUser consumerUser = this.createConsumerUserFromMember(member);
            memberRepository.updateConsumerUserId(storeToken.getCompanyId(), consumerUser.getId(), member.getId());
            return consumerUser;
        }
        return null;
    }

    /**
     * Create consumer user from existing member
     * @param member : existing member
     */
    @Override
    public ConsumerUser createConsumerUserFromMember(Member member) {
        ConsumerUser consumerUser = new ConsumerUser();

        consumerUser.prepare(storeToken.getCompanyId());
        consumerUser.setEmail(member.getEmail().toLowerCase().trim());
        consumerUser.setFirstName(member.getFirstName());
        consumerUser.setLastName(member.getLastName());
        consumerUser.setMiddleName(member.getMiddleName());
        consumerUser.setPrimaryPhone(member.getPrimaryPhone());
        consumerUser.setMarketingSource(member.getMarketingSource());
        consumerUser.setDob(member.getDob());
        consumerUser.setActive(true);
        consumerUser.setSourceCompanyId(storeToken.getCompanyId());
        consumerUser.setEmailOptIn(member.isEmailOptIn());
        consumerUser.setTextOptIn(member.isTextOptIn());
        consumerUser.setMedical(member.isMedical());
        consumerUser.setConsumerType(member.getConsumerType());
        consumerUser.setSex(member.getSex());
        consumerUser.setMemberId(member.getId());

        Address address = member.getAddress();
        if (address != null) {
            address.prepare(storeToken.getCompanyId());
            consumerUser.setAddress(address);
        }

        Shop shop = shopRepository.get(storeToken.getCompanyId(), storeToken.getShopId());
        String cpn = member.getPrimaryPhone();
        if (shop != null) {
            cpn = AmazonServiceManager.cleanPhoneNumber(member.getPrimaryPhone(), shop);
        }
        consumerUser.setCpn(cpn);

        //memberService.updateConsumerUserInfo(consumerUser, member);


        return consumerUserRepository.save(consumerUser);
    }

    // Private methods
    private StoreAuthToken createNewToken(String consumerUserId) {
        StoreAuthToken newToken = new StoreAuthToken();
        newToken.setCompanyId(storeToken.getCompanyId());
        newToken.setShopId(storeToken.getShopId());
        newToken.setConsumerId(consumerUserId);
        newToken.setLoginDate(DateTime.now().getMillis());
        newToken.setExpirationDate(DateTime.now().plusMinutes(connectConfiguration.getAuthTTL()).getMillis());
        return newToken;
    }

    // Private methods
    private ConsumerAssetAccessToken createConsumerAssetToken(String consumerUserId) {
        ConsumerAssetAccessToken newToken = new ConsumerAssetAccessToken();
        newToken.setConsumerId(consumerUserId);
        newToken.setSignedDate(DateTime.now().getMillis());
        newToken.setExpirationDate(DateTime.now().plusMinutes(connectConfiguration.getAuthTTL()).getMillis());
        return newToken;
    }

    private String getNewEncryptedToken(StoreAuthToken authToken) {
        return securityUtil.createStoreToken(authToken);
    }

    private String getNewEncryptedAssetToken(ConsumerAssetAccessToken assetAccessToken) {
        return securityUtil.createConsumerAssetAccessToken(assetAccessToken);
    }

    /**
     * This method check member of token's company and shop with no email or dob
     */
    @Override
    public void updateMemberWithEmailAndDob() {
        long count = memberRepository.countEmptyEmailAndDobMember(storeToken.getCompanyId(), storeToken.getShopId());

        if (count > 0) {
            MemberEmailAndDobUpdateEvent event = new MemberEmailAndDobUpdateEvent();
            event.setCompanyId(storeToken.getCompanyId());
            event.setShopId(storeToken.getShopId());

            eventBus.post(event);
            event.getResponse();
        }
    }

    /**
     * This method checks the consumer license number. It will return exception when license number already exist based on conditions.
     * @param dbConsumerUser
     * @param userDlNo
     */
    public void checkConsumerLicense(ConsumerUser dbConsumerUser, String userDlNo) {
        if (StringUtils.isBlank(userDlNo)) {
            return;
        }
        long consumerCount = 0;
        long memberCount = 0;
        if (StringUtils.isBlank(dbConsumerUser.getDlNo()) || (StringUtils.isNotBlank(dbConsumerUser.getDlNo()) && !dbConsumerUser.getDlNo().equals(userDlNo))) {
            consumerCount =  consumerUserRepository.countConsumerByDrivingLicense(dbConsumerUser.getCompanyId(), userDlNo);
            memberCount = memberRepository.countMemberByDrivingLicense(dbConsumerUser.getCompanyId(), userDlNo);
            if (consumerCount > 0 || memberCount > 0) {
                throw new BlazeInvalidArgException(CONSUMER_USER, "User already exists with this driving license.");
            }
        }
    }
}
