package com.fourtwenty.core.services.store.impl;

import com.fourtwenty.core.domain.models.company.Contract;
import com.fourtwenty.core.domain.models.company.MemberGroup;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.company.SignedContract;
import com.fourtwenty.core.domain.models.customer.Doctor;
import com.fourtwenty.core.domain.models.customer.Identification;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.customer.Recommendation;
import com.fourtwenty.core.domain.models.views.MemberLimitedView;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.tokens.StoreAuthToken;
import com.fourtwenty.core.services.AbstractStoreServiceImpl;
import com.fourtwenty.core.services.mgmt.RoleService;
import com.fourtwenty.core.services.store.MemberUserService;
import com.fourtwenty.core.util.DateUtil;
import com.google.inject.Provider;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;


public class MemberUserServiceImpl extends AbstractStoreServiceImpl implements MemberUserService {
    @Inject
    RoleService roleService;
    @Inject
    TransactionRepository transactionRepository;
    @Inject
    MemberRepository memberRepository;
    @Inject
    DoctorRepository doctorRepository;
    @Inject
    ContractRepository contractRepository;
    @Inject
    ShopRepository shopRepository;
    @Inject
    MemberGroupRepository memberGroupRepository;

    @Inject
    public MemberUserServiceImpl(Provider<StoreAuthToken> tokenProvider) {
        super(tokenProvider);
    }

    @Override
    public SearchResult<MemberLimitedView> getMembershipsForActiveShop(int start, int limit) {
        if (start < 0) start = 0;
        if (limit <= 0 || limit > 5000) {
            limit = 5000;
        }

        SearchResult<Member> memberSearchResult;
        memberSearchResult = memberRepository.findItems(storeToken.getCompanyId(),start, limit);

        SearchResult<MemberLimitedView> result = prepareDataForView(memberSearchResult, true);
        result.getValues().sort(new Comparator<MemberLimitedView>() {
            @Override
            public int compare(MemberLimitedView o1, MemberLimitedView o2) {
                return o1.getFirstName().compareTo(o2.getFirstName());
            }
        });
        return result;
    }


    private SearchResult<MemberLimitedView> prepareDataForView(SearchResult<Member> memberSearchResult, boolean limitView) {
        HashMap<String, Doctor> doctorHashMap = doctorRepository.listAllAsMap(storeToken.getCompanyId());
        List<Contract> contracts = contractRepository.getActiveContracts(storeToken.getCompanyId(), storeToken.getShopId());
        HashMap<String, MemberGroup> memberGroupHashMap = memberGroupRepository.listAsMap(storeToken.getCompanyId());

        Contract activeContract = null;
        if (contracts.size() > 0) {
            activeContract = contracts.get(0);
        }
        Shop shop = shopRepository.get(storeToken.getCompanyId(), storeToken.getShopId());
        if (shop == null) {
            throw new BlazeInvalidArgException("Shop", "Shop does not found");
        }
        long currentDate = DateTime.now().getMillis();
        long in30DaysDate = DateTime.now().plusDays(30).getMillis();
        SearchResult<MemberLimitedView> limitedViewSearchResult = new SearchResult<MemberLimitedView>();
        for (Member member : memberSearchResult.getValues()) {
            boolean isRecExpired = true;
            boolean isDLExpired = true;
            boolean isAgreementExpired = false;
            long recExpiryLeft = 0;
            if (member.getAddress() != null) {
                member.getAddress().clean();
            }

            for (Identification identification : member.getIdentifications()) {
                if (identification.getExpirationDate() != null && identification.getExpirationDate() >= currentDate) {
                    isDLExpired = false;
                }
            }

            String licenseNumber = "";
            if (member.getIdentifications().isEmpty()) {
                member.getExpStatuses().add("DL Missing");
            } else if (!member.getIdentifications().isEmpty()) {
                Identification lastId = member.getIdentifications().get(member.getIdentifications().size() - 1);
                if (Objects.isNull(lastId.getExpirationDate())) {
                    member.getExpStatuses().add("DL Expiration date missing.");
                } else if (lastId.getExpirationDate() < in30DaysDate) {
                    member.getExpStatuses().add("DL Expires: " + DateFormatUtils.format(lastId.getExpirationDate(), "MM/dd/yyyy"));
                }
                licenseNumber = lastId.getLicenseNumber();
            }


            for (Recommendation recommendation : member.getRecommendations()) {
                recommendation.setDoctor(doctorHashMap.get(recommendation.getDoctorId()));

                if (recommendation.getExpirationDate() != null && recommendation.getExpirationDate() >= currentDate) {
                    isRecExpired = false;
                    recExpiryLeft = DateUtil.getStandardDaysBetweenTwoDates(currentDate, recommendation.getExpirationDate());
                }
            }

            if (member.isMedical() && member.getRecommendations().isEmpty()) {
                member.getExpStatuses().add("Recommendation: Missing");
            } else if (member.isMedical() && !Objects.isNull(member.getRecommendations()) && !member.getRecommendations().isEmpty()) {
                Recommendation rec = member.getRecommendations().get(member.getRecommendations().size() - 1);

                if (Objects.isNull(rec.getExpirationDate())) {
                    member.getExpStatuses().add("Rec Expiration date missing.");
                } else if (rec.getExpirationDate() < in30DaysDate) {
                    member.getExpStatuses().add("Rec Expires: " + DateFormatUtils.format(rec.getExpirationDate(), "MM/dd/yyyy"));
                }

            }

            // active and required
            String agreeExp = "";
            if (activeContract != null && activeContract.isRequired()) {
                // ignore
                if (member.getContracts().size() > 0) {
                    agreeExp = "New Required Agreement";
                    isAgreementExpired = true;
                    for (SignedContract signedContract : member.getContracts()) {
                        if (signedContract != null && signedContract.getContractId() != null) {
                            if (signedContract.getContractId().equalsIgnoreCase(activeContract.getId())) {
                                // agreement match.
                                isAgreementExpired = false;
                                agreeExp = "";
                                break;
                            }
                        }
                    }
                } else {
                    isAgreementExpired = true;
                    agreeExp = "Agreement Missing";
                }
            }

            if (StringUtils.isNotEmpty(agreeExp)) {
                member.getExpStatuses().add(agreeExp);
            }


            member.setDlExpired(isDLExpired);
            member.setRecommendationExpired(isRecExpired);
            member.setAgreementExpired(isAgreementExpired);
            member.setRecommendationExpiryLeft(recExpiryLeft);

            if (limitView) {
                MemberLimitedView memberLimitedView = new MemberLimitedView();
                memberLimitedView.setId(member.getId());
                memberLimitedView.setCompanyId(member.getCompanyId());
                memberLimitedView.setCreated(member.getCreated());
                memberLimitedView.setModified(member.getModified());
                memberLimitedView.setFirstName(member.getFirstName());
                memberLimitedView.setLastName(member.getLastName());
                memberLimitedView.setPrimaryPhone(member.getPrimaryPhone());
                memberLimitedView.setExpStatuses(member.getExpStatuses());
                memberLimitedView.setEmail(member.getEmail());
                memberLimitedView.setLoyaltyPoints(member.getLoyaltyPoints());
                memberLimitedView.setMedical(member.isMedical());
                //memberLimitedView.setIdentifications(member.getIdentifications());
                //memberLimitedView.setRecommendations(member.getRecommendations());
                memberLimitedView.setStartDate(member.getStartDate());
                memberLimitedView.setLicenseNumber(licenseNumber);
                memberLimitedView.setStatus(member.getStatus());
                memberLimitedView.setBan(member.isBanPatient());
                memberLimitedView.setRecommendationExpired(isRecExpired);
                memberLimitedView.setRecommendationExpiryLeft(recExpiryLeft);

                MemberGroup memberGroup = memberGroupHashMap.get(member.getMemberGroupId());
                if (memberGroup != null) {
                    memberLimitedView.setMemberGroupName(memberGroup.getName());
                }

                limitedViewSearchResult.getValues().add(memberLimitedView);
            }
        }

        limitedViewSearchResult.setTotal((long) limitedViewSearchResult.getValues().size());

        return limitedViewSearchResult;
    }
}
