package com.fourtwenty.core.services.store.impl;

import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.domain.models.company.Contract;
import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.company.Role;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.company.Terminal;
import com.fourtwenty.core.domain.models.company.TimeCard;
import com.fourtwenty.core.domain.models.customer.Doctor;
import com.fourtwenty.core.domain.models.partner.PartnerWebHook;
import com.fourtwenty.core.domain.models.payment.ShopPaymentOption;
import com.fourtwenty.core.domain.models.product.Inventory;
import com.fourtwenty.core.domain.models.product.ProductWeightTolerance;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.domain.repositories.partner.PartnerWebHookRepository;
import com.fourtwenty.core.domain.repositories.payment.ShopPaymentOptionRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.company.EmployeeResult;
import com.fourtwenty.core.rest.partners.request.PartnerWebHookAddRequest;
import com.fourtwenty.core.rest.store.results.StoreInfoResult;
import com.fourtwenty.core.security.tokens.StoreAuthToken;
import com.fourtwenty.core.services.AbstractStoreServiceImpl;
import com.fourtwenty.core.services.store.StoreInfoService;
import com.google.common.collect.Lists;
import com.google.inject.Provider;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by mdo on 5/9/17.
 */
public class StoreInfoServiceImpl extends AbstractStoreServiceImpl implements StoreInfoService {

    private static final String MEMBER_NOT_FOUND = "Member does not found";
    private static final String SHOP = "Shop";
    private static final String SHOP_NOT_FOUND = "Shop does not exist.";

    @Inject
    ShopRepository shopRepository;
    @Inject
    ContractRepository contractRepository;
    @Inject
    DoctorRepository doctorRepository;
    @Inject
    CompanyRepository companyRepository;
    @Inject
    ProductWeightToleranceRepository weightToleranceRepository;
    @Inject
    TerminalRepository terminalRepository;
    @Inject
    private MemberRepository memberRepository;
    @Inject
    private InventoryRepository inventoryRepository;
    @Inject
    private PartnerWebHookRepository partnerWebHookRepository;
    @Inject
    private EmployeeRepository employeeRepository;
    @Inject
    private RoleRepository roleRepository;
    @Inject
    private TimeCardRepository timeCardRepository;
    @Inject
    private ShopPaymentOptionRepository shopPaymentOptionRepository;

    @Inject
    public StoreInfoServiceImpl(Provider<StoreAuthToken> tokenProvider) {
        super(tokenProvider);
    }

    @Override
    public StoreInfoResult getStoreInfo() {
        Shop shop = shopRepository.get(storeToken.getCompanyId(), storeToken.getShopId());
        Company company = companyRepository.getById(storeToken.getCompanyId());
        List<Contract> contracts = contractRepository.getActiveContracts(storeToken.getCompanyId(), storeToken.getShopId());

        Iterable<ProductWeightTolerance> tolerances = weightToleranceRepository.list(storeToken.getCompanyId());
        HashMap<String, ProductWeightTolerance> weightToleranceHashMap = new HashMap<>();
        for (ProductWeightTolerance tolerance : tolerances) {
            weightToleranceHashMap.put(tolerance.getWeightKey().name(), tolerance);
        }

        StoreInfoResult result = new StoreInfoResult();
        result.setShop(shop);
        if (contracts.size() > 0) {
            result.setContract(contracts.get(0));
        }
        if (company != null) {
            result.setCompanyLogoURL(company.getLogoURL());
        }
        result.setToleranceMap(weightToleranceHashMap);
        return result;
    }

    @Override
    public SearchResult<Doctor> searchDoctor(String term) {
        if (StringUtils.isBlank(term)) {
            return new SearchResult<Doctor>();
        }
        return doctorRepository.searchDoctorsByTerm(storeToken.getCompanyId(), term, 0, 100);
    }

    /**
     * This is override method to get all active terminals for current shop.
     *
     * @param start
     * @param limit
     * @return
     */
    @Override
    public SearchResult<Terminal> getActiveTerminalByShop(int start, int limit) {
        if (limit == 0 || limit > 200) {
            limit = 200;
        }
        if (start < 0) {
            start = 0;
        }
        return terminalRepository.getAllTerminalsByState(storeToken.getCompanyId(), storeToken.getShopId(), true, "{modified:-1}", start, limit);
    }

    @Override
    public SearchResult<Inventory> getActiveInventoriesByShop(int start, int limit) {
        if (limit == 0 || limit > 200) {
            limit = 200;
        }
        if (start < 0) {
            start = 0;
        }
        return inventoryRepository.findActiveItems(storeToken.getCompanyId(), storeToken.getShopId(), start, limit);

    }

    @Override
    public List<PartnerWebHook> createPartnerWebHookForPlugIn(List<PartnerWebHookAddRequest> request) {
        if (request == null) {
            throw new BlazeInvalidArgException("PARTNER WebHook", "Request does not exist.");
        }
        List<PartnerWebHook> partnerWebHooks = new ArrayList<>();

        for (PartnerWebHookAddRequest webHookRequest : request) {
            if (webHookRequest.getWebHookType() == null) {
                throw new BlazeInvalidArgException("PARTNER WebHook", "WebHook Type does not exist.");
            }
            PartnerWebHook webHook = createPartnerWebHooks(webHookRequest);
            partnerWebHooks.add(webHook);
        }

        return partnerWebHooks;
    }

    private PartnerWebHook createPartnerWebHooks(PartnerWebHookAddRequest request) {
        String shopId = request.getShopId();
        if (StringUtils.isBlank(shopId)) {
            shopId = storeToken.getShopId();
        }
        Shop shop = shopRepository.getById(shopId);
        if (shop == null) {
            throw new BlazeInvalidArgException(SHOP, SHOP_NOT_FOUND);
        }
        //Check partner web hook if already available
        PartnerWebHook partnerWebHook = partnerWebHookRepository.getByWebHookType(storeToken.getCompanyId(), shopId, request.getWebHookType());

        if (partnerWebHook == null) {
            partnerWebHook = new PartnerWebHook();
            partnerWebHook.prepare(storeToken.getCompanyId());
            partnerWebHook.setShopId(shopId);
            partnerWebHook.setWebHookType(request.getWebHookType());
            partnerWebHook.setWebHookUrls(request.getWebHookUrls());
            partnerWebHook.setUrl(request.getUrl());
            partnerWebHookRepository.save(partnerWebHook);
        } else {
            partnerWebHook.setWebHookUrls(request.getWebHookUrls());
            partnerWebHook.setUrl(request.getUrl());
            partnerWebHookRepository.update(partnerWebHook.getId(), partnerWebHook);
        }
        return partnerWebHook;
    }

    @Override
    public SearchResult<EmployeeResult> searchEmployees(String currentEmployeeId, int start, int limit) {
        if (limit == 0 || limit > 200) {
            limit = 200;
        }
        if (start < 0) {
            start = 0;
        }

        String projection = (StringUtils.isNotBlank(currentEmployeeId)) ? getPermissionProjection(currentEmployeeId) : "{password:0,pin:0}";
        SearchResult<EmployeeResult> result = employeeRepository.findItems(storeToken.getCompanyId(), start, limit, projection, EmployeeResult.class);
        processCompositeResults(result.getValues());
        return result;
    }

    @Override
    public Employee getEmployeeById(String employeeId) {
        String projection = "{password:0,pin:0}";
        Employee dbEmployee = employeeRepository.get(storeToken.getCompanyId(), employeeId, projection);
        if (dbEmployee == null) {
            throw new BlazeInvalidArgException("EmployeeId", "Employee does not exist with this id.");
        }
        return dbEmployee;
    }

    private String getPermissionProjection(String currentEmployeeId) {
        Employee employee = employeeRepository.get(storeToken.getCompanyId(), currentEmployeeId);
        Role role = roleRepository.get(storeToken.getCompanyId(), employee.getRoleId());
        String projection = "{password:0,pin:0}";
        if (role != null) {
            boolean isManager = role.getPermissions().contains(Role.Permission.WebEmployeeManage);
            if (isManager) {
                projection = "{password:0}";
            }
        }
        return projection;
    }

    private void processCompositeResults(List<EmployeeResult> employeeResults) {
        List<ObjectId> timeCardIds = new ArrayList<>();
        Set<ObjectId> roleIds = new HashSet<>();
        Set<ObjectId> terminalIds = new HashSet<>();

        for (EmployeeResult employee : employeeResults) {
            if (StringUtils.isNotBlank(employee.getRoleId()) && ObjectId.isValid(employee.getRoleId())) {
                roleIds.add(new ObjectId(employee.getRoleId()));
            }
            if (StringUtils.isNotBlank(employee.getAssignedTerminalId()) && ObjectId.isValid(employee.getAssignedTerminalId())) {
                terminalIds.add(new ObjectId(employee.getAssignedTerminalId()));
            }
            if (StringUtils.isNotEmpty(employee.getTimecardId()) && ObjectId.isValid(employee.getTimecardId())) {
                timeCardIds.add(new ObjectId(employee.getTimecardId()));
            }
        }

        HashMap<String, Role> roleHashMap = roleRepository.listAsMap(storeToken.getCompanyId(), Lists.newArrayList(roleIds));
        HashMap<String, Terminal> terminalMap = terminalRepository.listAsMap(storeToken.getCompanyId(), Lists.newArrayList(terminalIds));
        HashMap<String, TimeCard> timeCards = timeCardRepository.findItemsInAsMap(storeToken.getCompanyId(), storeToken.getShopId(), timeCardIds);

        for (EmployeeResult employee : employeeResults) {
            employee.setRole(roleHashMap.get(employee.getRoleId()));
            Terminal terminal = terminalMap.get(employee.getAssignedTerminalId());
            if (terminal != null) {
                employee.setTerminalName(terminal.getName());
            }
            employee.setTimeCard(timeCards.get(employee.getTimecardId()));
        }
    }

    @Override
    public List<ShopPaymentOption> getPaymentOptions() {
        List<ShopPaymentOption> list =
                Lists.newArrayList(shopPaymentOptionRepository.listAllByShop(storeToken.getCompanyId(), storeToken.getShopId()));
        return list;
    }
}
