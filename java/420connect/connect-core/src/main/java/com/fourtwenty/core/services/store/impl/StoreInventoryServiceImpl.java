package com.fourtwenty.core.services.store.impl;

import com.amazonaws.util.CollectionUtils;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.company.Terminal;
import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrder;
import com.fourtwenty.core.domain.models.store.OnlineStoreInfo;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.rest.dispensary.results.BrandResult;
import com.fourtwenty.core.rest.dispensary.requests.inventory.InventoryTransferStatusRequest;
import com.fourtwenty.core.rest.dispensary.requests.partners.PartnerInventoryTransferHistory;
import com.fourtwenty.core.rest.dispensary.requests.partners.PartnerInventoryTransferStatusRequest;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.ProductCategoryResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.store.results.ProductWithInfo;
import com.fourtwenty.core.security.tokens.StoreAuthToken;
import com.fourtwenty.core.services.AbstractStoreServiceImpl;
import com.fourtwenty.core.services.inventory.CommonInventoryService;
import com.fourtwenty.core.services.mgmt.ProductService;
import com.fourtwenty.core.services.store.StoreInventoryService;
import com.google.common.collect.Lists;
import com.google.inject.Provider;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Created by mdo on 4/21/17.
 */
public class StoreInventoryServiceImpl extends AbstractStoreServiceImpl implements StoreInventoryService {
    @Inject
    ProductCategoryRepository productCategoryRepository;
    @Inject
    ProductRepository productRepository;
    @Inject
    VendorRepository vendorRepository;
    @Inject
    ProductBatchRepository batchRepository;
    @Inject
    ProductWeightToleranceRepository weightToleranceRepository;
    @Inject
    TerminalRepository terminalRepository;
    @Inject
    InventoryRepository inventoryRepository;
    @Inject
    ProductPrepackageQuantityRepository productPrepackageQuantityRepository;
    @Inject
    BrandRepository brandRepository;
    @Inject
    private ShopRepository shopRepository;
    @Inject
    private ProductService productService;
    @Inject
    private ProductBatchRepository productBatchRepository;
    @Inject
    private PurchaseOrderRepository purchaseOrderRepository;
    @Inject
    private BatchQuantityRepository batchQuantityRepository;
    @Inject
    private CommonInventoryService commonInventoryService;

    @Inject
    public StoreInventoryServiceImpl(Provider<StoreAuthToken> tokenProvider) {
        super(tokenProvider);
    }

    @Override
    public SearchResult<ProductWithInfo> getAllProducts(String categoryId, String strain, String term, List<String> incomingTags,
                                                        String vendorId, int start, int limit, String brandId) {
        if (limit <= 0) {
            limit = 10000;
        }

        if (StringUtils.isNotBlank(term)) {
            term = term.replaceAll("\\+", "");
            term = term.replaceAll("\\{", "");
            term = term.replaceAll("\\)", "");
            term = term.replaceAll("\\(", "");
            term = term.replaceAll("}", "");
            term = term.replaceAll("\\[", "");
            term = term.replaceAll("]", "");
            term = term.replaceAll("\\*", "");
            term = term.replaceAll("\\\\", "");
        }
        LinkedHashSet<String> uniqueTags = new LinkedHashSet<>();

        // filter out tags
        if (incomingTags != null) {
            for (String tag : incomingTags) {
                if (StringUtils.isNotBlank(tag)) {
                    uniqueTags.add(tag);
                }
            }
        }

        List<String> tags = Lists.newArrayList(uniqueTags);
        Shop shop = shopRepository.get(storeToken.getCompanyId(), storeToken.getShopId());

        SearchResult<ProductWithInfo> products = new SearchResult<>();
        if (StringUtils.isNotBlank(categoryId)) {
            ProductCategory category = productCategoryRepository.get(storeToken.getCompanyId(), categoryId);
            if (category == null) {
                throw new BlazeInvalidArgException("Products", "Invalid category id");
            }
            /* when have strain without vendorId */
            if (StringUtils.isNotBlank(strain) && StringUtils.isBlank(vendorId) && StringUtils.isBlank(brandId)) {
                if (StringUtils.isNotBlank(term)) {
                    products = productRepository.findProductsByCategoryIdStrainSearch(storeToken.getCompanyId(),
                            storeToken.getShopId(),
                            categoryId,
                            strain,
                            term,
                            tags,
                            "{name:1}",
                            start,
                            limit, ProductWithInfo.class);
                } else {
                    products = productRepository.findProductsByCategoryIdStrain(storeToken.getCompanyId(),
                            storeToken.getShopId(),
                            categoryId,
                            strain,
                            tags,
                            "{name:1}",
                            start,
                            limit, ProductWithInfo.class);
                }

            } else if (StringUtils.isBlank(strain) && StringUtils.isNotBlank(vendorId) && StringUtils.isBlank(brandId)) {
                /* when have vendorId without strain */
                if (StringUtils.isNotBlank(term)) {
                    products = productRepository.findProductsByCategoryIdVendorSearch(storeToken.getCompanyId(),
                            storeToken.getShopId(),
                            categoryId,
                            vendorId,
                            term,
                            tags,
                            "{name:1}",
                            start,
                            limit, ProductWithInfo.class);
                } else {
                    products = productRepository.findProductsByCategoryIdVendor(storeToken.getCompanyId(),
                            storeToken.getShopId(),
                            categoryId,
                            vendorId,
                            tags,
                            "{name:1}",
                            start,
                            limit, ProductWithInfo.class);
                }
            } else if (StringUtils.isNotBlank(strain) && StringUtils.isNotBlank(vendorId) && StringUtils.isBlank(brandId)) {
                /* when have vendorId and starin */
                if (StringUtils.isNotBlank(term)) {
                    products = productRepository.findProductsByCategoryIdStrainVendorSearch(storeToken.getCompanyId(),
                            storeToken.getShopId(),
                            categoryId,
                            strain,
                            vendorId,
                            term,
                            tags,
                            "{name:1}",
                            start,
                            limit, ProductWithInfo.class);
                } else {
                    products = productRepository.findProductsByCategoryIdAndVendorId(storeToken.getCompanyId(),
                            storeToken.getShopId(),
                            categoryId,
                            strain,
                            vendorId,
                            tags,
                            "{name:1}",
                            start,
                            limit, ProductWithInfo.class);
                }
            } else if (StringUtils.isBlank(strain) && StringUtils.isBlank(vendorId) && StringUtils.isNotBlank(brandId)) {

                products = StringUtils.isBlank(term) ?
                        productRepository.findProductsByCategoryAndBrand(storeToken.getCompanyId(),storeToken.getShopId(), categoryId, tags, brandId, "{name:1}", start, limit, ProductWithInfo.class) :
                        productRepository.findProductsByCategoryAndBrandSearch(storeToken.getCompanyId(),storeToken.getShopId(), categoryId, term, tags, brandId, "{name:1}", start, limit, ProductWithInfo.class);

            } else if (StringUtils.isBlank(strain) && StringUtils.isNotBlank(vendorId) && StringUtils.isNotBlank(brandId)) {

                products = StringUtils.isBlank(term) ?
                        productRepository.findProductsByCategoryIdVendorBrand(storeToken.getCompanyId(),storeToken.getShopId(), categoryId, vendorId, tags, brandId, "{name:1}", start, limit, ProductWithInfo.class) :
                        productRepository.findProductsByCategoryIdVendorBrandSearch(storeToken.getCompanyId(),storeToken.getShopId(), categoryId, vendorId, term, tags, brandId, "{name:1}", start, limit, ProductWithInfo.class);

            } else if (StringUtils.isNotBlank(strain) && StringUtils.isBlank(vendorId) && StringUtils.isNotBlank(brandId)) {

                products = StringUtils.isBlank(term) ?
                        productRepository.findProductsByCategoryIdStrainBrand(storeToken.getCompanyId(),storeToken.getShopId(), categoryId, strain, tags, brandId, "{name:1}", start, limit, ProductWithInfo.class) :
                        productRepository.findProductsByCategoryIdStrainBrandSearch(storeToken.getCompanyId(),storeToken.getShopId(), categoryId, strain, term, tags, brandId, "{name:1}", start, limit, ProductWithInfo.class);
            } else if (StringUtils.isNotBlank(strain) && StringUtils.isNotBlank(vendorId) && StringUtils.isNotBlank(brandId)) {

                products = StringUtils.isBlank(term) ?
                        productRepository.findProductsByCategoryIdAndVendorIdAndBrand(storeToken.getCompanyId(),storeToken.getShopId(), categoryId, strain, vendorId, tags, brandId, "{name:1}", start, limit, ProductWithInfo.class) :
                        productRepository.findProductsByCategoryIdStrainVendorBrandSearch(storeToken.getCompanyId(),storeToken.getShopId(), categoryId, strain, vendorId, term, tags, brandId, "{name:1}", start, limit, ProductWithInfo.class);
            } else {
                if (StringUtils.isNotBlank(term)) {
                    products = productRepository.findProductsByCategoryIdSearch(storeToken.getCompanyId(),
                            storeToken.getShopId(),
                            categoryId,
                            term,
                            tags,
                            "{name:1}",
                            start,
                            limit, ProductWithInfo.class);
                } else {
                    // need another version for active == true
                    products = productRepository.findProductsByCategoryId(storeToken.getCompanyId(),
                            storeToken.getShopId(),
                            categoryId,
                            tags,
                            "{name:1}",
                            true,
                            start,
                            limit, ProductWithInfo.class);
                }
            }
        } else {
            /* when have strain without vendorId */
            if (StringUtils.isNotBlank(strain) && StringUtils.isBlank(vendorId) && StringUtils.isBlank(brandId)) {
                if (StringUtils.isNotBlank(term)) {
                    products = productRepository.findProductsStrainSearch(storeToken.getCompanyId(),
                            storeToken.getShopId(),
                            strain,
                            term,
                            tags,
                            "{name:1}",
                            start,
                            limit, ProductWithInfo.class);
                } else {
                    products = productRepository.findProductsStrain(storeToken.getCompanyId(),
                            storeToken.getShopId(),
                            strain,
                            tags,
                            "{name:1}",
                            start,
                            limit, ProductWithInfo.class);
                }
            } else if (StringUtils.isBlank(strain) && StringUtils.isNotBlank(vendorId) && StringUtils.isBlank(brandId)) {
                /* when have vendorId without strain */
                if (StringUtils.isNotBlank(term)) {
                    products = productRepository.findProductsByVendorIdSearch(storeToken.getCompanyId(),
                            storeToken.getShopId(),
                            vendorId,
                            term,
                            tags,
                            "{name:1}",
                            start,
                            limit, ProductWithInfo.class);
                } else {
                    products = productRepository.findProductsByVendorId(storeToken.getCompanyId(),
                            storeToken.getShopId(),
                            vendorId,
                            tags,
                            "{name:1}",
                            start,
                            limit, ProductWithInfo.class);
                }

            } else if (StringUtils.isNotBlank(strain) && StringUtils.isNotBlank(vendorId) && StringUtils.isBlank(brandId)) {
                /* when have strain and vendorId */
                if (StringUtils.isNotBlank(term)) {
                    products = productRepository.findProductsStrainVendorSearch(storeToken.getCompanyId(),
                            storeToken.getShopId(),
                            strain,
                            vendorId,
                            term,
                            tags,
                            "{name:1}",
                            start,
                            limit, ProductWithInfo.class);
                } else {
                    products = productRepository.findProductsStrainAndVendor(storeToken.getCompanyId(),
                            storeToken.getShopId(),
                            strain,
                            vendorId,
                            tags,
                            "{name:1}",
                                start,
                            limit, ProductWithInfo.class);
                }

            } else if (StringUtils.isBlank(strain) && StringUtils.isBlank(vendorId) && StringUtils.isNotBlank(brandId)) {
                products = StringUtils.isBlank(term) ?
                        productRepository.findProductsByBrand(storeToken.getCompanyId(), storeToken.getShopId(), tags, brandId, "{name:1}", start, limit, ProductWithInfo.class) :
                        productRepository.findProductsByBrandSearch(storeToken.getCompanyId(), storeToken.getShopId(), term, tags, brandId, "{name:1}", start, limit, ProductWithInfo.class);

            } else if (StringUtils.isBlank(strain) && StringUtils.isNotBlank(vendorId) && StringUtils.isNotBlank(brandId)) {
                products = StringUtils.isBlank(term) ?
                        productRepository.findProductsByVendorIdBrand(storeToken.getCompanyId(), storeToken.getShopId(), vendorId, tags, brandId, "{name:1}", start, limit, ProductWithInfo.class) :
                        productRepository.findProductsByVendorIdBrandIdSearch(storeToken.getCompanyId(), storeToken.getShopId(), vendorId, term, tags, brandId, "{name:1}", start, limit, ProductWithInfo.class);

            } else if (StringUtils.isNotBlank(strain) && StringUtils.isBlank(vendorId) && StringUtils.isNotBlank(brandId)) {
                products = StringUtils.isBlank(term) ?
                        productRepository.findProductsStrainBrand(storeToken.getCompanyId(), storeToken.getShopId(), strain, tags, brandId, "{name:1}", start, limit, ProductWithInfo.class) :
                        productRepository.findProductsStrainBrandSearch(storeToken.getCompanyId(), storeToken.getShopId(), strain, term, tags, brandId, "{name:1}", start, limit, ProductWithInfo.class);

            }  else if (StringUtils.isNotBlank(strain) && StringUtils.isNotBlank(vendorId) && StringUtils.isNotBlank(brandId)) {
                products = StringUtils.isBlank(term) ?
                        productRepository.findProductsStrainAndVendorBrand(storeToken.getCompanyId(), storeToken.getShopId(), strain, vendorId, tags, brandId, "{name:1}", start, limit, ProductWithInfo.class) :
                        productRepository.findProductsStrainVendorBrandSearch(storeToken.getCompanyId(), storeToken.getShopId(), strain, vendorId, term, tags, brandId, "{name:1}", start, limit, ProductWithInfo.class);

            } else {
                if (StringUtils.isNotBlank(term)) {
                    products = productRepository.findItemsSearch(storeToken.getCompanyId(),
                            storeToken.getShopId(),
                            term,
                            tags,
                            "{name:1}", start, limit, ProductWithInfo.class);
                } else {
                    // need another version for active == true
                    if (shop.isShowProductByAvailableQuantity()) {
                        products = productRepository.getProductListInStock(storeToken.getCompanyId(),
                                storeToken.getShopId(), tags,
                                "{name:1}", start, limit, ProductWithInfo.class);
                    } else {
                        products = productRepository.getProductList(storeToken.getCompanyId(),
                                storeToken.getShopId(), tags,
                                "{name:1}", start, limit, ProductWithInfo.class);
                    }
                }
            }
        }


        List<ProductWithInfo> productResult = new ArrayList<>();
        assignedProductDependencies(products.getValues(), shop.isShowProductByAvailableQuantity(), productResult, shop);

        if (shop.isShowProductByAvailableQuantity()) {
            products.setSkip(start);
            products.setLimit(limit);
            //products.setTotal((long)productResult.size());
            products.setValues(productResult);
        }

        return products;
    }


    @Override
    public SearchResult<ProductCategory> getStoreCategories() {
        SearchResult<ProductCategory> result = new SearchResult<>();
        SearchResult<ProductCategoryResult> categories = productCategoryRepository.findItems(storeToken.getCompanyId(), storeToken.getShopId(), "{priority:1}", 0, 100, ProductCategoryResult.class);

        List<String> ids = new ArrayList<>();
        for (ProductCategoryResult productCategoryResult : categories.getValues()) {
            ids.add(productCategoryResult.getId());
        }

        Iterable<Product> products = productRepository.getActiveProductsForCategories(storeToken.getCompanyId(), ids);

        HashMap<String, Integer> productCountByCategoryId = new HashMap<>();
        for (Product product : products) {
            if (productCountByCategoryId.containsKey(product.getCategoryId())) {
                Integer i = productCountByCategoryId.get(product.getCategoryId());
                i = i + 1;
                productCountByCategoryId.put(product.getCategoryId(), i);
            } else {
                productCountByCategoryId.put(product.getCategoryId(), 1);
            }
        }

        List<ProductCategory> productCategories = new ArrayList<>();
        for (ProductCategoryResult productCategoryResult : categories.getValues()) {
            Integer count = productCountByCategoryId.get(productCategoryResult.getId());
            productCategoryResult.setCount(count);
            productCategories.add(productCategoryResult);
        }

        result.setValues(productCategories);
        return result;
    }

    @Override
    public ProductWithInfo getProductById(String productId) {
        ProductWithInfo product = productRepository.get(storeToken.getCompanyId(), productId, ProductWithInfo.class);
        if (product == null) {
            throw new BlazeInvalidArgException("Product", "Product does not exist.");
        }
        assignedProductDependencies(Lists.newArrayList(product), Boolean.FALSE, null, null);

        return product;
    }

    @Override
    public SearchResult<Product> getProductsByTerminalId(String terminalId, int start, int limit) {
        Terminal terminal = terminalRepository.get(storeToken.getCompanyId(), terminalId);
        if (terminal == null) {
            throw new BlazeInvalidArgException("Terminal", "Terminal does not exist.");
        }
        if (terminal.isDeleted()
                || terminal.isActive() == false
                || !storeToken.getShopId().equalsIgnoreCase(terminal.getShopId())) {
            throw new BlazeInvalidArgException("Terminal", "Terminal is inactive or is not assigned to shop.");
        }

        Inventory inventory = inventoryRepository.getByShopAndId(storeToken.getCompanyId(), storeToken.getShopId(), terminal.getAssignedInventoryId());
        if (inventory == null || !inventory.isActive()) {
            throw new BlazeInvalidArgException("Inventory", "Inventory is inactive or does not exist.");
        }

        HashMap<String, Integer> prepackageQuantities = productPrepackageQuantityRepository.getQuantitiesForInventoryWithQuantityProductMap(storeToken.getCompanyId(), storeToken.getShopId(), inventory.getId());

        if (prepackageQuantities.size() > 0) {
            // need to get all products then comb through them
            SearchResult<Product> productSearchResult = new SearchResult<>();
            Iterable<Product> products = productRepository.listAllByShop(storeToken.getCompanyId(), storeToken.getShopId());

            List<Product> productsList = new ArrayList<>();
            for (Product p : products) {
                boolean found = false;
                for (ProductQuantity pq : p.getQuantities()) {
                    if (pq.getInventoryId().equalsIgnoreCase(inventory.getId()) && pq.getQuantity().doubleValue() > 0) {
                        productsList.add(p);
                        found = true;
                        break;
                    }
                }

                // found (check prepackage map
                if (!found) {
                    Integer value = prepackageQuantities.get(p.getId());
                    if (value != null && value > 0) {
                        productsList.add(p);
                    }
                }
            }
            if (productsList.size() == 0) {
                return new SearchResult<Product>();
            }

            // now, we need to get a subset of this list
            if (start < 0) {
                start = 0;
            }
            if (limit <= 0 || limit > 200) {
                limit = 200;
            }
            int toIndex = start + limit;
            if (toIndex >= productsList.size()) {
                toIndex = productsList.size() - 1;
            }
            List<Product> subList = productsList.subList(start, toIndex);

            SearchResult<Product> results = new SearchResult<>();
            results.setValues(subList);
            results.setSkip(start);
            results.setLimit(limit);
            results.setTotal((long) productsList.size());
            return results;
        } else {
            return productRepository.findProductWithAvailableInventory(storeToken.getCompanyId(),
                    storeToken.getShopId(), inventory.getId(),
                    start, limit, Product.class);
        }
    }

    @Override
    public SearchResult<Product> getProductsByDates(long afterDate, long beforeDate) {
        if (afterDate < 0) {
            afterDate = 0;
        }
        if (beforeDate <= 0) {
            beforeDate = DateTime.now().getMillis();
        }
        return productRepository.findItemsWithDate(storeToken.getCompanyId(),storeToken.getShopId(),afterDate,beforeDate);
    }

    private void assignedProductDependencies(List<ProductWithInfo> products, boolean showProductByAvailableQuantity, List<ProductWithInfo> productResult, Shop shop) {
        if (products.size() == 0) {
            return;
        }

        final HashMap<String, ProductWeightTolerance> toleranceHashMap = weightToleranceRepository.listAsMap(storeToken.getCompanyId());

        List<ObjectId> vendorIds = new ArrayList<>();
        List<ObjectId> brandIds = new ArrayList<>();

        if (showProductByAvailableQuantity) {
            //If custom inventory is enabled for store then get product from
            if (shop.getOnlineStoreInfo() != null
                    && shop.getOnlineStoreInfo().isEnableInventory()
                    && StringUtils.isNotBlank(shop.getOnlineStoreInfo().getActiveInventoryId())
                    && shop.getOnlineStoreInfo().getEnableInventoryType() == OnlineStoreInfo.EnableInventoryType.Custom
                    && inventoryRepository.get(storeToken.getCompanyId(), shop.getOnlineStoreInfo().getActiveInventoryId()) != null) {
                String activeInventoryId = shop.getOnlineStoreInfo().getActiveInventoryId();
                HashMap<String, Integer> quantityProductMap = productPrepackageQuantityRepository.getQuantitiesForInventoryWithQuantityProductMap(storeToken.getCompanyId(), storeToken.getShopId(), activeInventoryId);

                for (ProductWithInfo product : products) {
                    List<ProductQuantity> quantities = product.getQuantities();
                    List<ProductQuantity> productQuantityList = new ArrayList<>();
                    for (ProductQuantity productQuantity : quantities) {
                        if (productQuantity.getQuantity().doubleValue() > 0 && productQuantity.getInventoryId().equals(activeInventoryId)) {
                            productQuantityList.add(productQuantity);
                        }
                    }

                    Integer prepackageAmt = quantityProductMap.get(product.getId());

                    // if quantity exists and prepackageAmt is greater than 0
                    if (productQuantityList.size() > 0 || (prepackageAmt != null && prepackageAmt > 0)) {

                        product.setQuantities(productQuantityList);
                        prepareProductResultInfo(product, vendorIds, brandIds, toleranceHashMap);
                        productResult.add(product);
                    }
                    if (product.getPriceBreaks() != null) {
                        for (ProductPriceBreak priceBreak : product.getPriceBreaks()) {
                            if (StringUtils.isBlank(priceBreak.getDisplayName())) {
                                priceBreak.setDisplayName(priceBreak.getName());
                            }
                        }
                    }

                }

            } else {
                //Check product quantity in all inventories
                List<String> productIds = new ArrayList<>();
                for (ProductWithInfo product : products) {
                    if (ObjectId.isValid(product.getId())) {
                        productIds.add(product.getId());
                    }
                }

                HashMap<String, Integer> quantityProductMap = productPrepackageQuantityRepository.getQuantitiesForProductsWithQuantityProductMap(storeToken.getCompanyId(), storeToken.getShopId(), productIds);

                for (ProductWithInfo p : products) {
                    List<ProductQuantity> quantities = p.getQuantities();
                    List<ProductQuantity> productQuantityList = new ArrayList<>();

                    if (quantities != null && quantities.size() > 0) {
                        for (ProductQuantity quantity : quantities) {
                            if (quantity.getQuantity().doubleValue() > 0) {
                                productQuantityList.add(quantity);
                            }
                        }
                    }

                    Integer prepackageAmt = quantityProductMap.get(p.getId());

                    // if quantity exists and prepackageAmt is greater than 0
                    if (productQuantityList.size() > 0 || (prepackageAmt != null && prepackageAmt > 0)) {
                        p.setQuantities(productQuantityList);
                        prepareProductResultInfo(p, vendorIds, brandIds, toleranceHashMap);
                        productResult.add(p);
                    }

                    if (p.getPriceBreaks() != null) {
                        for (ProductPriceBreak priceBreak : p.getPriceBreaks()) {
                            if (StringUtils.isBlank(priceBreak.getDisplayName())) {
                                priceBreak.setDisplayName(priceBreak.getName());
                            }
                        }
                    }
                }
            }

            this.assignProductInformation(vendorIds, brandIds, productResult, new ArrayList<ProductWeightTolerance>(toleranceHashMap.values()));

        } else {
            for (ProductWithInfo p : products) {
                prepareProductResultInfo(p, vendorIds, brandIds, toleranceHashMap);


                if (p.getPriceBreaks() != null) {
                    for (ProductPriceBreak priceBreak : p.getPriceBreaks()) {
                        if (StringUtils.isBlank(priceBreak.getDisplayName())) {
                            priceBreak.setDisplayName(priceBreak.getName());
                        }
                    }
                }
            }

            this.assignProductInformation(vendorIds, brandIds, products, new ArrayList<ProductWeightTolerance>(toleranceHashMap.values()));

        }
    }

    private void assignProductInformation(List<ObjectId> vendorIds, List<ObjectId> brandIds, List<ProductWithInfo> productResult, ArrayList<ProductWeightTolerance> weightTolerancesList) {
        HashMap<String, Vendor> vendorHashMap = vendorRepository.findItemsInAsMap(storeToken.getCompanyId(), vendorIds);
        HashMap<String, ProductCategory> productCategoryHashMap = productCategoryRepository.listAllAsMap(storeToken.getCompanyId(), storeToken.getShopId());
        HashMap<String, Brand> brandHashMap = brandRepository.findItemsInAsMap(storeToken.getCompanyId(), brandIds);
        HashMap<String, ProductCategory> categoryMap = productCategoryRepository.listAsMap(storeToken.getCompanyId(), storeToken.getShopId());

        for (ProductWithInfo p : productResult) {
            p.setVendor(vendorHashMap.get(p.getVendorId()));
            p.setCategory(productCategoryHashMap.get(p.getCategoryId()));
            p.setBrand(brandHashMap.get(p.getBrandId()));

            ProductCategory productCategory = categoryMap.get(p.getCategoryId());

            if (productCategory != null) {
                if (productCategory.getUnitType() == ProductCategory.UnitType.grams && !CollectionUtils.isNullOrEmpty(p.getPriceBreaks())) {
                    List<ProductPriceRange> ranges = productService.populatePriceRanges(p, p.getPriceRanges(), "default", weightTolerancesList);
                    p.setPriceRanges(ranges);
                    p.setPriceBreaks(new ArrayList<ProductPriceBreak>());
                } else if (productCategory.getUnitType() == ProductCategory.UnitType.units && !CollectionUtils.isNullOrEmpty(p.getPriceRanges())) {
                    List<ProductPriceBreak> productPriceBreaks = productService.populatePriceBreaks(p, productCategory, p.getPriceBreaks(), true, weightTolerancesList, false, Boolean.FALSE);
                    p.setPriceBreaks(productPriceBreaks);
                    p.setPriceRanges(new ArrayList<ProductPriceRange>());
                }
            }
        }
    }

    private void prepareProductResultInfo(ProductWithInfo p, List<ObjectId> vendorIds, List<ObjectId> brandIds, HashMap<String, ProductWeightTolerance> toleranceHashMap) {
        if (p.getVendorId() != null && ObjectId.isValid(p.getVendorId())) {
            vendorIds.add(new ObjectId(p.getVendorId()));
        }
        if (p.getPriceRanges() != null) {
            p.getPriceRanges().removeIf(new Predicate<ProductPriceRange>() {
                @Override
                public boolean test(ProductPriceRange productPriceRange) {
                    ProductWeightTolerance tolerance = toleranceHashMap.get(productPriceRange.getWeightToleranceId());
                    return tolerance == null || !tolerance.isEnabled() || tolerance.isDeleted();
                }
            });
        }
        if (p.getBrandId() != null && ObjectId.isValid(p.getBrandId())) {
            brandIds.add(new ObjectId(p.getBrandId()));
        }
    }

    @Override
    public SearchResult<ProductWithInfo> getAllProductsByDates(String categoryId, String strain, List<String> productTags, String vendorId, long afterDate, long beforeDate) {
        if (beforeDate == 0) {
            beforeDate = DateTime.now().getMillis();
        }
        LinkedHashSet<String> tags = new LinkedHashSet<>();
        if (productTags != null && !productTags.isEmpty()) {
            for (String tag : productTags) {
                tags.add(tag);
            }
        }
        DateSearchResult<ProductWithInfo> searchResult = new DateSearchResult<>();
        /*if(BLStringUtils.isNotBlank(categoryId)){
            ProductCategory category = productCategoryRepository.getByShopAndId(storeToken.getCompanyId(),storeToken.getShopId(),categoryId);
            if(category == null){
                throw new BlazeInvalidArgException("Product","Product category doesn't found");
            }
            if(BLStringUtils.isBlank(strain) && BLStringUtils.isBlank(vendorId)){
                searchResult = productRepository.findProductsByTagsAndCategory(storeToken.getCompanyId(),storeToken.getShopId(),categoryId,tags,afterDate,beforeDate,ProductWithInfo.class);
            }else if(BLStringUtils.isNotBlank(strain) && BLStringUtils.isNotBlank(vendorId)){
                searchResult = productRepository.findProductsByStrainAndVendor(storeToken.getCompanyId(),storeToken.getShopId(),categoryId,vendorId,strain,tags,afterDate,beforeDate,ProductWithInfo.class);
            }else if(BLStringUtils.isNotBlank(vendorId)){
                searchResult = productRepository.findProductsByCategoryAndVendorId(storeToken.getCompanyId(),storeToken.getShopId(),categoryId,vendorId,tags,afterDate,beforeDate,ProductWithInfo.class);
            }else {
                searchResult = productRepository.findProductsByCategoryAndStrain(storeToken.getCompanyId(),storeToken.getShopId(),categoryId,strain,tags,afterDate,beforeDate,ProductWithInfo.class);
            }
        }else{
            if(BLStringUtils.isBlank(strain) && BLStringUtils.isBlank(vendorId)){
                searchResult = productRepository.findProductsByTags(storeToken.getCompanyId(),storeToken.getShopId(),tags,afterDate,beforeDate,ProductWithInfo.class);
            }else if(BLStringUtils.isNotBlank(strain) && BLStringUtils.isNotBlank(vendorId)){
                searchResult = productRepository.findProductsByStrainAndVendor(storeToken.getCompanyId(),storeToken.getShopId(),strain,vendorId,tags,afterDate,beforeDate,ProductWithInfo.class);
            }else if(BLStringUtils.isNotBlank(vendorId)){
                searchResult = productRepository.findProductsByTagsAndVendor(storeToken.getCompanyId(),storeToken.getShopId(),vendorId,tags,afterDate,beforeDate,ProductWithInfo.class);
            }else {
                searchResult = productRepository.findProductsByTagsAndStrain(storeToken.getCompanyId(),storeToken.getShopId(),strain,tags,afterDate,beforeDate,ProductWithInfo.class);
            }
        }(*/
        searchResult = productRepository.findItemsWithDate(storeToken.getCompanyId(), storeToken.getShopId(), afterDate, beforeDate, ProductWithInfo.class);


        Shop shop = shopRepository.get(storeToken.getCompanyId(), storeToken.getShopId());

        List<ProductWithInfo> productResult = new ArrayList<>();
        assignedProductDependencies(searchResult.getValues(), false, productResult, shop);

        if (shop.isShowProductByAvailableQuantity()) {
            searchResult.setAfterDate(afterDate);
            searchResult.setBeforeDate(beforeDate);
            searchResult.setTotal(searchResult.getTotal());
            searchResult.setValues(productResult);
        }

        return searchResult;
    }

    @Override
    public SearchResult<ProductBatch> getAllBatches(String batchId, String productId, ProductBatch.BatchStatus status, int start, int limit, String term) {
        SearchResult<ProductBatch> result = new SearchResult<>();
        if (limit > 100 || limit <= 0) {
            limit = 100;
        }
        if (start < 0) {
            start = 0;
        }
        if (StringUtils.isNotBlank(batchId)) {
            ProductBatch productBatch = productBatchRepository.getByShopAndId(storeToken.getCompanyId(), storeToken.getShopId(), batchId);
            result.setTotal(1L);
            result.setSkip(0);
            result.setLimit(0);
            result.setValues(Lists.newArrayList(productBatch));
        } else if (StringUtils.isBlank(term)) {
            if (StringUtils.isNotBlank(productId)) {
                result = (status == null) ?
                        productBatchRepository.getBatches(storeToken.getCompanyId(), storeToken.getShopId(), productId, start, limit, "{modified:-1}", ProductBatch.class) :
                        productBatchRepository.getBatchesProductByBatchStatus(storeToken.getCompanyId(), storeToken.getShopId(), productId, start, limit, "{modified:-1}", status);
            } else {
                result = (status == null) ?
                        productBatchRepository.getBatches(storeToken.getCompanyId(), storeToken.getShopId(), start, limit) :
                        productBatchRepository.getAllProductBatchesByStatus(storeToken.getCompanyId(), storeToken.getShopId(), status, "{modified:-1}", start, limit, ProductBatch.class);
            }
        } else {
            result = (status == null) ?
                    productBatchRepository.getBatchesByTerm(storeToken.getCompanyId(), storeToken.getShopId(), start, limit, "{modified:-1}", term) :
                    productBatchRepository.getAllProductBatchesByTermWithStatus(storeToken.getCompanyId(), storeToken.getShopId(), status, "{modified:-1}", start, limit, term, ProductBatch.class);
        }

        List<ObjectId> purchaseOrderIds = result.getValues().stream()
                .filter(productBatch -> StringUtils.isNotBlank(productBatch.getPoNumber()) && ObjectId.isValid(productBatch.getPoNumber()))
                .map(productBatch -> new ObjectId(productBatch.getPoNumber())).collect(Collectors.toList());

        Iterable<PurchaseOrder> purchaseOrders = purchaseOrderRepository.getAllPurchaseOrderById(storeToken.getCompanyId(), storeToken.getShopId(), purchaseOrderIds);
        Map<String, String> poNumberMap = new HashMap<>();

        for (PurchaseOrder purchaseOrder : purchaseOrders)
            if (!poNumberMap.containsKey(purchaseOrder.getId())) {
                poNumberMap.put(purchaseOrder.getId(), purchaseOrder.getPoNumber());
            }

        for (ProductBatch productBatch : result.getValues()) {
            if (productBatch != null && StringUtils.isNotBlank(productBatch.getPoNumber())) {
                if (poNumberMap.containsKey(productBatch.getPoNumber())) {
                    productBatch.setPoNumber(poNumberMap.get(productBatch.getPoNumber()));
                }
            }
        }
        return result;
    }

    @Override
    public SearchResult<ProductBatch> getAllBatches(long afterDate, long beforeDate, int start, int limit) {
        if (afterDate < 0) {
            afterDate = 0;
        }
        if (beforeDate <= 0) {
            beforeDate = DateTime.now().getMillis();
        }

        if (start < 0) {
            start = 0;
        }
        if (limit > 100 || limit <= 0) {
            limit = 100;
        }
        return productBatchRepository.findItemsWithDateAndLimitSorted(storeToken.getCompanyId(),storeToken.getShopId(),afterDate,beforeDate,start,limit);
    }

    @Override
    public List<BatchQuantity> getQuantityInfo(String productId) {
        return batchQuantityRepository.getBatchQuantities(storeToken.getCompanyId(), storeToken.getShopId(), productId);
    }

    @Override
    public SearchResult<BrandResult> getAllBrands(String term, int start, int limit) {
        if (limit > 100 || limit <= 0) {
            limit = 100;
        }
        if (start < 0) {
            start = 0;
        }
        SearchResult<BrandResult> result = (StringUtils.isNotBlank(term)) ?
                brandRepository.findBrandByStatusAndTerm(storeToken.getCompanyId(), true, term, start, limit, BrandResult.class) :
                brandRepository.getAllBrandByStatus(storeToken.getCompanyId(), true, start, limit, "{modified:-1}", BrandResult.class);

        if (!result.getValues().isEmpty()) {
            prepareBrandResult(result.getValues());
        }

        return result;
    }

    private void prepareBrandResult(List<BrandResult> result) {
        if (result.isEmpty()) {
            return;
        }
        Set<ObjectId> vendorIds = new HashSet<>();

        for (BrandResult brandResult : result) {
            if (brandResult.getVendorList() != null) {
                for (String vendorId : brandResult.getVendorList()) {
                    if (ObjectId.isValid(vendorId)) {
                        vendorIds.add(new ObjectId(vendorId));
                    }
                }
            }
        }

        HashMap<String, Vendor> vendorHashMap = vendorRepository.listAsMap(storeToken.getCompanyId(), new ArrayList<>(vendorIds));

        for (BrandResult brandResult : result) {
            List<Vendor> vendorList = new ArrayList<>();
            if (brandResult.getVendorList() != null) {
                for (String vendorId : brandResult.getVendorList()) {
                    Vendor vendor = vendorHashMap.get(vendorId);
                    if (vendor != null) {
                        vendorList.add(vendor);
                    }
                }
            }
            brandResult.setVendors(vendorList);
        }

    }

    @Override
    public InventoryTransferHistory addTransferHistory(PartnerInventoryTransferHistory request) {
        return commonInventoryService.bulkInventoryTransfer(storeToken.getCompanyId(), storeToken.getShopId(), request.getCurrentEmployeeId(), request);
    }

    @Override
    public InventoryTransferHistory updateTransferHistory(String historyId, PartnerInventoryTransferHistory request) {
        return commonInventoryService.updateInventoryTransfer(historyId, storeToken.getCompanyId(), storeToken.getShopId(), request.getCurrentEmployeeId(), request);
    }

    @Override
    public InventoryTransferHistory updateInventoryTransferStatus(String historyId, PartnerInventoryTransferStatusRequest request, boolean isAccept) {
        InventoryTransferStatusRequest updateRequest = new InventoryTransferStatusRequest();
        updateRequest.setStatus(isAccept);
        return commonInventoryService.updateInventoryTransferHistoryStatus(historyId, storeToken.getCompanyId(), storeToken.getShopId(), request.getCurrentEmployeeId(), updateRequest);
    }
}
