package com.fourtwenty.core.services.store.impl;

import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrder;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.rest.dispensary.requests.partners.purchaseorder.PartnerPurchaseOrderAddRequest;
import com.fourtwenty.core.rest.dispensary.requests.partners.purchaseorder.PartnerPurchaseOrderUpdateRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.PurchaseOrderItemResult;
import com.fourtwenty.core.security.tokens.StoreAuthToken;
import com.fourtwenty.core.services.AbstractStoreServiceImpl;
import com.fourtwenty.core.services.partners.CommonPurchaseOrderService;
import com.fourtwenty.core.services.store.StorePurchaseOrderService;
import com.fourtwenty.core.util.DateUtil;
import com.google.inject.Inject;
import com.google.inject.Provider;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class StorePurchaseOrderServiceImpl extends AbstractStoreServiceImpl implements StorePurchaseOrderService {

    private static final String PURCHASE_ORDER = "Purchase Order";
    private static final String CURRENT_EMPLOYEE_BLANK = "Current employee id cannot be blank.";
    @Inject
    private CommonPurchaseOrderService commonPurchaseOrderService;
    @Inject
    private ShopRepository shopRepository;

    @Inject
    public StorePurchaseOrderServiceImpl(Provider<StoreAuthToken> tokenProvider) {
        super(tokenProvider);
    }

    @Override
    public PurchaseOrder addPurchaseOrder(PartnerPurchaseOrderAddRequest request) {
        if (StringUtils.isBlank(request.getCurrentEmployeeId())) {
            throw new BlazeInvalidArgException(PURCHASE_ORDER, CURRENT_EMPLOYEE_BLANK);
        }
        return commonPurchaseOrderService.addPurchaseOrder(storeToken.getCompanyId(), storeToken.getShopId(), request.getCurrentEmployeeId(), request);
    }

    @Override
    public PurchaseOrder preparePurchaseOrder(PartnerPurchaseOrderAddRequest request) {
        if (StringUtils.isBlank(request.getCurrentEmployeeId())) {
            throw new BlazeInvalidArgException(PURCHASE_ORDER, CURRENT_EMPLOYEE_BLANK);
        }
        return commonPurchaseOrderService.preparePurchaseOrder(storeToken.getCompanyId(), storeToken.getShopId(), request.getCurrentEmployeeId(), request);
    }

    @Override
    public PurchaseOrder updatePurchaseOrder(String purchaseOrderId, PartnerPurchaseOrderUpdateRequest request) {
        if (StringUtils.isBlank(request.getCurrentEmployeeId())) {
            throw new BlazeInvalidArgException(PURCHASE_ORDER, CURRENT_EMPLOYEE_BLANK);
        }
        return commonPurchaseOrderService.updatePurchaseOrder(storeToken.getCompanyId(), storeToken.getShopId(), request.getCurrentEmployeeId(), purchaseOrderId, request);
    }

    @Override
    public PurchaseOrderItemResult getPurchaseOrderById(String purchaseOrderId) {
        return commonPurchaseOrderService.getPurchaseOrderById(storeToken.getCompanyId(), storeToken.getShopId(), purchaseOrderId);
    }

    @Override
    public SearchResult<PurchaseOrderItemResult> getAllPurchaseOrder(String startDate, String endDate, int start, int limit) {
        limit = (limit == 0) ? 200 : limit;
        Shop shop = shopRepository.getById(storeToken.getShopId());

        if (shop == null) {
            throw new BlazeInvalidArgException(PURCHASE_ORDER, "Shop does not exists.");
        }

        if (limit <= 0 || limit > 200) {
            limit = 200;
        }

        if (start < 0) {
            start = 0;
        }

        DateTimeFormatter formatter = DateTimeFormat.forPattern("MM/dd/yyyy");

        long timeZoneStartDateMillis = 0;
        long timeZoneEndDateMillis = DateTime.now().getMillis();

        if (StringUtils.isNotBlank(endDate) || StringUtils.isNotBlank(startDate)) {
            DateTime endDateTime;
            if (StringUtils.isNotBlank(endDate)) {
                endDateTime = formatter.parseDateTime(endDate).plusDays(1).minusSeconds(1);
            } else {
                endDateTime = DateUtil.nowUTC().plusDays(1).minusSeconds(1);
            }

            DateTime startDateTime;
            if (StringUtils.isNotBlank(startDate)) {
                startDateTime = formatter.parseDateTime(startDate).withTimeAtStartOfDay();
            } else {
                startDateTime = DateUtil.nowUTC().withTimeAtStartOfDay();
            }
            String timeZone = shop.getTimeZone();
            int timeZoneOffset = DateUtil.getTimezoneOffsetInMinutes(timeZone);

            timeZoneStartDateMillis = startDateTime.minusMinutes(timeZoneOffset).getMillis();
            timeZoneEndDateMillis = endDateTime.minusMinutes(timeZoneOffset).getMillis();
        }

        return commonPurchaseOrderService.getAllPurchaseOrderByDates(storeToken.getCompanyId(), storeToken.getShopId(), timeZoneStartDateMillis, timeZoneEndDateMillis, start, limit);
    }
}
