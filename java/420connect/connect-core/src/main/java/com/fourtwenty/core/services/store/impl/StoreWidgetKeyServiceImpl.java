package com.fourtwenty.core.services.store.impl;

import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.store.StoreWidgetKey;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.domain.repositories.store.StoreWidgetKeyRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.store.StoreWidgetKeyService;
import com.fourtwenty.core.util.SecurityUtil;
import com.google.inject.Provider;

import javax.inject.Inject;

/**
 * Created by mdo on 4/17/17.
 */
public class StoreWidgetKeyServiceImpl extends AbstractAuthServiceImpl implements StoreWidgetKeyService {
    @Inject
    StoreWidgetKeyRepository keyRepository;
    @Inject
    SecurityUtil securityUtil;
    @Inject
    ShopRepository shopRepository;

    @Inject
    public StoreWidgetKeyServiceImpl(Provider<ConnectAuthToken> tokenProvider) {
        super(tokenProvider);
    }

    @Override
    public StoreWidgetKey createStoreKey() {

        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());
        if (shop == null) {
            throw new BlazeInvalidArgException("DeveloperKeys", "Shop does not exist.");
        }

        // delete all previous key
        keyRepository.deleteAll(token.getCompanyId(), shop.getId());

        StoreWidgetKey storeKey = new StoreWidgetKey();
        storeKey.prepare(token.getCompanyId());
        storeKey.setShopId(token.getShopId());
        storeKey.setActive(true);

        String key = securityUtil.generateKey();
        String secret = securityUtil.generateSecret();
        storeKey.setKey(key);
        storeKey.setSecret(secret);

        keyRepository.save(storeKey);
        return storeKey;
    }

    @Override
    public StoreWidgetKey getCurrentStoreKey() {
        return keyRepository.getStoreKeyByActiveShop(token.getCompanyId(), token.getShopId());
    }

    @Override
    public SearchResult<StoreWidgetKey> getStoreKeys() {
        return keyRepository.getActiveKeys(token.getCompanyId());
    }
}
