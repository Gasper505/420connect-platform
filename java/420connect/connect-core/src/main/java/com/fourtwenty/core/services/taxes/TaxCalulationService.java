package com.fourtwenty.core.services.taxes;

import com.fourtwenty.core.domain.models.company.CultivationTaxInfo;
import com.fourtwenty.core.domain.models.company.ExciseTaxInfo;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductBatch;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.domain.models.product.ProductWeightTolerance;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrder;
import com.fourtwenty.core.domain.models.transaction.CultivationTaxResult;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public interface TaxCalulationService {
    CultivationTaxResult calculateOzTax(String companyId, String shopId, Product product, ProductCategory productCategory, double requestQuantity, ProductBatch productBatch);

    HashMap<String, BigDecimal> calculateExciseTax(String companyId, String shopId, BigDecimal quantity, BigDecimal costPerUnit, Vendor.ArmsLengthType armsLength, boolean isCannabis);

    CultivationTaxResult calculateOzTax(String companyId, String shopId, PurchaseOrder purchaseOrder, Map<String, Product> productMap, HashMap<String, ProductCategory> categoryHashMap, boolean isPurchaseOrder);

    BigDecimal calculateCultivationTax(double quantity, Product product, ProductCategory productCategory, CultivationTaxResult cultivationTaxResult, CultivationTaxInfo cultivationTaxInfo);

    HashMap<String, BigDecimal> calculateExciseTax(ExciseTaxInfo exciseTaxInfo, BigDecimal quantity, BigDecimal costPerUnit);


    BigDecimal calculateEstimateCultivationTax(double quantity, Product product, ProductCategory productCategory, CultivationTaxResult cultivationTaxResult, CultivationTaxInfo cultivationTaxInfo, HashMap<String, ProductBatch> allBatchMap, HashMap<String, ProductWeightTolerance> weightToleranceHashMap, String batchId);
}
