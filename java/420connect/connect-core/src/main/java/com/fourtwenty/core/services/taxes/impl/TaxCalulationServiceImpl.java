package com.fourtwenty.core.services.taxes.impl;

import com.fourtwenty.core.domain.models.company.*;
import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductBatch;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.domain.models.product.ProductWeightTolerance;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.models.purchaseorder.POProductRequest;
import com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrder;
import com.fourtwenty.core.domain.models.transaction.CultivationTaxResult;
import com.fourtwenty.core.domain.repositories.dispensary.CultivationTaxInfoRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ExciseTaxInfoRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.services.taxes.TaxCalulationService;
import com.fourtwenty.core.util.MeasurementConversionUtil;
import com.fourtwenty.core.util.NumberUtils;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class TaxCalulationServiceImpl implements TaxCalulationService {

    @Inject
    private ExciseTaxInfoRepository exciseTaxInfoRepository;
    @Inject
    private CultivationTaxInfoRepository cultivationTaxInfoRepository;
    @Inject
    private ShopRepository shopRepository;

    /** Override method to calculate oz tax for batches
     *
     * @param companyId       :  companyId
     * @param shopId          :  shopId
     * @param product         :  product
     * @param productCategory :  productCategory
     * @param requestQuantity :  requestQuantity
     * @return
     */
    @Override
    public CultivationTaxResult calculateOzTax(String companyId, String shopId, Product product, ProductCategory productCategory, double requestQuantity, ProductBatch productBatch) {
        Shop shop = shopRepository.get(companyId, shopId);
        // Default values
        String state = "CA";
        String country = "US";
        Address address = shop.getAddress();
        if (address != null) {
            state = address.getState();
            country = address.getCountry();
        }

        double moistureLossPercent = 0;

        if (productBatch != null && productBatch.isUnProcessed() && product.getCannabisType().equals(Product.CannabisType.PLANT)) {
            moistureLossPercent = productBatch.getMoistureLoss();
            if (moistureLossPercent > 0 && requestQuantity > 0) {
                requestQuantity = NumberUtils.round(requestQuantity / (1 - moistureLossPercent), 2);
            }
        }

        final CultivationTaxResult cultivationTaxResult = new CultivationTaxResult();
        CultivationTaxInfo cultivationTaxInfo =  cultivationTaxInfoRepository.getCultivationTaxInfoByState(state, country);
        BigDecimal totalCultivationTax = BigDecimal.ZERO;

        if (cultivationTaxInfo != null) {
            if (product != null && productCategory != null && (product.getCannabisType() == Product.CannabisType.DRY_FLOWER || product.getCannabisType() == Product.CannabisType.DRY_LEAF || (product.getCannabisType() == Product.CannabisType.PLANT && shop.getAppTarget()== CompanyFeatures.AppTarget.Grow))) {
                totalCultivationTax = totalCultivationTax.add(calculateCultivationTax(requestQuantity, product, productCategory, cultivationTaxResult, cultivationTaxInfo));
            }


        }
        return cultivationTaxResult;
    }

    /**
     * Override method to calculate excise taxes
     *
     * @param companyId    : companyId
     * @param shopId       : shopId
     * @param quantity     : quantity
     * @param costPerUnit  : costPerUnit
     * @param isCannabis   : cannabisTax
     * @return
     */
    @Override
    public HashMap<String, BigDecimal> calculateExciseTax(String companyId, String shopId, BigDecimal quantity, BigDecimal costPerUnit, Vendor.ArmsLengthType armsLength, boolean isCannabis) {
        Shop shop = shopRepository.get(companyId, shopId);

        String state = "";
        String country = "";
        Address address = shop.getAddress();
        if (address != null) {
            state = address.getState();
            country = address.getCountry();
        }

        ExciseTaxInfo exciseTaxInfo = exciseTaxInfoRepository.getExciseTaxInfoByState(state, country, isCannabis ? ExciseTaxInfo.CannabisTaxType.Cannabis : ExciseTaxInfo.CannabisTaxType.NonCannabis);

        HashMap<String, BigDecimal> exciseTaxMap = new HashMap<>();
        if (armsLength == Vendor.ArmsLengthType.ARMS_LENGTH) {
            exciseTaxMap = calculateExciseTax(exciseTaxInfo, quantity, costPerUnit);
        }
        return exciseTaxMap;
    }

    @Override
    public HashMap<String, BigDecimal> calculateExciseTax(ExciseTaxInfo exciseTaxInfo, BigDecimal quantity, BigDecimal costPerUnit) {

        if (costPerUnit == null) {
            costPerUnit = new BigDecimal(0);
        }

        HashMap<String, BigDecimal> returnInfo = new HashMap<>();

        double totalExciseTax = 0;
        double perUnitExciseTax = 0;

        if (exciseTaxInfo != null) {
            double stateMarkUp = exciseTaxInfo.getStateMarkUp().doubleValue();
            stateMarkUp = 1 + (stateMarkUp / 100);

            BigDecimal exciseTax = exciseTaxInfo.getExciseTax();
            perUnitExciseTax = (costPerUnit.doubleValue() * stateMarkUp) * exciseTax.doubleValue() / 100;


            totalExciseTax = perUnitExciseTax * quantity.doubleValue();

        }

        returnInfo.put("totalExciseTax", BigDecimal.valueOf(totalExciseTax));
        returnInfo.put("unitExciseTax", BigDecimal.valueOf(perUnitExciseTax));

        return returnInfo;
    }

    /**
     * Override method to calculate oz taxes for purchase orders
     *
     * @param companyId       : companyId
     * @param shopId          : shopId
     * @param purchaseOrder   : purchaseOrder
     * @param productMap      : productMap
     * @param categoryHashMap : categoryHashMap
     * @param isPurchaseOrder : isPurchaseOrder
     * @return
     */
    @Override
    public CultivationTaxResult calculateOzTax(String companyId, String shopId, PurchaseOrder purchaseOrder, Map<String, Product> productMap, HashMap<String, ProductCategory> categoryHashMap, boolean isPurchaseOrder) {
        CultivationTaxInfo cultivationTaxInfo = getCultivationTaxInfoForShop(companyId, shopId);
        BigDecimal totalCultivationTax = BigDecimal.ZERO;

        final CultivationTaxResult cultivationTaxResult = new CultivationTaxResult();
        if (cultivationTaxInfo != null) {

            for (POProductRequest poProductRequest : purchaseOrder.getPoProductRequestList()) {
                CultivationTaxResult poProductRequestTaxResult = new CultivationTaxResult();
                Product product = productMap.get(poProductRequest.getProductId());
                poProductRequest.setTotalCultivationTax(new BigDecimal(0));
                if (product != null && categoryHashMap.containsKey(product.getCategoryId())) {
                    ProductCategory productCategory = categoryHashMap.get(product.getCategoryId());
                    Shop shop = shopRepository.get(companyId,shopId);
                    if (productCategory != null && product.getCannabisType() == Product.CannabisType.DRY_FLOWER || product.getCannabisType() == Product.CannabisType.DRY_LEAF || (product.getCannabisType() == Product.CannabisType.PLANT && shop.getAppTarget()== CompanyFeatures.AppTarget.Grow)) {
                        double quantity = 0;
                        if (isPurchaseOrder) {
                            quantity = poProductRequest.getRequestQuantity().doubleValue();
                        } else {
                            quantity = poProductRequest.getReceivedQuantity().doubleValue();
                        }

                        poProductRequest.setTotalCultivationTax(calculateCultivationTax(quantity, product, productCategory, poProductRequestTaxResult, cultivationTaxInfo));
                        cultivationTaxResult.addTaxes(poProductRequestTaxResult);
                        totalCultivationTax = totalCultivationTax.add(poProductRequest.getTotalCultivationTax());
                    }
                }
            }


        }
        return cultivationTaxResult;
    }

    /**
     * Private method to get cultivation tax info
     *
     * @param companyId : companyId
     * @param shopId    : shopId
     * @return : CultivationTaxInfo
     */
    private CultivationTaxInfo getCultivationTaxInfoForShop(String companyId, String shopId) {
        Shop shop = shopRepository.get(companyId, shopId);
        // Default values
        String state = "CA";
        String country = "US";
        Address address = shop.getAddress();
        if (address != null) {
            state = address.getState();
            country = address.getCountry();
        }

        return cultivationTaxInfoRepository.getCultivationTaxInfoByState(state, country);
    }

    /** Override method to calculate taxes for according to category cannabis type
     *
     * @param quantity             : quantity
     * @param product              : product
     * @param productCategory      : productCategory
     * @param cultivationTaxResult : cultivationTaxResult
     * @param cultivationTaxInfo   : cultivationTaxInfo
     * @return
     */
    @Override
    public BigDecimal calculateCultivationTax(double quantity, Product product, ProductCategory productCategory, CultivationTaxResult cultivationTaxResult, CultivationTaxInfo cultivationTaxInfo) {
        double ounceQuantity = MeasurementConversionUtil.ounceQuantityCalculation(productCategory, product, quantity);

        BigDecimal leavesCultivationTax;
        BigDecimal flowerCultivationTax;
        BigDecimal plantCultivationTax;

        BigDecimal totalFlowersTax = cultivationTaxResult.getTotalFlowerTax();
        BigDecimal totalFlowersOz = cultivationTaxResult.getTotalFlowerOz();
        BigDecimal totalLeafTax = cultivationTaxResult.getTotalLeafTax();
        BigDecimal totalLeafOz = cultivationTaxResult.getTotalLeafOz();
        BigDecimal totalPlantTax = cultivationTaxResult.getTotalPlantTax();
        BigDecimal totalPlantOz = cultivationTaxResult.getTotalPlantOz();

        BigDecimal leavesTax = cultivationTaxInfo.getLeavesTax();
        BigDecimal flowersTax = cultivationTaxInfo.getFlowersTax();
        BigDecimal plantTax = cultivationTaxInfo.getPlantTax();

        if (product.getCannabisType() == Product.CannabisType.DRY_LEAF) {
            leavesCultivationTax = BigDecimal.valueOf(ounceQuantity).multiply(leavesTax);
            totalLeafTax = totalLeafTax.add(leavesCultivationTax);
            totalLeafOz = totalLeafOz.add(BigDecimal.valueOf(ounceQuantity));
        } else if (product.getCannabisType() == Product.CannabisType.DRY_FLOWER) {
            flowerCultivationTax = BigDecimal.valueOf(ounceQuantity).multiply(flowersTax);
            totalFlowersTax = totalFlowersTax.add(flowerCultivationTax);
            totalFlowersOz = totalFlowersOz.add(BigDecimal.valueOf(ounceQuantity));
        } else if (product.getCannabisType() == Product.CannabisType.PLANT) {
            plantCultivationTax = BigDecimal.valueOf(ounceQuantity).multiply(plantTax);
            totalPlantTax = totalPlantTax.add(plantCultivationTax);
            totalPlantOz = totalPlantOz.add(BigDecimal.valueOf(ounceQuantity));
        }

        cultivationTaxResult.setTotalLeafTax(totalLeafTax);
        cultivationTaxResult.setTotalLeafOz(totalLeafOz);
        cultivationTaxResult.setLeafTaxOz((totalLeafOz.doubleValue() != 0 ? BigDecimal.valueOf(totalLeafTax.doubleValue() / totalLeafOz.doubleValue()) : BigDecimal.ZERO));

        cultivationTaxResult.setTotalFlowerTax(totalFlowersTax);
        cultivationTaxResult.setTotalFlowerOz(totalFlowersOz);
        cultivationTaxResult.setFlowerTaxOz((totalFlowersOz.doubleValue() != 0 ? BigDecimal.valueOf(totalFlowersTax.doubleValue() / totalFlowersOz.doubleValue()) : BigDecimal.ZERO));

        cultivationTaxResult.setTotalPlantTax(totalPlantTax);
        cultivationTaxResult.setTotalPlantOz(totalPlantOz);
        cultivationTaxResult.setPlantTaxOz(totalPlantOz.doubleValue() != 0 ? BigDecimal.valueOf(totalPlantTax.doubleValue() / totalPlantOz.doubleValue()) : BigDecimal.ZERO);

        cultivationTaxResult.setTotalCultivationTax(totalFlowersTax.add(totalLeafTax).add(totalPlantTax));

        return cultivationTaxResult.getTotalCultivationTax();

    }

    /**
     * Override method to calculate cultivation tax according to weight per unit for requested quantity.
     *
     * @param quantity               : quantity
     * @param product                : product
     * @param productCategory        : productCategory
     * @param cultivationTaxResult   : cultivationTaxResult
     * @param cultivationTaxInfo     : cultivationTaxInfo
     * @param allBatchMap            : allBatchMap
     * @param weightToleranceHashMap : weightToleranceHashMap
     * @param batchId                : batchId
     * @return
     */
    @Override
    public BigDecimal calculateEstimateCultivationTax(double quantity, Product product, ProductCategory productCategory, CultivationTaxResult cultivationTaxResult, CultivationTaxInfo cultivationTaxInfo, HashMap<String, ProductBatch> allBatchMap, HashMap<String, ProductWeightTolerance> weightToleranceHashMap, String batchId) {
        List<ProductBatch> batches;
        if (StringUtils.isNotBlank(batchId)) {
            batches = Lists.newArrayList(allBatchMap.get(batchId));
        } else {
            batches = allBatchMap.values().stream().filter(productBatch -> (product.getId().equals(productBatch.getProductId()))).collect(Collectors.toList());
        }

        BigDecimal averageWeightValue = BigDecimal.ONE;
        double averageMoistureLoss = 0d;

        if (!batches.isEmpty()) {
            averageWeightValue = BigDecimal.ZERO;
            for (ProductBatch productBatch : batches) {
                BigDecimal weightValue = ProductWeightTolerance.WeightKey.UNIT.weightValue;
                double moistureLoss = 0;
                if (productBatch.getActualWeightPerUnit() != null) {
                    weightValue = productBatch.getActualWeightPerUnit();
                }
                if (productBatch.getMoistureLoss() > 0) {
                    moistureLoss = productBatch.getMoistureLoss();
                }
                averageWeightValue = averageWeightValue.add(weightValue);
                averageMoistureLoss += moistureLoss;
            }

            averageWeightValue = averageWeightValue.divide(BigDecimal.valueOf(batches.size()),4, RoundingMode.HALF_EVEN);
            averageMoistureLoss = averageMoistureLoss / (batches.size() * 100);
        }
        double convertedQuantity = quantity * averageWeightValue.doubleValue();
        if (product.getCannabisType().equals(Product.CannabisType.PLANT)) {
            convertedQuantity = convertedQuantity / (1 - averageMoistureLoss);
        }
        return calculateCultivationTax(convertedQuantity, product, productCategory, cultivationTaxResult, cultivationTaxInfo);
    }
}
