package com.fourtwenty.core.services.test.impl;

import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.managed.AmazonServiceManager;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.common.BackgroundJobService;
import com.fourtwenty.core.services.test.SMSTestService;
import com.fourtwenty.core.services.thirdparty.GoogleAPIService;
import com.fourtwenty.core.services.thirdparty.models.GoogleShortURLResult;
import com.google.common.collect.Lists;
import com.google.inject.Provider;

import javax.inject.Inject;

/**
 * Created by mdo on 6/20/17.
 */
public class SMSTestServiceImpl extends AbstractAuthServiceImpl implements SMSTestService {
    @Inject
    BackgroundJobService backgroundJobService;
    @Inject
    AmazonServiceManager amazonServiceManager;
    @Inject
    GoogleAPIService apiService;
    @Inject
    ShopRepository shopRepository;

    @Inject
    public SMSTestServiceImpl(Provider<ConnectAuthToken> tokenProvider) {
        super(tokenProvider);
    }

    @Override
    public void test() {
        GoogleShortURLResult result = apiService.requestShortUrl("https://app.blaze.me");
        Shop shop = shopRepository.getById(token.getShopId());
        amazonServiceManager.sendSMSMessage(Lists.newArrayList("15592307500"),
                String.format("Hello world: %s", result.getId()), shop);
    }
}
