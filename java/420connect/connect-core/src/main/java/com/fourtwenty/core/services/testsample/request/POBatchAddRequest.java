package com.fourtwenty.core.services.testsample.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.math.BigDecimal;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class POBatchAddRequest {
    private String batchSku;
    private BigDecimal quantity;
    private String testId;
    private String testingCompanyId;
    private Long dateSent = 0L;
    private String trackHarvestBatchId;
    private String trackHarvestBatchDate;
    private long batchExpirationDate;
    private String metrcLabel;
    private long sellBy;
    private String batchId;
    private List<POBundleBatchAddRequest> bundleItemsBatches;
    private String connectedBatchId;

    public String getBatchSku() {
        return batchSku;
    }

    public void setBatchSku(String batchSku) {
        this.batchSku = batchSku;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public String getTestId() {
        return testId;
    }

    public void setTestId(String testId) {
        this.testId = testId;
    }

    public String getTestingCompanyId() {
        return testingCompanyId;
    }

    public void setTestingCompanyId(String testingCompanyId) {
        this.testingCompanyId = testingCompanyId;
    }

    public Long getDateSent() {
        return dateSent;
    }

    public void setDateSent(Long dateSent) {
        this.dateSent = dateSent;
    }

    public String getTrackHarvestBatchId() {
        return trackHarvestBatchId;
    }

    public void setTrackHarvestBatchId(String trackHarvestBatchId) {
        this.trackHarvestBatchId = trackHarvestBatchId;
    }

    public String getTrackHarvestBatchDate() {
        return trackHarvestBatchDate;
    }

    public void setTrackHarvestBatchDate(String trackHarvestBatchDate) {
        this.trackHarvestBatchDate = trackHarvestBatchDate;
    }

    public long getBatchExpirationDate() {
        return batchExpirationDate;
    }

    public void setBatchExpirationDate(long batchExpirationDate) {
        this.batchExpirationDate = batchExpirationDate;
    }

    public String getMetrcLabel() {
        return metrcLabel;
    }

    public void setMetrcLabel(String metrcLabel) {
        this.metrcLabel = metrcLabel;
    }

    public long getSellBy() {
        return sellBy;
    }

    public void setSellBy(long sellBy) {
        this.sellBy = sellBy;
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public List<POBundleBatchAddRequest> getBundleItemsBatches() {
        return bundleItemsBatches;
    }

    public void setBundleItemsBatches(List<POBundleBatchAddRequest> bundleItemsBatches) {
        this.bundleItemsBatches = bundleItemsBatches;
    }

    public String getConnectedBatchId() {
        return connectedBatchId;
    }

    public void setConnectedBatchId(String connectedBatchId) {
        this.connectedBatchId = connectedBatchId;
    }

}


