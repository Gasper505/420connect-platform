package com.fourtwenty.core.services.testsample.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.models.testsample.TestResult;
import com.fourtwenty.core.domain.models.testsample.TestSample;
import com.fourtwenty.core.domain.serializers.BigDecimalTwoDigitsSerializer;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TestSampleAddRequest {
    @NotEmpty
    private String batchId;
    private Long sampleNumber = 0L;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    @DecimalMin("0")
    private BigDecimal sample = new BigDecimal(0);
    private Long dateSent = 0L;
    private TestSample.SampleStatus status;
    private Long dateTested = 0L;
    private TestResult testResult;
    private String testingCompanyId;
    private List<CompanyAsset> attachments = new ArrayList<>();
    private String testId;
    private String referenceNo;
    private String licenseId;

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public Long getSampleNumber() {
        return sampleNumber;
    }

    public void setSampleNumber(Long sampleNumber) {
        this.sampleNumber = sampleNumber;
    }

    public BigDecimal getSample() {
        return sample;
    }

    public void setSample(BigDecimal sample) {
        this.sample = sample;
    }

    public Long getDateSent() {
        return dateSent;
    }

    public void setDateSent(Long dateSent) {
        this.dateSent = dateSent;
    }

    public TestSample.SampleStatus getStatus() {
        return status;
    }

    public void setStatus(TestSample.SampleStatus status) {
        this.status = status;
    }

    public Long getDateTested() {
        return dateTested;
    }

    public void setDateTested(Long dateTested) {
        this.dateTested = dateTested;
    }

    public TestResult getTestResult() {
        return testResult;
    }

    public void setTestResult(TestResult testResult) {
        this.testResult = testResult;
    }

    public String getTestingCompanyId() {
        return testingCompanyId;
    }

    public void setTestingCompanyId(String testingCompanyId) {
        this.testingCompanyId = testingCompanyId;
    }

    public List<CompanyAsset> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<CompanyAsset> attachments) {
        this.attachments = attachments;
    }

    public String getTestId() {
        return testId;
    }

    public void setTestId(String testId) {
        this.testId = testId;
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    public String getLicenseId() {
        return licenseId;
    }

    public void setLicenseId(String licenseId) {
        this.licenseId = licenseId;
    }
}
