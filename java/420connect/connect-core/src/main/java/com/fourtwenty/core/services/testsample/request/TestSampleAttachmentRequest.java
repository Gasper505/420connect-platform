package com.fourtwenty.core.services.testsample.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.company.CompanyAsset;
import org.hibernate.validator.constraints.NotEmpty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TestSampleAttachmentRequest {
    @NotEmpty
    private String testSampleId;
    private CompanyAsset companyAsset;

    public String getTestSampleId() {
        return testSampleId;
    }

    public void setTestSampleId(String testSampleId) {
        this.testSampleId = testSampleId;
    }

    public CompanyAsset getCompanyAsset() {
        return companyAsset;
    }

    public void setCompanyAsset(CompanyAsset companyAsset) {
        this.companyAsset = companyAsset;
    }
}
