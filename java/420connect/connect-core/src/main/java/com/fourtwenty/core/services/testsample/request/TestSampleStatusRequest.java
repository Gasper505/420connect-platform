package com.fourtwenty.core.services.testsample.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.testsample.TestSample;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TestSampleStatusRequest {
    private TestSample.SampleStatus updatedStatus;

    public TestSample.SampleStatus getUpdatedStatus() {
        return updatedStatus;
    }

    public void setUpdatedStatus(TestSample.SampleStatus updatedStatus) {
        this.updatedStatus = updatedStatus;
    }
}
