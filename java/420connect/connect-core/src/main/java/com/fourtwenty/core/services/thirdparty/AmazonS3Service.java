package com.fourtwenty.core.services.thirdparty;

import com.amazonaws.services.s3.model.CopyObjectResult;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.transfer.MultipleFileDownload;
import com.fourtwenty.core.rest.dispensary.results.common.AssetStreamResult;
import com.fourtwenty.core.rest.dispensary.results.common.UploadFileResult;

import java.io.File;
import java.io.InputStream;

/**
 * The AmazonS3Service object.
 * User: mdo
 * Date: 11/1/13
 * Time: 2:51 AM
 */
public interface AmazonS3Service {
    String uploadFile(File file, String fileName, String extension);

    UploadFileResult uploadFilePublic(File file, String fileName, String extension);

    UploadFileResult uploadFilePublic(InputStream stream, String fileName, String extension);

    UploadFileResult uploadFilePublic(InputStream stream, ObjectMetadata objectMetadata, String fileName, String extension);

    UploadFileResult uploadFilePrivate(InputStream stream, ObjectMetadata objectMetadata, String fileName, String extension);

    String uploadPublicFile(InputStream inputStream);

    AssetStreamResult downloadFile(String keyName, boolean secured);

    void createFolder(String folderName);

    CopyObjectResult copyFile(String destinationFolder, String sourceKey, String destinationKey);

    MultipleFileDownload downloadFolder(String shopFolder, File directory);

    boolean uploadFile(InputStream stream, String fileName, String extension, String folderName);
}
