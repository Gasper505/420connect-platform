package com.fourtwenty.core.services.thirdparty;

import java.util.List;

/**
 * Created by Gaurav Saini on 29/5/17.
 */
public interface AmazonSNSClientService {
    void sendDirectSMSMessage(List<String> phoneNumbers, String message);
}
