package com.fourtwenty.core.services.thirdparty;

/**
 * Created by mdo on 8/28/17.
 */
public interface AmazonSQSFifoService extends FifoService {
    void publishItemToQueue();

    void publishQueuedTransaction(final String companyId, String shopId, String queueTransId);
    void publishComplianceSyncJob(final String companyId, String shopId, String complianceJobId);

    void publishWeedmapSyncJob(String companyId, String shopId, String weedmapSyncJobId);

    void publishLeaflySyncJob(String companyId, String shopId, String leaflySyncJobId);
}
