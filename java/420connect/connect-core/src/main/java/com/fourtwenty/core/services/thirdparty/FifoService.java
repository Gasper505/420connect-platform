package com.fourtwenty.core.services.thirdparty;

public interface FifoService {
    void publishItemToQueue();

    void publishQueuedTransaction(final String companyId, String shopId, String queueTransId);
}
