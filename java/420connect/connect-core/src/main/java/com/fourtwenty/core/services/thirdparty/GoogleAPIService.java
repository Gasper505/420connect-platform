package com.fourtwenty.core.services.thirdparty;

import com.fourtwenty.core.services.thirdparty.models.GoogleShortURLResult;

/**
 * Created by mdo on 6/30/17.
 */
public interface GoogleAPIService {
    GoogleShortURLResult requestShortUrl(String longUrl);
}
