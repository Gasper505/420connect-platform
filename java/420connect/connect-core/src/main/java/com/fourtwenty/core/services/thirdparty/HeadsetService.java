package com.fourtwenty.core.services.thirdparty;

import com.fourtwenty.core.domain.models.thirdparty.HeadsetLocation;

/**
 * Created by mdo on 7/13/17.
 */
public interface HeadsetService {
    HeadsetLocation acceptHeadsetTerms();

    HeadsetLocation updateHeadsetLocation(String headsetLocationId, HeadsetLocation headsetLocation);

    HeadsetLocation getHeadsetAccount();
}
