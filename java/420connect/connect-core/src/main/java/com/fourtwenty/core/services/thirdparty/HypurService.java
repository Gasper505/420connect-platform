package com.fourtwenty.core.services.thirdparty;

import com.blaze.clients.hypur.models.*;
import com.fourtwenty.core.domain.models.thirdparty.ThirdPartyShopAccount;

import java.util.List;

/**
 * Created by mdo on 9/14/17.
 */
public interface HypurService {
    ThirdPartyShopAccount getCurrentHypurAccount();

    ThirdPartyShopAccount updateHypurAccount(ThirdPartyShopAccount hypurAccount);

    CheckedInUserListResponse getHypurCheckinUserProfiles(int thumnailSize);

    CheckInTestUsersResponse generateHypurTestUsers();

    CheckInTestUsersResponse makeHypurPayment(HypurPayment hypurPayment);

    CheckInTestUsersResponse refundHypurPayment(HypurRefund hypurRefund);

    String getHypurInvoiceErrorCodes();

    HypurInvoiceList addHypurInvoices(List<HypurInvoice> hypurInvoice);

    HypurInvoice getHypurInvoiceDetails(String invoiceNumber);

    HypurInvoiceList getHypurInvoices(String pageNumber, String itemsPerPage);

    InvoiceResponse addHypurInvoice(HypurInvoice hypurInvoice);

    InvoiceResponse updateHypurInvoice(HypurInvoice hypurInvoice);
}
