package com.fourtwenty.core.services.thirdparty;

import com.fourtwenty.core.domain.models.thirdparty.leafly.LeaflyAccount;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.thirdparty.leafly.result.LeaflyAccountResult;

public interface LeaflyAccountService {

    SearchResult<LeaflyAccountResult> searchLeaflyAccounts(int start, int limit);

    LeaflyAccountResult getLeaflyAccountById(String id);

    LeaflyAccount createLeaflyAccount(LeaflyAccount account);

    LeaflyAccount updateLeaflyAccount(LeaflyAccount account, String id);

    void deleteLeaflyAccount(String id);

    void resetMenu(boolean shouldDeleted);
}
