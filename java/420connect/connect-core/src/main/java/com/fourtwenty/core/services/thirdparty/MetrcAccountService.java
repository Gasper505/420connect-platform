package com.fourtwenty.core.services.thirdparty;

import com.blaze.clients.metrcs.models.facilities.MetricsFacilityList;
import com.blaze.clients.metrcs.models.packages.MetricsPackages;
import com.blaze.clients.metrcs.models.packages.MetricsPackagesList;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.domain.models.thirdparty.MetrcAccount;
import com.fourtwenty.core.event.inventory.ComplianceBatch;
import com.fourtwenty.core.services.thirdparty.models.MetrcVerifiedPackage;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mdo on 8/25/17.
 */
public interface MetrcAccountService {
    List<MetrcAccount> getMetrcAccount(String companyId, String stateCode);

    List<MetrcAccount> getMyMetrcAccount();

    MetricsFacilityList getMetrcFacilities();
    Map<String, MetricsFacilityList> getMetrcFacilities(String env, String stateCode, String metrcApiKey);

    List<MetrcAccount> updateMetrcAccount(List<MetrcAccount> metrcAccounts);

    MetrcVerifiedPackage getMetrcVerifiedPackage(String label);

    MetricsPackages getMetrcPackageByLabel(String label);

    MetricsPackagesList getMetrcPackagesForCurrentShop();

    HashMap<String, MetricsPackages> getMetrcPackagesMap();

    Map<String, MetricsFacilityList> getMetrcFacilitiesByStateCode();

    MetrcAccount addMetrcAccount(MetrcAccount metrcAccount);

    ComplianceBatch getMetrcBatchByLabel(String label);
    ComplianceBatch getMetrcBatchByLabel(String label, String productId);

    BigDecimal convertToBatchQuantity(MetrcVerifiedPackage verifiedPackage, ProductCategory.UnitType unitType, Product productProfile);

}
