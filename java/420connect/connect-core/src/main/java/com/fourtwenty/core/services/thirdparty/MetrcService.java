package com.fourtwenty.core.services.thirdparty;

import com.blaze.clients.metrcs.MetrcAuthorization;
import com.fourtwenty.core.domain.models.common.IntegrationSetting;
import com.fourtwenty.core.domain.models.thirdparty.MetrcAccount;

import java.util.Map;

public interface MetrcService {
    MetrcAccount getMetrcAccountByShopId(String companyId, String shopId);
    MetrcAccount getMetrcAccount(String companyId, String stateCode);

    MetrcAuthorization getMetrcAuthorization(String companyId, String shopId);

    MetrcAuthorization createMetrcAuthorization(String companyId, String shopId, IntegrationSetting.Environment environment, String stateCode, String apiKey);

    Map<String, MetrcAuthorization> getMetrcAuthorizationDetails(String companyId, String shopId);

    String getShopStateCode(String CompanyId, String ShopId);

    String getShopStateCode(String CompanyId, String ShopId, boolean isRequired);
}
