package com.fourtwenty.core.services.thirdparty;

import com.fourtwenty.core.services.thirdparty.impl.RabbitMQFifoServiceImpl;

public interface RabbitMQFifoService extends FifoService {
    void processMessageFromQueue(String queueName, RabbitMQFifoServiceImpl.RabbitMQConsumer rabbitMQConsumer);
}
