package com.fourtwenty.core.services.thirdparty;

import com.sendgrid.Response;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

public interface SendgridSerive {
    Response sendMultiPartEmail(String supportEmail, String toEmail, String aSubject,
                                String aBody, String bccEmail, String ccEmail,
                                final HashMap<String, HashMap<String, InputStream>> streamMap, String contentType, String fileName, String fromName) throws IOException;

    Response sendEmail(final String supportEmail, final String toEmail, final String aSubject, final String aBody,
                       final String bccEmail, final String ccEmail, final String replyToEmail, final String fromName) throws IOException;

}
