package com.fourtwenty.core.services.thirdparty;

import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.thirdparty.WeedmapAccount;
import com.fourtwenty.core.domain.models.thirdparty.WmCategoryMapping;
import com.fourtwenty.core.domain.models.thirdparty.WmTagGroups;
import com.fourtwenty.core.domain.models.thirdparty.WmProductMapping;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.thirdparty.VersionRequest;
import com.fourtwenty.core.rest.thirdparty.WeedmapTokenType;
import com.fourtwenty.core.rest.thirdparty.WeedmapUpdateAPIRequest;
import com.fourtwenty.core.thirdparty.weedmap.models.ThirdPartyBrand;
import com.fourtwenty.core.thirdparty.weedmap.models.ThirdPartyProduct;
import com.fourtwenty.core.thirdparty.weedmap.result.WeedmapAccountResult;

import java.util.List;

/**
 * Created by mdo on 4/12/17.
 */
public interface WeedmapService {

    WeedmapAccountResult getCurrentWeedmapAccount();

    WeedmapAccount createWeedmapAccount(VersionRequest request);

    WeedmapAccount updateWeedmapAPIKey(WeedmapUpdateAPIRequest request);

    WeedmapAccount updateWeedmapAccount(WeedmapAccount account);

    void deleteWeedmapAccount();

    WeedmapAccount syncMenu();

    void syncCategories();
    void removeProductFromWeedMap(List<String> productCategoryId, boolean isDelete, List<Product> products);
    void resetMenu(boolean shouldDelete);
    WeedmapAccount fetchOrRefreshToken(WeedmapTokenType type);
    WeedmapAccount generateCode(String code);

    List<WmTagGroups> getWeedmapTags();

    SearchResult<ThirdPartyBrand> getThirdPartyBrand(String brandId, int skip, int limit, String term);

    SearchResult<ThirdPartyProduct> getThirdPartyProduct(String thirdPartyBrandId, int skip, int limit, String term);

    List<WmProductMapping> updateWmMapping(List<WmProductMapping> wmMapping);

    void updateCategoryMapping(List<WmCategoryMapping> wmMapping);
}
