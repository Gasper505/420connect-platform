package com.fourtwenty.core.services.thirdparty.impl;

import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.config.TransProcessorConfig;
import com.fourtwenty.core.domain.models.App;
import com.fourtwenty.core.domain.repositories.dispensary.AppRepository;
import com.fourtwenty.core.services.thirdparty.FifoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetAddress;
import java.net.UnknownHostException;

public abstract class AbstractFifoService implements FifoService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractFifoService.class);

    protected ConnectConfiguration connectConfiguration;
    protected AppRepository appRepository;

    public AbstractFifoService(ConnectConfiguration connectConfiguration, AppRepository appRepository) {
        this.connectConfiguration = connectConfiguration;
        this.appRepository = appRepository;
    }

    protected String createTransQueue(String companyId) {
        TransProcessorConfig processorConfig = connectConfiguration.getProcessorConfig();
        App app = appRepository.getAppByName(App.BLAZE_APP_NAME);

        int numOfQueues = 1;
        if (app != null && app.getNumOfQueues() > 0) {
            numOfQueues = app.getNumOfQueues();
        }
        int num = Character.getNumericValue(companyId.charAt(companyId.length() - 1));
        int queueNum = num % numOfQueues;

        String env = connectConfiguration.getEnv().name();
        if (connectConfiguration.getEnv() == ConnectConfiguration.EnvironmentType.LOCAL) {
            try {
                String computername = InetAddress.getLocalHost().getHostName();
                computername = computername.replace(".", "");
                env = connectConfiguration.getEnv().name() + "-" + computername;
            } catch (UnknownHostException e) {
            }
        }
        String queueNumStr = "";
        if (queueNum > 0) {
            queueNumStr = "_" + queueNum;
        }

        final String queueName = env + "-" + processorConfig.getQueueName() + queueNumStr + ".fifo";
        return createQueue(queueName);
    }

    protected abstract String createQueue(String queueName);

}
