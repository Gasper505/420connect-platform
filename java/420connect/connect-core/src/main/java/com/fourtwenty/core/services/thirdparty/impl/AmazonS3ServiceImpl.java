package com.fourtwenty.core.services.thirdparty.impl;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.*;
import com.amazonaws.services.s3.transfer.MultipleFileDownload;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.TransferManagerBuilder;
import com.fourtwenty.core.rest.dispensary.results.common.AssetStreamResult;
import com.fourtwenty.core.rest.dispensary.results.common.UploadFileResult;
import com.fourtwenty.core.services.thirdparty.AmazonS3Service;
import com.fourtwenty.core.util.AmazonServiceFactory;
import com.google.inject.Inject;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.*;
import java.util.UUID;

/**
 * The AmazonS3ServiceImpl object.
 * User: mdo
 * Date: 11/1/13
 * Time: 2:51 AM
 */
public class AmazonS3ServiceImpl implements AmazonS3Service {
    static final Log LOG = LogFactory.getLog(AmazonS3ServiceImpl.class);
    @Inject
    private AmazonServiceFactory amazonS3Util;

    private static final String DEFAULT_EXT = ".png";
    private static final String SUFFIX = "/";

    @Override
    public String uploadFile(File file, String fileName, String extension) {

        AmazonS3 s3client = amazonS3Util.getS3Client();
        try {
            String keyName = fileName + extension;

            PutObjectRequest putObjectRequest = new PutObjectRequest(amazonS3Util.getS3Config().getBucketName(), keyName, file);
            s3client.putObject(putObjectRequest);
            return keyName;
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
        return null;
    }

    @Override
    public UploadFileResult uploadFilePublic(InputStream stream, String fileName, String extension) {

        AmazonS3 s3client = amazonS3Util.getS3Client();
        try {
            File file = stream2file(stream, extension);

            String keyName = fileName + extension;

            PutObjectRequest putObjectRequest = new PutObjectRequest(amazonS3Util.getS3Config().getPublicBucketName(), keyName, file);
            s3client.putObject(putObjectRequest);

            String url = String.format("https://%s.s3.amazonaws.com/%s", amazonS3Util.getS3Config().getPublicBucketName(), keyName);
            UploadFileResult result = new UploadFileResult();
            result.setKey(keyName);

            result.setUrl(url);
            return result;
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
        return null;
    }

    @Override
    public UploadFileResult uploadFilePublic(File file, String fileName, String extension) {

        AmazonS3 s3client = amazonS3Util.getS3Client();
        try {

            String keyName = fileName + extension;

            PutObjectRequest putObjectRequest = new PutObjectRequest(amazonS3Util.getS3Config().getPublicBucketName(), keyName, file);
            s3client.putObject(putObjectRequest);

            String url = String.format("https://%s.s3.amazonaws.com/%s", amazonS3Util.getS3Config().getPublicBucketName(), keyName);
            UploadFileResult result = new UploadFileResult();
            result.setKey(keyName);

            result.setUrl(url);
            return result;
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
        return null;
    }

    @Override
    public UploadFileResult uploadFilePublic(InputStream stream, ObjectMetadata objectMetadata, String fileName, String extension) {
        AmazonS3 s3client = amazonS3Util.getS3Client();
        try {
            String keyName = fileName + extension;
            PutObjectRequest putObjectRequest = new PutObjectRequest(amazonS3Util.getS3Config().getPublicBucketName(), keyName, stream, objectMetadata);
            s3client.putObject(putObjectRequest);

            String url = String.format("https://%s.s3.amazonaws.com/%s", amazonS3Util.getS3Config().getPublicBucketName(), keyName);
            UploadFileResult result = new UploadFileResult();
            result.setKey(keyName);

            result.setUrl(url);
            return result;
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
        return null;
    }

    @Override
    public UploadFileResult uploadFilePrivate(InputStream stream, ObjectMetadata objectMetadata, String fileName, String extension) {
        AmazonS3 s3client = amazonS3Util.getS3Client();
        try {
            String keyName = fileName + extension;
            PutObjectRequest putObjectRequest = new PutObjectRequest(amazonS3Util.getS3Config().getBucketName(), keyName, stream, objectMetadata);
            s3client.putObject(putObjectRequest);

            String url = String.format("https://%s.s3.amazonaws.com/%s", amazonS3Util.getS3Config().getBucketName(), keyName);
            UploadFileResult result = new UploadFileResult();
            result.setKey(keyName);

            result.setUrl(url);
            return result;
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
        return null;
    }

    public String uploadPublicFile(InputStream inputStream) {
        AmazonS3 s3client = amazonS3Util.getS3Client();
        try {
            String fileName = UUID.randomUUID().toString();
            String extension = DEFAULT_EXT;
            File file = stream2file(inputStream, extension);
            String keyName = fileName + extension;

            PutObjectRequest putObjectRequest = new PutObjectRequest(amazonS3Util.getS3Config().getPublicBucketName(), keyName, file);
            s3client.putObject(putObjectRequest);
            return String.format("https://%s.s3.amazonaws.com/%s", amazonS3Util.getS3Config().getPublicBucketName(), keyName);
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }

        return null;
    }

    @Override
    public AssetStreamResult downloadFile(String keyName, boolean secured) {
        try {
            AmazonS3 s3Client = amazonS3Util.getS3Client();

            String bucketName = secured ? amazonS3Util.getS3Config().getBucketName() : amazonS3Util.getS3Config().getPublicBucketName();

            LOG.info("Accessing image: " + keyName + "secured: " + secured + "   bucketname: " + bucketName);
            S3Object object = s3Client.getObject(
                    new GetObjectRequest(bucketName, keyName));
            InputStream objectData = object.getObjectContent();
            AssetStreamResult result = new AssetStreamResult();
            String key = object.getKey();
            result.setKey(key);
            result.setStream(objectData);
            result.setContentType(object.getObjectMetadata().getContentType());
            return result;
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
        return null;
    }

    /**
     * This method creates folder
     *
     * @param folderName : folder name
     */
    @Override
    public void createFolder(String folderName) {

        AmazonS3 s3client = amazonS3Util.getS3Client();
        try {
            // create meta-data for your folder and set content-length to 0
            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentLength(0);

            // create empty content
            InputStream emptyContent = new ByteArrayInputStream(new byte[0]);

            // create a PutObjectRequest passing the folder name suffixed by /
            PutObjectRequest putObjectRequest = new PutObjectRequest(amazonS3Util.getS3Config().getBucketName(),
                    folderName + SUFFIX, emptyContent, metadata);

            // send request to S3 to create folder
            s3client.putObject(putObjectRequest);
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
    }

    /**
     * @param destinationFolder : destination folder name
     * @param sourceKey         : asset key
     * @param destinationKey
     * @implNote This method copy files in another folder
     */
    @Override
    public CopyObjectResult copyFile(String destinationFolder, String sourceKey, String destinationKey) {
        try {

            AmazonS3 s3Client = amazonS3Util.getS3Client();

            String sourceBucketName = amazonS3Util.getS3Config().getBucketName();

            CopyObjectResult result = s3Client.copyObject(sourceBucketName, sourceKey, sourceBucketName + SUFFIX + destinationFolder, destinationKey);

            return result;

        } catch (Exception e) {
            LOG.error("Error while copying file to another folder : " + e.getMessage(), e);
        }
        return null;
    }

    @Override
    public MultipleFileDownload downloadFolder(String shopFolder, File directory) {
        try {

            AmazonS3 s3Client = amazonS3Util.getS3Client();
            String bucketName = amazonS3Util.getS3Config().getBucketName();

            TransferManager transferManager = TransferManagerBuilder.standard().withS3Client(s3Client).build();

            MultipleFileDownload xfer = transferManager.downloadDirectory(
                    bucketName, shopFolder, directory);
            xfer.waitForCompletion();

            return xfer;
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
        return null;
    }

    @Override
    public boolean uploadFile(InputStream stream, String fileName, String extension, String folderName) {
        AmazonS3 s3Client = amazonS3Util.getS3Client();

        String sourceBucketName = amazonS3Util.getS3Config().getBucketName();

        if (StringUtils.isNotBlank(folderName)) {
            sourceBucketName = sourceBucketName + SUFFIX + folderName;
        }

        try {
            File file = stream2file(stream, extension);

            String keyName = fileName + extension;

            PutObjectRequest putObjectRequest = new PutObjectRequest(sourceBucketName, keyName, file);
            s3Client.putObject(putObjectRequest);
            return true;
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
        return false;
    }

    private static File stream2file(InputStream in, String extension) throws IOException {
        final File tempFile = File.createTempFile(UUID.randomUUID().toString(), extension);
        tempFile.deleteOnExit();
        try (FileOutputStream out = new FileOutputStream(tempFile)) {
            IOUtils.copy(in, out);
        }
        return tempFile;
    }
}
