package com.fourtwenty.core.services.thirdparty.impl;

import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.model.MessageAttributeValue;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;
import com.amazonaws.services.sns.model.SubscribeRequest;
import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.domain.repositories.dispensary.CompanyRepository;
import com.fourtwenty.core.domain.repositories.dispensary.MemberRepository;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.mgmt.impl.CompanyServiceImpl;
import com.fourtwenty.core.services.thirdparty.AmazonSNSClientService;
import com.fourtwenty.core.util.AmazonServiceFactory;
import com.google.inject.Inject;
import com.google.inject.Provider;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Gaurav Saini on 26/5/17.
 */
public class AmazonSNSClientServiceImpl extends AbstractAuthServiceImpl implements AmazonSNSClientService {
    private static final Log LOG = LogFactory.getLog(AmazonSNSClientServiceImpl.class);
    private final ConnectConfiguration config;

    @Inject
    MemberRepository memberRepository;
    @Inject
    CompanyRepository companyRepository;
    @Inject
    CompanyServiceImpl companyService;

    @Inject
    private AmazonServiceFactory amazonServiceFactory;

    @Inject
    public AmazonSNSClientServiceImpl(ConnectConfiguration configuration, Provider<ConnectAuthToken> token) {
        super(token);
        this.config = configuration;
    }


    @Override
    public void sendDirectSMSMessage(List<String> phoneNumbers, String message) {
        AmazonSNS snsClient = amazonServiceFactory.getSNSClient();
        for (String phone : phoneNumbers) {
            String phoneNumber = phoneNumber(phone);
            sendDirectSMSMessage(snsClient, phoneNumber, message);
        }
    }

    private String sendDirectSMSMessage(AmazonSNS snsClient, String phoneNumber,
                                        String message) {
        LOG.info("Sending SMS to: " + phoneNumber);
        Map<String, MessageAttributeValue> smsAttributes =
                new HashMap<String, MessageAttributeValue>();
        //<set SMS attributes>
        smsAttributes.put("AWS.SNS.SMS.SenderID", new MessageAttributeValue()
                .withStringValue(config.getAppName()) //The sender ID shown on the device.
                .withDataType("String"));
        PublishResult result = snsClient.publish(new PublishRequest()
                .withMessage(message)
                .withPhoneNumber(phoneNumber)
                .withMessageAttributes(smsAttributes));

        return result.getMessageId();
    }

    private static void subscribeToTopic(AmazonSNS snsClient, String topicArn, String phoneNumber) {
        SubscribeRequest subscribe = new SubscribeRequest(topicArn, "sms", phoneNumber);
        snsClient.subscribe(subscribe);
    }

    private static String sendSMSMessageToTopic(AmazonSNS snsClient, String topicArn,
                                                String message, Map<String, MessageAttributeValue> smsAttributes) {
        PublishResult result = snsClient.publish(new PublishRequest()
                .withTopicArn(topicArn)
                .withMessage(message)
                .withMessageAttributes(smsAttributes));
        return result.getMessageId();
    }


    private String phoneNumber(String phoneNumber) {
        phoneNumber = phoneNumber.replace("-", "").replace("(", "").replace(")", "").replaceAll("\\s+", "").trim();
        if (!phoneNumber.startsWith("+")) {
            phoneNumber = "+" + phoneNumber;
        }
        return phoneNumber;
    }
}