package com.fourtwenty.core.services.thirdparty.impl;

import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.model.CreateQueueRequest;
import com.amazonaws.services.sqs.model.DeleteMessageRequest;
import com.amazonaws.services.sqs.model.GetQueueUrlRequest;
import com.amazonaws.services.sqs.model.GetQueueUrlResult;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.MessageAttributeValue;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageResult;
import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.config.TransProcessorConfig;
import com.fourtwenty.core.domain.models.App;
import com.fourtwenty.core.domain.repositories.dispensary.AppRepository;
import com.fourtwenty.core.managed.ComplianceSyncProcessorManager;
import com.fourtwenty.core.managed.LeaflySyncProcessorManager;
import com.fourtwenty.core.managed.WeedmapSyncProcessorManager;
import com.fourtwenty.core.services.thirdparty.AmazonSQSFifoService;
import com.fourtwenty.core.services.thirdparty.models.ComplianceSQSMessageRequest;
import com.fourtwenty.core.services.thirdparty.models.LeaflySQSMessageRequest;
import com.fourtwenty.core.services.thirdparty.models.TransSQSMessageRequest;
import com.fourtwenty.core.services.thirdparty.models.WeedmapSQSMessageRequest;
import com.fourtwenty.core.util.AmazonServiceFactory;
import com.fourtwenty.core.util.JsonSerializer;
import com.google.inject.Inject;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mdo on 8/28/17.
 */
public class AmazonSQSFifoServiceImpl implements AmazonSQSFifoService {
    private static final Log LOG = LogFactory.getLog(AmazonSQSFifoServiceImpl.class);
    @Inject
    private AmazonServiceFactory amazonServiceFactory;
    @Inject
    ConnectConfiguration connectConfiguration;
    @Inject
    AppRepository appRepository;

    public void publishItemToQueue() {
        AmazonSQS amazonSQS = amazonServiceFactory.getSQSClient();


        System.out.println("Creating a new Amazon SQS FIFO queue called MyFifoQueue.fifo.\n");
        Map<String, String> attributes = new HashMap<String, String>();
        // A FIFO queue must have the FifoQueue attribute set to True
        attributes.put("FifoQueue", "true");
        // Generate a MessageDeduplicationId based on the content, if the user doesn't provide a MessageDeduplicationId
        attributes.put("ContentBasedDeduplication", "true");
        // The FIFO queue name must end with the .fifo suffix
        CreateQueueRequest createQueueRequest = new CreateQueueRequest("MyFifoQueue.fifo").withAttributes(attributes);

        GetQueueUrlRequest queueUrlRequest = new GetQueueUrlRequest("MyFifoQueue.fifo");
        String myQueueUrl = null;
        try {
            GetQueueUrlResult queueUrlResult = amazonSQS.getQueueUrl(queueUrlRequest);
            if (queueUrlResult != null) {
                myQueueUrl = queueUrlResult.getQueueUrl();
            }
        } catch (Exception e) {
            System.out.print("Queue does not exist.");
        }
        if (myQueueUrl == null) {
            myQueueUrl = amazonSQS.createQueue(createQueueRequest).getQueueUrl();
        }


        // List queues
        System.out.println("Listing all queues in your account.\n");
        for (String queueUrl : amazonSQS.listQueues().getQueueUrls()) {
            System.out.println("  QueueUrl: " + queueUrl);
        }
        System.out.println();


        // Send a message
        System.out.println("Sending a message to MyFifoQueue.fifo.\n");
        TransSQSMessageRequest transRequest = new TransSQSMessageRequest("comp123", "shop123", "queue123");
        String messageBody = JsonSerializer.toJson(transRequest);
        SendMessageRequest sendMessageRequest = new SendMessageRequest(myQueueUrl, messageBody);
        // You must provide a non-empty MessageGroupId when sending messages to a FIFO queue
        sendMessageRequest.setMessageGroupId("messageGroup1");
        // Uncomment the following to provide the MessageDeduplicationId
        //sendMessageRequest.setMessageDeduplicationId("1");

        SendMessageResult sendMessageResult = amazonSQS.sendMessage(sendMessageRequest);
        String sequenceNumber = sendMessageResult.getSequenceNumber();
        String messageId = sendMessageResult.getMessageId();
        System.out.println("SendMessage succeed with messageId " + messageId + ", sequence number " + sequenceNumber + "\n");


        // Receive messages
        System.out.println("Receiving messages from MyFifoQueue.fifo.\n");
        ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(myQueueUrl);
        // Uncomment the following to provide the ReceiveRequestDeduplicationId
        //receiveMessageRequest.setReceiveRequestAttemptId("1");
        List<Message> messages = amazonSQS.receiveMessage(receiveMessageRequest).getMessages();
        for (Message message : messages) {
            System.out.println("  Message");
            System.out.println("    MessageId:     " + message.getMessageId());
            System.out.println("    ReceiptHandle: " + message.getReceiptHandle());
            System.out.println("    MD5OfBody:     " + message.getMD5OfBody());
            System.out.println("    Body:          " + message.getBody());

            TransSQSMessageRequest request = JsonSerializer.fromJson(message.getBody(), TransSQSMessageRequest.class);

            System.out.println("CompanyId: " + request.getCompanyId());
            System.out.println("ShopId: " + request.getShopId());
            System.out.println("QueueId: " + request.getQueueTransactionId());

            for (Map.Entry<String, String> entry : message.getAttributes().entrySet()) {
                System.out.println("  Attribute");
                System.out.println("    NAME:  " + entry.getKey());
                System.out.println("    Value: " + entry.getValue());
            }
        }
        System.out.println();

        // Delete the message
        System.out.println("Deleting the message.\n");
        if (messages.size() > 0) {
            String messageReceiptHandle = messages.get(0).getReceiptHandle();
            amazonSQS.deleteMessage(new DeleteMessageRequest(myQueueUrl, messageReceiptHandle));
        }

        // Delete the queue
        System.out.println("Deleting the queue.\n");
        //amazonSQS.deleteQueue(new DeleteQueueRequest(myQueueUrl));

    }

    @Override
    public void publishQueuedTransaction(final String companyId, final String shopId, final String queueId) {
        String myQueueUrl = createTransQueue(companyId);
        TransSQSMessageRequest transRequest = new TransSQSMessageRequest(companyId,
                shopId,
                queueId);

        String messageBody = JsonSerializer.toJson(transRequest);
        SendMessageRequest sendMessageRequest = new SendMessageRequest(myQueueUrl, messageBody);
        // You must provide a non-empty MessageGroupId when sending messages to a FIFO queue
        sendMessageRequest.setMessageGroupId("TransProcessorQueue");
        // Uncomment the following to provide the MessageDeduplicationId
        sendMessageRequest.setMessageDeduplicationId(queueId);

        AmazonSQS amazonSQS = amazonServiceFactory.getSQSClient();
        SendMessageResult sendMessageResult = amazonSQS.sendMessage(sendMessageRequest);
        String sequenceNumber = sendMessageResult.getSequenceNumber();
        String messageId = sendMessageResult.getMessageId();
        LOG.info("SendMessage succeed with messageId " + messageId + ", sequence number " + sequenceNumber + "\n");

    }

    @Override
    public void publishComplianceSyncJob(String companyId, String shopId, String complianceJobId) {
        String myQueueUrl = createComplianceQueue(companyId);
        ComplianceSQSMessageRequest transRequest = new ComplianceSQSMessageRequest(companyId,
                shopId,
                complianceJobId);

        String messageBody = JsonSerializer.toJson(transRequest);
        SendMessageRequest sendMessageRequest = new SendMessageRequest(myQueueUrl, messageBody);
        // You must provide a non-empty MessageGroupId when sending messages to a FIFO queue
        sendMessageRequest.setMessageGroupId("ComplianceProcessorQueue");
        // Uncomment the following to provide the MessageDeduplicationId
        sendMessageRequest.setMessageDeduplicationId(complianceJobId);

        AmazonSQS amazonSQS = amazonServiceFactory.getSQSClient();
        SendMessageResult sendMessageResult = amazonSQS.sendMessage(sendMessageRequest);
        String sequenceNumber = sendMessageResult.getSequenceNumber();
        String messageId = sendMessageResult.getMessageId();
        LOG.info("SendMessage succeed with messageId " + messageId + ", sequence number " + sequenceNumber + "\n");
    }

    @Override
    public void publishWeedmapSyncJob(String companyId, String shopId, String weedmapSyncJobId) {
        String myQueueUrl = createWeedmapQueue(companyId);
        WeedmapSQSMessageRequest transRequest = new WeedmapSQSMessageRequest(companyId,
                shopId,
                weedmapSyncJobId);

        String messageBody = JsonSerializer.toJson(transRequest);
        SendMessageRequest sendMessageRequest = new SendMessageRequest(myQueueUrl, messageBody);
        // You must provide a non-empty MessageGroupId when sending messages to a FIFO queue
        sendMessageRequest.setMessageGroupId("WeedmapProcessorQueue");
        // Uncomment the following to provide the MessageDeduplicationId
        sendMessageRequest.setMessageDeduplicationId(weedmapSyncJobId);

        AmazonSQS amazonSQS = amazonServiceFactory.getSQSClient();
        SendMessageResult sendMessageResult = amazonSQS.sendMessage(sendMessageRequest);
        String sequenceNumber = sendMessageResult.getSequenceNumber();
        String messageId = sendMessageResult.getMessageId();
        LOG.info("SendMessage succeed with messageId " + messageId + ", sequence number " + sequenceNumber + "\n");
    }

    @Override
    public void publishLeaflySyncJob(String companyId, String shopId, String leaflySyncJobId) {
        String myQueueUrl = createLeaflyQueue(companyId);
        LeaflySQSMessageRequest transRequest = new LeaflySQSMessageRequest(companyId,
                shopId,
                leaflySyncJobId);

        String messageBody = JsonSerializer.toJson(transRequest);
        SendMessageRequest sendMessageRequest = new SendMessageRequest(myQueueUrl, messageBody);
        // You must provide a non-empty MessageGroupId when sending messages to a FIFO queue
        sendMessageRequest.setMessageGroupId("LeaflyProcessorQueue");
        // Uncomment the following to provide the MessageDeduplicationId
        sendMessageRequest.setMessageDeduplicationId(leaflySyncJobId);

        AmazonSQS amazonSQS = amazonServiceFactory.getSQSClient();
        SendMessageResult sendMessageResult = amazonSQS.sendMessage(sendMessageRequest);
        String sequenceNumber = sendMessageResult.getSequenceNumber();
        String messageId = sendMessageResult.getMessageId();
        LOG.info("SendMessage succeed with messageId " + messageId + ", sequence number " + sequenceNumber + "\n");
    }

    private String createTransQueue(final String companyId) {
        TransProcessorConfig processorConfig = connectConfiguration.getProcessorConfig();
        App app = appRepository.getAppByName(App.BLAZE_APP_NAME);

        int numOfQueues = 1;
        if (app != null && app.getNumOfQueues() > 0) {
            numOfQueues = app.getNumOfQueues();
        }
        int num = Character.getNumericValue(companyId.charAt(companyId.length() - 1));
        int queueNum = num % numOfQueues;

        String env = connectConfiguration.getEnv().name();
        if (connectConfiguration.getEnv() == ConnectConfiguration.EnvironmentType.LOCAL) {
            try {
                String computername = InetAddress.getLocalHost().getHostName();
                computername = computername.replace(".", "");
                env = connectConfiguration.getEnv().name() + "-" + computername;
            } catch (UnknownHostException e) {
            }
        }
        String queueNumStr = "";
        if (queueNum > 0) {
            queueNumStr = "_" + queueNum;
        }

        final String queueName =  env + "-" + processorConfig.getQueueName() + queueNumStr + ".fifo";
        LOG.info("SQSQueue: " + queueName);

        //String queueName = connectConfiguration.getEnv() + "-"+ processorConfig.getQueueName() + ".fifo";
        AmazonSQS amazonSQS = amazonServiceFactory.getSQSClient();
        // do some work
        Map<String, String> attributes = new HashMap<String, String>();
        // A FIFO queue must have the FifoQueue attribute set to True
        attributes.put("FifoQueue", "true");
        // Generate a MessageDeduplicationId based on the content, if the user doesn't provide a MessageDeduplicationId
        attributes.put("ContentBasedDeduplication", "true");
        // The FIFO queue name must end with the .fifo suffix

        GetQueueUrlRequest queueUrlRequest = new GetQueueUrlRequest(queueName);
        String myQueueUrl = null;
        try {
            GetQueueUrlResult queueUrlResult = amazonSQS.getQueueUrl(queueUrlRequest);
            if (queueUrlResult != null) {
                myQueueUrl = queueUrlResult.getQueueUrl();
            }
        } catch (Exception e) {
            LOG.info("Queue does not exist.");
        }
        if (myQueueUrl == null) {
            LOG.info("Queue does not exist. Creating new Queue");
            CreateQueueRequest createQueueRequest = new CreateQueueRequest(queueName).withAttributes(attributes);
            myQueueUrl = amazonSQS.createQueue(createQueueRequest).getQueueUrl();
        }

        return myQueueUrl;
    }

    private String createComplianceQueue(final String companyId) {
        TransProcessorConfig processorConfig = connectConfiguration.getProcessorConfig();
        App app = appRepository.getAppByName(App.BLAZE_APP_NAME);

        int numOfQueues = 1;
        int num = Character.getNumericValue(companyId.charAt(companyId.length() - 1));
        int queueNum = num % numOfQueues;

        String env = connectConfiguration.getEnv().name();
        if (connectConfiguration.getEnv() == ConnectConfiguration.EnvironmentType.LOCAL) {
            try {
                String computername = InetAddress.getLocalHost().getHostName();
                computername = computername.replace(".", "");
                env = connectConfiguration.getEnv().name() + "-" + computername;
            } catch (UnknownHostException e) {
            }
        }
        String queueNumStr = "";
        if (queueNum > 0) {
            queueNumStr = "_" + queueNum;
        }

        final String queueName =  env + "-" + ComplianceSyncProcessorManager.QUEUE_NAME + queueNumStr + ".fifo";
        LOG.info("SQSQueue: " + queueName);

        //String queueName = connectConfiguration.getEnv() + "-"+ processorConfig.getQueueName() + ".fifo";
        AmazonSQS amazonSQS = amazonServiceFactory.getSQSClient();
        // do some work
        Map<String, String> attributes = new HashMap<String, String>();
        // A FIFO queue must have the FifoQueue attribute set to True
        attributes.put("FifoQueue", "true");
        // Generate a MessageDeduplicationId based on the content, if the user doesn't provide a MessageDeduplicationId
        attributes.put("ContentBasedDeduplication", "true");
        // The FIFO queue name must end with the .fifo suffix

        GetQueueUrlRequest queueUrlRequest = new GetQueueUrlRequest(queueName);
        String myQueueUrl = null;
        try {
            GetQueueUrlResult queueUrlResult = amazonSQS.getQueueUrl(queueUrlRequest);
            if (queueUrlResult != null) {
                myQueueUrl = queueUrlResult.getQueueUrl();
            }
        } catch (Exception e) {
            LOG.info("Queue does not exist.");
        }
        if (myQueueUrl == null) {
            LOG.info("Queue does not exist. Creating new Queue");
            CreateQueueRequest createQueueRequest = new CreateQueueRequest(queueName).withAttributes(attributes);
            myQueueUrl = amazonSQS.createQueue(createQueueRequest).getQueueUrl();
        }

        return myQueueUrl;
    }

    private String createWeedmapQueue(final String companyId) {
        App app = appRepository.getAppByName(App.BLAZE_APP_NAME);
        int numOfQueues = 1;
        int num = Character.getNumericValue(companyId.charAt(companyId.length() - 1));

        if (app != null && app.getNumOfQueues() > 0) {
            numOfQueues = app.getNumOfQueues();
        }

        int queueNum = num % numOfQueues;

        String env = connectConfiguration.getEnv().name();
        if (connectConfiguration.getEnv() == ConnectConfiguration.EnvironmentType.LOCAL) {
            try {
                String computerName = InetAddress.getLocalHost().getHostName();
                env = String.format("%s-%s", connectConfiguration.getEnv().name(), computerName.replace(".", ""));
            } catch (UnknownHostException e) {
            }
        }
        String queueNumStr = "";
        if (queueNum > 0) {
            queueNumStr = "_" + queueNum;
        }

        final String queueName =  env + "-" + WeedmapSyncProcessorManager.QUEUE_NAME + queueNumStr + ".fifo";
        LOG.info("SQSQueue: " + queueName);

        //String queueName = connectConfiguration.getEnv() + "-"+ processorConfig.getQueueName() + ".fifo";
        AmazonSQS amazonSQS = amazonServiceFactory.getSQSClient();
        // do some work
        Map<String, String> attributes = new HashMap<String, String>();
        // A FIFO queue must have the FifoQueue attribute set to True
        attributes.put("FifoQueue", "true");
        // Generate a MessageDeduplicationId based on the content, if the user doesn't provide a MessageDeduplicationId
        attributes.put("ContentBasedDeduplication", "true");
        // The FIFO queue name must end with the .fifo suffix

        GetQueueUrlRequest queueUrlRequest = new GetQueueUrlRequest(queueName);
        String myQueueUrl = null;
        try {
            GetQueueUrlResult queueUrlResult = amazonSQS.getQueueUrl(queueUrlRequest);
            if (queueUrlResult != null) {
                myQueueUrl = queueUrlResult.getQueueUrl();
            }
        } catch (Exception e) {
            LOG.info("Queue does not exist.");
        }
        if (myQueueUrl == null) {
            LOG.info("Queue does not exist. Creating new Queue");
            CreateQueueRequest createQueueRequest = new CreateQueueRequest(queueName).withAttributes(attributes);
            myQueueUrl = amazonSQS.createQueue(createQueueRequest).getQueueUrl();
        }

        return myQueueUrl;
    }

    private static MessageAttributeValue createStringValue(String value) {
        MessageAttributeValue attr = new MessageAttributeValue().withStringValue(value);
        attr.setDataType("String");
        return attr;
    }

    private String createLeaflyQueue(final String companyId) {
        App app = appRepository.getAppByName(App.BLAZE_APP_NAME);
        int numOfQueues = 1;
        int num = Character.getNumericValue(companyId.charAt(companyId.length() - 1));

        if (app != null && app.getNumOfQueues() > 0) {
            numOfQueues = app.getNumOfQueues();
        }

        int queueNum = num % numOfQueues;

        String env = connectConfiguration.getEnv().name();
        if (connectConfiguration.getEnv() == ConnectConfiguration.EnvironmentType.LOCAL) {
            try {
                String computerName = InetAddress.getLocalHost().getHostName();
                env = String.format("%s-%s", connectConfiguration.getEnv().name(), computerName.replace(".", ""));
            } catch (UnknownHostException e) {
            }
        }
        String queueNumStr = "";
        if (queueNum > 0) {
            queueNumStr = "_" + queueNum;
        }

        final String queueName =  env + "-" + LeaflySyncProcessorManager.QUEUE_NAME + queueNumStr + ".fifo";
        LOG.info("SQSQueue: " + queueName);

        //String queueName = connectConfiguration.getEnv() + "-"+ processorConfig.getQueueName() + ".fifo";
        AmazonSQS amazonSQS = amazonServiceFactory.getSQSClient();
        // do some work
        Map<String, String> attributes = new HashMap<String, String>();
        // A FIFO queue must have the FifoQueue attribute set to True
        attributes.put("FifoQueue", "true");
        // Generate a MessageDeduplicationId based on the content, if the user doesn't provide a MessageDeduplicationId
        attributes.put("ContentBasedDeduplication", "true");
        // The FIFO queue name must end with the .fifo suffix

        GetQueueUrlRequest queueUrlRequest = new GetQueueUrlRequest(queueName);
        String myQueueUrl = null;
        try {
            GetQueueUrlResult queueUrlResult = amazonSQS.getQueueUrl(queueUrlRequest);
            if (queueUrlResult != null) {
                myQueueUrl = queueUrlResult.getQueueUrl();
            }
        } catch (Exception e) {
            LOG.info("Queue does not exist.");
        }
        if (myQueueUrl == null) {
            LOG.info("Queue does not exist. Creating new Queue");
            CreateQueueRequest createQueueRequest = new CreateQueueRequest(queueName).withAttributes(attributes);
            myQueueUrl = amazonSQS.createQueue(createQueueRequest).getQueueUrl();
        }

        return myQueueUrl;
    }
}
