package com.fourtwenty.core.services.thirdparty.impl;

import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.services.thirdparty.GoogleAPIService;
import com.fourtwenty.core.services.thirdparty.models.GoogleShortURLResult;
import com.fourtwenty.core.services.thirdparty.models.GoogleShortUrlRequest;
import com.mdo.pusher.SimpleRestUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.inject.Inject;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;

/**
 * Created by mdo on 6/30/17.
 */
public class GoogleAPIServiceImpl implements GoogleAPIService {
    private static final String GOOGLE_SHORT_URL = "https://www.googleapis.com/urlshortener/v1/url";

    private static final Log LOG = LogFactory.getLog(GoogleAPIServiceImpl.class);
    @Inject
    ConnectConfiguration configuration;

    @Override
    public GoogleShortURLResult requestShortUrl(String longUrl) {
        final String apiURL = String.format("%s?key=%s", GOOGLE_SHORT_URL, configuration.getGoogleAPIKey());

        GoogleShortUrlRequest request = new GoogleShortUrlRequest();
        request.setLongUrl(longUrl);
        try {
            MultivaluedMap<String, Object> headers = new MultivaluedHashMap<>();
            headers.putSingle("Content-Type", MediaType.APPLICATION_JSON);
            GoogleShortURLResult result = SimpleRestUtil.post(apiURL, request, GoogleShortURLResult.class, headers);
            return result;
        } catch (Exception e) {
            LOG.info("Issue shortening url", e);
        }
        return null;
    }
}
