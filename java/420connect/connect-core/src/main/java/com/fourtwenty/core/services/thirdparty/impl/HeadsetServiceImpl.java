package com.fourtwenty.core.services.thirdparty.impl;

import com.fourtwenty.core.domain.models.common.HeadsetConfig;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.thirdparty.HeadsetLocation;
import com.fourtwenty.core.domain.repositories.common.IntegrationSettingRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.domain.repositories.thirdparty.HeadsetLocationRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.rest.dispensary.requests.thirdparty.HeadsetAddRequest;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.thirdparty.HeadsetService;
import com.fourtwenty.core.thirdparty.headset.HeadsetAPIService;
import com.fourtwenty.core.thirdparty.headset.models.Headset;
import com.fourtwenty.core.thirdparty.headset.models.HeadsetError;
import com.fourtwenty.core.util.DateUtil;
import com.google.inject.Provider;
import com.mdo.pusher.RestErrorException;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.util.HashMap;

/**
 * Created by mdo on 7/13/17.
 */
public class HeadsetServiceImpl extends AbstractAuthServiceImpl implements HeadsetService {
    private static final Log LOG = LogFactory.getLog(HeadsetServiceImpl.class);
    @Inject
    HeadsetLocationRepository headsetLocationRepository;
    @Inject
    HeadsetAPIService headsetAPIService;
    @Inject
    ShopRepository shopRepository;
    @Inject
    IntegrationSettingRepository integrationSettingRepository;

    @Inject
    public HeadsetServiceImpl(Provider<ConnectAuthToken> tokenProvider) {
        super(tokenProvider);
    }

    @Override
    public HeadsetLocation acceptHeadsetTerms() {
        // Support for multiple locations
        Iterable<HeadsetLocation> headsetLocations = headsetLocationRepository.getHeadsetLocations(token.getCompanyId());

        HeadsetLocation location = null;

        String integrationKey = null;
        Long headsetCompanyId = null;

        boolean done = false;
        for (HeadsetLocation loc : headsetLocations) {
            if (loc.getHeadsetCompanyId() != null
                    && StringUtils.isNotBlank(loc.getIntegrationKey())) {
                integrationKey = loc.getIntegrationKey();
                headsetCompanyId = loc.getHeadsetCompanyId();
                done = true;
            }


            if (loc.getShopId().equalsIgnoreCase(token.getShopId())) {
                location = loc;
                if (done) {
                    break;
                }
            }
        }

        Headset headset = null;
        String apiKey = null;
        if (location == null || StringUtils.isBlank(location.getApiKey())) {
            // try to get a new apiKey for this location

            Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());
            HeadsetAddRequest addRequest = new HeadsetAddRequest();
            addRequest.setCompanyId((headsetCompanyId == null ? 0 : headsetCompanyId)); // 0 for new companyId, 0 > for updates
            addRequest.setName(shop.getName());
            addRequest.setContactEmail(shop.getEmailAdress());
            addRequest.setContactName(shop.getName());
            if (shop.getAddress() != null) {
                HashMap<String, String> timeZoneMap = DateUtil.getTimezoneMap();

                addRequest.setAddress1(shop.getAddress().getAddress());
                addRequest.setCity(shop.getAddress().getCity());
                addRequest.setState(shop.getAddress().getState());
                addRequest.setZip(shop.getAddress().getZipCode());
                addRequest.setTimezone(timeZoneMap.get(shop.getTimeZone()));

                if (StringUtils.isBlank(shop.getAddress().getAddress()) ||
                        StringUtils.isBlank(shop.getAddress().getCity()) ||
                        StringUtils.isBlank(shop.getAddress().getState()) ||
                        StringUtils.isBlank(shop.getAddress().getZipCode())) {
                    throw new BlazeInvalidArgException("ShopAddress", String.format("Shop address is not complete for '%s'. Please complete address info in shop settings.", shop.getName()));
                }

                if (StringUtils.isBlank(shop.getTimeZone())) {
                    throw new BlazeInvalidArgException("ShopAddress", String.format("Shop timezone is not specified for '%s'. Please specify address info in shop settings.", shop.getName()));
                }
            }

            HeadsetConfig headsetConfig = integrationSettingRepository.getHeadsetConfig(location.getEnvironment());

            addRequest.setPartner(headsetConfig.getPartner());
            addRequest.setPartnerKey(headsetConfig.getPartnerKey());
            try {
                headset = headsetAPIService.addAccount(headsetConfig, addRequest);

                // headsetCompanyId & integrationKey should be the same.. but we're re-assigning in case they are null.
                headsetCompanyId = headset.getCompanyId();
                integrationKey = headset.getIntegrationKey();
                apiKey = headset.getApiKey();
            } catch (RestErrorException e) {
                if (e.getErrorResponse() instanceof HeadsetError) {
                    HeadsetError error = (HeadsetError) e.getErrorResponse();
                    if (error != null) {
                        LOG.error("Error creating headset account", e);
                        throw new BlazeInvalidArgException("Headset", error.getErrorsAsString());
                    }
                }
                LOG.error("Error creating headset account", e);
                throw new BlazeInvalidArgException("Headset", "Issue creating a headset account.");
            } catch (Exception e) {
                // throw exception
                LOG.error("Error creating headset account", e);
                throw new BlazeInvalidArgException("Headset", "Issue creating a headset account.");
            }
        }
        if (apiKey == null) {
            LOG.error("Headset API Key was null");
            throw new BlazeInvalidArgException("Headset", "Issue creating a headset account.");
        }

        if (location == null) {

            location = new HeadsetLocation();
            location.prepare(token.getCompanyId());
            location.setShopId(token.getShopId());
            location.setAcceptedLiteVersion(true);
            location.setLiteVersionAcceptedDate(DateTime.now().getMillis());
            location.setApiKey(apiKey);
            location.setHeadsetCompanyId(headset.getCompanyId());
            location.setIntegrationKey(headset.getIntegrationKey());
            location.setEnableSync(true);
            location.setEnableSyncDate(DateTime.now().getMillis());
            headsetLocationRepository.save(location);
        } else {
            location.setApiKey(apiKey);
            location.setHeadsetCompanyId(headsetCompanyId);
            location.setIntegrationKey(integrationKey);

            location.setAcceptedLiteVersion(true);
            location.setLiteVersionAcceptedDate(DateTime.now().getMillis());
            location.setEnableSync(true);
            location.setEnableSyncDate(DateTime.now().getMillis());
            headsetLocationRepository.update(token.getCompanyId(), location.getId(), location);
        }
        return location;
    }

    @Override
    public HeadsetLocation updateHeadsetLocation(String headsetLocationId, HeadsetLocation headsetLocation) {
        HeadsetLocation dbLocation = headsetLocationRepository.get(token.getCompanyId(), headsetLocationId);
        if (dbLocation == null) {
            throw new BlazeInvalidArgException("HeadsetLocation", "HeadsetLocation has not been created for this shop.");
        }

        if (dbLocation.isAcceptedLiteVersion() != headsetLocation.isAcceptedLiteVersion()) {

            if (headsetLocation.isAcceptedLiteVersion()) {
                // we're changing acceptance date. Only allow if apiKey is set
                if (StringUtils.isBlank(dbLocation.getApiKey())) {
                    throw new BlazeInvalidArgException("HeadsetLocation", "Please accept the headset terms through the dashboard.");
                }
                dbLocation.setAcceptedLiteVersion(headsetLocation.isAcceptedLiteVersion());
                dbLocation.setLiteVersionAcceptedDate(DateTime.now().getMillis());
            } else {
                dbLocation.setAcceptedLiteVersion(false);
                dbLocation.setLiteVersionAcceptedDate(null);
            }
        }
        // we're changing enable sync
        if (dbLocation.isEnableSync() != headsetLocation.isEnableSync()) {
            dbLocation.setEnableSyncDate(DateTime.now().getMillis());
            dbLocation.setEnableSync(headsetLocation.isEnableSync());
        }

        headsetLocationRepository.update(token.getCompanyId(), dbLocation.getId(), dbLocation);

        return dbLocation;
    }

    @Override
    public HeadsetLocation getHeadsetAccount() {
        HeadsetLocation account = headsetLocationRepository.getHeadsetLocation(token.getCompanyId(), token.getShopId());
        if (account == null) {
            account = new HeadsetLocation();
            account.prepare(token.getCompanyId());
            account.setShopId(token.getShopId());
            account.setAcceptedLiteVersion(false);
            account.setLiteVersionAcceptedDate(null);
            headsetLocationRepository.save(account);
        }
        return account;
    }
}
