package com.fourtwenty.core.services.thirdparty.impl;

import com.blaze.clients.hypur.HypurInvoiceAPIService;
import com.blaze.clients.hypur.HypurPaymentAPIService;
import com.blaze.clients.hypur.models.*;
import com.fourtwenty.core.domain.models.thirdparty.ThirdPartyShopAccount;
import com.fourtwenty.core.domain.repositories.thirdparty.ThirdPartyShopAccountRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.thirdparty.HypurService;
import com.google.inject.Inject;
import com.google.inject.Provider;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.List;

/**
 * Created by mdo on 9/14/17.
 */
public class HypurServiceImpl extends AbstractAuthServiceImpl implements HypurService {
    private static final Log LOG = LogFactory.getLog(HypurServiceImpl.class);
    @Inject
    ThirdPartyShopAccountRepository thirdPartyShopAccountRepository;
    @Inject
    HypurPaymentAPIService hypurPaymentAPIService;
    @Inject
    HypurInvoiceAPIService hypurInvoiceAPIService;


    @Inject
    public HypurServiceImpl(Provider<ConnectAuthToken> tokenProvider) {
        super(tokenProvider);
    }

    @Override
    public ThirdPartyShopAccount getCurrentHypurAccount() {
        ThirdPartyShopAccount hypurAccount = thirdPartyShopAccountRepository.getTPShopAccount(token.getCompanyId(), token.getShopId(), ThirdPartyShopAccount.ThirdPartyShopAccountType.Hypur);
        if (hypurAccount == null) {
            try {
                // create a new one automatically
                hypurAccount = new ThirdPartyShopAccount();
                hypurAccount.prepare(token.getCompanyId());
                hypurAccount.setShopId(token.getShopId());
                hypurAccount.setAccountType(ThirdPartyShopAccount.ThirdPartyShopAccountType.Hypur);
                hypurAccount.setEnabled(false);
                thirdPartyShopAccountRepository.save(hypurAccount);
            } catch (Exception e) {
                // LO
                LOG.error("Error creating hypur account", e);
            }
        }
        return hypurAccount;
    }

    @Override
    public ThirdPartyShopAccount updateHypurAccount(ThirdPartyShopAccount hypurAccount) {
        ThirdPartyShopAccount dbHypurAccount = getCurrentHypurAccount();

        if (!dbHypurAccount.getShopApiKey().equals(hypurAccount.getShopApiKey())) {
            CheckedInUserListResponse checkedInUsers = hypurPaymentAPIService.getCheckedInUsers(hypurAccount.getShopApiKey(), 1);
            if (!checkedInUsers.getErrorCodes().isEmpty()) {
                // If API Key doesn't work
                throw new BlazeInvalidArgException("Hypur", "API Key is Invalid");
            }
        }

        dbHypurAccount.setEnabled(hypurAccount.isEnabled());
        dbHypurAccount.setSettings(hypurAccount.getSettings());
        dbHypurAccount.setShopApiKey(hypurAccount.getShopApiKey());

        thirdPartyShopAccountRepository.update(token.getCompanyId(), dbHypurAccount.getId(), dbHypurAccount);
        return dbHypurAccount;
    }

    @Override
    public CheckInTestUsersResponse generateHypurTestUsers() {
        // Merchant
        ThirdPartyShopAccount dbHypurAccount = getCurrentHypurAccount();
        if (dbHypurAccount.getShopApiKey() == null) {
            throw new BlazeInvalidArgException("Hypur", "Shop Key can't be null.");
        }
        try {
            return hypurPaymentAPIService.genCheckedInTestUsers(dbHypurAccount.getShopApiKey()); // shopApiKey is the merchant token
        } catch (Exception e) {
            throw new BlazeInvalidArgException("Hypur", e.getMessage());
        }
    }


    @Override
    public CheckedInUserListResponse getHypurCheckinUserProfiles(int thumnailSize) {
        ThirdPartyShopAccount dbHypurAccount = getCurrentHypurAccount();
        try {
            return hypurPaymentAPIService.getCheckedInUsers(dbHypurAccount.getShopApiKey(), thumnailSize);
        } catch (Exception e) {
            throw new BlazeInvalidArgException("Hypur", e.getMessage());
        }
    }

    /*
    NOTE: DO NOT EXPOSE as rest apis. This method will be used internally when a transaction is processed later.
     */
    @Override
    public CheckInTestUsersResponse makeHypurPayment(HypurPayment hypurPayment) {
        ThirdPartyShopAccount dbHypurAccount = getCurrentHypurAccount();
        try {
            return hypurPaymentAPIService.processPayment(hypurPayment, dbHypurAccount.getShopApiKey());
        } catch (Exception e) {
            throw new BlazeInvalidArgException("Hypur", e.getMessage());
        }
    }

    /*
    NOTE: DO NOT EXPOSE as rest apis. This method will be used internally when a transaction is processed later.
     */
    @Override
    public CheckInTestUsersResponse refundHypurPayment(HypurRefund hypurRefund) {
        ThirdPartyShopAccount dbHypurAccount = getCurrentHypurAccount();
        try {
            return hypurPaymentAPIService.refundPayment(hypurRefund, dbHypurAccount.getShopApiKey());
        } catch (Exception e) {
            throw new BlazeInvalidArgException("Hypur", e.getMessage());
        }
    }

    @Override
    public String getHypurInvoiceErrorCodes() {
        try {
            return hypurInvoiceAPIService.getInvoiceErrorCodes();
        } catch (Exception e) {
            throw new BlazeInvalidArgException("Hypur", e.getMessage());
        }
    }

    @Override
    public HypurInvoiceList addHypurInvoices(List<HypurInvoice> hypurInvoice) {
        ThirdPartyShopAccount dbHypurAccount = getCurrentHypurAccount();
        try {
            return hypurInvoiceAPIService.addInvoices(hypurInvoice, dbHypurAccount.getShopApiKey());
        } catch (Exception e) {
            throw new BlazeInvalidArgException("Hypur", e.getMessage());
        }
    }

    @Override
    public HypurInvoice getHypurInvoiceDetails(String invoiceNumber) {
        ThirdPartyShopAccount dbHypurAccount = getCurrentHypurAccount();
        try {
            return hypurInvoiceAPIService.getInvoiceDetails(dbHypurAccount.getShopApiKey(), invoiceNumber);
        } catch (Exception e) {
            throw new BlazeInvalidArgException("Hypur", e.getMessage());
        }
    }

    @Override
    public HypurInvoiceList getHypurInvoices(String pageNumber, String itemsPerPage) {
        ThirdPartyShopAccount dbHypurAccount = getCurrentHypurAccount();
        try {
            return hypurInvoiceAPIService.getInvoices(dbHypurAccount.getShopApiKey(), pageNumber, itemsPerPage);
        } catch (Exception e) {
            throw new BlazeInvalidArgException("Hypur", e.getMessage());
        }
    }

    @Override
    public InvoiceResponse addHypurInvoice(HypurInvoice hypurInvoice) {
        ThirdPartyShopAccount dbHypurAccount = getCurrentHypurAccount();
        try {
            return hypurInvoiceAPIService.addInvoice(hypurInvoice, dbHypurAccount.getShopApiKey());
        } catch (Exception e) {
            throw new BlazeInvalidArgException("Hypur", e.getMessage());
        }
    }

    @Override
    public InvoiceResponse updateHypurInvoice(HypurInvoice hypurInvoice) {
        ThirdPartyShopAccount dbHypurAccount = getCurrentHypurAccount();
        try {
            return hypurInvoiceAPIService.updateInvoice(hypurInvoice, dbHypurAccount.getShopApiKey());
        } catch (Exception e) {
            throw new BlazeInvalidArgException("Hypur", e.getMessage());
        }
    }
}
