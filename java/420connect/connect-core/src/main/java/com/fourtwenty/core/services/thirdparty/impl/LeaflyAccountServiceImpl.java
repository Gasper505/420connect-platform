package com.fourtwenty.core.services.thirdparty.impl;

import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.thirdparty.leafly.LeaflyAccount;
import com.fourtwenty.core.domain.models.thirdparty.leafly.LeaflyCategoryMapItem;
import com.fourtwenty.core.domain.models.thirdparty.leafly.LeaflySyncJob;
import com.fourtwenty.core.domain.models.thirdparty.leafly.LeaflyVariantUnitMap;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.domain.repositories.thirdparty.LeaflyAccountRepository;
import com.fourtwenty.core.domain.repositories.thirdparty.LeaflySyncJobRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.common.BackgroundJobService;
import com.fourtwenty.core.services.thirdparty.LeaflyAccountService;
import com.fourtwenty.core.thirdparty.leafly.result.LeaflyAccountResult;
import com.google.inject.Inject;
import com.google.inject.Provider;
import org.apache.commons.lang3.StringUtils;
import org.assertj.core.util.Lists;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class LeaflyAccountServiceImpl extends AbstractAuthServiceImpl implements LeaflyAccountService {

    @Inject
    private LeaflyAccountRepository leaflyAccountRepository;
    @Inject
    private ShopRepository shopRepository;
    @Inject
    private LeaflySyncJobRepository leaflySyncJobRepository;
    @Inject
    private BackgroundJobService backgroundJobService;
    @Inject
    private LeaflyConstants leaflyConstants;
    @Inject
    private ProductRepository productRepository;

    @Inject
    public LeaflyAccountServiceImpl(Provider<ConnectAuthToken> token) {
        super(token);
    }

    @Override
    public SearchResult<LeaflyAccountResult> searchLeaflyAccounts(int start, int limit) {
        if (limit == 0) {
            limit = Integer.MAX_VALUE;
        }
        SearchResult<LeaflyAccountResult> items = leaflyAccountRepository.findItems(token.getCompanyId(), start, limit, LeaflyAccountResult.class);
        this.prepareLeaflyAccountsResult(items);
        return items;
    }

    @Override
    public LeaflyAccountResult getLeaflyAccountById(String id) {
        LeaflyAccountResult account = leaflyAccountRepository.get(token.getCompanyId(), id, LeaflyAccountResult.class);
        if (account == null) {
            throw new BlazeInvalidArgException(LeaflyConstants.LEAFLY_ACCOUNT, LeaflyConstants.ACCOUNT_NOT_FOUND);
        }
        Shop shop = shopRepository.get(token.getCompanyId(), account.getShopId());
        if (shop != null) {
            this.prepareLeaflyAccountResult(account, shop);
        }

        return account;
    }

    @Override
    public LeaflyAccount createLeaflyAccount(LeaflyAccount account) {

        LeaflyAccount dbLeaflyAccount = leaflyAccountRepository.getAccountByShop(token.getCompanyId(), account.getShopId());
        if (dbLeaflyAccount != null) {
            throw new BlazeInvalidArgException(LeaflyConstants.LEAFLY_ACCOUNT, LeaflyConstants.ACCOUNT_ALREADY_EXIST);
        }
        Shop shop = shopRepository.get(token.getCompanyId(), account.getShopId());
        if (shop == null) {
            throw new BlazeInvalidArgException(LeaflyConstants.SHOP, LeaflyConstants.SHOP_NOT_FOUND);
        }
        if (account.getApiKey() == null) {
            throw new BlazeInvalidArgException(LeaflyConstants.LEAFLY_ACCOUNT, LeaflyConstants.API_KEY_NOT_FOUND);
        }
        if (account.getClientKey() == null) {
            throw new BlazeInvalidArgException(LeaflyConstants.LEAFLY_ACCOUNT, LeaflyConstants.CLIENT_KEY_NOT_FOUND);
        }

        List<LeaflyCategoryMapItem> leaflyCategoryMapItems = Lists.newArrayList(account.getCategoryMapping());
        List<LeaflyVariantUnitMap> leaflyVariantUnitMaps = Lists.newArrayList(account.getVariantUnitMapping());
        LeaflyAccount leaflyAccount = new LeaflyAccount();
        leaflyAccount.prepare(token.getCompanyId());
        leaflyAccount.setShopId(account.getShopId());
        leaflyAccount.setApiKey(account.getApiKey())
                .setClientKey(account.getClientKey())
                .setSyncEnabled(account.isSyncEnabled())
                .setCategoryMapping(leaflyCategoryMapItems.isEmpty() ? null : leaflyCategoryMapItems)
                .setVariantUnitMapping(leaflyVariantUnitMaps.isEmpty() ? null : leaflyVariantUnitMaps);

        return leaflyAccountRepository.save(leaflyAccount);
    }

    @Override
    public LeaflyAccount updateLeaflyAccount(LeaflyAccount request, String id) {

        LeaflyAccount dbAccount = leaflyAccountRepository.get(token.getCompanyId(), id);
        if (dbAccount == null) {
            throw new BlazeInvalidArgException(LeaflyConstants.LEAFLY_ACCOUNT, LeaflyConstants.ACCOUNT_NOT_FOUND);
        }
        if (!dbAccount.isDeleted() && !dbAccount.getId().equalsIgnoreCase(id)) {
            throw new BlazeInvalidArgException(LeaflyConstants.LEAFLY_ACCOUNT, LeaflyConstants.ACCOUNT_ALREADY_EXIST);
        }
        if (StringUtils.isBlank(request.getApiKey())) {
            throw new BlazeInvalidArgException(LeaflyConstants.LEAFLY_ACCOUNT, LeaflyConstants.API_KEY_NOT_FOUND);
        }
        if (StringUtils.isBlank(request.getClientKey())) {
            throw new BlazeInvalidArgException(LeaflyConstants.LEAFLY_ACCOUNT, LeaflyConstants.CLIENT_KEY_NOT_FOUND);
        }

        List<LeaflyCategoryMapItem> leaflyCategoryMapItems = Lists.newArrayList(dbAccount.getCategoryMapping());
        leaflyCategoryMapItems = prepareLeaflyCategoryMapping(dbAccount, request, leaflyCategoryMapItems);
        List<LeaflyVariantUnitMap> leaflyVariantUnitMaps = Lists.newArrayList(request.getVariantUnitMapping());
        leaflyVariantUnitMaps = prepareLeaflyVariantUnitMaps(dbAccount, request, leaflyVariantUnitMaps);
        Boolean keyUpdate = Boolean.TRUE;
        if (dbAccount.getApiKey().equalsIgnoreCase(request.getApiKey().trim()) || dbAccount.getClientKey().equalsIgnoreCase(request.getClientKey().trim())) {
            keyUpdate = dbAccount.getKeyUpdate();
        }
        dbAccount.setApiKey(request.getApiKey())
                .setClientKey(request.getClientKey())
                .setSyncEnabled(request.isSyncEnabled())
                .setKeyUpdate(keyUpdate)
                .setCategoryMapping(leaflyCategoryMapItems)
                .setVariantUnitMapping(leaflyVariantUnitMaps);

        return leaflyAccountRepository.update(token.getCompanyId(), id, dbAccount);
    }

    @Override
    public void deleteLeaflyAccount(String id) {
        LeaflyAccount dbAccount = leaflyAccountRepository.get(token.getCompanyId(), id);
        if (dbAccount == null) {
            throw new BlazeInvalidArgException(LeaflyConstants.LEAFLY_ACCOUNT, LeaflyConstants.ACCOUNT_NOT_FOUND);
        }
        leaflyAccountRepository.removeById(token.getCompanyId(), id);
    }

    @Override
    public void resetMenu(boolean shouldDelete) {
        LeaflyAccountResult dbAccount = leaflyAccountRepository.getAccountByShop(token.getCompanyId(), token.getShopId());
        if (dbAccount == null) {
            throw new BlazeInvalidArgException(LeaflyConstants.LEAFLY_ACCOUNT, LeaflyConstants.ACCOUNT_ALREADY_EXIST);
        }
        dbAccount.setLastSync(LeaflyConstants.LONGZERO);
        dbAccount.setMenuSyncRequestDate(LeaflyConstants.LONGZERO);
        dbAccount.setSyncStatus(LeaflyAccount.LeaflySyncStatus.Reset);
        leaflyAccountRepository.update(dbAccount.getCompanyId(), dbAccount.getId(), dbAccount);
        if (shouldDelete) {
            List<Product> productList = productRepository.getActiveProductList(dbAccount.getCompanyId(), dbAccount.getShopId());
            List<String> productIds = new ArrayList<>();
            for (Product product : productList) {
                productIds.add(product.getId());
            }
            leaflyConstants.createLeaflySyncJob(dbAccount.getCompanyId(), dbAccount.getShopId(), token.getActiveTopUser().getUserId(), LeaflySyncJob.LeaflySyncType.RESET, LeaflySyncJob.LeaflySyncJobStatus.Queued, productIds, dbAccount.getId(), leaflySyncJobRepository, backgroundJobService);
        }
    }

    private void prepareLeaflyAccountsResult(SearchResult<LeaflyAccountResult> items) {
        HashMap<String, Shop> shopMap = shopRepository.listAsMap(token.getCompanyId());
        for (LeaflyAccountResult accountResult : items.getValues()) {
            Shop shop = shopMap.get(accountResult.getShopId());
            if (shop != null) {
                this.prepareLeaflyAccountResult(accountResult, shop);
            }
        }
    }

    private void prepareLeaflyAccountResult(LeaflyAccountResult accountResult, Shop shop) {
        accountResult.setShopName(shop.getName());
    }

    private List<LeaflyCategoryMapItem> prepareLeaflyCategoryMapping(LeaflyAccount dbAccount, LeaflyAccount request, List<LeaflyCategoryMapItem> leaflyCategoryItems) {
        if (dbAccount.getCategoryMapping() != null && request.getCategoryMapping() != null && !request.getCategoryMapping().isEmpty()) {
            for (LeaflyCategoryMapItem categoryMapItem : dbAccount.getCategoryMapping()) {
                boolean isRemoved = request.getCategoryMapping()
                        .stream()
                        .filter(m -> categoryMapItem.getFlowerType().equalsIgnoreCase(m.getFlowerType()) && (categoryMapItem.getLeaflyCategory() == m.getLeaflyCategory()))
                        .findFirst()
                        .map(m -> false)
                        .orElse(true);
                if (!isRemoved) {
                    leaflyCategoryItems.remove(categoryMapItem);
                }
            }
        }
        return leaflyCategoryItems;
    }

    private List<LeaflyVariantUnitMap> prepareLeaflyVariantUnitMaps(LeaflyAccount dbAccount, LeaflyAccount request, List<LeaflyVariantUnitMap> leaflyVariantUnitMaps) {
        if (dbAccount.getVariantUnitMapping() != null && request.getVariantUnitMapping() != null && !request.getVariantUnitMapping().isEmpty()) {
            for (LeaflyVariantUnitMap variantUnitMap : dbAccount.getVariantUnitMapping()) {
                boolean isRemoved = request.getVariantUnitMapping()
                        .stream()
                        .filter(m -> variantUnitMap.getVariantUnit().equalsIgnoreCase(m.getVariantUnit()) && (variantUnitMap.getVariantUnit() == m.getVariantUnit()))
                        .findFirst()
                        .map(m -> false)
                        .orElse(true);
                if (!isRemoved) {
                    leaflyVariantUnitMaps.remove(variantUnitMap);
                }
            }
        }
        return leaflyVariantUnitMaps;
    }
}
