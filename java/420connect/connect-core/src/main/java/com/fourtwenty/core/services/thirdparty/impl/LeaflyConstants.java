package com.fourtwenty.core.services.thirdparty.impl;

import com.fourtwenty.core.domain.models.thirdparty.leafly.LeaflySyncJob;
import com.fourtwenty.core.domain.repositories.thirdparty.LeaflySyncJobRepository;
import com.fourtwenty.core.services.common.BackgroundJobService;
import org.joda.time.DateTime;

import java.util.List;

public class LeaflyConstants {
    protected static final String LEAFLY_ACCOUNT = "Leafly account";
    protected static final String ACCOUNT_NOT_FOUND = "Leafly account not found";
    public static final String ACCOUNT_DOES_NOT_EXIST = "Leafly account does not exist";
    protected static final String ACCOUNT_ALREADY_EXIST = "Leafly account already exist for this shop";
    protected static final String BLANK_DELETE_ACCOUNT = "Account not available for leafly account(s)";
    protected static final String PROGRESS_ACCOUNT_IDS = "Account Sync is already in progress for leafly account(s)";
    protected static final String SHOP = "Shop";
    protected static final String SHOP_NOT_FOUND = "Shop not found";
    protected static final String BLANK_API_KEY_ACCOUNT = "Api key is not available for leafly account(s)";
    protected static final String API_KEY_NOT_FOUND = "API key not found";
    public static final String EMPTY_API_KEY = "Api key is empty";
    protected static final String CLIENT_KEY_NOT_FOUND = "Client key not found";
    public static final String SUCCESS = "Success";
    public static final String FAIL = "Fail to Sync";
    public static final String OK = "OK";
    public static final String LEAFLY_SYNC_JOB = "LeaflySyncJob";
    public static final String LEAFLY_SYNC_QUARTZ_TRIGGER = "LeaflySyncQuartzTrigger";
    public static final String LEAFLY_SYNC = "LeaflySync";
    public static final String EMPTY_SYNC_PRODUT_LIST = "Sync products list is empty.";
    public static final String BLANK_COMPANY_SHOP = "Company or Shop cannot be blank.";
    public static final String BLANK_JOB_ACCOUNT_ID = "Account id is blank in job.";
    public static final String SYNC_JOB_TYPE_NOT_FOUND = "Sync Job type not found";
    protected static final long LONGZERO = 0L;
    public static final Double DOUBLEZERO = 0d;
    public static final String TIME_ZONE = "America/Los_Angeles";

    protected void createLeaflySyncJob(String companyId, String shopId, String employeeId, LeaflySyncJob.LeaflySyncType syncType, LeaflySyncJob.LeaflySyncJobStatus syncStatus, List<String> productIds, String accountId, LeaflySyncJobRepository leaflySyncJobRepository, BackgroundJobService backgroundJobService) {
        LeaflySyncJob job = new LeaflySyncJob();
        job.prepare(companyId);
        job.setShopId(shopId);
        job.setEmployeeId(employeeId)
                .setStartTime(DateTime.now().getMillis())
                .setRequestTime(DateTime.now().getMillis())
                .setJobType(syncType)
                .setStatus(syncStatus)
                .setProductIds(productIds)
                .setAccountId(accountId);
        leaflySyncJobRepository.save(job);
        backgroundJobService.addLeaflymapSyncJob(companyId, shopId, job.getId());
    }
}
