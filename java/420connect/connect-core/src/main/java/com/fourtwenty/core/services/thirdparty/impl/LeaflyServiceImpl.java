package com.fourtwenty.core.services.thirdparty.impl;

import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.thirdparty.leafly.LeaflyAccount;
import com.fourtwenty.core.domain.models.thirdparty.leafly.LeaflySyncJob;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.thirdparty.LeaflyAccountRepository;
import com.fourtwenty.core.domain.repositories.thirdparty.LeaflySyncJobRepository;
import com.fourtwenty.core.services.common.BackgroundJobService;
import com.fourtwenty.core.services.thirdparty.LeaflyService;
import com.google.inject.Inject;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class LeaflyServiceImpl implements LeaflyService {
    static final Logger LOGGER = LoggerFactory.getLogger(LeaflyServiceImpl.class);

    @Inject
    private LeaflyAccountRepository leaflyAccountRepository;
    @Inject
    private LeaflySyncJobRepository leaflySyncJobRepository;
    @Inject
    private BackgroundJobService backgroundJobService;
    @Inject
    private ProductRepository productRepository;
    @Inject
    private LeaflyConstants leaflyConstants;

    @Override
    public void syncAllMenu() {
        Iterable<LeaflyAccount> leaflyAccounts = leaflyAccountRepository.list();
        Set<String> deletedAccountIds = new HashSet<>();
        Set<String> blankApiKeyAccountIds = new HashSet<>();
        Set<String> inProgressAccountIds = new HashSet<>();
        for (LeaflyAccount account : leaflyAccounts) {
            if (account.isDeleted()) {
                //All deleted leafly accounts
                deletedAccountIds.add(account.getId());
                continue;
            }
            if (StringUtils.isBlank(account.getApiKey())) {
                //All blank API key leafly accounts
                blankApiKeyAccountIds.add(account.getId());
                continue;
            }
            if (!account.isSyncEnabled()) {
                //Skip accounts which have not enabled sync status
                continue;
            }
            if (account.getMenuSyncRequestDate() != null && account.getSyncStatus() == LeaflyAccount.LeaflySyncStatus.InProgress) {
                DateTime lastSync = new DateTime(account.getMenuSyncRequestDate());
                if (lastSync.isAfter(DateTime.now().minusHours(1))) {
                    //All accounts with sync status already in progress
                    inProgressAccountIds.add(account.getId());
                    continue;
                }
            }
            account.setMenuSyncRequestDate(DateTime.now().getMillis());
            account.setSyncStatus(LeaflyAccount.LeaflySyncStatus.InProgress);
            leaflyAccountRepository.update(account.getCompanyId(), account.getId(), account);
            List<Product> productList = productRepository.getActiveProductList(account.getCompanyId(), account.getShopId());
            List<String> productIds = new ArrayList<>();
            for (Product product : productList) {
                productIds.add(product.getId());
            }
            //Creating leafly sync job
            leaflyConstants.createLeaflySyncJob(account.getCompanyId(), account.getShopId(), null, LeaflySyncJob.LeaflySyncType.NIGHTLY_BASIS, LeaflySyncJob.LeaflySyncJobStatus.Queued, productIds, null, leaflySyncJobRepository, backgroundJobService);
        }
        //Validations
        if (!deletedAccountIds.isEmpty()) {
            LOGGER.info(String.format(LeaflyConstants.BLANK_DELETE_ACCOUNT + " %s", deletedAccountIds));
        }
        if (!blankApiKeyAccountIds.isEmpty()) {
            LOGGER.info(String.format(LeaflyConstants.BLANK_API_KEY_ACCOUNT + " %s", blankApiKeyAccountIds));
        }
        if (!inProgressAccountIds.isEmpty()) {
            LOGGER.info(String.format(LeaflyConstants.PROGRESS_ACCOUNT_IDS + " %s", inProgressAccountIds));
        }
    }
}
