package com.fourtwenty.core.services.thirdparty.impl;

import com.blaze.clients.metrcs.MetrcAuthorization;
import com.blaze.clients.metrcs.MetrcMeasurement;
import com.blaze.clients.metrcs.MetricsFacilitiesApiService;
import com.blaze.clients.metrcs.MetricsPackagesAPIService;
import com.blaze.clients.metrcs.models.facilities.MetricsFacilityList;
import com.blaze.clients.metrcs.models.packages.MetricsPackages;
import com.blaze.clients.metrcs.models.packages.MetricsPackagesList;
import com.fourtwenty.core.domain.models.common.IntegrationSetting;
import com.fourtwenty.core.domain.models.common.IntegrationSettingConstants;
import com.fourtwenty.core.domain.models.common.MetrcConfig;
import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.domain.models.product.ProductWeightTolerance;
import com.fourtwenty.core.domain.models.thirdparty.MetrcAccount;
import com.fourtwenty.core.domain.models.thirdparty.MetrcFacilityAccount;
import com.fourtwenty.core.domain.repositories.common.IntegrationSettingRepository;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.domain.repositories.thirdparty.MetrcAccountRepository;
import com.fourtwenty.core.event.inventory.ComplianceBatch;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.thirdparty.MetrcAccountService;
import com.fourtwenty.core.services.thirdparty.MetrcService;
import com.fourtwenty.core.services.thirdparty.models.MetrcVerifiedPackage;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.core.util.MeasurementConversionUtil;
import com.google.inject.Inject;
import com.google.inject.Provider;
import joptsimple.internal.Strings;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.assertj.core.util.Lists;

import java.math.BigDecimal;
import java.util.*;
import java.util.function.Predicate;

/**
 * Created by mdo on 8/25/17.
 */
public class MetrcAccountServiceImpl extends AbstractAuthServiceImpl implements MetrcAccountService {
    private static final Log LOG = LogFactory.getLog(MetrcServiceImpl.class);
    private static final String METRC = "Metrc";
    private static final String METRC_LABEL_NOT_BLANK = "Metrc Label cannot be blank.";


    @Inject
    MetrcAccountRepository metrcAccountRepository;
    @Inject
    ShopRepository shopRepository;
    @Inject
    ProductBatchRepository batchRepository;
    @Inject
    private CompanyRepository companyRepository;
    @Inject
    ProductWeightToleranceRepository toleranceRepository;
    @Inject
    ProductRepository productRepository;
    @Inject
    ProductCategoryRepository productCategoryRepository;

    @Inject
    MetrcService metrcService;
    @Inject
    private IntegrationSettingRepository integrationSettingRepository;
    @Inject
    Provider<ConnectAuthToken> tokenProvider;

    @Inject
    public MetrcAccountServiceImpl(Provider<ConnectAuthToken> tokenProvider) {
        super(tokenProvider);
    }


    @Override
    public List<MetrcAccount> getMetrcAccount(final String companyId, String stateCode) {
        Iterable<MetrcAccount> metrcs = metrcAccountRepository.list(companyId);
        ArrayList<MetrcAccount> metrcAccounts = Lists.newArrayList(metrcs);

        if (metrcAccounts.size() == 0) {
            MetrcAccount metrcAccount = new MetrcAccount();
            metrcAccount.prepare(token.getCompanyId());
            metrcAccount.setStateCode(stateCode);
            MetrcAccount dbMetrc = metrcAccountRepository.save(metrcAccount);
            metrcAccounts.add(dbMetrc);
        }

        Company company = companyRepository.getById(companyId);
        String companyState = "";
        if (company != null && company.getAddress() != null && company.getAddress().getState() != null) {
            companyState = company.getAddress().getState().toUpperCase().trim();
        }

        // Fill in the shops
        final HashMap<String, Shop> shopHashMap = shopRepository.listAsMap(token.getCompanyId());

        for (MetrcAccount metrcAccount : metrcAccounts) {
            if (StringUtils.isBlank(metrcAccount.getStateCode())) {
                metrcAccount.setStateCode(companyState);
            }
            int currFacilities = metrcAccount.getFacilities().size();
            metrcAccount.getFacilities().removeIf(new Predicate<MetrcFacilityAccount>() {
                @Override
                public boolean test(MetrcFacilityAccount metrcFacilityAccount) {
                    Shop shop = shopHashMap.get(metrcFacilityAccount.getShopId());
                    return shop == null; // remove if shop isn't found
                }
            });


            // Prepare to add new facilities
            for (Shop shop : shopHashMap.values()) {
                if (shop.isActive()) {
                    boolean newFacility = true;
                    for (MetrcFacilityAccount facilityAccount : metrcAccount.getFacilities()) {
                        if (facilityAccount.getShopId() != null && facilityAccount.getShopId().equalsIgnoreCase(shop.getId())) {
                            newFacility = false;
                            break; // done
                        }
                    }

                    if (newFacility) {
                        MetrcFacilityAccount account = new MetrcFacilityAccount();
                        account.prepare(token.getCompanyId());
                        account.setShopId(shop.getId());
                        account.setStateCode(stateCode);
                        metrcAccount.getFacilities().add(account);
                    }
                }
            }


            if (currFacilities != metrcAccount.getFacilities().size()) {
                // update if facilities changed
                metrcAccountRepository.update(token.getCompanyId(), metrcAccount.getId(), metrcAccount);
            }
        }


        return metrcAccounts;

    }

    @Override
    public List<MetrcAccount> getMyMetrcAccount() {
        String state = metrcService.getShopStateCode(token.getCompanyId(), token.getShopId());
        return getMetrcAccount(token.getCompanyId(), state);
    }


    @Override
    public MetricsFacilityList getMetrcFacilities() {
        ConnectAuthToken token = tokenProvider.get();
        MetrcAuthorization metrcAuthorization = metrcService.getMetrcAuthorization(token.getCompanyId(), token.getShopId());

        MetricsFacilitiesApiService facilitiesApiService = new MetricsFacilitiesApiService(metrcAuthorization);
        try {
            MetricsFacilityList facilities = facilitiesApiService.getFacilities();
            return facilities;
        } catch (Exception e) {
            throw new BlazeInvalidArgException("Metrc", e.getMessage());
        }
    }

    @Override
    public Map<String, MetricsFacilityList> getMetrcFacilities(String env,String stateCode, String metrcApiKey) {
        ConnectAuthToken token = tokenProvider.get();
        MetrcAuthorization metrcAuthorization;
        IntegrationSetting.Environment environment = IntegrationSetting.Environment.valueOf(env);
        if (StringUtils.isNotBlank(metrcApiKey)) {
            metrcAuthorization = metrcService.createMetrcAuthorization(token.getCompanyId(), token.getShopId(), environment, stateCode, metrcApiKey);
        } else {
            throw new BlazeInvalidArgException("Metrc API Key", "Metrc Api key should not blank.");
        }

        MetricsFacilitiesApiService facilitiesApiService = new MetricsFacilitiesApiService(metrcAuthorization);
        try {
            MetricsFacilityList facilities = facilitiesApiService.getFacilities();

            Map<String, MetricsFacilityList> metricsFacilityListMap = new HashMap<>();
            metricsFacilityListMap.put(stateCode,facilities);
            return metricsFacilityListMap;
        } catch (Exception e) {
            throw new BlazeInvalidArgException("Metrc", e.getMessage());
        }
    }

    @Override
    public List<MetrcAccount> updateMetrcAccount(List<MetrcAccount> metrcAccounts) {
        List<MetrcAccount> metrcAccountList = new ArrayList<>();
        HashMap<String, MetrcAccount> metrcAccountHashMap = metrcAccountRepository.listAsMap(tokenProvider.get().getCompanyId());

        List<MetrcAccount> updatedMetrcAccount = prepareMetrcFacilitiesByStateCode(metrcAccounts);

        for (MetrcAccount metrcAccount : updatedMetrcAccount) {
            if (metrcAccount == null) {
                throw new BlazeInvalidArgException("Metrc", "Please provide a valid metrc account.");
            }
            if (metrcAccountHashMap.containsKey(metrcAccount.getId())) {
                MetrcAccount dbMetrc = metrcAccountHashMap.get(metrcAccount.getId());
                dbMetrc.setEnabled(metrcAccount.isEnabled());
                dbMetrc.setFacilities(metrcAccount.getFacilities());
                dbMetrc.setUserApiKey(metrcAccount.getUserApiKey());
                if (dbMetrc.getFacilities() == null) {
                    dbMetrc.setFacilities(new ArrayList<MetrcFacilityAccount>());
                } else {
                    // Prepare the shop
                    for (MetrcFacilityAccount facilityAccount : dbMetrc.getFacilities()) {
                        facilityAccount.prepare(token.getCompanyId());
                    }
                }
                dbMetrc.setEnvironment(metrcAccount.getEnvironment());
                dbMetrc.setStateCode(metrcAccount.getStateCode());
                MetrcAccount updateMetrc = metrcAccountRepository.update(dbMetrc.getId(), dbMetrc);
                metrcAccountList.add(updateMetrc);
            }
        }
        return metrcAccountList;
    }

    /**
     *
     * @param metrcAccounts : metrcAccounts
     * @return this method is responsible for updated metrc facility for metrc account
     */
    public List<MetrcAccount> prepareMetrcFacilitiesByStateCode(List<MetrcAccount> metrcAccounts) {
        Map<String, List<MetrcFacilityAccount>> metrcFacilityAccountMap = new HashMap<>();
        for (MetrcAccount metrcAccount : metrcAccounts) {
            if (StringUtils.isNotBlank(metrcAccount.getStateCode())) {
                metrcAccount.setStateCode(metrcAccount.getStateCode().toUpperCase());
            }
            List<MetrcFacilityAccount> facilities = metrcAccount.getFacilities();
            for (MetrcFacilityAccount facility : facilities) {
                facility.setStateCode(facility.getStateCode().toUpperCase());

                if (StringUtils.isBlank(facility.getUserApiKey())) {
                    facility.setEnabled(false);
                    facility.setFacLicense(Strings.EMPTY);
                }
                if (facility.isEnabled()) {
                    metrcAccount.setEnabled(true);
                    metrcAccount.setStateCode(facility.getStateCode());
                }
            }
        }

        return metrcAccounts;
    }

    @Override
    public MetricsPackages getMetrcPackageByLabel(String packageLabel) {
        if (isMetrcsEnableForShop()) {
            try {
                ConnectAuthToken token = tokenProvider.get();
                MetrcAuthorization metrcAuthorization = metrcService.getMetrcAuthorization(token.getCompanyId(), token.getShopId());

                MetricsPackagesAPIService metricsPackagesAPIService = new MetricsPackagesAPIService(metrcAuthorization);
                MetricsPackages metricsPackages = metricsPackagesAPIService.getPackageLabel(packageLabel);
                if (metricsPackages == null) {
                    throw new BlazeInvalidArgException("packageLabel", "Package with " + metricsPackages.getLabel() + " label does not exist in Metrc.");
                }
                return metricsPackages;
            } catch (Exception e) {
                throw new BlazeInvalidArgException("Package Label", "Package with " + packageLabel + " label does not exists in Metrc or Metrc is not set up for this shop.");
            }
        }
        return null;
    }

    @Override
    public MetrcVerifiedPackage getMetrcVerifiedPackage(String label) {
        // check to see if this package label is being used anywhere
        //long count = batchRepository.countProductBatchesForLabel(token.getCompanyId(), token.getShopId(), label, ProductBatch.TrackTraceSystem.METRC);
        MetrcVerifiedPackage metrcVerifiedPackage = new MetrcVerifiedPackage();
        metrcVerifiedPackage.setLabel(label);
        metrcVerifiedPackage.setAvailable(false);
        metrcVerifiedPackage.setRequired(false);

        MetricsPackages packages = getMetrcPackageByLabel(label);
        if (packages != null) {
            if (packages.isOnHold()) {
                throw new BlazeInvalidArgException("Package", "This package is currently on hold.");
            }
            if (StringUtils.isNotBlank(packages.getFinishedDate())) {
                throw new BlazeInvalidArgException("Package", "This package was finished in Metrc on " + packages.getFinishedDate());
            }
            metrcVerifiedPackage.setRequired(true);
            metrcVerifiedPackage.setAvailable(true);
            metrcVerifiedPackage.setPackageType(packages.getPackageType());
            metrcVerifiedPackage.setId(packages.getId());
            metrcVerifiedPackage.setLabel(label);
            metrcVerifiedPackage.setQuantity(packages.getQuantity());
            metrcVerifiedPackage.setUnitOfMeasureAbbreviation(packages.getUnitOfMeasureAbbreviation());
            metrcVerifiedPackage.setUnitOfMeasureName(packages.getUnitOfMeasureName());

            if (metrcVerifiedPackage.getUnitOfMeasureName().equalsIgnoreCase("Ounces")) {
                // convert to grams
                // Use the defined unit value from the tolerance for ounce
                ProductWeightTolerance ozTolerance = toleranceRepository.getToleranceForWeight(token.getCompanyId(),
                        ProductWeightTolerance.WeightKey.OUNCE);
                BigDecimal ozUnitValue = ProductWeightTolerance.WeightKey.OUNCE.weightValue;
                if (ozTolerance != null && ozTolerance.getUnitValue().doubleValue() > 0) {
                    ozUnitValue = ozTolerance.getUnitValue();
                }

                BigDecimal blazeQuantity = ozUnitValue.multiply(metrcVerifiedPackage.getQuantity());
                if (blazeQuantity.doubleValue() < 0) {
                    blazeQuantity = new BigDecimal(0);
                }
                metrcVerifiedPackage.setBlazeQuantity(blazeQuantity);
                metrcVerifiedPackage.setBlazeMeasurement(ProductWeightTolerance.WeightKey.GRAM);
            } else if (metrcVerifiedPackage.getUnitOfMeasureName().equalsIgnoreCase("Grams")) {
                // just pass it on (grams)
                metrcVerifiedPackage.setBlazeQuantity(packages.getQuantity());
                metrcVerifiedPackage.setBlazeMeasurement(ProductWeightTolerance.WeightKey.GRAM);
            } else {
                // just pass it on
                metrcVerifiedPackage.setBlazeQuantity(packages.getQuantity());
                metrcVerifiedPackage.setBlazeMeasurement(ProductWeightTolerance.WeightKey.UNIT);
            }
        }

        return metrcVerifiedPackage;
    }

    @Override
    public MetricsPackagesList getMetrcPackagesForCurrentShop() {
        String licenseNumber = getMetrcShopLicense();
        if (StringUtils.isBlank(licenseNumber)) {
            throw new BlazeInvalidArgException("Metrc", "The current shop is not assigned to a metrc facility.");
        }

        ConnectAuthToken token = tokenProvider.get();
        MetrcAuthorization metrcAuthorization = metrcService.getMetrcAuthorization(token.getCompanyId(), token.getShopId());

        MetricsPackagesAPIService metricsPackagesAPIService = new MetricsPackagesAPIService(metrcAuthorization);
        return metricsPackagesAPIService.getActivePackages(licenseNumber);
    }

    private String getMetrcShopLicense() {
        List<MetrcAccount> metrcAccounts = getMyMetrcAccount();
        HashMap<String, Shop> shopHashMap = shopRepository.listAsMap();
        Shop shop = shopHashMap.get(token.getShopId());
        String state = null;
        if (shop.getAddress() != null && StringUtils.isNotBlank(shop.getAddress().getState())) {
            state = shop.getAddress().getState();
        } else {
            throw new BlazeInvalidArgException("Shop", "Shop state does not found for metrc.");
        }

        String licenseNumber = null;
        for (MetrcAccount metrcAccount : metrcAccounts) {
            for (MetrcFacilityAccount facilityAccount : metrcAccount.getFacilities()) {
                if (token.getShopId().equalsIgnoreCase(facilityAccount.getShopId()) && facilityAccount.getStateCode().equalsIgnoreCase(state)) {
                    licenseNumber = facilityAccount.getFacLicense();
                    break;
                }
            }
        }
        return licenseNumber;
    }

    // New support to handle MetrcAuthorization and remove it from DI paradigm
    private MetrcFacilityAccount getMetrcFacilityAccountForShopId(String companyId, String shopId) {
        String stateCode = metrcService.getShopStateCode(companyId, shopId);
        MetrcAccount metrcAccount = metrcAccountRepository.getMetrcAccount(companyId, stateCode);
        if (metrcAccount != null && metrcAccount.getFacilities() != null) {
            for (MetrcFacilityAccount metrcFacilityAccount : metrcAccount.getFacilities()) {
                if (metrcFacilityAccount.getShopId().equalsIgnoreCase(shopId)) {
                    return metrcFacilityAccount;
                }
            }
        }
        return null;
    }


    private boolean isMetrcsEnableForShop() {
        List<MetrcAccount> metrcAccounts = getMyMetrcAccount();
        HashMap<String, Shop> shopHashMap = shopRepository.listAsMap();
        Shop shop = shopHashMap.get(token.getShopId());
        String state = null;
        if (shop.getAddress() != null && StringUtils.isNotBlank(shop.getAddress().getState())) {
            state = shop.getAddress().getState();
        } else {
            throw new BlazeInvalidArgException("Shop", "Shop state does not found for metrc.");
        }

        for (MetrcAccount metrcAccount : metrcAccounts) {
            if (StringUtils.isBlank(metrcAccount.getStateCode())) {
                continue;
            }
            for (MetrcFacilityAccount facilityAccount : metrcAccount.getFacilities()) {
                if (token.getShopId().equalsIgnoreCase(facilityAccount.getShopId()) && facilityAccount.getStateCode().equalsIgnoreCase(state)) {
                    String licenseNumber = facilityAccount.getFacLicense();
                    return facilityAccount.isEnabled() && StringUtils.isNotBlank(licenseNumber);
                }
            }
        }
        return false;
    }

    @Override
    public HashMap<String, MetricsPackages> getMetrcPackagesMap() {
        HashMap<String, MetricsPackages> metrcMap = new HashMap<>();
        String licenseNumber = getMetrcShopLicense();
        if (StringUtils.isBlank(licenseNumber)) {
            throw new BlazeInvalidArgException("Metrc", "The current shop is not assigned to a metrc facility.");
        }

        ConnectAuthToken token = tokenProvider.get();
        MetrcAuthorization metrcAuthorization = metrcService.getMetrcAuthorization(token.getCompanyId(), token.getShopId());

        MetricsPackagesAPIService metricsPackagesAPIService = new MetricsPackagesAPIService(metrcAuthorization);
        List<MetricsPackages> metricsPackagesList = metricsPackagesAPIService.getActivePackages(licenseNumber);
        if (!metricsPackagesList.isEmpty()) {
            for (MetricsPackages metricsPackages : metricsPackagesList) {
                metrcMap.put(metricsPackages.getLabel(), metricsPackages);
            }
        }
        return metrcMap;
    }

    @Override
    public Map<String, MetricsFacilityList> getMetrcFacilitiesByStateCode() {
        Map<String, MetricsFacilityList> metricsFacilityListMap = new HashMap<>();
        ConnectAuthToken token = tokenProvider.get();
        Map<String, MetrcAuthorization> metrcAuthorizations = metrcService.getMetrcAuthorizationDetails(token.getCompanyId(), token.getShopId());
        for (String stateCode : metrcAuthorizations.keySet()) {
            MetrcAuthorization metrcAuthorization = metrcAuthorizations.get(stateCode);
            MetricsFacilitiesApiService facilitiesApiService = new MetricsFacilitiesApiService(metrcAuthorization);
            try {
                MetricsFacilityList facilities = facilitiesApiService.getFacilities();
                metricsFacilityListMap.put(stateCode, facilities);

            } catch (Exception e) {
                throw new BlazeInvalidArgException("Metrc", e.getMessage());
            }
        }
        return metricsFacilityListMap;
    }

    @Override
    public MetrcAccount addMetrcAccount(MetrcAccount metrcAccount) {
        if (metrcAccount == null) {
            throw new BlazeInvalidArgException("Metrc", "Metrc Account does not found");
        }

        if (StringUtils.isBlank(metrcAccount.getStateCode())) {
            throw new BlazeInvalidArgException("Metrc", "Metrc Account state code does not found");
        }

        if (metrcAccount.isEnabled() && StringUtils.isNotBlank(metrcAccount.getUserApiKey())) {
            validMetrcUserApiKey(metrcAccount);
        } else if (metrcAccount.isEnabled() && StringUtils.isBlank(metrcAccount.getUserApiKey())) {
            throw new BlazeInvalidArgException("Metrc", "Metrc user api key is not found.");
        }

        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());
        if (shop == null) {
            throw new BlazeInvalidArgException("Shop", "Shop does not found");
        }

        MetrcAccount dbMetrcAccount = null;

        if (!Objects.isNull(metrcAccount)) {
            MetrcAccount dbAccount = metrcAccountRepository.getMetrcAccount(token.getCompanyId(), metrcAccount.getStateCode());
            List<MetrcFacilityAccount> facilities = metrcAccount.getFacilities();
            for (MetrcFacilityAccount facility : facilities) {
                if (!facility.getStateCode().equalsIgnoreCase(metrcAccount.getStateCode())) {
                  throw new BlazeInvalidArgException("MetrcFacility", "Metrc facility state code is not match");
                }
            }

            if (dbAccount != null) {
                dbAccount.setEnvironment(metrcAccount.getEnvironment());
                dbAccount.setEnabled(metrcAccount.isEnabled());
                dbAccount.setUserApiKey(metrcAccount.getUserApiKey());
                dbAccount.setFacilities(metrcAccount.getFacilities());
                dbAccount.setStateCode(metrcAccount.getStateCode());
                dbMetrcAccount = metrcAccountRepository.update(dbAccount.getId(), dbAccount);
            } else {
                MetrcAccount metrc = new MetrcAccount();
                metrc.prepare(token.getCompanyId());
                metrc.setUserApiKey(metrcAccount.getUserApiKey());
                metrc.setStateCode(metrc.getStateCode());
                metrc.setFacilities(metrcAccount.getFacilities());
                metrc.setEnabled(metrcAccount.isEnabled());
                metrc.setEnvironment(metrcAccount.getEnvironment());
                dbMetrcAccount = metrcAccountRepository.save(metrc);
            }
        }
        return dbMetrcAccount;
    }

    /**
     *
     * @param metrcAccount  : metrcAccount
     * @implNote this method is responsible for metrc user api key is valid or not
     */
    private void validMetrcUserApiKey(MetrcAccount metrcAccount) {
        Map<String, MetrcAuthorization> metrcAuthorizationMap = new HashMap<>();
        Map<String, MetricsFacilityList> metricsFacilityListHashMap = new HashMap<>();

        List<MetrcFacilityAccount> facilities = metrcAccount.getFacilities();

        for (MetrcFacilityAccount facilityAccount : facilities) {
            IntegrationSettingConstants.Integrations.Metrc.MetrcState state = IntegrationSettingConstants.Integrations.Metrc.MetrcState.fromPrefix(metrcAccount.getStateCode());
            if (state == null) {
                throw new BlazeInvalidArgException("Metrc", "Unable to create Metrc Authorization. The shop is not configured for Metrc.");
            }

            if (facilityAccount.getShopId().equalsIgnoreCase(token.getShopId())) {
                metrcAccount.setStateCode(facilityAccount.getStateCode());
                metrcAccount.setUserApiKey(facilityAccount.getUserApiKey());
                metrcAccount.setEnvironment(facilityAccount.getEnvironment());
            }

            MetrcConfig metrcConfig = integrationSettingRepository.getMetricConfig(
                    state,
                    metrcAccount.getEnvironment());
            LOG.info("Metrc State: " + state.stateCode);
            LOG.info("Metrc Env: " + metrcAccount.getEnvironment());
            MetrcAuthorizationImpl authorization = new MetrcAuthorizationImpl(metrcConfig, metrcAccount, facilityAccount);

            metrcAuthorizationMap.put(metrcAccount.getStateCode(), authorization);
        }

        for (String stateCode : metrcAuthorizationMap.keySet()) {
            MetrcAuthorization metrcAuthorization = metrcAuthorizationMap.get(stateCode);
            MetricsFacilitiesApiService facilitiesApiService = new MetricsFacilitiesApiService(metrcAuthorization);

            try {
                MetricsFacilityList metricsFacilities = facilitiesApiService.getFacilities();
                metricsFacilityListHashMap.put(stateCode, metricsFacilities);
            } catch (Exception e) {
                throw new BlazeInvalidArgException("Metrc", "Metrc account user Api key is not valid for " + stateCode);
            }
        }

    }

    @Override
    public ComplianceBatch getMetrcBatchByLabel(String label) {
        return getMetrcBatchByLabel(label,null);
    }

    @Override
    public ComplianceBatch getMetrcBatchByLabel(String label, String productId) {
        if (StringUtils.isBlank(label)) {
            throw new BlazeInvalidArgException(METRC, METRC_LABEL_NOT_BLANK);
        }

        String stateCode = metrcService.getShopStateCode(token.getCompanyId(), token.getShopId());
        // Ignore if not enabled
        MetrcAccount metrcAccount = metrcService.getMetrcAccount(token.getCompanyId(), stateCode);

        if (metrcAccount == null || !metrcAccount.isEnabled()) {
            throw new BlazeInvalidArgException(METRC, "The shop is not configured for Metrc.");
        }

        MetrcFacilityAccount faciltyAccount = metrcAccount.getMetrcFacilityAccount(token.getShopId());

        if (StringUtils.isBlank(faciltyAccount.getFacLicense())) {
            throw new BlazeInvalidArgException(METRC, "The facility license is not configured for this shop.");
        }

        MetrcAuthorization metrcAuthorization = metrcService.getMetrcAuthorization(
                token.getCompanyId(),
                token.getShopId());

        MetricsPackagesAPIService metricsPackagesAPIService = new MetricsPackagesAPIService(metrcAuthorization);

        MetricsPackages metricsPackage = null;
        try {
            metricsPackage = metricsPackagesAPIService.getPackageLabel(label);
        } catch (Exception e) {
            LOG.info(String.format("Error in metrc package api calling. Metrc Label : %s", label));
        }

        if (metricsPackage == null) {
            throw new BlazeInvalidArgException(METRC, String.format("Metrc Package not found for label : %s", label));
        }
        ComplianceBatch batch = new ComplianceBatch();
        batch.setLabel(metricsPackage.getLabel());
        batch.setQuantity(metricsPackage.getQuantity());
        batch.setMeasurement(metricsPackage.getUnitOfMeasureName());
        batch.setProductName(metricsPackage.getProductName());
        batch.setCategoryName(metricsPackage.getProductCategoryName());
        batch.setPackagedDate(metricsPackage.getPackagedDate());
        batch.setTestingState(metricsPackage.getLabTestingState());
        batch.setTestingStateDate(metricsPackage.getLabTestingStateDate());
        batch.setLastModifiedDate((StringUtils.isBlank(metricsPackage.getLastModified())) ? "N/A" : DateUtil.convertToPatternUTC(metricsPackage.getLastModified(), "yyyy-MM-dd'T'HH:mm:ssZ", "MM/dd/yyyy hh:mm:ss a"));

        if (metricsPackage.getUnitOfMeasureName().equalsIgnoreCase(MetrcMeasurement.Ounces.measurementName)) {
            ProductWeightTolerance ozTolerance = toleranceRepository.getToleranceForWeight(token.getCompanyId(),
                    ProductWeightTolerance.WeightKey.OUNCE);
            BigDecimal ozUnitValue = ProductWeightTolerance.WeightKey.OUNCE.weightValue;
            if (ozTolerance != null && ozTolerance.getUnitValue().doubleValue() > 0) {
                ozUnitValue = ozTolerance.getUnitValue();
            }

            BigDecimal blazeQuantity = ozUnitValue.multiply(metricsPackage.getQuantity());
            if (blazeQuantity.doubleValue() < 0) {
                blazeQuantity = new BigDecimal(0);
            }
            batch.setBlazeQuantity(blazeQuantity);
            batch.setBlazeMeasurement(ProductWeightTolerance.WeightKey.GRAM);
        } else {
            batch.setBlazeQuantity(metricsPackage.getQuantity());
            batch.setBlazeMeasurement(metricsPackage.getUnitOfMeasureName().equalsIgnoreCase(MetrcMeasurement.Grams.measurementName) ? ProductWeightTolerance.WeightKey.GRAM : ProductWeightTolerance.WeightKey.UNIT);
        }

        if (StringUtils.isNotBlank(productId)) {
            Product product = productRepository.get(token.getCompanyId(),productId);
            if (product != null) {
                ProductCategory category = productCategoryRepository.get(token.getCompanyId(),product.getCategoryId());
                BigDecimal newQty = MeasurementConversionUtil.convertToBatchQuantity(toleranceRepository,
                        token.getCompanyId(),
                        batch.getBlazeMeasurement(),
                        batch.getQuantity(),
                        batch.getBlazeQuantity(),
                        category.getUnitType(),
                        product);
                batch.setBlazeQuantity(newQty);
                batch.setBlazeMeasurement(product.getWeightPerUnit().getCorrespondingWeightKey());
            }
        }


        return batch;
    }


    public BigDecimal convertToBatchQuantity(MetrcVerifiedPackage verifiedPackage, ProductCategory.UnitType unitType, Product productProfile) {

        return MeasurementConversionUtil.convertToBatchQuantity(toleranceRepository,
                token.getCompanyId(),
                verifiedPackage,
                unitType,
                productProfile);
    }
}
