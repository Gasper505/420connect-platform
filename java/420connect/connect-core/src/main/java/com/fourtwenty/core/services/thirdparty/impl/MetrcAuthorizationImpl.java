package com.fourtwenty.core.services.thirdparty.impl;

import com.blaze.clients.metrcs.MetrcAuthorization;
import com.fourtwenty.core.domain.models.common.MetrcConfig;
import com.fourtwenty.core.domain.models.thirdparty.MetrcAccount;
import com.fourtwenty.core.domain.models.thirdparty.MetrcFacilityAccount;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import org.apache.commons.codec.binary.Base64;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;

/**
 * Created by mdo on 8/24/17.
 */
public class MetrcAuthorizationImpl implements MetrcAuthorization {
    private MetrcConfig metrcConfig;
    private MetrcAccount metrcAccount;
    private MetrcFacilityAccount facilityAccount;

    public MetrcAuthorizationImpl() {
    }

    public MetrcAuthorizationImpl(MetrcConfig metrcConfig, MetrcAccount metrcAccount, MetrcFacilityAccount facilityAccount) {
        this.metrcConfig = metrcConfig;
        this.metrcAccount = metrcAccount;
        this.facilityAccount = facilityAccount;
    }

    public String getCompanyId() {
        return facilityAccount.getCompanyId();
    }

    public String getShopId() {
        return facilityAccount.getShopId();
    }

    @Override
    public String getHost() {
        return metrcConfig.getHost();
    }

    @Override
    public String getUserAPIKey() {
        return metrcAccount.getUserApiKey();
    }

    @Override
    public String getFacilityLicense() {
        return facilityAccount.getFacLicense();
    }

    @Override
    public String getVendorAPIKey() {
        return metrcConfig.getVendorKey();
    }

    public MultivaluedMap<String, Object> makeHeaders() {
        if (this.getUserAPIKey() == null) {
            throw new BlazeInvalidArgException("Metrc", "User API Key is not specified.");
        }
        MultivaluedMap<String, Object> headers = new MultivaluedHashMap<>();
        headers.putSingle("Content-Type", MediaType.APPLICATION_JSON);
        String binaryData = this.getVendorAPIKey() + ":" + this.getUserAPIKey();
        headers.putSingle("Authorization", "Basic " + Base64.encodeBase64String(binaryData.getBytes()));
        return headers;
    }

}
