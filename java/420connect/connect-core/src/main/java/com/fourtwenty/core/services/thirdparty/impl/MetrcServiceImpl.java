package com.fourtwenty.core.services.thirdparty.impl;

import com.blaze.clients.metrcs.MetrcAuthorization;
import com.fourtwenty.core.domain.models.common.IntegrationSetting;
import com.fourtwenty.core.domain.models.common.IntegrationSettingConstants;
import com.fourtwenty.core.domain.models.common.MetrcConfig;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.thirdparty.MetrcAccount;
import com.fourtwenty.core.domain.models.thirdparty.MetrcFacilityAccount;
import com.fourtwenty.core.domain.repositories.common.IntegrationSettingRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.domain.repositories.thirdparty.MetrcAccountRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.exceptions.BlazeOperationException;
import com.fourtwenty.core.services.thirdparty.MetrcService;
import com.google.inject.Inject;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.assertj.core.util.Lists;

import java.util.*;

public class MetrcServiceImpl implements MetrcService {
    private static final Log LOG = LogFactory.getLog(MetrcServiceImpl.class);
    @Inject
    MetrcAccountRepository metrcAccountRepository;
    @Inject
    IntegrationSettingRepository integrationSettingRepository;
    @Inject
    ShopRepository shopRepository;

    @Override
    public MetrcAccount getMetrcAccount(String companyId, String stateCode) {
        return metrcAccountRepository.getMetrcAccount(companyId, stateCode);
    }

    @Override
    public MetrcAccount getMetrcAccountByShopId(String companyId, String shopId) {
        String stateCode = getShopStateCode(companyId, shopId);
        MetrcAccount metrcAccount = getMetrcAccount(companyId, stateCode);

        MetrcFacilityAccount facilityAccount = metrcAccount.getMetrcFacilityAccount(shopId);
        if (facilityAccount != null) {
            if (StringUtils.isNotBlank(facilityAccount.getUserApiKey())) {
                metrcAccount.setEnabled(facilityAccount.isEnabled());
                metrcAccount.setEnvironment(facilityAccount.getEnvironment());
                metrcAccount.setUserApiKey(facilityAccount.getUserApiKey());
            }
        }


        return metrcAccount;
    }


    @Override
    public MetrcAuthorization getMetrcAuthorization(String companyId, String shopId) {
        String stateCode = getShopStateCode(companyId, shopId);
        MetrcAccount metrcAccount = getMetrcAccount(companyId, stateCode);
        if (metrcAccount == null) {
            throw new BlazeOperationException("Unable to create Metrc Authorization. The shop is not configured for Metrc.");
        }
        MetrcFacilityAccount facilityAccount = metrcAccount.getMetrcFacilityAccount(shopId);
        if (facilityAccount != null) {
            IntegrationSettingConstants.Integrations.Metrc.MetrcState state = getStateForShopId(shopId);
            if (state == null) {
                throw new BlazeOperationException("Unable to create Metrc Authorization. The shop is not configured for Metrc.");
            }

            if (StringUtils.isNotBlank(facilityAccount.getUserApiKey())) {
                metrcAccount.setEnabled(facilityAccount.isEnabled());
                metrcAccount.setEnvironment(facilityAccount.getEnvironment());
                metrcAccount.setUserApiKey(facilityAccount.getUserApiKey());
            }

            MetrcConfig metrcConfig = integrationSettingRepository.getMetricConfig(
                    state,
                    metrcAccount.getEnvironment());
            LOG.info("Metrc State: " + state.stateCode);
            LOG.info("Metrc Env: " + metrcAccount.getEnvironment());
            LOG.info("Metrc Env: " + metrcAccount.getEnvironment());
            MetrcAuthorizationImpl authorization = new MetrcAuthorizationImpl(metrcConfig, metrcAccount, facilityAccount);
            return authorization;
        }
        throw new BlazeOperationException("Unable to create Metrc Authorization. The shop is not configured for Metrc.");
    }

    @Override
    public MetrcAuthorization createMetrcAuthorization(String companyId, String shopId, IntegrationSetting.Environment environment, String stateCode, String apiKey) {
        MetrcAccount metrcAccount = getMetrcAccount(companyId, stateCode);
        if (metrcAccount == null) {
            metrcAccount = new MetrcAccount();
        }

        metrcAccount.setUserApiKey(apiKey);
        metrcAccount.setStateCode(stateCode);
        metrcAccount.setEnabled(true);
        metrcAccount.setEnvironment(environment);

        MetrcFacilityAccount facilityAccount = new MetrcFacilityAccount();
        facilityAccount.setStateCode(stateCode);
        facilityAccount.setUserApiKey(apiKey);

        IntegrationSettingConstants.Integrations.Metrc.MetrcState state = getStateForShopId(shopId);
        if (state == null) {
            throw new BlazeOperationException("Unable to create Metrc Authorization. The shop is not configured for Metrc.");
        }
        MetrcConfig metrcConfig = integrationSettingRepository.getMetricConfig(
                state,
                metrcAccount.getEnvironment());

        MetrcAuthorizationImpl authorization = new MetrcAuthorizationImpl(metrcConfig, metrcAccount, facilityAccount);
        return authorization;

    }

    private IntegrationSettingConstants.Integrations.Metrc.MetrcState getStateForShopId(String shopId) {
        try {
            Shop shop = shopRepository.getById(shopId);
            if (shop != null && shop.getAddress() != null && shop.getAddress().getState() != null) {
                return IntegrationSettingConstants.Integrations.Metrc.MetrcState.fromPrefix(shop.getAddress().getState());
            }
            throw new BlazeOperationException("Could not determine Metrc State, no address defined.");
        } catch (Exception e) {
            throw new BlazeOperationException("Could not determine Metrc State", e);
        }
    }

    @Override
    public Map<String, MetrcAuthorization> getMetrcAuthorizationDetails(String companyId, String shopId) {
        Map<String, MetrcAuthorization> metrcAuthorizationMap = new HashMap<>();
        Iterable<MetrcAccount> list = metrcAccountRepository.list(companyId);
        ArrayList<MetrcAccount> metrcAccounts = Lists.newArrayList(list);
        for (MetrcAccount metrcAccount : metrcAccounts) {
            if (!metrcAccount.isEnabled()) {
                continue;
            }
            List<MetrcFacilityAccount> facilities = metrcAccount.getFacilities();
            for (MetrcFacilityAccount facilityAccount : facilities) {
                if (facilityAccount != null) {
                    if (StringUtils.isBlank(metrcAccount.getStateCode())) {
                        throw new BlazeOperationException("Unable to create Metrc Authorization. The shop is not configured for Metrc.");
                    }
                    IntegrationSettingConstants.Integrations.Metrc.MetrcState state = IntegrationSettingConstants.Integrations.Metrc.MetrcState.fromPrefix(metrcAccount.getStateCode());
                    if (state == null) {
                        throw new BlazeInvalidArgException("Metrc", "Unable to create Metrc Authorization. The shop is not configured for Metrc.");
                    }
                    if (facilityAccount.getShopId().equalsIgnoreCase(shopId)) {
                        metrcAccount.setEnvironment(facilityAccount.getEnvironment());
                        metrcAccount.setUserApiKey(facilityAccount.getUserApiKey());
                        metrcAccount.setStateCode(facilityAccount.getStateCode());
                    }
                    MetrcConfig metrcConfig = integrationSettingRepository.getMetricConfig(
                            state,
                            metrcAccount.getEnvironment());
                    LOG.info("Metrc State: " + state.stateCode);
                    LOG.info("Metrc Env: " + metrcAccount.getEnvironment());
                    LOG.info("Metrc Env: " + metrcAccount.getEnvironment());
                    MetrcAuthorizationImpl authorization = new MetrcAuthorizationImpl(metrcConfig, metrcAccount, facilityAccount);

                    metrcAuthorizationMap.put(metrcAccount.getStateCode(), authorization);
                }
            }
        }

        return metrcAuthorizationMap;
    }


    @Override
    public String getShopStateCode(String companyId, String shopId, boolean isRequired) {
        String state = null;
        Shop shop = shopRepository.get(companyId, shopId);
        if (isRequired && Objects.isNull(shop)) {
            throw new BlazeInvalidArgException("Shop", "Shop doesn't found for metrc.");
        }
        if (isRequired && Objects.isNull(shop.getAddress())) {
            throw new BlazeInvalidArgException("Shop", "Shop address doesn't found for metrc.");
        } else {
            if (shop.getAddress() != null && StringUtils.isNotBlank(shop.getAddress().getState())) {
                state = shop.getAddress().getState();
            } else if (isRequired) {
                throw new BlazeInvalidArgException("Shop", "Shop state does not found for metrc.");
            }
        }
        return state;
    }

    @Override
    public String getShopStateCode(String companyId, String shopId) {
        return getShopStateCode(companyId, shopId, true);
    }


}
