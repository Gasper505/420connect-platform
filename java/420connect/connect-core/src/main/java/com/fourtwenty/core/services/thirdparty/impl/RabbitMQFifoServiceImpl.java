package com.fourtwenty.core.services.thirdparty.impl;

import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.domain.repositories.dispensary.AppRepository;
import com.fourtwenty.core.exceptions.BlazeOperationException;
import com.fourtwenty.core.services.thirdparty.RabbitMQFifoService;
import com.fourtwenty.core.services.thirdparty.models.TransSQSMessageRequest;
import com.fourtwenty.core.util.JsonSerializer;
import com.google.inject.Singleton;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.impl.DefaultCredentialsProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

@Singleton
public class RabbitMQFifoServiceImpl extends AbstractFifoService implements RabbitMQFifoService {

    private static final Logger LOGGER = LoggerFactory.getLogger(RabbitMQFifoServiceImpl.class);

    private ConnectionFactory factory;

    private Connection activeConnection;

    private Channel activeChannel;

    private boolean isActive;

    private Map<String, Consumer> consumerMap = new HashMap<>();

    @Inject
    public RabbitMQFifoServiceImpl(ConnectConfiguration connectConfiguration, AppRepository appRepository) {
        super(connectConfiguration, appRepository);
        factory = new ConnectionFactory();
        setupFactory();
    }

    @Override
    public void publishItemToQueue() {
        if (!isActive) {
            LOGGER.warn("RabbitMQ connections are unstable, skipping the queue");
            return;
        }
        final String queueName = createTransQueue("56ce8bf389288da6df3a6602");
        String messageBody = "Hello from R-MQ";
        publish(queueName, messageBody);
    }

    @Override
    public void publishQueuedTransaction(String companyId, String shopId, String queueTransId) {
        if (!isActive) {
            LOGGER.warn("RabbitMQ connections are unstable, skipping the queue");
            return;
        }
        final String queueName = createTransQueue(companyId);
        TransSQSMessageRequest transRequest = new TransSQSMessageRequest(companyId,
                shopId,
                queueTransId);
        String messageBody = JsonSerializer.toJson(transRequest);
        publish(queueName, messageBody);
    }

    @Override
    public void processMessageFromQueue(String queueName, RabbitMQConsumer rabbitMQConsumer) {
        try {
            if (!consumerMap.containsKey(queueName)) {
                Channel channel = getChannel();
                queueDeclare(channel, queueName);

                DefaultConsumer defaultConsumer = new DefaultConsumer(channel) {
                    @Override
                    public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) {
                        rabbitMQConsumer.processWithDelivery(getChannel(), consumerTag, envelope, properties, body);
                    }
                };

                channel.basicConsume(queueName, false, defaultConsumer);
                consumerMap.put(queueName, defaultConsumer);
            }
        } catch (Exception e) {
            LOGGER.error("Unable to read from the queue", e);
            throw new BlazeOperationException(e);
        }
    }

    @Override
    protected String createQueue(String queueName) {
        Channel channel = getChannel();
        try {
            queueDeclare(channel, queueName);
        } catch (Exception e) {
            LOGGER.error("Unable to declare the queue", e);
            throw new BlazeOperationException(e);
        }

        return queueName;
    }

    private Channel queueDeclare(Channel channel, String queueName) throws IOException {
        Map<String, Object> args = new HashMap<String, Object>();
        // If we want to create lazy queues in case of huge messages per seconds, so that messages can be stored in disk instead of RAM/SWAP memory
//            args.put("x-queue-mode", "lazy");
        channel.queueDeclare(queueName, true, false, false, args);
        return channel;
    }

    private void setupFactory() {
        if (factory == null) {
            LOGGER.warn("Factory is not initialized and can not initiate the connections");
            return;
        }

        if (connectConfiguration.getRabbitMQConfiguration() == null) {
            LOGGER.warn("RabbitMQ configs are not available hence can not initiate the connections");
            return;
        }

        factory.setHost(connectConfiguration.getRabbitMQConfiguration().getHost());
        factory.setPort(connectConfiguration.getRabbitMQConfiguration().getPort());

        factory.setCredentialsProvider(new DefaultCredentialsProvider(connectConfiguration.getRabbitMQConfiguration().getUser(),
                connectConfiguration.getRabbitMQConfiguration().getPassword()));

        Connection connection = getConnection();
        Channel channel = getChannel();
        try {
            if (connection.isOpen() && channel.isOpen()) {
                isActive = true;
            }
        } catch (Exception e) {
            LOGGER.error("Couldn't test the connection with RabbitMQ Server", e);
        }

    }

    private void publish(String queueName, String message) {
        Connection connection = getConnection();
        Channel channel = getChannel();
        try {
            queueDeclare(channel, queueName);
            channel.basicPublish("", queueName, null, message.getBytes(StandardCharsets.UTF_8));
            LOGGER.info("[RabbitMQ] queue-txn published with connectionID {}, sequence number {}", connection.getId(), (channel.getNextPublishSeqNo() - 1));
        } catch (Exception e) {
            LOGGER.error("Unable to publish to the queue", e);
            throw new BlazeOperationException(e);
        }
    }

    private synchronized Connection getConnection() {
        if (activeConnection == null) {
            try {
                activeConnection = factory.newConnection();
            } catch (Exception e) {
                LOGGER.error("Couldn't obtain the RabbitMQ Connection");
                throw new BlazeOperationException(e);
            }
        } else if (!activeConnection.isOpen()) {
            try {
                activeConnection = factory.newConnection();
            } catch (Exception e) {
                LOGGER.error("Couldn't obtain the RabbitMQ Connection");
                throw new BlazeOperationException(e);
            }
        }

        return activeConnection;

    }

    private Channel getChannel() {
        if (activeChannel == null) {
            try {
                activeChannel = getConnection().createChannel();
            } catch (IOException e) {
                LOGGER.error("Couldn't obtain the RabbitMQ Channel");
            }
        } else if (!activeChannel.isOpen()) {
            try {
                activeChannel = getConnection().createChannel();
            } catch (IOException e) {
                LOGGER.error("Couldn't obtain the RabbitMQ Channel");
            }
        }

        return activeChannel;
    }

    public interface RabbitMQConsumer {
        void processWithDelivery(Channel channel, String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body);
    }

}
