package com.fourtwenty.core.services.thirdparty.impl;

import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.exceptions.BlazeOperationException;
import com.fourtwenty.core.services.thirdparty.SendgridSerive;
import com.sendgrid.*;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class SendgridServiceImpl implements SendgridSerive {

    @Inject
    private ConnectConfiguration config;

    @Override
    public Response sendEmail(String supportEmail, String toEmail, String aSubject, String aBody, String bccEmail, String ccEmail, String replyToEmail, String fromName) throws IOException {

        if (StringUtils.isEmpty(toEmail)) {
            throw new BlazeOperationException("Validation for Sendgrid failed (any pair of to, cc, bcc must not be equal)");
        }

        final Personalization personalization = new Personalization();

        final Email from = new Email(supportEmail);
        from.setName(fromName);
        final Email to = new Email(toEmail);

        // make sure cc is not equal to to email
        if (!StringUtils.isEmpty(ccEmail) && !toEmail.equalsIgnoreCase(ccEmail)) {

            // make sure that cc is not equal to bcc
            if (StringUtils.isBlank(bccEmail) || !ccEmail.equalsIgnoreCase(bccEmail)) {
                final Email cc = new Email(ccEmail);
                if (StringUtils.isNotBlank(fromName)) {
                    cc.setName(fromName);
                }
                personalization.addCc(cc);
            }
        }

        // make sure bcc is not equal to toEmail
        if (!StringUtils.isEmpty(bccEmail)) {
            List<String> bccEmails = Arrays.asList(bccEmail.split("\\s*,\\s*"));
            for (String email : bccEmails) {
                if (!toEmail.equalsIgnoreCase(bccEmail)) {
                    final Email bcc = new Email(email);
                    personalization.addBcc(bcc);
                }
            }
        }
        if (StringUtils.isNotBlank(fromName)) {
            from.setName(fromName);
        }

        final com.sendgrid.Content content = new com.sendgrid.Content("text/html", aBody);
        final Mail mail = new Mail(from, aSubject, to, content);
        personalization.addTo(to);
        personalization.setSubject(aSubject);
        mail.addPersonalization(personalization);

        if (StringUtils.isNotBlank(replyToEmail)) {
            final Email replyTo = new Email(replyToEmail);
            if (StringUtils.isNotBlank(fromName)) {
                replyTo.setName(fromName);
            }
            mail.setReplyTo(replyTo);
        }

        final SendGrid sendGrid = new SendGrid(config.getSendgridConfiguration().getApiKey());

        Request request = new Request();
        request.setMethod(Method.POST);
        request.setEndpoint("mail/send");
        request.setBody(mail.build());

        return sendGrid.api(request);
    }

    @Override
    public Response sendMultiPartEmail(final String supportEmail, final String toEmail, final String aSubject,
                                       final String aBody, final String bccEmail, final String ccEmail,
                                       final HashMap<String, HashMap<String, InputStream>> streamMap, final String contentType, final String fileName, final String fromName) throws IOException {


        if (StringUtils.isEmpty(toEmail)
                || (!StringUtils.isEmpty(ccEmail) && toEmail.equals(ccEmail))
                || (!StringUtils.isEmpty(bccEmail) && toEmail.equals(bccEmail))
                || (!StringUtils.isEmpty(bccEmail) && bccEmail.equals(ccEmail))
                || (!StringUtils.isEmpty(ccEmail) && ccEmail.equals(bccEmail))
                ) {
            throw new BlazeOperationException("Validation for Sendgrid failed (any pair of to, cc, bcc must not be equal)");
        }

        final Personalization personalization = new Personalization();

        final Email from = new Email(supportEmail);
        from.setName(fromName);
        final Email to = new Email(toEmail);
        if (!StringUtils.isEmpty(ccEmail)) {
            final Email cc = new Email(ccEmail);
            if (StringUtils.isNotBlank(fromName)) {
                cc.setName(fromName);
            }
            personalization.addCc(cc);
        }

        if (!StringUtils.isEmpty(bccEmail)) {
            List<String> bccEmails = Arrays.asList(bccEmail.split("\\s*,\\s*"));
            for (String email : bccEmails) {
                final Email bcc = new Email(email);
                personalization.addBcc(bcc);
            }
        }
        if (StringUtils.isNotBlank(fromName)) {
            from.setName(fromName);
        }

        final com.sendgrid.Content content = new com.sendgrid.Content("text/html", aBody);
        final Mail mail = new Mail(from, aSubject, to, content);
        personalization.addTo(to);
        personalization.setSubject(aSubject);
        if (streamMap != null && !streamMap.isEmpty()) {
            HashMap<String, InputStream> attachmentMap = streamMap.get("attachment");
            HashMap<String, InputStream> inlineAttachmentMap = streamMap.get("inlineAttachment");
            if (attachmentMap != null) {
                for (String key : attachmentMap.keySet()) {
                    InputStream stream = attachmentMap.get(key);
                    final Attachments attachment = getAttachment(stream);
                    attachment.setDisposition("attachment");
                    attachment.setFilename(key);
                    mail.addAttachments(attachment);
                }
            }
            if (inlineAttachmentMap != null) {
                for (String key : inlineAttachmentMap.keySet()) {
                    InputStream stream = inlineAttachmentMap.get(key);
                    final Attachments attachment = getAttachment(stream);
                    attachment.setDisposition("inline");
                    attachment.setFilename(key);
                    attachment.setContentId(key);
                    mail.addAttachments(attachment);
                }
            }
        }
        mail.addPersonalization(personalization);

        if (StringUtils.isNotBlank(ccEmail)) {
            final Email cc = new Email(ccEmail);
            if (StringUtils.isNotBlank(fromName)) {
                cc.setName(fromName);
            }
            mail.setReplyTo(cc);
        }

        final SendGrid sendGrid = new SendGrid(config.getSendgridConfiguration().getApiKey());

        Request request = new Request();
        request.setMethod(Method.POST);
        request.setEndpoint("mail/send");
        request.setBody(mail.build());

        return sendGrid.api(request);

    }

    private Attachments getAttachment(final InputStream stream) throws IOException {
        final Attachments attachments = new Attachments();
        final Base64 x = new Base64();
        String encodedString = x.encodeAsString(IOUtils.toByteArray(stream));
        attachments.setContent(encodedString);
        return attachments;
    }

}
