package com.fourtwenty.core.services.thirdparty.impl;

import com.amazonaws.util.CollectionUtils;
import com.fourtwenty.core.domain.models.common.IntegrationSetting;
import com.fourtwenty.core.domain.models.common.WeedmapConfig;
import com.fourtwenty.core.domain.models.product.BlazeRegion;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.domain.models.thirdparty.*;
import com.fourtwenty.core.domain.models.thirdparty.weedmap.WeedmapSyncJob;
import com.fourtwenty.core.domain.repositories.common.IntegrationSettingRepository;
import com.fourtwenty.core.domain.repositories.dispensary.BlazeRegionRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductCategoryRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.thirdparty.*;
import com.fourtwenty.core.event.BlazeEventBus;
import com.fourtwenty.core.event.weedmap.WeedmapAuthEvent;
import com.fourtwenty.core.event.weedmap.WeedmapFetchEvent;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.managed.BackgroundTaskManager;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.thirdparty.VersionRequest;
import com.fourtwenty.core.rest.thirdparty.WeedmapTokenType;
import com.fourtwenty.core.rest.thirdparty.WeedmapUpdateAPIRequest;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.common.BackgroundJobService;
import com.fourtwenty.core.services.thirdparty.WeedmapService;
import com.fourtwenty.core.thirdparty.weedmap.models.ThirdPartyBrand;
import com.fourtwenty.core.thirdparty.weedmap.models.ThirdPartyProduct;
import com.fourtwenty.core.thirdparty.weedmap.result.WeedmapAccountResult;
import com.fourtwenty.core.thirdparty.weedmap.result.WmProductMappingResult;
import com.fourtwenty.core.thirdparty.weedmap.services.WeedmapAPIService;
import com.google.common.collect.Lists;
import com.google.inject.Provider;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by mdo on 4/12/17.
 */
public class WeedmapServiceImpl extends AbstractAuthServiceImpl implements WeedmapService {

    private static final Logger LOGGER = LoggerFactory.getLogger(WeedmapServiceImpl.class);

    private static final String REGION = "Region";
    private static final String REGION_NOT_FOUND = "Region not found";
    private static final String WEEDMAP = "WeedmapAccount";
    private static final String ACCOUNT_NOT_FOUND = "Weedmap account does not exist for this shop. Please create one.";

    @Inject
    WeedmapAccountRepository weedmapAccountRepository;
    @Inject
    BackgroundTaskManager taskManager;
    @Inject
    private BlazeRegionRepository blazeRegionRepository;
    @Inject
    private BlazeEventBus eventBus;
    @Inject
    private IntegrationSettingRepository integrationSettingRepository;
    @Inject
    private WmMappingRepository wmMappingRepository;
    @Inject
    private ProductRepository productRepository;
    @Inject
    private ProductCategoryRepository categoryRepository;
    @Inject
    private WeedmapSyncItemRepository weedmapSyncItemRepository;
    @Inject
    private WeedmapAPIService weedmapAPIService;
    @Inject
    private WmSyncJobRepository wmSyncJobRepository;
    @Inject
    private BackgroundJobService backgroundJobService;

    @Inject
    public WeedmapServiceImpl(Provider<ConnectAuthToken> tokenProvider) {
        super(tokenProvider);
    }

    @Override
    public WeedmapAccount createWeedmapAccount(VersionRequest request) {
        WeedmapAccount account = weedmapAccountRepository.getWeedmapAccount(token.getCompanyId(), token.getShopId());
        if (account == null) {
            account = new WeedmapAccount();
            account.prepare(token.getCompanyId());
            account.setShopId(token.getShopId());

            if (request == null)
                account.setVersion(WeedmapAccount.Version.V1);
            else
                account.setVersion(request.getVersion());

            weedmapAccountRepository.save(account);
        } else {
            if (request == null)
                account.setVersion(WeedmapAccount.Version.V1);
            else
                account.setVersion(request.getVersion());
            return weedmapAccountRepository.update(account.getId(), account);
        }
        return account;
    }

    @Override
    public WeedmapAccountResult getCurrentWeedmapAccount() {
        return getCurrentWeedmapAccount(true);
    }


    private WeedmapAccountResult getCurrentWeedmapAccount(boolean assignMapping) {
        WeedmapAccountResult result = weedmapAccountRepository.getWeedmapAccount(token.getCompanyId(), token.getShopId(), WeedmapAccountResult.class);
        if (result != null && assignMapping) {
            WeedmapConfig weedmapConfig = integrationSettingRepository.getWeedmapConfig(IntegrationSetting.Environment.Production); // Always use production
            result.setWeedmapConfig(weedmapConfig);
            result.setDefaultTagMapping(WmProductMapping.getDefaultTagMapping());
            prepareWmMappingResult(result);
            if (!CollectionUtils.isNullOrEmpty(result.getApiKeyList())) {
                result.getApiKeyList().forEach(apiKeyMap -> {
                    if (apiKeyMap.getVersion() == WeedmapAccount.Version.V2 && StringUtils.isNotBlank(apiKeyMap.getToken())) {
                        apiKeyMap.setStatus(WeedmapAccount.AuthStatus.Authorized);
                    } else if (apiKeyMap.getVersion() == WeedmapAccount.Version.V1) {
                        apiKeyMap.setStatus(WeedmapAccount.AuthStatus.Authorized);
                    }
                });
            }
        }
        return result;
    }

    @Override
    public WeedmapAccount updateWeedmapAPIKey(WeedmapUpdateAPIRequest request) {
        WeedmapAccount account = weedmapAccountRepository.getWeedmapAccount(token.getCompanyId(), token.getShopId());
        if (account == null) {
            throw new BlazeInvalidArgException("WeedmapAccount", "Weedmap account does not exist for this shop. Please create one.");
        }
        account.setApiKeyList(request.getApiKeyList());
        if (account.getApiKeyList() == null) {
            account.setApiKeyList(new ArrayList<WeedmapApiKeyMap>());
        }

        WeedmapApiKeyMap enableApiKey = account.getApiKeyList().stream().filter(WeedmapApiKeyMap::isEnabled).findFirst().orElse(null);
        WeedmapAccount.Version enableVersion = null;
        if (enableApiKey != null) {
            enableVersion = enableApiKey.getVersion();
        }

        // Disable all other version
        Set<ObjectId> regionIds = new HashSet<>();
        for (WeedmapApiKeyMap apiKeyMap : account.getApiKeyList()) {
            apiKeyMap.prepare();
            if (apiKeyMap.getVersion() != enableVersion) {
                apiKeyMap.setEnabled(false);
            }
            if (apiKeyMap.getRegion() != null && ObjectId.isValid(apiKeyMap.getRegion().getId())) {
                regionIds.add(new ObjectId(apiKeyMap.getRegion().getId()));
            }
        }

        HashMap<String, BlazeRegion> regionMap = blazeRegionRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(regionIds));

        for (WeedmapApiKeyMap map : account.getApiKeyList()) {
            map.prepare();
            if (map.getRegion() != null) {
                BlazeRegion region = regionMap.get(map.getRegion().getId());
                if (region == null) {
                    throw new BlazeInvalidArgException(REGION, REGION_NOT_FOUND);
                }
            }
        }
        weedmapAccountRepository.update(token.getCompanyId(), account.getId(), account);
        return account;
    }

    @Override
    public WeedmapAccount updateWeedmapAccount(WeedmapAccount account) {
        WeedmapAccount dbAccount = weedmapAccountRepository.getWeedmapAccount(token.getCompanyId(), token.getShopId());
        if (dbAccount == null) {
            throw new BlazeInvalidArgException("WeedmapAccount", "Weedmap account does not exist for this shop. Please create one.");
        }

        List<WeedmapCategoryMapItem> weedmapCategoryMapItems = Lists.newArrayList(dbAccount.getCategoryMapping());
        if (dbAccount.getCategoryMapping() != null && account.getCategoryMapping() != null && !account.getCategoryMapping().isEmpty()) {
            for (WeedmapCategoryMapItem categoryMapItem : dbAccount.getCategoryMapping()) {
                boolean isRemoved = account.getCategoryMapping()
                        .stream()
                        .filter(m -> categoryMapItem.getCategoryId().equalsIgnoreCase(m.getCategoryId()) && (categoryMapItem.getWeedmapCategory() == m.getWeedmapCategory()))
                        .findFirst()
                        .map(m -> false)
                        .orElse(true);
                if (!isRemoved) {
                    weedmapCategoryMapItems.remove(categoryMapItem);
                }
            }
        }

        if (!CollectionUtils.isNullOrEmpty(account.getApiKeyList())) {
            WeedmapApiKeyMap enableApiKey = account.getApiKeyList().stream().filter(WeedmapApiKeyMap::isEnabled).findFirst().orElse(null);
            WeedmapAccount.Version enableVersion = null;
            if (enableApiKey != null) {
                enableVersion = enableApiKey.getVersion();
            }
            // Disable all other version
            for (WeedmapApiKeyMap apiKeyMap : account.getApiKeyList()) {
                if (apiKeyMap.getVersion() != enableVersion) {
                    apiKeyMap.setEnabled(false);
                }
            }
        }

        dbAccount.setActive(account.isActive());
        dbAccount.setCategoryMapping(account.getCategoryMapping());
        dbAccount.setVersion(account.getVersion());
        dbAccount.setApiKeyList(account.getApiKeyList());


        weedmapAccountRepository.update(token.getCompanyId(), dbAccount.getId(), dbAccount);
/*
        if (!weedmapCategoryMapItems.isEmpty()) {
            WeedMapDeleteEvent event = new WeedMapDeleteEvent();
            event.setCompanyId(token.getCompanyId());
            event.setShopId(token.getShopId());
            event.setCategoryMappings(weedmapCategoryMapItems);
            event.setApiKeyMaps(dbAccount.getApiKeyList());
            eventBus.post(event);
        }*/
        return dbAccount;
    }

    @Override
    public void deleteWeedmapAccount() {
        WeedmapAccount dbAccount = weedmapAccountRepository.getWeedmapAccount(token.getCompanyId(), token.getShopId());
        if (dbAccount == null) {
            throw new BlazeInvalidArgException("WeedmapAccount", "Weedmap account does not exist for this shop. Please create one.");
        }
        weedmapAccountRepository.removeByIdSetState(token.getCompanyId(), dbAccount.getId());
    }

    @Override
    public WeedmapAccount syncMenu() {
        WeedmapAccount dbAccount = weedmapAccountRepository.getWeedmapAccount(token.getCompanyId(), token.getShopId());
        if (dbAccount == null) {
            throw new BlazeInvalidArgException("WeedmapAccount", "Weedmap account does not exist for this shop. Please create one.");
        }


        if (dbAccount.getMenuSyncRequestDate() != null && dbAccount.getSyncStatus() == WeedmapAccount.WeedmapSyncStatus.InProgress) {

            DateTime lastSync = new DateTime(dbAccount.getMenuSyncRequestDate());
            if (lastSync.isAfter(DateTime.now().minusHours(1))) {
                throw new BlazeInvalidArgException("WeedmapAccount", "Account Sync is already in progress. Please try after some time.");
            }
        }
        dbAccount.setMenuSyncRequestDate(DateTime.now().getMillis());
        dbAccount.setSyncStatus(WeedmapAccount.WeedmapSyncStatus.InProgress);
        //weedmapAccountRepository.update(token.getCompanyId(), dbAccount.getId(), dbAccount);

        weedmapAccountRepository.updateSyncStatus(token.getCompanyId(),dbAccount.getId(), WeedmapAccount.WeedmapSyncStatus.InProgress,DateTime.now().getMillis());
        createWeedmapSyncJob(token.getCompanyId(), token.getShopId(), token.getActiveTopUser().getUserId(), WeedmapSyncJob.WmSyncType.MANUAL, WeedmapSyncJob.WmSyncJobStatus.Queued, Lists.newArrayList(), null);
        return dbAccount;
    }

    @Override
    public void resetMenu(boolean shouldDelete) {
        WeedmapAccount dbAccount = weedmapAccountRepository.getWeedmapAccount(token.getCompanyId(), token.getShopId());
        if (dbAccount == null) {
            throw new BlazeInvalidArgException("WeedmapAccount", "Weedmap account does not exist for this shop. Please create one.");
        }
        dbAccount.setMenuLastSyncDate(0l);
        dbAccount.setMenuSyncRequestDate(0l);
        dbAccount.setSyncStatus(WeedmapAccount.WeedmapSyncStatus.Reset);
        for (WeedmapApiKeyMap apiMap : dbAccount.getApiKeyList()) {
            apiMap.setMenuLastSyncDate(0l);
            apiMap.setMenuSyncRequestDate(0l);
            apiMap.setSyncStatus(WeedmapAccount.WeedmapSyncStatus.NotStarted);
        }
        weedmapAccountRepository.update(token.getCompanyId(), dbAccount.getId(), dbAccount);

        if (shouldDelete) {
            createWeedmapSyncJob(token.getCompanyId(), token.getShopId(), token.getActiveTopUser().getUserId(), WeedmapSyncJob.WmSyncType.RESET, WeedmapSyncJob.WmSyncJobStatus.Queued, Lists.newArrayList(), dbAccount.getId());
        }
    }


    @Override
    public void syncCategories() {

    }

    @Override
    public void removeProductFromWeedMap(List<String> productCategoryIds, boolean isDelete, List<Product> products) {
        WeedmapAccount weedmapAccount = getCurrentWeedmapAccount(false);
        List<WmProductMapping> tagsMappings = new ArrayList<>();
        if (weedmapAccount != null) {
            tagsMappings = Lists.newArrayList(wmMappingRepository.getMappingForAccount(token.getCompanyId(), token.getShopId(), weedmapAccount.getId()));
        }
        if (weedmapAccount == null || !weedmapAccount.isActive() ||
                weedmapAccount.getApiKeyList() == null ||
                weedmapAccount.getApiKeyList().isEmpty() ||
                (weedmapAccount.getVersion() == WeedmapAccount.Version.V1 &&
                        (weedmapAccount.getCategoryMapping() == null ||
                weedmapAccount.getCategoryMapping().isEmpty())) ||
                (weedmapAccount.getVersion() == WeedmapAccount.Version.V2 &&
                        (tagsMappings.isEmpty()))
        ) {
            LOGGER.info("Weedmap account not found");
            return;
        }
        List<WeedmapCategoryMapItem> weedmapCategoryMapItems = new ArrayList<>();

        for (WeedmapCategoryMapItem categoryMapItem : weedmapAccount.getCategoryMapping()) {
            if (productCategoryIds.contains(categoryMapItem.getCategoryId())) {
                weedmapCategoryMapItems.add(categoryMapItem);
            }
        }

        List<WmProductMapping> weedMapTagMapping = new ArrayList<>();

        for (WmProductMapping tagsMapping : tagsMappings) {
            if (productCategoryIds.contains(tagsMapping.getCategoryId())) {
                weedMapTagMapping.add(tagsMapping);
            }
        }

        if ((!weedmapCategoryMapItems.isEmpty() || !weedMapTagMapping.isEmpty()) && !isDelete) {
            List<String> productIds = new ArrayList<>();
            for (Product product : products) {
                productIds.add(product.getId());
            }
            if (productIds.size() > 0) {
                createWeedmapSyncJob(token.getCompanyId(), token.getShopId(), token.getActiveTopUser().getUserId(), WeedmapSyncJob.WmSyncType.INDIVIDUAL, WeedmapSyncJob.WmSyncJobStatus.Queued, productIds, null);
            }
        }
    }

    @Override
    public WeedmapAccount fetchOrRefreshToken(WeedmapTokenType type) {
        WeedmapAccount dbAccount = weedmapAccountRepository.getWeedmapAccount(token.getCompanyId(), token.getShopId());
        if (dbAccount == null) {
            throw new BlazeInvalidArgException("WeedmapAccount", "Weedmap account does not exist for this shop. Please create one.");
        }
        if (type == null) {
            type = new WeedmapTokenType();
            type.setType(WeedmapTokenType.TokenType.REFRESHTOKEN);
        }
        return prepareWeedmapTokenApi(dbAccount, type);

    }

    private WeedmapAccount prepareWeedmapTokenApi(WeedmapAccount dbAccount, WeedmapTokenType type) {
        WeedmapAuthEvent event = new WeedmapAuthEvent();
        event.setCompanyId(token.getCompanyId());
        event.setShopId(token.getShopId());
        event.setAccount(dbAccount);
        event.setType(type);
        event.setResponseTimeout(30000); //set to 5 min api might take time
        eventBus.post(event);
        WeedmapAccount account = event.getResponse();

        WeedmapFetchEvent fetchEvent = new WeedmapFetchEvent();
        fetchEvent.setCompanyId(token.getCompanyId());
        fetchEvent.setShopId(token.getShopId());
        fetchEvent.setAccount(account);
        fetchEvent.setType(type);
        eventBus.post(fetchEvent);

        return account;
    }

    @Override
    public WeedmapAccount generateCode(String code) {
        WeedmapAccount dbAccount = weedmapAccountRepository.getWeedmapAccount(token.getCompanyId(), token.getShopId());
        if (dbAccount == null) {
            throw new BlazeInvalidArgException("WeedmapAccount", "Weedmap account does not exist for this shop. Please create one.");
        }
        if (StringUtils.isBlank(code)) {
            throw new BlazeInvalidArgException("WeedmapAccount", "Weedmap code is empty.");
        }

        WeedmapTokenType weedmapTokenType = new WeedmapTokenType();
        weedmapTokenType.setCode(code);
        weedmapTokenType.setType(WeedmapTokenType.TokenType.FETCHTOKEN);
        return prepareWeedmapTokenApi(dbAccount, weedmapTokenType);
    }

    @Override
    public List<WmTagGroups> getWeedmapTags() {
        List<WmTagGroups> list = new ArrayList<>();

        WeedmapAccount account = getCurrentWeedmapAccount(false);
        WeedmapConfig prodEnvironment = integrationSettingRepository.getWeedmapConfig(IntegrationSetting.Environment.Production);
        if (account == null) {
            return list;
        }
        HashMap<String, Object> returnMap = verifyWeedmapAccount(account, prodEnvironment);

        if (returnMap.isEmpty() || !returnMap.containsKey("organizationId") || !returnMap.containsKey("apiKeyMap")) {
            return list;
        }
        list = weedmapAPIService.getWmTagGroups(account.getCompanyId(), account.getShopId(), String.valueOf(returnMap.get("organizationId")), prodEnvironment, (WeedmapApiKeyMap)returnMap.get("apiKeyMap"));
        return list;
    }

    @Override
    public SearchResult<ThirdPartyBrand> getThirdPartyBrand(String brandId, int start, int limit, String term) {
        SearchResult<ThirdPartyBrand> result = new SearchResult<>();

        if (StringUtils.isNotBlank(term)) {
            WeedmapAccount account = getCurrentWeedmapAccount(false);
            WeedmapConfig prodEnvironment = integrationSettingRepository.getWeedmapConfig(IntegrationSetting.Environment.Production);
            if (account == null) {
                return result;
            }
            HashMap<String, Object> returnMap = verifyWeedmapAccount(account, prodEnvironment);

            if (returnMap.isEmpty() || !returnMap.containsKey("organizationId") || !returnMap.containsKey("apiKeyMap")) {
                return result;
            }
            List<ThirdPartyBrand> verifiedBrands = weedmapAPIService.getWmVerifiedBrands(account.getCompanyId(), account.getShopId(), String.valueOf(returnMap.get("organizationId")), prodEnvironment, (WeedmapApiKeyMap)returnMap.get("apiKeyMap"), term);
            result.setValues(verifiedBrands);
        }
        return result;
    }

    @Override
    public SearchResult<ThirdPartyProduct> getThirdPartyProduct(String thirdPartyBrandId, int start, int limit, String term) {
        SearchResult<ThirdPartyProduct> result = new SearchResult<>();
        if (StringUtils.isBlank(thirdPartyBrandId)) {
            return result;
        }
        WeedmapAccount account = getCurrentWeedmapAccount(false);
        WeedmapConfig prodEnvironment = integrationSettingRepository.getWeedmapConfig(IntegrationSetting.Environment.Production);
        if (account == null) {
            return result;
        }
        HashMap<String, Object> returnMap = verifyWeedmapAccount(account, prodEnvironment);

        if (returnMap.isEmpty() || !returnMap.containsKey("organizationId") || !returnMap.containsKey("apiKeyMap")) {
            return result;
        }
        List<ThirdPartyProduct> verifiedProducts = weedmapAPIService.getWmVerifiedProducts(account.getCompanyId(), account.getShopId(), String.valueOf(returnMap.get("organizationId")), prodEnvironment, (WeedmapApiKeyMap)returnMap.get("apiKeyMap"), thirdPartyBrandId, term);
        result.setValues(verifiedProducts);
        return result;
    }

    @Override
    public List<WmProductMapping> updateWmMapping(List<WmProductMapping> wmMapping) {
        WeedmapAccount dbAccount = weedmapAccountRepository.getWeedmapAccount(token.getCompanyId(), token.getShopId(), WeedmapAccount.class);
        if (dbAccount == null) {
            throw new BlazeInvalidArgException(WEEDMAP, ACCOUNT_NOT_FOUND);
        }

        HashMap<String, WmProductMapping> updateList = new HashMap<>();
        HashMap<String, WmProductMapping> saveList = new HashMap<>();

        Set<ObjectId> productIds = new HashSet<>();
        Set<String> tagGroupIds = new HashSet<>();
        wmMapping.forEach(tagsMapping -> {
            if (StringUtils.isNotBlank(tagsMapping.getProductId()) && ObjectId.isValid(tagsMapping.getProductId())) {
                productIds.add(new ObjectId(tagsMapping.getProductId()));
            }

            if (!CollectionUtils.isNullOrEmpty(tagsMapping.getProductTagGroups())) {
                tagsMapping.getProductTagGroups().forEach(productTagGroups -> {
                    if (StringUtils.isNotBlank(productTagGroups.getWmTagGroup())) {
                        tagGroupIds.add(productTagGroups.getWmTagGroup());
                    }
                });
            }

        });

        List<WmTagGroups> tagGroupsList = getWeedmapTags();
        HashMap<String, WmTagGroups> tagGroupMap = new HashMap<>();

        tagGroupsList.forEach(wmTagGroups -> {
            tagGroupMap.put(wmTagGroups.getTagGroupId(), wmTagGroups);
        });

        wmMapping.forEach(tagsMapping -> {
            if (!CollectionUtils.isNullOrEmpty(tagsMapping.getProductTagGroups())) {
                tagsMapping.getProductTagGroups().forEach(productTagGroups -> {
                    WmTagGroups tagGroup = tagGroupMap.get(productTagGroups.getWmTagGroup());
                    if (tagGroup == null) {
                        throw new BlazeInvalidArgException(WEEDMAP, "Tag Group does not exists.");
                    }
                    WmTagGroups.WmDiscoveryTags discoveryTag = null;
                    for (WmTagGroups.WmDiscoveryTags discoveryTags : tagGroup.getDiscoveryTags()) {
                        if (discoveryTags.getTagId().equals(productTagGroups.getWmDiscoveryTag())) {
                            discoveryTag = discoveryTags;
                            break;
                        }
                    }

                    if (discoveryTag == null) {
                        throw new BlazeInvalidArgException(WEEDMAP, "Discovery tag does not exists.");
                    }
                });
            }

        });

        HashMap<String, Product> productHashMap = productRepository.findItemsInAsMap(Lists.newArrayList(productIds));

        HashMap<String, WmProductMapping> dbMappingMap = wmMappingRepository.getMappingForProducts(token.getCompanyId(), token.getShopId(), Lists.newArrayList(productHashMap.keySet()));

        HashMap<Double, List<ObjectId>> thresholdMap = new HashMap<>();

        wmMapping.forEach(tagsMapping -> {
            if (StringUtils.isBlank(tagsMapping.getProductId())) {
                throw new BlazeInvalidArgException(WEEDMAP, "Product cannot blank.");
            }
            Product product = productHashMap.get(tagsMapping.getProductId());

            if (product == null) {
                throw new BlazeInvalidArgException(WEEDMAP, "Product does not exists.");
            }

            for (ProductTagGroups productTagGroups : tagsMapping.getProductTagGroups()) {
                WmTagGroups tagGroup = tagGroupMap.get(productTagGroups.getWmTagGroup());
                if (tagGroup == null) {
                    continue;
                }
                productTagGroups.setWmTagGroupName(tagGroup.getTagGroupName());
                for (WmTagGroups.WmDiscoveryTags discoveryTags : tagGroup.getDiscoveryTags()) {
                    if (discoveryTags.getTagId().equals(productTagGroups.getWmDiscoveryTag())) {
                        productTagGroups.setWmDiscoveryTagName(discoveryTags.getTagName());
                        break;
                    }
                }
            }

            if (dbMappingMap.containsKey(tagsMapping.getProductId())) {
                WmProductMapping wmProductMapping = dbMappingMap.get(tagsMapping.getProductId());
                wmProductMapping.setCategoryId(product.getCategoryId());
                wmProductMapping.setProductId(tagsMapping.getProductId());
                wmProductMapping.setThirdPartyProduct(tagsMapping.getThirdPartyProduct());
                if (tagsMapping.getThirdPartyProduct() == null) {
                    wmProductMapping.setWmDiscoveryTag(tagsMapping.getWmDiscoveryTag());
                    wmProductMapping.setWmTagGroup(tagsMapping.getWmTagGroup());
                    wmProductMapping.setWmTags(tagsMapping.getWmTags());
                    wmProductMapping.setProductTagGroups(tagsMapping.getProductTagGroups());
                }
                wmProductMapping.setDeleted(tagsMapping.isDeleted());
                wmProductMapping.setWmThreshold(tagsMapping.getWmThreshold());
                wmProductMapping.setWmAccountId(dbAccount.getId());
                updateList.putIfAbsent(wmProductMapping.getProductId(), wmProductMapping);
            } else {
                tagsMapping.resetPrepare(token.getCompanyId());
                tagsMapping.setShopId(token.getShopId());
                tagsMapping.setWmAccountId(dbAccount.getId());
                tagsMapping.setCategoryId(product.getCategoryId());
                saveList.putIfAbsent(tagsMapping.getProductId(), tagsMapping);
            }

            thresholdMap.putIfAbsent(tagsMapping.getWmThreshold().doubleValue(), new ArrayList<>());
            thresholdMap.get(tagsMapping.getWmThreshold().doubleValue()).add(new ObjectId(product.getId()));
        });

        if (!saveList.isEmpty()) {
            wmMappingRepository.save(Lists.newArrayList(saveList.values()));
        }
        if (!updateList.isEmpty()) {
            updateList.values().forEach(wmTagsMapping -> {
                wmMappingRepository.update(wmTagsMapping.getId(), wmTagsMapping);
            });
        }
        List<WmProductMapping> returnList = Lists.newArrayList(saveList.values());
        returnList.addAll(updateList.values());
        thresholdMap.entrySet().forEach(entry -> {
            productRepository.updateWmThreshold(token.getCompanyId(), entry.getValue(), entry.getKey());
        });
        return returnList;
    }

    /**
     * Private method to prepare wm mapping result
     *
     * @param account : account
     */
    private void prepareWmMappingResult(WeedmapAccountResult account) {
        Iterable<WmProductMapping> wmProductMappings = wmMappingRepository.getMappingForAccount(token.getCompanyId(), token.getShopId(), account.getId());

        Set<String> productIds = new HashSet<>();

        wmProductMappings.forEach(wmProductMapping -> {
            productIds.add(wmProductMapping.getProductId());
        });

        HashMap<String, WmSyncItems> syncItemsMap = weedmapSyncItemRepository.getItemsByItemIdsAsMap(token.getCompanyId(), token.getShopId(), Lists.newArrayList(productIds));

        List<WmProductMappingResult> mappingList = new ArrayList<>();

        wmProductMappings.forEach(wmProductMapping -> {
            WmProductMappingResult mappingResult = new WmProductMappingResult();

            mappingResult.setThirdPartyProduct(wmProductMapping.getThirdPartyProduct());
            mappingResult.setCategoryId(wmProductMapping.getCategoryId());
            mappingResult.setProductId(wmProductMapping.getProductId());
            mappingResult.setWmDiscoveryTag(wmProductMapping.getWmDiscoveryTag());
            mappingResult.setWmTagGroup(wmProductMapping.getWmTagGroup());
            mappingResult.setWmTags(wmProductMapping.getWmTags());
            mappingResult.setDeleted(wmProductMapping.isDeleted());
            mappingResult.setId(wmProductMapping.getId());
            mappingResult.setCompanyId(wmProductMapping.getCompanyId());
            mappingResult.setShopId(wmProductMapping.getShopId());
            mappingResult.setWmAccountId(account.getId());
            mappingResult.setSynced(syncItemsMap.containsKey(wmProductMapping.getProductId()));
            if (wmProductMapping.getThirdPartyProduct() != null) {
                ThirdPartyProduct thirdPartyProduct = wmProductMapping.getThirdPartyProduct();
                mappingResult.setThirdPartyBrandName(thirdPartyProduct != null ? thirdPartyProduct.getThirdPartyBrandName() : "");
                mappingResult.setThirdPartyBrandId(thirdPartyProduct != null ? thirdPartyProduct.getThirdPartyBrandId() : "");
                mappingResult.setThirdPartyProductName(thirdPartyProduct != null ? thirdPartyProduct.getName() : "");
            }
            mappingResult.setProductTagGroups(wmProductMapping.getProductTagGroups());
            mappingResult.setWmTags(wmProductMapping.getWmTags());
            mappingResult.setWmThreshold(null);
            mappingList.add(mappingResult);
        });
        account.setTagMapping(mappingList);
    }


    @Override
    public void updateCategoryMapping(List<WmCategoryMapping> wmMapping) {
        WeedmapAccount dbAccount = weedmapAccountRepository.getWeedmapAccount(token.getCompanyId(), token.getShopId(), WeedmapAccount.class);
        if (dbAccount == null) {
            throw new BlazeInvalidArgException(WEEDMAP, ACCOUNT_NOT_FOUND);
        }

        Set<ObjectId> categoryIds = new HashSet<>();

        wmMapping.forEach(wmCategoryMapping -> {
            if (StringUtils.isNotBlank(wmCategoryMapping.getCategoryId()) && ObjectId.isValid(wmCategoryMapping.getCategoryId())) {
                categoryIds.add(new ObjectId(wmCategoryMapping.getCategoryId()));
            }
        });
        HashMap<String, ProductCategory> categoryMap = categoryRepository.findItemsInAsMap(token.getCompanyId(), token.getShopId(), Lists.newArrayList(categoryIds));

        HashMap<String, List<String>> tagsMap = new HashMap<>();

        wmMapping.forEach(wmCategoryMapping -> {
            if (StringUtils.isBlank(wmCategoryMapping.getCategoryId())) {
                throw new BlazeInvalidArgException(WEEDMAP, "Category cannot be blank.");
            }

            ProductCategory category = categoryMap.get(wmCategoryMapping.getCategoryId());

            if (category == null) {
                throw new BlazeInvalidArgException(WEEDMAP, "Category does not exists.");
            }

            boolean isMainCategory = WmProductMapping.verifyMainCategory(wmCategoryMapping.getWmCategory());
            if (!isMainCategory) {
                throw new BlazeInvalidArgException(WEEDMAP, String.format("%s is not valid weedmap category.", wmCategoryMapping.getWmCategory()));
            }

            List<String> wmTags = WmProductMapping.getDefaultTags(wmCategoryMapping.getWmCategory());

            tagsMap.put(wmCategoryMapping.getCategoryId(), wmTags);
        });

        tagsMap.forEach((categoryId, tags) -> {
            wmMappingRepository.updateTagsForCategory(token.getCompanyId(), token.getShopId(), dbAccount.getId(), categoryId, tags);
            categoryRepository.updateWmCategory(token.getCompanyId(), token.getShopId(), categoryId, tags.get(0));
        });
    }

    private HashMap<String, Object> verifyWeedmapAccount(WeedmapAccount account, WeedmapConfig prodEnvironment) {
        HashMap<String, Object> returnMap = new HashMap<>();

        if (!account.isActive() || account.isDeleted()) {
            LOGGER.info(String.format("Account not active/delete/not for version 2 for companyId: %s shopId: %s", account.getCompanyId(), account.getShopId()));
            return returnMap;
        }

        if (CollectionUtils.isNullOrEmpty(account.getApiKeyList())) {
            LOGGER.info(String.format("Api key list is empty for companyId: %s shopId: %s", account.getCompanyId(), account.getShopId()));
            return returnMap;
        }

        boolean isUpdated = false;

        List<String> wmListId = new ArrayList<>();

        for (WeedmapApiKeyMap apiKeyMap : account.getApiKeyList()) {
            if (apiKeyMap.getVersion() != WeedmapAccount.Version.V2) {
                LOGGER.info(String.format("Api key map not for V2 companyId: %s shopId: %s", account.getCompanyId(), account.getShopId()));
                continue;
            }
            if (StringUtils.isBlank(apiKeyMap.getToken())) {
                LOGGER.info(String.format("Account not authorized for companyId: %s shopId: %s", account.getCompanyId(), account.getShopId()));
                continue;
            }
            DateTime expiredTime = new DateTime(apiKeyMap.getExpiresIn());

            if (expiredTime.isBeforeNow()) {
                LOGGER.info(String.format("Token expired for companyId: %s shopId: %s", account.getCompanyId(), account.getShopId()));

                LOGGER.info(String.format("Current time: %s, Expired Time: %s", DateTime.now(), expiredTime));

                LOGGER.info(String.format("Renew token for companyId: %s shopId: %s", account.getCompanyId(), account.getShopId()));

                WeedmapTokenType type = new WeedmapTokenType();
                type.setType(WeedmapTokenType.TokenType.REFRESHTOKEN);
                taskManager.authenticateWeedmapToken(account.getCompanyId(), account.getShopId(), account, type, true, prodEnvironment);

                isUpdated = true;
            }
            wmListId.add(apiKeyMap.getListingWmId());
        }

        HashMap<String, WmSyncItems> organizationMap = weedmapSyncItemRepository.getWmItemsByListingIdAsMap(account.getCompanyId(), account.getShopId(), wmListId, WmSyncItems.WMItemType.ORGANIZATIONS);

        for (WeedmapApiKeyMap apiKeyMap : account.getApiKeyList()) {
            if (apiKeyMap.getStatus() == WeedmapAccount.AuthStatus.Authorized && StringUtils.isNotBlank(apiKeyMap.getListingWmId())) {
                returnMap.put("apiKeyMap", apiKeyMap);
            }
            WmSyncItems items = organizationMap.get(apiKeyMap.getListingWmId());

            if (items == null) {
                continue;
            }
            returnMap.put("organizationId", items.getWmItemId());
        }

        if (isUpdated) {
            weedmapAccountRepository.update(account.getId(), account);
        }
        String organizationId = (String) returnMap.get("organizationId");
        WeedmapApiKeyMap apiKeyMap = (WeedmapApiKeyMap) returnMap.get("apiKeyMap");
        if (StringUtils.isBlank(organizationId) && apiKeyMap != null) {
            LOGGER.info(String.format("Organization does not exists...creating one...for shop: %s", token.getShopId()));
            WmSyncItems wmOrganization = taskManager.createWmOrganization(token.getCompanyId(), token.getShopId(), apiKeyMap, new ArrayList<>(), new HashMap<>(), prodEnvironment, false);
            if (wmOrganization != null) {
                organizationId = wmOrganization.getWmItemId();
                returnMap.put("organizationId", organizationId);
                weedmapSyncItemRepository.save(wmOrganization);
            }
            LOGGER.info(String.format("Organization creation status : %s", StringUtils.isBlank(organizationId)));
        }
        return returnMap;
    }

    private void createWeedmapSyncJob(String companyId, String shopId, String employeeId, WeedmapSyncJob.WmSyncType syncType, WeedmapSyncJob.WmSyncJobStatus syncStatus, List<String> productIds, String accountId) {
        WeedmapSyncJob job = new WeedmapSyncJob();
        job.prepare(companyId);
        job.setShopId(shopId);
        job.setEmployeeId(employeeId);
        job.setStartTime(DateTime.now().getMillis());
        job.setRequestTime(DateTime.now().getMillis());
        job.setJobType(syncType);
        job.setStatus(syncStatus);
        job.setProductIds(productIds);
        job.setAccountId(accountId);
        wmSyncJobRepository.save(job);
        backgroundJobService.addWeedmapSyncJob(companyId, shopId, job.getId());

    }
}


