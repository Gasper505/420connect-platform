package com.fourtwenty.core.services.thirdparty.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ComplianceSQSMessageRequest {
    private String companyId;
    private String shopId;
    private String queuedComplianceJobId;

    public ComplianceSQSMessageRequest() {
    }

    public ComplianceSQSMessageRequest(String companyId, String shopId, String queuedComplianceJobId) {
        this.companyId = companyId;
        this.shopId = shopId;
        this.queuedComplianceJobId = queuedComplianceJobId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getQueuedComplianceJobId() {
        return queuedComplianceJobId;
    }

    public void setQueuedComplianceJobId(String queuedComplianceJobId) {
        this.queuedComplianceJobId = queuedComplianceJobId;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }
}
