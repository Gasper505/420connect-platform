package com.fourtwenty.core.services.thirdparty.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by mdo on 6/30/17.
 * <p>
 * {
 * "kind": "urlshortener#url",
 * "id": "https://goo.gl/i5kk9n",
 * "longUrl": "http://www.concertbeats.com/"
 * }
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class GoogleShortURLResult {
    private String id;
    private String kind;
    private String longUrl;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getLongUrl() {
        return longUrl;
    }

    public void setLongUrl(String longUrl) {
        this.longUrl = longUrl;
    }
}
