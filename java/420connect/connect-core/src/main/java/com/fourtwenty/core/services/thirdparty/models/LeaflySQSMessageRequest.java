package com.fourtwenty.core.services.thirdparty.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LeaflySQSMessageRequest {

    private String companyId;
    private String shopId;
    private String queuedLeaflyJobId;

    public LeaflySQSMessageRequest() {
    }

    public LeaflySQSMessageRequest(String companyId, String shopId, String queuedLeaflyJobId) {
        this.companyId = companyId;
        this.shopId = shopId;
        this.queuedLeaflyJobId = queuedLeaflyJobId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getQueuedLeaflyJobId() {
        return queuedLeaflyJobId;
    }

    public void setQueuedLeaflyJobId(String queuedLeaflyJobId) {
        this.queuedLeaflyJobId = queuedLeaflyJobId;
    }
}
