package com.fourtwenty.core.services.thirdparty.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.models.product.ProductWeightTolerance;
import com.fourtwenty.core.domain.serializers.BigDecimalQuantityDeserializer;
import com.fourtwenty.core.domain.serializers.BigDecimalQuantitySerializer;

import java.math.BigDecimal;

/**
 * Created by mdo on 10/3/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class MetrcVerifiedPackage {
    private int Id;
    private String Label;
    private String PackageType;
    private String SourceHarvestNames;

    @JsonSerialize(using = BigDecimalQuantitySerializer.class)
    @JsonDeserialize(using = BigDecimalQuantityDeserializer.class)
    private BigDecimal Quantity = new BigDecimal(0);
    private String UnitOfMeasureName;
    private String UnitOfMeasureAbbreviation;
    private boolean available = false;

    @JsonSerialize(using = BigDecimalQuantitySerializer.class)
    @JsonDeserialize(using = BigDecimalQuantityDeserializer.class)
    private BigDecimal blazeQuantity = new BigDecimal(0);
    private boolean required = false;
    private ProductWeightTolerance.WeightKey blazeMeasurement = ProductWeightTolerance.WeightKey.CUSTOM;

    public ProductWeightTolerance.WeightKey getBlazeMeasurement() {
        return blazeMeasurement;
    }

    public void setBlazeMeasurement(ProductWeightTolerance.WeightKey blazeMeasurement) {
        this.blazeMeasurement = blazeMeasurement;
    }


    public BigDecimal getQuantity() {
        return Quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        Quantity = quantity;
    }

    public BigDecimal getBlazeQuantity() {
        return blazeQuantity;
    }

    public void setBlazeQuantity(BigDecimal blazeQuantity) {
        this.blazeQuantity = blazeQuantity;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }



    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getLabel() {
        return Label;
    }

    public void setLabel(String label) {
        Label = label;
    }

    public String getPackageType() {
        return PackageType;
    }

    public void setPackageType(String packageType) {
        PackageType = packageType;
    }

    public String getSourceHarvestNames() {
        return SourceHarvestNames;
    }

    public void setSourceHarvestNames(String sourceHarvestNames) {
        SourceHarvestNames = sourceHarvestNames;
    }



    public String getUnitOfMeasureName() {
        return UnitOfMeasureName;
    }

    public void setUnitOfMeasureName(String unitOfMeasureName) {
        UnitOfMeasureName = unitOfMeasureName;
    }

    public String getUnitOfMeasureAbbreviation() {
        return UnitOfMeasureAbbreviation;
    }

    public void setUnitOfMeasureAbbreviation(String unitOfMeasureAbbreviation) {
        UnitOfMeasureAbbreviation = unitOfMeasureAbbreviation;
    }
}
