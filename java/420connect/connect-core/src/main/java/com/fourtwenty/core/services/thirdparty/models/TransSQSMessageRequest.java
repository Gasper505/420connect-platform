package com.fourtwenty.core.services.thirdparty.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by mdo on 10/13/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class TransSQSMessageRequest {
    private String companyId;
    private String shopId;
    private String queueTransactionId;

    public TransSQSMessageRequest() {
    }

    public TransSQSMessageRequest(String companyId, String shopId, String queueTransactionId) {
        this.companyId = companyId;
        this.shopId = shopId;
        this.queueTransactionId = queueTransactionId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getQueueTransactionId() {
        return queueTransactionId;
    }

    public void setQueueTransactionId(String queueTransactionId) {
        this.queueTransactionId = queueTransactionId;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }
}
