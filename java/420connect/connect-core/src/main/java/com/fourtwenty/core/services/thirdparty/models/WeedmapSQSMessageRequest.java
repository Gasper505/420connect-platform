package com.fourtwenty.core.services.thirdparty.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WeedmapSQSMessageRequest {
    private String companyId;
    private String shopId;
    private String queuedWeedmapJobId;

    public WeedmapSQSMessageRequest() {
    }

    public WeedmapSQSMessageRequest(String companyId, String shopId, String queuedWeedmapJobId) {
        this.companyId = companyId;
        this.shopId = shopId;
        this.queuedWeedmapJobId = queuedWeedmapJobId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getQueuedWeedmapJobId() {
        return queuedWeedmapJobId;
    }

    public void setQueuedWeedmapJobId(String queuedWeedmapJobId) {
        this.queuedWeedmapJobId = queuedWeedmapJobId;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }
}
