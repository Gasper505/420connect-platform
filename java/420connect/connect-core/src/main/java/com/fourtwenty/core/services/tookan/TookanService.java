package com.fourtwenty.core.services.tookan;

import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.tookan.EmployeeTookanInfo;
import com.fourtwenty.core.domain.models.tookan.TookanAccount;
import com.fourtwenty.core.domain.models.tookan.TookanTeamInfo;
import com.fourtwenty.core.domain.models.tookan.TookanTeams;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.thirdparty.onfleet.models.request.SynchronizeTeamRequest;
import com.fourtwenty.core.thirdparty.tookan.model.request.TookanAccountRequest;
import com.fourtwenty.core.thirdparty.tookan.model.request.fleets.AgentSynchronizeRequest;
import com.fourtwenty.core.thirdparty.tookan.model.request.team.TeamAddRequest;
import com.fourtwenty.core.thirdparty.tookan.model.response.EmployeeTookanInfoResult;
import com.fourtwenty.core.thirdparty.tookan.model.response.TookanTaskResult;

public interface TookanService {

    TookanAccount addAccount(TookanAccountRequest request);

    TookanAccount updateAccount(String tookanAccountId, TookanAccountRequest account);

    SearchResult<TookanAccount> getAccount(String shopId, int start, int limit);

    TookanTeams teamOperationRequest(TeamAddRequest request);

    TookanTeams synchronizeTeam(SynchronizeTeamRequest request);

    EmployeeTookanInfoResult synchronizeAgents(String employeeId, AgentSynchronizeRequest request);

    void removeAgent(String employeeId, SynchronizeTeamRequest request);

    void createTookanTask(Transaction transaction, Employee employee, Member member, String teamId, String agentId, boolean isReassign);

    boolean checkTeamAssignment(String shopId, String teamId, EmployeeTookanInfo agentInfo);

    Employee createTookanAgent(String employeeId, AgentSynchronizeRequest request);

    void deleteAccount(String tookanAccountId);

    SearchResult<TookanTeams> getTookanTeams(String shopId, int start, int limit);

    DateSearchResult<TookanTaskResult> getTookanTasks(long afterDate, long beforeDate, TookanTaskResult.TookanTaskType taskType, TookanTaskResult.TookanTaskStatus status);

    void updateTaskStatus(String companyId, String shopId, Transaction transaction, TookanTaskResult.TookanTaskStatus status);

    TookanTeamInfo getTookanTeamInfo(String companyId, String shopId, String teamId);

}
