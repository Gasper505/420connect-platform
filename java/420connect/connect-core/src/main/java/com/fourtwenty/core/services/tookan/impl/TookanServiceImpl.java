package com.fourtwenty.core.services.tookan.impl;

import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.thirdparty.OnFleetTeams;
import com.fourtwenty.core.domain.models.tookan.EmployeeTookanInfo;
import com.fourtwenty.core.domain.models.tookan.TookanAccount;
import com.fourtwenty.core.domain.models.tookan.TookanTeamInfo;
import com.fourtwenty.core.domain.models.tookan.TookanTeams;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.EmployeeRepository;
import com.fourtwenty.core.domain.repositories.dispensary.MemberRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.domain.repositories.tookan.TookanAccountRepository;
import com.fourtwenty.core.domain.repositories.tookan.TookanTeamRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.managed.TookanApiManager;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.tookan.TookanService;
import com.fourtwenty.core.thirdparty.onfleet.models.request.SynchronizeTeamRequest;
import com.fourtwenty.core.thirdparty.tookan.model.request.TookanAccountRequest;
import com.fourtwenty.core.thirdparty.tookan.model.request.fleets.AgentAddRequest;
import com.fourtwenty.core.thirdparty.tookan.model.request.fleets.AgentDeleteRequest;
import com.fourtwenty.core.thirdparty.tookan.model.request.fleets.AgentEditRequest;
import com.fourtwenty.core.thirdparty.tookan.model.request.fleets.AgentSynchronizeRequest;
import com.fourtwenty.core.thirdparty.tookan.model.request.task.TaskDetailRequest;
import com.fourtwenty.core.thirdparty.tookan.model.request.task.TaskDetailStatusRequest;
import com.fourtwenty.core.thirdparty.tookan.model.request.team.TeamAddRequest;
import com.fourtwenty.core.thirdparty.tookan.model.request.team.TookanTeamAddRequest;
import com.fourtwenty.core.thirdparty.tookan.model.request.team.TookanTeamDeleteRequest;
import com.fourtwenty.core.thirdparty.tookan.model.request.team.TookanTeamUpdateRequest;
import com.fourtwenty.core.thirdparty.tookan.model.response.EmployeeTookanInfoResult;
import com.fourtwenty.core.thirdparty.tookan.model.response.TookanAgentInfo;
import com.fourtwenty.core.thirdparty.tookan.model.response.TookanTaskInfo;
import com.fourtwenty.core.thirdparty.tookan.model.response.TookanTaskResult;
import com.fourtwenty.core.thirdparty.tookan.service.TookanApiService;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.core.util.JsonSerializer;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.google.inject.Provider;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import java.util.*;

import static com.fourtwenty.core.managed.AmazonServiceManager.cleanPhoneNumber;

public class TookanServiceImpl extends AbstractAuthServiceImpl implements TookanService {

    private static final String TOOKAN_ACCOUNT = "Tookan Account";
    private static final String APIKEY_NOT_BLANK = "Api Key cannot be blank.";
    private static final String ACCOUNT_NOT_FOUND = "Tookan Account does not exist.";
    private static final String TOOKAN_TEAM = "Tookan Team";
    private static final String TOOKAN_TEAM_NOT_FOUND = "Tookan Team does not exist.";
    private static final String TYPE_NOT_FOUND = "Operation Type does not exist.";
    private static final String EMPLOYEE = "Tookan Agent";
    private static final String EMPLOYEE_NOT_FOUND = "Employee does not exist.";
    private static final String EMPLOYEE_MAIL_NOT_FOUND = "Employee email does not exist.";
    private static final String EMPLOYEE_PHONE_NOT_FOUND = "Employee phone does not exist.";
    private static final String TOOKAN_TASK = "Tookan Task";
    private static final String API_KEY_NOT_VALID = "Invalid Api Key";

    @Inject
    private TookanAccountRepository tookanAccountRepository;
    @Inject
    private TookanTeamRepository tookanTeamRepository;
    @Inject
    private TookanApiService tookanApiService;
    @Inject
    private EmployeeRepository employeeRepository;
    @Inject
    private ShopRepository shopRepository;
    @Inject
    private TookanApiManager tookanApiManager;
    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private MemberRepository memberRepository;

    @Inject
    public TookanServiceImpl(Provider<ConnectAuthToken> token) {
        super(token);
    }

    /**
     * Private method to get tookan account by shop.
     *
     * @param shopId : shop id
     * @return : tookan account.
     */
    private TookanAccount getTookanAccount(String shopId) {
        TookanAccount dbAccount = tookanAccountRepository.getAccountByShop(token.getCompanyId(), shopId);
        if (dbAccount == null || StringUtils.isBlank(dbAccount.getApiKey()) || !dbAccount.isActive()) {
            throw new BlazeInvalidArgException(TOOKAN_ACCOUNT, ACCOUNT_NOT_FOUND);
        }
        return dbAccount;
    }

    /**
     * Override method to create tookan account.
     * One per shop and if already exists then delete all previous added account.
     *
     * @param request : add tookan account
     * @return : tookan account
     */
    @Override
    public TookanAccount addAccount(TookanAccountRequest request) {
        if (request == null || StringUtils.isBlank(request.getApiKey())) {
            throw new BlazeInvalidArgException(TOOKAN_ACCOUNT, APIKEY_NOT_BLANK);
        }

        boolean isValidKey = tookanApiService.authenticateApiKey(token.getCompanyId(), request.getShopId(), request.getApiKey());
        if (!isValidKey) {
            throw new BlazeInvalidArgException(TOOKAN_ACCOUNT, API_KEY_NOT_VALID);
        }
        TookanAccount account = tookanAccountRepository.getAccountByShop(token.getCompanyId(), StringUtils.isNotBlank(request.getShopId()) ? request.getShopId() : token.getShopId());
        boolean isNew = false;
        if (account == null) {
            account = new TookanAccount();
            account.prepare(token.getCompanyId());
            account.setShopId(StringUtils.isNotBlank(request.getShopId()) ? request.getShopId() : token.getShopId());
            isNew = true;
        }
        if (!isNew && !account.getApiKey().equals(request.getApiKey()) && request.isResetTookan()) {
            resetTookanDetails(account.getShopId());
        }
        account.setApiKey(request.getApiKey());
        account.setActive(request.isActive());
        account = tookanAccountRepository.save(account);
        shopRepository.updateEnableTookan(account.getShopId(), account.isActive());

        return account;
    }

    /**
     * Override method to update tookan account for given shop.
     *
     * @param tookanAccountId : tookan account id
     * @param account         : tookan account request
     * @return : updated tookan response
     */
    @Override
    public TookanAccount updateAccount(String tookanAccountId, TookanAccountRequest account) {
        if (StringUtils.isBlank(tookanAccountId)) {
            throw new BlazeInvalidArgException(TOOKAN_ACCOUNT, ACCOUNT_NOT_FOUND);
        }
        if (account == null || (StringUtils.isBlank(account.getApiKey()) && account.isActive())) {
            throw new BlazeInvalidArgException(TOOKAN_ACCOUNT, APIKEY_NOT_BLANK);
        }
        if (StringUtils.isNotBlank(account.getApiKey())) {
            boolean isValidKey = tookanApiService.authenticateApiKey(token.getCompanyId(), account.getShopId(), account.getApiKey());
            if (!isValidKey) {
                throw new BlazeInvalidArgException(TOOKAN_ACCOUNT, API_KEY_NOT_VALID);
            }
        }
        TookanAccount dbAccount = tookanAccountRepository.getById(tookanAccountId);
        if (dbAccount == null) {
            throw new BlazeInvalidArgException(TOOKAN_ACCOUNT, ACCOUNT_NOT_FOUND);
        }
        if ((StringUtils.isBlank(account.getApiKey())
                || !dbAccount.getApiKey().equals(account.getApiKey()))
                && account.isResetTookan()) {
            resetTookanDetails(account.getShopId());
        }
        dbAccount.setActive(account.isActive());
        dbAccount.setApiKey(account.getApiKey());
        dbAccount = tookanAccountRepository.update(token.getCompanyId(), tookanAccountId, dbAccount);
        shopRepository.updateEnableTookan(dbAccount.getShopId(), dbAccount.isActive());
        return dbAccount;
    }

    /**
     * Overide method to get list of all tookan accounts.
     *
     * @param shopId : shopId
     * @param start  : start
     * @param limit  : limit
     * @return : list of all tookan accounts
     */
    @Override
    public SearchResult<TookanAccount> getAccount(String shopId, int start, int limit) {
        limit = (limit == 0) ? Integer.MAX_VALUE : limit;
        if (StringUtils.isNotBlank(shopId)) {
            return tookanAccountRepository.findItems(token.getCompanyId(), shopId, start, limit);
        } else {
            return tookanAccountRepository.findItems(token.getCompanyId(), start, limit);
        }
    }

    /**
     * Ovberride method to delete tookan account
     *
     * @param tookanAccountId : tookan account id
     */
    @Override
    public void deleteAccount(String tookanAccountId) {
        if (StringUtils.isBlank(tookanAccountId)) {
            throw new BlazeInvalidArgException(TOOKAN_ACCOUNT, ACCOUNT_NOT_FOUND);
        }
        TookanAccount dbAccount = tookanAccountRepository.getById(tookanAccountId);
        if (dbAccount == null) {
            throw new BlazeInvalidArgException(TOOKAN_ACCOUNT, ACCOUNT_NOT_FOUND);
        }
        tookanAccountRepository.removeById(tookanAccountId);
        shopRepository.updateEnableTookan(dbAccount.getShopId(), false);
    }

    /**
     * Override method for team add,delete,update request for tookan team add,update,delete..
     *
     * @param request : request
     * @return : tookan team
     */
    @Override
    public TookanTeams teamOperationRequest(TeamAddRequest request) {
        if (request == null || request.getOperationType() == null) {
            throw new BlazeInvalidArgException(TOOKAN_TEAM, TYPE_NOT_FOUND);
        }
        if (request.getOperationType() == TeamAddRequest.OperationType.CREATE && StringUtils.isBlank(request.getTeamName())) {
            throw new BlazeInvalidArgException(TOOKAN_TEAM, "Team name cannot be blank.");
        }
        if ((request.getOperationType() == TeamAddRequest.OperationType.UPDATE
                || request.getOperationType() == TeamAddRequest.OperationType.DELETE)
                && StringUtils.isBlank(request.getTeamId())) {
            throw new BlazeInvalidArgException(TOOKAN_TEAM, "Team Id cannot be blank.");
        }

        String shopId = StringUtils.isNotBlank(request.getShopId()) ? request.getShopId() : token.getShopId();

        TookanAccount dbAccount = getTookanAccount(shopId);

        TookanTeams tookanTeam = getOrUpdateTeam(token.getCompanyId(), shopId);

        TookanTeamInfo teamInfo = new TookanTeamInfo();
        if (request.getOperationType() == TeamAddRequest.OperationType.UPDATE ||
                request.getOperationType() == TeamAddRequest.OperationType.DELETE) {
            boolean isTeamFound = false;
            for (TookanTeamInfo tookanTeamInfo : tookanTeam.getTookanTeamInfo()) {
                if (tookanTeamInfo.getId().equals(request.getTeamId())) {
                    teamInfo = tookanTeamInfo;
                    isTeamFound = true;
                    break;
                }
            }
            if (isTeamFound) {
                throw new BlazeInvalidArgException(TOOKAN_TEAM, "Team not found for : " + request.getTeamId());
            }
        }

        switch (request.getOperationType()) {
            case CREATE:
                TookanTeamAddRequest addRequest = new TookanTeamAddRequest();
                addRequest.setApiKey(dbAccount.getApiKey());
                addRequest.setBatteryUsage(request.getBatteryUsage());
                addRequest.setTags(request.getTags());
                addRequest.setTeamName(request.getTeamName());
                teamInfo = tookanApiService.addTookanTeam(token.getCompanyId(), shopId, dbAccount.getApiKey(), addRequest);
                tookanTeam.getTookanTeamInfo().add(teamInfo);
                break;
            case UPDATE:
                TookanTeamUpdateRequest updateRequest = new TookanTeamUpdateRequest();
                updateRequest.setApiKey(dbAccount.getApiKey());
                updateRequest.setBatteryUsage(request.getBatteryUsage());
                updateRequest.setTags(request.getTags());
                updateRequest.setTeamName(request.getTeamName());
                updateRequest.setTeamId(teamInfo.getTeamId());
                tookanApiService.updateTookanTeam(token.getCompanyId(), shopId, dbAccount.getApiKey(), updateRequest, teamInfo);
                break;
            case DELETE:
                TookanTeamDeleteRequest deleteRequest = new TookanTeamDeleteRequest();
                deleteRequest.setApiKey(dbAccount.getApiKey());
                deleteRequest.setTeamId(teamInfo.getTeamId());
                tookanApiService.deleteTookanTeam(token.getCompanyId(), shopId, dbAccount.getApiKey(), deleteRequest, teamInfo);
                break;
        }

        if (teamInfo != null) {
            tookanTeamRepository.update(token.getCompanyId(), tookanTeam.getId(), tookanTeam);
        }

        return tookanTeam;
    }

    /**
     * Override method to synchronize teams from third party.
     *
     * @param request Synchronize Team Request
     */
    @Override
    public TookanTeams synchronizeTeam(SynchronizeTeamRequest request) {
        String shopId = (StringUtils.isNotBlank(request.getShopId())) ? request.getShopId() : token.getShopId();
        TookanAccount dbAccount = getTookanAccount(request.getShopId());
        TookanTeams tookanTeam = getOrUpdateTeam(token.getCompanyId(), shopId);
        tookanTeam.setSyncState(OnFleetTeams.SyncState.InProgress);
        tookanApiService.synchronizeTeams(token.getCompanyId(), shopId, dbAccount.getApiKey(), tookanTeam);

        tookanTeam.setSyncDate(DateTime.now().getMillis());
        tookanTeam.setSyncState(OnFleetTeams.SyncState.Completed);

        tookanTeamRepository.update(token.getCompanyId(), tookanTeam.getId(), tookanTeam);
        return tookanTeam;
    }

    /**
     * Private method for tookan teams.
     *
     * @param companyId : company id
     * @param shopId    : shop id
     * @return : get or create tookan team
     */
    private TookanTeams getOrUpdateTeam(String companyId, String shopId) {
        TookanTeams tookanTeam = tookanTeamRepository.getTeamByShop(companyId, shopId);
        if (tookanTeam == null) {
            tookanTeam = new TookanTeams();
            tookanTeam.prepare(companyId);
            tookanTeam.setShopId(shopId);
            tookanTeam.setTookanTeamInfo(new ArrayList<>());
            tookanTeamRepository.save(tookanTeam);
        }
        return tookanTeam;
    }

    /**
     * Override method for synchronizing/update agents from tookan
     *
     * @param employeeId : verify if employee on tookan
     * @param request    : shop synchronization request
     * @return : employee tookan info
     */
    @Override
    public EmployeeTookanInfoResult synchronizeAgents(String employeeId, AgentSynchronizeRequest request) {
        TookanAgentInfo agentInfo;
        EmployeeTookanInfo tookanAgentInfo = null;
        EmployeeTookanInfoResult result = new EmployeeTookanInfoResult();

        Employee employee = employeeRepository.getById(employeeId);
        if (employee == null) {
            throw new BlazeInvalidArgException(EMPLOYEE, EMPLOYEE_NOT_FOUND);
        }

        Shop shop = shopRepository.getById((StringUtils.isNotBlank(request.getShopId()) ? request.getShopId() : token.getShopId()));
        if (shop == null) {
            throw new BlazeInvalidArgException("Shop", "Shop not found");
        }

        if (StringUtils.isBlank(employee.getEmail())) {
            throw new BlazeInvalidArgException(EMPLOYEE, EMPLOYEE_MAIL_NOT_FOUND);
        }
        if (StringUtils.isBlank(employee.getPhoneNumber())) {
            throw new BlazeInvalidArgException(EMPLOYEE, EMPLOYEE_PHONE_NOT_FOUND);
        }


        TookanAccount dbAccount = getTookanAccount(shop.getId());

        TookanTeams tookanTeams = synchronizeTeam(request);
        if (tookanTeams == null || tookanTeams.getTookanTeamInfo() == null || tookanTeams.getTookanTeamInfo().isEmpty()) {
            throw new BlazeInvalidArgException(TOOKAN_TEAM, TOOKAN_TEAM_NOT_FOUND);
        }
        TookanTeamInfo tookanTeamInfo = null;
        for (TookanTeamInfo teamInfo : tookanTeams.getTookanTeamInfo()) {
            if (teamInfo.getId().equals(request.getTeamId())) {
                tookanTeamInfo = teamInfo;
                break;
            }
        }
        if (tookanTeamInfo == null) {
            throw new BlazeInvalidArgException(TOOKAN_TEAM, TOOKAN_TEAM_NOT_FOUND);
        }

        boolean isUpdateAgent = false;

        if (employee.getTookanInfoList() != null && !employee.getTookanInfoList().isEmpty()) {

            tookanAgentInfo = tookanApiService.getEmployeeTookanInfo(token.getCompanyId(), employee, shop.getId());

            if (tookanAgentInfo != null && StringUtils.isNotBlank(tookanAgentInfo.getTeamId())) {
                if (tookanAgentInfo.getTeamId().equals(tookanTeamInfo.getTeamId().toString())) {
                    result.setShopId(tookanAgentInfo.getShopId());
                    result.setSyncWithTookan(true);
                    result.setTeamId(tookanAgentInfo.getTeamId());
                    result.setStatus(tookanAgentInfo.getStatus());
                    result.setTookanAgentId(tookanAgentInfo.getTookanAgentId());
                    result.setTeamName(tookanTeamInfo.getTeamName());
                    return result;
                } else {
                    isUpdateAgent = true;
                }
            }
        }

        String timeZone = token.getRequestTimeZone();
        if (StringUtils.isBlank(token.getRequestTimeZone())) {
            timeZone = shop.getTimeZone();
        }

        AgentEditRequest agentRequest = new AgentEditRequest();
        agentRequest.setFirstName(employee.getFirstName());
        agentRequest.setLastName(employee.getLastName());
        agentRequest.setEmail(employee.getEmail());
        agentRequest.setPhone(cleanPhoneNumber(employee.getPhoneNumber(), shop));

        agentRequest.setUsername(employee.getEmail());

        agentRequest.setTeamId(tookanTeamInfo.getTeamId().toString());
        agentRequest.setTimezone(String.valueOf(DateUtil.getTimezoneOffsetInMinutes(timeZone)));
        agentRequest.setApiKey(dbAccount.getApiKey());
        if (isUpdateAgent) {
            agentRequest.setFleetId(tookanAgentInfo.getTookanAgentId());
        }
        String country = "US";
        if (shop.getAddress() != null && shop.getAddress().getCountry() != null) {
            country = shop.getAddress().getCountry();
        }
        agentInfo = tookanApiService.synchronizeAgents(token.getCompanyId(), shop.getId(), country, dbAccount.getApiKey(), agentRequest);
        if (agentInfo != null) {
            List<EmployeeTookanInfo> employeeTookanInfoList = employee.getTookanInfoList();
            if (employeeTookanInfoList == null) {
                employeeTookanInfoList = new ArrayList<>();
                EmployeeTookanInfo employeeTookanInfo = new EmployeeTookanInfo();
                employeeTookanInfo.setShopId(shop.getId());
                employeeTookanInfo.setTookanAgentId(agentInfo.getFleetId().toString());
                employeeTookanInfo.setTeamId(agentInfo.getTeamId().toString());
                employeeTookanInfo.setStatus(EmployeeTookanInfo.AgentStatus.toAgentStatus(agentInfo.getStatus().intValue()));
                employeeTookanInfoList.add(employeeTookanInfo);
                tookanAgentInfo = employeeTookanInfo;
            } else {
                for (EmployeeTookanInfo tookanInfo : employeeTookanInfoList) {
                    if (tookanInfo.getShopId().equals(shop.getId())) {
                        tookanInfo.setShopId(shop.getId());
                        tookanInfo.setTookanAgentId(agentInfo.getFleetId().toString());
                        tookanInfo.setTeamId(agentInfo.getTeamId().toString());
                        tookanInfo.setStatus(EmployeeTookanInfo.AgentStatus.toAgentStatus(agentInfo.getStatus().intValue()));
                        tookanAgentInfo = tookanInfo;
                        break;
                    }
                }
            }

            employee.setTookanInfoList(employeeTookanInfoList);
            employeeRepository.update(employee.getCompanyId(), employee.getId(), employee);
        }

        if (agentInfo != null && tookanAgentInfo != null) {
            result.setShopId(tookanAgentInfo.getShopId());
            result.setSyncWithTookan(true);
            result.setTeamId(tookanAgentInfo.getTeamId());
            result.setStatus(tookanAgentInfo.getStatus());
            result.setTookanAgentId(tookanAgentInfo.getTookanAgentId());
            result.setTeamName(tookanTeamInfo.getTeamName());
        } else {
            result.setShopId(request.getShopId());
            result.setSyncWithTookan(false);
            result.setTeamId(request.getTeamId());
        }

        return result;

    }

    @Override
    public void removeAgent(String employeeId, SynchronizeTeamRequest request) {

        Employee employee = employeeRepository.getById(employeeId);
        if (employee == null) {
            throw new BlazeInvalidArgException(EMPLOYEE, EMPLOYEE_NOT_FOUND);
        }

        Shop shop = shopRepository.getById((StringUtils.isNotBlank(request.getShopId()) ? request.getShopId() : token.getShopId()));
        if (shop == null) {
            throw new BlazeInvalidArgException("Shop", "Shop not found");
        }

        if (employee.getTookanInfoList() == null || employee.getTookanInfoList().isEmpty()) {
            throw new BlazeInvalidArgException(EMPLOYEE, "Employee does not exists on tookan.");
        }

        EmployeeTookanInfo employeeTookanInfo = tookanApiService.getEmployeeTookanInfo(token.getCompanyId(), employee, shop.getId());
        if (employeeTookanInfo == null) {
            throw new BlazeInvalidArgException(EMPLOYEE, "Employee does not exists on tookan.");
        }

        TookanAccount dbAccount = getTookanAccount(shop.getId());

        AgentDeleteRequest agentDeleteRequest = new AgentDeleteRequest();
        agentDeleteRequest.setFleetId(employeeTookanInfo.getTookanAgentId());
        agentDeleteRequest.setApiKey(dbAccount.getApiKey());

        boolean isDeleted = tookanApiService.removeAgent(token.getCompanyId(), shop.getId(), dbAccount.getApiKey(), agentDeleteRequest);
        if (isDeleted) {
            for (EmployeeTookanInfo tookanInfo : employee.getTookanInfoList()) {
                if (tookanInfo.getShopId().equals(shop.getId())) {
                    employee.getTookanInfoList().remove(tookanInfo);
                    break;
                }
            }
            employeeRepository.update(token.getCompanyId(), employeeId, employee);
        } else {
            throw new BlazeInvalidArgException(EMPLOYEE, "Agent does not deleted from tookan.");
        }
    }

    /**
     * Override method to create tookan task
     *
     * @param transaction : transaction
     * @param employee    : employee/agent
     * @param member      : member
     * @param teamId      : team id
     * @param agentId     : agent id
     */

    @Override
    public void createTookanTask(Transaction transaction, Employee employee, Member member, String teamId, String agentId, boolean isReassign) {
        TookanAccount dbAccount = tookanAccountRepository.getAccountByShop(token.getCompanyId(), token.getShopId());
        if (dbAccount == null || StringUtils.isBlank(dbAccount.getApiKey())) {
            throw new BlazeInvalidArgException(TOOKAN_ACCOUNT, ACCOUNT_NOT_FOUND);
        }
        String timeZone = token.getRequestTimeZone();
        if (StringUtils.isBlank(token.getRequestTimeZone())) {
            Shop shop = shopRepository.getById(token.getShopId());
            if (shop != null) {
                timeZone = shop.getTimeZone();
            }
        }
        if (StringUtils.isNotBlank(transaction.getAssignedEmployeeId()) && StringUtils.isNotBlank(teamId) && StringUtils.isNotBlank(agentId)) {
            tookanApiManager.createTookanTask(token.getCompanyId(), token.getShopId(), timeZone, dbAccount.getApiKey(), transaction, member, employee, teamId, agentId, isReassign);
        } else {
            tookanApiManager.createTookanTaskWithoutEmployee(token.getCompanyId(), token.getShopId(), timeZone, dbAccount.getApiKey(), transaction, member, isReassign);
        }
    }

    /**
     * Override method to check if team is assigned to employee
     *
     * @param shopId    : shop id
     * @param teamId    : team id
     * @param agentInfo : tookan agent info
     * @return : return false if fleet info is blank or null or if not found else return true
     */
    @Override
    public boolean checkTeamAssignment(String shopId, String teamId, EmployeeTookanInfo agentInfo) {
        SynchronizeTeamRequest request = new SynchronizeTeamRequest();
        request.setShopId(shopId);

        TookanTeams tookanTeams = synchronizeTeam(request);
        if (tookanTeams.getTookanTeamInfo() == null || tookanTeams.getTookanTeamInfo().isEmpty()) {
            return false;
        }

        TookanTeamInfo teamInfo = tookanApiService.getTookanTeamInfo(token.getCompanyId(), shopId, tookanTeams, teamId);

        return (teamInfo != null && teamInfo.getTeamId().toString().equalsIgnoreCase(agentInfo.getTeamId()));
    }

    /**
     * Override method to create tookan agent
     *
     * @param employeeId : employee id
     * @param request    : agent creation request
     * @return : return employee
     */
    @Override
    public Employee createTookanAgent(String employeeId, AgentSynchronizeRequest request) {
        TookanAgentInfo agentInfo;
        EmployeeTookanInfo tookanAgentInfo;

        Employee employee = employeeRepository.getById(employeeId);
        if (employee == null) {
            throw new BlazeInvalidArgException(EMPLOYEE, EMPLOYEE_NOT_FOUND);
        }

        Shop shop = shopRepository.getById((StringUtils.isNotBlank(request.getShopId()) ? request.getShopId() : token.getShopId()));
        if (shop == null) {
            throw new BlazeInvalidArgException("Shop", "Shop not found");
        }

        if (StringUtils.isBlank(employee.getEmail())) {
            throw new BlazeInvalidArgException(EMPLOYEE, EMPLOYEE_MAIL_NOT_FOUND);
        }
        if (StringUtils.isBlank(employee.getPhoneNumber())) {
            throw new BlazeInvalidArgException(EMPLOYEE, EMPLOYEE_PHONE_NOT_FOUND);
        }


        TookanAccount dbAccount = getTookanAccount(shop.getId());

        TookanTeams tookanTeams = synchronizeTeam(request);
        if (tookanTeams == null || tookanTeams.getTookanTeamInfo() == null || tookanTeams.getTookanTeamInfo().isEmpty()) {
            throw new BlazeInvalidArgException(TOOKAN_TEAM, TOOKAN_TEAM_NOT_FOUND);
        }
        TookanTeamInfo tookanTeamInfo = null;
        for (TookanTeamInfo teamInfo : tookanTeams.getTookanTeamInfo()) {
            if (teamInfo.getId().equals(request.getTeamId())) {
                tookanTeamInfo = teamInfo;
                break;
            }
        }
        if (tookanTeamInfo == null) {
            throw new BlazeInvalidArgException(TOOKAN_TEAM, TOOKAN_TEAM_NOT_FOUND);
        }

        if (employee.getTookanInfoList() != null && !employee.getTookanInfoList().isEmpty()) {

            tookanAgentInfo = tookanApiService.getEmployeeTookanInfo(token.getCompanyId(), employee, shop.getId());

            if (tookanAgentInfo != null && StringUtils.isNotBlank(tookanAgentInfo.getTeamId())) {
                if (tookanAgentInfo.getTeamId().equals(tookanTeamInfo.getTeamId().toString())) {
                    return employee;
                }
            }
        }

        // see if you can synchronize based on phone number
        String country = "US";
        if (shop.getAddress() != null && shop.getAddress().getCountry() != null) {
            country = shop.getAddress().getCountry();
        }
        String timeZone = token.getRequestTimeZone();
        if (StringUtils.isBlank(token.getRequestTimeZone())) {
            timeZone = shop.getTimeZone();
        }

        AgentEditRequest editAgentRequest = new AgentEditRequest();
        editAgentRequest.setFirstName(employee.getFirstName());
        editAgentRequest.setLastName(employee.getLastName());
        editAgentRequest.setEmail(employee.getEmail());
        editAgentRequest.setPhone(cleanPhoneNumber(employee.getPhoneNumber(), shop));

        editAgentRequest.setUsername(employee.getEmail());

        editAgentRequest.setTeamId(tookanTeamInfo.getTeamId().toString());
        editAgentRequest.setTimezone(String.valueOf(DateUtil.getTimezoneOffsetInMinutes(timeZone)));
        editAgentRequest.setApiKey(dbAccount.getApiKey());


        agentInfo = tookanApiService.synchronizeAgents(token.getCompanyId(), shop.getId(), country, dbAccount.getApiKey(), editAgentRequest);


        if (agentInfo == null) {
            // if agent does not already exist, try creating a new one
            AgentAddRequest agentRequest = new AgentAddRequest();
            agentRequest.setFirstName(employee.getFirstName());
            agentRequest.setLastName(employee.getLastName());
            agentRequest.setEmail(employee.getEmail());
            agentRequest.setPhone(cleanPhoneNumber(employee.getPhoneNumber(), shop));

            agentRequest.setUsername(employee.getEmail());

            agentRequest.setTeamId(tookanTeamInfo.getTeamId().toString());
            agentRequest.setTimezone(String.valueOf(DateUtil.getTimezoneOffsetInMinutes(timeZone)));
            agentRequest.setApiKey(dbAccount.getApiKey());

            agentInfo = tookanApiService.addTookanAgent(token.getCompanyId(), shop.getId(), dbAccount.getApiKey(), agentRequest);
        }
        if (agentInfo != null) {
            List<EmployeeTookanInfo> employeeTookanInfoList = employee.getTookanInfoList();
            EmployeeTookanInfo employeeTookanInfo = new EmployeeTookanInfo();
            boolean isFound = false;
            if (employeeTookanInfoList == null) {
                employeeTookanInfoList = new ArrayList<>();
            } else {
                for (EmployeeTookanInfo employeeTookan : employeeTookanInfoList) {
                    if (employeeTookan.getShopId().equalsIgnoreCase(shop.getId())) {
                        employeeTookanInfo = employeeTookan;
                        isFound = true;
                        break;
                    }
                }
            }
            employeeTookanInfo.setShopId(shop.getId());
            employeeTookanInfo.setTookanAgentId(agentInfo.getFleetId().toString());
            employeeTookanInfo.setTeamId(tookanTeamInfo.getTeamId().toString());
            employeeTookanInfo.setStatus(EmployeeTookanInfo.AgentStatus.toAgentStatus(agentInfo.getStatus().intValue()));
            if (!isFound) {
                employeeTookanInfoList.add(employeeTookanInfo);
            }
            employee.setTookanInfoList(employeeTookanInfoList);
            employee = employeeRepository.update(employee.getCompanyId(), employee.getId(), employee);
        }

        return employee;
    }

    /**
     * Override method to return all tookan teams or given shop teams
     *
     * @param shopId : shopId
     * @param start  : start
     * @param limit  : limit
     * @return : return list of all tookan teams
     */
    @Override
    public SearchResult<TookanTeams> getTookanTeams(String shopId, int start, int limit) {
        limit = (limit == 0) ? Integer.MAX_VALUE : limit;
        SearchResult<TookanTeams> result = new SearchResult<>();
        if (StringUtils.isNotBlank(shopId)) {
            TookanTeams tookanTeams = tookanTeamRepository.getTeamByShop(token.getCompanyId(), shopId);
            if (tookanTeams != null) {
                result.setTotal(1L);
                result.setSkip(start);
                result.setLimit(limit);
                result.setValues(Lists.newArrayList(tookanTeams));
            }
        } else {
            result = tookanTeamRepository.findItems(token.getCompanyId(), start, limit);
        }
        return result;
    }

    /**
     * Override method to get tookan task between dates
     *
     * @param afterDate      : start date
     * @param beforeDate     : end date
     * @param tookanTaskType : tookan task type
     * @param status         : tookan task status
     * @return : return search result for all tookan tasks
     */
    @Override
    public DateSearchResult<TookanTaskResult> getTookanTasks(long afterDate, long beforeDate, TookanTaskResult.TookanTaskType tookanTaskType, TookanTaskResult.TookanTaskStatus status) {
        DateSearchResult<TookanTaskResult> result = new DateSearchResult<>();

        afterDate = (afterDate == 0) ? DateTime.now().withTimeAtStartOfDay().getMillis() : afterDate;
        beforeDate = (beforeDate == 0) ? DateTime.now().getMillis() : beforeDate;

        if (tookanTaskType == null) {
            throw new BlazeInvalidArgException(TOOKAN_TASK, "Task type cannot be blank.");
        }

        TookanAccount dbAccount = getTookanAccount(token.getShopId());
        String timeZone = token.getRequestTimeZone();
        if (StringUtils.isBlank(token.getRequestTimeZone())) {
            Shop shop = shopRepository.getById(token.getShopId());
            if (shop != null) {
                timeZone = shop.getTimeZone();
            }
        }
        String startDate = DateUtil.toDateTimeFormatted(afterDate, timeZone, "yyyy-MM-dd");
        String endDate = DateUtil.toDateTimeFormatted(beforeDate, timeZone, "yyyy-MM-dd");

        String requestBody;
        if (status == null) {
            TaskDetailRequest request = new TaskDetailRequest();
            request.setApiKey(dbAccount.getApiKey());
            request.setStartDate(startDate);
            request.setEndDate(endDate);
            if (tookanTaskType == TookanTaskResult.TookanTaskType.Delivery) {
                request.setJobType(1);
            } else {
                request.setJobType(0);
            }

            requestBody = JsonSerializer.toJson(request);
        } else {
            TaskDetailStatusRequest request = new TaskDetailStatusRequest();
            request.setApiKey(dbAccount.getApiKey());
            request.setStartDate(startDate);
            request.setEndDate(endDate);
            if (tookanTaskType == TookanTaskResult.TookanTaskType.Delivery) {
                request.setJobType(1);
            } else {
                request.setJobType(0);
            }
            request.setJobStatus(status.code);
            requestBody = JsonSerializer.toJson(request);
        }

        List<TookanTaskInfo> taskInfos = tookanApiService.getAllTookanTask(token.getCompanyId(), token.getShopId(), dbAccount.getApiKey(), requestBody);

        if (taskInfos == null || taskInfos.isEmpty()) {
            return result;
        }

        Set<ObjectId> transactionIds = new HashSet<>();

        for (TookanTaskInfo taskInfo : taskInfos) {
            if (StringUtils.isNotBlank(taskInfo.getOrderId()) && ObjectId.isValid(taskInfo.getOrderId())) {
                transactionIds.add(new ObjectId(taskInfo.getOrderId()));
            }
        }

        HashMap<String, Transaction> transactionHashMap = transactionRepository.listAsMap(token.getCompanyId(), Lists.newArrayList(transactionIds));
        HashMap<String, Member> memberHashMap = memberRepository.listAllAsMap(token.getCompanyId());
        HashMap<String, Employee> employeeHashMap = employeeRepository.listAllAsMap(token.getCompanyId());

        List<TookanTaskResult> tookanTaskResult = new ArrayList<>();

        for (TookanTaskInfo taskInfo : taskInfos) {
            Transaction transaction = transactionHashMap.get(taskInfo.getOrderId());
            if (transaction == null) {
                continue;
            }
            Member member = memberHashMap.get(transaction.getMemberId());
            Employee employee = employeeHashMap.get(transaction.getAssignedEmployeeId());

            if (member == null || employee == null) {
                continue;
            }

            TookanTaskResult taskResult = new TookanTaskResult();
            taskResult.setTransactionId(transaction.getId());
            StringBuilder memberName = new StringBuilder();
            memberName.append(member.getFirstName())
                    .append((StringUtils.isNotBlank(member.getMiddleName()) ? " " + member.getMiddleName() : " "))
                    .append((StringUtils.isNotBlank(member.getLastName()) ? " " + member.getLastName() : " "));

            StringBuilder employeeName = new StringBuilder();
            employeeName.append(employee.getFirstName())
                    .append((StringUtils.isNotBlank(employee.getLastName()) ? " " + employee.getLastName() : " "));

            taskResult.setMemberName(memberName.toString());
            taskResult.setEmployeeName(employeeName.toString());
            taskResult.setQueueType(transaction.getQueueType());
            taskResult.setTookanTaskId(taskInfo.getJobId().toString());
            taskResult.setStatus(TookanTaskResult.TookanTaskStatus.toTaskStatus(taskInfo.getJobStatus().intValue()));
            taskResult.setTransNo(transaction.getTransNo());
            tookanTaskResult.add(taskResult);
        }

        result.setTotal((long) tookanTaskResult.size());
        result.setValues(tookanTaskResult);
        result.setAfterDate(afterDate);
        result.setBeforeDate(beforeDate);
        return result;
    }


    /**
     * Override method to update task status
     *
     * @param companyId   : companyId
     * @param shopId      : shopId
     * @param transaction : transaction
     * @param status      : status
     */
    @Override
    public void updateTaskStatus(String companyId, String shopId, Transaction transaction, TookanTaskResult.TookanTaskStatus status) {
        TookanAccount dbAccount = tookanAccountRepository.getAccountByShop(token.getCompanyId(), token.getShopId());
        if (dbAccount == null || StringUtils.isBlank(dbAccount.getApiKey())) {
            throw new BlazeInvalidArgException(TOOKAN_ACCOUNT, ACCOUNT_NOT_FOUND);
        }
        Shop shop = shopRepository.getById(shopId);
        String timeZone = token.getRequestTimeZone();
        if (StringUtils.isBlank(token.getRequestTimeZone())) {
            timeZone = shop.getTimeZone();
        }
        tookanApiManager.updateTaskStatus(companyId, shopId, dbAccount.getApiKey(), timeZone, transaction, status);
    }

    /**
     * Private method to reset tookan details from shop,transaction,employee,team
     *
     * @param shopId : shop id
     */
    private void resetTookanDetails(String shopId) {
        TookanTeams tookanTeams = tookanTeamRepository.getTeamByShop(token.getCompanyId(), shopId);
        if (tookanTeams != null) {
            tookanTeamRepository.removeById(token.getCompanyId(), tookanTeams.getId());
        }

        transactionRepository.updateTookanTransaction(token.getCompanyId(), shopId);

        List<Employee> employeesByShop = employeeRepository.getTookanEmployeesByShop(token.getCompanyId(), token.getShopId());

        for (Employee employee : employeesByShop) {
            if (employee.getTookanInfoList() != null && !employee.getTookanInfoList().isEmpty()) {
                EmployeeTookanInfo employeeTookanInfo = tookanApiService.getEmployeeTookanInfo(token.getCompanyId(), employee, shopId);
                employee.getTookanInfoList().remove(employeeTookanInfo);
                employeeRepository.update(employee.getCompanyId(), employee.getId(), employee);
            }
        }


    }

    /**
     * Override method to get tookan info
     *
     * @param teamId : team id
     * @return tookan team info
     */
    @Override
    public TookanTeamInfo getTookanTeamInfo(String companyId, String shopId, String teamId) {
        TookanTeams tookanTeams = tookanTeamRepository.getTeamByShop(companyId, shopId);
        if (tookanTeams != null && tookanTeams.getTookanTeamInfo() != null) {
            return tookanApiService.getTookanTeamInfo(companyId, shopId, tookanTeams, teamId);
        }
        return null;
    }
}
