package com.fourtwenty.core.services.twilio;

import com.fourtwenty.core.rest.features.TwilioMessage;

public interface TwilioMessageService {
    String sendMessage(final TwilioMessage twilioMessage);

    String findAndCreatePhoneNumber(String friendlyName, String country, String state);
}
