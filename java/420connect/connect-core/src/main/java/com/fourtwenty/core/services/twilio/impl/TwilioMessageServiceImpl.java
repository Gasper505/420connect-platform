package com.fourtwenty.core.services.twilio.impl;

import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.exceptions.BlazeOperationException;
import com.fourtwenty.core.rest.features.TwilioMessage;
import com.fourtwenty.core.services.twilio.TwilioMessageService;
import com.twilio.base.ResourceSet;
import com.twilio.rest.api.v2010.account.IncomingPhoneNumber;
import com.twilio.rest.api.v2010.account.IncomingPhoneNumberCreator;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.rest.api.v2010.account.availablephonenumbercountry.Local;
import com.twilio.rest.api.v2010.account.availablephonenumbercountry.TollFree;
import com.twilio.type.PhoneNumber;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

public class TwilioMessageServiceImpl implements TwilioMessageService {
    private static final Logger logger = LoggerFactory.getLogger(TwilioMessageServiceImpl.class);

    @Inject
    private ConnectConfiguration connectConfiguration;

    /**
     * sendMessage used to send the conversation on twilio basis on twilio credentials
     * and these parameters
     *
     * @param twilioMessage
     * @return
     */
    @Override
    public String sendMessage(final TwilioMessage twilioMessage) {
        //send message on twilio
        try {
            final Message message = Message.creator(new PhoneNumber(twilioMessage.getTo()),
                    new PhoneNumber(twilioMessage.getFrom()), twilioMessage.getBody())
                    .setStatusCallback(connectConfiguration.getAppApiUrl() + "/api/v1/mgmt/conversation/messageStatus")
                    .create();
            String id = message.getSid();
            logger.info("SUCCESS: SMS sent to: " + twilioMessage.getTo() + " id: " + id);
            return id;
        } catch (Exception e) {
            logger.warn("Unable to sent message to twilio", e);
            logger.info("FAILED: SMS sent to: " + twilioMessage.getTo());
            throw new BlazeOperationException(e);
        }
    }

    private PhoneNumber getAvailableLocalNumber(final String country, final String state) {
        //check number on twilio and create it in twilio numbers
        ResourceSet<Local> numbers = Local.reader(country).setInRegion(state).setSmsEnabled(true).read();
        return numbers.iterator().next().getPhoneNumber();
    }


    private PhoneNumber getAvailableTollFreeNumber(final String country, final String state) {
        //check number on twilio and create it in twilio numbers
        ResourceSet<TollFree> numbers = TollFree.reader(country).setSmsEnabled(true).read();
        return numbers.iterator().next().getPhoneNumber();
    }

    private void createNumber(final String friendlyName, PhoneNumber availableNumber) {

        IncomingPhoneNumberCreator phoneNumberCreator = IncomingPhoneNumber.creator(availableNumber)
                .setSmsUrl(connectConfiguration.getAppApiUrl() + "/api/v1/mgmt/conversation/receive")
                .setSmsFallbackUrl(connectConfiguration.getAppApiUrl() + "/api/v1/mgmt/conversation/messageStatus");

        if (StringUtils.isNotBlank(friendlyName)) {
            phoneNumberCreator = phoneNumberCreator.setFriendlyName(friendlyName);
        }

        final IncomingPhoneNumber incomingPhoneNumber = phoneNumberCreator.create();
        logger.info("incoming alloted number " + incomingPhoneNumber.getPhoneNumber());
        logger.info("incoming alloted number sid " + incomingPhoneNumber.getSid());
        logger.info("incoming alloted number capabilities" +
                " mms : sms : voice " + incomingPhoneNumber.getCapabilities().getMms()
                + " : " + incomingPhoneNumber.getCapabilities().getSms()
                + " : " + incomingPhoneNumber.getCapabilities().getVoice());
    }

    @Override
    public String findAndCreatePhoneNumber(final String friendlyName, final String country, final String state) {
        //TODO Update verified twilio number as long as we are on trial account to avoid "Trial accounts are allowed only one Twilio number. To purchase additional numbers please upgrade your account."
        final PhoneNumber phoneNumber = getAvailableTollFreeNumber(country, state);
        logger.info("Available twilio number: " + phoneNumber.toString());
        createNumber(friendlyName, phoneNumber);
        return phoneNumber.toString();
//        return "+TO_BE_CHANGED";
    }

}
