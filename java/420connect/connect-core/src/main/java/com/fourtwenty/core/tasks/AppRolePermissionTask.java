package com.fourtwenty.core.tasks;

import com.fourtwenty.core.domain.models.company.*;
import com.fourtwenty.core.domain.repositories.dispensary.AppRolePermissionRepository;
import com.fourtwenty.core.domain.repositories.dispensary.CompanyRepository;
import com.fourtwenty.core.domain.repositories.dispensary.EmployeeRepository;
import com.fourtwenty.core.domain.repositories.dispensary.RoleRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.google.common.collect.ImmutableMultimap;
import io.dropwizard.servlets.tasks.Task;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.PrintWriter;
import java.util.*;
import java.util.stream.Collectors;


public class AppRolePermissionTask extends Task {

    private static final Logger LOGGER = LoggerFactory.getLogger(AppRolePermissionTask.class);

    @Inject
    private RoleRepository roleRepository;
    @Inject
    private AppRolePermissionRepository permissionRepository;
    @Inject
    private EmployeeRepository employeeRepository;
    @Inject
    private CompanyRepository companyRepository;

    public AppRolePermissionTask() {
        super("app-permission-task");
    }

    @Override
    public void execute(ImmutableMultimap<String, String> parameters, PrintWriter output) throws Exception {
        LOGGER.info("Starting migration for creating app permission for role");

        List<Role> roleList = roleRepository.list();
        //<companyId, roleList>
        Map<String, LinkedHashSet<String>> roleByCompanyMap = new HashMap<>();
        if (roleList != null) {
            for (Role role : roleList) {
                String companyId = role.getCompanyId();
                roleByCompanyMap.putIfAbsent(companyId, new LinkedHashSet<>());

                roleByCompanyMap.get(companyId).add(role.getId());
            }
        }

        Map<String, List<AppRolePermission>> rolePermissionMap = new HashMap<>();
        List<AppRolePermission> permissionList = permissionRepository.list();
        for (AppRolePermission rolePermission : permissionList) {
            rolePermissionMap.putIfAbsent(rolePermission.getCompanyId(), new ArrayList<>());

            rolePermissionMap.get(rolePermission.getCompanyId()).add(rolePermission);

        }

        List<AppRolePermission> appRolePermissions = new ArrayList<>();

        // This is for Retail
        for (Map.Entry<String, LinkedHashSet<String>> entry : roleByCompanyMap.entrySet()) {
            List<AppRolePermission> rolePermissions = rolePermissionMap.get(entry.getKey());
            if (rolePermissions == null) {
                AppRolePermission rolePermission = new AppRolePermission();
                rolePermission.prepare(entry.getKey());
                rolePermission.setAppTarget(CompanyFeatures.AppTarget.Retail);
                rolePermission.setRoleList(entry.getValue());

                appRolePermissions.add(rolePermission);
            }
        }

        // This is for Distribution
        for (Map.Entry<String, LinkedHashSet<String>> entry : roleByCompanyMap.entrySet()) {
            List<AppRolePermission> rolePermissions = rolePermissionMap.get(entry.getKey());
            if (rolePermissions == null) {
                AppRolePermission rolePermission = new AppRolePermission();
                rolePermission.prepare(entry.getKey());
                rolePermission.setAppTarget(CompanyFeatures.AppTarget.Distribution);
                rolePermission.setRoleList(entry.getValue());

                appRolePermissions.add(rolePermission);
            }
        }

        permissionRepository.save(appRolePermissions);

        LOGGER.info("Completed migration for creating app permission for role");


        LOGGER.info("Migrating Employees app access to both Retail & Distribution");
        LinkedHashSet<CompanyFeatures.AppTarget> appTargets = new LinkedHashSet<>();
        Collections.addAll(appTargets, CompanyFeatures.AppTarget.Retail, CompanyFeatures.AppTarget.Distribution);

        List<Company> companies = companyRepository.listNonDeleted();
        int limit = 1000;
        for (Company company : companies) {
            int offset = 0;
            long totalCount = 0;
            int totalUpdates = 0;
            do {
                SearchResult<Employee> result = employeeRepository.findItems(company.getId(), offset, limit, "{_id:1}");
                if (result.getTotal() != 0) {
                    List<Employee> employeeList = result.getValues();
                    List<ObjectId> objectIds = employeeList.stream().map(e -> new ObjectId(e.getId())).collect(Collectors.toList());
                    employeeRepository.bulkUpdateEmployeeAppAccess(company.getId(), objectIds, appTargets);
                    totalUpdates += objectIds.size();
                }
                offset += limit;
                totalCount = result.getTotal();
            } while (offset < totalCount);

            LOGGER.info("Updated {} employees app access for {}", totalUpdates, company.getName());
        }

        LOGGER.info("Completed migration for employees app access");

    }
}
