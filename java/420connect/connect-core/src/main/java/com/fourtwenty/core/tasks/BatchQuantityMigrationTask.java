package com.fourtwenty.core.tasks;

import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.repositories.dispensary.BatchQuantityRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductBatchRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductCategoryRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.google.common.collect.ImmutableMultimap;
import com.google.inject.Inject;
import io.dropwizard.servlets.tasks.Task;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

/**
 * Created by mdo on 11/1/17.
 */
public class BatchQuantityMigrationTask extends Task {
    private static final Log LOG = LogFactory.getLog(BatchQuantityMigrationTask.class);
    @Inject
    ProductRepository productRepository;
    @Inject
    ProductBatchRepository productBatchRepository;
    @Inject
    ProductCategoryRepository categoryRepository;
    @Inject
    BatchQuantityRepository batchQuantityRepository;

    public BatchQuantityMigrationTask() {
        super("batch-quantity-migration");
    }

    @Override
    public void execute(ImmutableMultimap<String, String> parameters, PrintWriter output) throws Exception {
        long count = batchQuantityRepository.count();

        if (count > 0) {
            LOG.info("BatchQuantity was previously migrated.");
            return;
        }
        Iterable<Product> products = productRepository.listNonDeleted();
        Iterable<ProductBatch> productBatches = productBatchRepository.listNonDeleted();
        //Iterable<Inventory> inventories = inventoryRepository.listNonDeleted();
        Iterable<BatchQuantity> batchQuantities = batchQuantityRepository.list();
        HashMap<String, ProductCategory> categoryHashMap = categoryRepository.listAsMap();

        // build map for productBatches
        HashMap<String, List<ProductBatch>> productToBatches = new HashMap<>();
        for (ProductBatch productBatch : productBatches) {
            List<ProductBatch> items = productToBatches.get(productBatch.getProductId());
            if (items == null) {
                items = new ArrayList<>();
            }
            items.add(productBatch);
            productToBatches.put(productBatch.getProductId(), items);
        }

        //build map for batchquantities
        // key: productId+inventoryId+batchId
        HashMap<String, BatchQuantity> keyToBatchQuantities = new HashMap<>();
        for (BatchQuantity batchQuantity : batchQuantities) {
            String key = String.format("%s-%s-%s", batchQuantity.getProductId(), batchQuantity.getInventoryId(), batchQuantity.getBatchId());
            if (!keyToBatchQuantities.containsKey(key)) {
                keyToBatchQuantities.put(key, batchQuantity);
            }
        }

        List<BatchQuantity> quantitiesToAdd = new ArrayList<>();
        // lambda comparator
        Comparator<ProductBatch> batchComparator = (o1, o2) -> ((Long) o1.getCreated()).compareTo((Long) o2.getCreated()) * 1;

        for (Product product : products) {
            // get latest batch for product
            ProductCategory category = categoryHashMap.get(product.getCategoryId());

            List<ProductBatch> productBatchList = productToBatches.get(product.getId());
            if (productBatchList == null || productBatchList.size() == 0) {
                continue;
            }
            productBatchList.sort(batchComparator);

            ProductBatch latestBatch = productBatchList.get(0);
            for (ProductQuantity productQuantity : product.getQuantities()) {
                String key = String.format("%s-%s-%s", product.getId(), productQuantity.getInventoryId(), latestBatch.getId());
                BatchQuantity batchQuantity = keyToBatchQuantities.get(key);
                if (batchQuantity == null) {
                    batchQuantity = new BatchQuantity();
                    batchQuantity.prepare(product.getCompanyId());
                    batchQuantity.setShopId(product.getShopId());
                    batchQuantity.setQuantity(productQuantity.getQuantity());
                    batchQuantity.setUnitType(category.getUnitType());

                    batchQuantity.setProductId(product.getId());
                    batchQuantity.setInventoryId(productQuantity.getInventoryId());
                    batchQuantity.setBatchId(latestBatch.getId());
                    batchQuantity.setBatchPurchaseDate(latestBatch.getPurchasedDate());
                    keyToBatchQuantities.put(key, batchQuantity);
                    quantitiesToAdd.add(batchQuantity);
                }
            }
        }

        // save to db
        batchQuantityRepository.save(quantitiesToAdd);
        LOG.info("Batch Quantities added: " + quantitiesToAdd.size());
    }
}
