package com.fourtwenty.core.tasks;

import com.fourtwenty.core.domain.models.product.CompanyLicense;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.repositories.dispensary.VendorRepository;
import com.google.common.collect.ImmutableMultimap;
import io.dropwizard.servlets.tasks.Task;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class CompanyLicenseMigration extends Task {

    private static final Logger LOGGER = LoggerFactory.getLogger(CompanyLicenseMigration.class);

    private static final int FETCH_LIMIT = 1000;

    @Inject
    private VendorRepository vendorRepository;

    public CompanyLicenseMigration() {
        super("migrate-company-license");
    }

    @Override
    public void execute(ImmutableMultimap<String, String> parameters, PrintWriter output) throws Exception {
        LOGGER.info("Starting company license migration");

        long count = vendorRepository.count();
        int start = 0;
        while (count > start) {
            Iterable<Vendor> vendors = vendorRepository.findItems(start, FETCH_LIMIT, "{companyType:1, licenseNumber:1, licenceType:1, licenseExpirationDate:1, companyLicenses:1}");

            List<CompanyLicense> companyLicenses = new ArrayList<>();
            for (Vendor vendor : vendors) {
                if (!CollectionUtils.isEmpty(vendor.getCompanyLicenses())) {
                    continue;
                }

                Vendor.CompanyType companyType = vendor.getCompanyType() == null ? Vendor.CompanyType.RETAILER : vendor.getCompanyType();

                try {
                    companyLicenses = new ArrayList<>();
                    CompanyLicense companyLicense = new CompanyLicense();
                    companyLicense.prepare();
                    companyLicense.setCompanyType(companyType);
                    companyLicense.setLicenseType(vendor.getLicenceType());
                    companyLicense.setLicenseNumber(vendor.getLicenseNumber());
                    companyLicense.setLicenseExpirationDate(vendor.getLicenseExpirationDate());
                    companyLicense.setToDefault(true);
                    companyLicenses.add(companyLicense);

                    vendorRepository.updateCompanyLicense(vendor.getId(), companyLicenses);

                } catch (Exception e) {
                    LOGGER.error(String.format("Error in migration for vendor: %s, company type: %s, ", vendor.getId(), companyType), e);
                }

            }

            start += FETCH_LIMIT;
        }
        LOGGER.info("Company license migration completed");


    }
}
