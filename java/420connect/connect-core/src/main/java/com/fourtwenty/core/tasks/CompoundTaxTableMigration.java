package com.fourtwenty.core.tasks;

import com.fourtwenty.core.domain.models.company.CompoundTaxTable;
import com.fourtwenty.core.domain.models.company.ConsumerType;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.repositories.dispensary.MemberRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.domain.repositories.store.ConsumerUserRepository;
import com.google.common.collect.ImmutableMultimap;
import com.google.inject.Inject;
import com.mongodb.WriteResult;
import io.dropwizard.servlets.tasks.Task;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;
import org.jongo.MongoCollection;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mdo on 1/9/18.
 */
public class CompoundTaxTableMigration extends Task {
    private static final Log LOG = LogFactory.getLog(CompoundTaxTableMigration.class);
    @Inject
    ShopRepository shopRepository;
    @Inject
    MemberRepository memberRepository;
    @Inject
    ConsumerUserRepository consumerUserRepository;

    public CompoundTaxTableMigration() {
        super("migrate-compound-taxes");
    }

    @Override
    public void execute(ImmutableMultimap<String, String> parameters, PrintWriter output) throws Exception {
        Iterable<Shop> shops = shopRepository.list();
        int shopsUpdated = 0;
        for (Shop shop : shops) {
            if (shop.getTaxTables() == null || shop.getTaxTables().size() == 0) {
                List<CompoundTaxTable> taxTables = new ArrayList<>();
                shop.setTaxTables(taxTables);

                ConsumerType[] consumerTypes = ConsumerType.values();
                for (ConsumerType consumerType : consumerTypes) {
                    if (consumerType == ConsumerType.Other) {
                        continue;
                    }
                    CompoundTaxTable taxTable = new CompoundTaxTable();
                    taxTable.prepare(shop.getCompanyId());
                    taxTable.setShopId(shop.getId());
                    taxTable.setName(consumerType.getDisplayName());
                    taxTable.setConsumerType(consumerType);
                    taxTables.add(taxTable);
                    // set the ids
                    taxTable.getCityTax().prepare(shop.getCompanyId());
                    taxTable.getCityTax().setShopId(shop.getId());
                    taxTable.getCountyTax().prepare(shop.getCompanyId());
                    taxTable.getCountyTax().setShopId(shop.getId());
                    taxTable.getStateTax().prepare(shop.getCompanyId());
                    taxTable.getStateTax().setShopId(shop.getId());
                    taxTable.getFederalTax().prepare(shop.getCompanyId());
                    taxTable.getFederalTax().setShopId(shop.getId());
                }
                shopRepository.update(shop.getCompanyId(), shop.getId(), shop);
                shopsUpdated++;
            }
        }
        LOG.info("Migrated shops: " + shopsUpdated);
        MongoCollection memberColl = memberRepository.getMongoCollection();
        if (memberColl != null) {
            // update medicinal == true
            WriteResult result1 = memberColl.update("{medical:true,consumerType:{$exists:false}}").multi().with("{$set: {consumerType: #, modified:#}}", ConsumerType.MedicinalThirdParty, DateTime.now().getMillis());
            WriteResult result2 = memberColl.update("{medical:false,consumerType:{$exists:false}}").multi().with("{$set: {consumerType: #, modified:#}}", ConsumerType.AdultUse, DateTime.now().getMillis());


            LOG.info("Members updated with medicinal: " + result1.getN());
            LOG.info("Members updated with recreational: " + result2.getN());

        }

        MongoCollection consumerColl = consumerUserRepository.getMongoCollection();
        if (memberColl != null) {
            // update medicinal == true
            WriteResult result1 = consumerColl.update("{medical:true,consumerType:{$exists:false}}").multi().with("{$set: {consumerType: #, modified:#}}", ConsumerType.MedicinalThirdParty, DateTime.now().getMillis());
            WriteResult result2 = consumerColl.update("{medical:false,consumerType:{$exists:false}}").multi().with("{$set: {consumerType: #, modified:#}}", ConsumerType.AdultUse, DateTime.now().getMillis());

            LOG.info("ConsumerUsers updated with medicinal: " + result1.getN());
            LOG.info("ConsumerUsers updated with recreational: " + result2.getN());

        }

    }
}
