package com.fourtwenty.core.tasks;

import com.esotericsoftware.kryo.Kryo;
import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.domain.models.consumer.ConsumerMemberStatus;
import com.fourtwenty.core.domain.models.consumer.ConsumerUser;
import com.fourtwenty.core.domain.repositories.dispensary.CompanyRepository;
import com.fourtwenty.core.domain.repositories.store.ConsumerUserRepository;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Lists;
import io.dropwizard.servlets.tasks.Task;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class ConsumerUserMigration extends Task {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConsumerUserMigration.class);

    private static final int FETCH_LIMIT = 10000;

    @Inject
    private ConsumerUserRepository consumerUserRepository;
    @Inject
    private CompanyRepository companyRepository;

    public ConsumerUserMigration() {
        super("migrate-consumer-user");
    }

    @Override
    public void execute(ImmutableMultimap<String, String> parameters, PrintWriter output) throws Exception {

        LOGGER.info("Starting consumer user migration ");

        HashMap<String, Company> companyMap = companyRepository.listAsMap();

        int start = 0;
        Long consumerCount = consumerUserRepository.count();

        List<ConsumerUser> newConsumerUserList = new ArrayList<>();

        Kryo kryo = new Kryo();
        Set<String> visitedCompanyIds;
        do {

            Iterable<ConsumerUser> consumerUsers = consumerUserRepository.list(start, FETCH_LIMIT);

            ArrayList<ConsumerUser> users = Lists.newArrayList(consumerUsers);

            Iterator<ConsumerUser> userIterator = users.iterator();

            while (userIterator.hasNext()) {
                ConsumerUser consumerUser = userIterator.next();

                List<ConsumerMemberStatus> statusList = consumerUser.getMemberStatuses();

                if (statusList != null && statusList.size() > 0) {
                    visitedCompanyIds = new HashSet<>();

                    for (int i = 0; i < statusList.size(); i++) {
                        ConsumerMemberStatus memberStatus = statusList.get(i);
                        Company company = companyMap.get(memberStatus.getCompanyId());
                        if (company == null) {
                            continue;
                        }
                        if (i == 0) {
                            visitedCompanyIds.add(memberStatus.getCompanyId());
                            consumerUser.prepare(memberStatus.getCompanyId());
                            consumerUser.setMemberId(memberStatus.getMemberId());

                            this.checkMemberAcceptance(statusList, memberStatus.getCompanyId(), consumerUser);

                            consumerUserRepository.update(consumerUser.getId(), consumerUser);
                        } else if (!visitedCompanyIds.contains(memberStatus.getCompanyId())) {
                            ConsumerUser user = kryo.copy(consumerUser);

                            this.checkMemberAcceptance(statusList, memberStatus.getCompanyId(), user);

                            user.prepare(memberStatus.getCompanyId());
                            user.setId(ObjectId.get().toString());
                            user.setMemberId(memberStatus.getMemberId());
                            newConsumerUserList.add(user);
                            visitedCompanyIds.add(memberStatus.getCompanyId());
                        }
                    }
                } else {
                    consumerUser.prepare(consumerUser.getSourceCompanyId());
                    consumerUserRepository.update(consumerUser.getId(), consumerUser);
                }
                //TODO : What if consumer user doesn't have either 'sourceCompanyId'or 'memberStatuses'
            }

            consumerCount = consumerCount - (long) users.size();
            start = start + users.size();

        } while (consumerCount > 0);

        List<List<ConsumerUser>> usersPartitions = Lists.partition(newConsumerUserList, 1000);
        for (List<ConsumerUser> users : usersPartitions) {
            consumerUserRepository.save(users);
        }

        LOGGER.info("Completed consumer user migration ");

    }

    /**
     * Purpose of this method is to selection of member status {@link ConsumerMemberStatus}
     * @param statusList
     * @param companyId
     * @param consumerUser
     */
    private ConsumerMemberStatus checkMemberAcceptance(List<ConsumerMemberStatus> statusList, String companyId, ConsumerUser consumerUser) {

        ConsumerMemberStatus memberStatus = null;
        for (int i = 0; i < statusList.size(); i++) {
            ConsumerMemberStatus status = statusList.get(i);
            if (companyId.equals(status.getCompanyId())) {
                if (status.isAccepted()) {
                    memberStatus = status; break;
                }

                if (i == statusList.size() - 1) {
                    memberStatus = status;
                }
            }
        }

        if (memberStatus != null) {
            consumerUser.setAccepted(memberStatus.isAccepted());
            consumerUser.setAcceptedDate(memberStatus.getAcceptedDate());
            consumerUser.setReason(memberStatus.getReason());
            consumerUser.setLastSyncDate(memberStatus.getLastSyncDate());
        }

        return memberStatus;
    }
}
