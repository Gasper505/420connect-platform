package com.fourtwenty.core.tasks;

import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.models.company.MemberGroup;
import com.fourtwenty.core.domain.models.company.SignedContract;
import com.fourtwenty.core.domain.models.customer.*;
import com.fourtwenty.core.domain.models.generic.Note;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.ImmutableMultimap;
import com.google.inject.Inject;
import io.dropwizard.servlets.tasks.Task;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bson.types.ObjectId;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by mdo on 1/25/17.
 */
public class CopyDataTask extends Task {
    private static final Log LOG = LogFactory.getLog(CopyDataTask.class);

    enum CopyDataType {
        NONE,
        PRODUCTS,
        MEMBERS,
        PHYSICIANS,
        VENDORS
    }

    @Inject
    MemberRepository memberRepository;
    @Inject
    DoctorRepository doctorRepository;
    @Inject
    MemberGroupRepository memberGroupRepository;
    @Inject
    CompanyAssetRepository companyAssetRepository;
    @Inject
    VendorRepository vendorRepository;

    public CopyDataTask() {
        super("copy-data");
    }

    @Override
    public void execute(ImmutableMultimap<String, String> parameters, PrintWriter output) throws Exception {

        ImmutableCollection<String> fromCompanies = parameters.get("fromCompanyId");
        ImmutableCollection<String> fromShops = parameters.get("fromShopId");

        ImmutableCollection<String> toCompanies = parameters.get("toCompanyId");
        ImmutableCollection<String> toShops = parameters.get("toShopId");

        ImmutableCollection<String> types = parameters.get("type");
        String fromCompanyId = getFirstFromArray(fromCompanies);
        String fromShopId = getFirstFromArray(fromShops);

        String toCompanyId = getFirstFromArray(toCompanies);
        String toShopId = getFirstFromArray(toShops);


        CopyDataType type = CopyDataType.NONE;


        if (types != null && types.size() > 0) {
            String t = (String) types.toArray()[0];
            try {
                type = Enum.valueOf(CopyDataType.class, t);
            } catch (Exception e) {
                throw new BlazeInvalidArgException("CopyTask", "Invalid task.");
            }
        }
        if (fromCompanyId != null && fromCompanyId != null && toCompanyId != null && toShopId != null) {
            copyData(fromCompanyId, fromShopId, toCompanyId, toShopId, type);
        }
    }

    private String getFirstFromArray(ImmutableCollection<String> array) {
        if (array != null && array.size() > 0) {
            return (String) array.toArray()[0];
        }
        return null;
    }

    private void copyData(final String fromCompanyId,
                          final String fromShopId,
                          final String toCompanyId,
                          final String toShopId,
                          final CopyDataType type) {

        LOG.info(String.format("Copy Data '%s' Request from company: %s shop:%s to company:%s and toShop:%s",
                type.name(),
                fromCompanyId,
                fromShopId,
                toCompanyId,
                toShopId));

        if (type == CopyDataType.MEMBERS) {
            copyMembers(fromCompanyId, fromShopId, toCompanyId, toShopId);
        } else if (type == CopyDataType.PHYSICIANS) {
            copyPhysicians(fromCompanyId, fromShopId, toCompanyId, toShopId);
        } else if (type == CopyDataType.VENDORS) {
            copyVendors(fromCompanyId, fromShopId, toCompanyId, toShopId);
        }
    }

    private void copyMembers(final String fromCompanyId,
                             final String fromShopId,
                             final String toCompanyId,
                             final String toShopId) {
        Iterable<Member> fromMembers = memberRepository.list(fromCompanyId);

        Iterable<Doctor> oldDoctors = doctorRepository.listAll(fromCompanyId);
        HashMap<String, Doctor> oldDoctorMap = new HashMap<>();
        for (Doctor doc : oldDoctors) {
            oldDoctorMap.put(doc.getLicense(), doc);
        }


        Iterable<Doctor> curDoctors = doctorRepository.listAll(toCompanyId);
        HashMap<String, Doctor> doctorHashMap = new HashMap<>();
        for (Doctor doc : curDoctors) {
            doctorHashMap.put(doc.getLicense(), doc);
        }

        MemberGroup defaultGroup = memberGroupRepository.getDefaultMemberGroup(toCompanyId);


        List<Member> membersToAdd = new ArrayList<>();
        List<Doctor> doctorsToAdd = new ArrayList<>();

        for (Member member : fromMembers) {
            member.setId(ObjectId.get().toString());
            member.setCompanyId(toCompanyId);

            // Set Default Group
            member.setMemberGroupId(defaultGroup.getId());
            member.setMemberGroup(defaultGroup);

            // Update address
            if (member.getAddress() != null) {
                member.setId(ObjectId.get().toString());
                member.setCompanyId(toCompanyId);
            }

            // Change Notes
            member.setNotes(new ArrayList<Note>());

            // Contracts
            // clear contracts
            member.setContracts(new ArrayList<SignedContract>());


            // Preferences
            if (member.getPreferences() != null) {
                for (Preference pref : member.getPreferences()) {
                    pref.setCompanyId(toCompanyId);
                    pref.setId(ObjectId.get().toString());
                }
            }

            // Identifications
            if (member.getIdentifications() != null) {
                for (Identification identification : member.getIdentifications()) {
                    identification.setId(ObjectId.get().toString());
                    identification.setCompanyId(toCompanyId);

                    identification.setFrontPhoto(null);
                    identification.setAssets(new ArrayList<CompanyAsset>());
                }
            }


            // Recommendations
            if (member.getRecommendations() != null) {
                for (Recommendation rec : member.getRecommendations()) {
                    rec.setId(ObjectId.get().toString());
                    rec.setCompanyId(toCompanyId);

                    rec.setFrontPhoto(null);

                    Doctor doc = oldDoctorMap.get(rec.getDoctorId());
                    if (doc != null) {

                        Doctor curDoc = doctorHashMap.get(doc.getLicense());
                        if (curDoc == null) {
                            doc.setId(ObjectId.get().toString());
                            doc.setCompanyId(toCompanyId);
                            rec.setDoctorId(doc.getId());

                            // Assets
                            doc.setAssets(new ArrayList<CompanyAsset>());
                            // new doctor, let's add to add list
                            doctorsToAdd.add(doc);
                            doctorHashMap.put(doc.getLicense(), doc);
                        } else {
                            // set rec to this current doctor
                            rec.setDoctorId(curDoc.getId());
                            rec.setDoctor(curDoc);
                        }


                    }
                }
            }

            // Reset recent products
            member.setRecentProducts(new ArrayList<String>());

            membersToAdd.add(member);
        }

        doctorRepository.save(doctorsToAdd);
        memberRepository.save(membersToAdd);


        LOG.info("Doctors copied: " + doctorsToAdd.size());
        LOG.info("Members copied: " + membersToAdd.size());
    }

    private void copyPhysicians(final String fromCompanyId,
                                final String fromShopId,
                                final String toCompanyId,
                                final String toShopId) {

        Iterable<Doctor> fromDoctors = doctorRepository.list(fromCompanyId);

        Iterable<Doctor> curDoctors = doctorRepository.listAll(toCompanyId);
        HashMap<String, Doctor> doctorHashMap = new HashMap<>();
        for (Doctor doc : curDoctors) {
            doctorHashMap.put(doc.getLicense(), doc);
        }


        List<Doctor> toDoctors = new ArrayList<>();
        for (Doctor doc : fromDoctors) {
            if (doctorHashMap.containsKey(doc.getLicense())) {
                // this doctor already exist in the new company
                continue;
            }

            toDoctors.add(doc);
            doc.setCompanyId(toCompanyId);
            doc.setId(ObjectId.get().toString());

            // doc address
            if (doc.getAddress() != null) {
                doc.getAddress().setId(ObjectId.get().toString());
                doc.getAddress().setCompanyId(toCompanyId);
            }

            // Assets
            doc.setAssets(new ArrayList<CompanyAsset>());

            // Change Notes
            doc.setNotes(new ArrayList<Note>());
        }
        //companyAssetRepository.save(companyAssets);
        doctorRepository.save(toDoctors);


        LOG.info("Doctors copied: " + toDoctors.size());
    }

    private void copyVendors(final String fromCompanyId,
                             final String fromShopId,
                             final String toCompanyId,
                             final String toShopId) {
        Iterable<Vendor> fromVendors = vendorRepository.list(fromCompanyId);

        List<Vendor> vendorsToAdd = new ArrayList<>();

        for (Vendor vendor : fromVendors) {
            vendor.setId(ObjectId.get().toString());
            vendor.setCompanyId(toCompanyId);

            vendor.setNotes(new ArrayList<Note>());

            vendor.setAssets(new ArrayList<CompanyAsset>());


            vendorsToAdd.add(vendor);
        }

        vendorRepository.save(vendorsToAdd);


        LOG.info("Vendors copied: " + vendorsToAdd.size());
    }
}
