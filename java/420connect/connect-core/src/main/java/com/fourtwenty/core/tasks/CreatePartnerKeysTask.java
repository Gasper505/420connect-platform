package com.fourtwenty.core.tasks;

import com.fourtwenty.core.domain.models.developer.PartnerKey;
import com.fourtwenty.core.domain.repositories.developer.PartnerKeyRepository;
import com.fourtwenty.core.util.SecurityUtil;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import io.dropwizard.servlets.tasks.Task;

import java.io.PrintWriter;
import java.util.List;

/**
 * Created by mdo on 2/9/18.
 */
public class CreatePartnerKeysTask extends Task {
    @Inject
    PartnerKeyRepository partnerKeyRepository;
    @Inject
    SecurityUtil securityUtil;


    public CreatePartnerKeysTask() {
        super("partnerkeys-create");
    }

    @Override
    public void execute(ImmutableMultimap<String, String> parameters, PrintWriter output) throws Exception {

        long partnersCount = partnerKeyRepository.count();
        if (partnersCount == 0) {
            PartnerKey greenrush = createPartnerKey("Greenrush");
            PartnerKey getnugg = createPartnerKey("getnugg");
            PartnerKey tokr = createPartnerKey("tokr");
            PartnerKey blaze = createPartnerKey("blaze");

            List<PartnerKey> partnerKeyList = Lists.newArrayList(greenrush, getnugg, tokr, blaze);
            partnerKeyRepository.save(partnerKeyList);
        }
    }

    private PartnerKey createPartnerKey(String partnerName) {
        PartnerKey partnerKey = new PartnerKey();
        partnerKey.prepare();
        partnerKey.setName(partnerName);
        partnerKey.setKey(securityUtil.generateKey());
        partnerKey.setActive(true);

        return partnerKey;
    }
}
