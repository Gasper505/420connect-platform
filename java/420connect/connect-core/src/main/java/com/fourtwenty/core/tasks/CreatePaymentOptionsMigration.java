package com.fourtwenty.core.tasks;

import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.payment.ShopPaymentOption;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.domain.repositories.payment.ShopPaymentOptionRepository;
import com.google.common.collect.ImmutableMultimap;
import io.dropwizard.servlets.tasks.Task;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.inject.Inject;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class CreatePaymentOptionsMigration extends Task {

    private static final Log LOG = LogFactory.getLog(CreatePaymentOptionsMigration.class);

    @Inject
    private ShopRepository shopRepository;

    @Inject
    private ShopPaymentOptionRepository shopPaymentOptionRepository;


    protected CreatePaymentOptionsMigration() {
        super("payment-option-migration");
    }

    @Override
    public void execute(ImmutableMultimap<String, String> parameters, PrintWriter output) throws Exception {

        LOG.info("Starting payment option migration.");

        setupDefaultPaymentOptionsForAllShops();

        LOG.info("Ending payment option migration.");

    }

    private void setupDefaultPaymentOptionsForAllShops() {


        List<ShopPaymentOption> existingPaymentOptions = shopPaymentOptionRepository.list();

        List<Cart.PaymentOption> defaultPaymentOptions = new ArrayList<>();
        defaultPaymentOptions.add(Cart.PaymentOption.Cash);
        defaultPaymentOptions.add(Cart.PaymentOption.Credit);
        defaultPaymentOptions.add(Cart.PaymentOption.Check);
        defaultPaymentOptions.add(Cart.PaymentOption.Split);
        defaultPaymentOptions.add(Cart.PaymentOption.StoreCredit);
        defaultPaymentOptions.add(Cart.PaymentOption.Linx);


        List<ShopPaymentOption> paymentOptions = new ArrayList<>();

        List<Shop> shops = shopRepository.list();
        for (Shop shop : shops) {
            boolean hasExistingPaymentOptions = existingPaymentOptions
                    .stream()
                    .anyMatch(po -> po.getShopId().equalsIgnoreCase(shop.getId()));

            // Skip shops that already have payment options configured
            if (hasExistingPaymentOptions) continue;

            paymentOptions.addAll(
                    defaultPaymentOptions
                            .stream()
                            .map(po -> {
                                ShopPaymentOption spo = new ShopPaymentOption();
                                spo.prepare(shop.getCompanyId());
                                spo.setShopId(shop.getId());
                                spo.setPaymentOption(po);

                                if (po == Cart.PaymentOption.Linx) {
                                    spo.setEnabled(false);
                                } else {
                                    spo.setEnabled(true);
                                }

                                return spo;
                            })
                            .collect(Collectors.toList()));
        }

        shopPaymentOptionRepository.save(paymentOptions);
    }
}
