package com.fourtwenty.core.tasks;

import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.domain.models.company.Role;
import com.fourtwenty.core.domain.repositories.dispensary.CompanyRepository;
import com.fourtwenty.core.domain.repositories.dispensary.RoleRepository;
import com.fourtwenty.core.lifecycle.model.DefaultData;
import com.google.common.collect.ImmutableMultimap;
import io.dropwizard.servlets.tasks.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Raja Dushyant Vashishtha
 */
public class DispatcherRoleMigration extends Task {

    private static final String DISPATCHER = "Dispatcher";

    private static final Logger LOGGER = LoggerFactory.getLogger(DispatcherRoleMigration.class);

    @Inject
    private RoleRepository roleRepository;
    @Inject
    private DefaultData defaultData;
    @Inject
    private CompanyRepository companyRepository;

    protected DispatcherRoleMigration() {
        super("dispatcher-role-migration");
    }

    @Override
    public void execute(ImmutableMultimap<String, String> parameters, PrintWriter output) throws Exception {
        LOGGER.info("Started dispatcher role migration");

        List<Role> roles = defaultData.getRoles();
        Map<String, Role> roleMap = new HashMap<>();
        for (Role role : roles) {
            roleMap.put(role.getName(), role);
        }

        boolean hasDispatcher = false;
        List<Role> dbRoleList = roleRepository.list();
        for (Role dbRole : dbRoleList) {
            Role role = roleMap.get(dbRole.getName());
            dbRole.setPermissions(role.getPermissions());
            roleRepository.update(dbRole.getId(), role);
            if (dbRole.getName().equalsIgnoreCase(DISPATCHER)) {
                hasDispatcher = true;
            }
        }

        if (!hasDispatcher) {
            Role dispatcherRole = roleMap.get(DISPATCHER);
            Iterable<Company> companyList = companyRepository.list();
            for (Company company : companyList) {
                dispatcherRole.setId(null);
                dispatcherRole.setCompanyId(company.getId());
                roleRepository.save(dispatcherRole);
            }
        }

        LOGGER.info("Completed Dispatcher role migration");
    }
}
