package com.fourtwenty.core.tasks;

import com.fourtwenty.core.domain.models.company.Role;
import com.fourtwenty.core.domain.repositories.dispensary.EmployeeRepository;
import com.fourtwenty.core.domain.repositories.dispensary.RoleRepository;
import com.google.common.collect.ImmutableMultimap;
import com.google.inject.Inject;
import io.dropwizard.servlets.tasks.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class DriverRoleMigration extends Task {

    private static final String ROLE_DELIVERY_DRIVER = "Delivery Driver";
    private static final Logger LOGGER = LoggerFactory.getLogger(DriverRoleMigration.class);

    @Inject
    private RoleRepository roleRepository;
    @Inject
    private EmployeeRepository employeeRepository;

    public DriverRoleMigration() {
        super("migrate-employee-driver");
    }

    /**
     * Executes the task for updateing employee.driver to true if role in delivery Driver.
     *
     * @param parameters the query string parameters
     * @param output     a {@link PrintWriter} wrapping the output stream of the task
     * @throws Exception if something goes wrong
     */
    @Override
    public void execute(ImmutableMultimap<String, String> parameters, PrintWriter output) throws Exception {
        LOGGER.info("Starting driver role migration");
        Iterable<Role> roles =  roleRepository.getRoleByName(ROLE_DELIVERY_DRIVER);

        List<String> roleIds = new ArrayList<>();
        for (Role role : roles) {
            roleIds.add(role.getId());
        }

        employeeRepository.updateDriverInfo(roleIds, true);

        LOGGER.info("Completing driver role migration");
    }
}
