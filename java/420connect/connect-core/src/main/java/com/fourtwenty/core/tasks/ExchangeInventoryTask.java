package com.fourtwenty.core.tasks;

import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.product.Inventory;
import com.fourtwenty.core.domain.repositories.dispensary.InventoryRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.google.common.collect.ImmutableMultimap;
import io.dropwizard.servlets.tasks.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ExchangeInventoryTask extends Task {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExchangeInventoryTask.class);

    @Inject
    private ShopRepository shopRepository;

    @Inject
    private InventoryRepository inventoryRepository;

    public ExchangeInventoryTask() {
        super("exchange-inventory-task");
    }

    @Override
    public void execute(ImmutableMultimap<String, String> parameters, PrintWriter output) throws Exception {
        LOGGER.info("Migrating exchange inventory shop");
        final HashMap<String, Shop> shopHashMap = shopRepository.listAsMap();
        final List<Inventory> inventories = new ArrayList<>();
        for (Map.Entry<String, Shop> shopEntry : shopHashMap.entrySet()) {
            Inventory inventory = new Inventory();
            inventory.prepare(shopEntry.getValue().getCompanyId());
            inventory.setShopId(shopEntry.getKey());
            inventory.setName(Inventory.EXCHANGE);
            inventory.setType(Inventory.InventoryType.Storage);
            inventory.setActive(true);
            inventories.add(inventory);
            LOGGER.info("Processing for shop ID:{}, CompanyID:{}, Shop NAME:{}", shopEntry.getKey(), shopEntry.getValue().getCompanyId(), shopEntry.getValue().getName());
        }
        inventoryRepository.save(inventories);
        LOGGER.info("Migrated exchange inventory shop");
    }
}
