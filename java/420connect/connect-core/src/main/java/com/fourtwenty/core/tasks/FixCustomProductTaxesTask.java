package com.fourtwenty.core.tasks;

import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.google.common.collect.ImmutableMultimap;
import com.google.inject.Inject;
import io.dropwizard.servlets.tasks.Task;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.PrintWriter;

/**
 * Created by mdo on 10/13/17.
 */
public class FixCustomProductTaxesTask extends Task {
    private static final Log LOG = LogFactory.getLog(FixCustomProductTaxesTask.class);
    @Inject
    ProductRepository productRepository;

    public FixCustomProductTaxesTask() {
        super("fix-custom-taxes");
    }

    @Override
    public void execute(ImmutableMultimap<String, String> parameters, PrintWriter output) throws Exception {

        Iterable<Product> products = productRepository.list();
        int updated = 0;
        for (Product product : products) {
            if (product.getCustomTaxInfo() != null && product.getCustomTaxInfo().getId() == null) {
                product.getCustomTaxInfo().prepare();

                updated++;
                productRepository.updateProductTaxInfo(product.getCompanyId(), product.getId(), product.getCustomTaxInfo());
            }

        }
        LOG.info("Updating custom taxes: " + updated);

    }
}
