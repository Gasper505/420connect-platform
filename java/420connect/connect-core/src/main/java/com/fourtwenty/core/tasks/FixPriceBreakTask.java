package com.fourtwenty.core.tasks;

import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductPriceBreak;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.google.common.collect.ImmutableMultimap;
import com.google.inject.Inject;
import io.dropwizard.servlets.tasks.Task;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bson.types.ObjectId;

import java.io.PrintWriter;

/**
 * Created by mdo on 6/10/18.
 */
public class FixPriceBreakTask extends Task {
    private static final Log LOG = LogFactory.getLog(FixPriceBreakTask.class);
    @Inject
    private ProductRepository productRepository;

    public FixPriceBreakTask() {
        super("FixPriceBreakTask");
    }

    @Override
    public void execute(ImmutableMultimap<String, String> parameters, PrintWriter output) throws Exception {
        Iterable<Product> products = productRepository.listItemsBreaksAfterCreated(1519797486000L);
        int priceBreaksUpdated = 0;
        for (Product product : products) {
            if (product.getPriceBreaks() != null && product.getPriceBreaks().size() > 0) {
                for (ProductPriceBreak productPriceBreak : product.getPriceBreaks()) {
                    productPriceBreak.setId(ObjectId.get().toString());
                }
                priceBreaksUpdated++;
                LOG.info(String.format("Updating: %d - %s", priceBreaksUpdated, product.getName()));
                productRepository.updateProductPriceBreaks(product.getCompanyId(), product.getId(), product.getPriceBreaks());
            }
        }
        LOG.info("Products Price Breaks updated: " + priceBreaksUpdated);
    }
}
