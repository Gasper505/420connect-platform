package com.fourtwenty.core.tasks;

import com.fourtwenty.core.domain.models.company.CompoundTaxTable;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.google.common.collect.ImmutableMultimap;
import com.google.inject.Inject;
import io.dropwizard.servlets.tasks.Task;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.jongo.MongoCollection;

import java.io.PrintWriter;

/**
 * Created by mdo on 1/19/18.
 */
public class FixTaxTableMigrationIssue extends Task {
    private static final Log LOG = LogFactory.getLog(FixTaxTableMigrationIssue.class);
    @Inject
    ProductRepository productRepository;

    public FixTaxTableMigrationIssue() {
        super("taxtables-product-fix");
    }

    @Override
    public void execute(ImmutableMultimap<String, String> parameters, PrintWriter output) throws Exception {
        LOG.info("Running Fix TaxTables..");

        MongoCollection mongoCollection = productRepository.getMongoCollection();
        Iterable<Product> products = mongoCollection.find("{taxTables.0:{$exists:true}}").as(Product.class);
        int updated = 0;
        for (Product product : products) {
            if (product.getTaxTables() != null && product.getTaxTables().size() > 0) {

                for (CompoundTaxTable taxTable : product.getTaxTables()) {
                    taxTable.resetPrepare(product.getCompanyId());
                    taxTable.setShopId(product.getShopId());
                    taxTable.setActive(false);
                    if (taxTable.getCityTax() != null) {
                        taxTable.getCityTax().resetPrepare(product.getCompanyId());
                        taxTable.getCityTax().setShopId(product.getShopId());
                    }

                    if (taxTable.getStateTax() != null) {
                        taxTable.getCountyTax().resetPrepare(product.getCompanyId());
                        taxTable.getCountyTax().setShopId(product.getShopId());
                    }

                    if (taxTable.getCountyTax() != null) {
                        taxTable.getStateTax().resetPrepare(product.getCompanyId());
                        taxTable.getStateTax().setShopId(product.getShopId());
                    }
                    if (taxTable.getFederalTax() != null) {
                        taxTable.getFederalTax().resetPrepare(product.getCompanyId());
                        taxTable.getFederalTax().setShopId(product.getShopId());
                    }
                }
                mongoCollection.update("{_id: #}", new ObjectId(product.getId())).with("{$set: {taxTables:#, modified:#}}", product.getTaxTables(), DateTime.now().getMillis());
                updated++;
            }
        }

        LOG.info("Fix TaxTables: " + updated);
    }
}
