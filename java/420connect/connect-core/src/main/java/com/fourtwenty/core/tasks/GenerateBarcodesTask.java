package com.fourtwenty.core.tasks;

import com.fourtwenty.core.domain.models.company.BarcodeItem;
import com.fourtwenty.core.domain.models.product.PrepackageProductItem;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductBatch;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.google.common.collect.ImmutableMultimap;
import io.dropwizard.servlets.tasks.Task;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.inject.Inject;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by mdo on 4/4/17.
 */
public class GenerateBarcodesTask extends Task {
    private static final Log LOG = LogFactory.getLog(GenerateBarcodesTask.class);
    @Inject
    ProductRepository productRepository;
    @Inject
    ProductBatchRepository batchRepository;
    @Inject
    BarcodeItemRepository barcodeItemRepository;
    @Inject
    PrepackageProductItemRepository prepackageProductItemRepository;
    @Inject
    CompanyUniqueSequenceRepository companyUniqueSequenceRepository;

    public GenerateBarcodesTask() {
        super("gen-barcodes");
    }

    @Override
    public void execute(ImmutableMultimap<String, String> parameters, PrintWriter output) throws Exception {
        // Delete unused barcodes
        deleteUnusedBarcodes();

        HashMap<String, Product> products = productRepository.listNonDeletedAsMap();
        HashMap<String, ProductBatch> batches = batchRepository.listNonDeletedAsMap();
        HashMap<String, PrepackageProductItem> prepackageProductItems = prepackageProductItemRepository.listNonDeletedAsMap();
        Iterable<BarcodeItem> currentBarcodes = barcodeItemRepository.list();

        HashMap<String, BarcodeItem> itemHashMap = new HashMap<>();
        for (BarcodeItem item : currentBarcodes) {

            if (item.getShopId() == null) {
                if (item.getEntityType() == BarcodeItem.BarcodeEntityType.Product) {
                    Product product = products.get(item.getEntityId());
                    if (product != null) {
                        item.setProductId(product.getId());
                        item.setShopId(product.getShopId());
                    }
                } else if (item.getEntityType() == BarcodeItem.BarcodeEntityType.Batch) {
                    ProductBatch batch = batches.get(item.getEntityId());
                    if (batch != null) {
                        item.setProductId(batch.getProductId());
                        item.setShopId(batch.getShopId());
                    }
                } else if (item.getEntityType() == BarcodeItem.BarcodeEntityType.Prepackage) {
                    PrepackageProductItem prepackageProductItem = prepackageProductItems.get(item.getEntityId());
                    if (prepackageProductItem != null) {
                        item.setProductId(prepackageProductItem.getProductId());
                        item.setShopId(prepackageProductItem.getShopId());
                    }
                }
                barcodeItemRepository.update(item.getCompanyId(), item.getId(), item);
            }

            String key = item.getShopId() + "_" + item.getBarcode() + "_" + item.getEntityType().name();
            itemHashMap.put(key, item);
        }

        // Add sku
        List<BarcodeItem> newBarcodes = new ArrayList<>();
        List<Product> dirtyProducts = new ArrayList<>();
        HashMap<String, Product> processedSKUProducts = new HashMap<>();
        for (Product product : products.values()) {

            // Generate SKU if there's no SKU, or there was a previous sku that was processed
            if (StringUtils.isBlank(product.getSku()) || processedSKUProducts.containsKey(product.getSku())) {
                product.setSku((product.getCategoryId().substring(0, 4) + RandomStringUtils.randomAlphanumeric(8)).toUpperCase());
                dirtyProducts.add(product);
                productRepository.updateProductSKU(product.getCompanyId(), product.getId(), product.getSku());
            }
            processedSKUProducts.put(product.getSku(), product);

            String key = product.getShopId() + "_" + product.getSku() + "_" + BarcodeItem.BarcodeEntityType.Product.name();

            if (product.getSku().equalsIgnoreCase("13027001")) {
                System.out.println("hello");
            }
            if (itemHashMap.containsKey(key)) {
                if (product.getSku().equalsIgnoreCase("13027001")) {
                    System.out.println("hello");
                }
                continue;
            }
            BarcodeItem barcodeItem = new BarcodeItem();
            barcodeItem.setCompanyId(product.getCompanyId());
            barcodeItem.setShopId(product.getShopId());
            barcodeItem.setProductId(product.getId());
            barcodeItem.setEntityType(BarcodeItem.BarcodeEntityType.Product);
            barcodeItem.setEntityId(product.getId());
            barcodeItem.setBarcode(product.getSku());
            barcodeItem.setNumber(0);
            newBarcodes.add(barcodeItem);
            itemHashMap.put(key, barcodeItem);
        }

        List<ProductBatch> dirtyBatches = new ArrayList<>();
        HashMap<String, ProductBatch> processedBatches = new HashMap<>();
        for (ProductBatch productBatch : batches.values()) {

            if (!products.containsKey(productBatch.getProductId())) {
                continue; // ignore deleted products
            }
            // Generate SKU if there's no SKU, or there was a previous sku that was processed
            if (StringUtils.isBlank(productBatch.getSku()) || processedBatches.containsKey(productBatch.getSku())) {
                productBatch.setSku((productBatch.getProductId().substring(0, 4) + RandomStringUtils.randomAlphanumeric(8)).toUpperCase());
                dirtyBatches.add(productBatch);
                batchRepository.update(productBatch.getCompanyId(), productBatch.getId(), productBatch);
            }
            processedBatches.put(productBatch.getSku(), productBatch);

            String key = productBatch.getShopId() + "_" + productBatch.getSku() + "_" + BarcodeItem.BarcodeEntityType.Batch.name();
            if (itemHashMap.containsKey(key)) {
                continue;
            }


            BarcodeItem barcodeItem = new BarcodeItem();
            barcodeItem.setCompanyId(productBatch.getCompanyId());
            barcodeItem.setShopId(productBatch.getShopId());
            barcodeItem.setProductId(productBatch.getProductId());
            barcodeItem.setEntityType(BarcodeItem.BarcodeEntityType.Batch);
            barcodeItem.setEntityId(productBatch.getId());
            barcodeItem.setBarcode(productBatch.getSku());
            barcodeItem.setNumber(0);
            newBarcodes.add(barcodeItem);
            itemHashMap.put(key, barcodeItem);
        }

        for (PrepackageProductItem prepackageProductItem : prepackageProductItems.values()) {
            String key = prepackageProductItem.getShopId() + "_" + prepackageProductItem.getSku() + "_" + BarcodeItem.BarcodeEntityType.Prepackage.name();
            if (itemHashMap.containsKey(key)) {
                continue;
            }
            if (!products.containsKey(prepackageProductItem.getProductId())) {
                continue; // ignore deleted products
            }
            BarcodeItem barcodeItem = new BarcodeItem();
            barcodeItem.setCompanyId(prepackageProductItem.getCompanyId());
            barcodeItem.setShopId(prepackageProductItem.getShopId());
            barcodeItem.setProductId(prepackageProductItem.getProductId());
            barcodeItem.setEntityType(BarcodeItem.BarcodeEntityType.Prepackage);
            barcodeItem.setEntityId(prepackageProductItem.getId());
            barcodeItem.setBarcode(prepackageProductItem.getSku());
            barcodeItem.setNumber(0);
            newBarcodes.add(barcodeItem);
            itemHashMap.put(key, barcodeItem);
        }

        LOG.info("Product SKUs updated: " + dirtyProducts.size());
        LOG.info("Batch SKUs updated: " + dirtyBatches.size());
        LOG.info("New Barcodes: " + newBarcodes.size());
        barcodeItemRepository.save(newBarcodes);
    }

    private void deleteUnusedBarcodes() {
        Iterable<Product> deletedProducts = productRepository.getAllDeleted();
        List<String> productIds = new ArrayList<>();
        for (Product p : deletedProducts) {
            productIds.add(p.getId());
        }
        int deleted = barcodeItemRepository.removeAllInProductIds(productIds);


        LOG.info("Number of Deleted Products: " + productIds.size());
        LOG.info("Deleted Barcodes: " + deleted);
    }
}
