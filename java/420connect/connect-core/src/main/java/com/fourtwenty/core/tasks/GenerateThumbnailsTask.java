package com.fourtwenty.core.tasks;

import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.models.generic.Asset;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.repositories.dispensary.CompanyAssetRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.rest.dispensary.results.common.UploadFileResult;
import com.fourtwenty.core.services.imaging.ImageProcessorService;
import com.fourtwenty.core.services.thirdparty.AmazonS3Service;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import io.dropwizard.servlets.tasks.Task;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.*;
import java.net.URL;
import java.util.List;
import java.util.UUID;

/**
 * Created by mdo on 10/26/17.
 */
public class GenerateThumbnailsTask extends Task {
    private static final Log LOG = LogFactory.getLog(GenerateThumbnailsTask.class);
    @Inject
    CompanyAssetRepository companyAssetRepository;
    @Inject
    ProductRepository productRepository;
    @Inject
    ImageProcessorService imageProcessorService;
    @Inject
    AmazonS3Service amazonS3Service;
    @Inject
    ConnectConfiguration connectConfiguration;

    public GenerateThumbnailsTask() {
        super("gen-thumbs");
    }

    @Override
    public void execute(ImmutableMultimap<String, String> parameters, PrintWriter output) throws Exception {
        if (!connectConfiguration.isRegenerateThumbs()) {
            LOG.info("Generating thumbnails is disabled!!");
            return;
        }
        Iterable<Product> products = productRepository.listNonDeleted();

        LOG.info("Starting generating thumbnail images");
        int productPhotosUpdated = 0;
        List<Product> productList = Lists.newArrayList(products);
        int total = productList.size();
        int progress = 0;
        for (Product product : productList) {
            boolean updated = false;
            progress++;
            try {
                for (CompanyAsset companyAsset : product.getAssets()) {
                    if (companyAsset.getAssetType() == Asset.AssetType.Photo
                            && companyAsset.isSecured() == false
                            && StringUtils.isNotBlank(companyAsset.getPublicURL())
                            && StringUtils.isBlank(companyAsset.getThumbURL())) {
                        // all requirements match, let's generate new images

                        try {
                            URL publicURL = new URL(companyAsset.getPublicURL());
                            InputStream stream = publicURL.openStream();

                            String ext2 = "." + FilenameUtils.getExtension(companyAsset.getKey()); // returns "exe"
                            String keyName = companyAsset.getKey().replace(ext2, "");
                            File file = stream2file(stream, ext2);

                            ImageProcessorService.ImageProcessingResult imageProcessingResult = imageProcessorService.processImage(file, companyAsset.getKey());
                            boolean generated = false;
                            if (imageProcessingResult != null) {
                                if (imageProcessingResult.large != null) {
                                    UploadFileResult resizeResult = amazonS3Service.uploadFilePublic(imageProcessingResult.large.inputStream,
                                            imageProcessingResult.large.objectMetadata,
                                            keyName + "-" + imageProcessingResult.large.size, ext2);

                                    if (resizeResult != null) {
                                        companyAsset.setLargeURL(resizeResult.getUrl());
                                        companyAsset.setPublicURL(resizeResult.getUrl());
                                        companyAsset.setKey(resizeResult.getKey());
                                        updated = true;
                                        generated = true;
                                    }
                                }

                                if (imageProcessingResult.medium != null) {
                                    UploadFileResult resizeResult = amazonS3Service.uploadFilePublic(imageProcessingResult.medium.inputStream,
                                            imageProcessingResult.medium.objectMetadata,
                                            keyName + "-" + imageProcessingResult.medium.size, ext2);
                                    if (resizeResult != null) {
                                        companyAsset.setMediumURL(resizeResult.getUrl());
                                        updated = true;
                                        generated = true;
                                    }
                                }

                                if (imageProcessingResult.small != null) {
                                    UploadFileResult resizeResult = amazonS3Service.uploadFilePublic(imageProcessingResult.small.inputStream,
                                            imageProcessingResult.small.objectMetadata,
                                            keyName + "-" + imageProcessingResult.small.size, ext2);
                                    if (resizeResult != null) {
                                        companyAsset.setThumbURL(resizeResult.getUrl());
                                        updated = true;
                                        generated = true;
                                    }
                                }
                            }

                            if (generated) {
                                LOG.info("Asset generated: " + companyAsset.getKey());
                                companyAssetRepository.update(companyAsset.getCompanyId(), companyAsset.getId(), companyAsset);
                                productPhotosUpdated++;
                            }

                        } catch (Exception e) {
                            LOG.error("error retrieving image", e);
                        }
                    }
                }

                if (updated) {
                    // if any of the assets are updated, let's update the product
                    LOG.info(String.format("Generated: %d / %d", progress, total));
                    productRepository.updateCachedAssets(product.getCompanyId(), product.getId(), product.getAssets());
                    // break, we only want to do one for now
                }
            } catch (Exception e) {
                LOG.info("Fail to migrate product: " + product.getName() + " for shop: " + product.getShopId());
            }

        }
        LOG.info("Images generated: " + productPhotosUpdated);
    }

    private static File stream2file(InputStream in, String extension) throws IOException {
        final File tempFile = File.createTempFile(UUID.randomUUID().toString(), extension);
        tempFile.deleteOnExit();
        try (FileOutputStream out = new FileOutputStream(tempFile)) {
            IOUtils.copy(in, out);
        }
        return tempFile;
    }
}
