package com.fourtwenty.core.tasks;

import com.fourtwenty.core.domain.repositories.thirdparty.ThirdPartyShopAccountRepository;
import com.google.common.collect.ImmutableMultimap;
import io.dropwizard.servlets.tasks.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.PrintWriter;

public class HypurIndexMigration extends Task {

    private static final Logger LOGGER = LoggerFactory.getLogger(HypurIndexMigration.class);

    @Inject
    private ThirdPartyShopAccountRepository thirdPartyShopAccountRepository;

    public HypurIndexMigration() {
        super("hypur-index-migration");
    }

    @Override
    public void execute(ImmutableMultimap<String, String> parameters, PrintWriter output) throws Exception {
        LOGGER.info("Starting migration for Hypur index fix");
        thirdPartyShopAccountRepository.migrateIndex();
        LOGGER.info("Completed migration for Hypur index fix");
    }
}
