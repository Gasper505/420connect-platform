package com.fourtwenty.core.tasks;

import com.amazonaws.util.CollectionUtils;
import com.amazonaws.util.StringUtils;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.company.Terminal;
import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.QuantityLog;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.services.inventory.BatchQuantityService;
import com.fourtwenty.core.services.inventory.InventoryOperationService;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Lists;
import io.dropwizard.servlets.tasks.Task;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class InventoryActionMigration extends Task {

    private static final Log LOG = LogFactory.getLog(InventoryActionMigration.class);

    @Inject
    private PrepackageRepository prepackageRepository;
    @Inject
    private ProductBatchRepository productBatchRepository;
    @Inject
    private BatchQuantityRepository batchQuantityRepository;
    @Inject
    private InventoryActionRepository inventoryActionRepository;
    @Inject
    private PrepackageProductItemRepository prepackageProductItemRepository;
    @Inject
    private ProductPrepackageQuantityRepository prepackageQuantityRepository;
    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private InventoryOperationService inventoryOperationService;
    @Inject
    private InventoryRepository inventoryRepository;
    @Inject
    private BatchQuantityService batchQuantityService;
    @Inject
    private TerminalRepository terminalRepository;


    protected InventoryActionMigration() {
        super("inventory-action-migration");
    }

    @Override
    public void execute(ImmutableMultimap<String, String> parameters, PrintWriter output) throws Exception {

        LOG.info("Starting migration for update inventory in inventory action.");


        List<InventoryOperation> actionList = new ArrayList<>();

        actionList.addAll(migrateBatchQuantities());
        actionList.addAll(migratePrepackageQuantities());


        List<List<InventoryOperation>> partition = Lists.partition(actionList, 1000);

        for (List<InventoryOperation> actions : partition) {
            inventoryActionRepository.save(actions);
        }

        migrateTransactionInventory();

        LOG.info("Ending migration for update inventory in inventory action.");
    }

    private List<InventoryOperation> migrateBatchQuantities() {
        List<InventoryOperation> actionList = new ArrayList<>();

        HashMap<String, ProductBatch> productBatchMap = productBatchRepository.listAsMap();
        List<BatchQuantity> batchQuantities = batchQuantityRepository.list();
        ProductBatch productBatch = null;
        if (!CollectionUtils.isNullOrEmpty(batchQuantities)) {
            for (BatchQuantity batchQuantity : batchQuantities) {
                if (StringUtils.isNullOrEmpty(batchQuantity.getBatchId()) ||
                        StringUtils.isNullOrEmpty(batchQuantity.getProductId()) ||
                        StringUtils.isNullOrEmpty(batchQuantity.getInventoryId()) ||
                        batchQuantity.getQuantity().longValue() == 0) {
                    continue;
                }
                productBatch = productBatchMap.get(batchQuantity.getBatchId());
                if (productBatch == null || productBatch.isDeleted()) {
                    continue;
                }

                InventoryOperation inventoryOperation = new InventoryOperation();

                inventoryOperation.setDeleted(batchQuantity.isDeleted());
                inventoryOperation.prepare(batchQuantity.getCompanyId());
                inventoryOperation.setShopId(batchQuantity.getShopId());

                inventoryOperation.setAction(InventoryOperation.Operation.Adjust);
                inventoryOperation.setQuantity(batchQuantity.getQuantity());
                inventoryOperation.setRunningTotal(batchQuantity.getQuantity());
                inventoryOperation.setBatchId(batchQuantity.getBatchId());
                inventoryOperation.setInventoryId(batchQuantity.getInventoryId());
                inventoryOperation.setProductId(batchQuantity.getProductId());
                inventoryOperation.setSourceType(InventoryOperation.SourceType.System);
                inventoryOperation.setSourceId(batchQuantity.getBatchId());
                inventoryOperation.setRequestId(this.createSourceRequestId());

                actionList.add(inventoryOperation);
            }
        }

        return actionList;
    }

    private List<InventoryOperation> migratePrepackageQuantities() {
        List<InventoryOperation> actionList = new ArrayList<>();

        List<ProductPrepackageQuantity> prepackageQuantities = prepackageQuantityRepository.list();
        HashMap<String, PrepackageProductItem> prepackageProductItemMap = prepackageProductItemRepository.listAsMap();
        HashMap<String, Prepackage> prepackageMap = prepackageRepository.listAsMap();

        if (!CollectionUtils.isNullOrEmpty(prepackageQuantities)) {
            for (ProductPrepackageQuantity prepackageQuantity : prepackageQuantities) {

                if (StringUtils.isNullOrEmpty(prepackageQuantity.getInventoryId()) || StringUtils.isNullOrEmpty(prepackageQuantity.getPrepackageId())
                        || StringUtils.isNullOrEmpty(prepackageQuantity.getPrepackageItemId()) || StringUtils.isNullOrEmpty(prepackageQuantity.getProductId())) {
                    continue;
                }

                PrepackageProductItem prepackageProductItem = prepackageProductItemMap.get(prepackageQuantity.getPrepackageItemId());
                if (prepackageProductItem == null || prepackageProductItem.isDeleted()) {
                    continue;
                }

                Prepackage prepackage = prepackageMap.get(prepackageQuantity.getPrepackageId());
                if (prepackage == null) {
                    continue;
                }

                InventoryOperation inventoryOperation = new InventoryOperation();

                inventoryOperation.setDeleted(prepackageQuantity.isDeleted());
                inventoryOperation.prepare(prepackageQuantity.getCompanyId());
                inventoryOperation.setShopId(prepackageQuantity.getShopId());
                inventoryOperation.setPrepackaged(true);
                inventoryOperation.setAction(InventoryOperation.Operation.Adjust);
                inventoryOperation.setQuantity(new BigDecimal(prepackageQuantity.getQuantity()));
                inventoryOperation.setRunningTotal(new BigDecimal(prepackageQuantity.getQuantity()));
                inventoryOperation.setPrepackageItemId(prepackageQuantity.getPrepackageItemId());
                inventoryOperation.setBatchId(prepackageProductItem.getBatchId());
                inventoryOperation.setInventoryId(prepackageQuantity.getInventoryId());
                inventoryOperation.setProductId(prepackageQuantity.getProductId());
                inventoryOperation.setSourceType(InventoryOperation.SourceType.System);
                inventoryOperation.setSourceId(prepackageQuantity.getPrepackageItemId());
                inventoryOperation.setRequestId(this.createSourceRequestId());

                actionList.add(inventoryOperation);
            }
        }

        return actionList;
    }

    private void migrateTransactionInventory() {

        HashMap<String, Terminal> terminals = terminalRepository.listAsMap();


        Iterable<Transaction> transactions = transactionRepository.getAllActiveTransactions();
        for (Transaction transaction : transactions) {
            if (CollectionUtils.isNullOrEmpty(transaction.getCart().getItems())) continue;

            final Inventory safeInventory = inventoryRepository.getInventory(
                    transaction.getCompanyId(), transaction.getShopId(), Inventory.SAFE);

            for (OrderItem orderItem : transaction.getCart().getItems()) {
                if (CollectionUtils.isNullOrEmpty(orderItem.getQuantityLogs())) continue;

                for (QuantityLog quantityLog : orderItem.getQuantityLogs()) {
                    BigDecimal runningTotal = new BigDecimal(0);
                    InventoryOperation previousInventoryOperation = inventoryOperationService.getRecentOperation(
                            transaction.getCompanyId(),
                            transaction.getShopId(),
                            orderItem.getProductId(),
                            quantityLog.getBatchId(),
                            quantityLog.getInventoryId(),
                            quantityLog.getPrepackageItemId());
                    if (previousInventoryOperation != null) {
                        runningTotal = runningTotal.add(previousInventoryOperation.getRunningTotal());
                    }

                    BigDecimal newRunningTotal = runningTotal.add(quantityLog.getQuantity());


                    // Put Transaction's OrderItem inventory back
                    InventoryOperation inventoryOperation = new InventoryOperation();

                    inventoryOperation.prepare(transaction.getCompanyId());
                    inventoryOperation.setShopId(transaction.getShopId());

                    inventoryOperation.setAction(InventoryOperation.Operation.Adjust);
                    inventoryOperation.setQuantity(quantityLog.getQuantity());
                    inventoryOperation.setRunningTotal(newRunningTotal);
                    inventoryOperation.setBatchId(quantityLog.getBatchId());
                    inventoryOperation.setInventoryId(quantityLog.getInventoryId());
                    inventoryOperation.setProductId(orderItem.getProductId());
                    inventoryOperation.setSourceType(InventoryOperation.SourceType.System);
                    inventoryOperation.setSourceId(quantityLog.getBatchId());
                    inventoryOperation.setPrepackageItemId(quantityLog.getPrepackageItemId());
                    inventoryOperation.setRequestId(this.createSourceRequestId());
                    inventoryActionRepository.save(inventoryOperation);
                }

                takeItemsOutOfInventory(
                        transaction.getCompanyId(),
                        transaction.getShopId(),
                        orderItem,
                        transaction,
                        terminals.get(transaction.getTerminalId()),
                        safeInventory,
                        InventoryOperation.SourceType.Transaction,
                        createSourceRequestId());
            }
        }
    }


    // PULLED FROM InventoryActionManager
    private InventoryOperationService.InventoryOperationResult takeItemsOutOfInventory(String companyId,
                                                                                       String shopId,
                                                                                       OrderItem orderItem,
                                                                                       Transaction dbTransaction,
                                                                                       Terminal terminal,
                                                                                       Inventory safeInventory,
                                                                                       InventoryOperation.SourceType sourceType, String requestId) {
        InventoryOperationService.InventoryOperationResult result = new InventoryOperationService.InventoryOperationResult();

        if (dbTransaction.getCheckoutType() == Shop.ShopCheckoutType.Fulfillment) {
            if (!orderItem.isFulfilled()) {
                return result;
            }
        }

        // clear previous quantity logs as we are now taking new sets
        orderItem.getQuantityLogs().clear();

        // use safe inventory as default
        String assignedInventory = terminal != null ? terminal.getAssignedInventoryId() : safeInventory.getId();
        if (org.apache.commons.lang3.StringUtils.isNotBlank(dbTransaction.getOverrideInventoryId())) {
            assignedInventory = dbTransaction.getOverrideInventoryId();
        }

        // Calculate quantity
        if (org.apache.commons.lang3.StringUtils.isNotEmpty(orderItem.getPrepackageItemId())
                && ObjectId.isValid(orderItem.getPrepackageItemId())) {

            // Find the quantity we want to deduct this from
            orderItem.addQuantityLog(assignedInventory, orderItem.getPrepackageItemId(), orderItem.getBatchId(), orderItem.getQuantity().intValue());


        } else {
            // try to deduct accordingly
            List<QuantityLog> quantityLogs = batchQuantityService.fakeSubtractBatchQuantity(companyId,
                    shopId,
                    orderItem.getProductId(),
                    assignedInventory,
                    orderItem.getBatchId(),
                    orderItem.getQuantity(),
                    orderItem.getOrderItemId());

            orderItem.addQuantityLogs(quantityLogs);

        }

        // now try to adjust accordingly here
        for (QuantityLog quantityLog : orderItem.getQuantityLogs()) {
            BigDecimal takeQty = quantityLog.getQuantity().multiply(new BigDecimal(-1));

            String batchId = quantityLog.getBatchId();
            String prepackageItemId = quantityLog.getPrepackageItemId();

            InventoryOperationService.InventoryOperationResult result1 = inventoryOperationService.adjust(companyId,
                    shopId, orderItem.getProductId(), prepackageItemId,
                    batchId,
                    assignedInventory,
                    takeQty,
                    dbTransaction.getSellerId(),
                    sourceType,
                    dbTransaction.getId(),
                    orderItem.getId(), requestId,
                    InventoryOperation.SubSourceAction.Sale);

            result.operations.addAll(result1.operations);
        }

        return result;
    }

    private String createSourceRequestId() {
        return UUID.randomUUID().toString() + DateTime.now().getMillis();
    }
}
