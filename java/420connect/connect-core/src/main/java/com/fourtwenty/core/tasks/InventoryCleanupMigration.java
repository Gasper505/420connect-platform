package com.fourtwenty.core.tasks;

import com.amazonaws.util.CollectionUtils;
import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.google.common.collect.ImmutableMultimap;
import io.dropwizard.servlets.tasks.Task;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class InventoryCleanupMigration extends Task {

    private static final Log LOG = LogFactory.getLog(InventoryCleanupMigration.class);

    @Inject
    private ProductRepository productRepository;
    @Inject
    private InventoryRepository inventoryRepository;
    @Inject
    private BatchQuantityRepository batchQuantityRepository;
    @Inject
    private ProductCategoryRepository productCategoryRepository;
    @Inject
    private ProductBatchRepository productBatchRepository;


    protected InventoryCleanupMigration() {
        super("inventory-cleanup-migration");
    }

    @Override
    public void execute(ImmutableMultimap<String, String> parameters, PrintWriter output) throws Exception {

        LOG.info("Starting inventory cleanup migration.");

        HashMap<String, Product> productMap = productRepository.listAsMap();
        HashMap<String, Inventory> inventoryMap = inventoryRepository.listAsMap();

        removeBadBatchQuantities(inventoryMap);

        cleanEmptyProductQuantities(productMap, inventoryMap);

        createBatchesFromProductQuantities(productMap);

        LOG.info("Ending inventory cleanup migration.");

    }

    private void cleanEmptyProductQuantities(HashMap<String, Product> productMap,
                                             HashMap<String, Inventory> inventoryMap) {

        if (CollectionUtils.isNullOrEmpty(productMap.values())) return;

        for (Product product : productMap.values()) {
            if (CollectionUtils.isNullOrEmpty(product.getQuantities())) continue;

            // Clean Product.Quantities
            List<ProductQuantity> cleanProductQuantities = product.getQuantities()
                    .stream()
                    // Filter zero ProductQuantities
                    .filter(pq -> {
                        boolean keepIt = pq.getQuantity() != null && pq.getQuantity().longValue() != 0;
                        if (!keepIt) {
                            LOG.debug(String.format("Removing product quantity %s for product %s. Zero Quantity, no longer needed.",
                                    pq.getId(), product.getId()));
                        }
                        return keepIt;
                    })
                    // Filter ProductQuantities from other shops (previous bug)
                    .filter(pq -> {
                        Inventory inventory = inventoryMap.get(pq.getInventoryId());
                        boolean keepIt = inventory != null && inventory.getShopId().equals(product.getShopId());
                        if (!keepIt) {
                            LOG.debug(String.format("Removing product quantity %s for product %s. Doesn't match shop of product.",
                                    pq.getId(), product.getId()));
                        }
                        return keepIt;
                    })
                    .collect(Collectors.toList());

            // ReSave Product if necessary
            boolean saveProduct = product.getQuantities().size() != cleanProductQuantities.size();
            if (saveProduct) {
                product.setQuantities(cleanProductQuantities);
                productRepository.updateProductQuantities(
                        product.getCompanyId(), product.getId(), cleanProductQuantities);
            }
        }
    }

    private void createBatchesFromProductQuantities(HashMap<String, Product> productMap) {

        HashMap<String, ProductCategory> categoryHashMap = productCategoryRepository.listAsMap();

        if (CollectionUtils.isNullOrEmpty(productMap.values())) return;

        List<ProductBatch> newProductBatches = new ArrayList<>();
        List<BatchQuantity> newBatchQuantities = new ArrayList<>();

        for (Product product : productMap.values()) {
            if (CollectionUtils.isNullOrEmpty(product.getQuantities())) continue;

            List<BatchQuantity> productBatchQuantities = batchQuantityRepository.getBatchQuantities(
                    product.getCompanyId(), product.getShopId(), product.getId());

            // Get Distinct InventoryIds
            List<String> inventoryIds = product.getQuantities()
                    .stream()
                    .map(pq -> pq.getInventoryId())
                    .distinct()
                    .collect(Collectors.toList());

            // Loop through each inventoryId and identify any remaining balance after cancelling out batch quantities
            for (String inventoryId : inventoryIds) {

                long inventoryQuantity = product.getQuantities()
                        .stream()
                        .filter(pq -> pq.getInventoryId().equals(inventoryId))
                        .map(pq -> pq.getQuantity() != null ? pq.getQuantity().longValue() : 0)
                        .reduce(Long::sum)
                        .orElseGet(() -> Long.valueOf(0));

                long totalBatchQuantity = productBatchQuantities
                        .stream()
                        .filter(bq -> bq.getInventoryId().equals(inventoryId))
                        .map(bq -> bq.getQuantity() != null ? bq.getQuantity().longValue() : 0)
                        .reduce(Long::sum)
                        .orElseGet(() -> Long.valueOf(0));

                long remainingQuantity = inventoryQuantity - totalBatchQuantity;

                if (remainingQuantity == 0) continue;


                List<BatchQuantity> sortedInventoryBatchQuantities = productBatchQuantities
                        .stream()
                        .filter(bq -> bq.getInventoryId().equals(inventoryId))
                        .sorted((o1, o2) -> o2.getCreated().compareTo(o1.getCreated()))
                        .collect(Collectors.toList());

                if (!CollectionUtils.isNullOrEmpty(sortedInventoryBatchQuantities)) {

                    BatchQuantity batchQuantity = sortedInventoryBatchQuantities.get(0);

                    BigDecimal newQuantity = new BigDecimal(batchQuantity.getQuantity().longValue() + remainingQuantity);
                    batchQuantity.setQuantity(newQuantity);
                    batchQuantityRepository.update(product.getCompanyId(), batchQuantity.getId(), batchQuantity);

                } else {
                    // Since theres no existing Batch Quantity for this inventoryId, create a new one
                    ProductCategory category = categoryHashMap.get(product.getCategoryId());

                    // Check for existing ProductBatch

                    // Create ProductBatch
                    String sku = String.format("AUTOGEN-%s-%s", product.getId(), inventoryId);
                    ProductBatch productBatch = new ProductBatch();
                    productBatch.setShopId(product.getShopId());
                    productBatch.setQuantity(new BigDecimal(remainingQuantity));
                    productBatch.setSku(sku);
                    productBatch.setPurchasedDate(DateTime.now().getMillis());
                    productBatch.setReceiveDate(productBatch.getPurchasedDate());
                    productBatch.setProductId(product.getId());
                    productBatch.setVendorId(product.getVendorId());
                    productBatch.setBrandId(product.getBrandId());

                    productBatch.setActive(true);
                    productBatch.prepare(product.getCompanyId());
                    newProductBatches.add(productBatch);

                    // Create BatchQuantity
                    BatchQuantity batchQuantity = new BatchQuantity();
                    batchQuantity.setShopId(product.getShopId());
                    batchQuantity.setQuantity(productBatch.getQuantity());
                    batchQuantity.setBatchPurchaseDate(productBatch.getPurchasedDate());
                    batchQuantity.setProductId(product.getId());
                    batchQuantity.setInventoryId(inventoryId);
                    batchQuantity.setBatchId(productBatch.getId());
                    batchQuantity.setUnitType(category.getUnitType());
                    batchQuantity.prepare(product.getCompanyId());
                    newBatchQuantities.add(batchQuantity);

                    LOG.debug(String.format(
                            "Created new Batch Quantity %s for the remaining quantity balance of %s of product %s in inventory %s",
                            batchQuantity.getBatchId(),
                            remainingQuantity,
                            product.getId(),
                            inventoryId));
                }
            }
        }

        productBatchRepository.save(newProductBatches);
        batchQuantityRepository.save(newBatchQuantities);
    }

    private void removeBadBatchQuantities(HashMap<String, Inventory> inventories) {
        // Remove any BatchQuantities that dont have a BatchId specified or the BatchId is empty
        batchQuantityRepository.removeAnyWithoutBatchId();

        // Remove any BatchQuantities that are associated to Inventories that dont exist
        if (!CollectionUtils.isNullOrEmpty(inventories.values())) {
            List<String> inventoryIds = inventories.values()
                    .stream()
                    .map(i -> i.getId())
                    .collect(Collectors.toList());

            batchQuantityRepository.removeAnyWithoutInventoryIds(inventoryIds);
        }
    }
}
