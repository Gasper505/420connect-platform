package com.fourtwenty.core.tasks;

import com.fourtwenty.core.domain.models.marketing.MarketingJob;
import com.fourtwenty.core.domain.repositories.dispensary.MarketingJobRepository;
import com.google.common.collect.ImmutableMultimap;
import io.dropwizard.servlets.tasks.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.PrintWriter;
import java.util.List;

public class MarketingJobTask extends Task {
    private static final Logger LOGGER = LoggerFactory.getLogger(TransactionMileageTask.class);

    @Inject
    private MarketingJobRepository marketingJobRepository;

    protected MarketingJobTask() {
        super("update-job-type");
    }

    @Override
    public void execute(ImmutableMultimap<String, String> parameters, PrintWriter output) throws Exception {
        LOGGER.info("Starting migration for updating marketing job type");

        List<MarketingJob> marketingJobList = marketingJobRepository.getMarketingJobsWithoutType();

        for (MarketingJob marketingJob : marketingJobList) {
            marketingJob.setType(MarketingJob.MarketingType.Blasts);

            marketingJobRepository.update(marketingJob.getId(), marketingJob);
        }


        LOGGER.info("Completed migration for updating marketing job type");
    }
}
