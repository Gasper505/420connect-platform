package com.fourtwenty.core.tasks;

import com.fourtwenty.core.domain.models.customer.Identification;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.repositories.dispensary.MemberRepository;
import com.google.common.collect.ImmutableMultimap;
import io.dropwizard.servlets.tasks.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.PrintWriter;
import java.util.List;

public class MemberIdentificationExpiryDateMigration extends Task {

    private static final Logger LOGGER = LoggerFactory.getLogger(MemberIdentificationExpiryDateMigration.class);

    @Inject
    private MemberRepository memberRepository;

    public MemberIdentificationExpiryDateMigration() {
        super("member-identification-expiry-date-migration");
    }

    @Override
    public void execute(ImmutableMultimap<String, String> parameters, PrintWriter output) throws Exception {

        Iterable<Member> membersList = memberRepository.getMembersWithNegativeLongForIdentification();

        LOGGER.info("Starting migration for member identification expiry date fix ");

        for (Member member : membersList) {
            List<Identification> identificationList = member.getIdentifications();
            for (Identification identification : identificationList) {
                identification.setExpirationDate(null);
            }

            memberRepository.update(member.getId(), member);

        }

        LOGGER.info("Completed migration for member identification expiry date");

    }

}
