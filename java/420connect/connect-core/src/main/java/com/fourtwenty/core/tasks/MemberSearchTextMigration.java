package com.fourtwenty.core.tasks;

import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.repositories.dispensary.MemberRepository;
import com.fourtwenty.core.util.EntityFieldUtil;
import com.google.common.collect.ImmutableMultimap;
import com.google.inject.Inject;
import io.dropwizard.servlets.tasks.Task;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.PrintWriter;
import java.util.Objects;

/**
 * Created on 28/11/17 2:36 PM
 * Raja Dushyant Vashishtha (Sr. Software Engineer)
 * rajad@decipherzone.com
 * Decipher Zone Softwares
 * www.decipherzone.com
 */
public class MemberSearchTextMigration extends Task {

    private static final Logger LOGGER = LoggerFactory.getLogger(MemberSearchTextMigration.class);

    @Inject
    private MemberRepository memberRepository;

    public MemberSearchTextMigration() {
        super("member-search-text-migration");
    }

    @Override
    public void execute(ImmutableMultimap<String, String> parameters, PrintWriter output) throws Exception {
        Iterable<Member> memberIterable = memberRepository.iterator();
        LOGGER.info("Starting migration for member search text update");
        final int[] count = {0};
        memberIterable.forEach(member -> {
            String search = String.format("%s %s %s %s %s %s",
                    EntityFieldUtil.value(member.getFirstName()),
                    EntityFieldUtil.value(member.getLastName()),
                    EntityFieldUtil.value(!Objects.isNull(member.getAddress()) ? member.getAddress().getAddress() : StringUtils.EMPTY),
                    EntityFieldUtil.value(member.getEmail()),
                    EntityFieldUtil.value(member.getPrimaryPhone()),
                    EntityFieldUtil.value(!Objects.isNull(member.getMemberGroup()) ? member.getMemberGroup().getName() : StringUtils.EMPTY));
            member.setSearchText(search.toLowerCase());

            memberRepository.update(member.getCompanyId(), member.getId(), member);
            count[0]++;
        });

        LOGGER.info("Total records processed for member search text update : {}", count[0]);
        LOGGER.info("Finished migration for member search text update");

    }
}
