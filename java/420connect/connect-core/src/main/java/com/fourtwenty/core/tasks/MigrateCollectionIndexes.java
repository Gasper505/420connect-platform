package com.fourtwenty.core.tasks;

import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.lifecycle.model.IndexData;
import com.fourtwenty.core.lifecycle.model.IndexesData;
import com.google.common.collect.ImmutableMultimap;
import com.google.inject.Inject;
import io.dropwizard.servlets.tasks.Task;
import org.apache.commons.lang3.StringUtils;
import org.jongo.MongoCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class MigrateCollectionIndexes extends Task {
    private static final Logger LOGGER = LoggerFactory.getLogger(MigrateCollectionIndexes.class);
    private static final String ERR_MSG = "Error while updating index for collection :";

    private IndexesData indexesData;
    private MongoDb mongoDb;

    @Inject
    public MigrateCollectionIndexes(IndexesData indexesData, MongoDb mongoDb) {
        super("migrate-collection-indexes");
        this.indexesData = indexesData;
        this.mongoDb = mongoDb;
    }

    @Override
    public void execute(ImmutableMultimap<String, String> parameters, PrintWriter output) throws Exception {
        LOGGER.info("Starting migration for create/update index for collection.");

        List<IndexData> indexList = indexesData.getIndexesData();
        if (!indexesData.isCreate()) {
            return;
        }
        for (IndexData indexData : indexList) {
            if (StringUtils.isBlank(indexData.getCollectionName())) {
                continue;
            }
            prepareMongoIndexes(indexData.getCollectionName(), (indexData.getIndexes() == null ? new ArrayList<>() : indexData.getIndexes()), (indexData.getUniqueIndexes() == null) ? new ArrayList<>() : indexData.getUniqueIndexes(),
                    (indexData.getDeleteIndexes() == null) ? new ArrayList<>() : indexData.getDeleteIndexes());
        }
        LOGGER.info("Ending migration for create/update index for collection.");
    }

    private void prepareMongoIndexes(String collectionName, List<String> indexList, List<String> uniqueIndexes, List<String> deleteIndexs) throws Exception {
        MongoCollection mongoCollection = mongoDb.getJongoCollection(collectionName);
        // check for indexes

        try {
            for (String deleteIndex : deleteIndexs) {
                mongoCollection.dropIndex(deleteIndex);
            }
        } catch (Exception e) {
            LOGGER.error("Delete index\t" + ERR_MSG + collectionName + "\terror : " + e.getMessage());
        }

        try {
            for (String index : indexList) {
                mongoCollection.ensureIndex(index);
            }
        } catch (Exception e) {
            LOGGER.error("Index\t" + ERR_MSG + collectionName + "\terror : " + e.getMessage());
        }

        try {
            for (String index : uniqueIndexes) {
                mongoCollection.ensureIndex(index, "{unique:true}");
            }
        } catch (Exception e) {
            LOGGER.error("Unique index\t" + ERR_MSG + collectionName + "\terror : " + e.getMessage());
        }

    }
}
