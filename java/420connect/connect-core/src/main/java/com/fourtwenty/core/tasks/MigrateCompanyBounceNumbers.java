package com.fourtwenty.core.tasks;

import com.fourtwenty.core.domain.models.company.BouncedNumber;
import com.fourtwenty.core.domain.models.company.CompanyBounceNumber;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.marketing.MarketingJob;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.managed.AmazonServiceManager;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Lists;
import io.dropwizard.servlets.tasks.Task;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

public class MigrateCompanyBounceNumbers extends Task {

    private static final Logger LOGGER = LoggerFactory.getLogger(MigrateCompanyBounceNumbers.class);
    @Inject
    private MarketingJobRepository marketingJobRepository;
    @Inject
    private CompanyBounceNumberRepository companyBounceNumberRepository;
    @Inject
    private MemberRepository memberRepository;
    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private ProductRepository productRepository;
    @Inject
    private ShopRepository shopRepository;
    @Inject
    private BouncedNumberRepository bouncedNumberRepository;

    public MigrateCompanyBounceNumbers() {
        super("migrate-company-bounce-number");
    }

    @Override
    public void execute(ImmutableMultimap<String, String> parameters, PrintWriter output) throws Exception {
        LOGGER.info("Starting company bounce number migration task");

        long count = companyBounceNumberRepository.count();
        if (count > 0) {
            LOGGER.info("SKIPPING COMPANY BOUNCED MIGRATION");
            return;
        }

        Iterable<MarketingJob> marketingJobs = marketingJobRepository.findMarketingJobsByStatus(MarketingJob.MarketingJobType.SMS,
                MarketingJob.MarketingJobStatus.Completed);

        HashSet<String> companySet = new HashSet<>();
        for (MarketingJob job : marketingJobs) {
            companySet.add(job.getCompanyId());
        }

        List<BouncedNumber> bouncedNumbers = bouncedNumberRepository.list();
        HashMap<String, BouncedNumber> bouncedNumberHashMap = new HashMap<>();

        for (BouncedNumber bouncedNumber : bouncedNumbers) {
            bouncedNumberHashMap.put(bouncedNumber.getNumber(), bouncedNumber);
        }

        Iterable<Member> members = memberRepository.listWithOptions(Lists.newArrayList(companySet),
                Member.MembershipStatus.Active, "{companyId:1,primaryPhone:1}");

        HashMap<String, CompanyBounceNumber> toBeSaved = new HashMap<>();

        HashMap<String, List<ObjectId>> optOuts = new HashMap<>();

        for (Member m : members) {
            String cpn = AmazonServiceManager.cleanPhoneNumber(m.getPrimaryPhone(), null);
            BouncedNumber bouncedNumber = bouncedNumberHashMap.get(cpn);
            if (bouncedNumber != null && bouncedNumber.getNumber() != null) {
                String key = String.format("%s_%s", m.getCompanyId(), bouncedNumber.getNumber());

                CompanyBounceNumber companyBounceNumber = new CompanyBounceNumber();
                companyBounceNumber.prepare(m.getCompanyId());
                companyBounceNumber.setStatus(CompanyBounceNumber.BounceNumberStatus.OPT_OUT);
                companyBounceNumber.setNumber(bouncedNumber.getNumber());

                toBeSaved.put(key, companyBounceNumber);

                if (m.isTextOptIn()) {
                    // let's opt these out
                    List<ObjectId> objectIds = optOuts.getOrDefault(m.getCompanyId(), new ArrayList<ObjectId>());
                    objectIds.add(new ObjectId(m.getId()));
                    LOGGER.info(String.format("Opting-out: %s", m.getId()));
                    optOuts.put(m.getCompanyId(), objectIds);
                }
            }
        }

        // Update opt-outs
        for (String companyId : optOuts.keySet()) {
            List<ObjectId> memberIds = optOuts.get(companyId);
            memberRepository.bulkUpdateTextOptIn(companyId, memberIds, false);
        }

        // Create bounced
        if (!toBeSaved.isEmpty()) {
            companyBounceNumberRepository.save(Lists.newArrayList(toBeSaved.values()));
        }

        LOGGER.info("Complete company bounce number migration... migrated: " + toBeSaved.size());


    }

}
