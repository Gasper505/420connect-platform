package com.fourtwenty.core.tasks;

import com.amazonaws.util.CollectionUtils;
import com.fourtwenty.core.domain.models.company.*;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import io.dropwizard.servlets.tasks.Task;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

public class MigrateCompanyEmployeeTask extends Task {
    private static final Logger LOGGER = LoggerFactory.getLogger(MigrateCompanyEmployeeTask.class);
    private CompanyRepository companyRepository;
    private CompanyFeaturesRepository companyFeaturesRepository;
    private EmployeeRepository employeeRepository;
    private ShopRepository shopRepository;
    private RoleRepository roleRepository;

    @Inject
    public MigrateCompanyEmployeeTask(CompanyRepository companyRepository, CompanyFeaturesRepository companyFeaturesRepository,
                                      EmployeeRepository employeeRepository, ShopRepository shopRepository,
                                      RoleRepository roleRepository) {
        super("migrate-company-employee-task");
        this.companyRepository = companyRepository;
        this.companyFeaturesRepository = companyFeaturesRepository;
        this.employeeRepository = employeeRepository;
        this.shopRepository = shopRepository;
        this.roleRepository = roleRepository;
    }

    @Override
    public void execute(ImmutableMultimap<String, String> parameters, PrintWriter output) throws Exception {
        LOGGER.info("Starting migration for company and employee");
        //Update company feature's available apps for every company.
        SearchResult<Company> searchResult = companyRepository.findItems(0, Integer.MAX_VALUE);
        for (Company company : searchResult.getValues()) {
            SearchResult<Employee> employeeSearchResult = employeeRepository.findItems(company.getId(), 0, Integer.MAX_VALUE);
            SearchResult<Shop> shopSearchResult = shopRepository.findItems(company.getId(), 0, Integer.MAX_VALUE);
            List<ObjectId> objectIdsForAuthenticationAppAccess = new ArrayList<>();
            List<ObjectId> objectIdsForRetailAppAccess = new ArrayList<>();
            List<ObjectId> objectIdsForDistributionAppAccess = new ArrayList<>();
            LinkedHashSet<CompanyFeatures.AppTarget> appAccessList = new LinkedHashSet<>();

            if (!CollectionUtils.isNullOrEmpty(employeeSearchResult.getValues())) {
                for (Employee employee : employeeSearchResult.getValues()) {
                    Role role = roleRepository.get(company.getId(), employee.getRoleId());
                    List<String> employeeShopList = new ArrayList<>();
                    if (role.getName() != null && role.getName().equalsIgnoreCase("Admin")) {

                        //Collect shops for employee accessible shops
                        if (!CollectionUtils.isNullOrEmpty(shopSearchResult.getValues())) ;
                        for (Shop shop : shopSearchResult.getValues()) {
                            employeeShopList.add(shop.getId());
                        }

                        //Add app access list in employee
                        appAccessList.clear();
                        appAccessList.add(CompanyFeatures.AppTarget.AuthenticationApp);
                        appAccessList.add(CompanyFeatures.AppTarget.Retail);
                        appAccessList.add(CompanyFeatures.AppTarget.Distribution);
                        employee.setAppAccessList(appAccessList);
                        objectIdsForAuthenticationAppAccess.add(new ObjectId(employee.getId()));
                        objectIdsForRetailAppAccess.add(new ObjectId(employee.getId()));
                        objectIdsForDistributionAppAccess.add(new ObjectId(employee.getId()));

                        //Update all shops to admin employee
                        employeeRepository.setEmployeesShops(company.getId(), employee.getId(), employeeShopList);

                    } else {
                        //Add app access list for non-admin role employee
                        appAccessList.clear();
                        for (Shop shop : shopSearchResult.getValues()) {
                            if (employee.getShops().contains(shop.getId())) {
                                appAccessList.add(shop.getAppTarget());
                            }
                            // All employees should have access to retail as default
                            if (shop.getAppTarget() == CompanyFeatures.AppTarget.Retail) {
                                appAccessList.add(CompanyFeatures.AppTarget.Retail);
                            }
                        }
                        appAccessList.add(CompanyFeatures.AppTarget.AuthenticationApp);
                        employee.setAppAccessList(appAccessList);


                        if (employee.getAppAccessList().contains(CompanyFeatures.AppTarget.Retail)) {
                            objectIdsForRetailAppAccess.add(new ObjectId(employee.getId()));
                        }
                        if (employee.getAppAccessList().contains(CompanyFeatures.AppTarget.Distribution)) {
                            objectIdsForDistributionAppAccess.add(new ObjectId(employee.getId()));
                        }
                        if (employee.getAppAccessList().contains(CompanyFeatures.AppTarget.AuthenticationApp)) {
                            objectIdsForAuthenticationAppAccess.add(new ObjectId(employee.getId()));
                        }

                    }

                }
                //Divide employee list into chunks and update employee
                List<List<ObjectId>> listOfEmployeeAuthAppAccess = Lists.partition(objectIdsForAuthenticationAppAccess, 1000);
                List<List<ObjectId>> listOfEmployeeRetailAppAccess = Lists.partition(objectIdsForRetailAppAccess, 1000);
                List<List<ObjectId>> listOfEmployeeDistAppAccess = Lists.partition(objectIdsForDistributionAppAccess, 1000);
                for (List<ObjectId> employeeList : listOfEmployeeAuthAppAccess) {
                    employeeRepository.bulkUpdateEmployeeWithSingleAppAccess(company.getId(), employeeList, CompanyFeatures.AppTarget.AuthenticationApp);
                }
                for (List<ObjectId> employeeList : listOfEmployeeRetailAppAccess) {
                    employeeRepository.bulkUpdateEmployeeWithSingleAppAccess(company.getId(), employeeList, CompanyFeatures.AppTarget.Retail);
                }
                for (List<ObjectId> employeeList : listOfEmployeeDistAppAccess) {
                    employeeRepository.bulkUpdateEmployeeWithSingleAppAccess(company.getId(), employeeList, CompanyFeatures.AppTarget.Distribution);
                }

            }
        }
        LOGGER.info("Ending migration for company and employee");
    }
}
