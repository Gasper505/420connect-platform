package com.fourtwenty.core.tasks;

import com.amazonaws.util.CollectionUtils;
import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.domain.models.company.CompanyFeatures;
import com.fourtwenty.core.domain.models.generic.ConnectProduct;
import com.fourtwenty.core.domain.repositories.dispensary.CompanyFeaturesRepository;
import com.fourtwenty.core.domain.repositories.dispensary.CompanyRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ConnectProductRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.google.common.collect.ImmutableMultimap;
import com.google.inject.Inject;
import io.dropwizard.servlets.tasks.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class MigrateConnectProductTask extends Task {
    private static final Logger LOGGER = LoggerFactory.getLogger(MigrateConnectProductTask.class);
    private ConnectProductRepository connectProductRepository;
    private CompanyRepository companyRepository;
    private CompanyFeaturesRepository companyFeaturesRepository;

    @Inject
    public MigrateConnectProductTask(ConnectProductRepository connectProductRepository, CompanyRepository companyRepository,
                                     CompanyFeaturesRepository companyFeaturesRepository) {
        super("migrate-connect-product-task");
        this.connectProductRepository = connectProductRepository;
        this.companyRepository = companyRepository;
        this.companyFeaturesRepository = companyFeaturesRepository;
    }

    @Override
    public void execute(ImmutableMultimap<String, String> parameters, PrintWriter output) throws Exception {
        LOGGER.info("Starting migration for connect product.");
        List<CompanyFeatures.AppTarget> availableApps = new ArrayList<>();
        availableApps.add(CompanyFeatures.AppTarget.Retail);
        availableApps.add(CompanyFeatures.AppTarget.AuthenticationApp);
        //Update old connect products for available apps.
        SearchResult<ConnectProduct> result = connectProductRepository.findItems(0, Integer.MAX_VALUE);
        if (!CollectionUtils.isNullOrEmpty(result.getValues())) {
            for (ConnectProduct connectProduct : result.getValues()) {
                connectProduct.setAvailableApps(availableApps);
                connectProductRepository.update(connectProduct.getId(), connectProduct);
            }
        }

        availableApps.clear();
        //Add connect product for distribution.
        ConnectProduct dbConnectProduct = connectProductRepository.getProductBySKU("DISTRIBUTIONP1");
        if (dbConnectProduct == null) {
            ConnectProduct connectProduct = new ConnectProduct();
            availableApps.add(CompanyFeatures.AppTarget.AuthenticationApp);
            availableApps.add(CompanyFeatures.AppTarget.Distribution);
            connectProduct.setAvailableApps(availableApps);
            connectProduct.setProductSKU("DISTRIBUTIONP1");
            connectProductRepository.save(connectProduct);
        } else {
            availableApps.add(CompanyFeatures.AppTarget.AuthenticationApp);
            availableApps.add(CompanyFeatures.AppTarget.Distribution);
            dbConnectProduct.setAvailableApps(availableApps);
            connectProductRepository.update(dbConnectProduct.getId(), dbConnectProduct);

        }

        availableApps.clear();
        //Add connect product for retail and distribution.
        ConnectProduct dbConnectProduct1 = connectProductRepository.getProductBySKU("DELIVERYSTORE_DISTRIBUTIONP1");
        if (dbConnectProduct1 == null) {
            ConnectProduct connectProduct = new ConnectProduct();
            availableApps.add(CompanyFeatures.AppTarget.AuthenticationApp);
            availableApps.add(CompanyFeatures.AppTarget.Distribution);
            availableApps.add(CompanyFeatures.AppTarget.Retail);
            connectProduct.setAvailableApps(availableApps);
            connectProduct.setProductSKU("DELIVERYSTORE_DISTRIBUTIONP1");
            connectProductRepository.save(connectProduct);
        } else {
            availableApps.add(CompanyFeatures.AppTarget.AuthenticationApp);
            availableApps.add(CompanyFeatures.AppTarget.Distribution);
            availableApps.add(CompanyFeatures.AppTarget.Retail);
            dbConnectProduct1.setAvailableApps(availableApps);
            connectProductRepository.update(dbConnectProduct1.getId(), dbConnectProduct1);
        }

        //Update company feature's available apps for every company.
        SearchResult<Company> searchResult = companyRepository.findItems(0, Integer.MAX_VALUE);
        for (Company company : searchResult.getValues()) {
            CompanyFeatures companyFeatures = companyFeaturesRepository.getCompanyFeature(company.getId());
            ConnectProduct connectProduct = connectProductRepository.getProductBySKU(company.getProductSKU());
            companyFeatures.setAvailableApps(connectProduct.getAvailableApps());
            companyFeaturesRepository.update(companyFeatures.getId(), companyFeatures);

        }
        LOGGER.info("Ending migration for connect product.");

    }
}
