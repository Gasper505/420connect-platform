package com.fourtwenty.core.tasks;

import com.fourtwenty.core.domain.models.store.ConsumerCart;
import com.fourtwenty.core.domain.repositories.store.ConsumerCartRepository;
import com.google.common.collect.ImmutableMultimap;
import io.dropwizard.servlets.tasks.Task;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.inject.Inject;
import java.io.PrintWriter;
import java.util.UUID;

/**
 * Created by mdo on 6/30/17.
 */
public class MigrateConsumerCartTask extends Task {
    private static final Log LOG = LogFactory.getLog(MigrateConsumerCartTask.class);

    @Inject
    ConsumerCartRepository consumerCartRepository;


    public MigrateConsumerCartTask() {
        super("migrate-consumercarts");
    }

    @Override
    public void execute(ImmutableMultimap<String, String> parameters, PrintWriter output) throws Exception {

        Iterable<ConsumerCart> carts = consumerCartRepository.getConsumerCartsWithoutKey();
        int migrationCount = 0;
        for (ConsumerCart consumerCart : carts) {
            if (StringUtils.isBlank(consumerCart.getPublicKey())) {
                consumerCart.setPublicKey(UUID.randomUUID().toString());
                migrationCount++;
                consumerCartRepository.update(consumerCart.getId(), consumerCart);
            }
        }
        LOG.info("Migrated Consumer Carts: " + migrationCount);
    }
}
