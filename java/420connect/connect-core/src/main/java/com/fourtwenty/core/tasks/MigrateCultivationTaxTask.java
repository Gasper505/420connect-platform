package com.fourtwenty.core.tasks;

import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.models.company.CultivationTaxInfo;
import com.fourtwenty.core.domain.repositories.dispensary.CultivationTaxInfoRepository;
import com.google.common.collect.ImmutableMultimap;
import com.google.inject.Inject;
import io.dropwizard.servlets.tasks.Task;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jongo.MongoCollection;

import java.io.PrintWriter;
import java.math.BigDecimal;

public class MigrateCultivationTaxTask extends Task {
    private static final Log LOG = LogFactory.getLog(MigrateCultivationTaxTask.class);
    private CultivationTaxInfoRepository cultivationTaxInfoRepository;
    private MongoDb mongoDb;

    @Inject
    public MigrateCultivationTaxTask(CultivationTaxInfoRepository cultivationTaxInfoRepository, MongoDb mongoDb) {
        super("migrate-cultivation-tax-task");
        this.cultivationTaxInfoRepository = cultivationTaxInfoRepository;
        this.mongoDb = mongoDb;
    }

    @Override
    public void execute(ImmutableMultimap<String, String> parameters, PrintWriter output) throws Exception {
        // TODO: Need to remove this when index json will be implemented.
        CollectionName collectionName = CultivationTaxInfo.class.getAnnotation(CollectionName.class);
        MongoCollection mongoCollection = mongoDb.getJongoCollection(collectionName.name());
        // check for indexes
        String[] indexes = collectionName.indexes();
        for (String index : indexes) {
            mongoCollection.ensureIndex(index);
        }

        String[] uniqueIndexes = collectionName.uniqueIndexes();
        for (String index : uniqueIndexes) {
            mongoCollection.ensureIndex(index, "{unique:true}");
        }
        CultivationTaxInfo cultivationTaxInfos = cultivationTaxInfoRepository.getCultivationTaxInfoByState("CA", "US");
        if (cultivationTaxInfos == null) {
            LOG.info("Starting migration for cultivation tax");
            CultivationTaxInfo cultivationTaxInfo = new CultivationTaxInfo();
            cultivationTaxInfo.prepare();
            cultivationTaxInfo.setCountry("US");
            cultivationTaxInfo.setState("CA");
            cultivationTaxInfo.setFlowersTax(BigDecimal.valueOf(9.25));
            cultivationTaxInfo.setLeavesTax(BigDecimal.valueOf(2.75));
            cultivationTaxInfo.setPlantTax(BigDecimal.valueOf(1.29));
            cultivationTaxInfoRepository.save(cultivationTaxInfo);
            LOG.info("Ending migration for cultivation tax");
        } else {
            LOG.info("Starting migration for cultivation tax");
            cultivationTaxInfos.setPlantTax(BigDecimal.valueOf(1.29));
            cultivationTaxInfoRepository.update(cultivationTaxInfos.getId(),cultivationTaxInfos);
        }

    }
}
