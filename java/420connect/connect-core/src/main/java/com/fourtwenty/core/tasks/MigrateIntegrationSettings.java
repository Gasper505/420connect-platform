package com.fourtwenty.core.tasks;

import com.fourtwenty.core.domain.models.common.IntegrationSetting;
import com.fourtwenty.core.domain.models.common.IntegrationSettingConstants;
import com.fourtwenty.core.domain.repositories.common.IntegrationSettingRepository;
import com.google.common.collect.ImmutableMultimap;
import io.dropwizard.servlets.tasks.Task;

import javax.inject.Inject;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class MigrateIntegrationSettings extends Task {

    @Inject
    private IntegrationSettingRepository integrationSettingRepository;

    public MigrateIntegrationSettings() {
        super("migrate-integration-settings");
    }

    @Override
    public void execute(ImmutableMultimap<String, String> parameters, PrintWriter output) throws Exception {
        migrateHeadsetSettings();
        migrateMetricSettings();
    }

    private void migrateHeadsetSettings() {
        List<IntegrationSetting> settings = new ArrayList<>();
        settings.add(new IntegrationSetting(
                IntegrationSettingConstants.Integrations.Headset.NAME,
                IntegrationSetting.Environment.Development,
                IntegrationSettingConstants.Integrations.Headset.SYNC_INTERVAL,
                "10"));

        settings.add(new IntegrationSetting(
                IntegrationSettingConstants.Integrations.Headset.NAME,
                IntegrationSetting.Environment.Development,
                IntegrationSettingConstants.Integrations.Headset.HOST,
                "https://gateway.headset.io"));

        settings.add(new IntegrationSetting(
                IntegrationSettingConstants.Integrations.Headset.NAME,
                IntegrationSetting.Environment.Development,
                IntegrationSettingConstants.Integrations.Headset.PARTNER,
                "blaze"));

        settings.add(new IntegrationSetting(
                IntegrationSettingConstants.Integrations.Headset.NAME,
                IntegrationSetting.Environment.Development,
                IntegrationSettingConstants.Integrations.Headset.PARTNER_KEY,
                "8FCE46D2-C3AE-4DDD-9D3F-3AD91EF039A8"));

        integrationSettingRepository.save(settings);
    }

    private void migrateMetricSettings() {
        List<IntegrationSetting> settings = new ArrayList<>();
        settings.addAll(buildMetricAlaskaSettings());
        settings.addAll(buildMetricCaliforniaSettings());
        settings.addAll(buildMetricColoradoSettings());
        settings.addAll(buildMetricMarylandSettings());
        settings.addAll(buildMetricMichiganSettings());
        settings.addAll(buildMetricNevadaSettings());
        settings.addAll(buildMetricOregonSettings());
        integrationSettingRepository.save(settings);
    }

    private List<IntegrationSetting> buildMetricAlaskaSettings() {
        List<IntegrationSetting> settings = new ArrayList<>();

        String integrationName = IntegrationSettingConstants.Integrations.Metrc
                .makeName(IntegrationSettingConstants.Integrations.Metrc.MetrcState.Alaska);

        // Dev
        settings.add(new IntegrationSetting(
                integrationName,
                IntegrationSetting.Environment.Development,
                IntegrationSettingConstants.Integrations.Metrc.HOST,
                "https://sandbox-api-ak.metrc.com"));

        settings.add(new IntegrationSetting(
                integrationName,
                IntegrationSetting.Environment.Development,
                IntegrationSettingConstants.Integrations.Metrc.VENDOR_KEY,
                "2a0cESqN7EIFGuWHddGYvCpxHhk-aVMsqkXpWQvX7Mclfx4Q"));

        // Prod
        settings.add(new IntegrationSetting(
                integrationName,
                IntegrationSetting.Environment.Production,
                IntegrationSettingConstants.Integrations.Metrc.HOST,
                "https://api-ak.metrc.com"));

        settings.add(new IntegrationSetting(
                integrationName,
                IntegrationSetting.Environment.Production,
                IntegrationSettingConstants.Integrations.Metrc.VENDOR_KEY,
                "2a0cESqN7EIFGuWHddGYvCpxHhk-aVMsqkXpWQvX7Mclfx4Q"));

        integrationSettingRepository.save(settings);
        return settings;
    }

    private List<IntegrationSetting> buildMetricCaliforniaSettings() {
        List<IntegrationSetting> settings = new ArrayList<>();

        String integrationName = IntegrationSettingConstants.Integrations.Metrc
                .makeName(IntegrationSettingConstants.Integrations.Metrc.MetrcState.California);

        // Dev
        settings.add(new IntegrationSetting(
                integrationName,
                IntegrationSetting.Environment.Development,
                IntegrationSettingConstants.Integrations.Metrc.HOST,
                "https://sandbox-api-ca.metrc.com"));

        settings.add(new IntegrationSetting(
                integrationName,
                IntegrationSetting.Environment.Development,
                IntegrationSettingConstants.Integrations.Metrc.VENDOR_KEY,
                "9KT4ofNlRe36Nwbcb56JyigjdT8l3dFPpbgG1f0aP4LzynGd"));

        // Prod
        settings.add(new IntegrationSetting(
                integrationName,
                IntegrationSetting.Environment.Production,
                IntegrationSettingConstants.Integrations.Metrc.HOST,
                "https://api-ca.metrc.com"));

        settings.add(new IntegrationSetting(
                integrationName,
                IntegrationSetting.Environment.Production,
                IntegrationSettingConstants.Integrations.Metrc.VENDOR_KEY,
                "629uhiFEIFSC94qQA-L-Dku-a7IOdlXOC4WLoF7dUg3Dv9YA"));

        integrationSettingRepository.save(settings);
        return settings;
    }

    private List<IntegrationSetting> buildMetricColoradoSettings() {
        List<IntegrationSetting> settings = new ArrayList<>();

        String integrationName = IntegrationSettingConstants.Integrations.Metrc
                .makeName(IntegrationSettingConstants.Integrations.Metrc.MetrcState.Colorado);

        // Dev
        settings.add(new IntegrationSetting(
                integrationName,
                IntegrationSetting.Environment.Development,
                IntegrationSettingConstants.Integrations.Metrc.HOST,
                "https://sandbox-api-co.metrc.com"));

        settings.add(new IntegrationSetting(
                integrationName,
                IntegrationSetting.Environment.Development,
                IntegrationSettingConstants.Integrations.Metrc.VENDOR_KEY,
                "uRMSKuL0P1NaPkaYjykU5q0u1rcp7UG6yu02EqKoXgfptABW"));

        // Prod
        settings.add(new IntegrationSetting(
                integrationName,
                IntegrationSetting.Environment.Production,
                IntegrationSettingConstants.Integrations.Metrc.HOST,
                "https://api-co.metrc.com"));

        settings.add(new IntegrationSetting(
                integrationName,
                IntegrationSetting.Environment.Production,
                IntegrationSettingConstants.Integrations.Metrc.VENDOR_KEY,
                "t1rN1mssKdUyCxLucLQ0yrUv4gMulzNrl04GGveT-YcQtF-7"));

        integrationSettingRepository.save(settings);
        return settings;
    }

    private List<IntegrationSetting> buildMetricMarylandSettings() {
        List<IntegrationSetting> settings = new ArrayList<>();

        String integrationName = IntegrationSettingConstants.Integrations.Metrc
                .makeName(IntegrationSettingConstants.Integrations.Metrc.MetrcState.Maryland);

        // Dev
        settings.add(new IntegrationSetting(
                integrationName,
                IntegrationSetting.Environment.Development,
                IntegrationSettingConstants.Integrations.Metrc.HOST,
                "https://sandbox-api-md.metrc.com"));

        settings.add(new IntegrationSetting(
                integrationName,
                IntegrationSetting.Environment.Development,
                IntegrationSettingConstants.Integrations.Metrc.VENDOR_KEY,
                ""));

        // Prod
        settings.add(new IntegrationSetting(
                integrationName,
                IntegrationSetting.Environment.Production,
                IntegrationSettingConstants.Integrations.Metrc.HOST,
                "https://api-md.metrc.com"));

        settings.add(new IntegrationSetting(
                integrationName,
                IntegrationSetting.Environment.Production,
                IntegrationSettingConstants.Integrations.Metrc.VENDOR_KEY,
                "bzKqEBE5jtZ1XDIVGdJnvu0ES8YaD36p5jxMJqHPk2pRPdkq"));

        integrationSettingRepository.save(settings);
        return settings;
    }

    private List<IntegrationSetting> buildMetricMichiganSettings() {
        List<IntegrationSetting> settings = new ArrayList<>();

        String integrationName = IntegrationSettingConstants.Integrations.Metrc
                .makeName(IntegrationSettingConstants.Integrations.Metrc.MetrcState.Michigan);

        // Dev
        settings.add(new IntegrationSetting(
                integrationName,
                IntegrationSetting.Environment.Development,
                IntegrationSettingConstants.Integrations.Metrc.HOST,
                "https://sandbox-api-mi.metrc.com"));

        settings.add(new IntegrationSetting(
                integrationName,
                IntegrationSetting.Environment.Development,
                IntegrationSettingConstants.Integrations.Metrc.VENDOR_KEY,
                ""));

        // Prod
        settings.add(new IntegrationSetting(
                integrationName,
                IntegrationSetting.Environment.Production,
                IntegrationSettingConstants.Integrations.Metrc.HOST,
                "https://api-mi.metrc.com"));

        settings.add(new IntegrationSetting(
                integrationName,
                IntegrationSetting.Environment.Production,
                IntegrationSettingConstants.Integrations.Metrc.VENDOR_KEY,
                "OeSV2SNOumk2WV2RnuubU-XW0Y4tMOqowrhlMn2tDz2LFmrk"));

        integrationSettingRepository.save(settings);
        return settings;
    }

    private List<IntegrationSetting> buildMetricNevadaSettings() {
        List<IntegrationSetting> settings = new ArrayList<>();

        String integrationName = IntegrationSettingConstants.Integrations.Metrc
                .makeName(IntegrationSettingConstants.Integrations.Metrc.MetrcState.Nevada);

        // Dev
        settings.add(new IntegrationSetting(
                integrationName,
                IntegrationSetting.Environment.Development,
                IntegrationSettingConstants.Integrations.Metrc.HOST,
                "https://sandbox-api-nv.metrc.com"));

        settings.add(new IntegrationSetting(
                integrationName,
                IntegrationSetting.Environment.Development,
                IntegrationSettingConstants.Integrations.Metrc.VENDOR_KEY,
                ""));

        // Prod
        settings.add(new IntegrationSetting(
                integrationName,
                IntegrationSetting.Environment.Production,
                IntegrationSettingConstants.Integrations.Metrc.HOST,
                "https://api-nv.metrc.com"));

        settings.add(new IntegrationSetting(
                integrationName,
                IntegrationSetting.Environment.Production,
                IntegrationSettingConstants.Integrations.Metrc.VENDOR_KEY,
                "eukRzL-AOrvTiNrjXFeRtYBHDzrDJ16XPMIsqbVfdE3khFj5"));

        integrationSettingRepository.save(settings);
        return settings;
    }

    private List<IntegrationSetting> buildMetricOregonSettings() {
        List<IntegrationSetting> settings = new ArrayList<>();

        String integrationName = IntegrationSettingConstants.Integrations.Metrc
                .makeName(IntegrationSettingConstants.Integrations.Metrc.MetrcState.Oregon);

        // Dev
        settings.add(new IntegrationSetting(
                integrationName,
                IntegrationSetting.Environment.Development,
                IntegrationSettingConstants.Integrations.Metrc.HOST,
                "https://sandbox-api-or.metrc.com"));

        settings.add(new IntegrationSetting(
                integrationName,
                IntegrationSetting.Environment.Development,
                IntegrationSettingConstants.Integrations.Metrc.VENDOR_KEY,
                "uRMSKuL0P1NaPkaYjykU5q0u1rcp7UG6yu02EqKoXgfptABW"));

        // Prod
        settings.add(new IntegrationSetting(
                integrationName,
                IntegrationSetting.Environment.Production,
                IntegrationSettingConstants.Integrations.Metrc.HOST,
                "https://api-or.metrc.com"));

        settings.add(new IntegrationSetting(
                integrationName,
                IntegrationSetting.Environment.Production,
                IntegrationSettingConstants.Integrations.Metrc.VENDOR_KEY,
                "BOv1DUMApQdJzir2mM07lX6mwo5xk-AWbC1-un17MQxUe9Q0"));

        integrationSettingRepository.save(settings);
        return settings;
    }
}

