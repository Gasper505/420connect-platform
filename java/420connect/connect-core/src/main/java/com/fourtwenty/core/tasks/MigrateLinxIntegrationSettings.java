package com.fourtwenty.core.tasks;

import com.fourtwenty.core.domain.models.common.IntegrationSetting;
import com.fourtwenty.core.domain.models.common.IntegrationSettingConstants;
import com.fourtwenty.core.domain.repositories.common.IntegrationSettingRepository;
import com.google.common.collect.ImmutableMultimap;
import io.dropwizard.servlets.tasks.Task;

import javax.inject.Inject;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class MigrateLinxIntegrationSettings extends Task {

    @Inject
    private IntegrationSettingRepository integrationSettingRepository;

    public MigrateLinxIntegrationSettings() {
        super("migrate-integration-settings");
    }

    @Override
    public void execute(ImmutableMultimap<String, String> parameters, PrintWriter output) throws Exception {
        migrateLinxSettings();
    }

    private void migrateLinxSettings() {
        List<IntegrationSetting> settings = new ArrayList<>();

        long count = integrationSettingRepository.count();
        if (count == 0) {
            settings.add(new IntegrationSetting(
                    IntegrationSettingConstants.Integrations.Linx.NAME,
                    IntegrationSetting.Environment.Development,
                    IntegrationSettingConstants.Integrations.Linx.HOST,
                    "https://staging.linxkiosk.com"));

            settings.add(new IntegrationSetting(
                    IntegrationSettingConstants.Integrations.Linx.NAME,
                    IntegrationSetting.Environment.Production,
                    IntegrationSettingConstants.Integrations.Linx.HOST,
                    "https://cms.linxkiosk.com"));


            integrationSettingRepository.save(settings);
        }
    }
}

