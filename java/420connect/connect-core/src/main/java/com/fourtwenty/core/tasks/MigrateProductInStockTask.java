package com.fourtwenty.core.tasks;

import com.fourtwenty.core.domain.models.product.ProductPrepackageQuantity;
import com.fourtwenty.core.domain.repositories.dispensary.ProductPrepackageQuantityRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.google.common.collect.ImmutableMultimap;
import io.dropwizard.servlets.tasks.Task;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bson.types.ObjectId;
import org.jongo.MongoCollection;

import javax.inject.Inject;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

public class MigrateProductInStockTask extends Task {
    private static final Log LOG = LogFactory.getLog(MigrateProductInStockTask.class);
    @Inject
    ProductRepository productRepository;
    @Inject
    ProductPrepackageQuantityRepository productPrepackageQuantityRepository;

    public MigrateProductInStockTask() {
        super("migrate-product-instock");
    }

    @Override
    public void execute(ImmutableMultimap<String, String> parameters, PrintWriter output) throws Exception {

        LOG.info("Beginning InStock migration");
        MongoCollection productCollection = productRepository.getMongoCollection();
        productCollection.update("{quantities.quantity: {$gt: 0}}").multi().with("{$set:{instock:true}}");

        MongoCollection prepackQtyCollection = productPrepackageQuantityRepository.getMongoCollection();
        Iterable<ProductPrepackageQuantity> productPrepackageQuantities = prepackQtyCollection.find("{quantity: {$gt: 0}}").as(ProductPrepackageQuantity.class);
        LinkedHashSet<String> productIds = new LinkedHashSet<>();
        productPrepackageQuantities.forEach((qty) -> {
            if (!qty.isDeleted() && qty.getQuantity() > 0) {
                productIds.add(qty.getProductId());
            }
        });


        List<ObjectId> ids = new ArrayList<>();
        for (String id : productIds) {
            if (id != null && ObjectId.isValid(id)) {
                ids.add(new ObjectId(id));
            }
        }
        productCollection.update("{_id:{$in:#}}",ids).multi().with("{$set:{instock:true}}");


        LOG.info("Completed InStock migration");
    }
}
