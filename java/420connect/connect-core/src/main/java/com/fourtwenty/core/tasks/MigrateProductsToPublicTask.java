package com.fourtwenty.core.tasks;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.CopyObjectResult;
import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.models.generic.Asset;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.domain.repositories.dispensary.CompanyAssetRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductCategoryRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.util.AmazonServiceFactory;
import com.google.common.collect.ImmutableMultimap;
import com.google.inject.Inject;
import io.dropwizard.servlets.tasks.Task;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.UUID;

/**
 * Created by mdo on 2/21/17.
 */
public class MigrateProductsToPublicTask extends Task {
    @Inject
    CompanyAssetRepository companyAssetRepository;
    @Inject
    ProductCategoryRepository categoryRepository;
    @Inject
    ProductRepository productRepository;
    @Inject
    private AmazonServiceFactory amazonS3Util;

    private static final Log LOG = LogFactory.getLog(MigrateProductsToPublicTask.class);

    public MigrateProductsToPublicTask() {
        super("migrate-asset");
    }

    @Override
    public void execute(ImmutableMultimap<String, String> parameters, PrintWriter output) throws Exception {
        migrateProductsToPublicBucket();
    }


    private void migrateProductsToPublicBucket() {
        HashMap<String, CompanyAsset> assetHashMap = companyAssetRepository.listAsMap();
        Iterable<Product> products = productRepository.iterator();
        Iterable<ProductCategory> categories = categoryRepository.iterator();

        int categoryPhotosUpdated = 0;
        int categoryPhotosErrors = 0;
        int productPhotosUpdated = 0;
        int productPhotosErrors = 0;
        for (ProductCategory category : categories) {
            CompanyAsset photo = category.getPhoto();
            if (photo != null) {
                CompanyAsset dbPhoto = assetHashMap.get(photo.getId());
                if (dbPhoto != null) {


                    String ext2 = getExtension(photo.getAssetType(), photo.getKey());

                    if (StringUtils.isBlank(photo.getPublicURL())) {
                        String fromBucket = amazonS3Util.getS3Config().getBucketName();
                        if (StringUtils.isNotBlank(photo.getPublicURL())) {
                            // this item was previously in the public bucket so use that bucket isntead
                            fromBucket = amazonS3Util.getS3Config().getPublicBucketName();
                        }
                        // Only do this if it's a different extension or publicURL is null
                        String newKey = UUID.randomUUID().toString() + ext2;
                        String path = copyObjectToPublic(photo.getKey(), fromBucket, newKey, amazonS3Util.getS3Config().getPublicBucketName());
                        if (path == null) {
                            // try with the public key
                            path = copyObjectToPublic(photo.getKey(), amazonS3Util.getS3Config().getPublicBucketName(), newKey, amazonS3Util.getS3Config().getPublicBucketName());
                        }
                        if (path == null) {
                            // try with the public key
                            path = copyObjectToPublic(photo.getKey(), "connect-files-d1-pub", newKey, amazonS3Util.getS3Config().getPublicBucketName());

                        }
                        if (path != null) {
                            dbPhoto.setKey(newKey);
                            dbPhoto.setPublicURL(path);
                            dbPhoto.setSecured(false);
                            companyAssetRepository.update(dbPhoto.getCompanyId(), dbPhoto.getId(), dbPhoto);
                            category.setPhoto(dbPhoto);
                            categoryRepository.update(category.getCompanyId(), category.getId(), category);
                            categoryPhotosUpdated++;
                        } else {
                            LOG.info("Error moving category: " + category.getName());
                            categoryPhotosErrors++;
                        }
                    } else if (StringUtils.isNotBlank(dbPhoto.getPublicURL())) {
                        // Move files
                        // same ext and with a valid public url, let's make sure the key are the same
                        String newKey = FilenameUtils.getName(photo.getPublicURL());
                        if (!photo.getKey().equalsIgnoreCase(newKey)) {
                            dbPhoto.setKey(newKey);
                            dbPhoto.setPublicURL(photo.getPublicURL());
                            dbPhoto.setSecured(false);
                            companyAssetRepository.update(dbPhoto.getCompanyId(), dbPhoto.getId(), dbPhoto);
                            category.setPhoto(dbPhoto);
                            categoryRepository.update(category.getCompanyId(), category.getId(), category);
                            categoryPhotosUpdated++;
                        }
                    }

                } else {
                    String ext2 = getExtension(photo.getAssetType(), photo.getKey());

                    if (StringUtils.isBlank(photo.getPublicURL())) {
                        String fromBucket = amazonS3Util.getS3Config().getBucketName();
                        if (StringUtils.isNotBlank(photo.getPublicURL())) {
                            // this item was previously in the public bucket so use that bucket isntead
                            fromBucket = amazonS3Util.getS3Config().getPublicBucketName();
                        }
                        String newKey = UUID.randomUUID().toString() + ext2;
                        String path = copyObjectToPublic(photo.getKey(), fromBucket, newKey, amazonS3Util.getS3Config().getPublicBucketName());
                        if (path == null) {
                            // try with the public key
                            path = copyObjectToPublic(photo.getKey(), amazonS3Util.getS3Config().getPublicBucketName(), newKey, amazonS3Util.getS3Config().getPublicBucketName());
                        }
                        if (path == null) {
                            // try with the public key
                            path = copyObjectToPublic(photo.getKey(), "connect-files-d1-pub", newKey, amazonS3Util.getS3Config().getPublicBucketName());

                        }
                        if (path != null) {
                            photo.setPublicURL(path);
                            photo.setKey(newKey);
                            photo.setSecured(false);
                            category.setPhoto(photo);
                            categoryRepository.update(category.getCompanyId(), category.getId(), category);
                            categoryPhotosUpdated++;
                        } else {
                            categoryPhotosErrors++;
                        }
                    }

                }
            }
        }

        for (Product product : products) {
            boolean isUpdated = false;
            for (CompanyAsset asset : product.getAssets()) {
                CompanyAsset dbPhoto = assetHashMap.get(asset.getId());
                if (dbPhoto != null) {

                    String ext2 = getExtension(asset.getAssetType(), asset.getKey());

                    if (StringUtils.isBlank(asset.getPublicURL())) {
                        LOG.info("Asset need to be public: " + product.getName() + "    " + asset.getName());
                        String newKey = UUID.randomUUID().toString() + ext2;

                        String fromBucket = amazonS3Util.getS3Config().getBucketName();
                        if (StringUtils.isNotBlank(asset.getPublicURL())) {
                            // this item was previously in the public bucket so use that bucket isntead
                            fromBucket = amazonS3Util.getS3Config().getPublicBucketName();
                        }

                        String path = copyObjectToPublic(asset.getKey(), fromBucket, newKey, amazonS3Util.getS3Config().getPublicBucketName());

                        if (path == null) {
                            // try with the public key
                            path = copyObjectToPublic(asset.getKey(), amazonS3Util.getS3Config().getPublicBucketName(), newKey, amazonS3Util.getS3Config().getPublicBucketName());
                        }

                        if (path != null) {
                            LOG.info("Public path: " + path);
                            dbPhoto.setPublicURL(path);
                            dbPhoto.setSecured(false);
                            dbPhoto.setKey(newKey);
                            companyAssetRepository.update(dbPhoto.getCompanyId(), dbPhoto.getId(), dbPhoto);

                            asset.setKey(newKey);
                            asset.setSecured(false);
                            asset.setPublicURL(path);
                            asset.setSecured(false);
                            isUpdated = true;
                            productPhotosUpdated++;
                        } else {
                            productPhotosErrors++;
                        }
                    } else if (StringUtils.isNotBlank(asset.getPublicURL())) {
                        // same ext and with a valid public url, let's make sure the key are the same
                        String newKey = FilenameUtils.getName(asset.getPublicURL());
                        if (!asset.getKey().equalsIgnoreCase(newKey)) {
                            asset.setKey(newKey);
                            isUpdated = true;

                            dbPhoto.setKey(newKey);
                            companyAssetRepository.update(dbPhoto.getCompanyId(), dbPhoto.getId(), dbPhoto);
                        }
                    }
                } else {
                    LOG.info("Image Asset is null: " + product.getName());
                }
            }
            if (isUpdated) {
                productRepository.updateCachedAssets(product.getCompanyId(), product.getId(), product.getAssets());
                productPhotosUpdated++;
            }
        }
        LOG.info("Category Photos Updated: " + categoryPhotosUpdated);
        LOG.info("Category Photos Errors: " + categoryPhotosErrors);
        LOG.info("Product Photos Updated: " + productPhotosUpdated);
        LOG.info("Product Photos Errors: " + productPhotosErrors);
    }

    private boolean isSameExt(Asset.AssetType assetType, String name1, String name2) {
        String ext1 = getExtension(assetType, name1);
        String ext2 = getExtension(assetType, name2);
        return ext1.equalsIgnoreCase(ext2);
    }

    private String getExtension(Asset.AssetType assetType, String name) {
        String ext1 = FilenameUtils.getExtension(name); // returns "exe"
        if (StringUtils.isBlank(ext1)) {
            ext1 = assetType == Asset.AssetType.Document ? ".pdf" : ".jpeg";
        } else {
            ext1 = "." + ext1;
        }
        return ext1;
    }

    private String copyObjectToPublic(String key, String oldBucket, String newKey, String targetBucket) {

        if (key != null && key.startsWith("420default-")) {
            return "https://s3.amazonaws.com/connect-files-public/" + key;
        }

        AmazonS3 s3client = amazonS3Util.getS3Client();
        try {
            CopyObjectResult result = s3client.copyObject(oldBucket, key, targetBucket, newKey);
            return String.format("https://%s.s3.amazonaws.com/%s", targetBucket, newKey);
        } catch (Exception e) {
            LOG.error(e.getMessage() + "bucket: " + oldBucket + " key: " + key, e);
        }
        return null;
    }
}
