package com.fourtwenty.core.tasks;

import com.fourtwenty.core.domain.repositories.dispensary.ProductBatchRepository;
import com.google.common.collect.ImmutableMultimap;
import com.google.inject.Inject;
import io.dropwizard.servlets.tasks.Task;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.PrintWriter;

/**
 * Created by mdo on 9/29/17.
 */
public class ProductBatchSetArchivedTask extends Task {
    private static final Log LOG = LogFactory.getLog(ProductBatchSetArchivedTask.class);
    @Inject
    ProductBatchRepository productBatchRepository;


    public ProductBatchSetArchivedTask() {
        super("batch-archived");
    }

    @Override
    public void execute(ImmutableMultimap<String, String> parameters, PrintWriter output) throws Exception {
        int numUpdated = productBatchRepository.setProductBatchArchived();
        LOG.info("Updated ProductBatch archived update: " + numUpdated);
    }
}
