package com.fourtwenty.core.tasks;

import com.fourtwenty.core.domain.models.thirdparty.ProductTagGroups;
import com.fourtwenty.core.domain.models.thirdparty.WmProductMapping;
import com.fourtwenty.core.domain.models.thirdparty.WmTagGroups;
import com.fourtwenty.core.domain.repositories.thirdparty.WmMappingRepository;
import com.fourtwenty.core.domain.repositories.thirdparty.WmTagGroupRepository;
import com.google.common.collect.ImmutableMultimap;
import io.dropwizard.servlets.tasks.Task;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.assertj.core.util.Lists;
import org.bson.types.ObjectId;

import javax.inject.Inject;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ProductMappingMigrationTask extends Task {

    @Inject
    private WmMappingRepository mappingRepository;
    @Inject
    private WmTagGroupRepository tagGroupRepository;

    public ProductMappingMigrationTask() {
        super("migrate-product-mapping-task");
    }

    /**
     * Executes the task.
     *
     * @param parameters the query string parameters
     * @param output     a {@link PrintWriter} wrapping the output stream of the task
     * @throws Exception if something goes wrong
     */
    @Override
    public void execute(ImmutableMultimap<String, String> parameters, PrintWriter output) throws Exception {
        List<WmProductMapping> productMappings = mappingRepository.list();

        Set<ObjectId> tagGroups = new HashSet<>();

        for (WmProductMapping productMapping : productMappings) {
            if (StringUtils.isNotBlank(productMapping.getWmTagGroup()) && ObjectId.isValid(productMapping.getWmTagGroup())) {
                tagGroups.add(new ObjectId(productMapping.getWmTagGroup()));
            }
        }

        HashMap<String, WmTagGroups> wmTagGroupsMap = tagGroupRepository.findItemsInAsMap(Lists.newArrayList(tagGroups));

        HashMap<String, List<ObjectId>> tagsGroupMapping = new HashMap<>();
        HashMap<String, String> discoveryTagsMapping = new HashMap<>();

        for (WmProductMapping productMapping : productMappings) {
            WmTagGroups wmTagGroup = wmTagGroupsMap.get(productMapping.getWmTagGroup());

            if (wmTagGroup == null) {
                continue;
            }

            if (CollectionUtils.isNotEmpty(wmTagGroup.getDiscoveryTags())) {
                for (WmTagGroups.WmDiscoveryTags discoveryTags : wmTagGroup.getDiscoveryTags()) {
                    if (discoveryTags.getId().equalsIgnoreCase(productMapping.getWmDiscoveryTag())) {
                        String key = discoveryTags.getId();
                        tagsGroupMapping.putIfAbsent(key, Lists.newArrayList());

                        List<ObjectId> mappingIds = tagsGroupMapping.get(key);

                        mappingIds.add(new ObjectId(productMapping.getId()));

                        discoveryTagsMapping.putIfAbsent(key, wmTagGroup.getId());

                        break;
                    }
                }
            }
        }

        for (Map.Entry<String, String> entry : discoveryTagsMapping.entrySet()) {
            String discoveryTagId = entry.getKey();
            String tagGroupId = entry.getValue();
            WmTagGroups wmTagGroup = wmTagGroupsMap.get(tagGroupId);

            if (wmTagGroup == null) {
                continue;
            }

            List<ObjectId> mappingIds = tagsGroupMapping.get(discoveryTagId);

            if (CollectionUtils.isEmpty(mappingIds)) {
                continue;
            }
            ProductTagGroups productTagGroup = new ProductTagGroups();
            productTagGroup.setWmTagGroup(wmTagGroup.getTagGroupId());
            productTagGroup.setWmTagGroupName(wmTagGroup.getTagGroupName());

            WmTagGroups.WmDiscoveryTags discoveryTag = null;
            if (CollectionUtils.isNotEmpty(wmTagGroup.getDiscoveryTags())) {
                for (WmTagGroups.WmDiscoveryTags discoveryTags : wmTagGroup.getDiscoveryTags()) {
                    if (discoveryTags.getId().equalsIgnoreCase(discoveryTagId)) {
                        discoveryTag = discoveryTags;
                        break;
                    }
                }
            }

            if (discoveryTag != null) {
                productTagGroup.setWmDiscoveryTag(discoveryTag.getTagId());
                productTagGroup.setWmDiscoveryTagName(discoveryTag.getTagName());
            }

            mappingRepository.updateTagGroups(mappingIds, Lists.newArrayList(productTagGroup));

        }

    }
}
