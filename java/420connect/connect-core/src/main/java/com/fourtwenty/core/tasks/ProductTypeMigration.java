package com.fourtwenty.core.tasks;

import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.google.common.collect.ImmutableMultimap;
import io.dropwizard.servlets.tasks.Task;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.inject.Inject;
import java.io.PrintWriter;
import java.util.List;

public class ProductTypeMigration extends Task {
    private static final Log LOG = LogFactory.getLog(ProductTypeMigration.class);
    @Inject
    ProductRepository productRepository;

    protected ProductTypeMigration() {
        super("product-type-migration");
    }

    @Override
    public void execute(ImmutableMultimap<String, String> parameters, PrintWriter output) throws Exception {
        /* get Products List */
        List<Product> productList = productRepository.listNonProductType();
        int fixed = 0;

        /* Iterate products and define `productType` */
        for (Product product : productList) {

            if (product.isMedicinal()) {
                product.setProductSaleType(Product.ProductSaleType.Medicinal);
            } else {
                product.setProductSaleType(Product.ProductSaleType.Recreational);
            }

            //productRepository.update(product.getId(),product);
            fixed++;
        }
        LOG.info("Products Updated: " + fixed);
    }
}
