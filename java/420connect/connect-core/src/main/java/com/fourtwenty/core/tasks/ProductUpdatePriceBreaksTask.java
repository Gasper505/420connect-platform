package com.fourtwenty.core.tasks;

import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.domain.models.product.ProductPriceBreak;
import com.fourtwenty.core.domain.repositories.dispensary.ProductCategoryRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.google.common.collect.ImmutableMultimap;
import com.google.inject.Inject;
import io.dropwizard.servlets.tasks.Task;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.function.Predicate;

/**
 * Created by mdo on 7/19/17.
 */
public class ProductUpdatePriceBreaksTask extends Task {
    private static final Log LOG = LogFactory.getLog(ProductUpdatePriceBreaksTask.class);
    @Inject
    ProductRepository productRepository;
    @Inject
    ProductCategoryRepository productCategoryRepository;

    public ProductUpdatePriceBreaksTask() {
        super("product-pricebreaks-update");
    }

    @Override
    public void execute(ImmutableMultimap<String, String> parameters, PrintWriter output) throws Exception {

        Iterable<Product> products = productRepository.listNonDeleted();
        HashMap<String, ProductCategory> productCategoryHashMap = productCategoryRepository.listAsMap();

        int updated = 0;
        for (Product product : products) {
            if (product.getPriceBreaks() != null
                    && product.getPriceBreaks().size() > 0) {

                // loop through price breaks
                boolean shouldUpdate = false;
                for (ProductPriceBreak priceBreak : product.getPriceBreaks()) {

                    // only update if price isn't equal
                    if (!product.getUnitPrice().equals(priceBreak.getPrice())) {
                        if (product.getWeightPerUnit() == Product.WeightPerUnit.EACH
                                || product.getWeightPerUnit() == Product.WeightPerUnit.FULL_GRAM
                                || product.getWeightPerUnit() == Product.WeightPerUnit.EIGHTH
                                || product.getWeightPerUnit() == Product.WeightPerUnit.CUSTOM_GRAMS) {
                            // deal with one gram
                            if (priceBreak.getPriceBreakType() == ProductPriceBreak.PriceBreakType.OneGramUnit) {
                                priceBreak.setPrice(product.getUnitPrice());
                                shouldUpdate = true;
                            }
                        } else if (product.getWeightPerUnit() == Product.WeightPerUnit.HALF_GRAM) {
                            // deal with half gram
                            if (priceBreak.getPriceBreakType() == ProductPriceBreak.PriceBreakType.HalfGramUnit) {
                                priceBreak.setPrice(product.getUnitPrice());
                                shouldUpdate = true;
                            }
                        }
                    }
                }

                if (shouldUpdate) {
                    LOG.info("Update Product for break: " + product.getId());
                    productRepository.updateProductPriceBreaks(product.getCompanyId(), product.getId(), product.getPriceBreaks());
                    updated++;
                }
            } else {
                ProductCategory productCategory = productCategoryHashMap.get(product.getCategoryId());
                if (productCategory != null && productCategory.getUnitType() == ProductCategory.UnitType.units) {
                    if (product.getPriceBreaks() == null) {
                        product.setPriceBreaks(new ArrayList<ProductPriceBreak>());
                    }
                    if (product.getPriceBreaks().size() == 0) {
                        List<ProductPriceBreak> productPriceBreaks = populatePriceBreaks(product, productCategory, product.getPriceBreaks(), true);

                        LOG.info("Update Product for break2: " + product.getId());
                        productRepository.updateProductPriceBreaks(product.getCompanyId(), product.getId(), product.getPriceBreaks());
                        updated++;
                    }
                }
            }
        }
        LOG.info("Products with price breaks updated: " + updated);


    }

    private List<ProductPriceBreak> populatePriceBreaks(final Product product, ProductCategory category, List<ProductPriceBreak> priceBreaks, boolean isDefault) {

        // Fill in the priceBreaks or priceRanges
        if (category.getUnitType() == ProductCategory.UnitType.units) {

            final ProductPriceBreak.PriceBreakType[] priceBreakTypes = ProductPriceBreak.PriceBreakType.values();

            // Remove bad price breaks
            priceBreaks.removeIf(new Predicate<ProductPriceBreak>() {
                @Override
                public boolean test(ProductPriceBreak productPriceBreak) {

                    return (product.getWeightPerUnit() == Product.WeightPerUnit.EACH
                            || product.getWeightPerUnit() == Product.WeightPerUnit.FULL_GRAM
                            || product.getWeightPerUnit() == Product.WeightPerUnit.EIGHTH
                            || product.getWeightPerUnit() == Product.WeightPerUnit.FOURTH
                            || product.getWeightPerUnit() == Product.WeightPerUnit.CUSTOM_GRAMS)
                            && productPriceBreak.getPriceBreakType() == ProductPriceBreak.PriceBreakType.HalfGramUnit;
                }
            });

            for (ProductPriceBreak.PriceBreakType type : priceBreakTypes) {
                if (type == ProductPriceBreak.PriceBreakType.None) {
                    continue;
                }

                if (type == ProductPriceBreak.PriceBreakType.HalfGramUnit) {
                    if (product.getWeightPerUnit() == Product.WeightPerUnit.FULL_GRAM
                            || product.getWeightPerUnit() == Product.WeightPerUnit.EACH
                            || product.getWeightPerUnit() == Product.WeightPerUnit.EIGHTH
                            || product.getWeightPerUnit() == Product.WeightPerUnit.FOURTH
                            || product.getWeightPerUnit() == Product.WeightPerUnit.CUSTOM_GRAMS) {
                        continue;
                    }
                }

                // find the price break by type
                ProductPriceBreak priceBreak = null;
                for (ProductPriceBreak productPriceBreak : priceBreaks) {
                    if (productPriceBreak.getPriceBreakType() == type) {
                        priceBreak = productPriceBreak;
                        break;
                    }
                }
                // if none is found, create a new one
                if (priceBreak == null) {
                    // Add a new one
                    priceBreak = new ProductPriceBreak();
                    priceBreak.prepare(product.getCompanyId());
                    priceBreak.setPriceBreakType(type);
                    priceBreak.setActive(false);


                    // add price break to group
                    priceBreaks.add(priceBreak);
                }

                // specify name and quantity
                if (product.getWeightPerUnit() == Product.WeightPerUnit.EACH
                        || product.getWeightPerUnit() == Product.WeightPerUnit.EIGHTH
                        || product.getWeightPerUnit() == Product.WeightPerUnit.FOURTH
                        || product.getWeightPerUnit() == Product.WeightPerUnit.CUSTOM_GRAMS) {
                    priceBreak.setName(type.eachName);
                    priceBreak.setQuantity(type.fullGramValue);
                } else if (product.getWeightPerUnit() == Product.WeightPerUnit.FULL_GRAM) {
                    priceBreak.setQuantity(type.fullGramValue);
                    priceBreak.setName(type.gramName);
                } else {
                    priceBreak.setName(type.gramName);
                    priceBreak.setQuantity(type.halfGramValue);
                }

                // default cost
                if (isDefault) {
                    if (priceBreak.getPrice() == null) {
                        priceBreak.setPrice(product.getUnitPrice().multiply(new BigDecimal(priceBreak.getQuantity())));
                    }
                    if (product.getWeightPerUnit() == Product.WeightPerUnit.EACH
                            || product.getWeightPerUnit() == Product.WeightPerUnit.FULL_GRAM
                            || product.getWeightPerUnit() == Product.WeightPerUnit.EIGHTH
                            || product.getWeightPerUnit() == Product.WeightPerUnit.FOURTH
                            || product.getWeightPerUnit() == Product.WeightPerUnit.CUSTOM_GRAMS) {
                        if (priceBreak.getPriceBreakType() == ProductPriceBreak.PriceBreakType.OneGramUnit) {
                            priceBreak.setPrice(product.getUnitPrice());
                            priceBreak.setActive(true);
                        }
                    } else {
                        if (priceBreak.getPriceBreakType() == ProductPriceBreak.PriceBreakType.HalfGramUnit) {
                            priceBreak.setPrice(product.getUnitPrice());
                            priceBreak.setActive(true);
                        }
                    }
                }
            }

            // Sort price breaks
            priceBreaks.sort(new Comparator<ProductPriceBreak>() {
                @Override
                public int compare(ProductPriceBreak o1, ProductPriceBreak o2) {
                    return ((Integer) o1.getQuantity()).compareTo((Integer) o2.getQuantity());
                }
            });
        }

        return priceBreaks;
    }
}
