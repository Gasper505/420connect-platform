package com.fourtwenty.core.tasks;

import com.fourtwenty.core.domain.repositories.dispensary.MemberGroupRepository;
import com.fourtwenty.core.domain.repositories.dispensary.MemberRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.services.mgmt.CartService;
import com.google.common.collect.ImmutableMultimap;
import com.google.inject.Inject;
import io.dropwizard.servlets.tasks.Task;

import java.io.PrintWriter;

/**
 * Created by mdo on 12/15/17.
 */
public class PromoTransactionsRerunFixTask extends Task {

    @Inject
    TransactionRepository transactionRepository;
    @Inject
    CartService cartService;
    @Inject
    ShopRepository shopRepository;
    @Inject
    MemberGroupRepository memberGroupRepository;
    @Inject
    MemberRepository memberRepository;


    public PromoTransactionsRerunFixTask() {
        super("rerun-transactions");
    }

    @Override
    public void execute(ImmutableMultimap<String, String> parameters, PrintWriter output) throws Exception {
        // date
        /*
        long dateTime = 1513043805000l;
        String companyId = "";

        Iterable<Transaction> transactions = transactionRepository.listWithDate(companyId, dateTime, DateTime.now().getMillis());
        HashMap<String,Shop> shopHashMap = shopRepository.listAsMap();
        HashMap<String,MemberGroup> memberGroups = memberGroupRepository.listAllAsMap(companyId);

        List<ObjectId> memberIds = new ArrayList<>();

        for (Transaction transaction : transactions) {
            Shop shop = shopHashMap.get(transaction.getShopId());
            if (shop != null) {
                memberIds.add(new ObjectId(transaction.getMemberId()));
            }
        }

        HashMap<String,Member> memberHashMap = memberRepository.listAsMap(companyId,memberIds);


        for (Transaction transaction : transactions) {
            Shop shop = shopHashMap.get(transaction.getShopId());
            if (!transaction.isActive() && transaction.getTransType() == Transaction.TransactionType.Sale) {
                if (shop != null) {
                    System.out.println("Old: " + transaction.getCart().getDiscount());

                    Member member = memberHashMap.get(transaction.getMemberId());
                    if (member != null) {
                        MemberGroup memberGroup = memberGroups.get(member.getMemberGroupId());
                        cartService.prepareCart(shop, transaction, false, Transaction.TransactionStatus.Hold, false, memberGroup, false);

                        System.out.println("New: " + transaction.getCart().getDiscount());

                    }
                    transactionRepository.update(transaction.getCompanyId(),transaction.getId(),transaction);
                }
            }
        }
        */
    }
}
