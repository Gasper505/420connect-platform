package com.fourtwenty.core.tasks;

import com.fourtwenty.core.domain.models.loyalty.Promotion;
import com.fourtwenty.core.domain.repositories.dispensary.PromotionRepository;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Sets;
import com.google.inject.Inject;
import io.dropwizard.servlets.tasks.Task;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.PrintWriter;
import java.util.List;

/**
 * Created on 23/11/17 11:45 PM by Raja Dushyant Vashishtha
 * Sr. Software Developer
 * email : rajad@decipherzone.com
 * www.decipherzone.com
 */
public class PromotionPromocodeMigration extends Task {

    private static final Logger LOGGER = LoggerFactory.getLogger(PromotionPromocodeMigration.class);

    @Inject
    private PromotionRepository promotionRepository;

    public PromotionPromocodeMigration() {
        super("promotion-promocode-migration");
    }

    @Override
    public void execute(ImmutableMultimap<String, String> parameters, PrintWriter output) throws Exception {
        List<Promotion> promotions = promotionRepository.listNonDeleted();
        LOGGER.info("Starting migration for multiple promo codes");
        int counter = 0;
        for (Promotion promotion : promotions) {
            if (!StringUtils.isEmpty(promotion.getPromocode())) {
                promotion.setPromoCodes(Sets.newHashSet(promotion.getPromocode()));
                promotionRepository.update(promotion.getId(), promotion);
                counter++;
            }
        }
        LOGGER.info("Total records updated : {}", counter);
        LOGGER.info("Finished migrating promotions");
    }
}
