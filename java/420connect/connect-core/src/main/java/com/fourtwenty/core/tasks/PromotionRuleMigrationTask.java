package com.fourtwenty.core.tasks;

import com.fourtwenty.core.domain.models.loyalty.Promotion;
import com.fourtwenty.core.domain.models.loyalty.PromotionRule;
import com.fourtwenty.core.domain.repositories.dispensary.PromotionRepository;
import com.google.common.collect.ImmutableMultimap;
import com.mongodb.*;
import io.dropwizard.servlets.tasks.Task;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.PrintWriter;
import java.util.LinkedHashSet;
import java.util.List;

public class PromotionRuleMigrationTask extends Task {

    private static final Logger LOGGER = LoggerFactory.getLogger(PromotionRuleMigrationTask.class);

    @Inject
    private PromotionRepository promotionRepository;

    public PromotionRuleMigrationTask() {
        super("promotionrule-ids-conversion");
    }

    @Override
    public void execute(ImmutableMultimap<String, String> parameters, PrintWriter output) throws Exception {

        final List<Promotion> list = promotionRepository.list();
        final BasicDBObject field = new BasicDBObject();
        field.put("rules", 1);
        LOGGER.info("Migrating database for promotion rule");
        int updates = 0;
        for (Promotion promotion : list) {
            final BasicDBObject query = new BasicDBObject();
            query.put("_id", new ObjectId(promotion.getId()));
            final DBCursor cursor = promotionRepository.getPromotionsCursor(query, field);
            if (cursor.hasNext()) {
                final DBObject object = cursor.next();
                final LazyDBList rules = (LazyDBList) object.get("rules");
                final List<PromotionRule> promotionRules = promotion.getRules();
                if (rules != null && !rules.isEmpty()) {
                    for (int i = 0; i < rules.size(); i++) {
                        final LazyDBObject dbObject = (LazyDBObject) rules.get(i);
                        if (dbObject.get("productId") != null) {
                            String productId = String.valueOf(dbObject.get("productId"));
                            LinkedHashSet<String> items = new LinkedHashSet<>();
                            items.add(productId);
                            promotionRules.get(i).setProductIds(items);
                        }

                        if (dbObject.get("categoryId") != null) {
                            String categoryId = String.valueOf(dbObject.get("categoryId"));
                            LinkedHashSet<String> items = new LinkedHashSet<>();
                            items.add(categoryId);
                            promotionRules.get(i).setCategoryIds(items);
                        }

                        if (dbObject.get("vendorId") != null) {
                            String vendorId = String.valueOf(dbObject.get("vendorId"));
                            LinkedHashSet<String> items = new LinkedHashSet<>();
                            items.add(vendorId);
                            promotionRules.get(i).setVendorIds(items);
                        }
                    }
                    promotionRepository.update(promotion.getId(), promotion);
                    updates++;
                }
            }
        }

        LOGGER.info("{} promotions updated", updates);

    }
}
