package com.fourtwenty.core.tasks;

import com.fourtwenty.core.domain.models.company.Role;
import com.fourtwenty.core.domain.repositories.dispensary.RoleRepository;
import com.fourtwenty.core.lifecycle.model.DefaultData;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import io.dropwizard.servlets.tasks.Task;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.PrintWriter;
import java.util.List;

/**
 * Created by mdo on 4/11/17.
 */
public class RolesUpdateTask extends Task {
    private static final Log LOG = LogFactory.getLog(RolesUpdateTask.class);
    @Inject
    RoleRepository roleRepository;
    @Inject
    DefaultData defaultData;

    public RolesUpdateTask() {
        super("roles-update");
    }

    @Override
    public void execute(ImmutableMultimap<String, String> parameters, PrintWriter output) throws Exception {
        List<Role> roles = roleRepository.list();
        int fixed = 0;
        for (Role role : roles) {
            if (role.getName().equalsIgnoreCase("Manager")
                    || role.getName().equalsIgnoreCase("Admin")
                    || role.getName().equalsIgnoreCase("Shop Manager")) {

                List<Role.Permission> permissions = Lists.newArrayList(Role.Permission.WebPromotionsManage,
                        Role.Permission.WebPromotionsView);

                if (role.getPermissions() != null) {

                    boolean shouldUpdate = false;
                    for (Role.Permission permission : permissions) {
                        if (!role.getPermissions().contains(permission)) {
                            role.getPermissions().add(permission);
                            shouldUpdate = true;
                        }
                    }
                    if (shouldUpdate) {
                        roleRepository.update(role.getId(), role);
                        fixed++;
                    }
                }
            }
        }
        LOG.info("Roles Updated: " + fixed);
    }
}

