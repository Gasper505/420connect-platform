package com.fourtwenty.core.tasks;

import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.consumer.ConsumerUser;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.domain.repositories.store.ConsumerUserRepository;
import com.fourtwenty.core.managed.AmazonServiceManager;
import com.google.common.collect.ImmutableMultimap;
import com.google.inject.Inject;
import com.mongodb.WriteResult;
import io.dropwizard.servlets.tasks.Task;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.PrintWriter;
import java.util.HashMap;

/**
 * Created by mdo on 2/11/18.
 */
public class SetConsumerCleanPhoneNumberTask extends Task {
    private static final Log LOG = LogFactory.getLog(SetConsumerCleanPhoneNumberTask.class);

    @Inject
    ConsumerUserRepository consumerUserRepository;
    @Inject
    ShopRepository shopRepository;

    public SetConsumerCleanPhoneNumberTask() {
        super("consumer-clean-phone");
    }

    @Override
    public void execute(ImmutableMultimap<String, String> parameters, PrintWriter output) throws Exception {

        Iterable<ConsumerUser> consumerUsers = consumerUserRepository.getConsumerUnsetCPN();
        int updated = 0;
        Iterable<Shop> shops = shopRepository.list();
        HashMap<String, Shop> stringShopHashMap = new HashMap<>();
        for (Shop shop : shops) {
            if (StringUtils.isNotBlank(shop.getDefaultCountry())) {
                stringShopHashMap.put(shop.getCompanyId(), shop);
            }
        }


        for (ConsumerUser consumerUser : consumerUsers) {
            if (StringUtils.isBlank(consumerUser.getCpn())
                    && StringUtils.isNotBlank(consumerUser.getPrimaryPhone())) {
                Shop shop = stringShopHashMap.get(consumerUser.getSourceCompanyId());

                if (shop != null) {
                    try {
                        String cpn = AmazonServiceManager.cleanPhoneNumber(consumerUser.getPrimaryPhone(), shop);

                        WriteResult result = consumerUserRepository.updateCPN(consumerUser.getId(), cpn);

                        updated += result.getN();
                    } catch (Exception e) {
                        // LO
                        LOG.info("Error migrating: " + consumerUser.getPrimaryPhone(), e);
                    }
                }
            }
        }

        LOG.info("Migrated Consumer.cpn: " + updated);
    }
}
