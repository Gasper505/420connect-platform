package com.fourtwenty.core.tasks;

import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.repositories.dispensary.EmployeeRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.google.common.collect.ImmutableMultimap;
import com.google.inject.Inject;
import com.mongodb.WriteResult;
import io.dropwizard.servlets.tasks.Task;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by mdo on 11/14/17.
 */
public class SetShopForEmployeesTask extends Task {
    private static final Log LOG = LogFactory.getLog(SetShopForEmployeesTask.class);


    @Inject
    EmployeeRepository employeeRepository;
    @Inject
    ShopRepository shopRepository;

    public SetShopForEmployeesTask() {
        super("employees-set-shops");
    }

    @Override
    public void execute(ImmutableMultimap<String, String> parameters, PrintWriter output) throws Exception {
        Iterable<Employee> employees = employeeRepository.getEmployeesWithNoShops();

        Iterable<Shop> shops = shopRepository.list();
        HashMap<String, Shop> shopsPerCompany = new HashMap<>();
        for (Shop shop : shops) {
            Shop oldShop = shopsPerCompany.get(shop.getId());
            if (oldShop == null) {
                shopsPerCompany.put(shop.getCompanyId(), shop);
            } else if (shop.getCreated() < oldShop.getCreated()) {
                // if shop is older than the old shop, then make old shop the current shop
                shopsPerCompany.put(shop.getCompanyId(), shop);
            }
        }

        int totalUpdated = 0;
        int totalFailed = 0;
        for (Employee employee : employees) {
            if (employee.getShops() == null || employee.getShops().size() == 0) {
                List<String> shopIds = new ArrayList<>();
                Shop shop = shopsPerCompany.get(employee.getCompanyId());
                if (shop != null) {
                    shopIds.add(shop.getId());
                    WriteResult result = employeeRepository.setEmployeesShops(employee.getCompanyId(), employee.getId(), shopIds);
                    if (result.getN() > 0) {
                        totalUpdated++;
                    } else {
                        totalFailed++;
                    }
                }
            }
        }

        LOG.info("Employees updated with new shop: " + totalUpdated + "  failed: " + totalFailed);
    }
}
