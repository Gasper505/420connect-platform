package com.fourtwenty.core.tasks;

import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.repositories.dispensary.CompanyRepository;
import com.fourtwenty.core.domain.repositories.dispensary.MemberRepository;
import com.fourtwenty.core.lifecycle.ElasticSearchStartup;
import com.fourtwenty.core.managed.ElasticSearchManager;
import com.google.common.collect.ImmutableMultimap;
import com.google.inject.Inject;
import io.dropwizard.servlets.tasks.Task;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.PrintWriter;
import java.util.List;

public class SyncToElasticSearchV1Task extends Task {
    private static final Log LOG = LogFactory.getLog(SyncToElasticSearchV1Task.class);

    @Inject
    ElasticSearchStartup elasticSearchStartup;

    @Inject
    CompanyRepository companyRepository;
    @Inject
    MemberRepository memberRepository;


    @Inject
    ElasticSearchManager elasticSearchManager;

    protected SyncToElasticSearchV1Task() {
        super("elasticsearch-sync-members");
    }

    @Override
    public void execute(ImmutableMultimap<String, String> parameters, PrintWriter output) throws Exception {
        LOG.info("Starting sync to elastic search");
        createElasticSearchIndexes();
        LOG.info("Done creating search indexes");

        List<Company> companies = companyRepository.list();
        for (Company company : companies) {
            LOG.info("Syncing: " + company.getName());
            syncMembers(company.getId());
        }
    }

    public void createElasticSearchIndexes() {
        elasticSearchStartup.run();
    }

    public void syncMembers(String companyId) {

        List<Member> members = memberRepository.getMembers(companyId);
        LOG.info("Members size: " + members.size());
        elasticSearchManager.createOrUpdateIndexedDocuments(members, 50);
    }
}
