package com.fourtwenty.core.tasks;

import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductChangeLog;
import com.fourtwenty.core.domain.repositories.dispensary.ProductChangeLogRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.ImmutableMultimap;
import com.google.inject.Inject;
import io.dropwizard.servlets.tasks.Task;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by mdo on 12/29/16.
 */
public class TakeInventorySnapshotTask extends Task {
    private static final Log LOG = LogFactory.getLog(TakeInventorySnapshotTask.class);

    @Inject
    ProductRepository productRepository;
    @Inject
    ProductChangeLogRepository productChangeLogRepository;

    public TakeInventorySnapshotTask() {
        super("take-snapshot");
    }

    @Override
    public void execute(ImmutableMultimap<String, String> parameters, PrintWriter output) throws Exception {

        ImmutableCollection<String> companies = parameters.get("companyId");
        ImmutableCollection<String> shops = parameters.get("shopId");
        String companyId = null;
        String shopId = null;
        if (companies != null && companies.size() > 0) {
            companyId = (String) companies.toArray()[0];
        }


        if (shops != null && shops.size() > 0) {
            shopId = (String) shops.toArray()[0];
        }
        if (companyId != null && shopId != null) {
            takeSnapshotsOfAllProducts(companyId, shopId);
        }

    }


    private void takeSnapshotsOfAllProducts(final String companyId, final String shopId) {


        Iterable<Product> products = productRepository.listByShop(companyId, shopId);
        List<ProductChangeLog> productChangeLogs = new ArrayList<>();
        for (Product p : products) {
            productChangeLogs.add(createProductChangeLog("", p, "Inventory Snapshot"));
        }
        if (productChangeLogs.size() > 0) {
            productChangeLogRepository.save(productChangeLogs);
        }
    }


    private ProductChangeLog createProductChangeLog(String employeeId, Product product, String reference) {

        // Add change log
        ProductChangeLog changeLog = new ProductChangeLog();
        changeLog.prepare(product.getCompanyId());
        changeLog.setShopId(product.getShopId());
        changeLog.setProductId(product.getId());
        changeLog.setEmployeeId(employeeId);
        changeLog.setReference(reference);
        changeLog.setProductQuantities(product.getQuantities());
        return changeLog;
    }
}
