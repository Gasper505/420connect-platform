package com.fourtwenty.core.tasks;

import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TerminalRepository;
import com.google.common.collect.ImmutableMultimap;
import io.dropwizard.servlets.tasks.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class TerminalCheckoutMigration extends Task {
    private static final Logger LOGGER = LoggerFactory.getLogger(TerminalCheckoutMigration.class);

    @Inject
    private ShopRepository shopRepository;
    @Inject
    private TerminalRepository terminalRepository;

    public TerminalCheckoutMigration() {
        super("migrate-terminal-checkout");
    }

    @Override
    public void execute(ImmutableMultimap<String, String> parameters, PrintWriter output) throws Exception {

        LOGGER.info("Starting terminal checkout migration ");

        Iterable<Shop> shops = shopRepository.list();

        List<String> fulfilmentShopIds = new ArrayList<>();
        List<String> directShopIds = new ArrayList<>();


        for (Shop shop : shops) {
            if (shop.getCheckoutType() == Shop.ShopCheckoutType.Fulfillment) {
                fulfilmentShopIds.add(shop.getId());
            } else {
                directShopIds.add(shop.getId());
            }
        }

        terminalRepository.updateCheckoutType(fulfilmentShopIds, Shop.ShopCheckoutType.Fulfillment);
        terminalRepository.updateCheckoutType(directShopIds, Shop.ShopCheckoutType.Direct);

        LOGGER.info("Complete terminal checkout migration ");

    }
}
