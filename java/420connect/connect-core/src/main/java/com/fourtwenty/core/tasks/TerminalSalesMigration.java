package com.fourtwenty.core.tasks;

import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.google.common.collect.ImmutableMultimap;
import io.dropwizard.servlets.tasks.Task;

import javax.inject.Inject;
import java.io.PrintWriter;

public class TerminalSalesMigration extends Task {

    @Inject
    private ShopRepository shopRepository;

    public TerminalSalesMigration() {
        super("migrate-shop-terminal-sales-option");
    }

    @Override
    public void execute(ImmutableMultimap<String, String> parameters, PrintWriter output) throws Exception {

        /*Iterable<Shop> shops = shopRepository.list();
        for (Shop shop : shops) {
            if (shop.isUseAssignedEmployee()) {
                shopRepository.updateTerminalSaleOption(shop.getCompanyId(),shop.getId(), Shop.TerminalSalesOption.AssignedEmployeeTerminal);
            } else {
                shopRepository.updateTerminalSaleOption(shop.getCompanyId(),shop.getId(), Shop.TerminalSalesOption.DeviceTerminal);
            }
        }*/
    }
}
