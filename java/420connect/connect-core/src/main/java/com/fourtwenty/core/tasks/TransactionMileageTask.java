package com.fourtwenty.core.tasks;

import com.fourtwenty.core.domain.models.company.TerminalLocation;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.TerminalLocationRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.google.common.collect.ImmutableMultimap;
import io.dropwizard.servlets.tasks.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TransactionMileageTask extends Task {
    private static final Logger LOGGER = LoggerFactory.getLogger(TransactionMileageTask.class);

    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private TerminalLocationRepository terminalLocationRepository;

    protected TransactionMileageTask() {
        super("calculate-transaction-mileage-migration");
    }

    @Override
    public void execute(ImmutableMultimap<String, String> parameters, PrintWriter output) throws Exception {
        LOGGER.info("Starting migration for calculating mileage by transaction");

        HashMap<String, Transaction> transactionMap = transactionRepository.getTransactionsByMileageCalculated(false);
        Iterable<TerminalLocation> terminalLocations = terminalLocationRepository.listAllBySort("{created:1}");

        Map<String, List<TerminalLocation>> transactionLocationMap = new HashMap<>();
        for (Map.Entry<String, Transaction> entry : transactionMap.entrySet()) {
            Transaction transaction = entry.getValue();
            for (TerminalLocation location : terminalLocations) {
                List<TerminalLocation> locationList = transactionLocationMap.get(entry.getKey());
                //Check for terminal id of transaction id and terminal location and created date of transaction should be greater than terminal location
                if (transaction.getTerminalId() != null && transaction.getStartRouteDate() != null && transaction.getEndRouteDate() != null
                        && transaction.getTerminalId().equalsIgnoreCase(location.getTerminalId()) &&
                        (transaction.getStartRouteDate() <= location.getCreated() & transaction.getEndRouteDate() >= location.getCreated())) {
                    if (locationList == null) {
                        locationList = new ArrayList<>();
                    }
                    locationList.add(location);
                    transactionLocationMap.put(entry.getKey(), locationList);
                }
            }
        }

        double distance;
        for (Map.Entry<String, List<TerminalLocation>> entry : transactionLocationMap.entrySet()) {
            distance = 0;
            Transaction transaction = transactionMap.get(entry.getKey());
            List<TerminalLocation> locations = transactionLocationMap.get(entry.getKey());
            if (locations != null) {
                for (int i = 0; i < locations.size(); i++) {
                    if (i != locations.size() - 1) {
                        distance += distanceRaw(locations.get(i).getLoc().get(1), locations.get(i).getLoc().get(0),
                                locations.get(i + 1).getLoc().get(1), locations.get(i + 1).getLoc().get(0), 'M');
                    }
                }
            }


            transaction.setMileageCalculated(true);
            transaction.setMileage(new BigDecimal(distance));
            transactionRepository.update(transaction.getId(), transaction);
        }

        LOGGER.info("Completed migration for calculating mileage by transaction");
    }

    private double distanceRaw(double lat1, double lon1, double lat2, double lon2, char unit) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        if (unit == 'K') {
            dist = dist * 1.609344;
        } else if (unit == 'N') {
            dist = dist * 0.8684;
        }

        if (Double.isNaN(dist)) {
            dist = 0;
        }

        return (dist);
    }

    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }
}
