package com.fourtwenty.core.tasks;

import com.amazonaws.util.CollectionUtils;
import com.fourtwenty.core.domain.models.product.BatchQuantity;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductBatch;
import com.fourtwenty.core.domain.models.product.ProductQuantity;
import com.fourtwenty.core.domain.repositories.dispensary.BatchQuantityRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductBatchRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.google.common.collect.ImmutableMultimap;
import com.google.inject.Inject;
import io.dropwizard.servlets.tasks.Task;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class UpdateBatchQuantityMigrationTask extends Task {

    private static final Log LOG = LogFactory.getLog(UpdateBatchQuantityMigrationTask.class);

    @Inject
    private ProductRepository productRepository;
    @Inject
    private ProductBatchRepository productBatchRepository;
    @Inject
    private BatchQuantityRepository batchQuantityRepository;

    public UpdateBatchQuantityMigrationTask() {
        super("batch-quantity-update-migration");
    }

    @Override
    public void execute(ImmutableMultimap<String, String> parameters, PrintWriter output) throws Exception {

        LOG.info("Starting migration for updating product batch quantities according to product quantity");

        List<Product> products = productRepository.listNonDeleted();
        List<ProductBatch> productBatchList = productBatchRepository.listNonDeleted();

        // newest first
        productBatchList.sort((o1, o2) -> {
            Long pDate = o2.getPurchasedDate();
            return pDate.compareTo(o1.getPublishedDate());
        });

        List<BatchQuantity> batchQuantities = batchQuantityRepository.list();

        //Map product batch by product
        HashMap<String, List<ProductBatch>> productBatchMap = new HashMap<>();
        for (ProductBatch productBatch : productBatchList) {
            productBatchMap.putIfAbsent(productBatch.getProductId(), new ArrayList<>());
            productBatchMap.get(productBatch.getProductId()).add(productBatch);
        }


        HashMap<String, BatchQuantity> keyToBatchQuantities = new HashMap<>();
        HashMap<String, Double> quantityMap = new HashMap<>();
        for (BatchQuantity batchQuantity : batchQuantities) {
            String key = String.format("%s-%s-%s", batchQuantity.getProductId(), batchQuantity.getInventoryId(), batchQuantity.getBatchId());
            if (!keyToBatchQuantities.containsKey(key)) {
                keyToBatchQuantities.put(key, batchQuantity);
            }
            String quantityKey = String.format("%s-%s", batchQuantity.getProductId(), batchQuantity.getInventoryId());
            quantityMap.putIfAbsent(quantityKey, 0d);
            Double quantity = quantityMap.get(quantityKey);
            quantityMap.put(quantityKey, (quantity + batchQuantity.getQuantity().doubleValue()));
        }

        for (Product product : products) {
            if (CollectionUtils.isNullOrEmpty(product.getQuantities())) {
                continue;
            }

            List<ProductBatch> batches = productBatchMap.get(product.getId());
            if (CollectionUtils.isNullOrEmpty(batches)) {
                continue;
            }

            for (ProductQuantity productQuantity : product.getQuantities()) {
                String quantityKey = String.format("%s-%s", product.getId(), productQuantity.getInventoryId());

                double prodQuantity = productQuantity.getQuantity().doubleValue();
                Double batchQuant = quantityMap.get(quantityKey);
                if (batchQuant == null || prodQuantity >= batchQuant) {
                    continue;
                }
                double quantityDifference = batchQuant - prodQuantity;
                for (ProductBatch batch : batches) {
                    String key = String.format("%s-%s-%s", product.getId(), productQuantity.getInventoryId(), batch.getId());
                    BatchQuantity batchQuantity = keyToBatchQuantities.get(key);

                    if (batchQuantity != null && quantityDifference > 0) {
                        double origBatchQuantity = batchQuantity.getQuantity().doubleValue();
                        double diff = quantityDifference - origBatchQuantity;

                        if (diff < 0) {
                            batchQuantity.setQuantity(batchQuantity.getQuantity().subtract(new BigDecimal(Math.abs(quantityDifference))));
                        } else {
                            batchQuantity.setQuantity(new BigDecimal(0));
                        }
                        quantityDifference = quantityDifference - origBatchQuantity;

                        batchQuantityRepository.update(batchQuantity.getCompanyId(), batchQuantity.getId(), batchQuantity);
                    }
                }
            }
        }

        LOG.info("Completed migration for updating product batch quantities according to product quantity");

    }
}
