package com.fourtwenty.core.tasks;

import com.fourtwenty.core.domain.models.company.CashDrawerSession;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.repositories.dispensary.CashDrawerSessionRepository;
import com.fourtwenty.core.domain.repositories.dispensary.PaidInOutItemRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.services.common.RealtimeService;
import com.fourtwenty.core.services.global.CashDrawerProcessorService;
import com.google.common.collect.ImmutableMultimap;
import com.google.inject.Inject;
import io.dropwizard.servlets.tasks.Task;

import java.io.PrintWriter;
import java.util.HashMap;

/**
 * Created by mdo on 1/8/17.
 */
public class UpdateCashDrawerTask extends Task {

    @Inject
    CashDrawerSessionRepository cashDrawerSessionRepository;
    @Inject
    PaidInOutItemRepository paidInOutItemRepository;
    @Inject
    TransactionRepository transactionRepository;
    @Inject
    ShopRepository shopRepository;
    @Inject
    CashDrawerProcessorService cashDrawerProcessorService;

    @Inject
    RealtimeService realtimeService;

    public UpdateCashDrawerTask() {
        super("update-cashdrawers");
    }

    @Override
    public void execute(ImmutableMultimap<String, String> parameters, PrintWriter output) throws Exception {

        // Find any opened cash drawers
        Iterable<CashDrawerSession> cashDrawerSessions = cashDrawerSessionRepository.getOpenedCashDrawers();
        //List<ObjectId> shopIdList = new ArrayList<>();
        HashMap<String, Shop> shopHashMap = shopRepository.listAsMap();
        for (CashDrawerSession cashDrawerSession : cashDrawerSessions) {
            Shop shop = shopHashMap.get(cashDrawerSession.getShopId());
            if (shop != null) {
                //processCurrentCashDrawer(cashDrawerSession,shop.getCompanyId(),shop.getId(),shop.getTimeZone());
                cashDrawerProcessorService.processCurrentCashDrawer(shop, cashDrawerSession, false);

                cashDrawerSessionRepository.update(shop.getCompanyId(), cashDrawerSession.getId(), cashDrawerSession);
                realtimeService.sendRealTimeEvent(shop.getId(), RealtimeService.RealtimeEventType.CashDrawerUpdateEvent, null);
            }
        }
    }
}
