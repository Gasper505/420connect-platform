package com.fourtwenty.core.tasks;

import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductPriceRange;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.google.common.collect.ImmutableMultimap;
import com.google.inject.Inject;
import io.dropwizard.servlets.tasks.Task;
import org.apache.commons.lang3.StringUtils;

import java.io.PrintWriter;
import java.util.List;

/**
 * Created by mdo on 1/15/17.
 */
public class UpdatePriceRangesTask extends Task {

    @Inject
    ProductRepository productRepository;

    public UpdatePriceRangesTask() {
        super("priceranges-update");
    }

    @Override
    public void execute(ImmutableMultimap<String, String> parameters, PrintWriter output) throws Exception {
        migrateProductPriceRanges();
    }

    private void migrateProductPriceRanges() {
        List<Product> products = productRepository.list();
        for (Product product : products) {
            boolean shouldSave = false;
            for (ProductPriceRange range : product.getPriceRanges()) {
                if (StringUtils.isBlank(range.getId())) {
                    shouldSave = true;
                    range.setId(String.format("%s_%s", product.getId(), range.getWeightToleranceId()));
                }
            }
            if (shouldSave) {
                productRepository.updateProductPriceRanges(product.getCompanyId(), product.getId(), product.getPriceRanges());
            }
        }
    }
}
