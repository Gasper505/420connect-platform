package com.fourtwenty.core.thirdparty.elasticsearch;

public class AWSResponseWrapper {
    private String response;

    public AWSResponseWrapper(String response) {
        this.response = response;
    }

    public String getResponse() {
        return response;
    }
}
