package com.fourtwenty.core.thirdparty.elasticsearch;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.http.HttpResponse;
import com.amazonaws.http.HttpResponseHandler;
import com.amazonaws.util.IOUtils;

public class ResponseExceptionHandler implements HttpResponseHandler<AmazonServiceException> {

    @Override
    public AmazonServiceException handle(HttpResponse response) throws Exception {
        final AmazonServiceException ase = new AmazonServiceException(IOUtils.toString(response.getContent()));
        ase.setStatusCode(response.getStatusCode());
        ase.setErrorCode(response.getStatusText());
        return ase;
    }

    @Override
    public boolean needsConnectionLeftOpen() {
        return false;
    }

}
