package com.fourtwenty.core.thirdparty.elasticsearch;

import com.amazonaws.http.HttpResponse;
import com.amazonaws.http.HttpResponseHandler;
import org.apache.commons.io.IOUtils;

public class ResponseSuccessHandler implements HttpResponseHandler<AWSResponseWrapper> {
    @Override
    public AWSResponseWrapper handle(HttpResponse response) throws Exception {
        try {
            String responseString = IOUtils.toString(response.getContent());
            return new AWSResponseWrapper(responseString);
        } finally {
            try {
                response.getContent().close();
            } catch (Exception e) {
            }
        }
    }

    @Override
    public boolean needsConnectionLeftOpen() {
        return false;
    }
}
