package com.fourtwenty.core.thirdparty.elasticsearch.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fourtwenty.core.thirdparty.elasticsearch.models.response.AWSSearchResponseHit;
import org.json.JSONObject;

public interface ElasticSearchCapable {
    @JsonIgnore
    ElasticSearchIndex getElasticSearchIndex();

    @JsonIgnore
    void loadFrom(AWSSearchResponseHit hit);

    @JsonIgnore
    JSONObject toElasticSearchObject();
}
