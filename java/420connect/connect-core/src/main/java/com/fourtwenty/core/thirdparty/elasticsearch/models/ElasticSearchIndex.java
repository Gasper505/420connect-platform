package com.fourtwenty.core.thirdparty.elasticsearch.models;

import java.util.Map;

public class ElasticSearchIndex {
    private String index;
    private String type;
    private Map<String, FieldIndex> properties;

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Map<String, FieldIndex> getProperties() {
        return properties;
    }

    public void setProperties(Map<String, FieldIndex> properties) {
        this.properties = properties;
    }
}
