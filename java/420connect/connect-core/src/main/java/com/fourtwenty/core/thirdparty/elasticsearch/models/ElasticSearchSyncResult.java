package com.fourtwenty.core.thirdparty.elasticsearch.models;

import java.util.List;

public class ElasticSearchSyncResult<E> {
    private long totalFound;

    private List<E> records;

    public long getTotalFound() {
        return totalFound;
    }

    public void setTotalFound(long totalFound) {
        this.totalFound = totalFound;
    }

    public List<E> getRecords() {
        return records;
    }

    public void setRecords(List<E> records) {
        this.records = records;
    }
}
