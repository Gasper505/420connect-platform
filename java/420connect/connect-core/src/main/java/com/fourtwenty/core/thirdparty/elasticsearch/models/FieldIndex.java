package com.fourtwenty.core.thirdparty.elasticsearch.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FieldIndex {
    private String type;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String analyzer;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Boolean index;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Boolean fielddata;

    public FieldIndex() {
    }

    public FieldIndex(String type, String analyzer, Boolean index) {
        this(type, analyzer, index, null);
    }

    public FieldIndex(String type, String analyzer, Boolean index, Boolean fielddata) {
        this.type = type;
        this.analyzer = analyzer;
        this.index = index;
        this.fielddata = fielddata;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAnalyzer() {
        return analyzer;
    }

    public void setAnalyzer(String analyzer) {
        this.analyzer = analyzer;
    }

    public Boolean isIndex() {
        return index;
    }

    public void setIndex(Boolean index) {
        this.index = index;
    }

    public Boolean isFielddata() {
        return fielddata;
    }

    public void setFielddata(Boolean fielddata) {
        this.fielddata = fielddata;
    }
}
