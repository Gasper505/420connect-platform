package com.fourtwenty.core.thirdparty.elasticsearch.models.request;

import com.fourtwenty.core.thirdparty.elasticsearch.models.FieldIndex;

import java.util.HashMap;
import java.util.Map;

public class AWSMappingRequest {
    private Map<String, FieldIndex> properties = new HashMap<>();

    public Map<String, FieldIndex> getProperties() {
        return properties;
    }

    public void setProperties(Map<String, FieldIndex> properties) {
        this.properties = properties;
    }
}
