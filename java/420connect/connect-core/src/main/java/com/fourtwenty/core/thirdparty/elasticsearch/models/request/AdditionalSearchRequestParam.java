package com.fourtwenty.core.thirdparty.elasticsearch.models.request;

public final class AdditionalSearchRequestParam {

    private final String field;
    private final Object value;
    private final SearchRequestBuilder.SearchOperator operator;

    public AdditionalSearchRequestParam(String field, Object value, SearchRequestBuilder.SearchOperator operator) {
        this.field = field;
        this.value = value;
        this.operator = operator;
    }

    public String getField() {
        return field;
    }

    public Object getValue() {
        return value;
    }

    public SearchRequestBuilder.SearchOperator getOperator() {
        return operator;
    }
}
