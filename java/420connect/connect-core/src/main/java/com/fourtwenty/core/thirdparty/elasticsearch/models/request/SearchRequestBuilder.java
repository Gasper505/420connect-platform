package com.fourtwenty.core.thirdparty.elasticsearch.models.request;

import java.util.ArrayList;
import java.util.List;

public final class SearchRequestBuilder {

    public enum SearchOperator {
        OR, AND
    }

    private final List<AdditionalSearchRequestParam> additionalSearchRequestParams = new ArrayList<>();

    private SearchRequestBuilder() {
    }

    public static SearchRequestBuilder get() {
        return new SearchRequestBuilder();
    }

    public void add(String field, Object value, SearchOperator operator) {
        additionalSearchRequestParams.add(new AdditionalSearchRequestParam(field, value, operator));
    }

    public String build(final String term) {
        final StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("*" + term + "*");
        for (AdditionalSearchRequestParam additionalSearchRequestParam : additionalSearchRequestParams) {
            stringBuilder.append(" " + additionalSearchRequestParam.getOperator() + " ").append(additionalSearchRequestParam.getField() + ":" + additionalSearchRequestParam.getValue());
        }
        return stringBuilder.toString();
    }
}
