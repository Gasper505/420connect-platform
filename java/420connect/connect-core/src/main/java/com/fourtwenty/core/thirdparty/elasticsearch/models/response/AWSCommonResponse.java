package com.fourtwenty.core.thirdparty.elasticsearch.models.response;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fourtwenty.core.thirdparty.elasticsearch.AWSResponseWrapper;

public class AWSCommonResponse extends AWSResponse {

    private boolean acknowledged;

    public AWSCommonResponse(ObjectMapper objectMapper, AWSResponseWrapper wrapper) {
        super(objectMapper, wrapper);
    }

    public boolean isAcknowledged() {
        return acknowledged;
    }

    public void setAcknowledged(boolean acknowledged) {
        this.acknowledged = acknowledged;
    }

}
