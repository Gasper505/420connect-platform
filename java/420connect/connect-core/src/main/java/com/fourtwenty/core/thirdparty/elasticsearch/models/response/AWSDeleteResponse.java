package com.fourtwenty.core.thirdparty.elasticsearch.models.response;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fourtwenty.core.thirdparty.elasticsearch.AWSResponseWrapper;

public class AWSDeleteResponse extends AWSResponse {

    private long total;

    public AWSDeleteResponse(ObjectMapper objectMapper, AWSResponseWrapper wrapper) {
        super(objectMapper, wrapper);
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }
}
