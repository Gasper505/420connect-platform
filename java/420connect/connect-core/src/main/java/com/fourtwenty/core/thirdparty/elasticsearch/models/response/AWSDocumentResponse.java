package com.fourtwenty.core.thirdparty.elasticsearch.models.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fourtwenty.core.thirdparty.elasticsearch.AWSResponseWrapper;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AWSDocumentResponse extends AWSResponse {

    private String result;
    @JsonProperty("_version")
    private Integer version;
    @JsonProperty("_id")
    private String id;

    public AWSDocumentResponse(ObjectMapper objectMapper, AWSResponseWrapper wrapper) {
        super(objectMapper, wrapper);
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
