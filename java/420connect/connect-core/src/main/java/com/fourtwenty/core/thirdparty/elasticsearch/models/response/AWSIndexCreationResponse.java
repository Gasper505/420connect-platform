package com.fourtwenty.core.thirdparty.elasticsearch.models.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fourtwenty.core.thirdparty.elasticsearch.AWSResponseWrapper;

import java.io.IOException;

@JsonIgnoreProperties
public class AWSIndexCreationResponse extends AWSResponse {

    private boolean acknowledged;
    @JsonProperty("shards_acknowledged")
    private boolean shardsAcknowledged;
    private String index;

    public AWSIndexCreationResponse(ObjectMapper objectMapper, AWSResponseWrapper wrapper) {
        super(objectMapper, wrapper);
    }

    @Override
    public void load() throws IOException {
        objectMapper.readerForUpdating(this).readValue(wrapper.getResponse());
    }

    public boolean isAcknowledged() {
        return acknowledged;
    }

    public void setAcknowledged(boolean acknowledged) {
        this.acknowledged = acknowledged;
    }

    public boolean isShardsAcknowledged() {
        return shardsAcknowledged;
    }

    public void setShardsAcknowledged(boolean shardsAcknowledged) {
        this.shardsAcknowledged = shardsAcknowledged;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }
}
