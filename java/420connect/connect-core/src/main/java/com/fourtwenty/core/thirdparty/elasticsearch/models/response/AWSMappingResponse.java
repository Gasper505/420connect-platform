package com.fourtwenty.core.thirdparty.elasticsearch.models.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fourtwenty.core.thirdparty.elasticsearch.AWSResponseWrapper;
import com.fourtwenty.core.thirdparty.elasticsearch.models.FieldIndex;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@JsonIgnoreProperties
public class AWSMappingResponse extends AWSResponse {

    private Map<String, FieldIndex> properties = new HashMap<>();
    private String indexName;
    private String typeName;

    @Override
    public void load() throws IOException {
        final JsonNode jsonNode = objectMapper.readTree(wrapper.getResponse());
        if (jsonNode == null) return;

        final JsonNode rootNode = jsonNode.get(indexName);
        if (rootNode == null) return;

        final JsonNode mappings = rootNode.get("mappings");
        if (mappings == null) return;

        final JsonNode typeMapping = mappings.get(typeName);
        if (typeMapping == null) return;

        objectMapper.readerForUpdating(this).readValue(typeMapping);
    }

    public AWSMappingResponse(String indexName, String typeName, ObjectMapper objectMapper, AWSResponseWrapper wrapper) {
        super(objectMapper, wrapper);
        this.indexName = indexName;
        this.typeName = typeName;
    }

    public Map<String, FieldIndex> getProperties() {
        return properties;
    }

    public void setProperties(Map<String, FieldIndex> properties) {
        this.properties = properties;
    }
}
