package com.fourtwenty.core.thirdparty.elasticsearch.models.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fourtwenty.core.thirdparty.elasticsearch.AWSResponseWrapper;

import java.io.IOException;

@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class AWSResponse {
    protected ObjectMapper objectMapper;
    protected AWSResponseWrapper wrapper;

    public AWSResponse(ObjectMapper objectMapper, AWSResponseWrapper wrapper) {
        this.objectMapper = objectMapper;
        this.wrapper = wrapper;
    }

    public void load() throws IOException {
        objectMapper.readerForUpdating(this).readValue(wrapper.getResponse());
    }

}
