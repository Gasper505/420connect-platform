package com.fourtwenty.core.thirdparty.elasticsearch.models.response;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fourtwenty.core.domain.models.generic.BaseModel;
import com.fourtwenty.core.thirdparty.elasticsearch.AWSResponseWrapper;
import com.fourtwenty.core.thirdparty.elasticsearch.models.ElasticSearchCapable;
import org.bson.types.ObjectId;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class AWSSearchResponse extends AWSResponse {

    private List<ObjectId> documentIds = new ArrayList<>();
    private long total;
    private double maxScore;

    private List<AWSSearchResponseHit> hits = new ArrayList<>();

    public AWSSearchResponse(ObjectMapper objectMapper, AWSResponseWrapper wrapper) {
        super(objectMapper, wrapper);
    }

    @Override
    public void load() throws IOException {
        final Map<String, Object> searchResponseMap = objectMapper.readValue(wrapper.getResponse(), new TypeReference<Map<String, Object>>() {
        });

        //NEED TO REMOVE
        //System.out.println(wrapper.getResponse());

        Map<String, Object> elasticSearchHits = (Map<String, Object>) searchResponseMap.get("hits");
        total = (Integer) elasticSearchHits.get("total");
        maxScore = elasticSearchHits.get("max_score") != null ? (Double) elasticSearchHits.get("max_score") : 0;

        List<Map<String, Object>> itemHits = (List<Map<String, Object>>) elasticSearchHits.get("hits");
        itemHits.forEach(h -> {
            if (h != null && h.get("_id") != null) {
                documentIds.add(new ObjectId(String.valueOf(h.get("_id"))));

                hits.add(new AWSSearchResponseHit((String) h.get("_id"), (Map<String, Object>) h.get("_source")));
            }
        });
    }

    public List<ObjectId> getDocumentIds() {
        return documentIds;
    }

    public List<AWSSearchResponseHit> getHits() {
        return hits;
    }

    public long getTotal() {
        return total;
    }

    public double getMaxScore() {
        return maxScore;
    }

    public <E extends BaseModel & ElasticSearchCapable> List<E> populateObjects(Class clazz) {
        final List<E> objects = new ArrayList<>();
        for (AWSSearchResponseHit hit : hits) {
            try {
                E obj = (E) clazz.newInstance();
                obj.setId(hit.getId());
                obj.loadFrom(hit);
                objects.add(obj);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return objects;
    }
}
