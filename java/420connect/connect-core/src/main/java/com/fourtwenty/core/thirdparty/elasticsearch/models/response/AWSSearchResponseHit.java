package com.fourtwenty.core.thirdparty.elasticsearch.models.response;

import java.util.Map;

public class AWSSearchResponseHit {

    public String id;
    public Map<String, Object> source;

    public AWSSearchResponseHit(String id, Map<String, Object> source) {
        this.id = id;
        this.source = source;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Map<String, Object> getSource() {
        return source;
    }

    public void setSource(Map<String, Object> source) {
        this.source = source;
    }
}
