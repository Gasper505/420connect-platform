package com.fourtwenty.core.thirdparty.elasticsearch.models.types;

public enum Analyzer {
    STANDARD("standard"), ENGLISH("english"), NONE("");
    private String value;

    Analyzer(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
