package com.fourtwenty.core.thirdparty.elasticsearch.models.types;

public enum FieldType {
    TEXT("text"),
    KEYWORD("keyword"),
    LONG("long"),
    INTEGER("integer"),
    SHORT("short"), BYTE("byte"),
    DOUBLE("double"),
    FLOAT("float"),
    DATE("date"),
    BOOLEAN("boolean");

    private String value;

    FieldType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
