package com.fourtwenty.core.thirdparty.elasticsearch.services;

import com.fourtwenty.core.domain.models.generic.BaseModel;
import com.fourtwenty.core.domain.models.product.ProductBatch;
import com.fourtwenty.core.thirdparty.elasticsearch.models.ElasticSearchCapable;
import com.fourtwenty.core.thirdparty.elasticsearch.models.request.SearchRequestBuilder;
import com.fourtwenty.core.thirdparty.elasticsearch.models.response.*;
import org.json.JSONObject;

import java.util.List;

public interface ElasticSearchCommunicatorService {
    class SearchField {
        public SearchField(String name, float b) {
            this.fieldName = name;
            this.boost = b;
        }

        public String fieldName = "";
        public float boost = 1;
    }

    AWSDocumentResponse createOrUpdateIndexedDocument(String index, String type, BaseModel data, JSONObject jsonObject);

    AWSDocumentResponse createOrUpdateIndexedDocuments(List<ElasticSearchCapable> objs);

    AWSSearchResponse searchIndexedDocument(final String companyId, final String shopId, String index, String type, String term);

    AWSSearchResponse searchIndexedDocument(String index, String type, String term, SearchRequestBuilder searchRequestBuilder, int start, int limit);

    AWSSearchResponse searchIndexedDocument(final String companyId, final String shopId, String index, String type, String term, List<SearchField> fields, int start, int limit, String sortBy, String sortByDirection);

    AWSDocumentResponse deleteIndexedDocument(String index, String type, String id);

    AWSCommonResponse deleteIndex(String index);

    AWSCommonResponse updateIndex(String index, String type, String mappings);

    AWSIndexCreationResponse createIndex(String index, String mappings);

    AWSMappingResponse getIndexMappings(String index, String type);

    AWSDeleteResponse deleteIndexedDocumentsFor(String index, String type, String companyId);

    AWSSearchResponse searchIndexedDocument(final String companyId, final String shopId, String index, String type, String term, List<SearchField> fields, int start, int limit, String sortBy, String sortByDirection, ProductBatch.BatchStatus status);

    AWSDeleteResponse deleteIndexedDocumentsFor(String index, String type, List<String> ids);
}
