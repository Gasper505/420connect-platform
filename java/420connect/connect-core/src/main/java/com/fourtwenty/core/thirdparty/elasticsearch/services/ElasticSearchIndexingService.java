package com.fourtwenty.core.thirdparty.elasticsearch.services;

import com.fourtwenty.core.domain.models.company.CompanyBaseModel;
import com.fourtwenty.core.domain.models.generic.BaseModel;
import com.fourtwenty.core.thirdparty.elasticsearch.models.ElasticSearchCapable;
import com.fourtwenty.core.thirdparty.elasticsearch.models.response.AWSDeleteResponse;
import com.fourtwenty.core.thirdparty.elasticsearch.models.response.AWSDocumentResponse;
import org.json.JSONObject;

import java.util.List;

public interface ElasticSearchIndexingService {
    <E extends CompanyBaseModel> AWSDocumentResponse createOrUpdateIndexedDocument(ElasticSearchCapable obj, JSONObject data);

    <E extends CompanyBaseModel> AWSDocumentResponse createOrUpdateIndexedDocuments(List<ElasticSearchCapable> data);

    <E extends CompanyBaseModel> AWSDocumentResponse deleteIndexedDocument(String id, Class<E> clazz);


    List<String> getIndexAndType(Class<? extends BaseModel> clazz);

    <E extends CompanyBaseModel> AWSDeleteResponse deleteIndexedDocumentsFor(String companyId, Class<E> clazz);

    <E extends CompanyBaseModel> AWSDeleteResponse deleteIndexedDocumentsFor(List<String> ids, Class<E> clazz);
}
