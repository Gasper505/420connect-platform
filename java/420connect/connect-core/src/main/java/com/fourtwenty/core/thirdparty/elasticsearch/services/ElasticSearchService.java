package com.fourtwenty.core.thirdparty.elasticsearch.services;

import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.product.ProductBatch;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.models.views.MemberLimitedView;
import com.fourtwenty.core.importer.model.ProductBatchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.ProductCustomResult;
import com.fourtwenty.core.thirdparty.elasticsearch.models.ElasticSearchSyncResult;

public interface ElasticSearchService {
    SearchResult<Transaction> searchTransactions(String term, int start, int limit, String sortBy, String sortByDirection);

    SearchResult<MemberLimitedView> searchMembers(String term, int start, int limit, String sortBy, String sortByDirection);

    SearchResult<ProductCustomResult> searchProducts(String term, int start, int limit, String sortBy, String sortByDirection);

    SearchResult<ProductBatchResult> searchProductBatches(String term, int start, int limit, String sortBy, String sortByDirection, ProductBatch.BatchStatus status);



    ElasticSearchSyncResult<Transaction> resyncTransactions(String companyId);

    ElasticSearchSyncResult<Member> resyncMembers(String companyId);

    ElasticSearchSyncResult<ProductCustomResult> resyncProducts(String companyId);

    void resyncProductBatches(String companyId);

}
