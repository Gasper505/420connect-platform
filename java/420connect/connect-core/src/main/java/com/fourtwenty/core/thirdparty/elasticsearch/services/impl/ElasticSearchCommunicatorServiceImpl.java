package com.fourtwenty.core.thirdparty.elasticsearch.services.impl;

import com.amazonaws.*;
import com.amazonaws.http.AmazonHttpClient;
import com.amazonaws.http.HttpMethodName;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.domain.models.generic.BaseModel;
import com.fourtwenty.core.domain.models.product.ProductBatch;
import com.fourtwenty.core.exceptions.BlazeOperationException;
import com.fourtwenty.core.thirdparty.elasticsearch.AWSResponseWrapper;
import com.fourtwenty.core.thirdparty.elasticsearch.ResponseExceptionHandler;
import com.fourtwenty.core.thirdparty.elasticsearch.ResponseSuccessHandler;
import com.fourtwenty.core.thirdparty.elasticsearch.models.ElasticSearchCapable;
import com.fourtwenty.core.thirdparty.elasticsearch.models.request.SearchRequestBuilder;
import com.fourtwenty.core.thirdparty.elasticsearch.models.response.*;
import com.fourtwenty.core.thirdparty.elasticsearch.services.ElasticSearchCommunicatorService;
import com.fourtwenty.core.thirdparty.elasticsearch.util.ElasticSearchBulkJSONCollector;
import com.fourtwenty.core.thirdparty.elasticsearch.util.ElasticSearchUtil;
import com.fourtwenty.core.util.AmazonServiceFactory;
import io.dropwizard.setup.Environment;
import org.apache.commons.lang3.StringUtils;
import org.assertj.core.util.Lists;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.TermsQueryBuilder;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.ByteArrayInputStream;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

public class ElasticSearchCommunicatorServiceImpl implements ElasticSearchCommunicatorService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ElasticSearchCommunicatorServiceImpl.class);

    private final ConnectConfiguration connectConfiguration;
    private final AmazonServiceFactory.AmazonRequestSigner amazonRequestSigner;
    private final ObjectMapper objectMapper;
    private static final Map<String, String> headers = new HashMap<>();

    static {
        headers.put("Content-Type", "application/json");
    }


    @Inject
    public ElasticSearchCommunicatorServiceImpl(Environment environment, ConnectConfiguration connectConfiguration, AmazonServiceFactory amazonServiceFactory) {
        this.connectConfiguration = connectConfiguration;
        amazonRequestSigner = amazonServiceFactory.getAmazonRequestSigner();
        objectMapper = environment.getObjectMapper();
    }

    private URI getEndpoint(String path) {
        return URI.create(connectConfiguration.getAmazonS3Config().getEndpoint() + path);
    }

    private Request<?> buildRequest(HttpMethodName httpMethodName, String content, URI endpoint, Map<String, String> params) {
        final Request<?> request = new DefaultRequest<>(connectConfiguration.getAmazonS3Config().getServiceName());
        request.setHttpMethod(httpMethodName);
        request.setEndpoint(endpoint);
        if (params != null) {
            for (Map.Entry<String, String> entry : params.entrySet()) {
                request.addParameter(entry.getKey(), entry.getValue());
            }
        } else {
            request.setHeaders(headers);
            request.setContent(new ByteArrayInputStream(content.getBytes(StandardCharsets.UTF_8)));
        }
        return request;
    }

    private void signRequest(final Request request) {
        amazonRequestSigner.signRequest(request);
    }

    private Response<AWSResponseWrapper> sendRequest(final Request request) {
        signRequest(request);
        final ClientConfiguration clientConfiguration = new ClientConfiguration();
        final AmazonHttpClient client = new AmazonHttpClient(clientConfiguration);
        return client
                .requestExecutionBuilder()
                .request(request)
                .errorResponseHandler(new ResponseExceptionHandler())
                .execute(new ResponseSuccessHandler());
    }

    @Override
    public AWSDocumentResponse createOrUpdateIndexedDocument(final String index, final String type, final BaseModel data, JSONObject jsonObject) {
        try {
            final String path = injectEnvironment(index) + "/" + type + "/" + data.getId();
            final Request<?> request = buildRequest(HttpMethodName.PUT, getJsonString(jsonObject), getEndpoint(path), null);
            final AWSDocumentResponse response = new AWSDocumentResponse(objectMapper, sendRequest(request).getAwsResponse());
            response.load();
            return response;
        } catch (AmazonServiceException e) {
            throw new BlazeOperationException("Exception from elasticsearch : " + e.getErrorCode(), e);
        } catch (Exception e) {
            throw new BlazeOperationException("Exception while parsing response ", e);
        }
    }

    @Override
    public AWSDocumentResponse createOrUpdateIndexedDocuments(List<ElasticSearchCapable> objs) {
        try {
            LOGGER.info("syncing end: " + objs.size());
            final String jsonString = (String) objs
                    .stream()
                    .collect(new ElasticSearchBulkJSONCollector((Function<String, String>) this::injectEnvironment));

            final Request<?> request = buildRequest(HttpMethodName.POST, jsonString, getEndpoint("_bulk"), null);
            final AWSDocumentResponse response = new AWSDocumentResponse(objectMapper, sendRequest(request).getAwsResponse());
            response.load();
            LOGGER.info("syncing end: " + objs.size());
            return response;
        } catch (AmazonServiceException e) {
            throw new BlazeOperationException("Exception from elasticsearch : " + e.getErrorCode(), e);
        } catch (Exception e) {
            throw new BlazeOperationException("Exception while parsing response ", e);
        }
    }

    public AWSSearchResponse searchIndexedDocument(final String companyId, final String shopId, final String index, final String type, String term) {
        try {
            if (term == null) {
                term = "*";
            }

            // Build JSON Payload
            BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();
            boolQuery.should(QueryBuilders.matchQuery("_all", term));

            // Multi-tenant Filtering
            boolQuery.must(QueryBuilders.termQuery("companyId", companyId));
            if (StringUtils.isNotEmpty(shopId))
                boolQuery.must(QueryBuilders.termQuery("shopId", shopId));

            // Prepare Query
            String content = ElasticSearchUtil.prepareQuery(boolQuery);

            final Request<?> request = buildRequest(HttpMethodName.POST, boolQuery.toString(), getEndpoint(index + "/" + type + "/_search"), null);
            final AWSSearchResponse response = new AWSSearchResponse(objectMapper, sendRequest(request).getAwsResponse());
            response.load();
            return response;
        } catch (AmazonServiceException e) {
            throw new BlazeOperationException("Exception from elasticsearch : " + e.getErrorCode(), e);
        } catch (Exception e) {
            throw new BlazeOperationException("Exception while parsing response ", e);
        }
    }

    @Override
    public AWSSearchResponse searchIndexedDocument(final String index, final String type, String term, SearchRequestBuilder searchRequestBuilder, final int start, int limit) {
        try {
            if (limit == 0) {
                limit = 30 - start;
            }

            if (term == null) {
                term = "*";
            }

            final Map<String, String> params = new HashMap<>();
            if (searchRequestBuilder == null)
                params.put("q", term + "*");
            else
                params.put("q", searchRequestBuilder.build(term));

            params.put("from", String.valueOf(start));
            params.put("size", String.valueOf(limit));
            final Request<?> request = buildRequest(HttpMethodName.GET, "", getEndpoint(index + "/" + type + "/_search"), params);
            final AWSSearchResponse response = new AWSSearchResponse(objectMapper, sendRequest(request).getAwsResponse());
            response.load();
            return response;
        } catch (AmazonServiceException e) {
            throw new BlazeOperationException("Exception from elasticsearch : " + e.getErrorCode(), e);
        } catch (Exception e) {
            throw new BlazeOperationException("Exception while parsing response ", e);
        }
    }

    /*
    {
      "query": {
        "bool": {
          "should": [
            { "match": { "_all": "ry" }}
          ],
          "filter": [
            { "term":  { "companyId": "56ce8bf389288da6df3a6602" }},
            { "term":  { "shopId": "56cf846ee38179985229e59e" }}
          ]
        }
      },
      "sort": { "lastName": "desc" },
      "from": 0,
      "size": 100
    }
    */
    @Override
    public AWSSearchResponse searchIndexedDocument(final String companyId, final String shopId, final String index, final String type, String term, final List<SearchField> fields, final int start, int limit, String sortBy, String sortByDirection, ProductBatch.BatchStatus status) {
        try {
            if (limit == 0) {
                limit = 30 - start;
            }

            // Build JSON Payload
            BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();
            String[] terms = {};
            if (term != null) {
                terms = term.split(" ");
            }

            if (StringUtils.isBlank(term) && fields != null && fields.size() > 0) {
                boolQuery.should(QueryBuilders.wildcardQuery(fields.get(0).fieldName, "*"));
            } else {
                if (StringUtils.isNotBlank(term)) {
                    String qTerm = "*" + term.replaceAll(" ", "") + "*";
                    for (SearchField field : fields) {
                        boolQuery.should(QueryBuilders.wildcardQuery(field.fieldName, qTerm).boost(field.boost * 3f));
                        for (String term2 : terms) {
                            boolQuery.should(QueryBuilders.prefixQuery(field.fieldName, term2).boost(field.boost));
                        }
                    }
                }
            }

            boolQuery.minimumShouldMatch(1);

            // Multi-tenant Filtering
            boolQuery.must(QueryBuilders.termQuery("companyId", companyId));
            if (StringUtils.isNotBlank(shopId))
                boolQuery.must(QueryBuilders.termQuery("shopId", shopId));

            boolQuery.must(QueryBuilders.termQuery("deleted", false));
            if (status != null) {
                boolQuery.must(QueryBuilders.termQuery("status", status.toString()));
            }

            List<String> sortByList = (StringUtils.isNotBlank(sortBy)) ? Lists.newArrayList(sortBy.split("\\s*,\\s*")) : new ArrayList<>();
            // Prepare Query
            String content = ElasticSearchUtil.prepareQuery(boolQuery, start, limit, sortByList, sortByDirection);

            LOGGER.info("content: " + content);
            final String path = injectEnvironment(index) + "/" + type + "/_search";
            final Request<?> request = buildRequest(HttpMethodName.POST, content, getEndpoint(path), null);
            final AWSSearchResponse response = new AWSSearchResponse(objectMapper, sendRequest(request).getAwsResponse());
            response.load();
            return response;
        } catch (AmazonServiceException e) {
            throw new BlazeOperationException("Exception from elasticsearch : " + e.getErrorCode(), e);
        } catch (Exception e) {
            throw new BlazeOperationException("Exception while parsing response ", e);
        }
    }

    @Override
    public AWSDocumentResponse deleteIndexedDocument(final String index, final String type, final String id) {
        try {
            final String path = injectEnvironment(index) + "/" + type + "/" + id;
            final Request<?> request = buildRequest(HttpMethodName.DELETE, "", getEndpoint(path), null);
            final AWSDocumentResponse response = new AWSDocumentResponse(objectMapper, sendRequest(request).getAwsResponse());
            response.load();
            return response;
        } catch (AmazonServiceException e) {
            throw new BlazeOperationException("Exception from elasticsearch : " + e.getErrorCode(), e);
        } catch (Exception e) {
            throw new BlazeOperationException("Exception while parsing response ", e);
        }
    }

    @Override
    public AWSCommonResponse deleteIndex(final String index) {
        try {
            final String path = injectEnvironment(index);
            final Request<?> request = buildRequest(HttpMethodName.DELETE, "", getEndpoint(path), null);
            final AWSCommonResponse response = new AWSCommonResponse(objectMapper, sendRequest(request).getAwsResponse());
            response.load();
            return response;
        } catch (AmazonServiceException e) {
            throw new BlazeOperationException("Exception from elasticsearch : " + e.getErrorCode(), e);
        } catch (Exception e) {
            throw new BlazeOperationException("Exception while parsing response ", e);
        }
    }

    @Override
    public AWSCommonResponse updateIndex(final String index, final String type, final String mappings) {
        try {
            final String path = injectEnvironment(index) + "/_mapping/" + type;
            final Request<?> request = buildRequest(HttpMethodName.PUT, mappings, getEndpoint(path), null);
            final AWSCommonResponse response = new AWSCommonResponse(objectMapper, sendRequest(request).getAwsResponse());
            response.load();
            return response;
        } catch (AmazonServiceException e) {
            throw new BlazeOperationException("Exception from elasticsearch : " + e.getErrorCode(), e);
        } catch (Exception e) {
            throw new BlazeOperationException("Exception while parsing response ", e);
        }
    }

    @Override
    public AWSIndexCreationResponse createIndex(final String index, final String mappings) {
        try {
            final String path = injectEnvironment(index);
            final Request<?> request = buildRequest(HttpMethodName.PUT, mappings, getEndpoint(path), null);
            final AWSIndexCreationResponse response = new AWSIndexCreationResponse(objectMapper, sendRequest(request).getAwsResponse());
            response.load();
            return response;
        } catch (AmazonServiceException e) {
            throw new BlazeOperationException("Exception from elasticsearch : " + e.getErrorCode(), e);
        } catch (Exception e) {
            throw new BlazeOperationException("Exception while parsing response ", e);
        }
    }

    @Override
    public AWSMappingResponse getIndexMappings(final String index, final String type) {
        try {
            final String path = injectEnvironment(index) + "/_mapping";
            final Request<?> request = buildRequest(HttpMethodName.GET, "", getEndpoint(path), null);
            final AWSMappingResponse response = new AWSMappingResponse(index, type, objectMapper, sendRequest(request).getAwsResponse());
            response.load();
            return response;
        } catch (AmazonServiceException e) {
            throw new BlazeOperationException("Exception from elasticsearch : " + e.getErrorCode(), e);
        } catch (Exception e) {
            throw new BlazeOperationException("Exception while parsing response ", e);
        }
    }

    @Override
    public AWSDeleteResponse deleteIndexedDocumentsFor(String index, String type, String companyId) {
        try {
            // Build JSON Payload
            BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();
            boolQuery.must(QueryBuilders.termQuery("companyId", companyId));

            // Prepare Query
            String content = ElasticSearchUtil.prepareQuery(boolQuery);

            final String path = injectEnvironment(index) + "/_delete_by_query";
            final Request<?> request = buildRequest(HttpMethodName.POST, content, getEndpoint(path), null);
            final AWSDeleteResponse response = new AWSDeleteResponse(objectMapper, sendRequest(request).getAwsResponse());
            response.load();
            return response;
        } catch (AmazonServiceException e) {
            throw new BlazeOperationException("Exception from elasticsearch : " + e.getErrorCode(), e);
        } catch (Exception e) {
            throw new BlazeOperationException("Exception while parsing response ", e);
        }
    }

    private String getJsonString(final JSONObject obj) {
        return obj.toString();
    }


    private String injectEnvironment(String indexName) {
        return String.format("%s_%s", connectConfiguration.getEnv(), indexName).toLowerCase();
    }

    @Override
    public AWSSearchResponse searchIndexedDocument(String companyId, String shopId, String index, String type, String term, List<SearchField> fields, int start, int limit, String sortBy, String sortByDirection) {
        return searchIndexedDocument(companyId, shopId, index, type, term, fields, start, limit, sortBy, sortByDirection, null);
    }

    @Override
    public AWSDeleteResponse deleteIndexedDocumentsFor(String index, String type, List<String> ids) {
        try {
            // Build JSON Payload
            TermsQueryBuilder builder = QueryBuilders.termsQuery("_id", ids);
            // Prepare Query
            String content = ElasticSearchUtil.prepareDeleteQuery(builder);

            final String path = injectEnvironment(index) + "/_delete_by_query";
            final Request<?> request = buildRequest(HttpMethodName.POST, content, getEndpoint(path), null);
            final AWSDeleteResponse response = new AWSDeleteResponse(objectMapper, sendRequest(request).getAwsResponse());
            response.load();
            return response;
        } catch (AmazonServiceException e) {
            throw new BlazeOperationException("Exception from elasticsearch : " + e.getErrorCode(), e);
        } catch (Exception e) {
            throw new BlazeOperationException("Exception while parsing response ", e);
        }
    }

    public static void main(String[] args) {

    }
}
