package com.fourtwenty.core.thirdparty.elasticsearch.services.impl;

import com.fourtwenty.core.domain.models.company.CompanyBaseModel;
import com.fourtwenty.core.domain.models.generic.BaseModel;
import com.fourtwenty.core.exceptions.BlazeOperationException;
import com.fourtwenty.core.thirdparty.elasticsearch.models.ElasticSearchCapable;
import com.fourtwenty.core.thirdparty.elasticsearch.models.ElasticSearchIndex;
import com.fourtwenty.core.thirdparty.elasticsearch.models.response.AWSDeleteResponse;
import com.fourtwenty.core.thirdparty.elasticsearch.models.response.AWSDocumentResponse;
import com.fourtwenty.core.thirdparty.elasticsearch.services.ElasticSearchCommunicatorService;
import com.fourtwenty.core.thirdparty.elasticsearch.services.ElasticSearchIndexingService;
import com.google.inject.Injector;
import org.json.JSONObject;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.List;

public class ElasticSearchIndexingServiceImpl implements ElasticSearchIndexingService {

    private ElasticSearchCommunicatorService communicatorService;

    @Inject
    private Injector injector;

    @Inject
    public ElasticSearchIndexingServiceImpl(ElasticSearchCommunicatorService communicatorService) {
        this.communicatorService = communicatorService;
    }

    @Override
    public <E extends CompanyBaseModel> AWSDocumentResponse createOrUpdateIndexedDocument(ElasticSearchCapable obj, JSONObject data) {
        final ElasticSearchIndex index = obj.getElasticSearchIndex();
        return communicatorService.createOrUpdateIndexedDocument(index.getIndex(), index.getType(), (BaseModel) obj, data);
    }

    @Override
    public <E extends CompanyBaseModel> AWSDocumentResponse createOrUpdateIndexedDocuments(List<ElasticSearchCapable> data) {
        return communicatorService.createOrUpdateIndexedDocuments(data);
    }

    @Override
    public <E extends CompanyBaseModel> AWSDocumentResponse deleteIndexedDocument(String id, Class<E> clazz) {
        final List<String> indexAndType = getIndexAndType(clazz);
        return communicatorService.deleteIndexedDocument(indexAndType.get(0), indexAndType.get(1), id);
    }

    @Override
    public List<String> getIndexAndType(Class<? extends BaseModel> clazz) {
        // Code based approach
        if (ElasticSearchCapable.class.isAssignableFrom(clazz)) {
            ElasticSearchCapable definition = (ElasticSearchCapable) injector.getInstance(clazz);
            if (definition != null) {
                ElasticSearchIndex index = definition.getElasticSearchIndex();
                return Arrays.asList(index.getIndex(), index.getType());
            }
        }

        throw new BlazeOperationException("Unable to find ElasticSearch index.");
    }

    @Override
    public <E extends CompanyBaseModel> AWSDeleteResponse deleteIndexedDocumentsFor(String companyId, Class<E> clazz) {
        final List<String> indexAndType = getIndexAndType(clazz);
        return communicatorService.deleteIndexedDocumentsFor(indexAndType.get(0), indexAndType.get(1), companyId);
    }

    @Override
    public <E extends CompanyBaseModel> AWSDeleteResponse deleteIndexedDocumentsFor(List<String> ids, Class<E> clazz) {
        final List<String> indexAndType = getIndexAndType(clazz);
        return communicatorService.deleteIndexedDocumentsFor(indexAndType.get(0), indexAndType.get(1), ids);
    }
}
