package com.fourtwenty.core.thirdparty.elasticsearch.services.impl;

import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.product.Brand;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductBatch;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.models.views.MemberLimitedView;
import com.fourtwenty.core.domain.repositories.dispensary.BrandRepository;
import com.fourtwenty.core.domain.repositories.dispensary.InventoryRepository;
import com.fourtwenty.core.domain.repositories.dispensary.MemberRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductBatchRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ProductRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.domain.repositories.dispensary.TransactionRepository;
import com.fourtwenty.core.domain.repositories.dispensary.VendorRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.exceptions.BlazeOperationException;
import com.fourtwenty.core.importer.model.ProductBatchResult;
import com.fourtwenty.core.managed.ElasticSearchManager;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.ProductCustomResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.mgmt.MemberService;
import com.fourtwenty.core.services.mgmt.ProductBatchService;
import com.fourtwenty.core.services.mgmt.impl.ProductServiceImpl;
import com.fourtwenty.core.thirdparty.elasticsearch.models.ElasticSearchSyncResult;
import com.fourtwenty.core.thirdparty.elasticsearch.models.response.AWSSearchResponse;
import com.fourtwenty.core.thirdparty.elasticsearch.services.ElasticSearchCommunicatorService;
import com.fourtwenty.core.thirdparty.elasticsearch.services.ElasticSearchIndexingService;
import com.fourtwenty.core.thirdparty.elasticsearch.services.ElasticSearchService;
import com.fourtwenty.core.util.BLStringUtils;
import com.google.inject.Provider;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ElasticSearchServiceImpl extends AbstractAuthServiceImpl implements ElasticSearchService {
    private static final int ELASTIC_SEARCH_LIMIT = 10000;
    private static final Log LOG = LogFactory.getLog(ElasticSearchServiceImpl.class);


    private ElasticSearchCommunicatorService communicatorService;
    private TransactionRepository transactionRepository;
    private MemberRepository memberRepository;
    private ProductRepository productRepository;
    private MemberService memberService;
    private ProductServiceImpl productServiceImpl;
    private ElasticSearchIndexingService elasticSearchIndexingService;
    private InventoryRepository inventoryRepository;

    private ElasticSearchManager elasticSearchManager;
    private ProductBatchRepository productBatchRepository;
    @Inject
    private VendorRepository vendorRepository;
    @Inject
    private BrandRepository brandRepository;
    @Inject
    private ShopRepository shopRepository;
    @Inject
    private ProductBatchService productBatchService;

    @Inject
    public ElasticSearchServiceImpl(Provider<ConnectAuthToken> token, ElasticSearchCommunicatorService communicatorService,
                                    TransactionRepository transactionRepository, MemberRepository memberRepository, ProductRepository productRepository,
                                    MemberService memberService, ProductServiceImpl productServiceImpl, ElasticSearchIndexingService elasticSearchIndexingService,
                                    InventoryRepository inventoryRepository, ElasticSearchManager elasticSearchManager, ProductBatchRepository productBatchRepository) {
        super(token);
        this.communicatorService = communicatorService;
        this.transactionRepository = transactionRepository;
        this.memberRepository = memberRepository;
        this.productRepository = productRepository;
        this.memberService = memberService;
        this.productServiceImpl = productServiceImpl;
        this.elasticSearchIndexingService = elasticSearchIndexingService;
        this.inventoryRepository = inventoryRepository;
        this.elasticSearchManager = elasticSearchManager;
        this.productBatchRepository = productBatchRepository;
    }

    @Override
    public SearchResult<Transaction> searchTransactions(String term, int start, int limit, String sortBy, String sortByDirection) {
        final List<String> indexAndType = elasticSearchIndexingService.getIndexAndType(Transaction.class);

        final SearchResult<Transaction> searchResult = new SearchResult<>();
        searchResult.setLimit(limit);
        searchResult.setSkip(start);
        List<ElasticSearchCommunicatorService.SearchField> fields = new ArrayList<>();
        final AWSSearchResponse awsSearchResponse = communicatorService.searchIndexedDocument(token.getCompanyId(), token.getShopIdByShareOption(), indexAndType.get(0), indexAndType.get(1), term, fields, start, limit, sortBy, sortByDirection);
        searchResult.setTotal(awsSearchResponse.getTotal());
        searchResult.setSkip(start);
        searchResult.setLimit(limit);
        searchResult.setValues(awsSearchResponse.populateObjects(Transaction.class));

        return searchResult;
    }

    @Override
    public SearchResult<MemberLimitedView> searchMembers(String term, int start, int limit, String sortBy, String sortByDirection) {

        if (limit > ELASTIC_SEARCH_LIMIT)
            throw new BlazeInvalidArgException("limit", String.format("The limit parameter must be smaller than %s", ELASTIC_SEARCH_LIMIT));

        if (sortByDirection == null || (!sortByDirection.equalsIgnoreCase("asc") && !sortByDirection.equalsIgnoreCase("desc"))) {
            sortByDirection = "asc";
        }

        final List<String> indexAndType = elasticSearchIndexingService.getIndexAndType(Member.class);

        SearchResult<MemberLimitedView> limitedViewResult = new SearchResult<>();
        final String[] fields2 = new String[]{
                "firstName_l",
                "lastName_l",
                "email_l",
                "primaryPhone_l",
                "license_l",
                "memberGroup_l",
                "status_l"
        };
        List<ElasticSearchCommunicatorService.SearchField> fields = new ArrayList<>();
        fields.add(new ElasticSearchCommunicatorService.SearchField("firstName_l", 10));
        fields.add(new ElasticSearchCommunicatorService.SearchField("lastName_l", 10));
        fields.add(new ElasticSearchCommunicatorService.SearchField("email_l", 5));
        fields.add(new ElasticSearchCommunicatorService.SearchField("primaryPhone_l", 5));
        fields.add(new ElasticSearchCommunicatorService.SearchField("license_l", 5));
        fields.add(new ElasticSearchCommunicatorService.SearchField("memberGroup_l", 1));
        fields.add(new ElasticSearchCommunicatorService.SearchField("status_l", 1));
        fields.add(new ElasticSearchCommunicatorService.SearchField("recommendationNo_l", 1));

        if (term != null) {
            term = BLStringUtils.stripNonAlphaNumericChars(term.toLowerCase());
        }

        LOG.info("Search Term: " + term);

        final AWSSearchResponse awsSearchResponse = communicatorService.searchIndexedDocument(token.getCompanyId(), token.getShopIdByShareOption(), indexAndType.get(0), indexAndType.get(1), term, fields, start, limit, sortBy, sortByDirection);

        limitedViewResult.setTotal(awsSearchResponse.getTotal());
        limitedViewResult.setSkip(start);
        limitedViewResult.setLimit(limit);
        limitedViewResult.setValues(awsSearchResponse.populateObjects(MemberLimitedView.class));
        return limitedViewResult;
    }

    @Override
    public SearchResult<ProductCustomResult> searchProducts(String term, int start, int limit, String sortBy, String sortByDirection) {
        final List<String> indexAndType = elasticSearchIndexingService.getIndexAndType(Product.class);

        SearchResult<ProductCustomResult> productCustomSearchResult = new SearchResult<>();

        final SearchResult<ProductCustomResult> searchResult = new SearchResult<>();
        searchResult.setLimit(limit);
        searchResult.setSkip(start);

        List<ElasticSearchCommunicatorService.SearchField> fields = new ArrayList<>();
        final AWSSearchResponse awsSearchResponse = communicatorService.searchIndexedDocument(token.getCompanyId(), token.getShopIdByShareOption(), indexAndType.get(0), indexAndType.get(1), term, fields, start, limit, sortBy, sortByDirection);
        searchResult.setTotal(awsSearchResponse.getTotal());
        searchResult.setValues(awsSearchResponse.populateObjects(ProductCustomResult.class));

        searchResult.setSkip(start);
        searchResult.setLimit(limit);

        return productCustomSearchResult;
    }


    public ElasticSearchSyncResult<Transaction> resyncTransactions(String companyId) {

        ElasticSearchSyncResult result = new ElasticSearchSyncResult();

        List<Transaction> transactions = transactionRepository.getTransactions(companyId);

        elasticSearchManager.deleteIndexedDocumentsFor(companyId, Transaction.class);
        elasticSearchManager.waitUntilDone();
        elasticSearchManager.createOrUpdateIndexedDocuments(transactions, 50);
        elasticSearchManager.waitUntilDone();

        result.setTotalFound(transactions.size());
        result.setRecords(transactions);

        return result;
    }

    public ElasticSearchSyncResult<Member> resyncMembers(String companyId) {

        ElasticSearchSyncResult result = new ElasticSearchSyncResult();

        List<Member> members = memberRepository.getMembers(companyId);

        try {
            elasticSearchManager.deleteIndexedDocumentsFor(companyId, Member.class);
        } catch (Exception e) {
            LOG.error("Failed deleting documents..");
        }
        elasticSearchManager.waitUntilDone();
        elasticSearchManager.createOrUpdateIndexedDocuments(members, 50);
        elasticSearchManager.waitUntilDone();

        result.setTotalFound(members.size());
        result.setRecords(members);

        return result;
    }

    public ElasticSearchSyncResult<ProductCustomResult> resyncProducts(String companyId) {

        throw new BlazeOperationException("Not yet implemented.");
    }

    @Override
    public SearchResult<ProductBatchResult> searchProductBatches(String term, int start, int limit, String sortBy, String sortByDirection, ProductBatch.BatchStatus batchStatus) {
        final List<String> indexAndType = elasticSearchIndexingService.getIndexAndType(ProductBatch.class);

        final SearchResult<ProductBatchResult> searchResult = new SearchResult<>();

        List<ElasticSearchCommunicatorService.SearchField> fields = new ArrayList<>();
        fields.add(new ElasticSearchCommunicatorService.SearchField("sku_l", 10));
        fields.add(new ElasticSearchCommunicatorService.SearchField("vendorName_l", 10));
        fields.add(new ElasticSearchCommunicatorService.SearchField("productName_l", 10));
        fields.add(new ElasticSearchCommunicatorService.SearchField("brandName_l", 10));
        fields.add(new ElasticSearchCommunicatorService.SearchField("status_l", 10));

        if (term != null) {
            term = BLStringUtils.stripNonAlphaNumericChars(term.toLowerCase());
        }

        LOG.info("Search Term: " + term);

        if (StringUtils.isBlank(sortBy)) {
            sortBy = "modified";
        }
        if (StringUtils.isBlank(sortByDirection)) {
            sortByDirection = "desc";
        }

        final AWSSearchResponse awsSearchResponse = communicatorService.searchIndexedDocument(token.getCompanyId(), token.getShopId(), indexAndType.get(0), indexAndType.get(1), term, fields, start, limit, sortBy, sortByDirection, batchStatus);
        searchResult.setTotal(awsSearchResponse.getTotal());
        searchResult.setValues(awsSearchResponse.populateObjects(ProductBatchResult.class));


        searchResult.setSkip(start);
        searchResult.setLimit(limit);

        productBatchService.prepareProductBatchResult(searchResult.getValues());
        return searchResult;
    }

    @Override
    public void resyncProductBatches(String companyId) {
        HashMap<String, Shop> shopHashMap = shopRepository.listAsMap(companyId);
        HashMap<String, Vendor> vendorHashMap = vendorRepository.getLimitedVendorViewAsMap(companyId);
        HashMap<String, Brand> brandHashMap = brandRepository.getLimitedBrandViewAsMap(companyId);

        try {
            elasticSearchManager.deleteIndexedDocumentsFor(companyId, ProductBatch.class);
        } catch (Exception e) {
            LOG.error("Failed deleting documents..");
        }
        elasticSearchManager.waitUntilDone();

        for (String shopId : shopHashMap.keySet()) {
            Shop shop = shopHashMap.get(shopId);
            if (shop != null) {
                HashMap<String, Product> productHashMap = productRepository.getLimitedProductViewAsMap(companyId, shop.getId());
                List<ProductBatch> productBatches = productBatchRepository.getLimitedBatchViewAsMap(companyId, shop.getId());
                if (productBatches != null && productBatches.size() > 0) {
                    for (ProductBatch productBatchDetail : productBatches) {
                        Product product = productHashMap.get(productBatchDetail.getProductId());
                        Vendor vendor = vendorHashMap.get(productBatchDetail.getVendorId());
                        Brand brand = brandHashMap.get(productBatchDetail.getBrandId());
                        productBatchDetail.setProductName(product != null ? product.getName() : "");
                        productBatchDetail.setVendorName(vendor != null ? vendor.getName() : "");
                        productBatchDetail.setBrandName(brand != null ? brand.getName() : "");
                    }
                    elasticSearchManager.createOrUpdateIndexedDocuments(productBatches, 50);
                    elasticSearchManager.waitUntilDone();
                }
            } else {
                LOG.info("Shop for shopId : " + shopId + "does not exist.");
            }
        }
    }
}
