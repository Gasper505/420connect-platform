package com.fourtwenty.core.thirdparty.elasticsearch.util;

import com.fourtwenty.core.domain.models.generic.BaseModel;
import com.fourtwenty.core.thirdparty.elasticsearch.models.ElasticSearchCapable;
import com.fourtwenty.core.thirdparty.elasticsearch.models.ElasticSearchIndex;
import org.json.JSONObject;

import java.util.EnumSet;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;

public class ElasticSearchBulkJSONCollector<T extends BaseModel & ElasticSearchCapable>
        implements Collector<T, StringBuilder, String> {

    private Function<String, String> indexFormatter;

    public ElasticSearchBulkJSONCollector(Function<String, String> indexFormatter) {
        this.indexFormatter = indexFormatter;
    }

    @Override
    public Supplier<StringBuilder> supplier() {
        return StringBuilder::new;
    }

    @Override
    public BiConsumer<StringBuilder, T> accumulator() {
        return (builder, obj) -> {
            ElasticSearchIndex index = obj.getElasticSearchIndex();
            JSONObject jsonObject = obj.toElasticSearchObject();
            builder.append("{\"update\": {\"_id\": \"" + obj.getId() + "\", \"_type\": \"" + index.getType() + "\", \"_index\": \"" + indexFormatter.apply(index.getIndex()) + "\"}}\n");
            builder.append("{\"doc\": " + jsonObject.toString() + ", \"doc_as_upsert\" : true}\n");
        };
    }

    @Override
    public BinaryOperator<StringBuilder> combiner() {
        return (builder1, builder2) -> {
            builder1.append(builder2);
            return builder1;
        };
    }

    @Override
    public Function<StringBuilder, String> finisher() {
        return (builder) -> builder.toString();
    }

    @Override
    public Set<Characteristics> characteristics() {
        return EnumSet.of(Characteristics.UNORDERED);
    }
}
