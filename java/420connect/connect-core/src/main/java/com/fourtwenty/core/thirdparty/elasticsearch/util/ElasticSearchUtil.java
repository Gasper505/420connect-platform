package com.fourtwenty.core.thirdparty.elasticsearch.util;

import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.index.query.QueryBuilder;

import java.util.List;

public class ElasticSearchUtil {

    public static String prepareQuery(QueryBuilder queryBuilder) {
        return prepareQuery(queryBuilder, null, null, "", null);
    }

    public static String prepareQuery(QueryBuilder queryBuilder, Integer start, Integer limit, String sortBy,
                                      String sortByDirection) {

        // Hack to use ElasticSearch QueryBuilders with AWS API
        String content = "{";
        content += "\"query\": " + queryBuilder.toString();
        //content += ",\"min_score\": 1.0";
        if (StringUtils.isNotBlank(sortBy)) {
            content += ", \"sort\":";
            if (StringUtils.isNotBlank(sortByDirection)) {
                content += "{\"" + sortBy + "\": \"" + sortByDirection + "\"}";
            } else {
                content += "\"" + sortBy + "\"";
            }
        }
        if (start != null && limit != null) {
            content += ", \"from\": " + start + ", \"size\": " + limit;
        }

        content += "}";


        return content;
    }

    public static String prepareQuery(QueryBuilder queryBuilder, Integer start, Integer limit, List<String> sortByList,
                                      String sortByDirection) {

        // Hack to use ElasticSearch QueryBuilders with AWS API
        String content = "{";
        content += "\"query\": " + queryBuilder.toString();
        //content += ",\"min_score\": 1.0";
        if (!sortByList.isEmpty()) {
            content += ", \"sort\": [";
            String sortQuery = "";
            for (String sortBy: sortByList) {
                sortQuery += StringUtils.isNotBlank(sortQuery) ? ", \n " : "\n";
                if (StringUtils.isNotBlank(sortByDirection)) {
                    sortQuery += "{\"" + sortBy + "\": \"" + sortByDirection + "\"}";
                } else {
                    sortQuery += "\"" + sortBy + "\"";
                }
            }
            content += sortQuery + "]";
        }
        if (start != null && limit != null) {
            content += ", \"from\": " + start + ", \"size\": " + limit;
        }

        content += "}";


        return content;
    }

    public static String prepareDeleteQuery(QueryBuilder queryBuilder) {
        /* Hack to use ElasticSearch QueryBuilders with AWS API*/
        String content = "{";
        content += "\"query\": " + queryBuilder.toString();
        content += "}";


        return content;
    }
}
