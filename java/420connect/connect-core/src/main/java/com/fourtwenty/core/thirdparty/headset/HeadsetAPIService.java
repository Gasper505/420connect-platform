package com.fourtwenty.core.thirdparty.headset;

import com.fourtwenty.core.domain.models.common.HeadsetConfig;
import com.fourtwenty.core.domain.models.thirdparty.HeadsetLocation;
import com.fourtwenty.core.rest.dispensary.requests.thirdparty.*;
import com.fourtwenty.core.thirdparty.headset.models.*;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

/**
 * Created by Gaurav Saini on 4/7/17.
 */
public interface HeadsetAPIService {
    HeadsetMappingResult mapping(HeadsetConfig config, HeadsetLocation location) throws InterruptedException, ExecutionException, IOException;

    HeadsetBudTender addBudTender(HeadsetConfig config, HeadsetLocation location, BudTenderAddRequest request) throws InterruptedException, ExecutionException, IOException;

    HeadsetCustomer addCustomer(HeadsetConfig config, HeadsetLocation location, HeadsetCustomerAddRequest request) throws InterruptedException, ExecutionException, IOException;

    HeadsetProduct addProduct(HeadsetConfig config, HeadsetLocation location, HeadsetProductAddRequest request) throws InterruptedException, ExecutionException, IOException;

    String addSaleItem(HeadsetConfig config, HeadsetLocation location, TicketAddRequest request) throws InterruptedException, ExecutionException, IOException;

    HeadsetRestock restock(HeadsetConfig config, HeadsetLocation location, RestockAddRequest request) throws InterruptedException, ExecutionException, IOException;

    String updateInventory(HeadsetConfig config, HeadsetLocation location, String productId, String quantity) throws InterruptedException, ExecutionException, IOException;

    String getProductIdList(HeadsetConfig config, HeadsetLocation location);

    HeadsetProduct deleteProduct(HeadsetConfig config, HeadsetLocation location, String productId);

    String updateLastConnection(HeadsetConfig config, HeadsetLocation location, String date) throws InterruptedException, ExecutionException, IOException;

    Headset addAccount(HeadsetConfig config, HeadsetAddRequest request) throws InterruptedException, ExecutionException, IOException;

}
