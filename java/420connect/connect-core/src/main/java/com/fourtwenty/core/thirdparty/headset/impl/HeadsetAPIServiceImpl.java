package com.fourtwenty.core.thirdparty.headset.impl;

import com.fourtwenty.core.domain.models.common.HeadsetConfig;
import com.fourtwenty.core.domain.models.thirdparty.HeadsetLocation;
import com.fourtwenty.core.rest.dispensary.requests.thirdparty.*;
import com.fourtwenty.core.thirdparty.headset.HeadsetAPIService;
import com.fourtwenty.core.thirdparty.headset.models.*;
import com.mdo.pusher.SimpleRestUtil;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import java.io.IOException;
import java.util.concurrent.ExecutionException;

/**
 * Created by Gaurav Saini on 4/7/17.
 */
public class HeadsetAPIServiceImpl implements HeadsetAPIService {


    @Override
    public HeadsetBudTender addBudTender(final HeadsetConfig config, final HeadsetLocation location, BudTenderAddRequest request) throws InterruptedException, ExecutionException, IOException {


        String urlPath = config.getHost() + "/api/Budtender/save";
        MultivaluedMap<String, Object> headers = makeHeaders(location.getApiKey(), location.getDataSourceId());
        return SimpleRestUtil.post(urlPath, request, HeadsetBudTender.class, HeadsetError.class, headers);
    }

    @Override
    public HeadsetCustomer addCustomer(final HeadsetConfig config, final HeadsetLocation location, HeadsetCustomerAddRequest request) throws InterruptedException, ExecutionException, IOException {
        String urlPath = config.getHost() + "/api/Customer/save";
        MultivaluedMap<String, Object> headers = makeHeaders(location.getApiKey(), location.getDataSourceId());
        return SimpleRestUtil.post(urlPath, request, HeadsetCustomer.class, HeadsetError.class, headers);
    }

    @Override
    public HeadsetMappingResult mapping(final HeadsetConfig config, final HeadsetLocation location) throws InterruptedException, ExecutionException, IOException {
        String url = config.getHost() + "/api/mapping?token=" + location.getApiKey();
        return SimpleRestUtil.get(url, HeadsetMappingResult.class);
    }

    @Override
    public HeadsetProduct addProduct(final HeadsetConfig config, final HeadsetLocation location, HeadsetProductAddRequest request) throws InterruptedException, ExecutionException, IOException {
        String urlPath = config.getHost() + "/api/Product/save";
        MultivaluedMap<String, Object> headers = makeHeaders(location.getApiKey(), location.getDataSourceId());
        return SimpleRestUtil.post(urlPath, request, HeadsetProduct.class, HeadsetError.class, headers);
    }

    @Override
    public String updateInventory(final HeadsetConfig config, final HeadsetLocation location, String productId, String quantity) throws InterruptedException, ExecutionException, IOException {
        String urlPath = config.getHost() + "/api/Product/inventory?productId=" + productId + "&quantity=" + quantity;
        MultivaluedMap<String, Object> headers = makeHeaders(location.getApiKey(), location.getDataSourceId());
        return SimpleRestUtil.post(urlPath, "{}", String.class, HeadsetError.class, headers);
    }

    @Override
    public String getProductIdList(final HeadsetConfig config, final HeadsetLocation location) {
        String url = config.getHost() + "/api/Product";
        MultivaluedMap<String, Object> headers = makeHeaders(location.getApiKey(), location.getDataSourceId());
        return SimpleRestUtil.get(url, String.class, headers);
    }

    @Override
    public HeadsetProduct deleteProduct(final HeadsetConfig config, final HeadsetLocation location, String productId) {
        String url = config.getHost() + "/api/Product/" + productId;
        System.out.println("==============url====================" + url);
        MultivaluedMap<String, Object> headers = makeHeaders(location.getApiKey(), location.getDataSourceId());
        System.out.println("==============headers====================" + headers);

        return SimpleRestUtil.delete(url, HeadsetProduct.class, headers);
    }

    @Override
    public HeadsetRestock restock(final HeadsetConfig config, final HeadsetLocation location, RestockAddRequest request) throws InterruptedException, ExecutionException, IOException {
        String urlPath = config.getHost() + "/api/Restock";
        MultivaluedMap<String, Object> headers = makeHeaders(location.getApiKey(), location.getDataSourceId());
        return SimpleRestUtil.post(urlPath, request, HeadsetRestock.class, String.class, headers);
    }

    @Override
    public String addSaleItem(final HeadsetConfig config, final HeadsetLocation location, TicketAddRequest request) throws InterruptedException, ExecutionException, IOException {
        String urlPath = config.getHost() + "/api/Ticket/save";
        MultivaluedMap<String, Object> headers = makeHeaders(location.getApiKey(), location.getDataSourceId());
        return SimpleRestUtil.post(urlPath, request, String.class, String.class, headers);
    }

    @Override
    public String updateLastConnection(final HeadsetConfig config, final HeadsetLocation location, String date) throws InterruptedException, ExecutionException, IOException {
        String urlPath = config.getHost() + "/api/DataSource/lastconnection";
        MultivaluedMap<String, Object> headers = makeHeaders(location.getApiKey(), location.getDataSourceId());
        return SimpleRestUtil.post(urlPath, "\"" + date + "\"", String.class, HeadsetError.class, headers);
    }


    /*
    "errors": [
        "Timezone does not contain a valid value.  Options are: Hawaiian Standard Time,Alaskan Standard Time,Pacific Standard Time,Mountain Standard Time,US Mountain Standard Time,Central Standard Time,Eastern Standard Time"
    ]
     */
    @Override
    public Headset addAccount(final HeadsetConfig config, HeadsetAddRequest request) throws InterruptedException, ExecutionException, IOException {
        String urlPath = config.getHost() + "/api/account";
        MultivaluedMap<String, Object> headers = new MultivaluedHashMap<>();
        headers.putSingle("Content-Type", MediaType.APPLICATION_JSON);
        return SimpleRestUtil.post(urlPath, request, Headset.class, HeadsetError.class, headers);
    }

    private MultivaluedMap<String, Object> makeHeaders(String apiKey, String dataSourceId) {
        MultivaluedMap<String, Object> headers = new MultivaluedHashMap<>();
        headers.putSingle("X-ApiKey", apiKey);
        headers.putSingle("X-DataSourceId", dataSourceId);
        headers.putSingle("Content-Type", MediaType.APPLICATION_JSON);
        return headers;
    }
}