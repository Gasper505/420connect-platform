package com.fourtwenty.core.thirdparty.headset.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Gaurav Saini on 11/7/17.
 * {
 * "companyId": 0,
 * "apiKey": "string",
 * "integrationKey": "string"
 * }
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Headset {

    private Long companyId;
    private String apiKey;
    private String integrationKey;


    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public String getIntegrationKey() {
        return integrationKey;
    }

    public void setIntegrationKey(String integrationKey) {
        this.integrationKey = integrationKey;
    }
}
