package com.fourtwenty.core.thirdparty.headset.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Gaurav Saini on 3/7/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class HeadsetBudTender {

    private String budtenderId;
    private String email;
    private String firstName;
    private String lastName;

    public String getBudtenderId() {
        return budtenderId;
    }

    public void setBudtenderId(String budtenderId) {
        this.budtenderId = budtenderId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
