package com.fourtwenty.core.thirdparty.headset.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mdo on 7/18/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class HeadsetError {
    private List<String> errors = new ArrayList<>();


    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }

    public String getErrorsAsString() {
        StringBuilder sb = new StringBuilder();
        for (String err : errors) {
            sb.append(err);
            sb.append(";");
        }
        return sb.toString();
    }
}
