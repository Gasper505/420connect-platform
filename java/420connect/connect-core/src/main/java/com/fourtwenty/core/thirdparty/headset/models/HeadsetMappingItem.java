package com.fourtwenty.core.thirdparty.headset.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by mdo on 7/24/17.
 * <p>
 * [
 * {
 * "storeId": 0,
 * "dataSourceId": 0,
 * "externalId": "string",
 * "lastConnection": "2017-07-24T22:52:23.925Z"
 * }
 * ]
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class HeadsetMappingItem {
    private int storeId;
    private int dataSourceId;
    private String externalId;
    private String lastConnection;

    public int getDataSourceId() {
        return dataSourceId;
    }

    public void setDataSourceId(int dataSourceId) {
        this.dataSourceId = dataSourceId;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public String getLastConnection() {
        return lastConnection;
    }

    public void setLastConnection(String lastConnection) {
        this.lastConnection = lastConnection;
    }

    public int getStoreId() {
        return storeId;
    }

    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }
}
