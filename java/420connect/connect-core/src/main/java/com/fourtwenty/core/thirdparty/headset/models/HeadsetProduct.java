package com.fourtwenty.core.thirdparty.headset.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Gaurav Saini on 3/7/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class HeadsetProduct {

    private String productId = "";
    private String name = "";
    private int price;
    private String unit = "";
    private Integer quantityInStock;
    private String vendor = "";
    private String category = "";
    private boolean active;
    private boolean bulkItem;
    private String productType = "";

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Integer getQuantityInStock() {
        return quantityInStock;
    }

    public void setQuantityInStock(Integer quantityInStock) {
        this.quantityInStock = quantityInStock;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isBulkItem() {
        return bulkItem;
    }

    public void setBulkItem(boolean bulkItem) {
        this.bulkItem = bulkItem;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }
}
