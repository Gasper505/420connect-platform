package com.fourtwenty.core.thirdparty.headset.models;

/**
 * Created by Gaurav Saini on 3/7/17.
 */
public class HeadsetRestock {

    private String id;
    private String parentId;
    private String utcDate; // date-time
    private Integer quantity;
    private Integer costPerUnit;
    private String productId;
    private String inventoryId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getUtcDate() {
        return utcDate;
    }

    public void setUtcDate(String utcDate) {
        this.utcDate = utcDate;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getCostPerUnit() {
        return costPerUnit;
    }

    public void setCostPerUnit(Integer costPerUnit) {
        this.costPerUnit = costPerUnit;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(String inventoryId) {
        this.inventoryId = inventoryId;
    }
}
