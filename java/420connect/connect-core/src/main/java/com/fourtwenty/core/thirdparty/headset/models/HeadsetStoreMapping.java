package com.fourtwenty.core.thirdparty.headset.models;

/**
 * Created by Gaurav Saini on 3/7/17.
 */
public class HeadsetStoreMapping {

    private Integer storeId;
    private Integer dataSourceId;
    private String externalId;
    private String lastConnection;

    public Integer getStoreId() {
        return storeId;
    }

    public void setStoreId(Integer storeId) {
        this.storeId = storeId;
    }

    public Integer getDataSourceId() {
        return dataSourceId;
    }

    public void setDataSourceId(Integer dataSourceId) {
        this.dataSourceId = dataSourceId;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public String getLastConnection() {
        return lastConnection;
    }

    public void setLastConnection(String lastConnection) {
        this.lastConnection = lastConnection;
    }
}
