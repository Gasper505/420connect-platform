package com.fourtwenty.core.thirdparty.headset.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

/**
 * Created by Gaurav Saini on 3/7/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class HeadsetTicket {

    private String id = "";
    private String utcDate = "";
    private String localTime = "";

    public enum TicketType {
        Sale,
        Refund,
        Return,
        Void
    }

    private String customerReference = "";
    private String employeeReference = "";
    private String paymentType = "";
    private int total;
    private int tax;
    private int subTotal;
    private int discount;
    private List<HeadsetTicketItem> items;

    public String getCustomerReference() {
        return customerReference;
    }

    public void setCustomerReference(String customerReference) {
        this.customerReference = customerReference;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public String getEmployeeReference() {
        return employeeReference;
    }

    public void setEmployeeReference(String employeeReference) {
        this.employeeReference = employeeReference;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<HeadsetTicketItem> getItems() {
        return items;
    }

    public void setItems(List<HeadsetTicketItem> items) {
        this.items = items;
    }

    public String getLocalTime() {
        return localTime;
    }

    public void setLocalTime(String localTime) {
        this.localTime = localTime;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public int getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(int subTotal) {
        this.subTotal = subTotal;
    }

    public int getTax() {
        return tax;
    }

    public void setTax(int tax) {
        this.tax = tax;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public String getUtcDate() {
        return utcDate;
    }

    public void setUtcDate(String utcDate) {
        this.utcDate = utcDate;
    }
}
