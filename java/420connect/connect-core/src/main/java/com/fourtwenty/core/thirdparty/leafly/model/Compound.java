
package com.fourtwenty.core.thirdparty.leafly.model;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "type",
    "content",
    "unit"
})
public class Compound {

    public enum CompoundTypes {
        CBD,
        THC;
    }
    public enum CompoundUnits {
        PERCENT;
    }
    @JsonProperty("type")
    private String type;
    @JsonProperty("content")
    private Double content;
    @JsonProperty("unit")
    private String unit;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(CompoundTypes type) {
        this.type = type.toString().toLowerCase();
    }

    @JsonProperty("content")
    public Double getContent() {
        return content;
    }

    @JsonProperty("content")
    public void setContent(Double content) {
        this.content = content;
    }

    @JsonProperty("unit")
    public String getUnit() {
        return unit;
    }

    @JsonProperty("unit")
    public void setUnit(CompoundUnits unit) {
        this.unit = unit.toString().toLowerCase();
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
