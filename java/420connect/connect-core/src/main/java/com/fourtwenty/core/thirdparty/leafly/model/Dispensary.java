package com.fourtwenty.core.thirdparty.leafly.model;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "name",
    "menu_integration_key",
    "menu_integration_type",
    "menuSummary"
})
public class Dispensary {

    @JsonProperty("name")
    private String name;
    @JsonProperty("menu_integration_key")
    private String menuIntegrationKey;
    @JsonProperty("menu_integration_type")
    private Object menuIntegrationType;
    @JsonProperty("menuSummary")
    private MenuSummary menuSummary;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("menu_integration_key")
    public String getMenuIntegrationKey() {
        return menuIntegrationKey;
    }

    @JsonProperty("menu_integration_key")
    public void setMenuIntegrationKey(String menuIntegrationKey) {
        this.menuIntegrationKey = menuIntegrationKey;
    }

    @JsonProperty("menu_integration_type")
    public Object getMenuIntegrationType() {
        return menuIntegrationType;
    }

    @JsonProperty("menu_integration_type")
    public void setMenuIntegrationType(Object menuIntegrationType) {
        this.menuIntegrationType = menuIntegrationType;
    }

    @JsonProperty("menuSummary")
    public MenuSummary getMenuSummary() {
        return menuSummary;
    }

    @JsonProperty("menuSummary")
    public void setMenuSummary(MenuSummary menuSummary) {
        this.menuSummary = menuSummary;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
