package com.fourtwenty.core.thirdparty.leafly.model;

public enum LeaflyCategory {
    Other("Other", 0),
    Accessory("Accessory", 1),
    Seeds("Seeds", 2),
    Clone("Clone", 3),
    Flower("Flower", 4),
    Edible("Edible", 5),
    PreRoll("PreRoll", 6),
    Concentrate("Concentrate", 7),
    Cartridge("Cartridge", 8),
    Topical("Topical", 9);

    LeaflyCategory(String title, int id) {
        this.title = title;
        this.id = id;
    }

    public String title;
    public int id;

    public static LeaflyCategory findLeaflyCategory(String name) {
        LeaflyCategory[] categories = LeaflyCategory.values();
        for (LeaflyCategory category : categories) {
            if (category.title.equalsIgnoreCase(name)) {
                return category;
            }
        }
        return null;
    }
}
