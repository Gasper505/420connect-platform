
package com.fourtwenty.core.thirdparty.leafly.model;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "type",
    "name",
    "strain",
    "brand",
    "compounds",
    "variants",
    "description",
    "available_for_pickup",
    "image_url"
})
public class LeaflyMenuItem {

    @JsonProperty("id")
    private String id;
    @JsonProperty("type")
    private String type;
    @JsonProperty("name")
    private String name;
    @JsonProperty("strain")
    private String strain;
    @JsonProperty("brand")
    private String brand;
    @JsonProperty("compounds")
    private List<Compound> compounds = new ArrayList<>();
    @JsonProperty("variants")
    private List<Variant> variants = new ArrayList<>();
    @JsonProperty("description")
    private String description;
    @JsonProperty("available_for_pickup")
    private Boolean availableForPickup = Boolean.FALSE;
    @JsonProperty("image_url")
    private String imageUrl;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        if (StringUtils.isBlank(type)) {
            type = "Flower";
        }
        this.type = type;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("strain")
    public String getStrain() {
        return strain;
    }

    @JsonProperty("strain")
    public void setStrain(String strain) {
        this.strain = strain;
    }

    @JsonProperty("brand")
    public String getBrand() {
        return brand;
    }

    @JsonProperty("brand")
    public void setBrand(String brand) {
        if (StringUtils.isBlank(brand)) {
            brand = "";
        }
        this.brand = brand;
    }

    @JsonProperty("compounds")
    public List<Compound> getCompounds() {
        return compounds;
    }

    @JsonProperty("compounds")
    public void setCompounds(List<Compound> compounds) {
        if (compounds == null) {
            compounds = new ArrayList<>();
        }
        this.compounds = compounds;
    }

    @JsonProperty("variants")
    public List<Variant> getVariants() {
        return variants;
    }

    @JsonProperty("variants")
    public void setVariants(List<Variant> variants) {
        if (variants == null) {
            variants = new ArrayList<>();
        }
        this.variants = variants;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("available_for_pickup")
    public Boolean getAvailableForPickup() {
        return availableForPickup;
    }

    @JsonProperty("available_for_pickup")
    public void setAvailableForPickup(Boolean availableForPickup) {
        this.availableForPickup = availableForPickup;
    }

    @JsonProperty("image_url")
    public String getImageUrl() {
        return imageUrl;
    }

    @JsonProperty("image_url")
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
