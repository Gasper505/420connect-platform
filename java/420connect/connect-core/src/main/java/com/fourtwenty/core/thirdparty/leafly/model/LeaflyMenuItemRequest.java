package com.fourtwenty.core.thirdparty.leafly.model;

import java.util.List;

public class LeaflyMenuItemRequest {
    private List<LeaflyMenuItem> items;

    public List<LeaflyMenuItem> getItems() {
        return items;
    }

    public LeaflyMenuItemRequest setItems(List<LeaflyMenuItem> items) {
        this.items = items;
        return this;
    }
}
