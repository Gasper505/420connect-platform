package com.fourtwenty.core.thirdparty.leafly.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.event.leafly.LeaflySyncResponse;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LeaflyResult extends LeaflySyncResponse {

    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public enum LeaflyErrorCode {
        SUCCESS(200, "Success"),
        BAD_REQUEST(400, "Bad Request"),
        UNAUTHORIZED(401, "Unauthorized"),
        NO_CONTENT(204, "No Content");

        public int code;
        public String message;

        LeaflyErrorCode(int code, String message) {
            this.code = code;
            this.message = message;
        }

        public static LeaflyResult.LeaflyErrorCode toErrorCode(int code) {
            switch (code) {
                case 200:
                    return SUCCESS;
                case 400:
                    return BAD_REQUEST;
                case 401:
                    return UNAUTHORIZED;
                default:
                    return BAD_REQUEST;
            }
        }

    }
}
