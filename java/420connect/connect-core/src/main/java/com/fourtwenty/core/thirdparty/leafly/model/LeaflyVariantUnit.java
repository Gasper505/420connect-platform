package com.fourtwenty.core.thirdparty.leafly.model;

public enum LeaflyVariantUnit {
    OZ("oz", 0),
    G("g", 1),
    EACH("each", 2);

    LeaflyVariantUnit(String title, int id) {
        this.title = title;
        this.id = id;
    }

    public String title;
    public int id;

    public static LeaflyVariantUnit findLeaflyVariantUnit(String name) {
        LeaflyVariantUnit[] categories = LeaflyVariantUnit.values();
        for (LeaflyVariantUnit category : categories) {
            if (category.title.equalsIgnoreCase(name)) {
                return category;
            }
        }
        return null;
    }
}
