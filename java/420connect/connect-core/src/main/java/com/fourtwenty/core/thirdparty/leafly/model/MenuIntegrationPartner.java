package com.fourtwenty.core.thirdparty.leafly.model;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "name",
    "settings_url",
    "signup_url"
})
public class MenuIntegrationPartner {

    @JsonProperty("name")
    private String name;
    @JsonProperty("settings_url")
    private Object settingsUrl;
    @JsonProperty("signup_url")
    private Object signupUrl;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("settings_url")
    public Object getSettingsUrl() {
        return settingsUrl;
    }

    @JsonProperty("settings_url")
    public void setSettingsUrl(Object settingsUrl) {
        this.settingsUrl = settingsUrl;
    }

    @JsonProperty("signup_url")
    public Object getSignupUrl() {
        return signupUrl;
    }

    @JsonProperty("signup_url")
    public void setSignupUrl(Object signupUrl) {
        this.signupUrl = signupUrl;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
