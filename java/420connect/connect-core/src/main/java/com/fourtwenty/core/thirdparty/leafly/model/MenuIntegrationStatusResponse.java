
package com.fourtwenty.core.thirdparty.leafly.model;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "menuIntegrationPartner",
    "dispensary"
})
public class MenuIntegrationStatusResponse {

    @JsonProperty("menuIntegrationPartner")
    private MenuIntegrationPartner menuIntegrationPartner;
    @JsonProperty("dispensary")
    private Dispensary dispensary;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("menuIntegrationPartner")
    public MenuIntegrationPartner getMenuIntegrationPartner() {
        return menuIntegrationPartner;
    }

    @JsonProperty("menuIntegrationPartner")
    public void setMenuIntegrationPartner(MenuIntegrationPartner menuIntegrationPartner) {
        this.menuIntegrationPartner = menuIntegrationPartner;
    }

    @JsonProperty("dispensary")
    public Dispensary getDispensary() {
        return dispensary;
    }

    @JsonProperty("dispensary")
    public void setDispensary(Dispensary dispensary) {
        this.dispensary = dispensary;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
