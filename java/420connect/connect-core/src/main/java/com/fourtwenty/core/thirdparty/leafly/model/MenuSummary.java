package com.fourtwenty.core.thirdparty.leafly.model;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "totalItemCount",
    "integratedItemCount",
    "manualItemCount",
    "lastModifiedAt",
    "lastSyncedAt",
    "lastPayload"
})
public class MenuSummary {

    @JsonProperty("totalItemCount")
    private Integer totalItemCount;
    @JsonProperty("integratedItemCount")
    private Integer integratedItemCount;
    @JsonProperty("manualItemCount")
    private Integer manualItemCount;
    @JsonProperty("lastModifiedAt")
    private String lastModifiedAt;
    @JsonProperty("lastSyncedAt")
    private Object lastSyncedAt;
    @JsonProperty("lastPayload")
    private Object lastPayload;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("totalItemCount")
    public Integer getTotalItemCount() {
        return totalItemCount;
    }

    @JsonProperty("totalItemCount")
    public void setTotalItemCount(Integer totalItemCount) {
        this.totalItemCount = totalItemCount;
    }

    @JsonProperty("integratedItemCount")
    public Integer getIntegratedItemCount() {
        return integratedItemCount;
    }

    @JsonProperty("integratedItemCount")
    public void setIntegratedItemCount(Integer integratedItemCount) {
        this.integratedItemCount = integratedItemCount;
    }

    @JsonProperty("manualItemCount")
    public Integer getManualItemCount() {
        return manualItemCount;
    }

    @JsonProperty("manualItemCount")
    public void setManualItemCount(Integer manualItemCount) {
        this.manualItemCount = manualItemCount;
    }

    @JsonProperty("lastModifiedAt")
    public String getLastModifiedAt() {
        return lastModifiedAt;
    }

    @JsonProperty("lastModifiedAt")
    public void setLastModifiedAt(String lastModifiedAt) {
        this.lastModifiedAt = lastModifiedAt;
    }

    @JsonProperty("lastSyncedAt")
    public Object getLastSyncedAt() {
        return lastSyncedAt;
    }

    @JsonProperty("lastSyncedAt")
    public void setLastSyncedAt(Object lastSyncedAt) {
        this.lastSyncedAt = lastSyncedAt;
    }

    @JsonProperty("lastPayload")
    public Object getLastPayload() {
        return lastPayload;
    }

    @JsonProperty("lastPayload")
    public void setLastPayload(Object lastPayload) {
        this.lastPayload = lastPayload;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
