package com.fourtwenty.core.thirdparty.leafly.model;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "price",
    "price_includes_tax",
    "tax_rate",
    "amount",
    "unit",
    "inventory_level",
    "batch_id",
    "parent_batch_id",
    "sku"
})
public class Variant {

    @JsonProperty("price")
    private Double price;
    @JsonProperty("price_includes_tax")
    private Boolean priceIncludesTax;
    @JsonProperty("tax_rate")
    private Double taxRate;
    @JsonProperty("amount")
    private Double amount;
    @JsonProperty("unit")
    private String unit;
    @JsonProperty("inventory_level")
    private Double inventoryLevel;
    @JsonProperty("batch_id")
    private String batchId;
    @JsonProperty("parent_batch_id")
    private String parentBatchId;
    @JsonProperty("sku")
    private String sku;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("price")
    public Double getPrice() {
        return price;
    }

    @JsonProperty("price")
    public void setPrice(Double price) {
        this.price = price;
    }

    @JsonProperty("price_includes_tax")
    public Object getPriceIncludesTax() {
        return priceIncludesTax;
    }

    @JsonProperty("price_includes_tax")
    public void setPriceIncludesTax(Boolean priceIncludesTax) {
        this.priceIncludesTax = priceIncludesTax;
    }

    @JsonProperty("tax_rate")
    public Double getTaxRate() {
        return taxRate;
    }

    @JsonProperty("tax_rate")
    public void setTaxRate(Double taxRate) {
        this.taxRate = taxRate;
    }

    @JsonProperty("amount")
    public Double getAmount() {
        return amount;
    }

    @JsonProperty("amount")
    public void setAmount(Double amount) {
        this.amount = amount;
    }

    @JsonProperty("unit")
    public String getUnit() {
        return unit;
    }

    @JsonProperty("unit")
    public void setUnit(String unit) {
        this.unit = unit.toLowerCase();
    }

    @JsonProperty("inventory_level")
    public Double getInventoryLevel() {
        return inventoryLevel;
    }

    @JsonProperty("inventory_level")
    public void setInventoryLevel(Double inventoryLevel) {
        this.inventoryLevel = inventoryLevel;
    }

    @JsonProperty("batch_id")
    public String getBatchId() {
        return batchId;
    }

    @JsonProperty("batch_id")
    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    @JsonProperty("parent_batch_id")
    public String getParentBatchId() {
        return parentBatchId;
    }

    @JsonProperty("parent_batch_id")
    public void setParentBatchId(String parentBatchId) {
        this.parentBatchId = parentBatchId;
    }

    @JsonProperty("sku")
    public String getSku() {
        return sku;
    }

    @JsonProperty("sku")
    public void setSku(String sku) {
        this.sku = sku;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
