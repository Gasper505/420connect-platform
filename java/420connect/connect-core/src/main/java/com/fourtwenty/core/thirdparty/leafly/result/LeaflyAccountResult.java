package com.fourtwenty.core.thirdparty.leafly.result;

import com.fourtwenty.core.domain.models.thirdparty.leafly.LeaflyAccount;

public class LeaflyAccountResult extends LeaflyAccount {

    private transient String shopName;

    public String getShopName() {
        return shopName;
    }

    public LeaflyAccountResult setShopName(String shopName) {
        this.shopName = shopName;
        return this;
    }
}
