package com.fourtwenty.core.thirdparty.leafly.services;

import com.fourtwenty.core.domain.models.common.IntegrationSetting;
import com.fourtwenty.core.thirdparty.leafly.model.LeaflyMenuItem;
import com.fourtwenty.core.thirdparty.leafly.model.LeaflyMenuItemRequest;
import com.fourtwenty.core.thirdparty.leafly.model.LeaflyResult;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

public interface LeaflyAPIService {

    Boolean deleteMenuItems(IntegrationSetting.Environment environment, String clientKey, String apiKey, String... itemIds);

    LeaflyResult upsertMenuItem(IntegrationSetting.Environment environment, String clientKey, String apiKey, LeaflyMenuItemRequest leaflyMenuItem) throws InterruptedException, ExecutionException, IOException;

    LeaflyResult synchronizeMenuById(IntegrationSetting.Environment environment, String clientKey, String apiKey, LeaflyMenuItem leaflyMenuItem) throws InterruptedException, ExecutionException, IOException;

    Boolean getMenuIntegrationStatus(IntegrationSetting.Environment environment, String clientKey, String apiKey);


}
