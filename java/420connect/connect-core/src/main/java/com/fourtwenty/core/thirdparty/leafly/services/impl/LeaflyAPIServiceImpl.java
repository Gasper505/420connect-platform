package com.fourtwenty.core.thirdparty.leafly.services.impl;

import com.fourtwenty.core.domain.models.common.IntegrationSetting;
import com.fourtwenty.core.thirdparty.leafly.model.LeaflyMenuItem;
import com.fourtwenty.core.thirdparty.leafly.model.LeaflyMenuItemRequest;
import com.fourtwenty.core.thirdparty.leafly.model.LeaflyResult;
import com.fourtwenty.core.thirdparty.leafly.model.MenuIntegrationStatusResponse;
import com.fourtwenty.core.thirdparty.leafly.services.LeaflyAPIService;
import com.fourtwenty.core.util.JsonSerializer;
import com.mdo.pusher.RestErrorException;
import com.mdo.pusher.SimpleRestUtil;
import org.apache.commons.lang3.ArrayUtils;
import org.json.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;

public class LeaflyAPIServiceImpl implements LeaflyAPIService {

    private static final Logger LOGGER = LoggerFactory.getLogger(LeaflyAPIServiceImpl.class);

    private static final String LEAFLY_SANDBOX_URL = "https://api-sandbox.leafly.io/v1/menu_integration/";
    private static final String LEAFLY_PRODUCTION_URL = "https://api.leafly.io/v1/menu_integration/";
    private static final String X_LEAFLY_PARTNER_API_KEY = "X-LEAFLY-PARTNER-API-KEY";
    private static final String LEAFLY_MENU = "/menu";
    private static final String LEAFLY_ITEMS = "/items";
    private static final String LEAFLY_STATUS = "/status";


    /**
     * Deletes elements matching the ids provided in the body.
     *
     * @param clientKey : String
     * @param apiKey    : String
     * @param itemIds   : {@link String[]}
     * @return object {@link Boolean}
     */
    @Override
    public Boolean deleteMenuItems(IntegrationSetting.Environment environment, String clientKey, String apiKey, String... itemIds) {
        String url = createURL(environment, clientKey, LEAFLY_MENU, LEAFLY_ITEMS);
        try {
            JSONArray ids = new JSONArray();
            Arrays.stream(itemIds).forEach(ids::put);
            JSONObject requestBody = new JSONObject();
            requestBody.put("ids", ids);
            Response response = SimpleRestUtil.deleteWithSsl(url, requestBody.toJSONString(), getHeaders(apiKey), new HashMap<>(), MediaType.APPLICATION_JSON, LeaflyResult.LeaflyErrorCode.SUCCESS.code);
            LOGGER.info(String.format("End calling api %s with status: %s", url, response.getStatus() + "  " + response.getStatusInfo()));
            if (response.getStatus() == 200) {
                return Boolean.TRUE;
            } else {
                throw new RestErrorException(response.getStatus() + "  " + response.getStatusInfo());
            }
        } catch (Exception e) {
            LOGGER.error(String.format("Error in calling url: %s with error message: %s", url, e.getMessage()), e);
        }
        return Boolean.FALSE;
    }

    /**
     * Permits bulk updates of a menu, using id to determine if items are inserts or updates.
     * Updates existing items that have a matching id. Inserts items if no matching id exists.
     * Does not delete items. All items in the posted payload must have a unique value for id.
     *
     * @param clientKey      : String
     * @param apiKey         : String
     * @param leaflyMenuItem : {@link LeaflyMenuItemRequest}
     * @return object {@link LeaflyResult}
     * @throws InterruptedException
     * @throws ExecutionException
     * @throws IOException
     */
    @Override
    public LeaflyResult upsertMenuItem(IntegrationSetting.Environment environment, String clientKey, String apiKey, LeaflyMenuItemRequest leaflyMenuItem) throws InterruptedException, ExecutionException, IOException {
        String url = createURL(environment, clientKey, LEAFLY_MENU, LEAFLY_ITEMS);
        return SimpleRestUtil.put(url, leaflyMenuItem, LeaflyResult.class, String.class, getHeaders(apiKey));
    }

    /**
     * Permits bulk update of a menu, using id to determine if changes are inserts or updates.
     * Updates existing items that have a matching id. Inserts items if no matching id exists.
     * Items not present in the post body are deleted from the Leafly menu. All items in the posted payload must have a unique value for id.
     *
     * @param clientKey      : String
     * @param apiKey         : String
     * @param leaflyMenuItem : {@link LeaflyMenuItem}
     * @return object {@link LeaflyResult}
     * @throws InterruptedException
     * @throws ExecutionException
     * @throws IOException
     */
    @Override
    public LeaflyResult synchronizeMenuById(IntegrationSetting.Environment environment, String clientKey, String apiKey, LeaflyMenuItem leaflyMenuItem) throws InterruptedException, ExecutionException, IOException {
        String url = createURL(environment, clientKey, LEAFLY_MENU, LEAFLY_ITEMS);
        return SimpleRestUtil.post(url, leaflyMenuItem, LeaflyResult.class, String.class, getHeaders(apiKey));
    }

    /**
     * Provides the status of a dispensary's menu integration, including summary statistics for the dispensary menu.
     *
     * @param environment : {@link IntegrationSetting.Environment}
     * @param clientKey   : {@link String}
     * @param apiKey      : {@link String}
     * @return object {@link Boolean}
     * @throws InterruptedException
     * @throws ExecutionException
     * @throws IOException
     */
    @Override
    public Boolean getMenuIntegrationStatus(IntegrationSetting.Environment environment, String clientKey, String apiKey) {
        String url = createURL(environment, clientKey, LEAFLY_STATUS);
        try {
            MenuIntegrationStatusResponse menuIntegrationStatusResponse = SimpleRestUtil.get(url, MenuIntegrationStatusResponse.class, getHeaders(apiKey));
            String response = JsonSerializer.toJson(menuIntegrationStatusResponse);
            LOGGER.info(String.format("End calling api %s with response : %s", url, response));
            return Boolean.TRUE;
        } catch (Exception e) {
            LOGGER.error(String.format("Error in calling url: %s with error message: %s", url, e.getMessage()), e);
        }
        return Boolean.FALSE;
    }

    /**
     * Private method to get headers
     *
     * @param apiKey : String
     * @return : object {@link MultivaluedMap}
     */
    private MultivaluedMap<String, Object> getHeaders(String apiKey) {
        MultivaluedMap<String, Object> headers = new MultivaluedHashMap<>();
        headers.putSingle("Content-Type", MediaType.APPLICATION_JSON);
        headers.putSingle(X_LEAFLY_PARTNER_API_KEY, apiKey);
        return headers;
    }

    /**
     * create url with client key and other url tokens according to environment
     *
     * @param environment : {@link IntegrationSetting.Environment}
     * @param clientKey   : String
     * @param urlTokens   : {@link String[]}
     * @return object {@link String}
     */
    private static String createURL(IntegrationSetting.Environment environment, String clientKey, String... urlTokens) {
        StringBuilder pathSB = environment.equals(IntegrationSetting.Environment.Production) ? new StringBuilder(LEAFLY_PRODUCTION_URL) : new StringBuilder(LEAFLY_SANDBOX_URL);
        pathSB.append(clientKey);
        if (ArrayUtils.isNotEmpty(urlTokens)) {
            Arrays.stream(urlTokens).forEach(pathSB::append);
        }
        return pathSB.toString();
    }

}




