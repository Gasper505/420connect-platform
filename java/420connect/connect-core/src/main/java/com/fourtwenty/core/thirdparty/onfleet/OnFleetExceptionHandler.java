package com.fourtwenty.core.thirdparty.onfleet;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.thirdparty.onfleet.services.OnFleetExceptionMessage;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OnFleetExceptionHandler {

    private String code;
    private OnFleetExceptionMessage message;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public OnFleetExceptionMessage getMessage() {
        return message;
    }

    public void setMessage(OnFleetExceptionMessage message) {
        this.message = message;
    }
}
