package com.fourtwenty.core.thirdparty.onfleet.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.thirdparty.onfleet.models.response.CompletionDetails;
import com.fourtwenty.core.thirdparty.onfleet.models.response.DestinationResult;
import com.fourtwenty.core.thirdparty.onfleet.models.response.RecipientsResult;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OnFleetTask {

    private String id;
    private long timeCreated;
    private long timeLastModified;
    private String organization;
    private String shortId;
    private String trackingURL;
    private String worker;
    private String merchant;
    private String executor;
    private String creator;
    private int state;
    private String notes;
    private CompletionDetails completionDetails;
    private List<RecipientsResult> recipients;
    private DestinationResult destination;
    private long completeAfter;
    private boolean pickupTask;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getTimeCreated() {
        return timeCreated;
    }

    public void setTimeCreated(long timeCreated) {
        this.timeCreated = timeCreated;
    }

    public long getTimeLastModified() {
        return timeLastModified;
    }

    public void setTimeLastModified(long timeLastModified) {
        this.timeLastModified = timeLastModified;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getShortId() {
        return shortId;
    }

    public void setShortId(String shortId) {
        this.shortId = shortId;
    }

    public String getTrackingURL() {
        return trackingURL;
    }

    public void setTrackingURL(String trackingURL) {
        this.trackingURL = trackingURL;
    }

    public String getWorker() {
        return worker;
    }

    public void setWorker(String worker) {
        this.worker = worker;
    }

    public String getMerchant() {
        return merchant;
    }

    public void setMerchant(String merchant) {
        this.merchant = merchant;
    }

    public String getExecutor() {
        return executor;
    }

    public void setExecutor(String executor) {
        this.executor = executor;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public CompletionDetails getCompletionDetails() {
        return completionDetails;
    }

    public void setCompletionDetails(CompletionDetails completionDetails) {
        this.completionDetails = completionDetails;
    }

    public List<RecipientsResult> getRecipients() {
        return recipients;
    }

    public void setRecipients(List<RecipientsResult> recipients) {
        this.recipients = recipients;
    }

    public DestinationResult getDestination() {
        return destination;
    }

    public void setDestination(DestinationResult destination) {
        this.destination = destination;
    }

    public long getCompleteAfter() {
        return completeAfter;
    }

    public void setCompleteAfter(long completeAfter) {
        this.completeAfter = completeAfter;
    }

    public boolean isPickupTask() {
        return pickupTask;
    }

    public void setPickupTask(boolean pickupTask) {
        this.pickupTask = pickupTask;
    }
}
