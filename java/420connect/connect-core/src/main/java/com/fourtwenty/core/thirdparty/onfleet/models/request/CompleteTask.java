package com.fourtwenty.core.thirdparty.onfleet.models.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.thirdparty.onfleet.models.response.CompletionDetails;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CompleteTask {

    private CompletionDetails completionDetails = new CompletionDetails();

    public CompletionDetails getCompletionDetails() {
        return completionDetails;
    }

    public void setCompletionDetails(CompletionDetails completionDetails) {
        this.completionDetails = completionDetails;
    }
}
