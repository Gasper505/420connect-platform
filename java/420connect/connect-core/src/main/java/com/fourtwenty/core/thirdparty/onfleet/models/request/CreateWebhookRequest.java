package com.fourtwenty.core.thirdparty.onfleet.models.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.thirdparty.onfleet.models.serializers.WebhookTypeSerializer;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CreateWebhookRequest {

    @JsonSerialize(using = WebhookTypeSerializer.class)
    private OnFleetWebhookType trigger;

    private Integer threshold = 0;

    public OnFleetWebhookType getTrigger() {
        return trigger;
    }

    public void setTrigger(OnFleetWebhookType trigger) {
        this.trigger = trigger;
    }

    public Integer getThreshold() {
        return threshold;
    }

    public void setThreshold(Integer threshold) {
        this.threshold = threshold;
    }

}
