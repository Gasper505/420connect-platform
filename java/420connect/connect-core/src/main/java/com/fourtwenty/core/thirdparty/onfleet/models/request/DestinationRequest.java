package com.fourtwenty.core.thirdparty.onfleet.models.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.apache.commons.lang3.StringUtils;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DestinationRequest {
    private OnFleetAddress address;
    private String notes = StringUtils.EMPTY;

    public OnFleetAddress getAddress() {
        return address;
    }

    public void setAddress(OnFleetAddress address) {
        this.address = address;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
}
