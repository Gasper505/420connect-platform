package com.fourtwenty.core.thirdparty.onfleet.models.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.validator.constraints.NotEmpty;


@JsonIgnoreProperties(ignoreUnknown = true)
public class OnFleetAddRequest {
    @NotEmpty
    private String shopId;
    private String apiKey;
    private String organizationId;
    private String organizationName;
    private String hubName;
    private String hubId;
    private boolean enableOnFleet = Boolean.FALSE;
    private boolean resetOnFleetInfo = Boolean.FALSE;

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public String getHubName() {
        return hubName;
    }

    public void setHubName(String hubName) {
        this.hubName = hubName;
    }

    public String getHubId() {
        return hubId;
    }

    public void setHubId(String hubId) {
        this.hubId = hubId;
    }

    public boolean isEnableOnFleet() {
        return enableOnFleet;
    }

    public void setEnableOnFleet(boolean enableOnFleet) {
        this.enableOnFleet = enableOnFleet;
    }

    public boolean isResetOnFleetInfo() {
        return resetOnFleetInfo;
    }

    public void setResetOnFleetInfo(boolean resetOnFleetInfo) {
        this.resetOnFleetInfo = resetOnFleetInfo;
    }
}
