package com.fourtwenty.core.thirdparty.onfleet.models.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.apache.commons.lang3.StringUtils;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OnFleetAddress {
    private String unparsed = StringUtils.EMPTY;

    public String getUnparsed() {
        return unparsed;
    }

    public void setUnparsed(String unparsed) {
        this.unparsed = unparsed;
    }
}
