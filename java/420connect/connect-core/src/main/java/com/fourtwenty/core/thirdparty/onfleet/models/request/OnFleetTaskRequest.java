package com.fourtwenty.core.thirdparty.onfleet.models.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OnFleetTaskRequest {

    private OnFleetContainer container;
    private List<RecipientsRequest> recipients;
    private DestinationRequest destination;
    private String notes;
    private Long completeAfter;
    private Long completeBefore;

    public Long getCompleteAfter() {
        return completeAfter;
    }

    public void setCompleteAfter(Long completeAfter) {
        this.completeAfter = completeAfter;
    }

    public Long getCompleteBefore() {
        return completeBefore;
    }

    public void setCompleteBefore(Long completeBefore) {
        this.completeBefore = completeBefore;
    }

    public OnFleetContainer getContainer() {
        return container;
    }

    public void setContainer(OnFleetContainer container) {
        this.container = container;
    }

    public List<RecipientsRequest> getRecipients() {
        return recipients;
    }

    public void setRecipients(List<RecipientsRequest> recipients) {
        this.recipients = recipients;
    }

    public DestinationRequest getDestination() {
        return destination;
    }

    public void setDestination(DestinationRequest destination) {
        this.destination = destination;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
}
