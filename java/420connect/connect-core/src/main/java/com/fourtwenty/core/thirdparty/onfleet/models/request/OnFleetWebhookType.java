package com.fourtwenty.core.thirdparty.onfleet.models.request;

public enum OnFleetWebhookType {
    TASK_STARTED(0),
    WORKER_ETA_LESS_THAN_OR_EQUAL_TO(1),
    TASK_ARRIVAL(2),
    TASK_COMPLETED(3),
    TASK_FAILED(4),
    WORKER_STATUS_CHANGED(5),
    NEW_TASK_CREATED(6),
    TASK_UPDATED(7),
    TASK_DELETED(8),
    TASK_ASSIGNED(9),
    TASK_UNASSIGNED(10),
    TASK_DELAY_TIME_IS_GREATER_THAN_OR_EQUAL_TO(12);

    OnFleetWebhookType(int triggerId) {
        this.triggerId = triggerId;
    }

    int triggerId;

    public int getTriggerId() {
        return triggerId;
    }

    public static OnFleetWebhookType toOnFleetWebhookType(int triggerId) {
        for (OnFleetWebhookType onFleetWebhookType : OnFleetWebhookType.values()) {
            if (onFleetWebhookType.getTriggerId() == triggerId) {
                return onFleetWebhookType;
            }
        }
        return null;
    }

}
