package com.fourtwenty.core.thirdparty.onfleet.models.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.LinkedHashSet;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UpdateWorkerRequest {

    private String name;
    private LinkedHashSet<String> teams;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LinkedHashSet<String> getTeams() {
        return teams;
    }

    public void setTeams(LinkedHashSet<String> teams) {
        this.teams = teams;
    }
}
