package com.fourtwenty.core.thirdparty.onfleet.models.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.thirdparty.onfleet.models.OnFleetTask;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WebHookRequestData {

    private OnFleetTask task;

    public OnFleetTask getTask() {
        return task;
    }

    public void setTask(OnFleetTask task) {
        this.task = task;
    }
}
