package com.fourtwenty.core.thirdparty.onfleet.models.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.apache.commons.lang3.StringUtils;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WebhookAddRequest {

    //    @JsonSerialize(using = WebhookTypeSerializer.class)
    private Integer trigger;

    //NOTE : For trigger WORKER_ETA_LESS_THAN_OR_EQUAL_TO, the threshold in(time) seconds; for trigger WORKER_ARRIVING_AT_OR_CLOSER_THAN, the threshold in(distance) meters
    private Integer threshold;

    private String url = StringUtils.EMPTY;

    public Integer getTrigger() {
        return trigger;
    }

    public void setTrigger(Integer trigger) {
        this.trigger = trigger;
    }

    public Integer getThreshold() {
        return threshold;
    }

    public void setThreshold(Integer threshold) {
        this.threshold = threshold;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
