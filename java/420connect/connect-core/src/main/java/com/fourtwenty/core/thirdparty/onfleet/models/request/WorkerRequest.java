package com.fourtwenty.core.thirdparty.onfleet.models.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WorkerRequest {

    private String name;
    private String phone;
    private List<String> teams;
    private WorkerVehicleRequest vehicle;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public List<String> getTeams() {
        return teams;
    }

    public void setTeams(List<String> teams) {
        this.teams = teams;
    }

    public WorkerVehicleRequest getVehicle() {
        return vehicle;
    }

    public void setVehicle(WorkerVehicleRequest vehicle) {
        this.vehicle = vehicle;
    }
}
