package com.fourtwenty.core.thirdparty.onfleet.models.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.apache.commons.lang3.StringUtils;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WorkerVehicleRequest {

    public enum VehicleType {
        CAR,
        MOTORCYCLE,
        BICYCLE,
        TRUCK
    }

    private VehicleType type = VehicleType.CAR;
    private String description = StringUtils.EMPTY;
    private String licensePlate = StringUtils.EMPTY;
    private String color = StringUtils.EMPTY;

    public VehicleType getType() {
        return type;
    }

    public void setType(VehicleType type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
