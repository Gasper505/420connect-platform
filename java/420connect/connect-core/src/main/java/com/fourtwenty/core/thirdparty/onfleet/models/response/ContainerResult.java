package com.fourtwenty.core.thirdparty.onfleet.models.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.thirdparty.onfleet.models.request.OnFleetContainer;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ContainerResult {

    private OnFleetContainer.TaskType type;
    private String worker;

    public OnFleetContainer.TaskType getType() {
        return type;
    }

    public void setType(OnFleetContainer.TaskType type) {
        this.type = type;
    }

    public String getWorker() {
        return worker;
    }

    public void setWorker(String worker) {
        this.worker = worker;
    }
}
