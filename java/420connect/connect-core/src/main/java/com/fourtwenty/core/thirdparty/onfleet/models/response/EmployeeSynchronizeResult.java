package com.fourtwenty.core.thirdparty.onfleet.models.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EmployeeSynchronizeResult {

    private boolean synced;
    private String onFleetWorkerId;

    public boolean isSynced() {
        return synced;
    }

    public void setSynced(boolean synced) {
        this.synced = synced;
    }

    public String getOnFleetWorkerId() {
        return onFleetWorkerId;
    }

    public void setOnFleetWorkerId(String onFleetWorkerId) {
        this.onFleetWorkerId = onFleetWorkerId;
    }
}
