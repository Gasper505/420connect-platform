package com.fourtwenty.core.thirdparty.onfleet.models.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class HubAssignmentResult {

    private OrganizationResult organizationResult;
    private List<HubResult> hubResults;

    public OrganizationResult getOrganizationResult() {
        return organizationResult;
    }

    public void setOrganizationResult(OrganizationResult organizationResult) {
        this.organizationResult = organizationResult;
    }

    public List<HubResult> getHubResults() {
        return hubResults;
    }

    public void setHubResults(List<HubResult> hubResults) {
        this.hubResults = hubResults;
    }
}
