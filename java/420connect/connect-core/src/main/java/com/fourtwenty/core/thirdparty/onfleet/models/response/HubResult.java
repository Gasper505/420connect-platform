package com.fourtwenty.core.thirdparty.onfleet.models.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.thirdparty.onfleet.models.request.OnFleetAddress;

@JsonIgnoreProperties(ignoreUnknown = true)
public class HubResult {
    private String id;
    private String name;
    private double[] location;
    private OnFleetAddress address;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double[] getLocation() {
        return location;
    }

    public void setLocation(double[] location) {
        this.location = location;
    }

    public OnFleetAddress getAddress() {
        return address;
    }

    public void setAddress(OnFleetAddress address) {
        this.address = address;
    }
}
