package com.fourtwenty.core.thirdparty.onfleet.models.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.thirdparty.onfleet.models.OnFleetTask;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OnFleetTaskList {

    private String lastId;
    private List<OnFleetTask> tasks;

    public String getLastId() {
        return lastId;
    }

    public void setLastId(String lastId) {
        this.lastId = lastId;
    }

    public List<OnFleetTask> getTasks() {
        return tasks;
    }

    public void setTasks(List<OnFleetTask> tasks) {
        this.tasks = tasks;
    }
}
