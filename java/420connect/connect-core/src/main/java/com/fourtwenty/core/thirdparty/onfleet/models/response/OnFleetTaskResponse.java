package com.fourtwenty.core.thirdparty.onfleet.models.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OnFleetTaskResponse {

    private String id;
    private ContainerResult container;
    private String shortId;
    private String trackingURL;
    private String worker;
    private String merchant;
    private String executor;
    private String creator;
    private List<RecipientsResult> recipients;
    private DestinationResult destination;
    private int state;
    private String error;
    private String cause;
    private boolean notCreated;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ContainerResult getContainer() {
        return container;
    }

    public void setContainer(ContainerResult container) {
        this.container = container;
    }

    public String getShortId() {
        return shortId;
    }

    public void setShortId(String shortId) {
        this.shortId = shortId;
    }

    public String getTrackingURL() {
        return trackingURL;
    }

    public void setTrackingURL(String trackingURL) {
        this.trackingURL = trackingURL;
    }

    public String getWorker() {
        return worker;
    }

    public void setWorker(String worker) {
        this.worker = worker;
    }

    public String getMerchant() {
        return merchant;
    }

    public void setMerchant(String merchant) {
        this.merchant = merchant;
    }

    public String getExecutor() {
        return executor;
    }

    public void setExecutor(String executor) {
        this.executor = executor;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public List<RecipientsResult> getRecipients() {
        return recipients;
    }

    public void setRecipients(List<RecipientsResult> recipients) {
        this.recipients = recipients;
    }

    public DestinationResult getDestination() {
        return destination;
    }

    public void setDestination(DestinationResult destination) {
        this.destination = destination;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getCause() {
        return cause;
    }

    public void setCause(String cause) {
        this.cause = cause;
    }

    public boolean isNotCreated() {
        return notCreated;
    }

    public void setNotCreated(boolean notCreated) {
        this.notCreated = notCreated;
    }
}
