package com.fourtwenty.core.thirdparty.onfleet.models.response;

import com.fourtwenty.core.domain.models.thirdparty.OnFleetTeams;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OnFleetTeamResult extends OnFleetTeams {

    private String shopName;

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }
}
