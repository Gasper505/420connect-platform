package com.fourtwenty.core.thirdparty.onfleet.models.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.transaction.Transaction;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OnFleetTransactionResult {

    private String id;
    private String onFleetTaskId;
    private String transNo;
    private Transaction.QueueType queueType;
    private String memberName;
    private String employeeName;
    private String state;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOnFleetTaskId() {
        return onFleetTaskId;
    }

    public void setOnFleetTaskId(String onFleetTaskId) {
        this.onFleetTaskId = onFleetTaskId;
    }

    public String getTransNo() {
        return transNo;
    }

    public void setTransNo(String transNo) {
        this.transNo = transNo;
    }

    public Transaction.QueueType getQueueType() {
        return queueType;
    }

    public void setQueueType(Transaction.QueueType queueType) {
        this.queueType = queueType;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
