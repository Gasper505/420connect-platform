package com.fourtwenty.core.thirdparty.onfleet.models.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OrganizationResult {

    private String id;
    private long timeCreated;
    private long timeLastModified;
    private String name;
    private String email;
    private String image;
    private String timezone;
    private String country;
    private String[] delegatees;
    private boolean didAutoAssign;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getTimeCreated() {
        return timeCreated;
    }

    public void setTimeCreated(long timeCreated) {
        this.timeCreated = timeCreated;
    }

    public long getTimeLastModified() {
        return timeLastModified;
    }

    public void setTimeLastModified(long timeLastModified) {
        this.timeLastModified = timeLastModified;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String[] getDelegatees() {
        return delegatees;
    }

    public void setDelegatees(String[] delegatees) {
        this.delegatees = delegatees;
    }

    public boolean isDidAutoAssign() {
        return didAutoAssign;
    }

    public void setDidAutoAssign(boolean didAutoAssign) {
        this.didAutoAssign = didAutoAssign;
    }
}
