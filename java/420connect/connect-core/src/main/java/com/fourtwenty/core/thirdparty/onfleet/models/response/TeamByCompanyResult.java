package com.fourtwenty.core.thirdparty.onfleet.models.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.thirdparty.OnFleetTeamInfo;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TeamByCompanyResult {

    private String shopId;
    private String shopName;

    private List<OnFleetTeamInfo> teamList;

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public List<OnFleetTeamInfo> getTeamList() {
        return teamList;
    }

    public void setTeamList(List<OnFleetTeamInfo> teamList) {
        this.teamList = teamList;
    }
}
