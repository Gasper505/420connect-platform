package com.fourtwenty.core.thirdparty.onfleet.models.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.company.Shop;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UpdateOnFleetShopInfoResult {

    private boolean onFleetInfoUpdated;

    private Shop shop;

    public boolean isOnFleetInfoUpdated() {
        return onFleetInfoUpdated;
    }

    public void setOnFleetInfoUpdated(boolean onFleetInfoUpdated) {
        this.onFleetInfoUpdated = onFleetInfoUpdated;
    }

    public Shop getShop() {
        return shop;
    }

    public void setShop(Shop shop) {
        this.shop = shop;
    }
}
