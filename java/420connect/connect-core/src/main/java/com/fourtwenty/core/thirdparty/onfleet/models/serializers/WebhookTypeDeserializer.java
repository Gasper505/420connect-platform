package com.fourtwenty.core.thirdparty.onfleet.models.serializers;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fourtwenty.core.thirdparty.onfleet.models.request.OnFleetWebhookType;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;

public class WebhookTypeDeserializer extends JsonDeserializer<OnFleetWebhookType> {

    @Override
    public OnFleetWebhookType deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        String value = p.getValueAsString();
        if (StringUtils.isNotBlank(value)) {
            return OnFleetWebhookType.toOnFleetWebhookType(Integer.parseInt(value));
        }
        return null;
    }
}
