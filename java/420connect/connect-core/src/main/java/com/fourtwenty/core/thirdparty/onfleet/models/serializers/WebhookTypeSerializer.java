package com.fourtwenty.core.thirdparty.onfleet.models.serializers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fourtwenty.core.thirdparty.onfleet.models.request.OnFleetWebhookType;

import java.io.IOException;

public class WebhookTypeSerializer extends JsonSerializer<OnFleetWebhookType> {

    @Override
    public void serialize(OnFleetWebhookType value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        gen.writeNumber(value.getTriggerId());
    }
}
