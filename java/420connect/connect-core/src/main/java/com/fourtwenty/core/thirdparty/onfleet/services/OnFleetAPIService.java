package com.fourtwenty.core.thirdparty.onfleet.services;

import com.fourtwenty.core.domain.models.thirdparty.OnFleetErrorLog;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.thirdparty.onfleet.models.OnFleetTask;
import com.fourtwenty.core.thirdparty.onfleet.models.request.*;
import com.fourtwenty.core.thirdparty.onfleet.models.response.*;

import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.List;

public interface OnFleetAPIService {
    AuthenticateAPIKeyResult authenticateApiKey(String apiKey) throws IOException;

    OrganizationResult getOrganizationInfoByApiKey(String apiKey);

    List<HubResult> getHubInfoByApiKey(String apiKey);

    OnFleetTaskResponse createOnFleetTask(String companyId, String shopId, String apiKey, OnFleetTaskRequest onFleetTaskRequest, Transaction transaction);

    OnFleetTask getOnFleetTaskById(String apiKey, String taskId);

    OnFleetTaskResponse updateOnFleetTask(String apiKey, OnFleetTask onFleetTask);

    Response completeTask(String apiKey, String taskId, CompleteTask completeTask);

    Response deleteTask(String apiKey, String taskId);

    OnFleetTaskList getAllTask(String apiKey, long from, String state);

    List<WorkerResult> getAllWorker(String apiKey, String states);

    WorkerResult getWorkerById(String apiKey, String workerId);

    WorkerResult createWorker(String apiKey, WorkerRequest workerRequest);

    String getAllTeams(String apiKey, String companyId, String shopId);

    WorkerResult updateWorker(String apiKey, String workerId, UpdateWorkerRequest workerRequest);

    WebHookResponse createWebhook(WebhookAddRequest request, String apiKey);

    OnFleetErrorLog addOnFleetError(String onFleetMessage, String message, String code, String cause, OnFleetErrorLog.ErrorType errorType, String errorSubType, String apiKey, String companyId, String shopId, String transactionId);
}
