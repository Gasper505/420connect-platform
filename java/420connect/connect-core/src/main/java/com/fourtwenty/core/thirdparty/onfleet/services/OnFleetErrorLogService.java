package com.fourtwenty.core.thirdparty.onfleet.services;

import com.fourtwenty.core.domain.models.thirdparty.OnFleetErrorLog;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;

public interface OnFleetErrorLogService {
    OnFleetErrorLog addOnFleetError(String onFleetMessage, String message, String code, String cause, OnFleetErrorLog.ErrorType errorType, String errorSubType, String apiKey);

    SearchResult<OnFleetErrorLog> getOnFleetErrorLogByCompany(int start, int limit);

    SearchResult<OnFleetErrorLog> getOnFleetErrorLogByShop(int start, int limit, String shopId);
}
