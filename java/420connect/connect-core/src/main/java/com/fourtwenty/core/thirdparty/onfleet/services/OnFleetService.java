package com.fourtwenty.core.thirdparty.onfleet.services;

import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.company.EmployeeOnFleetInfo;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.customer.StatusRequest;
import com.fourtwenty.core.domain.models.thirdparty.OnFleetTeams;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.thirdparty.onfleet.models.OnFleetTask;
import com.fourtwenty.core.thirdparty.onfleet.models.request.*;
import com.fourtwenty.core.thirdparty.onfleet.models.response.*;

import javax.ws.rs.core.Response;
import java.util.List;

public interface OnFleetService {
    UpdateOnFleetShopInfoResult updateShopOnFleetInformation(String shopId, OnFleetAddRequest addRequest);

    AuthenticateAPIKeyResult authenticateApiKey(String apiKey);

    OrganizationResult getOrganizationInfoByApiKey(String apiKey);

    HubAssignmentResult getHubInfoByApiKey(String apiKey);

    void createOnFleetTask(Shop shop, Transaction transaction, String workerId, String requestedTeamId, Employee employee, EmployeeOnFleetInfo employeeOnFleetInfo, Member member, boolean reAssign);

    OnFleetTask getOnFleetTaskById(String apiKey, String taskId);

    OnFleetTaskResponse updateOnFleetTaskForWorkerByTaskId(String transactionId, String workerId);

    OnFleetTaskResponse updateOnFleetTask(String apiKey, String taskId, OnFleetTask onFleetTask);

    Response completeTask(String taskId, String apiKey, CompleteTask completeTask);

    Response deleteTask(String apiKey, String taskId);

    SearchResult<OnFleetTransactionResult> getAllTask(long from, String shopId);

    List<WorkerResult> getAllWorker(String apiKey, String states);

    WorkerResult getWorkerById(String workerId, String apiKey);

    EmployeeSynchronizeResult syncEmployeeAtOnFleet(String employeeId, SynchronizeEmployeeRequest request);

    EmployeeSynchronizeResult createWorkerForEmployee(String employeeId, CreateEmployeeRequest employeeRequest);

    List<TeamResult> getAllTeams(String apiKey);

    void reAssignTask(Shop shop, Transaction transaction, String workerId, String onFleetTeamId, Employee employee, EmployeeOnFleetInfo employeeOnFleetInfo, Member member);

    WorkerResult getWorkerById(String employeeId);

    List<TeamByCompanyResult> getAllTeamsByCompany();

    Employee updateEmployeeOnFleetDetails(String employeeId, CreateEmployeeRequest request);

    WebHookResponse createOnfleetWebhook(CreateWebhookRequest request, String apiEndpoint);

    void handleOnFleetWebHookRequest(String webhookAddRequest, String jsonObject);

    void handleOnFleetWebHookTaskArrival(String request, WebHookRequest data);

    void handleOnFleetWebHookTaskCompleted(String request, WebHookRequest data);

    void handleOnFleetWebHookTaskFailed(String request, WebHookRequest data);

    EmployeeOnFleetInfo getEmployeeOnfFleetInfoByShop(Employee employee, String shopId);

    Employee removeWorkerDetailOfEmployee(String employeeId, CreateEmployeeRequest shopId);

    void synchronizeTeamForShop(SynchronizeTeamRequest request);

    OnFleetTeams getOnFleetTeamListByShop(String shopId);

    List<OnFleetTeamResult> getOnFleetTeamListByCompany(String companyId);

    void completeTransactionById(String transactionId, StatusRequest request);

    void updateOnfleetTaskSchedule(Shop shop, Transaction dbTransaction, String employeeWorkerId, Employee employee, EmployeeOnFleetInfo employeeOnFleetInfo, Member member);

    void completeTaskAtOnFleet(Shop shop, Transaction trans, boolean taskStatus);

    void deleteOrMarkFailedTaskAtOnfleet(Shop shop, Transaction transaction);
}
