package com.fourtwenty.core.thirdparty.onfleet.services.impl;

import com.fourtwenty.core.domain.models.thirdparty.OnFleetErrorLog;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.thirdparty.OnFleetErrorLogRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.thirdparty.onfleet.OnFleetExceptionHandler;
import com.fourtwenty.core.thirdparty.onfleet.models.OnFleetTask;
import com.fourtwenty.core.thirdparty.onfleet.models.request.*;
import com.fourtwenty.core.thirdparty.onfleet.models.response.*;
import com.fourtwenty.core.thirdparty.onfleet.services.OnFleetAPIService;
import com.mdo.pusher.RestErrorException;
import com.mdo.pusher.SimpleRestUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.inject.Inject;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

@SuppressWarnings("unchecked")
public class OnFleetAPIServiceImpl implements OnFleetAPIService {

    @Inject
    private OnFleetErrorLogRepository onFleetErrorLogRepository;

    private static final Log LOG = LogFactory.getLog(OnFleetAPIServiceImpl.class);

    private static final String END_POINT = "https://onfleet.com/api/v2";
    private static final String TEST_API_URL = "/auth/test";
    private static final String ORGANIZATION_URL = "/organization";
    private static final String HUB_URL = "/hubs";
    private static final String TASK = "/tasks";
    private static final String ALL_TASK = "/tasks/all";
    private static final String WORKERS = "/workers";
    private static final String TEAMS = "/teams";
    private static final String WEBHOOKS = "/webhooks";

    private static final String API_KEY_INVALID = "API key is invalid";
    private static final String API_KEY = "API key";

    private static final String CREATE_TASK = "Create Task";
    private static final String GET_TEAM = "Get Team";
    private static final String ERROR_CREATE_TASK = "Error while creating task at OnFleet";
    private static final String ONFLEET_TASK = "ONFleet Task";
    private static final String ERROR_GET_TASK = "Error while getting task from OnFleet";
    private static final String UPDATE_TASK = "Update Task";
    private static final String ERROR_UPDATE_TASK = "Error while updating task at OnFleet";
    private static final String ERROR_COMPLETE_TASK = "Error in complete task at OnFleet";
    private static final String ERROR_GET_WORKER = "Error while getting worker";
    private static final String ONFLEET_WORKER = "Worker";
    private static final String ONFLEET_TEAM = "Team";
    private static final String ERROR_CREATE_WORKER = "Error while creating worker at OnFleet";
    private static final String ERROR_UPDATE_WORKER = "Error while update worker at OnFleet";
    private static final String ERROR_GET_TEAM = "Error while getting team from OnFleet";
    private static final String ERROR_CREATE_WEBHOOK = "Error while creating webhooks on OnFleet";

    private String createURL(String apiKey, String apiPath) {
        if (StringUtils.isBlank(apiKey)) {
            throw new BlazeInvalidArgException(API_KEY, API_KEY_INVALID);
        }

        StringBuilder apiURl = new StringBuilder(END_POINT);
        apiURl.append(apiPath);

        return apiURl.toString();
    }

    private MultivaluedMap<String, Object> getHeader(String apiKey) {
        MultivaluedMap<String, Object> headers = new MultivaluedHashMap<>();
        String basicAuth = new String(Base64.getEncoder().encode(apiKey.getBytes()));
        headers.putSingle("Authorization", "Basic " + basicAuth);

        return headers;
    }

    @Override
    public AuthenticateAPIKeyResult authenticateApiKey(String apiKey) throws IOException {
        String url = this.createURL(apiKey, TEST_API_URL);

        MultivaluedMap<String, Object> headers = this.getHeader(apiKey);

        String message = API_KEY_INVALID;
        AuthenticateAPIKeyResult result = new AuthenticateAPIKeyResult();
        try {
            result = SimpleRestUtil.get(url, AuthenticateAPIKeyResult.class, headers);
            result.setMessage("This API key is valid");
            result.setValid(true);
        } catch (Exception e) {
            result.setValid(false);
            result.setMessage(message);
            LOG.info("Invalid API key" + e.getMessage());
        }


        return result;
    }

    @Override
    public OrganizationResult getOrganizationInfoByApiKey(String apiKey) {
        String url = this.createURL(apiKey, ORGANIZATION_URL);

        MultivaluedMap<String, Object> headers = this.getHeader(apiKey);

        return SimpleRestUtil.get(url, OrganizationResult.class, headers);

    }

    @Override
    public List<HubResult> getHubInfoByApiKey(String apiKey) {
        String url = this.createURL(apiKey, HUB_URL);

        MultivaluedMap<String, Object> headers = this.getHeader(apiKey);

        return (List<HubResult>) SimpleRestUtil.get(url, ArrayList.class, headers);

    }

    @Override
    public OnFleetTaskResponse createOnFleetTask(String companyId, String shopId, String apiKey, OnFleetTaskRequest onFleetTaskRequest, Transaction transaction) {
        String urlPath = this.createURL(apiKey, TASK);

        MultivaluedMap<String, Object> headers = this.getHeader(apiKey);
        OnFleetTaskResponse response = new OnFleetTaskResponse();
        response.setError(ERROR_CREATE_TASK);
        response.setCause("");

        try {
            return SimpleRestUtil.post(urlPath, onFleetTaskRequest, OnFleetTaskResponse.class, OnFleetExceptionHandler.class, headers);
        } catch (RestErrorException e) {
            LOG.error(e.getMessage(), e);
            response.setNotCreated(true);
            if (e.getErrorResponse() instanceof OnFleetExceptionHandler) {
                OnFleetExceptionHandler handler = (OnFleetExceptionHandler) e.getErrorResponse();

                response.setError(handler.getMessage().getMessage());
                response.setCause(handler.getMessage().getCause());

                this.addOnFleetError(handler.getMessage().getMessage(), ERROR_CREATE_TASK, handler.getCode(),
                        handler.getMessage().getCause(), OnFleetErrorLog.ErrorType.Task, CREATE_TASK, apiKey, companyId, shopId, transaction.getId());
                return response;
            }
            this.addOnFleetError(ERROR_CREATE_TASK, ERROR_CREATE_TASK, "400",
                    e.getMessage(), OnFleetErrorLog.ErrorType.Task, CREATE_TASK, apiKey, companyId, shopId, transaction.getId());
            return response;
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            response.setNotCreated(true);
            this.addOnFleetError(ERROR_CREATE_TASK, ERROR_CREATE_TASK, "400",
                    e.getMessage(), OnFleetErrorLog.ErrorType.Task, CREATE_TASK, apiKey, companyId, shopId, transaction.getId());
            return response;
        }
    }

    @Override
    public OnFleetTask getOnFleetTaskById(String apiKey, String taskId) {
        String url = this.createURL(apiKey, TASK + "/" + taskId);

        MultivaluedMap<String, Object> headers = this.getHeader(apiKey);

        try {
            return SimpleRestUtil.get(url, OnFleetTask.class, OnFleetExceptionHandler.class, headers);
        } catch (RestErrorException e) {
            LOG.error(e.getMessage(), e);
            if (e.getErrorResponse() instanceof OnFleetExceptionHandler) {
                OnFleetExceptionHandler handler = (OnFleetExceptionHandler) e.getErrorResponse();

                throw new BlazeInvalidArgException(ONFLEET_TASK, handler.getMessage().getMessage());
            }
            throw new BlazeInvalidArgException(ONFLEET_TASK, ERROR_GET_TASK);
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            throw new BlazeInvalidArgException(ONFLEET_TASK, ERROR_GET_TASK);
        }
    }

    @Override
    public OnFleetTaskResponse updateOnFleetTask(String apiKey, OnFleetTask onFleetTask) {

        if (onFleetTask == null || StringUtils.isBlank(onFleetTask.getId())) {
            throw new BlazeInvalidArgException(API_KEY, API_KEY_INVALID);
        }

        String url = this.createURL(apiKey, TASK + "/" + onFleetTask.getId());

        MultivaluedMap<String, Object> headers = this.getHeader(apiKey);


        try {
            return SimpleRestUtil.put(url, onFleetTask, OnFleetTaskResponse.class, OnFleetExceptionHandler.class, headers);
        } catch (RestErrorException e) {
            LOG.error(e.getMessage(), e);
            if (e.getErrorResponse() instanceof OnFleetExceptionHandler) {
                OnFleetExceptionHandler handler = (OnFleetExceptionHandler) e.getErrorResponse();

                throw new BlazeInvalidArgException(UPDATE_TASK, handler.getMessage().getMessage());
            }
            throw new BlazeInvalidArgException(UPDATE_TASK, ERROR_UPDATE_TASK);
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            throw new BlazeInvalidArgException(UPDATE_TASK, ERROR_UPDATE_TASK);
        }
    }

    @Override
    public Response completeTask(String apiKey, String taskId, CompleteTask completeTask) {
        String url = this.createURL(apiKey, TASK + "/" + taskId + "/complete");

        MultivaluedMap<String, Object> headers = this.getHeader(apiKey);

        try {
            return SimpleRestUtil.post(url, completeTask, Response.class, OnFleetExceptionHandler.class, headers);
        } catch (RestErrorException e) {
            LOG.error(e.getMessage(), e);
            if (e.getErrorResponse() instanceof OnFleetExceptionHandler) {
                OnFleetExceptionHandler handler = (OnFleetExceptionHandler) e.getErrorResponse();

                throw new BlazeInvalidArgException(ONFLEET_TASK, handler.getMessage().getMessage());
            }
            throw new BlazeInvalidArgException(ONFLEET_TASK, ERROR_COMPLETE_TASK);
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            throw new BlazeInvalidArgException(ONFLEET_TASK, ERROR_COMPLETE_TASK);
        }
    }

    @Override
    public Response deleteTask(String apiKey, String taskId) {
        String url = this.createURL(apiKey, TASK + "/" + taskId);

        MultivaluedMap<String, Object> headers = this.getHeader(apiKey);

        SimpleRestUtil.delete(url, String.class, headers);

        return Response.ok().build();
    }

    @Override
    public OnFleetTaskList getAllTask(String apiKey, long from, String state) {
        String url = this.createURL(apiKey, ALL_TASK);

        MultivaluedMap<String, Object> queryParams = new MultivaluedHashMap<>();
        queryParams.putSingle("from", from);
        queryParams.putSingle("state", state);

        try {
            return SimpleRestUtil.get(url, OnFleetTaskList.class, queryParams, getHeader(apiKey));
        } catch (RestErrorException e) {
            LOG.error(e.getMessage(), e);
            if (e.getErrorResponse() instanceof OnFleetExceptionHandler) {
                OnFleetExceptionHandler handler = (OnFleetExceptionHandler) e.getErrorResponse();

                throw new BlazeInvalidArgException(ONFLEET_TASK, handler.getMessage().getMessage());
            }
            throw new BlazeInvalidArgException(ONFLEET_TASK, ERROR_GET_TASK);
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            throw new BlazeInvalidArgException(ONFLEET_TASK, ERROR_GET_TASK);
        }
    }

    @Override
    public List<WorkerResult> getAllWorker(String apiKey, String states) {
        String url = this.createURL(apiKey, WORKERS);

        MultivaluedMap<String, Object> queryParams = new MultivaluedHashMap<>();
        queryParams.putSingle("states", states);

        try {
            return SimpleRestUtil.get(url, ArrayList.class, queryParams, getHeader(apiKey));
        } catch (RestErrorException e) {
            LOG.error(e.getMessage(), e);
            if (e.getErrorResponse() instanceof OnFleetExceptionHandler) {
                OnFleetExceptionHandler handler = (OnFleetExceptionHandler) e.getErrorResponse();

                throw new BlazeInvalidArgException(ONFLEET_WORKER, handler.getMessage().getMessage());
            }
            throw new BlazeInvalidArgException(ONFLEET_WORKER, ERROR_GET_WORKER);
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            throw new BlazeInvalidArgException(ONFLEET_WORKER, ERROR_GET_WORKER);
        }

    }

    @Override
    public WorkerResult getWorkerById(String apiKey, String workerId) {
        String url = this.createURL(apiKey, WORKERS + "/" + workerId);

        MultivaluedMap<String, Object> headers = this.getHeader(apiKey);

        try {
            return SimpleRestUtil.get(url, WorkerResult.class, OnFleetExceptionHandler.class, headers);
        } catch (RestErrorException e) {
            LOG.error(e.getMessage(), e);
            if (e.getErrorResponse() instanceof OnFleetExceptionHandler) {
                OnFleetExceptionHandler handler = (OnFleetExceptionHandler) e.getErrorResponse();

                throw new BlazeInvalidArgException(ONFLEET_WORKER, handler.getMessage().getMessage());
            }
            throw new BlazeInvalidArgException(ONFLEET_WORKER, ERROR_GET_WORKER);
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            throw new BlazeInvalidArgException(ONFLEET_WORKER, ERROR_GET_WORKER);
        }
    }

    @Override
    public WorkerResult createWorker(String apiKey, WorkerRequest workerRequest) {
        String url = this.createURL(apiKey, WORKERS);

        MultivaluedMap<String, Object> headers = this.getHeader(apiKey);

        try {
            return SimpleRestUtil.post(url, workerRequest, WorkerResult.class, OnFleetExceptionHandler.class, headers);
        } catch (RestErrorException e) {
            LOG.error(e.getMessage(), e);
            if (e.getErrorResponse() instanceof OnFleetExceptionHandler) {
                OnFleetExceptionHandler handler = (OnFleetExceptionHandler) e.getErrorResponse();

                throw new BlazeInvalidArgException(ONFLEET_WORKER, handler.getMessage().getMessage());
            }
            throw new BlazeInvalidArgException(ONFLEET_WORKER, ERROR_CREATE_WORKER);
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            throw new BlazeInvalidArgException(ONFLEET_WORKER, ERROR_CREATE_WORKER);
        }

    }

    @Override
    public String getAllTeams(String apiKey, String companyId, String shopId) {
        String url = this.createURL(apiKey, TEAMS);
        MultivaluedMap<String, Object> headers = this.getHeader(apiKey);

        try {
            return SimpleRestUtil.get(url, String.class, OnFleetExceptionHandler.class, headers);
        } catch (RestErrorException e) {
            LOG.error(e.getMessage(), e);
            if (e.getErrorResponse() instanceof OnFleetExceptionHandler) {
                OnFleetExceptionHandler handler = (OnFleetExceptionHandler) e.getErrorResponse();

                this.addOnFleetError(handler.getMessage().getMessage(), ERROR_GET_TEAM, handler.getCode(),
                        handler.getMessage().getCause(), OnFleetErrorLog.ErrorType.Team, ONFLEET_TEAM, apiKey, companyId, shopId, "");
                return "";
            }

            this.addOnFleetError(ERROR_GET_TEAM, ERROR_GET_TEAM, "400",
                    e.getMessage(), OnFleetErrorLog.ErrorType.Task, GET_TEAM, apiKey, companyId, shopId, "");
            return "";
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            this.addOnFleetError(ERROR_GET_TEAM, ERROR_GET_TEAM, "400",
                    e.getMessage(), OnFleetErrorLog.ErrorType.Task, GET_TEAM, apiKey, companyId, shopId, "");
            return "";
        }
    }

    @Override
    public WorkerResult updateWorker(String apiKey, String workerId, UpdateWorkerRequest workerRequest) {
        String url = this.createURL(apiKey, WORKERS + "/" + workerId);

        MultivaluedMap<String, Object> headers = this.getHeader(apiKey);

        try {
            return SimpleRestUtil.put(url, workerRequest, WorkerResult.class, OnFleetExceptionHandler.class, headers);
        } catch (RestErrorException e) {
            LOG.error(e.getMessage(), e);
            if (e.getErrorResponse() instanceof OnFleetExceptionHandler) {
                OnFleetExceptionHandler handler = (OnFleetExceptionHandler) e.getErrorResponse();

                throw new BlazeInvalidArgException(ONFLEET_WORKER, handler.getMessage().getMessage());
            }
            throw new BlazeInvalidArgException(ONFLEET_WORKER, ERROR_UPDATE_WORKER);
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            throw new BlazeInvalidArgException(ONFLEET_WORKER, ERROR_UPDATE_WORKER);
        }

    }

    @Override
    public WebHookResponse createWebhook(WebhookAddRequest request, String apiKey) {
        String url = this.createURL(apiKey, WEBHOOKS);
        MultivaluedMap<String, Object> headers = this.getHeader(apiKey);
        try {
            return SimpleRestUtil.post(url, request, WebHookResponse.class, OnFleetExceptionHandler.class, headers);
        } catch (RestErrorException e) {
            LOG.error(e.getMessage(), e);
            if (e.getErrorResponse() instanceof OnFleetExceptionHandler) {
                OnFleetExceptionHandler handler = (OnFleetExceptionHandler) e.getErrorResponse();
                throw new BlazeInvalidArgException(ONFLEET_TASK, handler.getMessage().getMessage());
            }
            throw new BlazeInvalidArgException(ONFLEET_TASK, ERROR_CREATE_WEBHOOK);
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            throw new BlazeInvalidArgException(ONFLEET_TASK, ERROR_CREATE_WEBHOOK);
        }
    }

    @Override
    public OnFleetErrorLog addOnFleetError(String onFleetMessage, String message, String code, String cause, OnFleetErrorLog.ErrorType errorType,
                                           String errorSubType, String apiKey, String companyId, String shopId, String transactionId) {

        OnFleetErrorLog onFleetErrorLog = new OnFleetErrorLog();
        onFleetErrorLog.prepare(companyId);
        onFleetErrorLog.setShopId(shopId);
        onFleetErrorLog.setOnFleetMessage(onFleetMessage);
        onFleetErrorLog.setMessage(message);
        onFleetErrorLog.setCode(code);
        onFleetErrorLog.setCause(cause);
        onFleetErrorLog.setErrorType(errorType);
        onFleetErrorLog.setErrorSubType(errorSubType);
        onFleetErrorLog.setApiKey(apiKey);
        onFleetErrorLog.setTransactionId(transactionId);

        return onFleetErrorLogRepository.save(onFleetErrorLog);
    }
}
