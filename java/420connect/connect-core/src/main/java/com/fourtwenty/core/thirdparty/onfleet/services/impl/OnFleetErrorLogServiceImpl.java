package com.fourtwenty.core.thirdparty.onfleet.services.impl;

import com.fourtwenty.core.domain.models.thirdparty.OnFleetErrorLog;
import com.fourtwenty.core.domain.repositories.thirdparty.OnFleetErrorLogRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.thirdparty.onfleet.services.OnFleetErrorLogService;
import com.google.inject.Provider;

import javax.inject.Inject;

public class OnFleetErrorLogServiceImpl extends AbstractAuthServiceImpl implements OnFleetErrorLogService {

    private OnFleetErrorLogRepository onFleetErrorLogRepository;

    @Inject
    public OnFleetErrorLogServiceImpl(Provider<ConnectAuthToken> token, OnFleetErrorLogRepository onFleetErrorLogRepository) {
        super(token);
        this.onFleetErrorLogRepository = onFleetErrorLogRepository;
    }

    /**
     * Save error log of OnFleet
     *
     * @param onFleetMessage : on fleet error message
     * @param message        : custom message provided by our application
     * @param code           : code
     * @param cause          : cause return from OnFleet
     * @param errorType      : error type
     * @param errorSubType   : error sub type
     * @param apiKey         : api key
     */
    @Override
    public OnFleetErrorLog addOnFleetError(String onFleetMessage, String message, String code, String cause, OnFleetErrorLog.ErrorType errorType, String errorSubType, String apiKey) {

        OnFleetErrorLog onFleetErrorLog = new OnFleetErrorLog();
        onFleetErrorLog.prepare(token.getCompanyId());
        onFleetErrorLog.setShopId(token.getShopId());
        onFleetErrorLog.setOnFleetMessage(onFleetMessage);
        onFleetErrorLog.setMessage(message);
        onFleetErrorLog.setCode(code);
        onFleetErrorLog.setCause(cause);
        onFleetErrorLog.setErrorType(errorType);
        onFleetErrorLog.setErrorSubType(errorSubType);
        onFleetErrorLog.setApiKey(apiKey);

        return onFleetErrorLogRepository.save(onFleetErrorLog);
    }

    @Override
    public SearchResult<OnFleetErrorLog> getOnFleetErrorLogByCompany(int start, int limit) {
        if (limit <= 0 || limit >= 200) {
            limit = 200;
        }
        return onFleetErrorLogRepository.findItems(token.getCompanyId(), start, limit);
    }

    @Override
    public SearchResult<OnFleetErrorLog> getOnFleetErrorLogByShop(int start, int limit, String shopId) {
        if (shopId == null) {
            shopId = token.getShopId();
        }
        if (limit <= 0 || limit >= 200) {
            limit = 200;
        }
        return onFleetErrorLogRepository.findItems(token.getCompanyId(), shopId, "{created:-1}",start, limit);
    }
}
