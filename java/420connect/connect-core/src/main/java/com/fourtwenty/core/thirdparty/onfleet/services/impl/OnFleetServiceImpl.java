package com.fourtwenty.core.thirdparty.onfleet.services.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.company.EmployeeOnFleetInfo;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.customer.StatusRequest;
import com.fourtwenty.core.domain.models.thirdparty.OnFleetTeamInfo;
import com.fourtwenty.core.domain.models.thirdparty.OnFleetTeams;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.managed.AmazonServiceManager;
import com.fourtwenty.core.managed.OnFleetManager;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.services.mgmt.EmployeeService;
import com.fourtwenty.core.thirdparty.onfleet.models.OnFleetTask;
import com.fourtwenty.core.thirdparty.onfleet.models.request.*;
import com.fourtwenty.core.thirdparty.onfleet.models.response.*;
import com.fourtwenty.core.thirdparty.onfleet.services.OnFleetAPIService;
import com.fourtwenty.core.thirdparty.onfleet.services.OnFleetService;
import com.google.inject.Provider;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.assertj.core.util.Lists;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;

import javax.inject.Inject;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.*;

public class OnFleetServiceImpl extends AbstractAuthServiceImpl implements OnFleetService {
    private static final Log LOG = LogFactory.getLog(OnFleetServiceImpl.class);
    private static final String API_KEY = "API key";
    private static final String INVALID_API_KEY = "Invalid API key";
    private static final String TASK = "Task";
    private static final String INVALID_TASK_ID = "Invalid task id";
    private static final String EMPLOYEE = "Employee";
    private static final String EMPLOYEE_NOT_FOUND = "Employee not found";
    private static final String ONFLEET_NOT_ENABLED = "OnFleet is not enabled for the company";
    private static final String ON_FLEET = "OnFleet";
    private static final String EMPLOYEE_ALREADY_EXIST = "Employee already exist at OnFleet";
    private static final String EMPLOYEE_INVALID_PHONE_NUMBER = "Employee does not have phone number";
    private static final String SHOP = "Shop";
    private static final String SHOP_NOT_FOUND = "Shop does not exist";
    private static final String TRANSACTION_NOT_FOUND = "Transaction does not found";
    private static final String TRANSACTION = "Transaction";

    @Inject
    private OnFleetAPIService onFleetAPIService;
    @Inject
    private ShopRepository shopRepository;
    @Inject
    private MemberRepository memberRepository;
    @Inject
    private MemberGroupRepository memberGroupRepository;
    @Inject
    private EmployeeRepository employeeRepository;
    @Inject
    private EmployeeService employeeService;
    @Inject
    private ConnectConfiguration connectConfiguration;
    @Inject
    private AmazonServiceManager amazonServiceManager;
    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private OnFleetManager onFleetManager;
    @Inject
    private OnFleetTeamsRepository onFleetTeamsRepository;

    @Inject
    public OnFleetServiceImpl(Provider<ConnectAuthToken> token) {
        super(token);
    }

    /**
     * Assign OnFleet information to shop
     *
     * @param shopId     : shop id
     * @param addRequest : Required information
     */
    @Override
    public UpdateOnFleetShopInfoResult updateShopOnFleetInformation(String shopId, OnFleetAddRequest addRequest) {

        UpdateOnFleetShopInfoResult onFleetShopInfoResult = new UpdateOnFleetShopInfoResult();

        Shop shop = shopRepository.getById(shopId);
        if (shop == null) {
            throw new BlazeInvalidArgException("Shop", "Shop does not found");
        }

        if (StringUtils.isBlank(addRequest.getApiKey()) && addRequest.isResetOnFleetInfo()) {
            resetOnFleetInfo(shop);

            shopRepository.update(token.getCompanyId(), shopId, shop);
            onFleetShopInfoResult.setOnFleetInfoUpdated(true);
            onFleetShopInfoResult.setShop(shop);

            return onFleetShopInfoResult;
        }

        if (StringUtils.isBlank(addRequest.getHubId()) || StringUtils.isBlank(addRequest.getOrganizationId()) || StringUtils.isBlank(addRequest.getApiKey())) {
            throw new BlazeInvalidArgException("OnFleet information", "Please provide required information for OnFleet");
        }

        AuthenticateAPIKeyResult apiKeyResult = this.authenticateApiKey(addRequest.getApiKey());
        if (!apiKeyResult.isValid()) {
            throw new BlazeInvalidArgException(API_KEY, INVALID_API_KEY);
        }

        if (StringUtils.isNotBlank(shop.getOnFleetApiKey()) && !shop.getOnFleetApiKey().equalsIgnoreCase(addRequest.getApiKey()) && !addRequest.isResetOnFleetInfo()) {
            //If requested shop's API key is already present and requested is different from current and reset is false

            OrganizationResult currentOrganization = this.getOrganizationInfoByApiKey(shop.getOnFleetApiKey());
            OrganizationResult requestedOrganization = this.getOrganizationInfoByApiKey(addRequest.getApiKey());

            if (currentOrganization.getId().equalsIgnoreCase(requestedOrganization.getId())) {
                Shop updatedShop = this.updateOnFleetShopInfo(shop, addRequest);

                onFleetShopInfoResult.setShop(updatedShop);
                onFleetShopInfoResult.setOnFleetInfoUpdated(true);
            } else {
                onFleetShopInfoResult.setShop(shop);
                onFleetShopInfoResult.setOnFleetInfoUpdated(false);
            }

        } else if (StringUtils.isNotBlank(shop.getOnFleetApiKey()) && !shop.getOnFleetApiKey().equalsIgnoreCase(addRequest.getApiKey()) && addRequest.isResetOnFleetInfo()) {
            //If requested shop's API key is already present and requested is different from current and reset is true

            this.resetOnFleetInfo(shop);

            Shop updatedShop = this.updateOnFleetShopInfo(shop, addRequest);

            onFleetShopInfoResult.setShop(updatedShop);
            onFleetShopInfoResult.setOnFleetInfoUpdated(true);

        } else {
            Shop updatedShop = this.updateOnFleetShopInfo(shop, addRequest);

            onFleetShopInfoResult.setShop(updatedShop);
            onFleetShopInfoResult.setOnFleetInfoUpdated(true);
        }

        return onFleetShopInfoResult;
    }

    private void resetOnFleetInfo(Shop shop) {

        //Reset transaction
        SearchResult<Transaction> activeTransactions = transactionRepository.getActiveOnFleetTransactions(token.getCompanyId(), shop.getId());
        List<Transaction> transactionList = activeTransactions.getValues();

        for (Transaction transaction : transactionList) {
            if (StringUtils.isNotBlank(transaction.getOnFleetTaskId())) {
                /*transaction.setCreateOnfleetTask(false);
                transaction.setShortId(BLStringUtils.EMPTY);
                transaction.setOnFleetTaskId(BLStringUtils.EMPTY);
                transaction.setState(0);

                transactionRepository.update(token.getCompanyId(), transaction.getId(), transaction);*/

                transactionRepository.updateOnfleetInfo(token.getCompanyId(), transaction.getId(), 0, StringUtils.EMPTY, StringUtils.EMPTY, false);
            }
        }

        //Reset employee
        List<Employee> employeesByShop = employeeRepository.getEmployeesByShop(token.getCompanyId(), token.getShopId());

        for (Employee employee : employeesByShop) {
            EmployeeOnFleetInfo employeeOnfFleetInfo = this.getEmployeeOnfFleetInfoByShop(employee, shop.getId());
            if (employeeOnfFleetInfo != null) {
                List<EmployeeOnFleetInfo> infoList = employee.getEmployeeOnFleetInfoList();
                infoList.remove(employeeOnfFleetInfo);

                employeeRepository.update(employee.getCompanyId(), employee.getId(), employee);
            }
        }

        OnFleetTeams onFleetTeams = onFleetTeamsRepository.getByCompanyAndShop(token.getCompanyId(), shop.getId());
        onFleetTeams.setOnFleetTeamInfoList(new ArrayList<>());
        onFleetTeamsRepository.update(token.getCompanyId(), onFleetTeams.getId(), onFleetTeams);

        //Reset shop
        shop.setHubId(StringUtils.EMPTY);
        shop.setHubName(StringUtils.EMPTY);
        shop.setEnableOnFleet(Boolean.FALSE);
        shop.setOnFleetApiKey(StringUtils.EMPTY);
        shop.setOnFleetOrganizationId(StringUtils.EMPTY);
        shop.setOnFleetOrganizationName(StringUtils.EMPTY);

    }

    private Shop updateOnFleetShopInfo(Shop shop, OnFleetAddRequest addRequest) {
        shop.setOnFleetApiKey(addRequest.getApiKey());
        shop.setOnFleetOrganizationId(addRequest.getOrganizationId());
        shop.setOnFleetOrganizationName(addRequest.getOrganizationName());
        shop.setHubId(addRequest.getHubId());
        shop.setHubName(addRequest.getHubName());
        shop.setEnableOnFleet(addRequest.isEnableOnFleet());

        return shopRepository.update(shop.getId(), shop);
    }

    /**
     * Authenticate API key from onfleet
     *
     * @param apiKey : api key
     */
    @Override
    public AuthenticateAPIKeyResult authenticateApiKey(String apiKey) {
        AuthenticateAPIKeyResult result = null;
        try {
            result = onFleetAPIService.authenticateApiKey(apiKey);
        } catch (Exception e) {
            LOG.info("Exception while authenticating API key : " + e.getMessage());
        }
        return result;
    }

    /**
     * Get Organization(Company) info from API key
     *
     * @param apiKey : api key
     */
    @Override
    public OrganizationResult getOrganizationInfoByApiKey(String apiKey) {
        return onFleetAPIService.getOrganizationInfoByApiKey(apiKey);
    }

    /**
     * Get list of hubs(Shop) from api key
     *
     * @param apiKey : OnFleet API key
     */
    @Override
    public HubAssignmentResult getHubInfoByApiKey(String apiKey) {
        HubAssignmentResult result = new HubAssignmentResult();

        AuthenticateAPIKeyResult apiKeyResult = this.authenticateApiKey(apiKey);
        if (!apiKeyResult.isValid()) {
            throw new BlazeInvalidArgException(API_KEY, INVALID_API_KEY);
        }

        OrganizationResult organizationResult = this.getOrganizationInfoByApiKey(apiKey);

        List<HubResult> hubResults = onFleetAPIService.getHubInfoByApiKey(apiKey);

        result.setOrganizationResult(organizationResult);
        result.setHubResults(hubResults);
        return result;
    }

    /**
     * Create task on OnFleet
     *
     * @param shop
     * @param transaction         : transaction
     * @param workerId            : worker id
     * @param requestedTeamId
     * @param employee
     * @param employeeOnFleetInfo
     */
    @Override
    public void createOnFleetTask(Shop shop, Transaction transaction, String workerId, String requestedTeamId, Employee employee,
                                  EmployeeOnFleetInfo employeeOnFleetInfo, Member member, boolean reAssign) {

        onFleetManager.createOnFleetTask(shop, member, transaction, workerId, token.getCompanyId(), employee, requestedTeamId, employeeOnFleetInfo, reAssign);
    }

    /**
     * Get OnFleet task by id
     *
     * @param apiKey : api key
     * @param taskId : task id
     */
    @Override
    public OnFleetTask getOnFleetTaskById(String apiKey, String taskId) {
        if (StringUtils.isBlank(taskId)) {
            throw new BlazeInvalidArgException(TASK, INVALID_TASK_ID);
        }

        apiKey = getOnFleetAPiKeyOfShop(apiKey);
        return onFleetAPIService.getOnFleetTaskById(apiKey, taskId);
    }

    /**
     * Update on fleet task by task id
     *
     * @param taskId   : task id
     * @param workerId : worker id
     */
    @Override
    public OnFleetTaskResponse updateOnFleetTaskForWorkerByTaskId(String taskId, String workerId) {
        Shop shop = shopRepository.getById(token.getShopId());
        OnFleetTask onFleetTask = onFleetAPIService.getOnFleetTaskById(shop.getOnFleetApiKey(), taskId);
        onFleetTask.setWorker(workerId);
        return onFleetAPIService.updateOnFleetTask(shop.getOnFleetApiKey(), onFleetTask);
    }

    @Override
    public OnFleetTaskResponse updateOnFleetTask(String apiKey, String taskId, OnFleetTask onFleetTask) {
        Shop shop = shopRepository.getById(token.getShopId());

        if (!shop.isEnableOnFleet()) {
            throw new BlazeInvalidArgException(ON_FLEET, ONFLEET_NOT_ENABLED);
        }

        if (StringUtils.isNotBlank(apiKey) && !apiKey.equals(shop.getOnFleetApiKey())) {
            throw new BlazeInvalidArgException(API_KEY, INVALID_API_KEY);
        }

        OnFleetTask dbOnFleetTask = onFleetAPIService.getOnFleetTaskById(shop.getOnFleetApiKey(), taskId);
        if (Objects.isNull(dbOnFleetTask)) {
            throw new BlazeInvalidArgException(TASK, "No task found for the given task id");
        }

        if (Objects.isNull(onFleetTask)) {
            throw new BlazeInvalidArgException(TASK, "Please provide a valid OnFleet task for update");
        }

        dbOnFleetTask.setDestination(onFleetTask.getDestination());
        dbOnFleetTask.setRecipients(onFleetTask.getRecipients());
        dbOnFleetTask.setWorker(onFleetTask.getWorker());
        dbOnFleetTask.setCreator(onFleetTask.getCreator());
        dbOnFleetTask.setOrganization(onFleetTask.getOrganization());
        dbOnFleetTask.setShortId(onFleetTask.getShortId());
        dbOnFleetTask.setMerchant(onFleetTask.getMerchant());
        dbOnFleetTask.setExecutor(onFleetTask.getExecutor());
        dbOnFleetTask.setState(onFleetTask.getState());
        dbOnFleetTask.setNotes(onFleetTask.getNotes());
        dbOnFleetTask.setCompletionDetails(onFleetTask.getCompletionDetails());
        dbOnFleetTask.setCompleteAfter(onFleetTask.getCompleteAfter());

        return onFleetAPIService.updateOnFleetTask(shop.getOnFleetApiKey(), onFleetTask);
    }

    /**
     * Mark task as complete at OnFleet
     *
     * @param taskId            : task id
     * @param apiKey            : api key
     * @param completeTask : completion details
     */
    @Override
    public Response completeTask(String taskId, String apiKey, CompleteTask completeTask) {
        if (StringUtils.isBlank(taskId)) {
            throw new BlazeInvalidArgException(TASK, INVALID_TASK_ID);
        }

        apiKey = getOnFleetAPiKeyOfShop(apiKey);

        return onFleetAPIService.completeTask(apiKey, taskId, completeTask);
    }

    /**
     * Delete task at OnFLeet
     *
     * @param taskId : task id
     * @param apiKey : api key
     */
    @Override
    public Response deleteTask(String apiKey, String taskId) {

        apiKey = getOnFleetAPiKeyOfShop(apiKey);

        return onFleetAPIService.deleteTask(apiKey, taskId);
    }

    /**
     * Get all task fromm OnFleet
     *
     * @param from   : time from task needs to fetch
     * @param shopId : shop id for which all tasks needs to fetch
     */
    @Override
    public SearchResult<OnFleetTransactionResult> getAllTask(long from, String shopId) {
        SearchResult<OnFleetTransactionResult> searchResult = new SearchResult<>();

        if (StringUtils.isBlank(shopId)) {
            shopId = token.getShopId();
        }

        Shop shop = shopRepository.get(token.getCompanyId(), shopId);

        String apiKey = getOnFleetAPiKeyOfShop(shop.getOnFleetApiKey());

        if (StringUtils.isBlank(apiKey)) {
            throw new BlazeInvalidArgException(API_KEY, INVALID_API_KEY);
        }

        if (from == 0) {
            from = DateTime.now().getMillis();
        }

        String state = "0,1,2";
        OnFleetTaskList allTask = onFleetAPIService.getAllTask(apiKey, from, state);

        List<OnFleetTransactionResult> resultList = new ArrayList<>();
        if (allTask.getTasks() != null && allTask.getTasks().size() != 0) {

            List<OnFleetTask> onFleetTasks = allTask.getTasks();

            Iterable<Transaction> onFleetTransactions = transactionRepository.getOnFleetTransactions(token.getCompanyId(), token.getShopId());
            HashMap<String, Member> memberHashMap = memberRepository.listAsMap(token.getCompanyId());
            HashMap<String, Employee> employeeHashMap = employeeRepository.listAsMap(token.getCompanyId());

            for (Transaction transaction : onFleetTransactions) {

                for (OnFleetTask onFleetTask : onFleetTasks) {
                    if (onFleetTask.getId().equalsIgnoreCase(transaction.getOnFleetTaskId())) {
                        OnFleetTransactionResult transactionResult = new OnFleetTransactionResult();

                        transactionResult.setId(transaction.getId());
                        transactionResult.setOnFleetTaskId(transaction.getOnFleetTaskId());
                        transactionResult.setTransNo(transaction.getTransNo());
                        transactionResult.setQueueType(transaction.getQueueType());

                        Member member = memberHashMap.get(transaction.getMemberId());
                        if (member != null) {
                            transactionResult.setMemberName(member.getFirstName() + " " + member.getLastName());
                        }

                        Employee employee = employeeHashMap.get(transaction.getAssignedEmployeeId());
                        if (employee != null) {
                            transactionResult.setEmployeeName(employee.getFirstName() + " " + employee.getLastName());
                        }

                        transactionResult.setState(this.getTransactionState(onFleetTask.getState()));

                        resultList.add(transactionResult);
                        break;
                    }
                }
            }
        }

        searchResult.setSkip(0);
        searchResult.setTotal((long) resultList.size());
        searchResult.setLimit(resultList.size());
        searchResult.setValues(resultList);
        return searchResult;
    }

    private String getTransactionState(int state) {

        switch (state) {
            case 0:
                return "Unassigned";
            case 1:
                return "Assigned";
            case 2:
                return "Active";
            case 3:
                return "Completed";
        }
        return "Unassigned";
    }

    /**
     * Get all worker
     *
     * @param apiKey
     * @param states : 0 is off-duty, 1 is idle (on-duty, no active task) and 2 is active (on-duty, active task).
     */
    @Override
    public List<WorkerResult> getAllWorker(String apiKey, String states) {

        apiKey = getOnFleetAPiKeyOfShop(apiKey);

        return onFleetAPIService.getAllWorker(apiKey, states);
    }

    /**
     * Get worker by id
     *
     * @param workerId : worker id
     * @param apiKey   : api key
     */
    @Override
    public WorkerResult getWorkerById(String workerId, String apiKey) {
        if (StringUtils.isBlank(workerId)) {
            return null;
        }
        apiKey = getOnFleetAPiKeyOfShop(apiKey);
        return onFleetAPIService.getWorkerById(apiKey, workerId);
    }

    @Override
    public WorkerResult getWorkerById(String workerId) {
        if (StringUtils.isBlank(workerId)) {
            return null;
        }
        return getWorkerById(workerId, this.getOnFleetAPiKeyOfShop(StringUtils.EMPTY));
    }

    /**
     * Synchronize employee with OnFleet Worker
     *
     * @param employeeId : employee id
     * @param request
     */
    @Override
    public EmployeeSynchronizeResult syncEmployeeAtOnFleet(String employeeId, SynchronizeEmployeeRequest request) {

        EmployeeSynchronizeResult synchronizeResult = new EmployeeSynchronizeResult();
        Employee employee = employeeService.getEmployeeById(employeeId);

        if (employee == null) {
            throw new BlazeInvalidArgException(EMPLOYEE, EMPLOYEE_NOT_FOUND);
        }

        Shop shop = shopRepository.getById(request.getShopId());
        if (shop == null) {
            throw new BlazeInvalidArgException("Shop", "Shop not found");
        }

        if (StringUtils.isBlank(employee.getPhoneNumber())) {
            throw new BlazeInvalidArgException(EMPLOYEE, EMPLOYEE_INVALID_PHONE_NUMBER);
        }

        String employeeNumber = amazonServiceManager.cleanPhoneNumber(employee.getPhoneNumber(), shop).replaceAll("[^0-9]", "");
        if (StringUtils.isBlank(employeeNumber)) {
            throw new BlazeInvalidArgException(EMPLOYEE, EMPLOYEE_INVALID_PHONE_NUMBER);
        }

        String apiKey = getOnFleetAPiKeyOfShop(shop.getOnFleetApiKey());

        //Check id employee already exist
        EmployeeOnFleetInfo employeeOnFleetInfo = this.getEmployeeOnfFleetInfoByShop(employee, request.getShopId());
        if (employeeOnFleetInfo != null) {
            WorkerResult workerById = this.getWorkerById(employeeOnFleetInfo.getOnFleetWorkerId(), apiKey);
            if (workerById != null) {
                throw new BlazeInvalidArgException(EMPLOYEE, EMPLOYEE_ALREADY_EXIST);
            }
        }


        List<WorkerResult> allWorker = onFleetAPIService.getAllWorker(apiKey, "0,1,2");

        if (allWorker == null) {
            return synchronizeResult;
        }

        ObjectMapper mapper = new ObjectMapper();
        List<WorkerResult> workersList = mapper.convertValue(allWorker, new TypeReference<List<WorkerResult>>() {
        });

        EmployeeOnFleetInfo employeeInfo = this.getEmployeeOnfFleetInfoByShop(employee, shop.getId());
        if (employeeInfo == null) {
            employeeInfo = new EmployeeOnFleetInfo();
        }

        WorkerResult worker = null;

        for (WorkerResult workerResult : workersList) {

//            String employeePhoneNo = employee.getPhoneNumber().replaceAll("[^0-9]", "");
            String workerPhoneNo = workerResult.getPhone().replaceAll("[^0-9]", "");

            if (StringUtils.isNotBlank(employee.getPhoneNumber()) && workerPhoneNo.equalsIgnoreCase(employeeNumber)) {
                LinkedHashSet<String> teams = new LinkedHashSet<>();
                teams.add(request.getTeamId());

                employeeInfo.setShopId(request.getShopId());
                employeeInfo.setOnFleetWorkerId(workerResult.getId());
                employeeInfo.setOnFleetTeamList(teams);

                synchronizeResult.setSynced(true);
                synchronizeResult.setOnFleetWorkerId(workerResult.getId());
                worker = workerResult;
            }
        }

        if (worker != null && StringUtils.isNotBlank(employeeInfo.getOnFleetWorkerId())) {
            List<EmployeeOnFleetInfo> employeeOnFleetInfoList = employee.getEmployeeOnFleetInfoList();
            if (employeeOnFleetInfoList == null) {
                employeeOnFleetInfoList = new ArrayList<>();
            }

            employeeOnFleetInfoList.add(employeeInfo);
            employee.setEmployeeOnFleetInfoList(employeeOnFleetInfoList);

            employeeRepository.update(token.getCompanyId(), employeeId, employee);
        }

        return synchronizeResult;
    }

    @Override
    public EmployeeOnFleetInfo getEmployeeOnfFleetInfoByShop(Employee employee, String shopId) {
        EmployeeOnFleetInfo fleetInfo = null;
        if (employee != null && employee.getEmployeeOnFleetInfoList() != null) {
            for (EmployeeOnFleetInfo info : employee.getEmployeeOnFleetInfoList()) {
                if (shopId.equalsIgnoreCase(info.getShopId())) {
                    fleetInfo = info;
                    break;
                }
            }
        }
        return fleetInfo;
    }

    @Override
    public Employee removeWorkerDetailOfEmployee(String employeeId, CreateEmployeeRequest request) {

        Employee employee = employeeRepository.get(token.getCompanyId(), employeeId);
        if (employee == null) {
            throw new BlazeInvalidArgException(EMPLOYEE, EMPLOYEE_NOT_FOUND);
        }

        EmployeeOnFleetInfo employeeOnFleetInfo = this.getEmployeeOnfFleetInfoByShop(employee, request.getShopId());

        if (employeeOnFleetInfo == null) {
            throw new BlazeInvalidArgException(EMPLOYEE, "Worker id does not exist for this employee");
        }

        List<EmployeeOnFleetInfo> employeeOnFleetInfoList = employee.getEmployeeOnFleetInfoList();
        employeeOnFleetInfoList.remove(employeeOnFleetInfo);

        employee.setEmployeeOnFleetInfoList(employeeOnFleetInfoList);

        return employeeRepository.update(token.getCompanyId(), employeeId, employee);
    }

    /**
     * Create worker at OnFleet
     *
     * @param employeeId      : employee id
     * @param employeeRequest
     */
    @Override
    public EmployeeSynchronizeResult createWorkerForEmployee(String employeeId, CreateEmployeeRequest employeeRequest) {
        Shop shop = shopRepository.getById(employeeRequest.getShopId());

        String apiKey = getOnFleetAPiKeyOfShop(shop.getOnFleetApiKey());
        Employee employee = employeeService.getEmployeeById(employeeId);
        if (employee == null) {
            throw new BlazeInvalidArgException(EMPLOYEE, "Employee does not found");
        }
        if (StringUtils.isBlank(employee.getPhoneNumber())) {
            throw new BlazeInvalidArgException(EMPLOYEE, EMPLOYEE_INVALID_PHONE_NUMBER);
        }
        String phoneNumber = amazonServiceManager.cleanPhoneNumber(employee.getPhoneNumber(), shop);
        if (StringUtils.isBlank(phoneNumber)) {
            throw new BlazeInvalidArgException(EMPLOYEE, EMPLOYEE_INVALID_PHONE_NUMBER);
        }

        if (employeeRequest == null || StringUtils.isBlank(employeeRequest.getTeamId())) {
            throw new BlazeInvalidArgException("Team id", "Please provide team id for employee");
        }

        EmployeeOnFleetInfo employeeOnfFleetInfo = this.getEmployeeOnfFleetInfoByShop(employee, employeeRequest.getShopId());
        if (employeeOnfFleetInfo != null) {
            WorkerResult workerById = this.getWorkerById(employeeOnfFleetInfo.getOnFleetWorkerId(), apiKey);
            if (workerById != null) {
                throw new BlazeInvalidArgException(EMPLOYEE, EMPLOYEE_ALREADY_EXIST);
            }
        }

        if (employeeOnfFleetInfo == null) {
            employeeOnfFleetInfo = new EmployeeOnFleetInfo();
        }

        WorkerRequest workerRequest = new WorkerRequest();
        workerRequest.setName(employee.getFirstName() + " " + employee.getLastName());
        workerRequest.setPhone(phoneNumber);
        workerRequest.setTeams(Lists.newArrayList(employeeRequest.getTeamId()));

        WorkerVehicleRequest vehicle = new WorkerVehicleRequest();
        workerRequest.setVehicle(vehicle);

        WorkerResult worker = null;
        try {
            worker = onFleetAPIService.createWorker(apiKey, workerRequest);
        } catch (Exception e) {
            throw new BlazeInvalidArgException(EMPLOYEE, "Employee should have valid phone number and OnFleet team.");
        }

        EmployeeSynchronizeResult synchronizeResult = new EmployeeSynchronizeResult();
        if (worker != null) {
            LinkedHashSet<String> teamList = new LinkedHashSet<>();
            teamList.add(employeeRequest.getTeamId());

            employeeOnfFleetInfo.setOnFleetWorkerId(worker.getId());
            employeeOnfFleetInfo.setOnFleetTeamList(teamList);
            employeeOnfFleetInfo.setShopId(employeeRequest.getShopId());

            List<EmployeeOnFleetInfo> onFleetInfoList = employee.getEmployeeOnFleetInfoList();
            if (onFleetInfoList == null) {
                onFleetInfoList = new ArrayList<>();
            }

            onFleetInfoList.add(employeeOnfFleetInfo);

            employee.setEmployeeOnFleetInfoList(onFleetInfoList);

            employeeRepository.update(token.getCompanyId(), employeeId, employee);
            synchronizeResult.setSynced(true);
            synchronizeResult.setOnFleetWorkerId(worker.getId());
        }

        return synchronizeResult;
    }

    private String getOnFleetAPiKeyOfShop(String apiKey) {
        if (StringUtils.isBlank(apiKey)) {
            Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());
            apiKey = shop.getOnFleetApiKey();
        }

        if (StringUtils.isBlank(apiKey)) {
            throw new BlazeInvalidArgException(API_KEY, INVALID_API_KEY);
        }
        return apiKey;
    }

    @Override
    public List<TeamResult> getAllTeams(String apiKey) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            String data = onFleetAPIService.getAllTeams(getOnFleetAPiKeyOfShop(apiKey), token.getCompanyId(), token.getShopId());
            return mapper.readValue(data, new TypeReference<List<TeamResult>>() {
            });

        } catch (IOException e) {
            LOG.error(e.getMessage());
            return null;
        }
    }

    @Override
    public void reAssignTask(Shop shop, Transaction transaction, String workerId, String onFleetTeamId, Employee employee, EmployeeOnFleetInfo employeeOnFleetInfo, Member member) {
        if (transaction.getCreateOnfleetTask()) {
            createOnFleetTask(shop, transaction, workerId, onFleetTeamId, employee, employeeOnFleetInfo, member, true);
        }
    }

    /**
     * This method gets list of OnFleet teams for all shops listed in company
     */
    @Override
    public List<TeamByCompanyResult> getAllTeamsByCompany() {

        List<TeamByCompanyResult> resultList = new ArrayList<>();

        SearchResult<Shop> shopSearchResult = shopRepository.findItems(token.getCompanyId(), 0, Integer.MAX_VALUE);


        if (shopSearchResult.getValues() != null && !shopSearchResult.getValues().isEmpty()) {
            Iterable<OnFleetTeams> onFleetTeamsList = onFleetTeamsRepository.list(token.getCompanyId());
            Map<String, OnFleetTeams> teamByShopMap = new HashMap<>();
            for (OnFleetTeams onFleetTeams : onFleetTeamsList) {
                teamByShopMap.put(onFleetTeams.getShopId(), onFleetTeams);
            }


            for (Shop shop : shopSearchResult.getValues()) {
                TeamByCompanyResult teamResult = new TeamByCompanyResult();
                teamResult.setShopId(shop.getId());
                teamResult.setShopName(shop.getName());
                if (StringUtils.isNotBlank(shop.getOnFleetApiKey()) && StringUtils.isNotBlank(shop.getHubId())) {
                    OnFleetTeams onFleetTeams = teamByShopMap.get(shop.getId());
                    if (onFleetTeams != null && onFleetTeams.getOnFleetTeamInfoList() != null) {
                        List<OnFleetTeamInfo> onFleetTeamInfoList = onFleetTeams.getOnFleetTeamInfoList();

                        teamResult.setTeamList(onFleetTeamInfoList);
                    }
                }
                resultList.add(teamResult);
            }
        }

        return resultList;
    }

    @Override
    public Employee updateEmployeeOnFleetDetails(String employeeId, CreateEmployeeRequest request) {
        Employee employee = employeeService.getEmployeeById(employeeId);
        if (employee == null) {
            throw new BlazeInvalidArgException(EMPLOYEE, "Employee does not found");
        }
        Shop shop = shopRepository.get(token.getCompanyId(), request.getShopId());
        if (shop == null) {
            throw new BlazeInvalidArgException("Shop", "Shop does not exist");
        }
        if (StringUtils.isBlank(employee.getPhoneNumber())) {
            throw new BlazeInvalidArgException(EMPLOYEE, EMPLOYEE_INVALID_PHONE_NUMBER);
        }

        String apiKey = this.getOnFleetAPiKeyOfShop(shop.getOnFleetApiKey());

        LinkedHashSet<String> teamList = new LinkedHashSet<>();
        teamList.add(request.getTeamId());

        List<EmployeeOnFleetInfo> onFleetInfoList = employee.getEmployeeOnFleetInfoList();
        EmployeeOnFleetInfo employeeOnfFleetInfo = this.getEmployeeOnfFleetInfoByShop(employee, request.getShopId());

        UpdateWorkerRequest updateWorkerRequest = new UpdateWorkerRequest();
        updateWorkerRequest.setName(employee.getFirstName() + " " + employee.getLastName());
        updateWorkerRequest.setTeams(teamList);


        if (employeeOnfFleetInfo == null) {
            throw new BlazeInvalidArgException(EMPLOYEE, "Employee does not have team for this shop");
        }

        WorkerResult workerResult = onFleetAPIService.updateWorker(apiKey, employeeOnfFleetInfo.getOnFleetWorkerId(), updateWorkerRequest);

        employeeOnfFleetInfo.setOnFleetTeamList(teamList);

        employee.setEmployeeOnFleetInfoList(onFleetInfoList);
        return employeeRepository.update(token.getCompanyId(), employeeId, employee);
    }

    @Override
    public WebHookResponse createOnfleetWebhook(CreateWebhookRequest request, String apiEndpoint) {
        WebhookAddRequest webHookRequest = new WebhookAddRequest();
        String url = this.getWebhookURL(request.getTrigger(), apiEndpoint);
        String apiKey = this.getOnFleetAPiKeyOfShop(StringUtils.EMPTY);

        LOG.info("WebHook Url : " + url + "\tAPI key " + apiKey + "\ttype :" + request.getTrigger());
        webHookRequest.setUrl(url);
        webHookRequest.setTrigger(request.getTrigger().getTriggerId());
        webHookRequest.setThreshold(request.getThreshold());

        return this.onFleetAPIService.createWebhook(webHookRequest, apiKey);
    }

    private String getWebhookURL(OnFleetWebhookType trigger, String apiEndpoint) {

        String platformDNS = connectConfiguration.getAppApiUrl();
        String url = platformDNS + apiEndpoint + "/webhook/receiver";

        if (trigger.equals(OnFleetWebhookType.TASK_ARRIVAL)) {
            url = url + "/taskArrival";
        } else if (trigger.equals(OnFleetWebhookType.TASK_COMPLETED)) {
            url = url + "/taskCompleted";
        } else if (trigger.equals(OnFleetWebhookType.TASK_FAILED)) {
            url = url + "/taskFailed";
        } else {
            throw new BlazeInvalidArgException("Trigger", "Invalid trigger type");
        }

        return url;
    }

    @Override
    public void handleOnFleetWebHookRequest(String request, String data) {
        //TODO : Remove email functionality when it's tested successfully
        amazonServiceManager.sendEmail("support@blaze.me", "testjava03@gmail.com", "webhook received response", request + data, null, null, "Blaze Support");
    }

    @Override
    public void handleOnFleetWebHookTaskArrival(String request, WebHookRequest data) {

        try {
            ObjectMapper mapper = new ObjectMapper();
            String response = mapper.writeValueAsString(data);
            LOG.info("handleOnFleetWebHookTaskArrival response " + response);

            handleOnFleetWebHookRequest("Task Arrival : ", response);

            Transaction transaction = null;
            if (data != null) {
                transaction = transactionRepository.getTransactionByTaskId(data.getTaskId());
            }

            if (transaction != null && data.getData() != null && data.getData().getTask() != null) {
                /*transaction.setState(data.getData().getTask().getState());
                transaction.setOnFleetTaskStatus(Transaction.OnFleetTaskStatus.TASK_ARRIVAL);

                transactionRepository.update(transaction.getCompanyId(), transaction.getId(), transaction);*/
                transactionRepository.updateOnfleetInfo(transaction.getCompanyId(), transaction.getId(), data.getData().getTask().getState(), Transaction.OnFleetTaskStatus.TASK_ARRIVAL);
            }

        } catch (Exception e) {
            LOG.error(e);
        }
    }

    @Override
    public void handleOnFleetWebHookTaskCompleted(String request, WebHookRequest data) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            String response = mapper.writeValueAsString(data);
            LOG.info("handleOnFleetWebHookTaskCompleted response " + response);
            handleOnFleetWebHookRequest("Task Completed : ", response);

            Transaction transaction = null;
            if (data != null) {
                transaction = transactionRepository.getTransactionByTaskId(data.getTaskId());
            }

            if (transaction != null && data.getData() != null && data.getData().getTask() != null) {
                /*transaction.setState(data.getData().getTask().getState());
                transaction.setOnFleetTaskStatus(Transaction.OnFleetTaskStatus.TASK_COMPLETED);

                transactionRepository.update(transaction.getCompanyId(), transaction.getId(), transaction);*/
                transactionRepository.updateOnfleetInfo(transaction.getCompanyId(), transaction.getId(), data.getData().getTask().getState(), Transaction.OnFleetTaskStatus.TASK_COMPLETED);
            }
        } catch (Exception e) {
            LOG.error("Error in task complete web hook : " + e.getMessage());
        }
    }

    @Override
    public void handleOnFleetWebHookTaskFailed(String request, WebHookRequest data) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            String response = mapper.writeValueAsString(data);
            LOG.info("handleOnFleetWebHookTaskFailed response " + response);

            handleOnFleetWebHookRequest("Task Failed : ", response);

            Transaction transaction = null;
            if (data != null) {
                transaction = transactionRepository.getTransactionByTaskId(data.getTaskId());
            }

            if (transaction != null && data.getData() != null && data.getData().getTask() != null) {
                /*transaction.setState(data.getData().getTask().getState());
                transaction.setOnFleetTaskStatus(Transaction.OnFleetTaskStatus.TASK_FAILED);

                transactionRepository.update(transaction.getCompanyId(), transaction.getId(), transaction);*/
                transactionRepository.updateOnfleetInfo(transaction.getCompanyId(), transaction.getId(), data.getData().getTask().getState(), Transaction.OnFleetTaskStatus.TASK_FAILED);
            }

        } catch (Exception e) {
            LOG.error(e);
        }
    }

    /**
     * This method synchronize onlfeet's team with system (Blaze)
     *
     * @param request : Required information for synchronize team
     */
    @Override
    public void synchronizeTeamForShop(SynchronizeTeamRequest request) {
        String shopId = request.getShopId();
        if (StringUtils.isBlank(shopId) || !ObjectId.isValid(shopId)) {
            shopId = token.getShopId();
        }

        Shop shop = shopRepository.get(token.getCompanyId(), shopId);
        if (shop == null && StringUtils.isBlank(shop.getOnFleetApiKey())) {
            throw new BlazeInvalidArgException(SHOP, SHOP_NOT_FOUND);
        }

        if (StringUtils.isBlank(shop.getOnFleetApiKey())) {
            throw new BlazeInvalidArgException(API_KEY, INVALID_API_KEY);
        }

        OnFleetTeams onFleetTeams = onFleetTeamsRepository.getByCompanyAndShop(token.getCompanyId(), shopId);

        boolean create = false;
        if (onFleetTeams == null) {
            create = true;
            onFleetTeams = new OnFleetTeams();
            onFleetTeams.prepare(token.getCompanyId());
            onFleetTeams.setShopId(shop.getId());
        }

        onFleetTeams.setSyncState(OnFleetTeams.SyncState.InProgress);
        if (create) {
            onFleetTeamsRepository.save(onFleetTeams);
        } else {
            onFleetTeamsRepository.update(token.getCompanyId(), onFleetTeams);
        }

        onFleetManager.synchronizeTeam(shop.getOnFleetApiKey(), token.getCompanyId(), shop, onFleetTeams);

    }

    @Override
    public OnFleetTeams getOnFleetTeamListByShop(String shopId) {
        if (StringUtils.isBlank(shopId) || !ObjectId.isValid(shopId)) {
            shopId = token.getShopId();
        }

        return onFleetTeamsRepository.getByCompanyAndShop(token.getCompanyId(), shopId);
    }

    @Override
    public List<OnFleetTeamResult> getOnFleetTeamListByCompany(String companyId) {
        if (StringUtils.isBlank(companyId) || !ObjectId.isValid(companyId)) {
            companyId = token.getCompanyId();
        }

        Iterable<OnFleetTeamResult> teamResults = onFleetTeamsRepository.list(companyId, OnFleetTeamResult.class);

        HashMap<String, Shop> shopHashMap = shopRepository.listAsMap(companyId);

        ArrayList<OnFleetTeamResult> teams = Lists.newArrayList(teamResults);

        for (OnFleetTeamResult teamResult : teams) {
            if (shopHashMap.get(teamResult.getShopId()) != null) {
                teamResult.setShopName(shopHashMap.get(teamResult.getShopId()).getName());
            }
        }

        return teams;
    }

    @Override
    public void completeTransactionById(String transactionId, StatusRequest request) {

        Shop shop = shopRepository.get(token.getCompanyId(), token.getShopId());

        if (shop == null) {
            throw new BlazeInvalidArgException(SHOP, SHOP_NOT_FOUND);
        }

        Transaction transaction = transactionRepository.get(token.getCompanyId(), transactionId);

        if (transaction == null) {
            throw new BlazeInvalidArgException(TRANSACTION, TRANSACTION_NOT_FOUND);
        }

        if (!transaction.getCreateOnfleetTask() && StringUtils.isEmpty(transaction.getOnFleetTaskId())) {
            throw new BlazeInvalidArgException(TRANSACTION, "OnFleet task is not available for this transaction");
        }

        onFleetManager.completeTask(shop, transaction, request);

    }

    @Override
    public void updateOnfleetTaskSchedule(Shop shop, Transaction dbTransaction, String employeeWorkerId, Employee employee, EmployeeOnFleetInfo employeeOnFleetInfo, Member member) {
        onFleetManager.updateOnfleetTaskSchedule(token.getCompanyId(), shop, dbTransaction, employeeWorkerId, employee, employeeOnFleetInfo, member);
    }

    /**
     * This method completes task at onfleet
     * @param shop: shop instance
     * @param trans: transaction instance
     */
    @Override
    public void completeTaskAtOnFleet(Shop shop, Transaction trans, boolean taskStatus) {
        StatusRequest request = new StatusRequest();
        request.setStatus(taskStatus);
        onFleetManager.completeTaskAtOnfleet(shop, trans, request);
    }

    /**
     * This method deletes task at Onfleet
     * @param shop: shop instance
     * @param transaction: transaction instance
     */
    @Override
    public void deleteOrMarkFailedTaskAtOnfleet(Shop shop, Transaction transaction) {
        onFleetManager.deleteOrMarkFailedTaskAtOnfleet(shop, transaction);
    }
}
