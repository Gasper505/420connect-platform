package com.fourtwenty.core.thirdparty.springbig.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SpringBigMemberRequest {

    private SpringBigMember member = new SpringBigMember();

    public SpringBigMember getMember() {
        return member;
    }

    public void setMember(SpringBigMember member) {
        this.member = member;
    }
}
