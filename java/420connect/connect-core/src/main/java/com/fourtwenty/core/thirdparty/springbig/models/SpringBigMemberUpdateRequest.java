package com.fourtwenty.core.thirdparty.springbig.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SpringBigMemberUpdateRequest {

    private MemberUpdateRequest member;

    public MemberUpdateRequest getMember() {
        return member;
    }

    public void setMember(MemberUpdateRequest member) {
        this.member = member;
    }
}
