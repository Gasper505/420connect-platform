package com.fourtwenty.core.thirdparty.springbig.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SpringBigVisitRequest {

    private VisitRequest visit;

    public VisitRequest getVisit() {
        return visit;
    }

    public void setVisit(VisitRequest visit) {
        this.visit = visit;
    }
}
