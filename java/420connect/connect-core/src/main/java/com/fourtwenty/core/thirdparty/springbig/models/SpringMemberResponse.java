package com.fourtwenty.core.thirdparty.springbig.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SpringMemberResponse {

    private SpringBigMember member;

    public SpringBigMember getMember() {
        return member;
    }

    public void setMember(SpringBigMember member) {
        this.member = member;
    }
}
