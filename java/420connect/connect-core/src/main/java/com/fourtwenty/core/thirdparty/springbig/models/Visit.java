package com.fourtwenty.core.thirdparty.springbig.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Visit {

    @JsonProperty("id")
    private Long id;
    @JsonProperty("merchant_id")
    private Long merchantId;
    @JsonProperty("pos_user")
    private String posUser;
    @JsonProperty("points_earned")
    private Long pointsEarned;
    @JsonProperty("points_available")
    private Long pointsAvailable;
    @JsonProperty("transaction_date")
    private String transactionDate;
    @JsonProperty("visit_details")
    private List<VisitDetail> visitDetails = new ArrayList<>();
    @JsonProperty("pos_type")
    private String posType;
    @JsonProperty("send_notification")
    private String sendNotification;
    @JsonProperty("created_at")
    private String created;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(Long merchantId) {
        this.merchantId = merchantId;
    }

    public String getPosUser() {
        return posUser;
    }

    public void setPosUser(String posUser) {
        this.posUser = posUser;
    }

    public Long getPointsEarned() {
        return pointsEarned;
    }

    public void setPointsEarned(Long pointsEarned) {
        this.pointsEarned = pointsEarned;
    }

    public Long getPointsAvailable() {
        return pointsAvailable;
    }

    public void setPointsAvailable(Long pointsAvailable) {
        this.pointsAvailable = pointsAvailable;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    public List<VisitDetail> getVisitDetails() {
        return visitDetails;
    }

    public void setVisitDetails(List<VisitDetail> visitDetails) {
        this.visitDetails = visitDetails;
    }

    public String getPosType() {
        return posType;
    }

    public void setPosType(String posType) {
        this.posType = posType;
    }

    public String getSendNotification() {
        return sendNotification;
    }

    public void setSendNotification(String sendNotification) {
        this.sendNotification = sendNotification;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }
}
