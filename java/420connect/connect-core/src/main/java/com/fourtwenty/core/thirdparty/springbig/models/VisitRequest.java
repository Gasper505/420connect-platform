package com.fourtwenty.core.thirdparty.springbig.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fourtwenty.core.domain.serializers.BigDecimalPercentageSerializer;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class VisitRequest {

    @JsonProperty("transaction_total")
    @JsonSerialize(using = BigDecimalPercentageSerializer.class)
    @DecimalMin("0")
    private BigDecimal transactionTotal = new BigDecimal(0);

    @JsonProperty("pos_id")
    private String posId;

    @JsonProperty("pos_user")
    private String posUser;

    @JsonProperty("visit_details_attributes")
    private List<VisitDetail> visitDetails = new ArrayList<>();

    @JsonProperty("pos_type")
    private String posType;

    @JsonProperty("transaction_date")
    private String transactionDate;

    @JsonProperty("send_notification")
    private String sendNotification;


    @JsonProperty("location")
    private String location;

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public BigDecimal getTransactionTotal() {
        return transactionTotal;
    }

    public void setTransactionTotal(BigDecimal transactionTotal) {
        this.transactionTotal = transactionTotal;
    }

    public String getPosId() {
        return posId;
    }

    public void setPosId(String posId) {
        this.posId = posId;
    }

    public String getPosUser() {
        return posUser;
    }

    public void setPosUser(String posUser) {
        this.posUser = posUser;
    }

    public List<VisitDetail> getVisitDetails() {
        return visitDetails;
    }

    public void setVisitDetails(List<VisitDetail> visitDetails) {
        this.visitDetails = visitDetails;
    }

    public String getPosType() {
        return posType;
    }

    public void setPosType(String posType) {
        this.posType = posType;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getSendNotification() {
        return sendNotification;
    }

    public void setSendNotification(String sendNotification) {
        this.sendNotification = sendNotification;
    }

}
