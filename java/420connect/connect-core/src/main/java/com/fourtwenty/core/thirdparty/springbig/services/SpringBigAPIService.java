package com.fourtwenty.core.thirdparty.springbig.services;

import com.fourtwenty.core.domain.models.thirdparty.SpringBigInfo;
import com.fourtwenty.core.thirdparty.springbig.models.*;

public interface SpringBigAPIService {
    SpringMemberResponse getMemberByPhoneNumber(SpringBigInfo spInfo, String primaryPhone);

    SpringMemberResponse getMemberByByMemberId(SpringBigInfo spInfo, String memberId);

    SpringMemberResponse createMember(SpringBigInfo spInfo, SpringBigMemberRequest springBigMemberRequest);

    SpringBigVisitResponse createVisit(SpringBigInfo spInfo, SpringBigVisitRequest visitData);

    SpringMemberResponse updateMember(SpringBigInfo spInfo, SpringBigMemberUpdateRequest memberUpdateRequest, String memberId);
}
