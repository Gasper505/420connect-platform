package com.fourtwenty.core.thirdparty.springbig.services.impl;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.config.SpringBigConfiguration;
import com.fourtwenty.core.domain.models.thirdparty.SpringBigInfo;
import com.fourtwenty.core.thirdparty.springbig.models.*;
import com.fourtwenty.core.thirdparty.springbig.services.SpringBigAPIService;
import com.mdo.pusher.RestErrorException;
import com.mdo.pusher.SimpleRestUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.inject.Inject;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import java.util.ArrayList;

public class SpringBigAPIServiceImpl implements SpringBigAPIService {

    @Inject
    private ConnectConfiguration connectConfiguration;

    private static final Log LOG = LogFactory.getLog(SpringBigAPIServiceImpl.class);

    private static final String sEND_POINT = "https://api-staging.springbig.com/pos/v1";
    private static final String sMEMBER_END_POINT = "https://api-staging.springbig.com/general/v1";
    private static final String ERROR_GET_MEMBER = "Error while getting member by phone number ";
    private static final String ERROR_GET_MEMBER_ID = "Error while getting member by phone number ";
    private static final String ERROR_CREATE_MEMBER = "Error while creating member ";
    private static final String ERROR_UPDATE_MEMBER = "Error while updating member ";
    private static final String ERROR_CREATE_VISIT = "Error while creating visit ";

    private String getSPEndPoint(SpringBigInfo spInfo) {
        if (spInfo.getEnv() == SpringBigInfo.SpringBigEnv.STAGE) {
            return connectConfiguration.getSpringBigConfiguration().getStageEndPoint();
        } else if (spInfo.getEnv() == SpringBigInfo.SpringBigEnv.PROD) {
            return connectConfiguration.getSpringBigConfiguration().getProdEndPoint();
        }
        return "";
    }

    @Override
    public SpringMemberResponse getMemberByPhoneNumber(SpringBigInfo spInfo, String primaryPhone) {

        String url = getSPEndPoint(spInfo) + "/pos/v1/members?phone_number=" + primaryPhone;
        LOG.info("springBig url: " + url);
        try {
            MultivaluedMap<String, Object> headers = this.getHeader(spInfo);
            ArrayList list = SimpleRestUtil.getWithSsl(url, ArrayList.class, headers);
            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.readValue(objectMapper.writeValueAsString(list.get(0)), SpringMemberResponse.class);
        } catch (Exception e) {
            LOG.error(ERROR_GET_MEMBER + e.getMessage());
        }

        return null;
    }

    @Override
    public SpringMemberResponse getMemberByByMemberId(SpringBigInfo spInfo, String memberId) {
        String url = getSPEndPoint(spInfo) + "/pos/v1/members/" + memberId;
        LOG.info("springBig url: " + url);
        try {
            MultivaluedMap<String, Object> headers = this.getHeader(spInfo);
            SpringMemberResponse response = SimpleRestUtil.getWithSsl(url, SpringMemberResponse.class, headers);

            System.out.println(response);
            return response;
        } catch (Exception e) {
            LOG.error(ERROR_GET_MEMBER_ID + e.getMessage());
        }

        return null;
    }

    @Override
    public SpringMemberResponse createMember(SpringBigInfo spInfo, SpringBigMemberRequest springBigMemberRequest) {
        String url = getSPEndPoint(spInfo) + "/pos/v1/members";

        LOG.info("springBig url: " + url);
        try {
            MultivaluedMap<String, Object> headers = this.getHeader(spInfo);
//            return SimpleRestUtil.postWithSsl2(url, springBigMemberRequest, SpringMemberResponse.class, String.class, headers);
            return SimpleRestUtil.postWithSsl(url, springBigMemberRequest, SpringMemberResponse.class, headers);

        } catch (RestErrorException e) {
            LOG.error(ERROR_CREATE_MEMBER + e.getMessage() + "\tCause : " + e.getErrorResponse());
        } catch (Exception e) {
            LOG.error(ERROR_CREATE_MEMBER + e.getMessage() + "\tCause : " + e.getCause());
        }

        return null;
    }

    @Override
    public SpringBigVisitResponse createVisit(SpringBigInfo spInfo, SpringBigVisitRequest visitData) {
        String url = getSPEndPoint(spInfo) + "/pos/v1/visits";

        LOG.info("springBig url: " + url);
        try {

            MultivaluedMap<String, Object> headers = this.getHeader(spInfo);

            String response = SimpleRestUtil.postWithSsl(url, visitData, String.class, headers);

            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
            return objectMapper.readValue(response, SpringBigVisitResponse.class);

        } catch (Exception e) {
            LOG.error(ERROR_CREATE_VISIT + e.getMessage());
        }

        return null;
    }

    @Override
    public SpringMemberResponse updateMember(SpringBigInfo spInfo, SpringBigMemberUpdateRequest memberUpdateRequest, String memberId) {
        String url = getSPEndPoint(spInfo) + "/pos/v1/members/" + memberId;

        LOG.info("springBig url: " + url);
        try {

            MultivaluedMap<String, Object> headers = this.getHeader(spInfo);
            return SimpleRestUtil.putWithSsl(url, memberUpdateRequest, SpringMemberResponse.class, String.class, headers);
        } catch (RestErrorException e) {
            LOG.error(ERROR_CREATE_MEMBER + e.getMessage() + "\tCause : " + e.getErrorResponse());
        } catch (Exception e) {
            LOG.error(ERROR_UPDATE_MEMBER + e.getMessage() + "\tCause : " + e.getCause());
        }

        return null;
    }

    private MultivaluedMap<String, Object> getHeader(SpringBigInfo spInfo) {
        String authToken = "";
        SpringBigConfiguration configuration = connectConfiguration.getSpringBigConfiguration();
        if (spInfo != null) {
            authToken = spInfo.getAuthToken();
        }

        MultivaluedMap<String, Object> headers = new MultivaluedHashMap<>();
        headers.putSingle("AUTH-TOKEN ", authToken);
        headers.putSingle("X-Api-Key", configuration.getApiKey());

        return headers;
    }


}
