package com.fourtwenty.core.thirdparty.tookan.model.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.validator.constraints.NotEmpty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TookanAccountRequest {
    @NotEmpty
    private String shopId;
    private String apiKey;
    private boolean active;
    private boolean resetTookan;

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isResetTookan() {
        return resetTookan;
    }

    public void setResetTookan(boolean resetTookan) {
        this.resetTookan = resetTookan;
    }
}
