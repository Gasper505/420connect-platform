package com.fourtwenty.core.thirdparty.tookan.model.request.fleets;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fourtwenty.core.thirdparty.tookan.model.request.TookanBaseRequest;

public class AgentDeleteRequest extends TookanBaseRequest {
    @JsonProperty("fleet_id")
    private String fleetId;

    public String getFleetId() {
        return fleetId;
    }

    public void setFleetId(String fleetId) {
        this.fleetId = fleetId;
    }
}
