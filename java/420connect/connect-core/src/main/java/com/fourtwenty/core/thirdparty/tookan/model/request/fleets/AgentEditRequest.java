package com.fourtwenty.core.thirdparty.tookan.model.request.fleets;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AgentEditRequest extends AgentAddRequest {

    @JsonProperty("fleet_id")
    private String fleetId;

    public String getFleetId() {
        return fleetId;
    }

    public void setFleetId(String fleetId) {
        this.fleetId = fleetId;
    }
}
