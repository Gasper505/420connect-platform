package com.fourtwenty.core.thirdparty.tookan.model.request.fleets;

import com.fourtwenty.core.thirdparty.onfleet.models.request.SynchronizeTeamRequest;
import org.hibernate.validator.constraints.NotEmpty;

public class AgentSynchronizeRequest extends SynchronizeTeamRequest {
    @NotEmpty
    private String teamId;

    public String getTeamId() {
        return teamId;
    }

    public void setTeamId(String teamId) {
        this.teamId = teamId;
    }

}
