package com.fourtwenty.core.thirdparty.tookan.model.request.fleets;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fourtwenty.core.thirdparty.tookan.model.response.TookanAgentInfo;
import com.fourtwenty.core.thirdparty.tookan.model.response.TookanTaskInfo;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TookanAgentProfileInfo {
    @JsonProperty("fleet_details")
    List<TookanAgentInfo> fleetDetails = new ArrayList<>();
    @JsonProperty("jobs")
    List<TookanTaskInfo> jobs = new ArrayList<>();

    public List<TookanAgentInfo> getFleetDetails() {
        return fleetDetails;
    }

    public void setFleetDetails(List<TookanAgentInfo> fleetDetails) {
        this.fleetDetails = fleetDetails;
    }

    public List<TookanTaskInfo> getJobs() {
        return jobs;
    }

    public void setJobs(List<TookanTaskInfo> jobs) {
        this.jobs = jobs;
    }
}
