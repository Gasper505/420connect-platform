package com.fourtwenty.core.thirdparty.tookan.model.request.fleets;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fourtwenty.core.domain.models.generic.BaseModel;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TookanFleetInfo extends BaseModel {
    @JsonProperty("fleet_id")
    private Long fleetId;
    @JsonProperty("fleet_name")
    private String fleetName;
    @JsonProperty("phone")
    private String phone;
    @JsonProperty("is_deleted")
    private Long isDeleted;
    @JsonProperty("status")
    private Long status;
    @JsonProperty("email")
    private String email;

    public Long getFleetId() {
        return fleetId;
    }

    public void setFleetId(Long fleetId) {
        this.fleetId = fleetId;
    }

    public String getFleetName() {
        return fleetName;
    }

    public void setFleetName(String fleetName) {
        this.fleetName = fleetName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Long getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Long isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
