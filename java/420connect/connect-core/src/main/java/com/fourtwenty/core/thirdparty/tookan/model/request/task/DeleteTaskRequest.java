package com.fourtwenty.core.thirdparty.tookan.model.request.task;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fourtwenty.core.thirdparty.tookan.model.request.TookanBaseRequest;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DeleteTaskRequest extends TookanBaseRequest {
    @JsonProperty("job_id")
    private String jobId;

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }
}
