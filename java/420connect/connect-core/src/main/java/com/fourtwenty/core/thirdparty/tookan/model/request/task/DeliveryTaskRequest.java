package com.fourtwenty.core.thirdparty.tookan.model.request.task;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fourtwenty.core.thirdparty.tookan.model.request.TookanBaseRequest;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DeliveryTaskRequest extends TookanBaseRequest {
    @JsonProperty("order_id")
    private String orderId;
    @JsonProperty("customer_email")
    private String customerEmail;
    @JsonProperty("customer_username")
    private String customerUsername;
    @JsonProperty("customer_phone")
    private String customerPhone;
    @JsonProperty("customer_address")
    private String customerAddress;
    @JsonProperty("job_delivery_datetime")
    private String jobDeliveryDatetime;
    @JsonProperty("team_id")
    private String teamId;
    @JsonProperty("timezone")
    private String timezone;
    @JsonProperty("fleet_id")
    private String fleetId;
    @JsonProperty("tracking_link")
    private Long trackingLink;
    @JsonProperty("has_pickup")
    private Long hasPickup;
    @JsonProperty("has_delivery")
    private Long hasDelivery;
    @JsonProperty("layout_type")
    private Long layoutType;
    @JsonProperty("auto_assignment")
    private boolean isAutoAssigned;
    @JsonProperty("custom_field_template")
    private String template;
    @JsonProperty("meta_data")
    private List<TookanCustomFields> customFieldsList;
    @JsonProperty("job_description")
    private String jobDescription;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public String getCustomerUsername() {
        return customerUsername;
    }

    public void setCustomerUsername(String customerUsername) {
        this.customerUsername = customerUsername;
    }

    public String getCustomerPhone() {
        return customerPhone;
    }

    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getJobDeliveryDatetime() {
        return jobDeliveryDatetime;
    }

    public void setJobDeliveryDatetime(String jobDeliveryDatetime) {
        this.jobDeliveryDatetime = jobDeliveryDatetime;
    }

    public String getTeamId() {
        return teamId;
    }

    public void setTeamId(String teamId) {
        this.teamId = teamId;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getFleetId() {
        return fleetId;
    }

    public void setFleetId(String fleetId) {
        this.fleetId = fleetId;
    }

    public Long getTrackingLink() {
        return trackingLink;
    }

    public void setTrackingLink(Long trackingLink) {
        this.trackingLink = trackingLink;
    }

    public Long getHasPickup() {
        return hasPickup;
    }

    public void setHasPickup(Long hasPickup) {
        this.hasPickup = hasPickup;
    }

    public Long getHasDelivery() {
        return hasDelivery;
    }

    public void setHasDelivery(Long hasDelivery) {
        this.hasDelivery = hasDelivery;
    }

    public Long getLayoutType() {
        return layoutType;
    }

    public void setLayoutType(Long layoutType) {
        this.layoutType = layoutType;
    }

    public boolean isAutoAssigned() {
        return isAutoAssigned;
    }

    public void setAutoAssigned(boolean autoAssigned) {
        isAutoAssigned = autoAssigned;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public List<TookanCustomFields> getCustomFieldsList() {
        return customFieldsList;
    }

    public void setCustomFieldsList(List<TookanCustomFields> customFieldsList) {
        this.customFieldsList = customFieldsList;
    }

    public String getJobDescription() {
        return jobDescription;
    }

    public void setJobDescription(String jobDescription) {
        this.jobDescription = jobDescription;
    }
}
