package com.fourtwenty.core.thirdparty.tookan.model.request.task;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fourtwenty.core.thirdparty.tookan.model.request.TookanBaseRequest;

import java.util.List;


@JsonIgnoreProperties(ignoreUnknown = true)
public class PickUpTaskRequest extends TookanBaseRequest {
    @JsonProperty("order_id")
    private String orderId;
    @JsonProperty("job_pickup_address")
    private String jobPickupAddress;
    @JsonProperty("job_pickup_datetime")
    private String jobPickupDatetime;
    @JsonProperty("job_pickup_phone")
    private String jobPickupPhone;
    @JsonProperty("tracking_link")
    private Long trackingLink;
    @JsonProperty("has_pickup")
    private Long hasPickup;
    @JsonProperty("has_delivery")
    private Long hasDelivery;
    @JsonProperty("layout_type")
    private Long layoutType;
    @JsonProperty("team_id")
    private String teamId;
    @JsonProperty("timezone")
    private String timezone;
    @JsonProperty("fleet_id")
    private String fleetId;
    @JsonProperty("auto_assignment")
    private boolean isAutoAssigned;
    @JsonProperty("pickup_custom_field_template")
    private String template;
    @JsonProperty("pickup_meta_data")
    private List<TookanCustomFields> customFieldsList;
    @JsonProperty("job_description")
    private String jobDescription;
    @JsonProperty("job_pickup_name")
    private String jobPickupName;
    @JsonProperty("job_pickup_email")
    private String jobPickupEmail;


    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getJobPickupAddress() {
        return jobPickupAddress;
    }

    public void setJobPickupAddress(String jobPickupAddress) {
        this.jobPickupAddress = jobPickupAddress;
    }

    public String getJobPickupDatetime() {
        return jobPickupDatetime;
    }

    public void setJobPickupDatetime(String jobPickupDatetime) {
        this.jobPickupDatetime = jobPickupDatetime;
    }

    public String getJobPickupPhone() {
        return jobPickupPhone;
    }

    public void setJobPickupPhone(String jobPickupPhone) {
        this.jobPickupPhone = jobPickupPhone;
    }

    public Long getTrackingLink() {
        return trackingLink;
    }

    public void setTrackingLink(Long trackingLink) {
        this.trackingLink = trackingLink;
    }

    public Long getHasPickup() {
        return hasPickup;
    }

    public void setHasPickup(Long hasPickup) {
        this.hasPickup = hasPickup;
    }

    public Long getHasDelivery() {
        return hasDelivery;
    }

    public void setHasDelivery(Long hasDelivery) {
        this.hasDelivery = hasDelivery;
    }

    public Long getLayoutType() {
        return layoutType;
    }

    public void setLayoutType(Long layoutType) {
        this.layoutType = layoutType;
    }

    public String getTeamId() {
        return teamId;
    }

    public void setTeamId(String teamId) {
        this.teamId = teamId;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getFleetId() {
        return fleetId;
    }

    public void setFleetId(String fleetId) {
        this.fleetId = fleetId;
    }

    public boolean isAutoAssigned() {
        return isAutoAssigned;
    }

    public void setAutoAssigned(boolean autoAssigned) {
        isAutoAssigned = autoAssigned;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public List<TookanCustomFields> getCustomFieldsList() {
        return customFieldsList;
    }

    public void setCustomFieldsList(List<TookanCustomFields> customFieldsList) {
        this.customFieldsList = customFieldsList;
    }

    public String getJobDescription() {
        return jobDescription;
    }

    public void setJobDescription(String jobDescription) {
        this.jobDescription = jobDescription;
    }

    public String getJobPickupName() {
        return jobPickupName;
    }

    public void setJobPickupName(String jobPickupName) {
        this.jobPickupName = jobPickupName;
    }

    public String getJobPickupEmail() {
        return jobPickupEmail;
    }

    public void setJobPickupEmail(String jobPickupEmail) {
        this.jobPickupEmail = jobPickupEmail;
    }
}
