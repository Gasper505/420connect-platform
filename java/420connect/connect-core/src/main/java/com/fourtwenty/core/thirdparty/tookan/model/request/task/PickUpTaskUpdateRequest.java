package com.fourtwenty.core.thirdparty.tookan.model.request.task;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PickUpTaskUpdateRequest extends PickUpTaskRequest {
    @JsonProperty("job_id")
    private String jobId;
    @JsonProperty("job_status")
    private long jobStatus;

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public long getJobStatus() {
        return jobStatus;
    }

    public void setJobStatus(long jobStatus) {
        this.jobStatus = jobStatus;
    }
}
