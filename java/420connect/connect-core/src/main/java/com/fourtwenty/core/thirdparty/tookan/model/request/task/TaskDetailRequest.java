package com.fourtwenty.core.thirdparty.tookan.model.request.task;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fourtwenty.core.thirdparty.tookan.model.request.TookanBaseRequest;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TaskDetailRequest extends TookanBaseRequest {

    @JsonProperty("job_type")
    private long jobType;
    @JsonProperty("start_date")
    private String startDate;
    @JsonProperty("end_date")
    private String endDate;

    public long getJobType() {
        return jobType;
    }

    public void setJobType(long jobType) {
        this.jobType = jobType;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
}
