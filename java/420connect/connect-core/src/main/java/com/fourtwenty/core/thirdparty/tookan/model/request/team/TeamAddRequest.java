package com.fourtwenty.core.thirdparty.tookan.model.request.team;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.validator.constraints.NotEmpty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TeamAddRequest {

    public enum OperationType {
        CREATE,
        UPDATE,
        DELETE
    }

    @NotEmpty
    private String shopId;
    private OperationType operationType;
    private String teamId;
    private String teamName;
    private Long batteryUsage;
    private String tags;

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public OperationType getOperationType() {
        return operationType;
    }

    public void setOperationType(OperationType operationType) {
        this.operationType = operationType;
    }

    public String getTeamId() {
        return teamId;
    }

    public void setTeamId(String teamId) {
        this.teamId = teamId;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public Long getBatteryUsage() {
        return batteryUsage;
    }

    public void setBatteryUsage(Long batteryUsage) {
        this.batteryUsage = batteryUsage;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }
}