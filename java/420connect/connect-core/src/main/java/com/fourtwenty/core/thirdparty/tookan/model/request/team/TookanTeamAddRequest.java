package com.fourtwenty.core.thirdparty.tookan.model.request.team;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fourtwenty.core.thirdparty.tookan.model.request.TookanBaseRequest;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TookanTeamAddRequest extends TookanBaseRequest {
    @JsonProperty("team_name")
    private String teamName;
    @JsonProperty("battery_usage")
    private long batteryUsage;
    @JsonProperty("tags")
    private String tags;

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public long getBatteryUsage() {
        return batteryUsage;
    }

    public void setBatteryUsage(long batteryUsage) {
        this.batteryUsage = batteryUsage;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }
}
