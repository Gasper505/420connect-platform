package com.fourtwenty.core.thirdparty.tookan.model.request.team;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fourtwenty.core.thirdparty.tookan.model.request.TookanBaseRequest;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TookanTeamDeleteRequest extends TookanBaseRequest {
    @JsonProperty("team_id")
    private Long teamId;

    public Long getTeamId() {
        return teamId;
    }

    public void setTeamId(Long teamId) {
        this.teamId = teamId;
    }
}
