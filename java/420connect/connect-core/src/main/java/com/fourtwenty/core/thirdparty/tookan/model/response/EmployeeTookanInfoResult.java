package com.fourtwenty.core.thirdparty.tookan.model.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.tookan.EmployeeTookanInfo;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EmployeeTookanInfoResult extends EmployeeTookanInfo {
    private boolean syncWithTookan;
    private String teamName;

    public boolean isSyncWithTookan() {
        return syncWithTookan;
    }

    public void setSyncWithTookan(boolean syncWithTookan) {
        this.syncWithTookan = syncWithTookan;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }
}
