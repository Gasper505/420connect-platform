package com.fourtwenty.core.thirdparty.tookan.model.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.thirdparty.tookan.model.request.fleets.TookanAgentProfileInfo;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TookanAgentProfileResponse extends TookanBaseResponse {
    private List<TookanAgentProfileInfo> data = new ArrayList<>();

    public List<TookanAgentProfileInfo> getData() {
        return data;
    }

    public void setData(List<TookanAgentProfileInfo> data) {
        this.data = data;
    }
}
