package com.fourtwenty.core.thirdparty.tookan.model.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TookanAgentResponse extends TookanBaseResponse {
    private List<TookanAgentInfo> data = new ArrayList<>();

    public List<TookanAgentInfo> getData() {
        return data;
    }

    public void setData(List<TookanAgentInfo> data) {
        this.data = data;
    }
}
