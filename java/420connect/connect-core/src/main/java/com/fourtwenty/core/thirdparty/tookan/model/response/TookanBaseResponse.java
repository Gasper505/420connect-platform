package com.fourtwenty.core.thirdparty.tookan.model.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TookanBaseResponse {
    public enum TookanErrorCode {
        PARAMETER_MISSING(100),
        INVALID_KEY(101),
        ACTION_COMPLETE(200),
        SHOW_ERROR_MESSAGE(201),
        ERROR_IN_EXECUTION(404),
        BAD_REQUEST(400);

        long code;

        TookanErrorCode(long code) {
            this.code = code;
        }

        public static TookanErrorCode toErrorCode(int code) {
            switch (code) {
                case 100:
                    return PARAMETER_MISSING;
                case 101:
                    return INVALID_KEY;
                case 200:
                    return ACTION_COMPLETE;
                case 201:
                    return SHOW_ERROR_MESSAGE;
                case 404:
                    return ERROR_IN_EXECUTION;
                default:
                    return BAD_REQUEST;
            }
        }
    }

    private String message;
    private int status;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
