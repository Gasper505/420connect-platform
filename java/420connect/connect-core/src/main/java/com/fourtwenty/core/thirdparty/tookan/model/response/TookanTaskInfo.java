package com.fourtwenty.core.thirdparty.tookan.model.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TookanTaskInfo {
    @JsonProperty("job_id")
    private Long jobId;
    @JsonProperty("job_hash")
    private String jobHash;
    @JsonProperty("job_pickup_address")
    private String jobPickupAddress;
    @JsonProperty("job_token")
    private String jobToken;
    @JsonProperty("tracking_link")
    private String trackingLink;
    @JsonProperty("order_id")
    private String orderId;
    @JsonProperty("pickupOrderId")
    private String pickupOrderId;
    @JsonProperty("pickupAddressNotFound")
    private Boolean pickupAddressNotFound;
    @JsonProperty("deliveryAddressNotFound")
    private Boolean deliveryAddressNotFound;
    @JsonProperty("customer_name")
    private String customerName;
    @JsonProperty("customer_address")
    private String customerAddress;
    @JsonProperty("customer_email")
    private String customerEmail;
    @JsonProperty("customer_phone")
    private String customerPhone;
    @JsonProperty("deliveryOrderId")
    private String deliveryOrderId;
    @JsonProperty("job_status")
    private Long jobStatus;
    @JsonProperty("team_id")
    private Long teamId;
    @JsonProperty("fleet_id")
    private Long agentId;
    @JsonProperty("job_time")
    private String jobTime;
    @JsonProperty("job_pickup_email")
    private String jobPickupEmail;
    @JsonProperty("job_pickup_name")
    private String jobPickupName;
    @JsonProperty("job_pickup_phone")
    private String jobPickupPhone;
    @JsonProperty("job_address")
    private String jobAddress;
    @JsonProperty("customer_username")
    private String customerUserName;


    public Long getJobId() {
        return jobId;
    }

    public void setJobId(Long jobId) {
        this.jobId = jobId;
    }

    public String getJobHash() {
        return jobHash;
    }

    public void setJobHash(String jobHash) {
        this.jobHash = jobHash;
    }

    public String getJobPickupAddress() {
        return jobPickupAddress;
    }

    public void setJobPickupAddress(String jobPickupAddress) {
        this.jobPickupAddress = jobPickupAddress;
    }

    public String getJobToken() {
        return jobToken;
    }

    public void setJobToken(String jobToken) {
        this.jobToken = jobToken;
    }

    public String getTrackingLink() {
        return trackingLink;
    }

    public void setTrackingLink(String trackingLink) {
        this.trackingLink = trackingLink;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getPickupOrderId() {
        return pickupOrderId;
    }

    public void setPickupOrderId(String pickupOrderId) {
        this.pickupOrderId = pickupOrderId;
    }

    public Boolean getPickupAddressNotFound() {
        return pickupAddressNotFound;
    }

    public void setPickupAddressNotFound(Boolean pickupAddressNotFound) {
        this.pickupAddressNotFound = pickupAddressNotFound;
    }

    public Boolean getDeliveryAddressNotFound() {
        return deliveryAddressNotFound;
    }

    public void setDeliveryAddressNotFound(Boolean deliveryAddressNotFound) {
        this.deliveryAddressNotFound = deliveryAddressNotFound;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getDeliveryOrderId() {
        return deliveryOrderId;
    }

    public void setDeliveryOrderId(String deliveryOrderId) {
        this.deliveryOrderId = deliveryOrderId;
    }

    public Long getJobStatus() {
        return jobStatus;
    }

    public void setJobStatus(Long jobStatus) {
        this.jobStatus = jobStatus;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public String getCustomerPhone() {
        return customerPhone;
    }

    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    public Long getTeamId() {
        return teamId;
    }

    public void setTeamId(Long teamId) {
        this.teamId = teamId;
    }

    public Long getAgentId() {
        return agentId;
    }

    public void setAgentId(Long agentId) {
        this.agentId = agentId;
    }

    public String getJobTime() {
        return jobTime;
    }

    public void setJobTime(String jobTime) {
        this.jobTime = jobTime;
    }

    public String getJobPickupEmail() {
        return jobPickupEmail;
    }

    public void setJobPickupEmail(String jobPickupEmail) {
        this.jobPickupEmail = jobPickupEmail;
    }

    public String getJobPickupName() {
        return jobPickupName;
    }

    public void setJobPickupName(String jobPickupName) {
        this.jobPickupName = jobPickupName;
    }

    public String getJobPickupPhone() {
        return jobPickupPhone;
    }

    public void setJobPickupPhone(String jobPickupPhone) {
        this.jobPickupPhone = jobPickupPhone;
    }

    public String getJobAddress() {
        return jobAddress;
    }

    public void setJobAddress(String jobAddress) {
        this.jobAddress = jobAddress;
    }

    public String getCustomerUserName() {
        return customerUserName;
    }

    public void setCustomerUserName(String customerUserName) {
        this.customerUserName = customerUserName;
    }
}
