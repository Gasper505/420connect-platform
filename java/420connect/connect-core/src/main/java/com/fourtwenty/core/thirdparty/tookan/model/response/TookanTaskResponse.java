package com.fourtwenty.core.thirdparty.tookan.model.response;


import java.util.ArrayList;
import java.util.List;

public class TookanTaskResponse extends TookanBaseResponse {
    private List<TookanTaskInfo> data = new ArrayList<>();

    public List<TookanTaskInfo> getData() {
        return data;
    }

    public void setData(List<TookanTaskInfo> data) {
        this.data = data;
    }
}
