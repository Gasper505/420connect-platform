package com.fourtwenty.core.thirdparty.tookan.model.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.transaction.Transaction;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TookanTaskResult {
    public enum TookanTaskType {
        PickUp,
        Delivery
    }

    public enum TookanTaskStatus {
        ASSIGNED(0),
        STARTED(1),
        SUCCESSFUL(2),
        FAILED(3),
        INPROGRESS(4),
        UNASSIGNED(6),
        ACCEPTED(7),
        DECLINE(8),
        CANCEL(9),
        DELETED(10);

        public long code;

        TookanTaskStatus(long code) {
            this.code = code;
        }

        public static TookanTaskStatus toTaskStatus(int code) {
            switch (code) {
                case 0:
                    return ASSIGNED;
                case 1:
                    return STARTED;
                case 2:
                    return SUCCESSFUL;
                case 3:
                    return FAILED;
                case 4:
                    return INPROGRESS;
                case 6:
                    return UNASSIGNED;
                case 7:
                    return ACCEPTED;
                case 8:
                    return DECLINE;
                case 9:
                    return CANCEL;
                case 10:
                    return DELETED;
                default:
                    return ASSIGNED;
            }
        }
    }

    private String transactionId;
    private String memberName;
    private Transaction.QueueType queueType;
    private String employeeName;
    private String tookanTaskId;
    private TookanTaskStatus status;
    private String transNo;

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public Transaction.QueueType getQueueType() {
        return queueType;
    }

    public void setQueueType(Transaction.QueueType queueType) {
        this.queueType = queueType;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getTookanTaskId() {
        return tookanTaskId;
    }

    public void setTookanTaskId(String tookanTaskId) {
        this.tookanTaskId = tookanTaskId;
    }

    public TookanTaskStatus getStatus() {
        return status;
    }

    public void setStatus(TookanTaskStatus status) {
        this.status = status;
    }

    public String getTransNo() {
        return transNo;
    }

    public void setTransNo(String transNo) {
        this.transNo = transNo;
    }
}
