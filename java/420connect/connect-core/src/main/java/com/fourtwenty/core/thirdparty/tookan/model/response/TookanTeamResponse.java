package com.fourtwenty.core.thirdparty.tookan.model.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.models.tookan.TookanTeamInfo;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TookanTeamResponse extends TookanBaseResponse {
    private List<TookanTeamInfo> data = new ArrayList<>();

    public List<TookanTeamInfo> getData() {
        return data;
    }

    public void setData(List<TookanTeamInfo> data) {
        this.data = data;
    }
}
