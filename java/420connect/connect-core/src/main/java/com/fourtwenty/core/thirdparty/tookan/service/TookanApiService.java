package com.fourtwenty.core.thirdparty.tookan.service;

import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.thirdparty.TookanErrorLog;
import com.fourtwenty.core.domain.models.tookan.EmployeeTookanInfo;
import com.fourtwenty.core.domain.models.tookan.TookanTeamInfo;
import com.fourtwenty.core.domain.models.tookan.TookanTeams;
import com.fourtwenty.core.thirdparty.tookan.model.request.fleets.AgentAddRequest;
import com.fourtwenty.core.thirdparty.tookan.model.request.fleets.AgentDeleteRequest;
import com.fourtwenty.core.thirdparty.tookan.model.request.fleets.AgentEditRequest;
import com.fourtwenty.core.thirdparty.tookan.model.request.task.DeleteTaskRequest;
import com.fourtwenty.core.thirdparty.tookan.model.request.task.TookanTaskStatusRequest;
import com.fourtwenty.core.thirdparty.tookan.model.request.team.TookanTeamAddRequest;
import com.fourtwenty.core.thirdparty.tookan.model.request.team.TookanTeamDeleteRequest;
import com.fourtwenty.core.thirdparty.tookan.model.request.team.TookanTeamUpdateRequest;
import com.fourtwenty.core.thirdparty.tookan.model.response.TookanAgentInfo;
import com.fourtwenty.core.thirdparty.tookan.model.response.TookanTaskInfo;

import java.util.List;

public interface TookanApiService {
    TookanTeamInfo addTookanTeam(String companyId, String shopId, String apiKey, TookanTeamAddRequest request);

    TookanTeamInfo updateTookanTeam(String companyId, String shopId, String apiKey, TookanTeamUpdateRequest request, TookanTeamInfo teamInfo);

    TookanTeamInfo deleteTookanTeam(String companyId, String shopId, String apiKey, TookanTeamDeleteRequest request, TookanTeamInfo teamInfo);

    void synchronizeTeams(String companyId, String shopId, String apiKey, TookanTeams tokenTeams);

    TookanAgentInfo synchronizeAgents(String companyId, String shopId, String country, String apiKey, AgentEditRequest request);

    TookanAgentInfo getAgentById(String companyId, String shopId, String apiKey, AgentDeleteRequest fleetId);

    boolean removeAgent(String companyId, String shopId, String apiKey, AgentDeleteRequest request);

    TookanTaskInfo createTask(String companyId, String shopId, String apiKey, boolean isPickUp, String request);

    boolean deleteTookanTask(String companyId, String shopId, String apiKey, String taskId);

    TookanTeamInfo getTookanTeamInfo(String companyId, String shopId, TookanTeams tookanTeams, String teamId);

    EmployeeTookanInfo getEmployeeTookanInfo(String companyId, Employee employee, String shopId);

    TookanAgentInfo addTookanAgent(String companyId, String shopId, String apiKey, AgentAddRequest request);

    TookanAgentInfo updateTookanAgent(String companyId, String shopId, String apiKey, AgentEditRequest request);

    List<TookanTaskInfo> getAllTookanTask(String companyId, String shopId, String apiKey, String requestBody);

    void addTookanLog(String companyId, String shopId, TookanErrorLog.ErrorType errorType, TookanErrorLog.SubErrorType subErrorType,
                      String tookanMessage, int status, String apiKey, String message, String sourceId);

    boolean authenticateApiKey(String companyId, String shopId, String apiKey);

    boolean updateTaskStatus(String companyId, String shopId, String apiKey, TookanTaskStatusRequest request);

    TookanTaskInfo getTaskDetails(String companyId, String shopId, String apiKey, DeleteTaskRequest request);

    boolean updateTaskDetails(String companyId, String shopId, String apiKey, String requestBody);

    TookanTeamInfo getTookanTeamInfo(String companyId, String shopId, TookanTeams tookanTeams, String teamId, boolean isCheck);

}
