package com.fourtwenty.core.thirdparty.tookan.service;

import com.fourtwenty.core.domain.models.thirdparty.TookanErrorLog;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;

public interface TookanErrorLogService {
    SearchResult<TookanErrorLog> getTookanErrorLog(String shopId, int start, int limit);
}
