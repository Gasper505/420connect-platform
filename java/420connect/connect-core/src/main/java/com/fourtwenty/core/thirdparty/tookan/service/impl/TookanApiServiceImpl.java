package com.fourtwenty.core.thirdparty.tookan.service.impl;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.thirdparty.TookanErrorLog;
import com.fourtwenty.core.domain.models.tookan.EmployeeTookanInfo;
import com.fourtwenty.core.domain.models.tookan.TookanTeamInfo;
import com.fourtwenty.core.domain.models.tookan.TookanTeams;
import com.fourtwenty.core.domain.repositories.dispensary.CompanyRepository;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.domain.repositories.tookan.TookanErrorLogRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.managed.AmazonServiceManager;
import com.fourtwenty.core.thirdparty.tookan.model.request.TookanBaseRequest;
import com.fourtwenty.core.thirdparty.tookan.model.request.fleets.*;
import com.fourtwenty.core.thirdparty.tookan.model.request.task.DeleteTaskRequest;
import com.fourtwenty.core.thirdparty.tookan.model.request.task.TookanTaskStatusRequest;
import com.fourtwenty.core.thirdparty.tookan.model.request.team.TookanTeamAddRequest;
import com.fourtwenty.core.thirdparty.tookan.model.request.team.TookanTeamDeleteRequest;
import com.fourtwenty.core.thirdparty.tookan.model.request.team.TookanTeamUpdateRequest;
import com.fourtwenty.core.thirdparty.tookan.model.response.*;
import com.fourtwenty.core.thirdparty.tookan.service.TookanApiService;
import com.fourtwenty.core.util.JsonSerializer;
import com.fourtwenty.core.util.SSLUtilities;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.mdo.pusher.SimpleRestUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TookanApiServiceImpl implements TookanApiService {

    private static final Log LOG = LogFactory.getLog(TookanApiServiceImpl.class);


    private static final String CREATE_TEAM = "/create_team";
    private static final String UPDATE_TEAM = "/update_team";
    private static final String DELETE_TEAM = "/delete_team";
    private static final String VIEW_TEAMS = "/view_teams";

    private static final String GET_AGENT = "/get_available_agents";
    private static final String CREATE_AGENT = "/add_agent";
    private static final String EDIT_AGENT = "/edit_agent";
    private static final String VIEW_AGENT = "/view_fleet_profile";
    private static final String DELETE_AGENT = "/delete_fleet_account";

    private static final String CREATE_TASK = "/create_task";
    private static final String DELETE_TASK = "/delete_task";
    private static final String GET_ALL_TASK = "/get_all_tasks";
    private static final String UPDATE_TASK_STATUS = "/update_task_status";
    private static final String GET_TASK = "/get_task_details";
    private static final String UPDATE_TASK = "/edit_task";

    private static final String API_KEY_INVALID = "API key is invalid";
    private static final String API_KEY = "API key";

    @Inject
    private TookanErrorLogRepository tookanErrorLogRepository;
    @Inject
    private ShopRepository shopRepository;
    @Inject
    private CompanyRepository companyRepository;
    @Inject
    private ConnectConfiguration connectConfiguration;


    /**
     * Private method to create url for tookan api
     *
     * @param apiKey  : api key
     * @param apiPath : api operation
     * @return : return tookan api url
     */
    private String createURL(String apiKey, String apiPath) {
        if (StringUtils.isBlank(apiKey)) {
            throw new BlazeInvalidArgException(API_KEY, API_KEY_INVALID);
        }

        StringBuilder apiURl = new StringBuilder(connectConfiguration.getTookanConfiguration().getUrl());
        apiURl.append(apiPath);

        return String.valueOf(apiURl);
    }

    /**
     * Override method to add tookan team
     *
     * @param apiKey  : api key for tookan
     * @param request : add team request
     * @return tookan team info
     */
    @Override
    public TookanTeamInfo addTookanTeam(String companyId, String shopId, String apiKey, TookanTeamAddRequest request) {
        String requestBody = JsonSerializer.toJson(request);

        TookanTeamResponse response = new TookanTeamResponse();

        response = callTookanTeamApi(companyId, shopId, apiKey, CREATE_TEAM, requestBody, response, TookanTeamResponse.class);

        TookanTeamInfo teamInfo = null;

        if (TookanBaseResponse.TookanErrorCode.toErrorCode(response.getStatus()) == TookanBaseResponse.TookanErrorCode.ACTION_COMPLETE && response.getData() != null && !response.getData().isEmpty()) {
            List<TookanTeamInfo> responseTeamInfo = response.getData();
            teamInfo = new TookanTeamInfo();
            teamInfo.prepare();
            teamInfo.setBatteryUsage(TookanTeamInfo.BatteryUsage.toBatteryUsage(request.getBatteryUsage()));
            teamInfo.setTags(request.getTags());
            teamInfo.setTeamName(request.getTeamName());
            teamInfo.setTeamId(responseTeamInfo.get(0).getTeamId());

        } else {
            LOG.info("Error in tookan api calling: Api Request: Create Team  Error:" + TookanBaseResponse.TookanErrorCode.toErrorCode(response.getStatus()) + " Message: " + response.getMessage());
            addTookanLog(companyId, shopId, TookanErrorLog.ErrorType.TEAM, TookanErrorLog.SubErrorType.Create_Team,
                    response.getMessage(), response.getStatus(),
                    apiKey, "Error in tookan api calling: Api Request: Create Team  Error:"
                            + TookanBaseResponse.TookanErrorCode.toErrorCode(response.getStatus())
                            + " Message: " + response.getMessage(), null);
        }
        return teamInfo;
    }

    /**
     * Override method to update tookan team.
     *
     * @param apiKey   : api key for tookan
     * @param request  : team update request
     * @param teamInfo : team info
     * @return : updated tookan info
     */
    @Override
    public TookanTeamInfo updateTookanTeam(String companyId, String shopId, String apiKey, TookanTeamUpdateRequest request, TookanTeamInfo teamInfo) {
        String requestBody = JsonSerializer.toJson(request);

        TookanTeamResponse response = new TookanTeamResponse();

        response = callTookanTeamApi(companyId, shopId, apiKey, UPDATE_TEAM, requestBody, response, TookanTeamResponse.class);

        if (TookanBaseResponse.TookanErrorCode.toErrorCode(response.getStatus()) == TookanBaseResponse.TookanErrorCode.ACTION_COMPLETE && response.getData() != null && !response.getData().isEmpty()) {
            List<TookanFleetInfo> fleetInfos = response.getData().get(0).getFleets();
            teamInfo.setBatteryUsage(TookanTeamInfo.BatteryUsage.toBatteryUsage(request.getBatteryUsage()));
            teamInfo.setTags(request.getTags());
            teamInfo.setTeamName(request.getTeamName());
            teamInfo.setFleets(fleetInfos);
        } else {
            LOG.info("Error in tookan api calling: Api Request: Update Team  Error:" + TookanBaseResponse.TookanErrorCode.toErrorCode(response.getStatus()) + " Message: " + response.getMessage());
            addTookanLog(companyId, shopId, TookanErrorLog.ErrorType.TEAM, TookanErrorLog.SubErrorType.Update_Team,
                    response.getMessage(), response.getStatus(),
                    apiKey, "Error in tookan api calling: Api Request: Update Team  Error:" + TookanBaseResponse.TookanErrorCode.toErrorCode(response.getStatus()) + " Message: " + response.getMessage(), request.getTeamId().toString());
        }

        return teamInfo;
    }

    /**
     * Override method to delete tookan team
     *
     * @param apiKey   : api key for tookan
     * @param request  : team delete request
     * @param teamInfo : team info
     * @return : deleted tookan info
     */
    @Override
    public TookanTeamInfo deleteTookanTeam(String companyId, String shopId, String apiKey, TookanTeamDeleteRequest request, TookanTeamInfo teamInfo) {
        String requestBody = JsonSerializer.toJson(request);

        TookanTeamResponse response = new TookanTeamResponse();

        response = callTookanTeamApi(companyId, shopId, apiKey, DELETE_TEAM, requestBody, response, TookanTeamResponse.class);

        if (TookanBaseResponse.TookanErrorCode.toErrorCode(response.getStatus()) == TookanBaseResponse.TookanErrorCode.ACTION_COMPLETE && response.getData() != null && !response.getData().isEmpty()) {
            teamInfo.setDeleted(true);
        } else {
            LOG.info("Error in tookan api calling: Api Request: Delete Team  Error:" + TookanBaseResponse.TookanErrorCode.toErrorCode(response.getStatus()) + " Message: " + response.getMessage());
            addTookanLog(companyId, shopId, TookanErrorLog.ErrorType.TEAM, TookanErrorLog.SubErrorType.Delete_Team,
                    response.getMessage(), response.getStatus(),
                    apiKey, "Error in tookan api calling: Api Request: Delete Team  Error:" + TookanBaseResponse.TookanErrorCode.toErrorCode(response.getStatus()) + " Message: " + response.getMessage(), request.getTeamId().toString());
        }

        return teamInfo;
    }

    /**
     * Override method to synchronize tookan teams
     *
     * @param apiKey     : api key for tookan
     * @param tokenTeams : tookan team list
     */
    @Override
    public void synchronizeTeams(String companyId, String shopId, String apiKey, TookanTeams tokenTeams) {
        TookanBaseRequest baseRequest = new TookanBaseRequest();
        baseRequest.setApiKey(apiKey);
        String requestBody = JsonSerializer.toJson(baseRequest);

        TookanTeamResponse response = new TookanTeamResponse();

        response = callTookanTeamApi(companyId, shopId, apiKey, VIEW_TEAMS, requestBody, response, TookanTeamResponse.class);

        if (TookanBaseResponse.TookanErrorCode.toErrorCode(response.getStatus()) == TookanBaseResponse.TookanErrorCode.ACTION_COMPLETE && response.getData() != null && !response.getData().isEmpty()) {

            HashMap<Long, TookanTeamInfo> dbTeamInfoMap = new HashMap<>();
            HashMap<Long, TookanTeamInfo> tookanTeamInfoMap = new HashMap<>();

            for (TookanTeamInfo teamInfo : tokenTeams.getTookanTeamInfo()) {
                dbTeamInfoMap.put(teamInfo.getTeamId(), teamInfo);
            }

            List<TookanTeamInfo> tookanTeamInfos = response.getData();

            for (TookanTeamInfo teamInfo : tookanTeamInfos) {
                tookanTeamInfoMap.put(teamInfo.getTeamId(), teamInfo);
                if (!dbTeamInfoMap.containsKey(teamInfo.getTeamId())) {
                    //If team is new add new team in db
                    teamInfo.prepare();
                    tokenTeams.getTookanTeamInfo().add(teamInfo);
                } else {
                    //If exists at both places then update
                    TookanTeamInfo mapTeamInfo = dbTeamInfoMap.get(teamInfo.getTeamId());
                    mapTeamInfo.setBatteryUsage(teamInfo.getBatteryUsage());
                    mapTeamInfo.setTags(teamInfo.getTags());
                    mapTeamInfo.setTeamName(teamInfo.getTeamName());
                    mapTeamInfo.setFleets(teamInfo.getFleets());
                }
            }

            List<TookanTeamInfo> tookanTeamsCopy = Lists.newArrayList(tokenTeams.getTookanTeamInfo());

            for (TookanTeamInfo teamInfo : tokenTeams.getTookanTeamInfo()) {
                if (!tookanTeamInfoMap.containsKey(teamInfo.getTeamId())) {
                    tookanTeamsCopy.remove(teamInfo);
                }
            }

            tokenTeams.setTookanTeamInfo(tookanTeamsCopy);
        } else {
            LOG.info("Error in tookan api calling: Api Request: Synchronize Team  Error:" + TookanBaseResponse.TookanErrorCode.toErrorCode(response.getStatus()) + " Message: " + response.getMessage());
            addTookanLog(companyId, shopId, TookanErrorLog.ErrorType.TEAM, TookanErrorLog.SubErrorType.Synchronize_Team,
                    response.getMessage(), response.getStatus(),
                    apiKey, "Error in tookan api calling: Api Request: Synchronize Team  Error:" + TookanBaseResponse.TookanErrorCode.toErrorCode(response.getStatus()) + " Message: " + response.getMessage(), null);
        }

    }

    /**
     * Private method to call tookan api
     *
     * @param apiKey       : api key
     * @param apiOperation : api operation for url
     * @param requestBody  : request body
     * @return : return tookan response
     */
    private <T extends TookanBaseResponse> T callTookanTeamApi(String companyId, String shopId, String apiKey, String apiOperation, String requestBody, T responseObject, Class<T> clazz) {
        String url = createURL(apiKey, apiOperation);

        SSLUtilities.trustAllHostnames();
        SSLUtilities.trustAllHttpsCertificates();

        MultivaluedMap<String, Object> headers = new MultivaluedHashMap<>();
        headers.putSingle("Content-Type", MediaType.APPLICATION_JSON);

        LOG.info("Calling tookan api for :" + apiOperation);
        LOG.info("Tookan URL:" + url);

        try {
            String response = "";
            if (checkSslURL(url)) {
                response = SimpleRestUtil.postWithSsl(url, requestBody, String.class, headers);
            } else {
                response = SimpleRestUtil.post(url, requestBody, String.class, headers);
            }

            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
            responseObject = objectMapper.readValue(response, clazz);

        } catch (Exception e) {
            responseObject.setStatus(400);
            responseObject.setMessage("Exception in api calling");
            LOG.error(e.getMessage(), e);
        }

        LOG.info("Api call completed for :" + apiOperation);
        return responseObject;
    }

    /**
     * Override method to synchronize agents with tookan and if not found create one.
     *
     * @param apiKey   : api key
     * @param request: agent sync request
     * @return : tookan agent info
     */
    @Override
    public TookanAgentInfo synchronizeAgents(String companyId, String shopId, String country, String apiKey, AgentEditRequest request) {
        TookanBaseRequest baseRequest = new TookanBaseRequest();
        baseRequest.setApiKey(apiKey);
        String requestBody = JsonSerializer.toJson(baseRequest);

        TookanAgentResponse response = new TookanAgentResponse();

        response = callTookanTeamApi(companyId, shopId, apiKey, GET_AGENT, requestBody, response, TookanAgentResponse.class);
        TookanAgentInfo returnInfo = null;

        if (TookanBaseResponse.TookanErrorCode.toErrorCode(response.getStatus()) == TookanBaseResponse.TookanErrorCode.ACTION_COMPLETE
                && response.getData() != null && !response.getData().isEmpty()) {

            TookanAgentInfo tookanAgentInfo = null;
            String cpn = request.getPhone();
            if (StringUtils.isNotBlank(cpn)) {
                cpn = AmazonServiceManager.cleanPhoneNumberCountry(cpn,country);
            }

            for (TookanAgentInfo agentInfo : response.getData()) {
                if (agentInfo.getUsername().equalsIgnoreCase(request.getUsername())) {
                    tookanAgentInfo = agentInfo;
                    break;
                }
                if (agentInfo.getEmail().equalsIgnoreCase(request.getEmail())) {
                    tookanAgentInfo = agentInfo;
                    break;
                }

                if (StringUtils.isNotBlank(cpn) && StringUtils.isNotBlank(agentInfo.getPhone())) {
                    String agentCPN = AmazonServiceManager.cleanPhoneNumberCountry(agentInfo.getPhone(),country);
                    if (cpn.equalsIgnoreCase(agentCPN)) {
                        tookanAgentInfo = agentInfo;
                        break;
                    }
                }
            }

            if (tookanAgentInfo != null && !request.getTeamId().equals(tookanAgentInfo.getTeamId().toString())) {
                request.setFleetId(tookanAgentInfo.getFleetId().toString());
                request.setTeamId(request.getTeamId());
                returnInfo = updateTookanAgent(companyId, shopId, apiKey, request);

            } else {
                returnInfo = tookanAgentInfo;
            }
        } else {
            LOG.info("Error in tookan api calling: Api Request: Synchronize Agent  Error:" + TookanBaseResponse.TookanErrorCode.toErrorCode(response.getStatus()) + " Message: " + response.getMessage());
            addTookanLog(companyId, shopId, TookanErrorLog.ErrorType.AGENT, TookanErrorLog.SubErrorType.Synchronize_Agent,
                    response.getMessage(), response.getStatus(),
                    apiKey, "Error in tookan api calling: Api Request: Synchronize Agent  Error:" + TookanBaseResponse.TookanErrorCode.toErrorCode(response.getStatus()) + " Message: " + response.getMessage(), null);
        }
        return returnInfo;
    }

    /**
     * Override method to create tookan agent
     *
     * @param apiKey  : api key
     * @param request : agent create request.
     * @return : tookan agent info
     */
    @Override
    public TookanAgentInfo addTookanAgent(String companyId, String shopId, String apiKey, AgentAddRequest request) {
        String requestBody = JsonSerializer.toJson(request);
        TookanAgentResponse response = new TookanAgentResponse();
        response = callTookanTeamApi(companyId, shopId, apiKey, CREATE_AGENT, requestBody, response, TookanAgentResponse.class);
        TookanAgentInfo agentInfo;
        if (TookanBaseResponse.TookanErrorCode.toErrorCode(response.getStatus()) == TookanBaseResponse.TookanErrorCode.ACTION_COMPLETE && response.getData() != null && !response.getData().isEmpty()) {
            agentInfo = response.getData().get(0);
        } else {
            LOG.info("Error in tookan api calling: Api Request: Add Agent Error:" + TookanBaseResponse.TookanErrorCode.toErrorCode(response.getStatus()) + " Message: " + response.getMessage());
            addTookanLog(companyId, shopId, TookanErrorLog.ErrorType.AGENT, TookanErrorLog.SubErrorType.Create_Agent,
                    response.getMessage(), response.getStatus(),
                    apiKey, "Error in tookan api calling: Api Request: Add Agent Error:" + TookanBaseResponse.TookanErrorCode.toErrorCode(response.getStatus()) + " Message: " + response.getMessage(), null);
            throw new BlazeInvalidArgException("Tookan Agent", response.getMessage());

        }
        return agentInfo;
    }

    /**
     * Override method to update tookan agent
     *
     * @param apiKey  : api key
     * @param request : agent create request.
     * @return : tookan agent info
     */
    @Override
    public TookanAgentInfo updateTookanAgent(String companyId, String shopId, String apiKey, AgentEditRequest request) {

        String requestBody = JsonSerializer.toJson(request);
        TookanAgentResponse response = new TookanAgentResponse();
        response = callTookanTeamApi(companyId, shopId, apiKey, EDIT_AGENT, requestBody, response, TookanAgentResponse.class);
        TookanAgentInfo agentInfo = null;
        if (TookanBaseResponse.TookanErrorCode.toErrorCode(response.getStatus()) == TookanBaseResponse.TookanErrorCode.ACTION_COMPLETE && response.getData() != null && !response.getData().isEmpty()) {
            AgentDeleteRequest getAgentRequest = new AgentDeleteRequest();
            getAgentRequest.setFleetId(request.getFleetId());
            getAgentRequest.setApiKey(apiKey);
            agentInfo = getAgentById(companyId, shopId, apiKey, getAgentRequest);
            if (agentInfo != null && agentInfo.getTeamId() == null) {
                agentInfo.setTeamId(new Long(request.getTeamId()));
            }
        } else {
            LOG.info("Error in tookan api calling: Api Request: Update Agent Error:" + TookanBaseResponse.TookanErrorCode.toErrorCode(response.getStatus()) + " Message: " + response.getMessage());
            addTookanLog(companyId, shopId, TookanErrorLog.ErrorType.AGENT, TookanErrorLog.SubErrorType.Update_Agent,
                    response.getMessage(), response.getStatus(),
                    apiKey, "Error in tookan api calling: Api Request: Update Agent Error:" + TookanBaseResponse.TookanErrorCode.toErrorCode(response.getStatus()) + " Message: " + response.getMessage(), request.getTeamId());

        }
        return agentInfo;
    }

    /**
     * Override method to get agent detail by fleet(agent) id.
     *
     * @param apiKey  : api key
     * @param request : agent get request
     * @return : agent info by id
     */

    @Override
    public TookanAgentInfo getAgentById(String companyId, String shopId, String apiKey, AgentDeleteRequest request) {
        String requestBody = JsonSerializer.toJson(request);
        TookanAgentProfileResponse response = new TookanAgentProfileResponse();
        response = callTookanTeamApi(companyId, shopId, apiKey, VIEW_AGENT, requestBody, response, TookanAgentProfileResponse.class);
        TookanAgentInfo agentInfo = null;
        if (TookanBaseResponse.TookanErrorCode.toErrorCode(response.getStatus()) == TookanBaseResponse.TookanErrorCode.ACTION_COMPLETE
                && response.getData() != null && !response.getData().isEmpty()) {
            TookanAgentProfileInfo profileInfo = response.getData().get(0);
            if (profileInfo != null && profileInfo.getFleetDetails() != null && !profileInfo.getFleetDetails().isEmpty()) {
                agentInfo = profileInfo.getFleetDetails().get(0);
            }
        } else {
            LOG.info("Error in tookan api calling Api Request : View Agent Error:" + TookanBaseResponse.TookanErrorCode.toErrorCode(response.getStatus()) + " Message:" + response.getMessage());
            addTookanLog(companyId, shopId, TookanErrorLog.ErrorType.AGENT, TookanErrorLog.SubErrorType.GET_Agent_Profile,
                    response.getMessage(), response.getStatus(),
                    apiKey, "Error in tookan api calling Api Request : View Agent Error:" + TookanBaseResponse.TookanErrorCode.toErrorCode(response.getStatus()) + " Message:" + response.getMessage(), request.getFleetId());

        }
        return agentInfo;

    }

    /**
     * Override method to remove agent from tookan.
     *
     * @param apiKey  : api key
     * @param request : delete agent request.
     * @return boolean response
     */
    @Override
    public boolean removeAgent(String companyId, String shopId, String apiKey, AgentDeleteRequest request) {
        String requestBody = JsonSerializer.toJson(request);
        TookanAgentResponse agentResponse = new TookanAgentResponse();
        agentResponse = callTookanTeamApi(companyId, shopId, apiKey, DELETE_AGENT, requestBody, agentResponse, TookanAgentResponse.class);
        if (TookanBaseResponse.TookanErrorCode.toErrorCode(agentResponse.getStatus()) == TookanBaseResponse.TookanErrorCode.ACTION_COMPLETE) {
            return true;
        } else {
            LOG.info("Error in tookan api calling Api Request : Delete Agent Error:" + TookanBaseResponse.TookanErrorCode.toErrorCode(agentResponse.getStatus()) + " Message:" + agentResponse.getMessage());
            addTookanLog(companyId, shopId, TookanErrorLog.ErrorType.TEAM, TookanErrorLog.SubErrorType.Delete_Agent,
                    agentResponse.getMessage(), agentResponse.getStatus(),
                    apiKey, "Error in tookan api calling Api Request : Delete Agent Error:" + TookanBaseResponse.TookanErrorCode.toErrorCode(agentResponse.getStatus()) + " Message:" + agentResponse.getMessage(), request.getFleetId());

        }
        return false;
    }

    /**
     * Overrride method to delete tookan task
     *
     * @param apiKey   : api key
     * @param isPickUp : true if pick up task or false if delivery
     * @param request  : task request body
     * @return : tookan task info
     */

    @Override
    public TookanTaskInfo createTask(String companyId, String shopId, String apiKey, boolean isPickUp, String request) {
        TookanTaskResponse response = new TookanTaskResponse();
        TookanTaskInfo taskInfo = null;
        response = callTookanTeamApi(companyId, shopId, apiKey, CREATE_TASK, request, response, TookanTaskResponse.class);
        if (TookanBaseResponse.TookanErrorCode.toErrorCode(response.getStatus()) == TookanBaseResponse.TookanErrorCode.ACTION_COMPLETE
                && response.getData() != null && !response.getData().isEmpty()) {
            taskInfo = response.getData().get(0);
        } else {
            TookanErrorLog.SubErrorType subSource = isPickUp ? TookanErrorLog.SubErrorType.PickUp_Task : TookanErrorLog.SubErrorType.Delivery_Task;
            LOG.info("Error in tookan api calling Api Request : Create Task Error:" + TookanBaseResponse.TookanErrorCode.toErrorCode(response.getStatus()) + " Message:" + response.getMessage());
            addTookanLog(companyId, shopId, TookanErrorLog.ErrorType.TASK, subSource,
                    response.getMessage(), response.getStatus(),
                    apiKey, "Error in tookan api calling Api Request : Create Task Error:" + TookanBaseResponse.TookanErrorCode.toErrorCode(response.getStatus()) + " Message:" + response.getMessage()
                    , null);


        }
        return taskInfo;
    }

    /**
     * Override method to delete tookan task
     *
     * @param apiKey : api key
     * @param taskId : task id
     * @return : true if deleted else false
     */
    @Override
    public boolean deleteTookanTask(String companyId, String shopId, String apiKey, String taskId) {
        DeleteTaskRequest deleteTaskRequest = new DeleteTaskRequest();
        deleteTaskRequest.setApiKey(apiKey);
        deleteTaskRequest.setJobId(taskId);

        String requestBody = JsonSerializer.toJson(deleteTaskRequest);

        TookanTaskResponse response = new TookanTaskResponse();
        response = callTookanTeamApi(companyId, shopId, apiKey, DELETE_TASK, requestBody, response, TookanTaskResponse.class);

        if (TookanBaseResponse.TookanErrorCode.toErrorCode(response.getStatus()) == TookanBaseResponse.TookanErrorCode.ACTION_COMPLETE) {
            return true;
        } else {
            LOG.info("Error in tookan api calling Api Request : Delete Task Error:" + TookanBaseResponse.TookanErrorCode.toErrorCode(response.getStatus()) + " Message:" + response.getMessage());
            addTookanLog(companyId, shopId, TookanErrorLog.ErrorType.TASK, TookanErrorLog.SubErrorType.Delete_Task,
                    response.getMessage(), response.getStatus(),
                    apiKey, "Error in tookan api calling Api Request : Delete Task Error:" + TookanBaseResponse.TookanErrorCode.toErrorCode(response.getStatus()) + " Message:" + response.getMessage(), deleteTaskRequest.getJobId());


        }
        return false;
    }

    /**
     * Override method to get tookan info
     *
     * @param tookanTeams : shop's tookan team info
     * @param teamId      : team id
     * @return tookan team info
     */
    @Override
    public TookanTeamInfo getTookanTeamInfo(String companyId, String shopId, TookanTeams tookanTeams, String teamId) {
        return getTookanTeamInfo(companyId, shopId, tookanTeams, teamId, false);
    }

    /**
     * Override method to get employee fleet/agent info
     *
     * @param employee : Employee
     * @param shopId   : shop id
     * @return employee tookan info.
     */
    @Override
    public EmployeeTookanInfo getEmployeeTookanInfo(String companyId, Employee employee, String shopId) {
        if (employee.getTookanInfoList() != null && !employee.getTookanInfoList().isEmpty()) {
            for (EmployeeTookanInfo tookanInfo : employee.getTookanInfoList()) {
                if (tookanInfo.getShopId().equals(shopId)) {
                    return tookanInfo;
                }
            }
        }
        return null;
    }

    /**
     * Override method to get list of all tookan task
     *
     * @param apiKey      : api key
     * @param requestBody : rquest body
     * @return : list of all tookan tasks
     */
    @Override
    public List<TookanTaskInfo> getAllTookanTask(String companyId, String shopId, String apiKey, String requestBody) {
        TookanTaskResponse response = new TookanTaskResponse();
        response = callTookanTeamApi(companyId, shopId, apiKey, GET_ALL_TASK, requestBody, response, TookanTaskResponse.class);

        List<TookanTaskInfo> taskInfos = new ArrayList<>();

        if (TookanBaseResponse.TookanErrorCode.toErrorCode(response.getStatus()) == TookanBaseResponse.TookanErrorCode.ACTION_COMPLETE
                && response.getData() != null
                && !response.getData().isEmpty()) {
            taskInfos = response.getData();
        } else {
            LOG.info("Error in tookan api calling Api Request : Get All Tasks Error:" + TookanBaseResponse.TookanErrorCode.toErrorCode(response.getStatus()) + " Message:" + response.getMessage());
            addTookanLog(companyId, shopId, TookanErrorLog.ErrorType.TASK, TookanErrorLog.SubErrorType.Get_All_Task,
                    response.getMessage(), response.getStatus(),
                    apiKey,
                    "Error in tookan api calling Api Request : Get All Tasks Error:"
                            + TookanBaseResponse.TookanErrorCode.toErrorCode(response.getStatus())
                            + " Message:" + response.getMessage(), null);


        }
        return taskInfos;
    }

    @Override
    public void addTookanLog(String companyId, String shopId, TookanErrorLog.ErrorType errorType, TookanErrorLog.SubErrorType subErrorType,
                             String tookanMessage, int status, String apiKey, String message, String sourceId) {

        Shop shop = shopRepository.getById(shopId);
        Company company = companyRepository.getById(companyId);

        if (TookanBaseResponse.TookanErrorCode.toErrorCode(status) == TookanBaseResponse.TookanErrorCode.ACTION_COMPLETE) {
            message = "API call is successful but data not found for company:" + ((company != null) ? company.getName() : companyId) + " and shop:" + (shop != null ? shop.getName() : shopId);
        }

        TookanErrorLog errorLog = new TookanErrorLog();
        errorLog.prepare(companyId);
        errorLog.setShopId(shopId);
        errorLog.setErrorCause(errorType);
        errorLog.setSubErrorCause(subErrorType);
        errorLog.setTookanMessage(tookanMessage);
        errorLog.setMessage(message);
        errorLog.setStatus(TookanBaseResponse.TookanErrorCode.toErrorCode(status));
        errorLog.setStatusCode(status);
        errorLog.setSourceId(sourceId);
        errorLog.setApiKey(apiKey);

        // saveTookanLog(errorLog);
        tookanErrorLogRepository.save(errorLog);
    }

    /**
     * Override method to verify if apikey is valid or not.
     * There is no api at tookan to verify api key thats
     * why get agent detail api is called to check response
     * if status is 101 then apikey is invalid.
     *
     * @param companyId : company id
     * @param shopId    : shop id
     * @param apiKey    : api key
     * @return : if valid true else false
     */

    @Override
    public boolean authenticateApiKey(String companyId, String shopId, String apiKey) {
        TookanBaseRequest baseRequest = new TookanBaseRequest();
        baseRequest.setApiKey(apiKey);
        String requestBody = JsonSerializer.toJson(baseRequest);

        TookanAgentResponse response = new TookanAgentResponse();

        response = callTookanTeamApi(companyId, shopId, apiKey, GET_AGENT, requestBody, response, TookanAgentResponse.class);

        LOG.info("Response for authentication api key is :" + response.getStatus() + " " + response.getMessage() + ", apikey:" + apiKey);

        return TookanBaseResponse.TookanErrorCode.toErrorCode(response.getStatus()) == TookanBaseResponse.TookanErrorCode.ACTION_COMPLETE;
    }

    /**
     * Override method to update tookan task status
     *
     * @param companyId : company id
     * @param shopId    : shop id
     * @param apiKey    : api key
     * @param request   : request
     * @return : true if updated else false
     */
    @Override
    public boolean updateTaskStatus(String companyId, String shopId, String apiKey, TookanTaskStatusRequest request) {
        String requestBody = JsonSerializer.toJson(request);

        TookanTaskResponse response = new TookanTaskResponse();

        response = callTookanTeamApi(companyId, shopId, apiKey, UPDATE_TASK_STATUS, requestBody, response, TookanTaskResponse.class);

        if (TookanBaseResponse.TookanErrorCode.toErrorCode(response.getStatus()) == TookanBaseResponse.TookanErrorCode.ACTION_COMPLETE) {
            return true;
        } else {
            LOG.info("Error in tookan api calling Api Request : Update task status Error:" + TookanBaseResponse.TookanErrorCode.toErrorCode(response.getStatus()) + " Message:" + response.getMessage());
            addTookanLog(companyId, shopId, TookanErrorLog.ErrorType.TASK, TookanErrorLog.SubErrorType.Update_Task_Status,
                    response.getMessage(), response.getStatus(),
                    apiKey,
                    "Error in tookan api calling Api Request : Update Task Status Error:"
                            + TookanBaseResponse.TookanErrorCode.toErrorCode(response.getStatus())
                            + " Message:" + response.getMessage(), null);

        }

        return false;
    }

    /**
     * Override method to get task details
     *
     * @param companyId : company id
     * @param shopId    : shop id
     * @param apiKey    : apikey
     * @param request   : request
     * @return : task info
     */
    @Override
    public TookanTaskInfo getTaskDetails(String companyId, String shopId, String apiKey, DeleteTaskRequest request) {
        String requestBody = JsonSerializer.toJson(request);

        TookanTaskResponse response = new TookanTaskResponse();
        TookanTaskInfo taskInfo = null;
        response = callTookanTeamApi(companyId, shopId, apiKey, GET_TASK, requestBody, response, TookanTaskResponse.class);

        if (TookanBaseResponse.TookanErrorCode.toErrorCode(response.getStatus()) == TookanBaseResponse.TookanErrorCode.ACTION_COMPLETE
                && response.getData() != null && !response.getData().isEmpty()) {
            taskInfo = response.getData().get(0);
        } else {
            LOG.info("Error in tookan api calling Api Request : Get task Error:" + TookanBaseResponse.TookanErrorCode.toErrorCode(response.getStatus()) + " Message:" + response.getMessage());
            addTookanLog(companyId, shopId, TookanErrorLog.ErrorType.TASK, TookanErrorLog.SubErrorType.Get_Task,
                    response.getMessage(), response.getStatus(),
                    apiKey,
                    "Error in tookan api calling Api Request : Get Task  Error:"
                            + TookanBaseResponse.TookanErrorCode.toErrorCode(response.getStatus())
                            + " Message:" + response.getMessage(), null);

        }

        return taskInfo;
    }

    /**
     * Override method to update tookan task details
     *
     * @param companyId   : companyId
     * @param shopId      : shopId
     * @param apiKey      : apikey
     * @param requestBody : request body
     * @return : true if updated else false
     */
    @Override
    public boolean updateTaskDetails(String companyId, String shopId, String apiKey, String requestBody) {
        TookanTaskResponse response = new TookanTaskResponse();
        response = callTookanTeamApi(companyId, shopId, apiKey, UPDATE_TASK, requestBody, response, TookanTaskResponse.class);

        if (TookanBaseResponse.TookanErrorCode.toErrorCode(response.getStatus()) == TookanBaseResponse.TookanErrorCode.ACTION_COMPLETE
                && response.getData() != null && !response.getData().isEmpty()) {
            return true;
        } else {
            LOG.info("Error in tookan api calling Api Request : Edit task Error:" + TookanBaseResponse.TookanErrorCode.toErrorCode(response.getStatus()) + " Message:" + response.getMessage());
            addTookanLog(companyId, shopId, TookanErrorLog.ErrorType.TASK, TookanErrorLog.SubErrorType.Update_Task,
                    response.getMessage(), response.getStatus(),
                    apiKey,
                    "Error in tookan api calling Api Request : Edit Task Error:"
                            + TookanBaseResponse.TookanErrorCode.toErrorCode(response.getStatus())
                            + " Message:" + response.getMessage(), null);

        }

        return false;
    }

    /**
     * Override and overloaded method to get tookan team info
     *
     * @param companyId   : companyId
     * @param shopId      : shopId
     * @param tookanTeams : tookan teams
     * @param teamId      : team Id
     * @param isCheck     : if true : teamId is compared with tookan's team id(teamId);
     *                    if false : teamId is compared with _id
     * @return : tookan team info
     */

    @Override
    public TookanTeamInfo getTookanTeamInfo(String companyId, String shopId, TookanTeams tookanTeams, String teamId, boolean isCheck) {
        if (!isCheck) {
            for (TookanTeamInfo teamInfo : tookanTeams.getTookanTeamInfo()) {
                if (teamInfo.getId().equals(teamId)) {
                    return teamInfo;
                }
            }
        } else {
            for (TookanTeamInfo teamInfo : tookanTeams.getTookanTeamInfo()) {
                if (teamInfo.getTeamId().toString().equals(teamId)) {
                    return teamInfo;
                }
            }
        }
        return null;
    }

    private boolean checkSslURL(String url) {
        String pattern = "^(https)://.*$";
        return url.matches(pattern);
    }
}
