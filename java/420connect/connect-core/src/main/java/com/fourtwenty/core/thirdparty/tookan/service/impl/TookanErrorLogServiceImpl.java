package com.fourtwenty.core.thirdparty.tookan.service.impl;

import com.fourtwenty.core.domain.models.thirdparty.TookanErrorLog;
import com.fourtwenty.core.domain.repositories.tookan.TookanErrorLogRepository;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.AbstractAuthServiceImpl;
import com.fourtwenty.core.thirdparty.tookan.service.TookanErrorLogService;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.restfb.util.StringUtils;

public class TookanErrorLogServiceImpl extends AbstractAuthServiceImpl implements TookanErrorLogService {

    @Inject
    private TookanErrorLogRepository tookanErrorLogRepository;

    @Inject
    public TookanErrorLogServiceImpl(Provider<ConnectAuthToken> token) {
        super(token);
    }

    @Override
    public SearchResult<TookanErrorLog> getTookanErrorLog(String shopId, int start, int limit) {
        limit = (limit == 0) ? Integer.MAX_VALUE : limit;
        if (StringUtils.isBlank(shopId)) {
            return tookanErrorLogRepository.findItems(token.getCompanyId(), start, limit);
        }
        return tookanErrorLogRepository.findItems(token.getCompanyId(), shopId, start, limit);
    }
}

