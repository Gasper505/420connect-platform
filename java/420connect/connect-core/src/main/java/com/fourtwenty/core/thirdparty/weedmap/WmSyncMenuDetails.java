package com.fourtwenty.core.thirdparty.weedmap;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fourtwenty.core.domain.models.thirdparty.WmSyncItems;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class WmSyncMenuDetails {

    private String id;
    private String name;
    private HashMap<String, WmSyncProductDetails> productDetail = new HashMap<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public HashMap<String, WmSyncProductDetails> getProductDetail() {
        return productDetail;
    }

    public void setProductDetail(HashMap<String, WmSyncProductDetails> productDetail) {
        this.productDetail = productDetail;
    }


    public static class WmSyncProductDetails {
        private String id;
        private String blazeId;
        private String name;
        private String sku;
        private String wmsin;
        private List<String> images = new ArrayList<>();
        private List<String> variants = new ArrayList<>();
        private List<String> catalogItems = new ArrayList<>();
        private List<String> variantOptions = new ArrayList<>();
        private List<String> tags = new ArrayList<>();
        private String batchId;
        private String brandId;
        private List<HashMap<String, Object>> variantDetail = new ArrayList<>();
        private HashMap<String, String> variantValuesDetail = new HashMap<>();

        private HashMap<String, WmProductVariantDetails> weightVariantDetailsMap = new HashMap<>();

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getBlazeId() {
            return blazeId;
        }

        public void setBlazeId(String blazeId) {
            this.blazeId = blazeId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getSku() {
            return sku;
        }

        public void setSku(String sku) {
            this.sku = sku;
        }

        public String getWmsin() {
            return wmsin;
        }

        public void setWmsin(String wmsin) {
            this.wmsin = wmsin;
        }

        public List<String> getImages() {
            return images;
        }

        public void setImages(List<String> images) {
            this.images = images;
        }

        public List<String> getVariants() {
            return variants;
        }

        public void setVariants(List<String> variants) {
            this.variants = variants;
        }

        public List<String> getCatalogItems() {
            return catalogItems;
        }

        public void setCatalogItems(List<String> catalogItems) {
            this.catalogItems = catalogItems;
        }

        public List<String> getTags() {
            return tags;
        }

        public void setTags(List<String> tags) {
            this.tags = tags;
        }

        public String getBatchId() {
            return batchId;
        }

        public void setBatchId(String batchId) {
            this.batchId = batchId;
        }

        public List<String> getVariantOptions() {
            return variantOptions;
        }

        public void setVariantOptions(List<String> variantOptions) {
            this.variantOptions = variantOptions;
        }

        public String getBrandId() {
            return brandId;
        }

        public void setBrandId(String brandId) {
            this.brandId = brandId;
        }

        public List<HashMap<String, Object>> getVariantDetail() {
            return variantDetail;
        }

        public void setVariantDetail(List<HashMap<String, Object>> variantDetail) {
            this.variantDetail = variantDetail;
        }

        public HashMap<String, String> getVariantValuesDetail() {
            return variantValuesDetail;
        }

        public void setVariantValuesDetail(HashMap<String, String> variantValuesDetail) {
            this.variantValuesDetail = variantValuesDetail;
        }

        public HashMap<String, WmProductVariantDetails> getWeightVariantDetailsMap() {
            return weightVariantDetailsMap;
        }

        public void setWeightVariantDetailsMap(HashMap<String, WmProductVariantDetails> weightVariantDetailsMap) {
            this.weightVariantDetailsMap = weightVariantDetailsMap;
        }

        @JsonIgnore
        public HashMap<WmSyncItems.WMItemType, String> getLinkedItems() {
            HashMap<WmSyncItems.WMItemType, String> items = new HashMap<>();
            if (!tags.isEmpty()) {
                items.putIfAbsent(WmSyncItems.WMItemType.LINKED_TAGS, StringUtils.join(tags, ","));
            }
            if (!images.isEmpty()) {
                items.putIfAbsent(WmSyncItems.WMItemType.PRODUCT_IMAGE, StringUtils.join(images, ","));
            }
            if (!variants.isEmpty()) {
                items.putIfAbsent(WmSyncItems.WMItemType.PRODUCT_VARIANTS, StringUtils.join(variants, ","));
            }
            if (!variantOptions.isEmpty()) {
                items.putIfAbsent(WmSyncItems.WMItemType.VARIANT_OPTIONS, StringUtils.join(variantOptions, ","));
            }
            if (!catalogItems.isEmpty()) {
                items.putIfAbsent(WmSyncItems.WMItemType.CATALOG_ITEMS, StringUtils.join(catalogItems, ","));
            }
            if (StringUtils.isNotBlank(brandId)) {
                items.putIfAbsent(WmSyncItems.WMItemType.BRANDS, brandId);
            }
            if (StringUtils.isNotBlank(batchId)) {
                items.putIfAbsent(WmSyncItems.WMItemType.BATCHES, batchId);
            }
            return items;
        }
    }

    public static class WmProductVariantDetails {
        private String variantId;
        private String optionId;
        private String catalogId;
        private String weightKey;

        public String getVariantId() {
            return variantId;
        }

        public void setVariantId(String variantId) {
            this.variantId = variantId;
        }

        public String getOptionId() {
            return optionId;
        }

        public void setOptionId(String optionId) {
            this.optionId = optionId;
        }

        public String getCatalogId() {
            return catalogId;
        }

        public void setCatalogId(String catalogId) {
            this.catalogId = catalogId;
        }

        public String getWeightKey() {
            return weightKey;
        }

        public void setWeightKey(String weightKey) {
            this.weightKey = weightKey;
        }
    }
}