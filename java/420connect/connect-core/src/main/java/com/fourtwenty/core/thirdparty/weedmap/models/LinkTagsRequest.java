package com.fourtwenty.core.thirdparty.weedmap.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fourtwenty.core.domain.models.thirdparty.WmSyncItems;

import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({
        "data"
})
public class LinkTagsRequest {
    private List<WeedmapDetails> data = new ArrayList<>();

    public List<WeedmapDetails> getData() {
        return data;
    }

    public void setData(List<WeedmapDetails> data) {
        this.data = data;
    }

    public void addAll(List<String> tags) {
        tags.stream().forEach(s -> getData().add(new WeedmapDetails(WmSyncItems.WMItemType.TAGS, s)));
    }
}
