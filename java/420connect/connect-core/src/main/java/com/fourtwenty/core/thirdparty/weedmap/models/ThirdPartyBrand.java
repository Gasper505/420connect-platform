package com.fourtwenty.core.thirdparty.weedmap.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.BaseModel;

@CollectionName(name = "thirdPartyBrand")
@JsonIgnoreProperties(ignoreUnknown = true)
public class ThirdPartyBrand extends BaseModel {
    private String sourceBrandId;
    private String name;
    private ThirdPartyProduct.ThirdPartySource source = ThirdPartyProduct.ThirdPartySource.Weedmap;
    private String slug;

    public String getSourceBrandId() {
        return sourceBrandId;
    }

    public void setSourceBrandId(String sourceBrandId) {
        this.sourceBrandId = sourceBrandId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ThirdPartyProduct.ThirdPartySource getSource() {
        return source;
    }

    public void setSource(ThirdPartyProduct.ThirdPartySource source) {
        this.source = source;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }
}
