package com.fourtwenty.core.thirdparty.weedmap.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fourtwenty.core.domain.annotations.CollectionName;
import com.fourtwenty.core.domain.models.generic.BaseModel;

import java.util.List;

@CollectionName(name = "thirdPartyProduct")
@JsonIgnoreProperties(ignoreUnknown = true)
public class ThirdPartyProduct extends BaseModel {
    public enum ThirdPartySource {
        Weedmap
    }

    private String sourceProductId;
    private String name;
    private String sourceSku;
    private String thirdPartyBrandId;
    private ThirdPartySource source = ThirdPartySource.Weedmap;
    private String slug;
    private boolean soldByWeight;
    private String thirdPartyBrandName;

    private List<ThirdPartyVariant> variantDetail;

    public String getSourceProductId() {
        return sourceProductId;
    }

    public void setSourceProductId(String sourceProductId) {
        this.sourceProductId = sourceProductId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSourceSku() {
        return sourceSku;
    }

    public void setSourceSku(String sourceSku) {
        this.sourceSku = sourceSku;
    }

    public String getThirdPartyBrandId() {
        return thirdPartyBrandId;
    }

    public void setThirdPartyBrandId(String thirdPartyBrandId) {
        this.thirdPartyBrandId = thirdPartyBrandId;
    }

    public List<ThirdPartyVariant> getVariantDetail() {
        return variantDetail;
    }

    public void setVariantDetail(List<ThirdPartyVariant> variantDetail) {
        this.variantDetail = variantDetail;
    }

    public ThirdPartySource getSource() {
        return source;
    }

    public void setSource(ThirdPartySource source) {
        this.source = source;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public boolean isSoldByWeight() {
        return soldByWeight;
    }

    public void setSoldByWeight(boolean soldByWeight) {
        this.soldByWeight = soldByWeight;
    }

    public String getThirdPartyBrandName() {
        return thirdPartyBrandName;
    }

    public void setThirdPartyBrandName(String thirdPartyBrandName) {
        this.thirdPartyBrandName = thirdPartyBrandName;
    }

    public static class ThirdPartyVariant {
        private String sourceVariantId;
        private boolean soldByWeight;
        private String sourceOptionId;
        private String sourceValueId;

        public String getSourceVariantId() {
            return sourceVariantId;
        }

        public void setSourceVariantId(String sourceVariantId) {
            this.sourceVariantId = sourceVariantId;
        }

        public boolean isSoldByWeight() {
            return soldByWeight;
        }

        public void setSoldByWeight(boolean soldByWeight) {
            this.soldByWeight = soldByWeight;
        }

        public String getSourceOptionId() {
            return sourceOptionId;
        }

        public void setSourceOptionId(String sourceOptionId) {
            this.sourceOptionId = sourceOptionId;
        }

        public String getSourceValueId() {
            return sourceValueId;
        }

        public void setSourceValueId(String sourceValueId) {
            this.sourceValueId = sourceValueId;
        }
    }
}
