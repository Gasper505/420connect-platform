package com.fourtwenty.core.thirdparty.weedmap.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.math.BigDecimal;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({
        "inserted_at",
        "is_verified",
        "name",
        "updated_at",
        "listing_wmid",
        "image_url",
        "is_primary"
})
public class WeedmapAttributes {

    @JsonProperty("name")
    private String name;
    @JsonProperty("description")
    private String description;
    @JsonProperty("inserted_at")
    private String insertedAt;
    @JsonProperty("is_verified")
    private Boolean isVerified;
    @JsonProperty("updated_at")
    private String updatedAt;
    @JsonProperty("listing_wmid")
    private String listingWmId;
    @JsonProperty("wmsin")
    private String wmsin;
    @JsonProperty("image_url")
    private String imageUrl;
    @JsonProperty("is_primary")
    private Boolean primary;
    @JsonProperty("msrp_in_cents")
    private Long msrp;
    @JsonProperty("sku")
    private String sku;
    @JsonProperty("sold_by_weight")
    private Boolean soldBy;
    @JsonProperty("available")
    private Boolean available;
    @JsonProperty("online_orderable")
    private Boolean onlineAvailable;
    @JsonProperty("price_in_cents")
    private Long price;
    @JsonProperty("unit_type")
    private String unitType;
    @JsonProperty("cbd_amount")
    private BigDecimal cbdAmount;
    @JsonProperty("thc_amount")
    private BigDecimal thcAmount;
    @JsonProperty("organization_id")
    private String organizationId;
    @JsonProperty("quantity_allocated")
    private Long currentQuantity;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getInsertedAt() {
        return insertedAt;
    }

    public void setInsertedAt(String insertedAt) {
        this.insertedAt = insertedAt;
    }

    public Boolean getVerified() {
        return isVerified;
    }

    public void setVerified(Boolean verified) {
        isVerified = verified;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getListingWmId() {
        return listingWmId;
    }

    public void setListingWmId(String listingWmId) {
        this.listingWmId = listingWmId;
    }

    public String getWmsin() {
        return wmsin;
    }

    public void setWmsin(String wmsin) {
        this.wmsin = wmsin;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Boolean getPrimary() {
        return primary;
    }

    public void setPrimary(Boolean primary) {
        this.primary = primary;
    }

    public Long getMsrp() {
        return msrp;
    }

    public void setMsrp(Long msrp) {
        this.msrp = msrp;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public Boolean isSoldBy() {
        return soldBy;
    }

    public void setSoldBy(Boolean soldBy) {
        this.soldBy = soldBy;
    }

    public Boolean isAvailable() {
        return available;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }

    public Boolean isOnlineAvailable() {
        return onlineAvailable;
    }

    public void setOnlineAvailable(Boolean onlineAvailable) {
        this.onlineAvailable = onlineAvailable;
    }

    public Boolean getSoldBy() {
        return soldBy;
    }

    public Boolean getAvailable() {
        return available;
    }

    public Boolean getOnlineAvailable() {
        return onlineAvailable;
    }

    public String getUnitType() {
        return unitType;
    }

    public void setUnitType(String unitType) {
        this.unitType = unitType;
    }

    public BigDecimal getCbdAmount() {
        return cbdAmount;
    }

    public void setCbdAmount(BigDecimal cbdAmount) {
        this.cbdAmount = cbdAmount;
    }

    public BigDecimal getThcAmount() {
        return thcAmount;
    }

    public void setThcAmount(BigDecimal thcAmount) {
        this.thcAmount = thcAmount;
    }

    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }

    public Long getCurrentQuantity() {
        return currentQuantity;
    }

    public void setCurrentQuantity(Long currentQuantity) {
        this.currentQuantity = currentQuantity;
    }
}
