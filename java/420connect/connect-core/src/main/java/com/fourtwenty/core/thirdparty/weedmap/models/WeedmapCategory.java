package com.fourtwenty.core.thirdparty.weedmap.models;

/**
 * Created by mdo on 4/25/17.
 */
public enum WeedmapCategory {
    None("None", 0),
    Indica("Indica", 1),
    Sativa("Sativa", 2),
    Hybrid("Hybrid", 3),
    Edible("Edible", 4),
    Concentrate("Concentrate", 5),
    Drink("Drink", 6),
    Clone("Clone", 7),
    Seed("Seed", 8),
    Tincture("Tincture", 9),
    Gear("Gear", 10),
    Topicals("Topicals", 11),
    Preroll("Preroll", 12),
    Wax("Wax", 13);

    WeedmapCategory(String title, int id) {
        this.title = title;
        this.id = id;
    }

    public String title;
    public int id;

    public static WeedmapCategory findWeedmapCategory(String name) {
        WeedmapCategory[] categories = WeedmapCategory.values();
        for (WeedmapCategory category : categories) {
            if (category.title.equalsIgnoreCase(name)) {
                return category;
            }
        }
        return null;
    }
}
