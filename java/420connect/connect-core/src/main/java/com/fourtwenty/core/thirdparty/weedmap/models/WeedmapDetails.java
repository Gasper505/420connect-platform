package com.fourtwenty.core.thirdparty.weedmap.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fourtwenty.core.domain.models.thirdparty.WmSyncItems;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({
        "type",
        "id"
})
public class WeedmapDetails {

    @JsonProperty("type")
    private WmSyncItems.WMItemType type;
    @JsonProperty("id")
    private String id;

    public WeedmapDetails() {

    }

    public WeedmapDetails(WmSyncItems.WMItemType type, String id) {
        this.type = type;
        this.id = id;
    }

    public WmSyncItems.WMItemType getType() {
        return type;
    }

    public void setType(WmSyncItems.WMItemType type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
