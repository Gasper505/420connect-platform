package com.fourtwenty.core.thirdparty.weedmap.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by mdo on 4/25/17.
 * <p>
 * "item_id": "e1cffb73-f8aa-4402-b8a1-759bc4d6edab",
 * "listing_api_key": "0c17f5f5e00b1db8e548fcce91809648",
 * "type": "integrator_menu_item",
 * "attributes": {
 * "body": "this is my description",
 * "category_id": 1,
 * "category_name": "Indica",
 * "cbd_test_result": null,
 * "cbn_test_result": null,
 * "thc_test_result": null,
 * "image_url": "https://images.weedmaps.com/pictures/listings/352/761/143/large/5250142_fjords.jpg",
 * "lab_tested": null,
 * "name": "name change",
 * "prices": {
 * "price_gram": 10,
 * "price_two_grams": 0,
 * "price_eighth": 59,
 * "price_quarter": 115,
 * "price_half_ounce": 215,
 * "price_ounce": 420,
 * "prices_other": {
 * "price_bale": "420000.0"
 * }
 * },
 * "published": false,
 * "test_result_expires": null,
 * "test_result_expired": null,
 * "thumb_image_url": "https://images.weedmaps.com/pictures/listings/352/761/143/medium_oriented/5250142_fjords.jpg",
 * "slug": "name-change",
 * "strain_id": null,
 * "license_type": "medical"
 * },
 * "image_url": "https://www.w3schools.com/w3images/fjords.jpg"
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class WeedmapItemAttribute {

    @JsonProperty(value = "name")
    private String name;
    @JsonProperty(value = "body")
    private String body;
    @JsonProperty(value = "category_id")
    private int categoryId;
    @JsonProperty(value = "category_name")
    private String categoryName;
    @JsonProperty(value = "image_url")
    private String imageURL;
    @JsonProperty(value = "prices")
    private WeedmapPrices prices;
    @JsonProperty(value = "published")
    private boolean published;
    @JsonProperty(value = "thumb_image_url")
    private String thumbImageURL;
    @JsonProperty(value = "slug")
    private String slug;
    @JsonProperty(value = "strain_id")
    private String strainId;
    @JsonProperty(value = "license_type")
    private String licenseType;


    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getLicenseType() {
        return licenseType;
    }

    public void setLicenseType(String licenseType) {
        this.licenseType = licenseType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public WeedmapPrices getPrices() {
        return prices;
    }

    public void setPrices(WeedmapPrices prices) {
        this.prices = prices;
    }

    public boolean isPublished() {
        return published;
    }

    public void setPublished(boolean published) {
        this.published = published;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getStrainId() {
        return strainId;
    }

    public void setStrainId(String strainId) {
        this.strainId = strainId;
    }

    public String getThumbImageURL() {
        return thumbImageURL;
    }

    public void setThumbImageURL(String thumbImageURL) {
        this.thumbImageURL = thumbImageURL;
    }
}
