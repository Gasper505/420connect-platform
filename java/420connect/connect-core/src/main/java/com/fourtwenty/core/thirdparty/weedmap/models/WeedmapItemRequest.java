package com.fourtwenty.core.thirdparty.weedmap.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by mdo on 4/24/17.
 */

/*
"item_id": "e1cffb73-f8aa-4402-b8a1-759bc4d6edab",
      "body": "this is my description",
      "category_id": 1,
      "category_name": "Indica",
      "cbd_test_result": "0.25",
      "cbn_test_result": null,
      "thc_test_result": null,
      "lab_tested": null,
      "name": "name change",
      "prices": {
        "price_gram": 10,
        "price_two_grams": 0,
        "price_eighth": 59,
        "price_quarter": 115,
        "price_half_ounce": 215,
        "price_ounce": 420,
        "prices_other": {
          "price_bale": "420000.0"
        }
      },
      "published": false,
      "test_result_expires": "10/10/2017",
      "test_result_expired": null,
      "thumb_image_url": "https://images.weedmaps.com/pictures/listings/352/761/143/medium_oriented/5249946_fjords.jpg",
      "slug": "another-test-product",
      "strain_id": null,
      "license_type": "medical",
    "image_url": "https://www.w3schools.com/w3images/fjords.jpg"
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class WeedmapItemRequest {
    @JsonProperty(value = "item_id")
    private String itemId; // internal POS id
    @JsonProperty(value = "name")
    private String name;
    @JsonProperty(value = "category_name")
    private String categoryName;
    @JsonProperty(value = "category_id")
    private int categoryId;
    @JsonProperty(value = "body")
    private String body;
    @JsonProperty(value = "image_url")
    private String imageURL;
    @JsonProperty(value = "prices")
    private WeedmapPrices prices = null;


    @JsonProperty(value = "cbd_test_result")
    private String cbd;
    @JsonProperty(value = "thc_test_result")
    private String thc;
    @JsonProperty(value = "cbn_test_result")
    private String cbn;


    @JsonProperty(value = "published")
    private boolean published = true;

    @JsonProperty(value = "lab_tested")
    private boolean labTested = false;

    public boolean isPublished() {
        return published;
    }

    public void setPublished(boolean published) {
        this.published = published;
    }

    public boolean isLabTested() {
        return labTested;
    }

    public void setLabTested(boolean labTested) {
        this.labTested = labTested;
    }

    public String getCbd() {
        return cbd;
    }

    public void setCbd(String cbd) {
        this.cbd = cbd;
    }

    public String getCbn() {
        return cbn;
    }

    public void setCbn(String cbn) {
        this.cbn = cbn;
    }

    public String getThc() {
        return thc;
    }

    public void setThc(String thc) {
        this.thc = thc;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public WeedmapPrices getPrices() {
        return prices;
    }

    public void setPrices(WeedmapPrices prices) {
        this.prices = prices;
    }

}
