package com.fourtwenty.core.thirdparty.weedmap.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by mdo on 4/25/17.
 * <p>
 * "item_id": "e1cffb73-f8aa-4402-b8a1-759bc4d6edab",
 * "listing_api_key": "0c17f5f5e00b1db8e548fcce91809648",
 * "type": "integrator_menu_item",
 * "attributes": {
 * "body": "this is my description",
 * "category_id": 1,
 * "category_name": "Indica",
 * "cbd_test_result": null,
 * "cbn_test_result": null,
 * "thc_test_result": null,
 * "image_url": "https://images.weedmaps.com/pictures/listings/352/761/143/large/5250142_fjords.jpg",
 * "lab_tested": null,
 * "name": "name change",
 * "prices": {
 * "price_gram": 10,
 * "price_two_grams": 0,
 * "price_eighth": 59,
 * "price_quarter": 115,
 * "price_half_ounce": 215,
 * "price_ounce": 420,
 * "prices_other": {
 * "price_bale": "420000.0"
 * }
 * },
 * "published": false,
 * "test_result_expires": null,
 * "test_result_expired": null,
 * "thumb_image_url": "https://images.weedmaps.com/pictures/listings/352/761/143/medium_oriented/5250142_fjords.jpg",
 * "slug": "name-change",
 * "strain_id": null,
 * "license_type": "medical"
 * },
 * "image_url": "https://www.w3schools.com/w3images/fjords.jpg"
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class WeedmapMenuItem {

    @JsonProperty(value = "item_id")
    private String itemId;
    @JsonProperty(value = "listing_api_key")
    private String listingAPIKey;
    @JsonProperty(value = "type")
    private String type;
    @JsonProperty(value = "image_url")
    private String imageURL;

    @JsonProperty(value = "attributes")
    private WeedmapItemAttribute attributes;

    public WeedmapItemAttribute getAttributes() {
        return attributes;
    }

    public void setAttributes(WeedmapItemAttribute attributes) {
        this.attributes = attributes;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getListingAPIKey() {
        return listingAPIKey;
    }

    public void setListingAPIKey(String listingAPIKey) {
        this.listingAPIKey = listingAPIKey;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
