package com.fourtwenty.core.thirdparty.weedmap.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.util.HashMap;

/**
 * Created by mdo on 4/24/17.
 */
/*
"prices": {
        "price_gram": 10,
        "price_two_grams": 0,
        "price_eighth": 59,
        "price_quarter": 115,
        "price_half_ounce": 215,
        "price_ounce": 420,
        "prices_other": {
          "price_bale": "420000.0"
        }
      }
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class WeedmapPrices {
    @JsonProperty(value = "price_unit")
    private BigDecimal priceUnit = new BigDecimal(0);
    @JsonProperty(value = "price_half_gram")
    private BigDecimal priceHalfGram = new BigDecimal(0);
    @JsonProperty(value = "price_gram")
    private BigDecimal priceGram = new BigDecimal(0);
    @JsonProperty(value = "price_two_grams")
    private BigDecimal priceTwoGrams = new BigDecimal(0);
    @JsonProperty(value = "price_eighth")
    private BigDecimal priceEighth = new BigDecimal(0);
    @JsonProperty(value = "price_quarter")
    private BigDecimal priceQuarter = new BigDecimal(0);
    @JsonProperty(value = "price_half_ounce")
    private BigDecimal priceHalfOunce = new BigDecimal(0);
    @JsonProperty(value = "price_ounce")
    private BigDecimal priceOunce = new BigDecimal(0);
    @JsonProperty(value = "prices_other")
    private HashMap<String, BigDecimal> pricesOther = new HashMap<>();

    public BigDecimal getPriceEighth() {
        return priceEighth;
    }

    public void setPriceEighth(BigDecimal priceEighth) {
        this.priceEighth = priceEighth;
    }

    public BigDecimal getPriceGram() {
        return priceGram;
    }

    public void setPriceGram(BigDecimal priceGram) {
        this.priceGram = priceGram;
    }

    public BigDecimal getPriceHalfGram() {
        return priceHalfGram;
    }

    public void setPriceHalfGram(BigDecimal priceHalfGram) {
        this.priceHalfGram = priceHalfGram;
    }

    public BigDecimal getPriceHalfOunce() {
        return priceHalfOunce;
    }

    public void setPriceHalfOunce(BigDecimal priceHalfOunce) {
        this.priceHalfOunce = priceHalfOunce;
    }

    public BigDecimal getPriceOunce() {
        return priceOunce;
    }

    public void setPriceOunce(BigDecimal priceOunce) {
        this.priceOunce = priceOunce;
    }

    public BigDecimal getPriceQuarter() {
        return priceQuarter;
    }

    public void setPriceQuarter(BigDecimal priceQuarter) {
        this.priceQuarter = priceQuarter;
    }

    public HashMap<String, BigDecimal> getPricesOther() {
        return pricesOther;
    }

    public void setPricesOther(HashMap<String, BigDecimal> pricesOther) {
        this.pricesOther = pricesOther;
    }

    public BigDecimal getPriceTwoGrams() {
        return priceTwoGrams;
    }

    public void setPriceTwoGrams(BigDecimal priceTwoGrams) {
        this.priceTwoGrams = priceTwoGrams;
    }

    public BigDecimal getPriceUnit() {
        return priceUnit;
    }

    public void setPriceUnit(BigDecimal priceUnit) {
        this.priceUnit = priceUnit;
    }
}
