package com.fourtwenty.core.thirdparty.weedmap.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({
        "organization",
        "brand"
})
public class WeedmapRelationships {

    @JsonProperty("organization")
    private WmOrganizations organization;
    @JsonProperty("brand")
    private WmOrganizations brand;
    @JsonProperty("product")
    private WmOrganizations product;
    @JsonProperty("catalog")
    private WmOrganizations catalog;
    @JsonProperty("product_variant")
    private WmOrganizations variant;
    @JsonProperty("variant_attribute")
    private WmOrganizations attribute;
    @JsonProperty("variant_value")
    private WmOrganizations variantValue;


    public WeedmapRelationships() {
    }

    public WeedmapRelationships(WmOrganizations organization, WmOrganizations brand, WmOrganizations product) {
        this.organization = organization;
        this.brand = brand;
        this.product = product;
    }

    public WeedmapRelationships(WmOrganizations catalog, WmOrganizations variant) {
        this.catalog = catalog;
        this.variant = variant;
    }

    public WmOrganizations getOrganization() {
        return organization;
    }

    public void setOrganization(WmOrganizations organization) {
        this.organization = organization;
    }

    public WmOrganizations getBrand() {
        return brand;
    }

    public void setBrand(WmOrganizations brand) {
        this.brand = brand;
    }

    public WmOrganizations getProduct() {
        return product;
    }

    public void setProduct(WmOrganizations product) {
        this.product = product;
    }

    public WmOrganizations getCatalog() {
        return catalog;
    }

    public void setCatalog(WmOrganizations catalog) {
        this.catalog = catalog;
    }

    public WmOrganizations getVariant() {
        return variant;
    }

    public void setVariant(WmOrganizations variant) {
        this.variant = variant;
    }

    public WmOrganizations getAttribute() {
        return attribute;
    }

    public void setAttribute(WmOrganizations attribute) {
        this.attribute = attribute;
    }

    public WmOrganizations getVariantValue() {
        return variantValue;
    }

    public void setVariantValue(WmOrganizations variantValue) {
        this.variantValue = variantValue;
    }
}
