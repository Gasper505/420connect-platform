package com.fourtwenty.core.thirdparty.weedmap.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mdo on 4/25/17.
 */
public class WeedmapResult {
    List<WeedmapMenuItem> data = new ArrayList<>();

    public List<WeedmapMenuItem> getData() {
        return data;
    }

    public void setData(List<WeedmapMenuItem> data) {
        this.data = data;
    }
}
