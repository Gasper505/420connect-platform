package com.fourtwenty.core.thirdparty.weedmap.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fourtwenty.core.domain.models.product.ProductWeightTolerance;

import java.util.HashMap;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({
        "id",
        "variant_values"
})
public class WmAttributeResponse {
    public enum VariantAttributes {
        UNIT("Unit"),
        HALF_GRAM(".5 g"),
        GRAM("1 g"),
        TWO_GRAMS("2 g"),
        QUARTER("1/4"),
        HALF("1/2"),
        ONE_EIGHTTH("1/8"),
        OUNCE("Ounce"),
        CUSTOM("CUSTOM");

        VariantAttributes(String weightName) {
            this.weightName = weightName;
        }

        public String weightName;

        public static VariantAttributes getCorrespondingKey(ProductWeightTolerance.WeightKey weightName) {
            if (weightName == null) {
                return GRAM;
            }

            if (weightName == ProductWeightTolerance.WeightKey.GRAM || weightName == ProductWeightTolerance.WeightKey.UNIT) {
                return GRAM;
            } else if (weightName == ProductWeightTolerance.WeightKey.ONE_EIGHTTH) {
                return ONE_EIGHTTH;
            } else if (weightName == ProductWeightTolerance.WeightKey.QUARTER) {
                return QUARTER;
            } else if (weightName == ProductWeightTolerance.WeightKey.HALF) {
                return HALF;
            } else if (weightName == ProductWeightTolerance.WeightKey.OUNCE) {
                return OUNCE;
            } else if (weightName == ProductWeightTolerance.WeightKey.HALF_GRAM) {
                return HALF_GRAM;
            } else if (weightName == ProductWeightTolerance.WeightKey.TWO_GRAMS) {
                return TWO_GRAMS;
            }  else if (weightName == ProductWeightTolerance.WeightKey.CUSTOM) {
                return CUSTOM;
            }
            return null;
        }

        public static ProductWeightTolerance.WeightKey getCorrespondingKey(VariantAttributes weightName) {
            if (weightName == null) {
                return ProductWeightTolerance.WeightKey.GRAM;
            }

            if (weightName == VariantAttributes.GRAM || weightName == VariantAttributes.UNIT) {
                return ProductWeightTolerance.WeightKey.GRAM;
            } else if (weightName == VariantAttributes.ONE_EIGHTTH) {
                return ProductWeightTolerance.WeightKey.ONE_EIGHTTH;
            } else if (weightName == VariantAttributes.QUARTER) {
                return ProductWeightTolerance.WeightKey.QUARTER;
            } else if (weightName == VariantAttributes.HALF) {
                return ProductWeightTolerance.WeightKey.HALF;
            } else if (weightName == VariantAttributes.OUNCE) {
                return ProductWeightTolerance.WeightKey.OUNCE;
            } else if (weightName == VariantAttributes.HALF_GRAM) {
                return ProductWeightTolerance.WeightKey.HALF_GRAM;
            } else if (weightName == VariantAttributes.TWO_GRAMS) {
                return ProductWeightTolerance.WeightKey.TWO_GRAMS;
            }  else if (weightName == VariantAttributes.CUSTOM) {
                return ProductWeightTolerance.WeightKey.CUSTOM;
            }
            return null;
        }

        public static VariantAttributes getKey(String weightName) {
            if (weightName == null) {
                return GRAM;
            }
            weightName = weightName.equalsIgnoreCase("0.5 g") ? ".5 g" : weightName;
            if (weightName.equalsIgnoreCase(VariantAttributes.GRAM.weightName) || weightName.equalsIgnoreCase(VariantAttributes.UNIT.weightName)) {
                return GRAM;
            } else if (weightName.equalsIgnoreCase(VariantAttributes.ONE_EIGHTTH.weightName)) {
                return ONE_EIGHTTH;
            } else if (weightName.equalsIgnoreCase(VariantAttributes.QUARTER.weightName)) {
                return QUARTER;
            } else if (weightName.equalsIgnoreCase(VariantAttributes.HALF.weightName)) {
                return HALF;
            } else if (weightName.equalsIgnoreCase(VariantAttributes.OUNCE.weightName)) {
                return OUNCE;
            } else if (weightName.equalsIgnoreCase(VariantAttributes.HALF_GRAM.weightName)) {
                return HALF_GRAM;
            } else if (weightName.equalsIgnoreCase(VariantAttributes.TWO_GRAMS.weightName)) {
                return TWO_GRAMS;
            }  else if (weightName.equalsIgnoreCase(VariantAttributes.CUSTOM.weightName)) {
                return CUSTOM;
            }
            return GRAM;
        }
    }
    @JsonProperty("id")
    private String id;
    @JsonProperty("variant_values")
    private HashMap<String, String> variantValues;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public HashMap<String, String> getVariantValues() {
        return variantValues;
    }

    public void setVariantValues(HashMap<String, String> variantValues) {
        this.variantValues = variantValues;
    }
}
