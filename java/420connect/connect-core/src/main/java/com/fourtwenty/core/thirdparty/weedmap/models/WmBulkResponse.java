package com.fourtwenty.core.thirdparty.weedmap.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fourtwenty.core.thirdparty.weedmap.WmSyncMenuDetails;

import java.util.HashMap;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class WmBulkResponse {
    private HashMap<String, WmSyncMenuDetails> data;

    private WmMetaResult pageDetails;

    public HashMap<String, WmSyncMenuDetails> getData() {
        return data;
    }

    public void setData(HashMap<String, WmSyncMenuDetails> data) {
        this.data = data;
    }

    public WmMetaResult getPageDetails() {
        return pageDetails;
    }

    public void setPageDetails(WmMetaResult pageDetails) {
        this.pageDetails = pageDetails;
    }

    @JsonIgnore
    public long getTotal() {
        return pageDetails == null ? 0 : pageDetails.getTotalEntries();
    }

    @JsonIgnore
    public long getPageNumber() {
        return pageDetails == null ? 0 : pageDetails.getPageNumber();
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class WmMetaResult {
        @JsonProperty("page-number")
        private long pageNumber;
        @JsonProperty("page-size")
        private long pageSize;
        @JsonProperty("total-entries")
        private long totalEntries;
        @JsonProperty("total-pages")
        private long totalPages;

        public long getPageNumber() {
            return pageNumber;
        }

        public void setPageNumber(long pageNumber) {
            this.pageNumber = pageNumber;
        }

        public long getPageSize() {
            return pageSize;
        }

        public void setPageSize(long pageSize) {
            this.pageSize = pageSize;
        }

        public long getTotalEntries() {
            return totalEntries;
        }

        public void setTotalEntries(long totalEntries) {
            this.totalEntries = totalEntries;
        }

        public long getTotalPages() {
            return totalPages;
        }

        public void setTotalPages(long totalPages) {
            this.totalPages = totalPages;
        }
    }

}
