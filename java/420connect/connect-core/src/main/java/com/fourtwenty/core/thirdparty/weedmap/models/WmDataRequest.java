package com.fourtwenty.core.thirdparty.weedmap.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fourtwenty.core.domain.models.thirdparty.WmSyncItems;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({
        "type",
        "attributes",
        "relationships"
})
public class WmDataRequest {

    @JsonProperty("type")
    private WmSyncItems.WMItemType type;
    @JsonProperty("attributes")
    private WeedmapAttributes weedmapAttributes;
    @JsonProperty("relationships")
    private WeedmapRelationships weedmapRelationships;
    @JsonProperty("id")
    private String id;

    public WmDataRequest() {
    }

    public WmDataRequest(WeedmapAttributes attributes, WeedmapRelationships relationships) {
        weedmapAttributes = attributes;
        weedmapRelationships = relationships;
    }

    public WmSyncItems.WMItemType getType() {
        return type;
    }

    public void setType(WmSyncItems.WMItemType type) {
        this.type = type;
    }

    public WeedmapAttributes getWeedmapAttributes() {
        return weedmapAttributes;
    }

    public void setWeedmapAttributes(WeedmapAttributes weedmapAttributes) {
        this.weedmapAttributes = weedmapAttributes;
    }

    public WeedmapRelationships getWeedmapRelationships() {
        return weedmapRelationships;
    }

    public void setWeedmapRelationships(WeedmapRelationships weedmapRelationships) {
        this.weedmapRelationships = weedmapRelationships;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
