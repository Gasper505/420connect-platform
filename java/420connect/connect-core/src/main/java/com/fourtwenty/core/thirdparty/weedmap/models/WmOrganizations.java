package com.fourtwenty.core.thirdparty.weedmap.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({
        "data"
})
public class WmOrganizations {

    @JsonProperty("data")
    private WeedmapDetails data;

    public WmOrganizations() {
    }

    public WmOrganizations(WeedmapDetails data) {
        this.data = data;
    }

    public WeedmapDetails getData() {
        return data;
    }

    public void setData(WeedmapDetails data) {
        this.data = data;
    }
}
