package com.fourtwenty.core.thirdparty.weedmap.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({
        "organization",
        "brand"
})
public class WmRelationshipResponse {

    @JsonProperty("organization")
    private WmOrganizations organization;

    public WmOrganizations getOrganization() {
        return organization;
    }

    public void setOrganization(WmOrganizations organization) {
        this.organization = organization;
    }
}
