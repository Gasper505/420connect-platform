package com.fourtwenty.core.thirdparty.weedmap.models;

import com.fourtwenty.core.domain.models.thirdparty.WmSyncItems;
import org.apache.commons.collections.CollectionUtils;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.List;

public class WmRequestGenerator {
    public static WmSyncRequest catalogRequest(String wmListingId) {
        WmSyncRequest request = new WmSyncRequest();

        WeedmapAttributes attributes = new WeedmapAttributes();

        attributes.setListingWmId(wmListingId.trim());

        request.getWmData().setWeedmapAttributes(attributes);
        request.getWmData().setType(WmSyncItems.WMItemType.CATALOG);

        return request;
    }

    public static WmSyncRequest brandRequest(String organizationId, String name) {
        WmSyncRequest request = new WmSyncRequest();

        WeedmapDetails details = new WeedmapDetails(WmSyncItems.WMItemType.ORGANIZATIONS, organizationId);

        WmOrganizations wmOrganizations = new WmOrganizations(details);

        request.setWmData(new WmDataRequest(new WeedmapAttributes(), new WeedmapRelationships()));

        request.getWmData().getWeedmapAttributes().setName(name);
        request.getWmData().getWeedmapRelationships().setOrganization(wmOrganizations);
        request.getWmData().setType(WmSyncItems.WMItemType.BRANDS);

        return request;
    }

    public static WmSyncRequest productRequest(String organizationId, String brandId, String name, String description, String id, String wmsin) {
        WmSyncRequest request = new WmSyncRequest();

        WeedmapAttributes attributes = new WeedmapAttributes();
        attributes.setName(name);
        attributes.setDescription(description);
        attributes.setWmsin(wmsin);

        WeedmapDetails organizationDetails = new WeedmapDetails(WmSyncItems.WMItemType.ORGANIZATIONS, organizationId);

        WeedmapDetails brandDetails = new WeedmapDetails(WmSyncItems.WMItemType.BRANDS, brandId);

        WeedmapRelationships relationships = new WeedmapRelationships(new WmOrganizations(organizationDetails), new WmOrganizations(brandDetails), null);

        request.setWmData(new WmDataRequest(new WeedmapAttributes(), new WeedmapRelationships()));

        request.getWmData().setWeedmapAttributes(attributes);
        request.getWmData().setWeedmapRelationships(relationships);
        request.getWmData().setType(WmSyncItems.WMItemType.PRODUCTS);
        request.getWmData().setId(id);

        return request;
    }

    public static WmSyncRequest imageRequest(String productId, String imageUrl, boolean isPrimary, String id) {
        WmSyncRequest request = new WmSyncRequest();

        WeedmapAttributes attributes = new WeedmapAttributes();
        attributes.setImageUrl(imageUrl);
        attributes.setPrimary(isPrimary);

        WeedmapDetails productDetails = new WeedmapDetails(WmSyncItems.WMItemType.PRODUCTS, productId);

        WeedmapRelationships relationships = new WeedmapRelationships(null, null, new WmOrganizations(productDetails));

        request.setWmData(new WmDataRequest(new WeedmapAttributes(), new WeedmapRelationships()));
        request.getWmData().setWeedmapAttributes(attributes);
        request.getWmData().setWeedmapRelationships(relationships);
        request.getWmData().setType(WmSyncItems.WMItemType.PRODUCT_IMAGE);
        request.getWmData().setId(id);
        return request;
    }

    public static WmSyncRequest productVariantRequest(String organizationId, String productId, BigDecimal unitPrice, String sku, boolean soldBy, String id) {
        WmSyncRequest request = new WmSyncRequest();

        WeedmapAttributes attributes = new WeedmapAttributes();
        attributes.setSku(sku);
        attributes.setSoldBy(soldBy);
        attributes.setMsrp(unitPrice.longValueExact());

        WeedmapDetails organizationDetails = new WeedmapDetails(WmSyncItems.WMItemType.ORGANIZATIONS, organizationId);

        WeedmapDetails productDetails = new WeedmapDetails(WmSyncItems.WMItemType.PRODUCTS, productId);

        WeedmapRelationships relationships = new WeedmapRelationships(new WmOrganizations(organizationDetails), null, new WmOrganizations(productDetails));

        request.setWmData(new WmDataRequest(new WeedmapAttributes(), new WeedmapRelationships()));
        request.getWmData().setWeedmapAttributes(attributes);
        request.getWmData().setWeedmapRelationships(relationships);
        request.getWmData().setType(WmSyncItems.WMItemType.PRODUCT_VARIANTS);
        request.getWmData().setId(id);

        return request;
    }

    public static LinkTagsRequest linkTagsRequest(List<String> tags) {
        LinkTagsRequest request = new LinkTagsRequest();
        if (CollectionUtils.isEmpty(tags)) {
            return request;
        }
        request.addAll(tags);
        return request;
    }

    public static WmSyncRequest catalogItemRequest(String catalogId, String variantId, boolean available, boolean onlineAvailable, BigDecimal price, String id) {
        WmSyncRequest request = new WmSyncRequest();

        WeedmapAttributes attributes = new WeedmapAttributes();
        attributes.setAvailable(available);
        attributes.setOnlineAvailable(onlineAvailable);
        attributes.setPrice(price.longValueExact());

        WeedmapDetails catalogDetails = new WeedmapDetails(WmSyncItems.WMItemType.CATALOG, catalogId);

        WeedmapDetails variantDetails = new WeedmapDetails(WmSyncItems.WMItemType.PRODUCT_VARIANTS, variantId);

        WeedmapRelationships relationships = new WeedmapRelationships(new WmOrganizations(catalogDetails), new WmOrganizations(variantDetails));

        request.setWmData(new WmDataRequest(new WeedmapAttributes(), new WeedmapRelationships()));
        request.getWmData().setWeedmapAttributes(attributes);
        request.getWmData().setWeedmapRelationships(relationships);
        request.getWmData().setType(WmSyncItems.WMItemType.CATALOG_ITEMS);
        request.getWmData().setId(id);

        return request;
    }

    public static WmSyncRequest createOptionRequest(String variantId, String attributeId, String valueId) {
        WmSyncRequest request = new WmSyncRequest();

        WeedmapDetails attributeDetails = new WeedmapDetails(WmSyncItems.WMItemType.VARIANT_ATTRIBUTES, attributeId);

        WeedmapDetails variantDetails = new WeedmapDetails(WmSyncItems.WMItemType.PRODUCT_VARIANTS, variantId);

        WeedmapDetails valueDetails = new WeedmapDetails(WmSyncItems.WMItemType.VARIANT_VALUES, valueId);

        WeedmapRelationships relationships = new WeedmapRelationships();

        relationships.setVariant(new WmOrganizations(variantDetails));
        relationships.setVariantValue(new WmOrganizations(valueDetails));
        relationships.setAttribute(new WmOrganizations(attributeDetails));

        request.setWmData(new WmDataRequest(new WeedmapAttributes(), new WeedmapRelationships()));
        request.getWmData().setWeedmapRelationships(relationships);
        request.getWmData().setType(WmSyncItems.WMItemType.VARIANT_OPTIONS);

        return request;
    }

    public static WmSyncRequest createBatchRequest(String productId, String organizationId, BigDecimal thcAmount, BigDecimal cbdAmount, String unitType, String id) {
        WmSyncRequest request = new WmSyncRequest();

        WeedmapDetails organizationDetail = new WeedmapDetails(WmSyncItems.WMItemType.ORGANIZATIONS, organizationId);

        WeedmapDetails productDetails = new WeedmapDetails(WmSyncItems.WMItemType.PRODUCTS, productId);

        WeedmapRelationships relationships = new WeedmapRelationships();

        relationships.setProduct(new WmOrganizations(productDetails));
        relationships.setOrganization(new WmOrganizations(organizationDetail));

        WeedmapAttributes data = new WeedmapAttributes();
        data.setCbdAmount(cbdAmount);
        data.setThcAmount(thcAmount);
        data.setUnitType(unitType);

        request.setWmData(new WmDataRequest(data, new WeedmapRelationships()));
        request.getWmData().setId(id);
        request.getWmData().setWeedmapRelationships(relationships);
        request.getWmData().setType(WmSyncItems.WMItemType.BATCHES);

        return request;
    }

    public static WmSyncRequest createInventoryRequest(String organizationId, BigDecimal currentQuantity) {
        WmSyncRequest request = new WmSyncRequest();

        WeedmapAttributes data = new WeedmapAttributes();
        data.setOrganizationId(organizationId);
        data.setCurrentQuantity(currentQuantity.round(new MathContext(3)).longValue());

        request.setWmData(new WmDataRequest());
        request.getWmData().setWeedmapAttributes(data);

        return request;
    }
}
