package com.fourtwenty.core.thirdparty.weedmap.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fourtwenty.core.domain.models.thirdparty.WmSyncItems;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({
        "data"
})
public class WmSyncRequest {
    @JsonProperty("data")
    private WmDataRequest wmData;

    public WmSyncRequest() {
        wmData = new WmDataRequest();
    }

    public WmDataRequest getWmData() {
        return wmData;
    }

    public void setWmData(WmDataRequest wmData) {
        this.wmData = wmData;
    }

    @JsonIgnore
    public String getId() {
        return getWmData().getId();
    }

    @JsonIgnore
    public WmSyncItems.WMItemType getType() {
        return getWmData().getType();
    }

    @JsonIgnore
    public String getOrganizationId() {
        if (getWmData() != null && getWmData().getWeedmapRelationships() != null && getWmData().getWeedmapRelationships().getOrganization() != null && getWmData().getWeedmapRelationships().getOrganization().getData() != null) {
            return getWmData().getWeedmapRelationships().getOrganization().getData().getId();
        }
        return null;
    }

    public enum WmErrorCode {
        SUCCESS(201, "Success"),
        CREATED(200, "Success"),
        BAD_REQUEST(400, "Bad Request"),
        UNAUTHORIZED(401, "Unauthorized"),
        NO_CONTENT(204, "No Content");

        public int code;
        public String message;

        WmErrorCode(int code, String message) {
            this.code = code;
            this.message = message;
        }

        public static WmErrorCode toErrorCode(int code) {
            switch (code) {
                case 201:
                    return SUCCESS;
                case 400:
                    return BAD_REQUEST;
                case 401:
                    return UNAUTHORIZED;
                default:
                    return BAD_REQUEST;
            }
        }

    }
}
