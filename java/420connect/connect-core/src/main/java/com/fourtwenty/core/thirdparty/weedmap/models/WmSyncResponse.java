package com.fourtwenty.core.thirdparty.weedmap.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fourtwenty.core.domain.models.thirdparty.WmSyncItems;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({
        "data"
})
public class WmSyncResponse {
    @JsonProperty("data")
    private WmDataResponse wmData;

    public WmDataResponse getWmData() {
        return wmData;
    }

    public void setWmData(WmDataResponse wmData) {
        this.wmData = wmData;
    }

    @JsonIgnore
    public String getId() {
        if (getWmData() != null) {
            return getWmData().getId();
        }
        return "";
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class WmDataResponse {
        @JsonProperty("type")
        private WmSyncItems.WMItemType type;
        @JsonProperty("attributes")
        private WeedmapAttributes weedmapAttributes;
        @JsonProperty("relationships")
        private WmRelationshipResponse weedmapRelationships;
        @JsonProperty("id")
        private String id;

        public WmSyncItems.WMItemType getType() {
            return type;
        }

        public void setType(WmSyncItems.WMItemType type) {
            this.type = type;
        }

        public WeedmapAttributes getWeedmapAttributes() {
            return weedmapAttributes;
        }

        public void setWeedmapAttributes(WeedmapAttributes weedmapAttributes) {
            this.weedmapAttributes = weedmapAttributes;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public WmRelationshipResponse getWeedmapRelationships() {
            return weedmapRelationships;
        }

        public void setWeedmapRelationships(WmRelationshipResponse weedmapRelationships) {
            this.weedmapRelationships = weedmapRelationships;
        }
    }
}
