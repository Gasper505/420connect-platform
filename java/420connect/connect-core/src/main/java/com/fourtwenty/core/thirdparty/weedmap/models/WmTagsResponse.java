package com.fourtwenty.core.thirdparty.weedmap.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WmTagsResponse {

    @JsonProperty("data")
    private List<WmDataRequest> data;

    public List<WmDataRequest> getData() {
        return data;
    }

    public void setData(List<WmDataRequest> data) {
        this.data = data;
    }
}
