package com.fourtwenty.core.thirdparty.weedmap.result;


import com.fourtwenty.core.domain.models.thirdparty.leafly.LeaflyAccount;

public class LeaflyAccountResult extends LeaflyAccount {

    private String shopName;

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }
}
