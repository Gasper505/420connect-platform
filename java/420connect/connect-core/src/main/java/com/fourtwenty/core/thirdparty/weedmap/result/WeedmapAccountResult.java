package com.fourtwenty.core.thirdparty.weedmap.result;

import com.fourtwenty.core.domain.models.common.WeedmapConfig;
import com.fourtwenty.core.domain.models.thirdparty.WeedmapAccount;
import com.fourtwenty.core.domain.models.thirdparty.WmProductMapping;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;

public class WeedmapAccountResult extends WeedmapAccount {
    private transient  HashMap<String, LinkedHashMap<String, LinkedHashSet<WmProductMapping.Tags>>> defaultTagMapping = new HashMap<>();
    private transient WeedmapConfig weedmapConfig;
    private transient List<WmProductMappingResult> tagMapping = new ArrayList<>();

    public HashMap<String, LinkedHashMap<String, LinkedHashSet<WmProductMapping.Tags>>> getDefaultTagMapping() {
        return defaultTagMapping;
    }

    public void setDefaultTagMapping(HashMap<String, LinkedHashMap<String, LinkedHashSet<WmProductMapping.Tags>>> defaultTagMapping) {
        this.defaultTagMapping = defaultTagMapping;
    }

    public WeedmapConfig getWeedmapConfig() {
        return weedmapConfig;
    }

    public void setWeedmapConfig(WeedmapConfig weedmapConfig) {
        this.weedmapConfig = weedmapConfig;
    }

    public List<WmProductMappingResult> getTagMapping() {
        return tagMapping;
    }

    public void setTagMapping(List<WmProductMappingResult> tagMapping) {
        this.tagMapping = tagMapping;
    }
}
