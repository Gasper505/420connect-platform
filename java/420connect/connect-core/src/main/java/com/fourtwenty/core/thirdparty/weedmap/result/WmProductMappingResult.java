package com.fourtwenty.core.thirdparty.weedmap.result;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fourtwenty.core.domain.models.thirdparty.WmProductMapping;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class WmProductMappingResult extends WmProductMapping {
    private String thirdPartyProductName;
    private String thirdPartyBrandName;
    private String tagGroupName;
    private String discoveryTagName;
    private String thirdPartyBrandId;
    private boolean synced;

    public String getThirdPartyProductName() {
        return thirdPartyProductName;
    }

    public void setThirdPartyProductName(String thirdPartyProductName) {
        this.thirdPartyProductName = thirdPartyProductName;
    }

    public String getThirdPartyBrandName() {
        return thirdPartyBrandName;
    }

    public void setThirdPartyBrandName(String thirdPartyBrandName) {
        this.thirdPartyBrandName = thirdPartyBrandName;
    }

    public String getTagGroupName() {
        return tagGroupName;
    }

    public void setTagGroupName(String tagGroupName) {
        this.tagGroupName = tagGroupName;
    }

    public String getDiscoveryTagName() {
        return discoveryTagName;
    }

    public void setDiscoveryTagName(String discoveryTagName) {
        this.discoveryTagName = discoveryTagName;
    }

    public String getThirdPartyBrandId() {
        return thirdPartyBrandId;
    }

    public void setThirdPartyBrandId(String thirdPartyBrandId) {
        this.thirdPartyBrandId = thirdPartyBrandId;
    }

    public boolean isSynced() {
        return synced;
    }

    public void setSynced(boolean synced) {
        this.synced = synced;
    }
}
