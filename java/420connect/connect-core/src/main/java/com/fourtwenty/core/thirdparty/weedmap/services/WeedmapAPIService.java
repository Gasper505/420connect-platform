package com.fourtwenty.core.thirdparty.weedmap.services;

import com.fourtwenty.core.domain.models.common.WeedmapConfig;
import com.fourtwenty.core.domain.models.thirdparty.WeedmapApiKeyMap;
import com.fourtwenty.core.domain.models.thirdparty.WmSyncItems;
import com.fourtwenty.core.domain.models.thirdparty.WmTagGroups;
import com.fourtwenty.core.rest.thirdparty.WeedmapTokenRequest;
import com.fourtwenty.core.rest.thirdparty.WeedmapTokenResponse;
import com.fourtwenty.core.thirdparty.weedmap.WmSyncMenuDetails;
import com.fourtwenty.core.thirdparty.weedmap.models.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by mdo on 4/25/17.
 */
public interface WeedmapAPIService {
    WeedmapResult getWeedmapMenu(String apiKey) throws InterruptedException, ExecutionException, IOException;

    WeedmapMenuItem addWeedmapMenuItem(String apiKey, WeedmapItemRequest request) throws InterruptedException, ExecutionException, IOException;

    WeedmapMenuItem updateWeedmapMenuItem(String apiKey, String itemId, WeedmapItemRequest request) throws InterruptedException, ExecutionException, IOException;

    WeedmapMenuItem deleteWeedmapMenuItem(String apiKey, String itemId);

    WmSyncRequest createWeedmapCatalog(String companyId, String shopId, WeedmapApiKeyMap apiKeyMap, WmSyncRequest request, WeedmapConfig weedmapConfig);
    WmSyncRequest getCatalogById(String companyId, String shopId,  String catalogId, WeedmapApiKeyMap apiKeyMap, WeedmapConfig weedmapConfig);
    String getCatalogId(String companyId, String shopId, WeedmapApiKeyMap apiKeyMap, WeedmapConfig weedmapConfig);

    WeedmapTokenResponse authenticateWeedmap(String companyId, String shopId, WeedmapTokenRequest request, WeedmapConfig weedmapConfig);

    boolean linkProductTags(String companyId, String shopId, String organizationId, WeedmapApiKeyMap apiKeyMap, LinkTagsRequest linkTagsRequest, String wmItemId, WeedmapConfig weedmapConfig);

    <T extends Object> T syncItems(String companyId, String shopId, String organisationId, WeedmapApiKeyMap apiKeyMap, WmSyncRequest request, Class<T> clazz, WeedmapConfig weedmapConfig);

    <T extends Object> boolean deleteWmItem(String companyId, String shopId, String organizationId, WeedmapApiKeyMap apiKey, WmSyncItems.WMItemType type, String wmItemId, T request, WeedmapConfig weedmapConfig);

    WmTagsResponse getWeedmapTags(String companyId, String shopId, String organizationId, WeedmapApiKeyMap apiKeyMap, WeedmapConfig weedmapConfig);

    WmAttributeResponse getVariantAttributes(String companyId, String shopId, String organizationId, WeedmapApiKeyMap apiKeyMap, WeedmapConfig weedmapConfig);

    HashMap<String, WmSyncMenuDetails> getWeedmapMenu(String companyId, String shopId, String organizationId, WeedmapConfig weedmapConfig, WeedmapApiKeyMap apiKeyMap);

    boolean validateTags(String companyId, String shopId, String organizationId, LinkTagsRequest linkTagsRequest, WeedmapApiKeyMap apiKeyMap, WeedmapConfig weedmapConfig);

    List<ThirdPartyBrand> getWmVerifiedBrands(String companyId, String shopId, String organizationId, WeedmapConfig weedmapConfig, WeedmapApiKeyMap apiKeyMap, String term);

    List<ThirdPartyProduct> getWmVerifiedProducts(String companyId, String shopId, String organizationId, WeedmapConfig config, WeedmapApiKeyMap apiKeyMap, String brandId, String term);

    <T extends Object> T syncInventory(String companyId, String shopId, String organisationId, String catalogItemId, WeedmapApiKeyMap apiKeyMap, WmSyncRequest request, Class<T> clazz, WeedmapConfig weedmapConfig);

    WmSyncMenuDetails.WmSyncProductDetails getWmVerifiedProducts(String companyId, String shopId, String organizationId, WeedmapConfig weedmapConfig, WeedmapApiKeyMap apiKeyMap, String sourceProductId);

    WmSyncMenuDetails.WmSyncProductDetails getWmCustomProduct(String companyId, String shopId, String organizationId, WeedmapConfig weedmapConfig, WeedmapApiKeyMap apiKeyMap, String sourceProductId);

    List<WmTagGroups> getWmTagGroups(String companyId, String shopId, String organizationId, WeedmapConfig config, WeedmapApiKeyMap apiKeyMap);
}
