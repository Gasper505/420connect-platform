package com.fourtwenty.core.thirdparty.weedmap.services.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fourtwenty.core.domain.models.common.WeedmapConfig;
import com.fourtwenty.core.domain.models.thirdparty.WeedmapApiKeyMap;
import com.fourtwenty.core.domain.models.thirdparty.WmSyncItems;
import com.fourtwenty.core.domain.models.thirdparty.WmTagGroups;
import com.fourtwenty.core.rest.thirdparty.WeedmapTokenRequest;
import com.fourtwenty.core.rest.thirdparty.WeedmapTokenResponse;
import com.fourtwenty.core.thirdparty.weedmap.WmSyncMenuDetails;
import com.fourtwenty.core.thirdparty.weedmap.models.*;
import com.fourtwenty.core.thirdparty.weedmap.services.WeedmapAPIService;
import com.fourtwenty.core.util.JsonSerializer;
import com.fourtwenty.core.util.SSLUtilities;
import com.github.slugify.Slugify;
import com.google.common.collect.Lists;
import com.mdo.pusher.RestErrorException;
import com.mdo.pusher.SimpleRestUtil;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ExecutionException;

/**
 * Created by mdo on 4/25/17.
 */
public class WeedmapAPIServiceImpl implements WeedmapAPIService {
    private static final Logger LOGGER = LoggerFactory.getLogger(WeedmapAPIServiceImpl.class);

    private static final String WEEDMAP_MEDIATYPE = "application/vnd.api+json";
    private static final String WEEDMAP_BASE_URL = "https://weedmaps.com";
    private static final String WEEDMAP_API_URL = "/api/web/v1/integrator_menu_items";
    private static final String WEEDMAP_V2_API_URL = "/catalog/v1";
    private static final String WEEDMAP_V2_BRANDS = "/brands";
    private static final String WEEDMAP_V2_CATALOGS = "/catalogs";
    private static final String WEEDMAP_V2_PRODUCTS = "/products";
    private static final String WEEDMAP_V2_IMAGES = "/product_images";
    private static final String WEEDMAP_V2_VARIANTS = "/product_variants";
    private static final String WEEDMAP_V2_LINKTAGS = "/products/%s/relationships/tags";
    private static final String WEEDMAP_V2_CATALOGITEM = "/catalog_items";
    private static final String WEEDMAP_V2_TAGS = "/tags";
    private static final String WEEDMAP_V2_ATTRIBUTES = "/variant_attributes";
    private static final String WEEDMAP_V2_OPTIONS = "/product_variant_options";
    private static final String WEEDMAP_V2_VALUES = "/variant_values";
    private static final String WEEDMAP_V2_BATCHES = "/batches";
    private static final String WEEDMAP_V2_INVENTORY = "/inventory/v1/catalog_items/%s/quantities";
    private static final String WEEDMAP_V2_TAGGROUP = "/tag_groups";
    private static final Long FETCH_LIMIT = 100L;

    @Override
    public WeedmapMenuItem addWeedmapMenuItem(String apiKey, WeedmapItemRequest request) throws InterruptedException, ExecutionException, IOException {
        String urlPath = createURL(apiKey, null);
        MultivaluedMap<String, Object> headers = new MultivaluedHashMap<>();
        headers.putSingle("Content-Type", MediaType.APPLICATION_JSON);
        return SimpleRestUtil.post(urlPath, request, WeedmapMenuItem.class, String.class, headers);
    }

    @Override
    public WeedmapResult getWeedmapMenu(String apiKey) throws InterruptedException, ExecutionException, IOException {
        String urlPath = createURL(apiKey, null);

        return SimpleRestUtil.get(urlPath, WeedmapResult.class);
    }

    @Override
    public WeedmapMenuItem updateWeedmapMenuItem(String apiKey, String itemId, WeedmapItemRequest request) throws InterruptedException, ExecutionException, IOException {
        String urlPath = createURL(apiKey, itemId);
        MultivaluedMap<String, Object> headers = new MultivaluedHashMap<>();
        headers.putSingle("Content-Type", MediaType.APPLICATION_JSON);
        return SimpleRestUtil.put(urlPath, request, WeedmapMenuItem.class, String.class, headers);
    }

    @Override
    public WeedmapMenuItem deleteWeedmapMenuItem(String apiKey, String itemId) {
        String urlPath = createURL(apiKey, itemId);
        MultivaluedMap<String, Object> headers = new MultivaluedHashMap<>();
        headers.putSingle("Content-Type", MediaType.APPLICATION_JSON);
        return SimpleRestUtil.delete(urlPath, WeedmapMenuItem.class, headers);
    }

    private static String createURL(String apiKey, String itemId) {
        StringBuilder pathSB = new StringBuilder(WEEDMAP_BASE_URL);
        pathSB.append(WEEDMAP_API_URL);
        if (StringUtils.isNotBlank(apiKey)) {
            pathSB.append("/" + apiKey);
        } else {
            return pathSB.toString();
        }

        if (StringUtils.isNotBlank(itemId)) {
            pathSB.append("/" + itemId);
        }
        return pathSB.toString();
    }

    /* Weedmap Version2 Sync*/

    /**
     * Private method to create wm V2 url
     *
     * @param type       : type
     * @param syncItemId : itemId
     * @return: url
     */
    private String createV2Url(WmSyncItems.WMItemType type, String syncItemId, WeedmapConfig config) {
        if (config == null || StringUtils.isBlank(config.getHost())) {
            LOGGER.info("Configuration not found for Version 2");
            return "";
        }
        if (type == null) {
            return "";
        }
        String url = "";
        switch (type) {
            case BRANDS:
                url = WEEDMAP_V2_BRANDS;
                break;
            case PRODUCTS:
                url = WEEDMAP_V2_PRODUCTS;
                break;
            case TAGS:
                url = WEEDMAP_V2_TAGS;
                break;
            case TAGS_GROUPS:
                url = WEEDMAP_V2_TAGGROUP;
                break;
            case CATALOG:
                url = WEEDMAP_V2_CATALOGS;
                break;
            case PRODUCT_IMAGE:
                url = WEEDMAP_V2_IMAGES;
                break;
            case PRODUCT_VARIANTS:
                url = WEEDMAP_V2_VARIANTS;
                break;
            case VARIANT_ATTRIBUTES:
                url = WEEDMAP_V2_ATTRIBUTES;
                break;
            case VARIANT_VALUES:
                url = WEEDMAP_V2_VALUES;
                break;
            case VARIANT_OPTIONS:
                url = WEEDMAP_V2_OPTIONS;
                break;
            case CATALOG_ITEMS:
                url = WEEDMAP_V2_CATALOGITEM;
                break;
            case LINKED_TAGS:
                url = String.format(WEEDMAP_V2_LINKTAGS, syncItemId);
                break;
            case BATCHES:
                url = String.format(WEEDMAP_V2_BATCHES, syncItemId);
                break;
            case INVENTORY:
                url = String.format(WEEDMAP_V2_INVENTORY, syncItemId);
                break;
        }

        if (StringUtils.isBlank(syncItemId) || (type == WmSyncItems.WMItemType.LINKED_TAGS)) {
            return config.getHost() + WEEDMAP_V2_API_URL + url;
        } else if (type == WmSyncItems.WMItemType.INVENTORY) {
            return config.getHost() + url;
        }
        return config.getHost() + WEEDMAP_V2_API_URL + url + "/" + syncItemId;
    }

    /**
     * Private method to get headers
     *
     * @param accessToken : accessToken
     * @return : headers
     */
    private MultivaluedMap<String, Object> getHeaders(String accessToken) {
        MultivaluedMap<String, Object> headers = new MultivaluedHashMap<>();
        headers.putSingle("Authorization", "Bearer " + accessToken);
        headers.putSingle("Content-Type", WEEDMAP_MEDIATYPE);
        headers.putSingle("Accept", WEEDMAP_MEDIATYPE);
        headers.putSingle("user-agent","Blaze-http-client");
        return headers;
    }

    /**
     * Private method to get query param
     *
     * @param organisationId : organizationId
     * @return: query params
     */
    private HashMap<String, Object> getQueryParams(String organisationId) {
        HashMap<String, Object> queryParams = new HashMap<>();
        queryParams.putIfAbsent("scope_by[organization_id]", organisationId);

        return queryParams;
    }

    /**
     * Private method to call weedmap api's
     *
     * @param companyId   :  companyId
     * @param shopId      :  shopId
     * @param url         :  url
     * @param headers     :  headers
     * @param queryParams :  queryParams
     * @param jsonBody    :  jsonBody
     * @param clazz       :  clazz
     * @param isPatch     :  isPatch
     * @param <T>         :  clazz
     * @return : response object
     */
    private <T extends Object> T callWeedmapApi(String companyId, String shopId, String url, MultivaluedMap<String, Object> headers, HashMap<String, Object> queryParams, String jsonBody, Class<T> clazz, boolean isPatch) {
        SSLUtilities.trustAllHostnames();
        SSLUtilities.trustAllHttpsCertificates();
        T responseClazz;
        try {
            String response;
            if (isPatch) {
                response = SimpleRestUtil.patchWithSsl(url, jsonBody, String.class, headers, queryParams, WEEDMAP_MEDIATYPE, WmSyncRequest.WmErrorCode.CREATED.code);
            } else {
                response = SimpleRestUtil.postWithSsl(url, jsonBody, String.class, headers, queryParams, WEEDMAP_MEDIATYPE, WmSyncRequest.WmErrorCode.SUCCESS.code);
            }
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
            responseClazz = objectMapper.readValue(response, clazz);
        } catch (RestErrorException error) {
            LOGGER.error(String.format("Error in calling url: %s with error message: %s", url, error.getErrorResponse()), error);
            responseClazz = null;
        } catch (Exception e) {
            LOGGER.error(String.format("Error in calling url: %s with error message: %s", url, e.getMessage()), e);
            responseClazz = null;
        }

        return responseClazz;
    }

    /**
     * Override method to sync items
     *
     * @param <T>            :  <T>
     * @param companyId      :  companyId
     * @param shopId         :  shopId
     * @param organisationId :  organisationId
     * @param apiKeyMap      :  apiKeyMap
     * @param request        :  request
     * @param clazz          :  clazz
     * @param weedmapConfig
     * @return
     */
    @Override
    public <T extends Object> T syncItems(String companyId, String shopId, String organisationId, WeedmapApiKeyMap apiKeyMap, WmSyncRequest request, Class<T> clazz, WeedmapConfig weedmapConfig) {
        String url = createV2Url(request.getType(), request.getId(), weedmapConfig);

        MultivaluedMap<String, Object> headers = getHeaders(apiKeyMap.getToken());

        String jsonBody = JsonSerializer.toJson(request);

        HashMap<String, Object> queryParams = getQueryParams(organisationId);

        LOGGER.info(String.format("Calling start api: %s", url));

        T response = (T) callWeedmapApi(companyId, shopId, url, headers, queryParams, jsonBody, clazz, StringUtils.isNotBlank(request.getId()));

        LOGGER.info(String.format("Calling end api: %s", url));
        return response;
    }

    /**
     * Override method to create catalog
     *
     * @param companyId :  companyId
     * @param shopId    :  shopId
     * @param apiKeyMap :  apiKeyMap
     * @param request   :  request
     * @param weedmapConfig : config
     * @return : response
     */
    @Override
    public WmSyncRequest createWeedmapCatalog(String companyId, String shopId, WeedmapApiKeyMap apiKeyMap, WmSyncRequest request, WeedmapConfig weedmapConfig) {

        String url = createV2Url(request.getType(), null, weedmapConfig);

        MultivaluedMap<String, Object> headers = getHeaders(apiKeyMap.getToken());

        String jsonBody = JsonSerializer.toJson(request);
        WmSyncRequest response = callWeedmapApi(companyId, shopId, url, headers, new HashMap<>(), jsonBody, WmSyncRequest.class, false);

        return response;
    }

    /**
     * Override method to authenticate token
     *
     * @param companyId :  companyId
     * @param shopId    :  shopId
     * @param request   :  request
     * @param weedmapConfig
     * @return : response
     */
    @Override
    public WeedmapTokenResponse authenticateWeedmap(String companyId, String shopId, WeedmapTokenRequest request, WeedmapConfig weedmapConfig) {
        SSLUtilities.trustAllHostnames();
        SSLUtilities.trustAllHttpsCertificates();

        String url =  weedmapConfig.getHost() + "/auth/token";

        MultivaluedMap<String, Object> headers = new MultivaluedHashMap<>();
        headers.putSingle("Content-Type", MediaType.APPLICATION_JSON);
        headers.putSingle("accept", "*/*");
        headers.putSingle("user-agent","Blaze-http-client");

        String json = JsonSerializer.toJson(request);
        WeedmapTokenResponse weedmapTokenResponse = null;
        String response = "";
        try {
            response = SimpleRestUtil.post(url, json, String.class, headers);
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
            weedmapTokenResponse = objectMapper.readValue(response, WeedmapTokenResponse.class);
        } catch (RestErrorException e) {
            LOGGER.error(String.format("Error in calling url: %s with error message: %s with request: %s response: %s", url, e.getErrorResponse(), json, response), e);
        }  catch (Exception e) {
            LOGGER.error(String.format("Error in calling url: %s with error message: %s with response: %s", url, e.getMessage(), response), e);
        }
        return weedmapTokenResponse;
    }


    @Override
    public String getCatalogId(String companyId, String shopId,  WeedmapApiKeyMap apiKeyMap, WeedmapConfig weedmapConfig) {
        SSLUtilities.trustAllHostnames();
        SSLUtilities.trustAllHttpsCertificates();

        String url =  weedmapConfig.getHost() + WEEDMAP_V2_API_URL + "/listings/" + apiKeyMap.getListingWmId().trim() + "/catalog_id";

        MultivaluedMap<String, Object> headers = getHeaders(apiKeyMap.getToken());
        headers.putSingle("Content-Type", MediaType.APPLICATION_JSON);
        headers.putSingle("accept", "*/*");



        String weedmapTokenResponse = null;
        try {
            weedmapTokenResponse = SimpleRestUtil.get(url, String.class, headers);
        } catch (Exception e) {
            LOGGER.error(String.format("Error in calling url: %s with error message: %s", url, e.getMessage()), e);
        }
        return weedmapTokenResponse;
    }

    @Override
    public WmSyncRequest getCatalogById(String companyId, String shopId,  String catalogId, WeedmapApiKeyMap apiKeyMap, WeedmapConfig weedmapConfig) {
        SSLUtilities.trustAllHostnames();
        SSLUtilities.trustAllHttpsCertificates();

        String url = createV2Url(WmSyncItems.WMItemType.CATALOG, catalogId, weedmapConfig);

        MultivaluedMap<String, Object> headers = getHeaders(apiKeyMap.getToken());
        headers.putSingle("Content-Type", WEEDMAP_MEDIATYPE);


        MultivaluedHashMap<String, Object> queryParams = new MultivaluedHashMap<>();
        queryParams.putSingle("scope_by[catalog_id]", catalogId);


        WmSyncRequest weedmapTokenResponse = null;
        try {
            weedmapTokenResponse = SimpleRestUtil.get(url, WmSyncRequest.class, queryParams,headers);
        } catch (Exception e) {
            LOGGER.error(String.format("Error in calling url: %s with error message: %s", url, e.getMessage()), e);
        }
        return weedmapTokenResponse;
    }

    /**
     * Override method to link product tags
     *
     * @param companyId      :  companyId
     * @param shopId         :  shopId
     * @param organizationId :  organizationId
     * @param apiKeyMap      :  apiKeyMap
     * @param request        :  request
     * @param wmItemId       :  wmItemId
     * @param weedmapConfig
     * @return : true/false
     */
    @Override
    public boolean linkProductTags(String companyId, String shopId, String organizationId, WeedmapApiKeyMap apiKeyMap, LinkTagsRequest request, String wmItemId, WeedmapConfig weedmapConfig) {
        String url = createV2Url(WmSyncItems.WMItemType.LINKED_TAGS, wmItemId, weedmapConfig);

        String jsonBody = JsonSerializer.toJson(request);

        HashMap<String, Object> queryParams = new HashMap<>();
        queryParams.putIfAbsent("scope_by[organization_id]", organizationId);

        MultivaluedMap<String, Object> headers = getHeaders(apiKeyMap.getToken());

        LinkTagsRequest response = callWeedmapApi(companyId, shopId, url, headers, queryParams, jsonBody, LinkTagsRequest.class, false);
        return (response != null);
    }

    /**
     * Override method to delete WmItem
     *
     * @param <T>            :  clazz
     * @param companyId      :  companyId
     * @param shopId         :  shopId
     * @param organizationId :  organizationId
     * @param apiKey         :  apiKey
     * @param type           :  type
     * @param wmItemId       :  wmItemId
     * @param request        :  request
     * @param weedmapConfig
     * @return : true/false
     */
    @Override
    public <T extends Object> boolean deleteWmItem(String companyId, String shopId, String organizationId, WeedmapApiKeyMap apiKey, WmSyncItems.WMItemType type, String wmItemId, T request, WeedmapConfig weedmapConfig) {
        SSLUtilities.trustAllHostnames();
        SSLUtilities.trustAllHttpsCertificates();

        String url = createV2Url(type, wmItemId, weedmapConfig);

        MultivaluedMap<String, Object> headers = getHeaders(apiKey.getToken());

        HashMap<String, Object> queryParams = new HashMap<>();
        queryParams.putIfAbsent("scope_by[organization_id]", organizationId);
        String body = "";
        try {
            LOGGER.info(String.format("Start calling api %s with type: %s", url, type));
            Response response;
            if (request != null) {
                body = JsonSerializer.toJson(request);
                response = SimpleRestUtil.deleteWithSsl(url, body, headers, queryParams, WEEDMAP_MEDIATYPE, WmSyncRequest.WmErrorCode.NO_CONTENT.code);
            } else {
                response = SimpleRestUtil.deleteWithSsl(url, body, headers, queryParams, WEEDMAP_MEDIATYPE, WmSyncRequest.WmErrorCode.SUCCESS.code);
            }
            LOGGER.info(String.format("End calling api %s with type: %s %s", url, type, response.getStatus() + "  " + response.getStatusInfo()));

            return true;
        } catch (Exception e) {
            LOGGER.error(String.format("Error in calling url: %s with error message: %s", url, e.getMessage()), e);
        }

        return false;
    }

    /**
     * Override method to get weedmap tags
     *
     * @param companyId      :  companyId
     * @param shopId         :  shopId
     * @param organizationId :  organizationId
     * @param apiKeyMap      :  apiKeyMap
     * @param weedmapConfig
     * @return : response
     */
    @Override
    public WmTagsResponse getWeedmapTags(String companyId, String shopId, String organizationId, WeedmapApiKeyMap apiKeyMap, WeedmapConfig weedmapConfig) {
        SSLUtilities.trustAllHostnames();
        SSLUtilities.trustAllHttpsCertificates();

        String url = createV2Url(WmSyncItems.WMItemType.TAGS, "", weedmapConfig);

        MultivaluedMap<String, Object> headers = getHeaders(apiKeyMap.getToken());

        HashMap<String, Object> queryParams = new HashMap<>();
        queryParams.putIfAbsent("scope_by[organization_id]", organizationId);
       // queryParams.putIfAbsent("filter[is_category]", false);
        queryParams.putIfAbsent("page[size]", Integer.MAX_VALUE);
        queryParams.putIfAbsent("page[page]", 1);


        try {
            return SimpleRestUtil.getWithSsl(url, WmTagsResponse.class, headers, queryParams, WEEDMAP_MEDIATYPE);
        } catch (Exception e) {
            LOGGER.error(String.format("Error in calling url: %s with error message: %s", url, e.getMessage()), e);
        }

        return null;
    }

    /**
     * Override method to get wm attributes with values
     *
     * @param companyId      :  companyId
     * @param shopId         :  shopId
     * @param organizationId :  organizationId
     * @param apiKeyMap      :  apiKeyMap
     * @param weedmapConfig
     * @return : response
     */
    @Override
    public WmAttributeResponse getVariantAttributes(String companyId, String shopId, String organizationId, WeedmapApiKeyMap apiKeyMap, WeedmapConfig weedmapConfig) {
        SSLUtilities.trustAllHostnames();
        SSLUtilities.trustAllHttpsCertificates();

        String url = createV2Url(WmSyncItems.WMItemType.VARIANT_ATTRIBUTES, "", weedmapConfig);

        MultivaluedMap<String, Object> headers = getHeaders(apiKeyMap.getToken());

        HashMap<String, Object> queryParams = new HashMap<>();
        queryParams.putIfAbsent("scope_by[organization_id]", organizationId);
        queryParams.putIfAbsent("filter[name]", "Weight Breakpoint");
        queryParams.putIfAbsent("page[size]", Integer.MAX_VALUE);
        queryParams.putIfAbsent("page[page]", 1);
        queryParams.putIfAbsent("include", "variant_values");


        try {
            String response = SimpleRestUtil.getWithSsl(url, String.class, headers, queryParams, "application/vnd.api+json");
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
            JsonNode jsonNode = objectMapper.readValue(response, JsonNode.class);

            JsonNode newNode = objectMapper.createObjectNode();
            jsonNode.at("/data").forEach(node -> {
                ((ObjectNode) newNode).set("id", node.at("/id"));
                //    ((ObjectNode) newNode).set("variants", node.at("/relationships/variant_values"));
            });

            JsonNode variantValues = objectMapper.createObjectNode();
            jsonNode.at("/included").forEach(node -> {
                String value = node.at("/attributes/value").textValue();
                if (StringUtils.isNotBlank(value) &&
                        (value.equalsIgnoreCase("0.5 g") ||
                                value.equalsIgnoreCase("1 g") ||
                                value.equalsIgnoreCase("2 g") ||
                                value.equalsIgnoreCase("1/4") ||
                                value.equalsIgnoreCase("1/2") ||
                                value.equalsIgnoreCase("1/8") ||
                                value.equalsIgnoreCase("CUSTOM") ||
                                value.equalsIgnoreCase("Ounce"))) {
                    value = value.equalsIgnoreCase("0.5 g") ? ".5 g" : value;
                    ((ObjectNode) variantValues).set(value, node.at("/id"));
                }
            });
            ((ObjectNode) newNode).set("variant_values", variantValues);
            return objectMapper.readValue(newNode.toString(), WmAttributeResponse.class);


        } catch (Exception e) {
            LOGGER.error(String.format("Error in calling url: %s with error message: %s", url, e.getMessage()), e);
        }

        return null;
    }

    /**
     * Override method to get wm products
     *
     * @param companyId      : companyId
     * @param shopId         : shopId
     * @param organizationId : organizationId
     * @param weedmapConfig  : weedmapConfig
     * @param apiKeyMap      : apiKeyMap
     * @return
     */
    @Override
    public HashMap<String, WmSyncMenuDetails> getWeedmapMenu(String companyId, String shopId, String organizationId, WeedmapConfig weedmapConfig, WeedmapApiKeyMap apiKeyMap) {
        HashMap<String, WmSyncMenuDetails> wmCustomBrands = getwmBrands(companyId, shopId, organizationId, weedmapConfig, apiKeyMap, "", false, "");
        HashMap<String, WmSyncMenuDetails.WmSyncProductDetails> wmCustomProducts = getWmProducts(companyId, shopId, organizationId, weedmapConfig, apiKeyMap, false, "", "", "");
        HashMap<String, String> batches = getWmBatches(companyId, shopId, organizationId, apiKeyMap, weedmapConfig);

        wmCustomBrands.forEach((key, details) -> {
            details.getProductDetail().forEach((productId, wmSyncProductDetails) -> {
                if (wmCustomProducts.containsKey(productId)) {
                    WmSyncMenuDetails.WmSyncProductDetails productDetails = wmCustomProducts.get(productId);
                    productDetails.setBatchId(batches.get(productId));
                    details.getProductDetail().put(productId, productDetails);
                }
            });
        });

        return wmCustomBrands;
    }

    @Override
    public boolean validateTags(String companyId, String shopId, String organizationId, LinkTagsRequest linkTagsRequest, WeedmapApiKeyMap apiKeyMap, WeedmapConfig weedmapConfig) {
        String url = weedmapConfig.getHost() + WEEDMAP_V2_API_URL + "/products/relationships/tags:validate";

        MultivaluedMap<String, Object> headers = getHeaders(apiKeyMap.getToken());

        try {
            String body = JsonSerializer.toJson(linkTagsRequest);
            SimpleRestUtil.postWithSsl(url, body, String.class, headers, new HashMap<>(), WEEDMAP_MEDIATYPE, WmSyncRequest.WmErrorCode.NO_CONTENT.code);
            return true;
        } catch (RestErrorException e) {
            LOGGER.info(String.format("Error in validate api calling : CompanyId: %s shopId %s", companyId, shopId));
        }
        return false;
    }

    /**
     * Override method to get verified products
     *
     * @param companyId      : companyId
     * @param shopId         : shopId
     * @param organizationId : organizationId
     * @param weedmapConfig  : weedmapConfig
     * @param apiKeyMap      : apiKeyMap
     * @param term           : term
     * @return
     */
    @Override
    public List<ThirdPartyBrand> getWmVerifiedBrands(String companyId, String shopId, String organizationId, WeedmapConfig weedmapConfig, WeedmapApiKeyMap apiKeyMap, String term) {
        HashMap<String, WmSyncMenuDetails> wmVerifiedBrands = getwmBrands(companyId, shopId, organizationId, weedmapConfig, apiKeyMap, term, true, "");

        List<ThirdPartyBrand> thirdPartyBrands = new ArrayList<>();
        wmVerifiedBrands.forEach((s, wmSyncMenuDetails) -> {
            ThirdPartyBrand thirdPartyBrand = new ThirdPartyBrand();
            thirdPartyBrand.prepare();
            thirdPartyBrand.setName(wmSyncMenuDetails.getName());
            thirdPartyBrand.setSourceBrandId(wmSyncMenuDetails.getId());
            thirdPartyBrand.setSource(ThirdPartyProduct.ThirdPartySource.Weedmap);
            thirdPartyBrand.setSlug((new Slugify()).slugify(wmSyncMenuDetails.getName().replaceAll("[&#$%^-_+=]", "").trim()));
            thirdPartyBrands.add(thirdPartyBrand);
        });

        return thirdPartyBrands;
    }

    /**
     * Override method to get wm verified products
     *
     * @param companyId      : companyId
     * @param shopId         : shopId
     * @param organizationId : organizationId
     * @param config         : config
     * @param apiKeyMap      : apiKeyMap
     * @param brandId        : brandId
     * @param term           : term
     * @return
     */
    @Override
    public List<ThirdPartyProduct> getWmVerifiedProducts(String companyId, String shopId, String organizationId, WeedmapConfig config, WeedmapApiKeyMap apiKeyMap, String brandId, String term) {
        HashMap<String, WmSyncMenuDetails> wmBrands = getwmBrands(companyId, shopId, organizationId, config, apiKeyMap, "", true, brandId);
        if (wmBrands.isEmpty()) {
            return Lists.newArrayList();
        }
        HashMap<String, WmSyncMenuDetails.WmSyncProductDetails> wmProducts = getWmProducts(companyId, shopId, organizationId, config, apiKeyMap, true, brandId, term, "");

        List<ThirdPartyProduct> itemList = new ArrayList<>();
        wmProducts.forEach((key, wmSyncProductDetails) -> {
            WmSyncMenuDetails brandDetail = wmBrands.get(wmSyncProductDetails.getBrandId());
            if (brandDetail == null) {
                return;
            }
            String slug = (new Slugify()).slugify(wmSyncProductDetails.getName().replaceAll("[&#$%^-_+=]", "").trim());
            ThirdPartyProduct product = new ThirdPartyProduct();
            product.prepare();
            product.setName(wmSyncProductDetails.getName());
            product.setSlug(slug);
            product.setSourceProductId(wmSyncProductDetails.getId());
            product.setSourceSku(wmSyncProductDetails.getWmsin());
            int i = 0;
            for (HashMap<String, Object> map : wmSyncProductDetails.getVariantDetail()) {
                ThirdPartyProduct.ThirdPartyVariant variant = new ThirdPartyProduct.ThirdPartyVariant();
                variant.setSoldByWeight((Boolean) map.get("sold_by_weight"));
                variant.setSourceVariantId(String.valueOf(map.get("id")));
                if (wmSyncProductDetails.getVariantOptions().size() > i) {
                    variant.setSourceOptionId(wmSyncProductDetails.getVariantOptions().get(i++));
                }
                if (CollectionUtils.isEmpty(product.getVariantDetail())) {
                    product.setVariantDetail(new ArrayList<>());
                }
                variant.setSourceValueId(wmSyncProductDetails.getVariantValuesDetail().get(variant.getSourceVariantId()));
                product.setSoldByWeight(variant.isSoldByWeight());

                product.getVariantDetail().add(variant);
            }

            product.setThirdPartyBrandId(brandDetail.getId());
            product.setThirdPartyBrandName(brandDetail.getName());
            itemList.add(product);
        });
        return itemList;
    }

    /**
     * Private method to get batches for wm products
     *
     * @param companyId      : companyId
     * @param shopId         : shopId
     * @param organizationId : organizationId
     * @param apiKeyMap      : apiKeyMap
     * @param weedmapConfig  : weedmapConfig
     * @return
     */
    private HashMap<String, String> getWmBatches(String companyId, String shopId, String organizationId, WeedmapApiKeyMap apiKeyMap, WeedmapConfig weedmapConfig) {
        String url = createV2Url(WmSyncItems.WMItemType.BATCHES, null, weedmapConfig);

        HashMap<String, String> productBatchMap = new HashMap<>();

        HashMap<String, Object> params = new HashMap<>();
        params.put("scope_by[organization_id]", organizationId);
        MultivaluedMap<String, Object> headers = getHeaders(apiKeyMap.getToken());
        try {
            long count = 1;
            long totalPages = 0;
            WmBulkResponse.WmMetaResult wmMetaResult = null;
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
            do {
                params.put("page[page]", count);
                params.put("page[size]", FETCH_LIMIT);

                String response = SimpleRestUtil.getWithSsl(url, String.class, headers, params, WEEDMAP_MEDIATYPE);

                JsonNode jsonNode = objectMapper.readValue(response, JsonNode.class);

                jsonNode.at("/data").forEach(node -> {
                    String id = node.at("/id").textValue();
                    if (StringUtils.isBlank(id)) {
                        return;
                    }
                    String pId = node.at("/relationships").at("/product/data/id").textValue();
                    productBatchMap.put(pId, id);

                });

                if (wmMetaResult == null) {
                    wmMetaResult = objectMapper.readValue(jsonNode.at("/meta").toString(), WmBulkResponse.WmMetaResult.class);
                }
                if (wmMetaResult != null && totalPages == 0) {
                    totalPages = wmMetaResult.getTotalPages();
                }
                LOGGER.info("fetching verified items for weedmap : page : " + count + ", total pages : " + totalPages);
                count++;
            } while (count <= totalPages);

        } catch (Exception e) {
            LOGGER.error(String.format("Error in api calling : CompanyId: %s shopId %s", companyId, shopId), e);
        }
        return productBatchMap;
    }

    /**
     * Private method to parse product response
     *
     * @param objectMapper    : objectMapper
     * @param jsonNode        : jsonNode
     * @param isVerified      : isVerified
     * @param productDetails  : productDetails
     */
    private void parseWmProductResponse(ObjectMapper objectMapper, JsonNode jsonNode, boolean isVerified, HashMap<String, WmSyncMenuDetails.WmSyncProductDetails> productDetails) {

        HashMap<String, String> variantProductList = new HashMap<>();
        HashMap<String, String> optionVariantMap = new HashMap<>();
        HashMap<String, String> variantValueMap = new HashMap<>();

        jsonNode.at("/data").forEach(node -> {
            String id = node.at("/id").textValue();
            String name = node.at("/attributes/name").textValue();
            String wmsin = node.at("/attributes/wmsin").textValue();
            if (StringUtils.isBlank(id)) {
                return;
            }

            WmSyncMenuDetails.WmSyncProductDetails wmSyncProductDetails = new WmSyncMenuDetails.WmSyncProductDetails();
            wmSyncProductDetails.setId(id);
            wmSyncProductDetails.setBrandId(node.at("/relationships/brand/data/id").textValue());
            wmSyncProductDetails.setName(name);
            wmSyncProductDetails.setWmsin(wmsin);

            node.at("/relationships").forEach(jsonNode1 -> {
                if (jsonNode1.has("data")) {
                    jsonNode1.at("/data").forEach(jsonNode2 -> {
                        String nType = jsonNode2.at("/type").textValue();
                        if (StringUtils.isBlank(nType)) {
                            return;
                        }
                        String nId = jsonNode2.at("/id").textValue();
                        switch (nType) {
                            case "product_images":
                                wmSyncProductDetails.getImages().add(nId);
                                break;
                            case "catalog_items":
                                wmSyncProductDetails.getCatalogItems().add(nId);
                                break;
                            case "product_variants":
                                variantProductList.put(nId, id);
                                wmSyncProductDetails.getVariants().add(nId);
                                break;
                            case "tags":
                                wmSyncProductDetails.getTags().add(nId);
                                break;
                        }
                    });
                }
            });

            productDetails.put(id, wmSyncProductDetails);
        });

        jsonNode.at("/included").forEach(node -> {
            String type = node.at("/type").textValue();
            String id = node.at("/id").textValue();
            if (WmSyncItems.WMItemType.VARIANT_VALUES.getWeedmapType().equalsIgnoreCase(type)) {
                variantValueMap.put(id, node.at("/attributes/value").textValue());
            }
        });
        jsonNode.at("/included").forEach(node -> {
            String id = node.at("/id").textValue();
            String type = node.at("/type").textValue();
            if (WmSyncItems.WMItemType.PRODUCT_VARIANTS.name().equalsIgnoreCase(type)) {
                String productId = variantProductList.get(id);
                if (StringUtils.isBlank(productId) || !productDetails.containsKey(productId)) {
                    return;
                }
                List<String> array = Arrays.asList(node.at("/attributes/sku").textValue().split("\\s*-\\s*"));

                WmSyncMenuDetails.WmSyncProductDetails wmSyncProductDetails = productDetails.get(productId);
                wmSyncProductDetails.setSku(array.get(0));
                WmSyncMenuDetails.WmProductVariantDetails wmProductVariantDetails = new WmSyncMenuDetails.WmProductVariantDetails();
                wmProductVariantDetails.setVariantId(id);
                if (array.size() == 3) {
                    wmSyncProductDetails.setBlazeId(array.get(2));
                }
                if (array.size() >= 2) {
                    wmSyncProductDetails.getWeightVariantDetailsMap().putIfAbsent(array.get(1), wmProductVariantDetails);
                }

                if (isVerified) {
                    HashMap<String, Object> variantDetailMap = objectMapper.convertValue(node.at("/attributes"), new TypeReference<Map<String, Object>>() {
                    });
                    variantDetailMap.putIfAbsent("id", id);
                    wmSyncProductDetails.getVariantDetail().add(variantDetailMap);
                    boolean soldByWeight = (Boolean) variantDetailMap.get("sold_by_weight");
                    if (!soldByWeight) {
                        wmSyncProductDetails.getWeightVariantDetailsMap().putIfAbsent("Each", wmProductVariantDetails);
                    } else {

                    }
                }
                node.at("/relationships").forEach(jsonNode1 -> {
                    if (jsonNode1.has("data")) {
                        jsonNode1.at("/data").forEach(jsonNode2 -> {
                            String nType = jsonNode2.at("/type").textValue();
                            String nId = jsonNode2.at("/id").textValue();
                            switch (nType) {
                                case "catalog_items":
                                    wmSyncProductDetails.getCatalogItems().add(nId);
                                    wmProductVariantDetails.setCatalogId(nId);
                                    break;
                                case "product_variant_options":
                                    optionVariantMap.put(nId, id);
                                    wmProductVariantDetails.setOptionId(nId);
                                    wmSyncProductDetails.getVariantOptions().add(nId);
                                    break;
                            }
                        });
                    }
                });
            }
        });
        if (isVerified) {
            jsonNode.at("/included").forEach(node -> {
                String id = node.at("/id").textValue();
                String type = node.at("/type").textValue();
                if (!WmSyncItems.WMItemType.VARIANT_OPTIONS.getWeedmapType().equalsIgnoreCase(type)) {
                    return;
                }
                String variantId = optionVariantMap.get(id);
                if (StringUtils.isBlank(variantId)) {
                    return;
                }
                String productId = variantProductList.get(variantId);
                if (StringUtils.isBlank(productId) || !productDetails.containsKey(productId)) {
                    return;
                }
                WmSyncMenuDetails.WmSyncProductDetails wmSyncProductDetails = productDetails.get(productId);

                node.at("/relationships").forEach(jsonNode1 -> {
                    if (jsonNode1.has("data")) {
                        String nType = jsonNode1.at("/data/type").textValue();
                        String nId = jsonNode1.at("/data/id").textValue();
                        switch (nType) {
                            case "variant_values":
                                wmSyncProductDetails.getVariantValuesDetail().put(variantId, variantValueMap.get(nId));
                                break;
                        }
                    }
                });

            });
        }
    }

    /**
     * Private method to get wm product details
     *
     * @param companyId      : companyId
     * @param shopId         : shopId
     * @param organizationId : organizationId
     * @param config         : config
     * @param apiKeyMap      : apiKeyMap
     * @param isVerified     : isVerified
     * @param brandId        : brandId
     * @param term           : term
     * @return
     */
    private HashMap<String, WmSyncMenuDetails.WmSyncProductDetails> getWmProducts(String companyId, String shopId, String organizationId, WeedmapConfig config, WeedmapApiKeyMap apiKeyMap, boolean isVerified, String brandId, String term, String productId) {
        HashMap<String, WmSyncMenuDetails.WmSyncProductDetails> productDetails = new HashMap<>();

        String url = createV2Url(WmSyncItems.WMItemType.PRODUCTS, productId, config);

        HashMap<String, Object> params = new HashMap<>();
        params.put("scope_by[organization_id]", organizationId);
        params.put("filter[is_verified]", isVerified);
        params.put("include", "product_images,product_variants,product_variants.product_variant_options.variant_attribute,product_variants.product_variant_options.variant_attribute,product_variants.product_variant_options.variant_value,product_variants.catalog_items,tags");
        if (StringUtils.isNotBlank(brandId) && isVerified && StringUtils.isBlank(productId)) {
            params.put("filter[brand_id]", brandId);
        }
        if (StringUtils.isNotBlank(term)) {
            params.put("filter[name]", term);
        }

        MultivaluedMap<String, Object> headers = getHeaders(apiKeyMap.getToken());
        try {
            long totalPages = 0;
            long count = 1;
            WmBulkResponse.WmMetaResult wmMetaResult = null;
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
            do {
                params.put("page[size]", FETCH_LIMIT);
                params.put("page[page]", count);

                String response = SimpleRestUtil.getWithSsl(url, String.class, headers, params, WEEDMAP_MEDIATYPE);

                JsonNode jsonNode = objectMapper.readValue(response, JsonNode.class);

                if (StringUtils.isNotBlank(productId)) {
                    ObjectNode objectNode = objectMapper.createObjectNode();
                    ArrayNode arrayNode = objectMapper.createArrayNode();
                    arrayNode.add(jsonNode.at("/data"));
                    objectNode.set("data", arrayNode);
                    objectNode.set("included", jsonNode.at("/included"));
                    String metaStr = objectMapper.writeValueAsString(new WmBulkResponse.WmMetaResult());
                    objectNode.set("meta", objectMapper.readTree(metaStr));
                    jsonNode = objectNode;
                }
                parseWmProductResponse(objectMapper, jsonNode, isVerified, productDetails);

                if (wmMetaResult == null) {
                    wmMetaResult = objectMapper.readValue(jsonNode.at("/meta").toString(), WmBulkResponse.WmMetaResult.class);
                }
                if (wmMetaResult != null && totalPages == 0) {
                    totalPages = wmMetaResult.getTotalPages();
                }
                LOGGER.info("fetching non verified items for weedmap : page : " + count + ", total pages : " + totalPages);
                count++;
            } while (count <= totalPages);
        } catch (Exception e) {
            LOGGER.error(String.format("Error in api calling : CompanyId: %s shopId %s", companyId, shopId), e);
        }
        return productDetails;
    }

    /**
     * Private method to get wm brands
     *
     * @param companyId      : companyId
     * @param shopId         : shopId
     * @param organizationId : organizationId
     * @param weedmapConfig  : weedmapConfig
     * @param apiKeyMap      : apiKeyMap
     * @param term           : term
     * @param isVerified     : isVerified
     * @param brandId        : brandId
     * @return
     */
    private HashMap<String, WmSyncMenuDetails> getwmBrands(String companyId, String shopId, String organizationId, WeedmapConfig weedmapConfig, WeedmapApiKeyMap apiKeyMap, String term, boolean isVerified, String brandId) {
        HashMap<String, WmSyncMenuDetails> result = new HashMap<>();
        String url = createV2Url(WmSyncItems.WMItemType.BRANDS, brandId, weedmapConfig);

        HashMap<String, Object> params = new HashMap<>();
        params.put("scope_by[organization_id]", organizationId);
        params.put("filter[is_verified]", isVerified);
        if (StringUtils.isNotBlank(term)) {
            params.put("filter[name]", term);
        }

        MultivaluedMap<String, Object> headers = getHeaders(apiKeyMap.getToken());
        try {
            long count = 1;
            long totalPages = 0;
            WmBulkResponse.WmMetaResult wmMetaResult = null;
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
            do {
                params.put("page[page]", count);
                params.put("page[size]", FETCH_LIMIT);

                String response = SimpleRestUtil.getWithSsl(url, String.class, headers, params, WEEDMAP_MEDIATYPE);

                JsonNode jsonNode = objectMapper.readValue(response, JsonNode.class);

                if (StringUtils.isNotBlank(brandId)) {
                    prepareWmBrands(jsonNode.at("/data"), result, isVerified);
                } else {
                    jsonNode.at("/data").forEach(node -> {
                        prepareWmBrands(node, result, isVerified);
                    });
                    if (wmMetaResult == null) {
                        wmMetaResult = objectMapper.readValue(jsonNode.at("/meta").toString(), WmBulkResponse.WmMetaResult.class);
                    }
                    if (wmMetaResult != null && totalPages == 0) {
                        totalPages = wmMetaResult.getTotalPages();
                    }
                }
                LOGGER.info(String.format("Fetching %s verified items for weedmap : page : %s, total pages : %s", isVerified ? "" : "non - ", count, totalPages));
                count++;
            } while (count <= totalPages);
        } catch (Exception e) {
            LOGGER.error(String.format("Error in api calling : CompanyId: %s shopId %s", companyId, shopId), e);
        }
        return result;
    }

    /**
     * Private method to prepare brands
     *
     * @param node       : node
     * @param result     : result
     * @param isVerified : isVerified
     */
    private void prepareWmBrands(JsonNode node, HashMap<String, WmSyncMenuDetails> result, boolean isVerified) {
        String name = node.at("/attributes/name").textValue();
        String id = node.at("/id").textValue();
        if (StringUtils.isBlank(id)) {
            return;
        }
        String key = isVerified ? id : name; //new different identifications
        result.putIfAbsent(key, new WmSyncMenuDetails());

        WmSyncMenuDetails details = result.get(key);
        details.setId(id);
        details.setName(name);
        node.at("/relationships").at("/products").at("/data").forEach(jsonNode1 -> {
            String pId = jsonNode1.at("/id").textValue();
            details.getProductDetail().putIfAbsent(pId, new WmSyncMenuDetails.WmSyncProductDetails());
            WmSyncMenuDetails.WmSyncProductDetails wmSyncProductDetails = details.getProductDetail().get(pId);
            wmSyncProductDetails.setBrandId(id);
            wmSyncProductDetails.setId(pId);
        });

    }

    /**
     * Override method to sync items
     *
     * @param <T>            :  <T>
     * @param companyId      :  companyId
     * @param shopId         :  shopId
     * @param organisationId :  organisationId
     * @param apiKeyMap      :  apiKeyMap
     * @param request        :  request
     * @param clazz          :  clazz
     * @param weedmapConfig
     * @return
     */
    @Override
    public <T extends Object> T syncInventory(String companyId, String shopId, String organisationId, String catalogItemId, WeedmapApiKeyMap apiKeyMap, WmSyncRequest request, Class<T> clazz, WeedmapConfig weedmapConfig) {
        String url = createV2Url(WmSyncItems.WMItemType.INVENTORY, catalogItemId, weedmapConfig);

        MultivaluedMap<String, Object> headers = getHeaders(apiKeyMap.getToken());
        headers.remove("Accept");
        String jsonBody = JsonSerializer.toJson(request);

        HashMap<String, Object> queryParams = getQueryParams(organisationId);

        LOGGER.info(String.format("Calling start api: %s", url));

        SSLUtilities.trustAllHostnames();
        SSLUtilities.trustAllHttpsCertificates();
        T responseClazz;
        try {
            String response = SimpleRestUtil.putWithSsl(url, jsonBody, String.class, headers, queryParams, WEEDMAP_MEDIATYPE, WmSyncRequest.WmErrorCode.CREATED.code);
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
            responseClazz = objectMapper.readValue(response, clazz);
            LOGGER.info(String.format("Calling end api: %s", url));
        } catch (RestErrorException error) {
            LOGGER.error(String.format("Error in calling url: %s with error message: %s", url, error.getErrorResponse()), error);
            responseClazz = null;
        } catch (Exception e) {
            LOGGER.error(String.format("Error in calling url: %s with error message: %s", url, e.getMessage()), e);
            responseClazz = null;
        }
        return responseClazz;
    }

    @Override
    public WmSyncMenuDetails.WmSyncProductDetails getWmVerifiedProducts(String companyId, String shopId, String organizationId, WeedmapConfig weedmapConfig, WeedmapApiKeyMap apiKeyMap, String sourceProductId) {
        HashMap<String, WmSyncMenuDetails.WmSyncProductDetails> wmProducts = getWmProducts(companyId, shopId, organizationId, weedmapConfig, apiKeyMap, true, "", "", sourceProductId);
        return wmProducts.get(sourceProductId);
    }

    @Override
    public WmSyncMenuDetails.WmSyncProductDetails getWmCustomProduct(String companyId, String shopId, String organizationId, WeedmapConfig weedmapConfig, WeedmapApiKeyMap apiKeyMap, String sourceProductId) {
        HashMap<String, WmSyncMenuDetails.WmSyncProductDetails> wmProducts = getWmProducts(companyId, shopId, organizationId, weedmapConfig, apiKeyMap, false, "", "", sourceProductId);
        return wmProducts.get(sourceProductId);
    }

    @Override
    public List<WmTagGroups> getWmTagGroups(String companyId, String shopId, String organizationId, WeedmapConfig config, WeedmapApiKeyMap apiKeyMap) {
        String url = createV2Url(WmSyncItems.WMItemType.TAGS_GROUPS, null, config);

        MultivaluedMap<String, Object> headers = getHeaders(apiKeyMap.getToken());

        HashMap<String, Object> params = new HashMap<>();
        params.put("scope_by[organization_id]", organizationId);
        params.put("filter[is_category_group]", false);
        params.put("include", "tags");

        HashMap<String, WmTagGroups> wmTagGroupsMap = new HashMap<>();
        try {
            String res = SimpleRestUtil.getWithSsl(url, String.class, headers, params, "application/vnd.api+json");
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
            JsonNode jsonNode = objectMapper.readValue(res, JsonNode.class);


            jsonNode.at("/data").forEach(node -> {
                WmTagGroups group = new WmTagGroups();
                group.setTagGroupId(node.at("/id").textValue());
                group.setTagGroupName(node.at("/attributes/name").textValue());
                group.prepare();
                node.at("/relationships/tags/data").forEach(node1 -> {
                    WmTagGroups.WmDiscoveryTags discoveryTags = new WmTagGroups.WmDiscoveryTags();
                    String id = node1.at("/id").textValue();
                    discoveryTags.setTagId(id);
                    jsonNode.at("/included").forEach(node2 -> {
                        String name = node2.at("/attributes/name").textValue();
                        String tagId = node2.at("/id").textValue();
                        if (tagId.equalsIgnoreCase(id)) {
                            discoveryTags.setTagName(name);
                            discoveryTags.prepare();
                            group.getDiscoveryTags().add(discoveryTags);
                        }
                    });

                    wmTagGroupsMap.put(group.getTagGroupId(), group);

                });
            });
        } catch (Exception e) {
            LOGGER.error(String.format("Error in api calling for url: %s , type: %s, message: %s", url, WmSyncItems.WMItemType.TAGS_GROUPS, e.getMessage()), e);
        }
        return Lists.newArrayList(wmTagGroupsMap.values());
    }
}