package com.fourtwenty.core.util;

import com.amazonaws.Request;
import com.amazonaws.auth.AWS4Signer;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceAsync;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceAsyncClientBuilder;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClientBuilder;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSAsyncClient;
import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.config.S3Config;

/**
 * The AmazonServiceFactory object.
 * User: mdo
 * Date: 11/1/13
 * Time: 10:30 PM
 */
public class AmazonServiceFactory {
    private final ConnectConfiguration config;
    private final AmazonS3 s3Client;
    private final AmazonSimpleEmailServiceAsync sesClient;
    private final AmazonSQS sqsClient;
    private final AmazonSNS snsClient;
    private final AmazonRequestSigner amazonRequestSigner;

    public AmazonServiceFactory(ConnectConfiguration configuration) {
        this.config = configuration;


        AWSStaticCredentialsProvider credentials = new AWSStaticCredentialsProvider(
                new BasicAWSCredentials(config.getAmazonS3Config().getApiKey(),
                        config.getAmazonS3Config().getApiSecret()));
        s3Client = AmazonS3ClientBuilder.standard()
                .withRegion(Regions.US_EAST_1)
                .withCredentials(credentials).build();  //new AmazonS3Client(credentials);
        sesClient = AmazonSimpleEmailServiceAsyncClientBuilder
                .standard()
                .withRegion(Regions.US_EAST_1)
                .withCredentials(credentials).build(); // new AmazonSimpleEmailServiceAsyncClient(credentials);


        snsClient = AmazonSNSClientBuilder.standard()
                .withRegion(Regions.US_EAST_1).withCredentials(credentials).build();
        sqsClient = AmazonSQSAsyncClient.asyncBuilder()
                .withRegion(Regions.US_EAST_1).withCredentials(credentials).build();

        // AWSStaticCredentialsProvider Can be replaced with current S3 Credentials
        // This is dev credentials
        amazonRequestSigner = new AmazonRequestSigner(configuration, new AWSStaticCredentialsProvider(
                new BasicAWSCredentials(config.getAmazonS3Config().getApiKey(),
                        config.getAmazonS3Config().getApiSecret())));

    }


    public S3Config getS3Config() {
        return config.getAmazonS3Config();
    }

    public AmazonS3 getS3Client() {
        return s3Client;
    }

    public AmazonSNS getSNSClient() {
        return snsClient;
    }

    public AmazonSQS getSQSClient() {
        return sqsClient;
    }

    public AmazonSimpleEmailServiceAsync getSESClient() {
        return sesClient;
    }

    public AmazonRequestSigner getAmazonRequestSigner() {
        return amazonRequestSigner;
    }

    public static class AmazonRequestSigner {
        private final AWS4Signer signer;
        private final ConnectConfiguration connectConfiguration;
        private AWSCredentialsProvider credentials;

        AmazonRequestSigner(final ConnectConfiguration connectConfiguration, AWSCredentialsProvider credentials) {
            this.connectConfiguration = connectConfiguration;
            this.credentials = credentials;
            signer = new AWS4Signer();
            signer.setServiceName(this.connectConfiguration.getAmazonS3Config().getServiceName());
            signer.setRegionName(this.connectConfiguration.getAmazonS3Config().getRegion());
        }

        public void signRequest(final Request request) {
            signer.sign(request, credentials.getCredentials());
        }

    }

}
