package com.fourtwenty.core.util;

public class BLStringUtils {
    public static String stripNonAlphaNumericChars(String str) {
        if (str == null) return "";

        return str.replaceAll("[^A-Za-z0-9 @._]", "");
    }

    public static String cleanPhoneNumber(String str) {
        if (str == null) return "";

        return str.replaceAll("[^0-9]", "");
    }

    public static String splitCamelCase(String s) {
        return s.replaceAll(
                String.format("%s|%s|%s",
                        "(?<=[A-Z])(?=[A-Z][a-z])",
                        "(?<=[^A-Z])(?=[A-Z])",
                        "(?<=[A-Za-z])(?=[^A-Za-z])"
                ),
                " "
        );
    }

    public static String appendDQ(String str) {
        return "\"" + str + "\"";
    }
}
