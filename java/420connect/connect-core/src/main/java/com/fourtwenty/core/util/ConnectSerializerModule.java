package com.fourtwenty.core.util;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.bson.types.ObjectId;

import java.io.IOException;

/**
 * The FCSerializerModule object.
 * User: mdo
 * Date: 9/8/13
 * Time: 2:13 PM
 */
public class ConnectSerializerModule extends SimpleModule {
    public static final class ObjectIdSerializer extends JsonSerializer<ObjectId> {

        @Override
        public void serialize(ObjectId value, JsonGenerator jgen, SerializerProvider provider) throws IOException, JsonProcessingException {
            jgen.writeString(value.toString());
        }
    }

    public static final class ObjectIdDeserializer extends JsonDeserializer<ObjectId> {
        @Override
        public ObjectId deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
            JsonNode node = jp.readValueAsTree();
            String value = node.textValue();
            return new ObjectId(value);
        }
    }

    public ConnectSerializerModule() {
        init();
    }

    protected void init() {
        this.addSerializer(ObjectId.class, new ObjectIdSerializer());
        this.addDeserializer(ObjectId.class, new ObjectIdDeserializer());
    }
}
