package com.fourtwenty.core.util;

import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import org.apache.commons.lang.StringUtils;
import org.joda.time.*;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

/**
 * Created by mdo on 6/29/16.
 * <p>
 * <p>
 * export const TimeZones = [
 * {name:"Eastern Standard Time", value: "America/New_York"},
 * {name:"Central Standard Time ", value: "America/Chicago"},
 * {name:"Mountain Standard Time", value: "America/Denver"},
 * {name:"Pacific Standard Time", value: "America/Los_Angeles"},
 * {name:"Alaskan Standard Time", value: "America/Anchorage"},
 * {name: "Hawaiian Standard Time", value: "Pacific/Honolulu"}
 * <p>
 * ]
 * <p>
 * Hawaiian Standard Time,
 * Alaskan Standard Time,
 * Pacific Standard Time,
 * Mountain Standard Time,
 * US Mountain Standard Time,
 * Central Standard Time,
 * Eastern Standard Time
 */
public final class DateUtil {
    static final int MILLI_TO_HOUR = 1000 * 60 * 60;
    static final int MILLI_TO_MINUTE = 1000 * 60;

    public static final HashMap<String, String> getTimezoneMap() {
        HashMap<String, String> map = new HashMap<>();

        map.put("America/Los_Angeles", "Pacific Standard Time");
        map.put("America/Chicago", "Central Standard Time");
        map.put("America/New_York", "Eastern Standard Time");
        map.put("America/Denver", "Mountain Standard Time");
        map.put("America/Anchorage", "Alaskan Standard Time");
        map.put("Pacific/Honolulu", "Hawaiian Standard Time");

        return map;
    }


    public static final DateTime parseDate(String dateStr) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern("MM/dd/yyyy");
        long date = formatter.parseDateTime(dateStr).getMillis();
        return new DateTime(date);
    }

    public static final DateTime parseDateKey(String dateStr, String timeZone) {
        final String name = timeZone == null ? ConnectAuthToken.DEFAULT_REQ_TIMEZONE : timeZone;


        DateTimeZone tz = DateTimeZone.forID(name);
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyyMMdd");
        long date = formatter.parseDateTime(dateStr).getMillis();

        return new DateTime(date,tz);
    }


    public static final DateTime parseDateKey(String dateStr) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyyMMdd");
        long date = formatter.parseDateTime(dateStr).getMillis();
        return new DateTime(date);
    }

    public static final DateTime parseLongToDate(long dateMillis) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyyMMdd");
        String date = formatter.print(dateMillis);
        return new DateTime(date);
    }

    public static int getTimezoneOffsetInMinutes(String timeZone) {
        final String name = timeZone == null ? ConnectAuthToken.DEFAULT_REQ_TIMEZONE : timeZone;

        DateTimeZone tz = DateTimeZone.forID(name);
        Long instant = DateTime.now().getMillis();
        //String name = tz.getName(instant);
        long offsetInMilliseconds = tz.getOffset(instant);
        long minutes = TimeUnit.MILLISECONDS.toMinutes(offsetInMilliseconds);
        return (int) minutes;

    }

    public static long getTimezoneOffsetInMillis(String timeZone) {
        final String name = timeZone == null ? ConnectAuthToken.DEFAULT_REQ_TIMEZONE : timeZone;

        DateTimeZone tz = DateTimeZone.forID(name);
        Long instant = DateTime.now().getMillis();
        long offsetInMilliseconds = tz.getOffset(instant);
        return offsetInMilliseconds;
    }

    public static DateTime nowUTC() {
        return DateTime.now(DateTimeZone.forID("UTC"));
    }

    public static DateTime nowWithTimeZone(String timeZone) {
        final String name = timeZone == null ? ConnectAuthToken.DEFAULT_REQ_TIMEZONE : timeZone;

        DateTimeZone tz = DateTimeZone.forID(name);
        DateTime dt = DateTime.now(tz);
        return dt;
    }

    public static long timeAtStartWithTimeZone(String timeZone) {
        final String name = timeZone == null ? ConnectAuthToken.DEFAULT_REQ_TIMEZONE : timeZone;

        DateTimeZone tz = DateTimeZone.forID(name);
        DateTime dt = DateTime.now(tz);

        DateTime t = new DateTime(0, tz);
        t = t.withTimeAtStartOfDay().plusSeconds(dt.getSecondOfDay());
        return t.getMillis();
    }

    public static DateTime dateTimeAtStartWithTimeZone(String timeZone) {
        final String name = timeZone == null ? ConnectAuthToken.DEFAULT_REQ_TIMEZONE : timeZone;

        DateTimeZone tz = DateTimeZone.forID(name);
        DateTime dt = DateTime.now(tz);

        DateTime t = new DateTime(0, tz);
        t = t.withTimeAtStartOfDay().plusSeconds(dt.getSecondOfDay());
        return t;
    }

    public static Long timeAtStartWithTimeZone(String timeZone, DateTime dt) {
        DateTimeZone tz = DateTimeZone.forID(timeZone);

        DateTime t = new DateTime(0, tz);
        t = t.withTimeAtStartOfDay().plusSeconds(dt.getSecondOfDay());
        return t.getMillis();
    }

    public static String toDateTimeFormatted(long dateTime, String timeZone) {
        final String name = timeZone == null ? ConnectAuthToken.DEFAULT_REQ_TIMEZONE : timeZone;

        DateTimeZone tz = DateTimeZone.forID(name);
        DateTimeFormatter dtf = DateTimeFormat.forPattern("MM/dd/yyyy hh:mm a");
        DateTime jodatime = new DateTime(dateTime, tz);
        return dtf.print(jodatime);
    }

    public static String toTimeFormatted(long dateTime, String timeZone) {
        final String name = timeZone == null ? ConnectAuthToken.DEFAULT_REQ_TIMEZONE : timeZone;

        DateTimeZone tz = DateTimeZone.forID(name);
        DateTimeFormatter dtf = DateTimeFormat.forPattern("hh:mm a");
        DateTime jodatime = new DateTime(dateTime, tz);
        return dtf.print(jodatime);
    }

    public static String toDateTimeFormatted(long dateTime, String timeZone, String format) {
        final String name = timeZone == null ? ConnectAuthToken.DEFAULT_REQ_TIMEZONE : timeZone;

        DateTimeZone tz = DateTimeZone.forID(name);
        DateTimeFormatter dtf = DateTimeFormat.forPattern(format);
        DateTime jodatime = new DateTime(dateTime, tz);
        return dtf.print(jodatime);
    }

    public static String toDateFormatted(Long dateTime, String timeZone) {
        if (dateTime == null) {
            return "";
        }
        final String name = timeZone == null ? ConnectAuthToken.DEFAULT_REQ_TIMEZONE : timeZone;

        DateTimeZone tz = DateTimeZone.forID(name);
        DateTimeFormatter dtf = DateTimeFormat.forPattern("MM/dd/yyyy");
        DateTime jodatime = new DateTime(dateTime, tz);
        return dtf.print(jodatime);
    }

    public static String toDateFormatted(Long dateTime, String timeZone, String format) {
        if (dateTime == null) {
            return "";
        }
        final String name = timeZone == null ? ConnectAuthToken.DEFAULT_REQ_TIMEZONE : timeZone;

        DateTimeZone tz = DateTimeZone.forID(name);
        DateTimeFormatter dtf = DateTimeFormat.forPattern(format);
        DateTime jodatime = new DateTime(dateTime, tz);
        return dtf.print(jodatime);
    }

    public static String toDateFormattedURC(Long dateTime, String format) {

        DateTimeZone tz = DateTimeZone.UTC;
        DateTimeFormatter dtf = DateTimeFormat.forPattern(format);
        DateTime jodatime = new DateTime(dateTime, tz);
        return dtf.print(jodatime);
    }


    public static String toDateFormatted(Long dateTime) {
        if (dateTime == null) {
            return "";
        }
        DateTimeFormatter dtf = DateTimeFormat.forPattern("MM/dd/yyyy");
        DateTime jodatime = new DateTime(dateTime);
        return dtf.print(jodatime);
    }

    public static String toUTCDateTimeFormat(long dateTime) {
        DateTimeZone tz = DateTimeZone.forID("UTC");
        DateTimeFormatter dtf = DateTimeFormat.forPattern("MM/dd/yyyy hh:mm a");
        DateTime jodatime = new DateTime(dateTime, tz);
        return dtf.print(jodatime);
    }

    public static DateTime toUTCDateTime(long dateTime) {
        DateTimeZone tz = DateTimeZone.forID("UTC");
        DateTime jodatime = new DateTime(dateTime, tz);
        return jodatime;
    }

    public static DateTime getCurrentUTCTime() {
        DateTimeZone tz = DateTimeZone.forID("UTC");
        DateTime jodatime = new DateTime(DateTime.now(), tz);
        return jodatime;
    }

    public static DateTime toDateTime(long dateTime, String timeZone) {
        final String name = timeZone == null ? ConnectAuthToken.DEFAULT_REQ_TIMEZONE : timeZone;

        DateTimeZone tz = DateTimeZone.forID(name);
        DateTime jodatime = new DateTime(dateTime, tz);
        return jodatime;
    }

    public static int getDaysBetweenTwoDates(long startDate, long endDate) {
        Days days = Days.daysBetween(new DateTime(startDate), new DateTime(endDate));
        return days.getDays();
    }

    public static int getMonthsBetweenTwoDates(long startDate, long endDate) {
        Months months = Months.monthsBetween(new DateTime(startDate), new DateTime(endDate));
        return months.getMonths();
    }


    public static int getYearsBetweenTwoDates(long startDate, long endDate) {
        Years years = Years.yearsBetween(new DateTime(startDate), new DateTime(endDate));
        int numberOfYears = years.getYears();
        return numberOfYears;

    }

    public static int getHoursBetweenTwoDates(long date, long endDate) {
        //Period period = new Period(date, endDate);
        //return Math.abs(period.getHours());

        return Math.abs((int) (date - endDate) / MILLI_TO_HOUR);

    }

    public static int getMinutesBetweenTwoDates(long date, long endDate) {
        return Math.abs((int) (date - endDate) / MILLI_TO_MINUTE);

    }

    public static DateTime parseStrDateToDateTimeForPattern(String dateStr, String pattern) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern(pattern);
        return new DateTime(formatter.parseDateTime(dateStr).getMillis());
    }

    public static String parseTodayToPattern(String pattern) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern(pattern);
        return formatter.print(getCurrentUTCTime().getMillis());
    }


    public static long getStandardDaysBetweenTwoDates(long startDate, long endDate) {
        return TimeUnit.MILLISECONDS.toDays(endDate - startDate);
    }

    public static String stripTimezoneOffset(String dateTimeWithOffset) {
        return dateTimeWithOffset.replaceAll("-[0-9][0-9]:[0-9][0-9]$", "Z");
    }

    public static String convertToPatternUTC(String dateStr, String fromPattern, String toPattern) {
        DateTimeZone tz = DateTimeZone.UTC;
        DateTimeFormatter fromFormatter = DateTimeFormat.forPattern(fromPattern);
        DateTimeFormatter toFormatter = DateTimeFormat.forPattern(toPattern);
        return toFormatter.print(new DateTime(fromFormatter.parseDateTime(dateStr).getMillis(), tz));
    }

    public static String convertToISO8601(Long date, String timezone) {
        if (StringUtils.isBlank(timezone)) {
            timezone = ConnectAuthToken.DEFAULT_REQ_TIMEZONE;
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone(timezone));
        simpleDateFormat.applyPattern("yyyy-MM-dd'T'HH:mm:ssXXX");
        return simpleDateFormat.format(date);
    }
}
