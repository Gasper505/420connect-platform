package com.fourtwenty.core.util;

import java.io.InputStream;

/**
 * Created by mdo on 4/26/16.
 */
public class DefaultImageUtil {

    public static InputStream getDefaultImage(String name) {
        if (name == null || !name.startsWith("420default-")) {
            return null;
        } else {
            return DefaultImageUtil.class.getResourceAsStream("/images/" + name.replace("420default-", ""));
        }
    }
}
