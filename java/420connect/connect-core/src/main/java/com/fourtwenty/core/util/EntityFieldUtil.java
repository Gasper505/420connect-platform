package com.fourtwenty.core.util;

import com.fourtwenty.core.domain.models.generic.BaseModel;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by Stephen Schmidt on 11/14/2015.
 */
public class EntityFieldUtil {
    public static final String GET = "get";
    public static final String GET_BOOLEAN = "is";
    public static final String SET = "set";


    public static <T extends BaseModel> void deepCopy(T oldT, T newT) {
        //Oh shit, here comes Reflection
        Field[] oldFields = oldT.getClass().getDeclaredFields();
        for (int i = 0; i < oldFields.length; i++) {
            Field field = oldFields[i];
            String fieldName = oldFields[i].getName().substring(0, 1).toUpperCase() + oldFields[i].getName().substring(1);
            // ignore id field
            if (fieldName.equalsIgnoreCase("id")) continue;

            String setMethodName = SET + fieldName;
            String getMethodName = GET + fieldName;
            //System.out.println("Finding methods " + setMethodName + " and " + getMethodName );
            try {
                Method getM = getGetterMethod(oldT.getClass(), fieldName); //oldT.getClass().getDeclaredMethod(getMethodName);
                Class returnType = getM.getReturnType();
                Method setM = newT.getClass().getDeclaredMethod(setMethodName, returnType);

                if (setM != null || getM != null) {
                    //If it is not a list field, set new value
                    if (!returnType.isAssignableFrom(List.class)) {
                        Object returnObjTest = getM.invoke(oldT);
                        if (returnObjTest instanceof BaseModel) {

                            BaseModel returnObj = (BaseModel) returnObjTest;
                            BaseModel returnObj2 = (BaseModel) getM.invoke(newT);
                            if (returnObj != null && returnObj2 != null) {
                                deepCopy(returnObj, returnObj2);
                            } else if (returnObj != null && returnObj2 == null) {
                                setM.invoke(oldT);
                            }
                            // ignore if both are null
                        } else {
                            setM.invoke(oldT, getM.invoke(newT));
                        }
                    } else {
                        //else recurse through
                        try {
                            //setM.invoke(oldT, deepCopy((List) getM.invoke(oldT, null), (List) getM.invoke(newT, null)));

                            List oldList = (List) getM.invoke(oldT);
                            List newList = (List) getM.invoke(newT);
                            if (oldList == null) {
                                setM.invoke(oldT, newList); // just assign it
                            } else {
                                ParameterizedType listType = (ParameterizedType) field.getGenericType();
                                Class<?> listClass = (Class<?>) listType.getActualTypeArguments()[0];
                                HashMap<String, BaseModel> map = new HashMap<>();

                                Object tempInstance = listClass.newInstance();
                                if (tempInstance instanceof BaseModel) {
                                    for (Object o : oldList) {
                                        BaseModel bo = (BaseModel) o;
                                        map.put(bo.getId(), bo);

                                    }
                                    for (Object o : newList) {
                                        BaseModel newBo = (BaseModel) o;
                                        BaseModel oldBo = map.get(newBo.getId());
                                        if (oldBo == null)
                                            oldList.add(newBo);
                                        else {
                                            deepCopy(oldBo, newBo);
                                        }
                                    }

                                } else {
                                    // just assign it
                                    setM.invoke(oldT, newList); // just assign it
                                }
                            }


                        } catch (ClassCastException cce) {
                            cce.printStackTrace();
                        } catch (InstantiationException e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    System.out.println("methods " + setMethodName + " or " + getMethodName + " not found.");
                    System.out.println("setM: " + Boolean.toString(setM == null) + "| getM: " + Boolean.toString(getM == null));
                }
            } catch (NoSuchMethodException nsme) {
                nsme.printStackTrace();
            } catch (IllegalAccessException iae) {
                iae.printStackTrace();
            } catch (InvocationTargetException ite) {
                ite.printStackTrace();
            }

        }
    }

    public static <T extends BaseModel> List<T> deepCopy(List<T> oldList, List<T> newList) {
        HashMap<String, T> map = new HashMap<>();
        for (T entity : oldList)
            map.put(entity.getId(), entity);
        for (T newT : newList) {
            T oldT = map.get(newT.getId());
            if (oldT == null)
                oldList.add(newT);
            else {
                deepCopy(oldT, newT);
            }
        }
        return oldList;
    }


    private static Method getGetterMethod(Class clazz, String propertyName) throws NoSuchMethodException {
        try {
            Method getM = clazz.getDeclaredMethod(GET + propertyName);
            return getM;
        } catch (Exception e) {
            Method getM = clazz.getDeclaredMethod(GET_BOOLEAN + propertyName);
            return getM;
        }

    }

    public static String value(String input) {
        return input != null ? input : "";
    }

    public static boolean isValidPhone(String input) {
        Pattern pattern = Pattern.compile("^(1\\-)?[0-9]{3}\\-?[0-9]{3}\\-?[0-9]{4}$");
        return pattern.matcher(input).matches();
    }
}
