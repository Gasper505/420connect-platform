package com.fourtwenty.core.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.google.inject.Singleton;
import org.bson.types.ObjectId;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;


@Singleton
public final class JsonSerializer {

    private static final ObjectMapper mapper = new ObjectMapper();

    static {
        //mapper.registerModule(new FCSerializerModule());
    }

    public static String toJson(Object data) {
        try {
            return mapper.writeValueAsString(data);
        } catch (JsonProcessingException e) {
            //Logger.info(e.getMessage(), e);
            return null;
        }
    }

    public static byte[] toBytes(Object data) {
        try {
            return mapper.writeValueAsBytes(data);
        } catch (JsonProcessingException e) {
            return null;
        }
    }

    public static <T> T fromBytes(byte[] bytes, Class<? extends T> clazz) {
        try {
            return mapper.readValue(bytes, clazz);
        } catch (Exception e) {
            //Logger.info(e.getMessage(), e);
            return null;
        }
    }

    public static <T> T fromJson(String json, Class<? extends T> clazz) {
        try {
            return mapper.readValue(json, clazz);
        } catch (Exception e) {
            //Logger.info(e.getMessage(), e);
            return null;
        }
    }

    public static <T> T fromJson(InputStream stream, Class<? extends T> clazz) {
        try {
            return mapper.readValue(stream, clazz);
        } catch (Exception e) {
            //Logger.info(e.getMessage(), e);
            return null;
        }
    }

    public static <T> T convert(Object object, Class<? extends T> clazz) {
        try {
            return mapper.convertValue(object, clazz);
        } catch (Exception e) {
            return null;
        }
    }

    public static void convertIds(LinkedHashMap map) {
        if (map == null) {
            return;
        }
        Set<Object> keys = map.keySet();
        List<Object> myKeys = Lists.newArrayList(keys);
        for (Object key : myKeys) {
            String k = (String) key;
            Object entry = map.get(key);
            if (k != null && k.equals("id")) {
                map.remove("id");
                String id = (String)entry;
                ObjectId entityId = null;
                if (id != null && ObjectId.isValid(id)) {
                    entityId = new ObjectId(id);
                } else {
                    entityId = ObjectId.get(); // New Id
                }
                map.put("_id",entityId);
            } else if (entry instanceof LinkedHashMap) {
                convertIds((LinkedHashMap) entry);
            } else if (entry instanceof ArrayList) {
                ArrayList arrayList = (ArrayList) entry;
                for (Object e : arrayList) {
                    if (e instanceof LinkedHashMap) {
                        convertIds((LinkedHashMap)e);
                    }
                }
            }
        }
    }
}
