package com.fourtwenty.core.util;

import com.blaze.clients.metrcs.MetrcMeasurement;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.domain.models.product.ProductWeightTolerance;
import com.fourtwenty.core.domain.repositories.dispensary.ProductWeightToleranceRepository;
import com.fourtwenty.core.services.thirdparty.models.MetrcVerifiedPackage;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class MeasurementConversionUtil {

    public static double ounceQuantityCalculation(ProductCategory productCategory, Product product, double quantity) {
        // Convert product qty to ounce
        double ounceQuantity = 0.0;
        double perOunce = 0.035274; // 1 gram = 0.035274
        if (productCategory.getUnitType() == ProductCategory.UnitType.grams) {
            ounceQuantity = perOunce * quantity;
        } else {
            final Product.WeightPerUnit productWeightPerUnit = product.getWeightPerUnit();

            if (productWeightPerUnit == Product.WeightPerUnit.FULL_GRAM || productWeightPerUnit == Product.WeightPerUnit.EACH) {
                ounceQuantity = perOunce * quantity;
            } else if (productWeightPerUnit == Product.WeightPerUnit.HALF_GRAM) {
                ounceQuantity = (perOunce / 2) * quantity;
            } else if (productWeightPerUnit == Product.WeightPerUnit.FOURTH) {
                ounceQuantity = (perOunce / 4) * quantity;
            } else if (productWeightPerUnit == Product.WeightPerUnit.EIGHTH) {
                ounceQuantity = (perOunce / 8) * quantity;
            } else if (productWeightPerUnit == Product.WeightPerUnit.OUNCE) {
                ounceQuantity = quantity;
            } else if (productWeightPerUnit == Product.WeightPerUnit.HALF_OUNCE) {
                ounceQuantity = (perOunce / 2) * quantity;
            } else if (productWeightPerUnit == Product.WeightPerUnit.CUSTOM_GRAMS) {
                //TODO when we have custom grams implemented
            }
        }
        return ounceQuantity;
    }


    public static BigDecimal convertToMetrcQuantity(final ProductWeightToleranceRepository toleranceRepository,
                                                    final String companyId,
                                                    String metrcMeasurement,
                                                    final Product productProfile,
                                                    BigDecimal blazeQty) {
        BigDecimal qty = blazeQty;
        if (metrcMeasurement.equalsIgnoreCase(MetrcMeasurement.Each.measurementName)) {
            // SEND AS IS
            return qty;
        } else {
            BigDecimal qtyInGrams = blazeQty;

            BigDecimal gramDivisor = new BigDecimal(1);

            ProductWeightTolerance tolerance = toleranceRepository.getToleranceForWeight(companyId,productProfile.getWeightPerUnit().getCorrespondingWeightKey());
            if (tolerance == null || tolerance.isEnabled() == false) {
                switch (productProfile.getWeightPerUnit()) {
                    case EIGHTH:
                        gramDivisor = ProductWeightTolerance.WeightKey.ONE_EIGHTTH.weightValue;
                        break;
                    case FOURTH:
                        gramDivisor = ProductWeightTolerance.WeightKey.QUARTER.weightValue;
                        break;
                    case HALF_GRAM:
                        gramDivisor = ProductWeightTolerance.WeightKey.HALF_GRAM.weightValue;
                        break;
                    case OUNCE:
                        gramDivisor = ProductWeightTolerance.WeightKey.OUNCE.weightValue;
                        break;
                    case HALF_OUNCE:
                        gramDivisor = ProductWeightTolerance.WeightKey.HALF.weightValue;
                        break;
                    case CUSTOM_GRAMS:
                        if (Product.CustomGramType.GRAM == productProfile.getCustomGramType()) {
                            gramDivisor = productProfile.getCustomWeight();
                        } else if (Product.CustomGramType.MILLIGRAM == productProfile.getCustomGramType()) {
                            gramDivisor =  (productProfile.getCustomWeight().divide(new BigDecimal(1000),2,RoundingMode.HALF_EVEN));
                        }
                        break;
                    default:
                        gramDivisor = ProductWeightTolerance.WeightKey.UNIT.weightValue;
                }
            } else {
                gramDivisor = tolerance.getUnitValue();
            }

            qtyInGrams = qtyInGrams.multiply(gramDivisor);

            if (metrcMeasurement.equalsIgnoreCase(MetrcMeasurement.Ounces.measurementName)) {
                // convert grams to ounces
                BigDecimal ounceQty = qtyInGrams.divide(ProductWeightTolerance.WeightKey.OUNCE.weightValue, 6, RoundingMode.HALF_EVEN);
                return ounceQty;
            }
            return qtyInGrams;
        }
    }


    public static BigDecimal convertToBatchQuantity(final ProductWeightToleranceRepository toleranceRepository,
                                                    final String companyId,
                                                    final MetrcVerifiedPackage verifiedPackage,
                                                    final ProductCategory.UnitType unitType,
                                                    final Product productProfile) {
        /*final Product.WeightPerUnit weightPerUnit = productProfile.getWeightPerUnit();
        final Product.CustomGramType customGramType = productProfile.getCustomGramType();
        final BigDecimal customWeight = productProfile.getCustomWeight();

        BigDecimal verifiedQuantity = verifiedPackage.getQuantity();
        if (unitType == ProductCategory.UnitType.units && verifiedPackage.getBlazeMeasurement() != ProductWeightTolerance.WeightKey.UNIT) {
            // incoming is in grams or units
            BigDecimal gramDivisor = new BigDecimal(1);
            if (weightPerUnit == Product.WeightPerUnit.HALF_GRAM) {
                // set it as normally
                //productBatch.setQuantity(verifiedPackage.getBlazeQuantity().multiply(new BigDecimal(2))); // multiply by 2 as there are twice many in half grams
                gramDivisor = BigDecimal.valueOf(0.5d); // multiply by 2
            } else if (weightPerUnit == Product.WeightPerUnit.EIGHTH) {
                ProductWeightTolerance eighthTolerance = toleranceRepository.getToleranceForWeight(companyId,
                        ProductWeightTolerance.WeightKey.ONE_EIGHTTH);
                BigDecimal eighthValue = ProductWeightTolerance.WeightKey.ONE_EIGHTTH.weightValue;
                if (eighthTolerance != null && eighthTolerance.getUnitValue().doubleValue() > 0) {
                    eighthValue = eighthTolerance.getUnitValue();
                }
                // set it as normally
                gramDivisor = eighthValue;
            } else if (weightPerUnit == Product.WeightPerUnit.FOURTH) {
                ProductWeightTolerance quarterTolerance = toleranceRepository.getToleranceForWeight(companyId,
                        ProductWeightTolerance.WeightKey.QUARTER);
                BigDecimal quarterValue = ProductWeightTolerance.WeightKey.QUARTER.weightValue;
                if (quarterTolerance != null && quarterTolerance.getUnitValue().doubleValue() > 0) {
                    quarterValue = quarterTolerance.getUnitValue();
                }
                // set it as normally
                gramDivisor = quarterValue;
            } else if (weightPerUnit == Product.WeightPerUnit.CUSTOM_GRAMS && customWeight != null && customWeight.doubleValue() > 0) {
                if (customGramType == Product.CustomGramType.GRAM) {
                    gramDivisor = customWeight;
                } else if (Product.CustomGramType.MILLIGRAM == customGramType) {
                    gramDivisor = customWeight.multiply(new BigDecimal(1000));
                }
            }

            verifiedQuantity = verifiedPackage.getBlazeQuantity().divide(gramDivisor,4, RoundingMode.HALF_EVEN);
        }
        return verifiedQuantity;*/

        return convertToBatchQuantity(toleranceRepository,
                companyId,
                verifiedPackage.getBlazeMeasurement(),
                verifiedPackage.getQuantity(),
                verifiedPackage.getBlazeQuantity(),
                unitType,
                productProfile);
    }


    public static BigDecimal convertToBatchQuantity(final ProductWeightToleranceRepository toleranceRepository,
                                                    final String companyId,
                                                    final ProductWeightTolerance.WeightKey blazeWeightKey,
                                                    final BigDecimal metrcQty,
                                                    final BigDecimal blazeQtyRaw,
                                                    final ProductCategory.UnitType unitType,
                                                    final Product productProfile) {
        final Product.WeightPerUnit weightPerUnit = productProfile.getWeightPerUnit();
        final Product.CustomGramType customGramType = productProfile.getCustomGramType();
        final BigDecimal customWeight = productProfile.getCustomWeight();

        BigDecimal verifiedQuantity = metrcQty;
        if (unitType == ProductCategory.UnitType.units && blazeWeightKey != ProductWeightTolerance.WeightKey.UNIT) {
            // incoming is in grams or units
            BigDecimal gramDivisor = new BigDecimal(1);
            if (weightPerUnit == Product.WeightPerUnit.HALF_GRAM) {
                // set it as normally
                //productBatch.setQuantity(verifiedPackage.getBlazeQuantity().multiply(new BigDecimal(2))); // multiply by 2 as there are twice many in half grams
                gramDivisor = BigDecimal.valueOf(0.5d); // multiply by 2
            } else if (weightPerUnit == Product.WeightPerUnit.EIGHTH) {
                ProductWeightTolerance eighthTolerance = toleranceRepository.getToleranceForWeight(companyId,
                        ProductWeightTolerance.WeightKey.ONE_EIGHTTH);
                BigDecimal eighthValue = ProductWeightTolerance.WeightKey.ONE_EIGHTTH.weightValue;
                if (eighthTolerance != null && eighthTolerance.getUnitValue().doubleValue() > 0) {
                    eighthValue = eighthTolerance.getUnitValue();
                }
                // set it as normally
                gramDivisor = eighthValue;
            } else if (weightPerUnit == Product.WeightPerUnit.FOURTH) {
                ProductWeightTolerance quarterTolerance = toleranceRepository.getToleranceForWeight(companyId,
                        ProductWeightTolerance.WeightKey.QUARTER);
                BigDecimal quarterValue = ProductWeightTolerance.WeightKey.QUARTER.weightValue;
                if (quarterTolerance != null && quarterTolerance.getUnitValue().doubleValue() > 0) {
                    quarterValue = quarterTolerance.getUnitValue();
                }
                // set it as normally
                gramDivisor = quarterValue;
            } else if (weightPerUnit == Product.WeightPerUnit.CUSTOM_GRAMS && customWeight != null && customWeight.doubleValue() > 0) {
                if (customGramType == Product.CustomGramType.GRAM) {
                    gramDivisor = customWeight;
                } else if (Product.CustomGramType.MILLIGRAM == customGramType) {
                    gramDivisor = customWeight.divide(new BigDecimal(1000));
                }
            }

            verifiedQuantity = blazeQtyRaw.divide(gramDivisor,4, RoundingMode.HALF_EVEN);
        }
        return verifiedQuantity;
    }

}
