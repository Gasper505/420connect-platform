package com.fourtwenty.core.util;

import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductWeightTolerance;

public class ModelUtil {
    public static ProductWeightTolerance.WeightKey getWeightKeyFor(Product.WeightPerUnit weightPerUnit) {
        switch (weightPerUnit) {
            case HALF_GRAM:
                return ProductWeightTolerance.WeightKey.HALF;
            case EIGHTH:
                return ProductWeightTolerance.WeightKey.ONE_EIGHTTH;
            case FOURTH:
                return ProductWeightTolerance.WeightKey.QUARTER;
            case EACH:
                return ProductWeightTolerance.WeightKey.UNIT;
        }
        return null;
    }
}
