package com.fourtwenty.core.util;

import java.math.BigDecimal;

/**
 * Created by mdo on 6/18/16.
 */
public final class NumberUtils {
    public static final int DEFAULT_ROUND_PLACES = 2;
    public static final int MONEY_ROUND_PLACES = 2;

    public static double roundMoney(double value) {
        return round(value, MONEY_ROUND_PLACES);
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        double divisor = Math.pow(10.0, places);
        double newValue = Math.round(value * divisor) / divisor;
        return newValue;
        //BigDecimal bd = new BigDecimal(value);
        //bd = bd.setScale(places, RoundingMode.HALF_UP);
        //return bd.doubleValue();
    }

    public static double round(BigDecimal value, int places) {
        if (places < 0) throw new IllegalArgumentException();
        /*
        value = value.setScale(places, RoundingMode.HALF_UP);
        return value.doubleValue();*/
        return round(value.doubleValue(), places);
    }

    public static BigDecimal round2(BigDecimal value, int places) {
        if (places < 0) throw new IllegalArgumentException();
        /*
        value = value.setScale(places, RoundingMode.HALF_UP);
        return value.doubleValue();*/
        return new BigDecimal(round(value.doubleValue(), places));
    }

    public static double round(BigDecimal value) {
        return round(value, DEFAULT_ROUND_PLACES);
    }

    public static double roundCurrency(double value) {
        return round(value, DEFAULT_ROUND_PLACES);
    }

    public static double roundToNearestCent(double amount) {
        return ((double) (long) (amount * 20 + 0.5)) / 20;
    }

    public static double ceil(double value) {
        return Math.ceil(value);
    }

    public static double floor(double value) {
        return Math.floor(value);
    }

    public static String uniqueNumber(final String initial, final Long ordinal) {
        String uniqueNumber;
        int length = Long.toString(ordinal).length();
        if (length == 1) {
            uniqueNumber = initial.trim() + "-00000" + ordinal;
        } else if (length == 2) {
            uniqueNumber = initial.trim() + "-0000" + ordinal;
        } else if (length == 3) {
            uniqueNumber = initial.trim() + "-000" + ordinal;
        } else if (length == 4) {
            uniqueNumber = initial.trim() + "-00" + ordinal;
        } else if (length == 5) {
            uniqueNumber = initial.trim() + "-0" + ordinal;
        } else {
            uniqueNumber = initial.trim() + "-" + ordinal;
        }
        return uniqueNumber;
    }

    public static double truncateDecimal(double value) {
        return truncateDecimal(value, 2);
    }

    public static double truncateDecimal(double value, int decimalPlaces) {
        double divisor = Math.pow(10.0, decimalPlaces);
        double newValue = Math.floor(value * divisor) / divisor;
        return newValue;
    }

    public static double valueOrZero(BigDecimal value) {
        if (value == null) {
            return 0d;
        } else {
            return value.doubleValue();
        }
    }
}
