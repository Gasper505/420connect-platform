package com.fourtwenty.core.util;

import net.glxn.qrgen.core.image.ImageType;
import net.glxn.qrgen.javase.QRCode;
import org.apache.commons.io.IOUtils;

import java.io.*;
import java.util.UUID;

public class QrCodeUtil {
    public static InputStream getQrCode(String uniqueNumber) {
        // get QR stream from text using defaults
        ByteArrayOutputStream stream = QRCode.from(uniqueNumber).withSize(250, 250).to(ImageType.PNG).stream();
        byte[] data = stream.toByteArray();
        return new ByteArrayInputStream(data);
    }

    public static File stream2file(InputStream in, String extension) throws IOException {
        final File tempFile = File.createTempFile(UUID.randomUUID().toString(), extension);
        tempFile.deleteOnExit();
        try (FileOutputStream out = new FileOutputStream(tempFile)) {
            IOUtils.copy(in, out);
        }
        return tempFile;
    }
}
