package com.fourtwenty.core.util;

import java.util.Base64;
import java.util.Random;

/**
 * Created by mdo on 10/26/17.
 */
public final class RandomUtil {
    private static Random random = new Random();

    public static String base64Hash() {
        byte[] buffer = getRandom(6);
        String encoded = Base64.getEncoder().encodeToString(buffer);

        return encoded;
    }

    public static String getIdentifier() {
        byte[] buffer = getRandom(6);
        String encoded = Base64.getEncoder().encodeToString(buffer);
        encoded = encoded.replaceAll("\\+", "");
        encoded = encoded.replaceAll("/", "");
        return encoded;
    }

    // <summary>
// This is used by all Unique identifier examples
// </summary>
    public static byte[] getRandom(int size) {
        byte[] buffer = new byte[size];
        random.nextBytes(buffer);
        return buffer;
    }
}
