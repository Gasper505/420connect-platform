package com.fourtwenty.core.util;

import com.google.common.base.Splitter;
import org.apache.commons.lang3.StringUtils;
import org.davidmoten.text.utils.WordWrap;

import java.math.BigDecimal;
import java.util.ArrayList;

public final class ReceiptUtil {

    private static final int MAX_LINES = 2;

    public static void addTaxOrSkip(StringBuilder sb, String label, BigDecimal value, String defaultCountry, int length, boolean preTax) {
        if (value != null && value.doubleValue() > 0) {
            if (preTax) {
                sb.append(addDataWithSpace(label, String.format("(%s)", TextUtil.toCurrency(value.doubleValue(), defaultCountry)), length)).append("\n");
            } else {
                sb.append(addDataWithSpace(label, TextUtil.toCurrency(value.doubleValue(), defaultCountry), length)).append("\n");
            }
        }
    }

    public static String getCommonItem(String a, String b, int length) {
        int lengthA = 0;
        String data = "";
        if (StringUtils.isNotBlank(a)) {
            lengthA = a.length();
            data = data.concat(a).concat("").concat(":").concat(" ").concat(b);
        } else {
            data = data.concat(b);
        }

        int lengthB = b.length();
        int totalLength = lengthA + lengthB;
        String format1 = "";
        if (length >= data.length()) {
            format1 = format1.concat(data);
        } else {
            /*Iterable<String> split = Splitter.fixedLength(length).split(data);
            ArrayList<String> stringList = new ArrayList<>();
            for (String s : split) {
                stringList.add(s);
            }
            for (int i = 0; i < stringList.size(); i++) {
                format1 = format1.concat(stringList.get(i)) + "\n";
            }*/
            format1 = format1.concat(WordWrap.from(data).maxWidth(length).wrap()) + "\n";
        }
        return WordWrap.from(format1).maxWidth(length).wrap();
    }


    public static String repeatContent(int spaceCount, String content, String contentToRepeat, boolean isLeftAlign) {
        String data = content;
        for (int j = 1; j <= spaceCount; j++) {
            if (isLeftAlign) {
                data = data.concat(contentToRepeat);
            } else {
                data = contentToRepeat.concat(data);
            }
        }
        return data;
    }

    public static String addDataWithSpace(String a, String b, int length) {
        int length1 = a.length();
        int length2 = b.length();

        if (length1 > 20) {
            a = a.substring(0, 20);
            length1 = a.length();
        }
        String data = "";
        int l = length1 + length2;
        if (length > l) {
            String s = repeatContent(length - l, "", " ", Boolean.TRUE);
            data = new StringBuilder().append(a).append(s).append(b).toString();
        }
        return data;
    }


    public static String getLineItem(String a, String b, int width) {
        int length1 = a.length();
        int length = b.length();
        StringBuilder format1 = new StringBuilder();
        StringBuilder data = new StringBuilder();
        int j = width - (length + 1);
        if (length1 < j) {
            String s = forSpace((j + 1 - length1), " ");
            format1 = format1.append(a).append(s).append(b);
        } else {
            Iterable<String> split = Splitter.fixedLength(j).split(a);
            ArrayList<String> arrayList = new ArrayList<>();
            for (String s : split) {
                arrayList.add(s);
            }

            int arSize = arrayList.size();
            for (int i = 0; i < arSize; i++) {
                if (i == 0) {
                    String s = forSpace((width - (arrayList.get(i).length() + length)), " ");
                    data = data.append(arrayList.get(i)).append(s).append(b);
                } else {
                    data = data.append("\n").append(arrayList.get(i));
                    if ((i + 1) < arSize) {
                        data = data.append("\n");
                    }
                }
            }
            //data = data.("\n") ? data.substring(0,data.lastIndexOf("\n")) : data ;

            format1 = format1.append(data);
        }

        return format1.toString();
    }

    public static String forSpace(int i, String space) {
        StringBuilder sb = new StringBuilder();
        for (int j = 1; j <= i; j++) {
            sb.append(space);
        }
        return sb.toString();
    }

    public static String addProductWithSpace(String a, String b, int length) {
        final int maxwidth = length > 32 ? (length - 9) : 23;///23;
        String origA = a;
        int length2 = b.length();
        int start = 0;

        int limit = a.length() > maxwidth ? maxwidth : a.length();

        StringBuilder data = new StringBuilder();
        int maxLine = 2;
        int line = 0;
        while (start < origA.length()) {
            if (line >= maxLine) {
                break;
            }
            a = origA.substring(start, limit);
            a = a.trim();
            int length1 = a.length();
            if (start != 0) {
                b = "";
                length2 = b.length();
            }
            int l = length1 + length2;
            if (length > l) {
                String s = repeatContent(length - l, "", " ", Boolean.TRUE);
                if (StringUtils.isNotBlank(data)) {
                    data = data.append("\n");
                }
                data = data.append(a).append(s).append(b);
            }
            start = limit;
            if (limit + maxwidth > origA.length()) {
                limit = origA.length();
            } else {
                limit += maxwidth;
            }
            line++;
        }
        return data.toString();
    }

    public static String addProductWithWordWrap(String a, String b, int length) {
        final int maxwidth = length > 32 ? (length - 9) : 23;///23;

        String origA = a;
        int length2 = b.length();
        int start = 0;

        int limit = a.length() > maxwidth ? maxwidth : a.length();

        StringBuilder data = new StringBuilder();
        int maxLine = 2;
        int line = 0;
        String[] strList = WordWrap.from(a).maxWidth(maxwidth).wrap().split("\\n");
        for (String s : strList) {
            if (line >= maxLine) {
                break;
            }
            a = s.trim();
            int length1 = a.length();
            if (start != 0) {
                b = "";
                length2 = b.length();
            }
            int l = length1 + length2;
            start = limit;
            if (length > l) {
                s = repeatContent(length - l, "", " ", Boolean.TRUE);
                if (StringUtils.isNotBlank(data)) {
                    data = data.append("\n");
                }
                data = data.append(a).append(s).append(b);
            }
            if (limit + maxwidth > origA.length()) {
                limit = origA.length();
            } else {
                limit += maxwidth;
            }
            line++;
        }
        return data.toString();
    }
}
