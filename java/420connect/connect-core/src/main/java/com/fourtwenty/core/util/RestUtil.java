package com.fourtwenty.core.util;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import com.google.inject.Singleton;
import org.glassfish.jersey.client.JerseyClient;
import org.glassfish.jersey.client.JerseyClientBuilder;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.ExecutionException;

/**
 * Created with IntelliJ IDEA.
 * User: mdo
 * Date: 8/15/13
 * Time: 12:52 AM
 * To change this template use File | Settings | File Templates.
 */
@Singleton
public class RestUtil {
    public static <T> T get(String url, Class<T> clazz) throws IOException, ExecutionException, InterruptedException {
        JerseyClient client = JerseyClientBuilder.createClient().register(JacksonJsonProvider.class);
        WebTarget webTarget = client.target(url);
        return webTarget.request().accept(MediaType.APPLICATION_JSON).header("Content-Type", MediaType.APPLICATION_JSON).get(clazz);
    }

    public static <T> T post(String url, Object jsonBody, Class<T> clazz) throws IOException, ExecutionException, InterruptedException {
        JerseyClient client = JerseyClientBuilder.createClient().register(JacksonJsonProvider.class);
        WebTarget webTarget = client.target(url);
        Entity entity = Entity.entity(jsonBody, MediaType.APPLICATION_JSON);
        return (T) webTarget.request().accept(MediaType.APPLICATION_JSON).header("Content-Type", MediaType.APPLICATION_JSON).post(entity, clazz);
    }

    public static InputStream getFile(String url) throws IOException, ExecutionException, InterruptedException {
        return null;
    }
}

