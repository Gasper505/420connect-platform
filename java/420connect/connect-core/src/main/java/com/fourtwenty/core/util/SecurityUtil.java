package com.fourtwenty.core.util;

import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.exceptions.BlazeAuthException;
import com.fourtwenty.core.rest.dispensary.results.shop.ShopAssetToken;
import com.fourtwenty.core.security.tokens.AssetAccessToken;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.security.tokens.ConsumerAssetAccessToken;
import com.fourtwenty.core.security.tokens.EmployeeInviteToken;
import com.fourtwenty.core.security.tokens.PartnerAuthToken;
import com.fourtwenty.core.security.tokens.StoreAuthToken;
import com.google.inject.Singleton;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jasypt.util.password.BasicPasswordEncryptor;
import org.jasypt.util.text.BasicTextEncryptor;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Base64;
import java.util.UUID;

//importer org.mindrot.jbcrypt.BCrypt;


@Singleton
public class SecurityUtil {
    private static final int MAX_PIN_LENGTH = 5;
    private static final int PASSWORD_SIZE = 12;
    private static final String VALID_PW_CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@&#";
    private static final String CIPHER_INSTANCE = "AES/CBC/PKCS5Padding";

    private BasicPasswordEncryptor passwordEncryptor = new BasicPasswordEncryptor();
    private BasicTextEncryptor textEncryptor = new BasicTextEncryptor();
    private Log logUtil = LogFactory.getLog(SecurityUtil.class);

    final ConnectConfiguration config;

    public SecurityUtil(ConnectConfiguration config) {
        this.config = config;
        textEncryptor.setPassword(config.getAppSecret());
    }

    public String createAssetAccessToken(AssetAccessToken token) {
        final String json = JsonSerializer.toJson(token);
        return textEncryptor.encrypt(json);
    }

    public AssetAccessToken decryptAssetAccessToken(String encryptedToken) {
        final String text = textEncryptor.decrypt(encryptedToken);
        return JsonSerializer.fromJson(text, AssetAccessToken.class);
    }

    public String createConsumerAssetAccessToken(ConsumerAssetAccessToken token) {
        final String json = JsonSerializer.toJson(token);
        return textEncryptor.encrypt(json);
    }

    public ConsumerAssetAccessToken decryptConsumerAssetAccessToken(String encryptedToken) {
        final String text = textEncryptor.decrypt(encryptedToken);
        return JsonSerializer.fromJson(text, ConsumerAssetAccessToken.class);
    }

    public String createAccessToken(ConnectAuthToken token) {
        final String json = JsonSerializer.toJson(token);
        return textEncryptor.encrypt(json);
    }

    public ConnectAuthToken decryptAccessToken(String encryptedToken) {
        final String text = textEncryptor.decrypt(encryptedToken);
        return JsonSerializer.fromJson(text, ConnectAuthToken.class);
    }

    public String createStoreToken(StoreAuthToken token) {
        final String json = JsonSerializer.toJson(token);
        return textEncryptor.encrypt(json);
    }

    public StoreAuthToken decryptStoreToken(String encryptedToken) {
        final String text = textEncryptor.decrypt(encryptedToken);
        return JsonSerializer.fromJson(text, StoreAuthToken.class);
    }

    public String createPartnerToken(PartnerAuthToken token) {
        final String json = JsonSerializer.toJson(token);
        return textEncryptor.encrypt(json);
    }

    public PartnerAuthToken decryptPartnerToken(String encryptedToken) {
        final String text = textEncryptor.decrypt(encryptedToken);
        return JsonSerializer.fromJson(text, PartnerAuthToken.class);
    }

    public String createEmployeeInviteToken(EmployeeInviteToken employeeInviteToken) {
        final String json = JsonSerializer.toJson(employeeInviteToken);
        return textEncryptor.encrypt(json);
    }

    public EmployeeInviteToken decryptEmployeeInviteToken(String encryptedToken) {
        final String text = textEncryptor.decrypt(encryptedToken);
        return JsonSerializer.fromJson(text, EmployeeInviteToken.class);
    }

    public String encryptPassword(String password) {
        return passwordEncryptor.encryptPassword(password);
    }

    public boolean checkPassword(String password, String encrypted) {
        return passwordEncryptor.checkPassword(password, encrypted);
    }


    public String getNextPassCode() {
        String[] uuid = UUID.randomUUID().toString().split("-");
        String code = uuid[1];
        return code;
    }

    public String getNextResetCode() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

    public String hash(String raw) {
        MessageDigest messageDigest = null;
        try {
            messageDigest = MessageDigest.getInstance("SHA-256");
            messageDigest.update(raw.getBytes());
            String encryptedString = new String(messageDigest.digest());
            return encryptedString;
        } catch (NoSuchAlgorithmException e) {
            logUtil.error(e.getMessage(), e);
        }
        return null;
    }

    public String generatePin() {
        return RandomStringUtils.randomNumeric(MAX_PIN_LENGTH);
    }

    public static String generatPassword() {
        SecureRandom random = new SecureRandom();
        StringBuilder password = new StringBuilder();
        while (password.length() < PASSWORD_SIZE) {
            char character = (char) random.nextInt(Character.MAX_VALUE);
            if (VALID_PW_CHARS.contains(String.valueOf(character))) {
                password.append(character);
            }
        }
        return password.toString();
    }

    public String generateKey() {
        return UUID.randomUUID().toString().replaceAll("-", "").toLowerCase();
    }

    public String generateSecret() {
        try {
            KeyGenerator keyGen = KeyGenerator.getInstance("HmacMD5");
            SecretKey key = keyGen.generateKey();
            String encodedKey = Base64.getEncoder().encodeToString(key.getEncoded());
            return encodedKey;
        } catch (NoSuchAlgorithmException e) {
            logUtil.error("Error generating key", e);
        }

        return null;
    }

    public String encryptAssetDownloadToken(ShopAssetToken result) {
        final String json = JsonSerializer.toJson(result);
        return textEncryptor.encrypt(json);
    }

    public ShopAssetToken decryptAssetDownloadToken(String encryptedToken) {
        final String text = textEncryptor.decrypt(encryptedToken);
        return JsonSerializer.fromJson(text, ShopAssetToken.class);
    }


    public String encryptByInternalApiSecret(String plainText) {
        SecretKeySpec secretKey = new SecretKeySpec(Arrays.copyOfRange(config.getInternalApiSecret().getBytes(), 0, 16), "AES");
        try {
            Cipher cipher = Cipher.getInstance(CIPHER_INSTANCE);
            cipher.init(Cipher.ENCRYPT_MODE, secretKey, new IvParameterSpec(config.getInternalApiIV().getBytes()));
            return  Base64.getEncoder().encodeToString(cipher.doFinal(plainText.getBytes()));
        } catch (Exception e) {
            throw new BlazeAuthException("Auth", e.getMessage());
        }

    }

    public ConnectAuthToken decryptInternalApiToken(String encryptedToken) {

        final String text = decryptByInternalApiSecret(encryptedToken);
        return JsonSerializer.fromJson(text, ConnectAuthToken.class);
    }

    public Boolean isInternalApiToken(String encryptedToken) {
        try {
            return decryptInternalApiToken(encryptedToken) != null;
        } catch (Exception e) {
            return Boolean.FALSE;
        }
    }

    public String decryptByInternalApiSecret(String plainText)  {
        SecretKeySpec secretKey = new SecretKeySpec(Arrays.copyOfRange(config.getInternalApiSecret().getBytes(), 0, 16), "AES");
        try {
            Cipher cipher = Cipher.getInstance(CIPHER_INSTANCE);
            cipher.init(Cipher.DECRYPT_MODE, secretKey, new IvParameterSpec(config.getInternalApiIV().getBytes()));
            return new String(cipher.doFinal(Base64.getDecoder().decode(plainText)));
        } catch (Exception e) {
            throw new BlazeAuthException("Auth", e.getMessage());
        }
    }
    public String decryptByOnPremApiSecret(String encryptedString, String secret, String iv) {
        SecretKeySpec secretKey = new SecretKeySpec(Arrays.copyOfRange(secret.getBytes(), 0, 16), "AES");
        try {
            Cipher cipher = Cipher.getInstance(CIPHER_INSTANCE);
            cipher.init(Cipher.DECRYPT_MODE, secretKey, new IvParameterSpec(iv.getBytes()));
            return new String(cipher.doFinal(Base64.getDecoder().decode(encryptedString)));
        } catch (Exception e) {
            throw new BlazeAuthException("Auth", e.getMessage());
        }
    }

    public String encryptByOnPremApiSecret(String plainText, String secret, String iv) {
        SecretKeySpec secretKey = new SecretKeySpec(Arrays.copyOfRange(secret.getBytes(), 0, 16), "AES");
        try {
            Cipher cipher = Cipher.getInstance(CIPHER_INSTANCE);
            cipher.init(Cipher.ENCRYPT_MODE, secretKey, new IvParameterSpec(iv.getBytes()));
            return  Base64.getEncoder().encodeToString(cipher.doFinal(plainText.getBytes()));
        } catch (Exception e) {
            throw new BlazeAuthException("Auth", e.getMessage());
        }

    }
    public String encryptClover(String text) {
        return textEncryptor.encrypt(text);
    }

    public String dencryptClover(String encryptedString) {
        return textEncryptor.decrypt(encryptedString);
    }
}
