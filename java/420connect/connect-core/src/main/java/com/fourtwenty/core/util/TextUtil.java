package com.fourtwenty.core.util;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Currency;
import java.util.Locale;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by mdo on 4/3/16.
 */
public final class TextUtil {
    @Deprecated
    public static Pattern createPattern(String term) {
        String[] terms = term.split(" ");
        StringBuilder sb = new StringBuilder("\\b(");
        for (int i = 0; i < terms.length; i++) {
            sb.append(terms[i]);
            if (i < (terms.length - 1)) {
                sb.append("|");
            }
        }
        sb.append(")");
        Pattern pattern = Pattern.compile(sb.toString().toLowerCase(), Pattern.CASE_INSENSITIVE);
        return pattern;
    }

    public static String toCurrency(Double value, String defaultCountry) {
        double amount = value;
        // set defaultCountry to 'US'
        if (defaultCountry == null) {
            defaultCountry = "US";
        }
        Locale locale = new Locale("en", defaultCountry);
        NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(locale);
        return currencyFormatter.format(amount);
    }

    public static String toCurrencyFormat(double value, String defaultCountry) {

        if (defaultCountry == null) {
            defaultCountry = "US";
        }
        if (value == 0.0) {
            value = 0.00;
        }
        Locale locale = new Locale("en", defaultCountry);
        DecimalFormat decimalFormat = (DecimalFormat) NumberFormat.getNumberInstance(locale);
        decimalFormat.setCurrency(Currency.getInstance(locale));
        String symbol = decimalFormat.getCurrency().getSymbol(locale).replaceAll("\\.", "");

        decimalFormat.applyPattern(symbol + "#,##0.00;" + symbol + "-#,##0.00");
        return decimalFormat.format(value);
    }

    public static String toEscapeCurrency(Double value, String defaultCountry) {
        return "\\" + toCurrency(value, defaultCountry);
    }

    public static String toDate(long dateTime) {
        DateTimeFormatter dtf = DateTimeFormat.forPattern("MM/dd/yyyy");
        DateTime jodatime = new DateTime(dateTime);
        return dtf.print(jodatime);
    }

    public static String toDateTime(long dateTime) {
        DateTimeFormatter dtf = DateTimeFormat.forPattern("MM/dd/yyyy HH:mm:ss");
        DateTime jodatime = new DateTime(dateTime);
        return dtf.print(jodatime);
    }

    public static String toDateTime(long dateTime, String format) {
        DateTimeFormatter dtf = DateTimeFormat.forPattern(format);
        DateTime jodatime = new DateTime(dateTime);
        return dtf.print(jodatime);
    }

    public static String formatToTwoDecimalPoints(Number val) {
        return Objects.isNull(val) ? StringUtils.EMPTY : new DecimalFormat("0.00").format(val);
    }

    public static String formatToFourDecimalPoints(Number val) {
        return Objects.isNull(val) ? StringUtils.EMPTY : new DecimalFormat("0.0000").format(val);
    }

    public static String textOrEmpty(String text) {
        if (text == null) {
            return "";
        }
        return text;
    }

    public static boolean isTextEqual(String text1, String text2) {
        if (text1 == null && text2 == null) {
            return true;
        }
        if (text1 != null) {
            return text1.equalsIgnoreCase(text2);
        }
        return false;
    }

    public static boolean isNumberEqual(Long number1, Long number2) {
        if (number1 == null && number2 == null) {
            return true;
        }
        if (number1 != null && number2 != null) {
            return number1.longValue() == number2.longValue();
        }
        return false;
    }

    public static String toDate(DateTime dateTime) {
        DateTimeFormatter dtf = DateTimeFormat.forPattern("MMM dd, yyyy");
        return dtf.print(dateTime);
    }

    public static String toTime(DateTime dateTime) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern("hh:mm a");
        return formatter.print(dateTime);
    }

    public static String formatToNDecimalDigits(Number val, int decimalPlace) {
        String numberFormat = StringUtils.rightPad("0.", decimalPlace, '0');
        return Objects.isNull(val) ? StringUtils.EMPTY : new DecimalFormat(numberFormat).format(val);
    }

    // Convert Only special character into hexa code
    public static String formatSpecialCharacterConvertToHexCode(String value) {
        char[] chars = value.toCharArray();
        Pattern pattern = Pattern.compile("[^a-z A-Z 0-9]");
        StringBuffer hex = new StringBuffer();
        for (int i = 0; i < chars.length; i++) {
            Matcher matcher = pattern.matcher(String.valueOf(chars[i]));
            boolean status = matcher.find();
            if (status) {
                hex.append("&#x").append(Integer.toHexString((int)chars[i])).append(";");
            } else {
                hex.append(chars[i]);
            }
        }
        return hex.toString();
    }

    public static Pattern createPatternWithSpecialChars(String keyword) {
        if (StringUtils.isNotEmpty(keyword)) {
            keyword = keyword.replace("\\", "\\\\")
                    .replace("*", "\\*")
                    .replace("(", "\\(")
                    .replace(")", "\\)")
                    .replace("+", "\\+")
                    .replace("[", "\\[")
                    .replace("]", "\\]")
                    .replace("{", "\\{")
                    .replace("}", "\\}")
                    .replace(".", "\\.")
                    .replace("<", "\\<")
                    .replace(">", "\\>")
                    .replace("?", "\\?")
                    .replace("$", "\\$")
                    .replace("-","\\-")
                    .replace("^", "\\^")
                    .replace("|", "\\|");
        }
        return Pattern.compile("^.*" + keyword + ".*$", Pattern.CASE_INSENSITIVE);
    }
    public static String escapeHtmlText(String input) {

        if (StringUtils.isNotBlank(input)) {
            return StringEscapeUtils.escapeHtml4(input);
        }
        return "";
    }

    public static String escapeXMLText(String input) {

        if (StringUtils.isNotBlank(input)) {
            return StringEscapeUtils.escapeXml10(input);
        }
        return "";
    }
}
