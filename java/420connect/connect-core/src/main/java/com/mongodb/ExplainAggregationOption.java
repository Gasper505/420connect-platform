package com.mongodb;

/**
 * Created by mdo on 7/21/18.
 */
public class ExplainAggregationOption extends AggregationOptions {
    private String explain = "true";

    public ExplainAggregationOption() {
        super(AggregationOptions.builder());
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("AggregationOptions{");
        sb.append("cursor=")
                .append(explain);
        sb.append('}');
        return sb.toString();
    }
}
