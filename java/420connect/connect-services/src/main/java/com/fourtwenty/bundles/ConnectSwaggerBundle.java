package com.fourtwenty.bundles;

import com.fourtwenty.core.config.ConnectConfiguration;
import io.federecio.dropwizard.swagger.SwaggerBundle;
import io.federecio.dropwizard.swagger.SwaggerBundleConfiguration;

/**
 * Created by mdo on 9/4/16.
 */
public class ConnectSwaggerBundle extends SwaggerBundle<ConnectConfiguration> {
    @Override
    protected SwaggerBundleConfiguration getSwaggerBundleConfiguration(ConnectConfiguration configuration) {
        return configuration.swaggerBundleConfiguration;
    }
}