package com.fourtwenty.migrations;

import com.esotericsoftware.kryo.Kryo;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.domain.models.App;
import com.fourtwenty.core.domain.models.company.*;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.generic.ConnectProduct;
import com.fourtwenty.core.domain.models.global.StateCannabisLimit;
import com.fourtwenty.core.domain.models.global.USState;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductChangeLog;
import com.fourtwenty.core.domain.models.product.ProductPriceRange;
import com.fourtwenty.core.domain.models.product.ProductWeightTolerance;
import com.fourtwenty.core.domain.models.thirdparty.WeedmapAccount;
import com.fourtwenty.core.domain.models.thirdparty.WeedmapApiKeyMap;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.repositories.dispensary.*;
import com.fourtwenty.core.domain.repositories.global.StateCannabisLimitRepository;
import com.fourtwenty.core.domain.repositories.thirdparty.WeedmapAccountRepository;
import com.fourtwenty.core.lifecycle.Migration;
import com.fourtwenty.core.lifecycle.model.DefaultData;
import com.fourtwenty.core.lifecycle.model.GlobalData;
import com.fourtwenty.core.managed.BackgroundTaskManager;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.onprem.OnPremSyncService;
import com.fourtwenty.core.tasks.*;
import com.fourtwenty.core.util.NumberUtils;
import com.fourtwenty.core.util.SecurityUtil;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import io.dropwizard.setup.Environment;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

/**
 * Created by mdo on 7/12/16.
 */
public class ConnectMigration implements Migration {
    private static final Log LOG = LogFactory.getLog(ConnectMigration.class);
    @Inject
    AppRepository appRepository;
    @Inject
    ConnectConfiguration connectConfiguration;
    @Inject
    DefaultData defaultData;
    @Inject
    RoleRepository roleRepository;
    @Inject
    CompanyRepository companyRepository;
    @Inject
    CompanyFeaturesRepository companyFeaturesRepository;
    @Inject
    ConnectProductRepository connectProductRepository;
    @javax.inject.Inject
    Environment environment;
    @Inject
    SecurityUtil securityUtil;
    @Inject
    ProductRepository productRepository;
    @Inject
    TransactionRepository transactionRepository;
    @Inject
    ProductChangeLogRepository productChangeLogRepository;
    @Inject
    MemberRepository memberRepository;
    @Inject
    ProductWeightToleranceRepository weightToleranceRepository;
    @Inject
    ShopRepository shopRepository;
    @javax.inject.Inject
    MigrateProductsToPublicTask productsToPublicTask;
    @javax.inject.Inject
    GenerateBarcodesTask generateBarcodesTask;
    @javax.inject.Inject
    RolesUpdateTask rolesUpdateTask;
    @javax.inject.Inject
    BackgroundTaskManager backgroundTaskManager;
    @Inject
    WeedmapAccountRepository weedmapAccountRepository;
    @Inject
    MigrateConsumerCartTask migrateConsumerCartTask;
    @Inject
    ProductUpdatePriceBreaksTask productUpdatePriceBreaksTask;
    @Inject
    ProductTypeMigration productTypeMigration;
    @Inject
    ProductBatchSetArchivedTask productBatchSetArchivedTask;
    @Inject
    FixCustomProductTaxesTask fixCustomProductTaxesTask;
    @Inject
    private PromotionRuleMigrationTask promotionRuleMigrationTask;
    @Inject
    private GenerateThumbnailsTask generateThumbnailsTask;
    @Inject
    private BatchQuantityMigrationTask batchQuantityMigrationTask;
    @Inject
    private SetShopForEmployeesTask setShopForEmployeesTask;
    @Inject
    private PromotionPromocodeMigration promotionPromocodeMigration;
    @Inject
    private MemberSearchTextMigration memberSearchTextMigration;
    @Inject
    private MemberIdentificationExpiryDateMigration memberIdentificationExpiryDateMigration;
    @Inject
    private HypurIndexMigration hypurIndexMigration;

    @Inject
    private MarketingJobTask marketingJobTask;

    @Inject
    private CompoundTaxTableMigration compoundTaxTableMigration;

    @Inject
    private DispatcherRoleMigration dispatcherRoleMigration;
    @Inject
    private FixTaxTableMigrationIssue fixTaxTableMigrationIssue;
    @Inject
    private CreatePartnerKeysTask createPartnerKeysTask;
    @Inject
    SetConsumerCleanPhoneNumberTask setConsumerCleanPhoneNumberTask;
    @Inject
    StateCannabisLimitRepository stateCannabisLimitRepository;
    @Inject
    private AppRolePermissionTask appRolePermissionTask;
    @Inject
    private ExchangeInventoryTask exchangeInventoryTask;
    @Inject
    private FixPriceBreakTask fixPriceBreakTask;
    @Inject
    private UpdateBatchQuantityMigrationTask updateBatchQuantityMigrationTask;
    @Inject
    private SyncToElasticSearchV1Task syncToElasticSearchV1Task;
    @Inject
    private MigrateCultivationTaxTask migrateCultivationTaxTask;
    @Inject
    private MigrateConnectProductTask migrateConnectProductTask;
    @Inject
    private InventoryCleanupMigration inventoryCleanupMigration;
    @Inject
    private InventoryActionMigration inventoryActionMigration;
    @Inject
    private MigrateCompanyEmployeeTask migrateCompanyEmployeeTask;
    @Inject
    private MigrateCollectionIndexes migrateCollectionIndexes;
    @Inject
    private MigrateIntegrationSettings migrateIntegrationSettings;
    @Inject
    private MigrateLinxIntegrationSettings migrateLinxIntegrationSettings;
    @Inject
    private CreatePaymentOptionsMigration createPaymentOptionsMigration;
    @Inject
    private MigrateCompanyBounceNumbers migrateCompanyBounceNumbers;
    @Inject
    private ConsumerUserMigration consumerUserMigration;
    @Inject
    private CompanyLicenseMigration companyLicenseMigration;
    @Inject
    private TerminalSalesMigration terminalSalesMigration;
    @Inject
    private MigrateProductInStockTask migrateProductInStockTask;
    @Inject
    private DriverRoleMigration driverRoleMigration;
    @Inject
    private TerminalCheckoutMigration terminalCheckoutMigration;
    @Inject
    private OnPremSyncService onPremSyncService;
    @Inject
    private ProductMappingMigrationTask productMappingMigrationTask;

    @Override
    public void migrate() {
        /*  onPremSyncService.initialDownload();*/
        onPremSyncService.initialDownload(); // download data from cloud

        App app = appRepository.getAppByName(App.BLAZE_APP_NAME);
        if (connectConfiguration.isEnableMigration() == false) {
            return;
        }
        /*For creating and updating indexes*/

        backgroundTaskManager.runTaskInBackground(migrateCollectionIndexes);

        int version = app.getVersion();
        if (app.getVersion() <= 1) {
            runMigration1();

            version = 2;
        }

        if (app.getVersion() <= 2) {
            runMigration2();
            version = 3;
        }

        if (app.getVersion() <= 3) {
            // Migrate product price ranges
            migrateProductPriceRanges();
            version = 4;
        }

        if (app.getVersion() <= 4) {
            migrateRefundTransactions();
            version = 5;
        }
        if (app.getVersion() <= 5) {
            migrateRoles();
            version = 6;
        }

        if (app.getVersion() <= 6) {
            fixMissingOrderItemId();
            version = 7;
        }

        if (app.getVersion() <= 7) {
            takeSnapshotsOfAllProducts();
            version = 8;
        }

        if (app.getVersion() <= 8) {
            fixMembers();
            version = 9;
        }

        if (app.getVersion() <= 9) {
            updateWeightTolerancesWithWeightKeys();
            setShopTimeZone();
            version = 10;
        }


        if (app.getVersion() <= 12) {
            try {
                rolesUpdateTask.execute(null, null);
                version = 13;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (app.getVersion() <= 13) {
            updateWeightTolerancesWithWeightKeys();
            version = 14;
        }

        if (app.getVersion() <= 14) {
            migrateWeedAccountApiKey();
            version = 15;
        }

        if (app.getVersion() <= 15) {
            backgroundTaskManager.runTaskInBackground(migrateConsumerCartTask);
            version = 16;
        }
        //if (app.getVersion() <= 16) {
        version = 17;
        //}

        if (app.getVersion() <= 17) {
            migrateShopDeliveryFee();
            version = 18;
        }

        if (app.getVersion() <= 18) {
            backgroundTaskManager.runTaskInBackground(fixCustomProductTaxesTask);
            version = 19;
        }

        backgroundTaskManager.runTaskInBackground(productBatchSetArchivedTask);
        if (app.getVersion() <= 19) {
            updateProductToShowInWidget();
            version = 20;

        }

        if (app.getVersion() <= 20) {
            backgroundTaskManager.runTaskInBackground(promotionRuleMigrationTask);
            version = 21;
        }

        if (app.getVersion() <= 21) {
            backgroundTaskManager.runTaskInBackground(promotionPromocodeMigration);
            version = 22;
        }

        if (app.getVersion() <= 22) {
            backgroundTaskManager.runTaskInBackground(memberSearchTextMigration);
            version = 23;
        }

        if (app.getVersion() <= 23) {
            backgroundTaskManager.runTaskInBackground(dispatcherRoleMigration);
            version = 24;
        }

        if (app.getVersion() <= 24) {
            backgroundTaskManager.runTaskInBackground(memberIdentificationExpiryDateMigration);
            version = 25;
        }

        if (app.getVersion() <= 25) {
            backgroundTaskManager.runTaskInBackground(hypurIndexMigration);
            version = 26;
        }

        if (app.getVersion() <= 26) {
            backgroundTaskManager.runTaskInBackground(fixTaxTableMigrationIssue);
            version = 27;
        }

        if (app.getVersion() <= 27) {
            backgroundTaskManager.runTaskInBackground(createPartnerKeysTask);
            version = 28;
        }

        //if (app.getVersion() <= 28) {
        migrateStateLimits();
        //  version = 29;
        //}

        if (app.getVersion() <= 28) {
            backgroundTaskManager.runTaskInBackground(appRolePermissionTask);
            version = 29;
        }

        if (app.getVersion() <= 29) {
            backgroundTaskManager.runTaskInBackground(exchangeInventoryTask);
            version = 30;
        }

        if (app.getVersion() <= 30) {
            backgroundTaskManager.runTaskInBackground(fixPriceBreakTask);
            version = 31;
        }

        if (app.getVersion() == 31) {
            backgroundTaskManager.runTaskInBackground(updateBatchQuantityMigrationTask);
            version = 32;
        }

        if (app.getVersion() <= 32) {
            backgroundTaskManager.runTaskInBackground(syncToElasticSearchV1Task);
            version = 33;
        }

        if (app.getVersion() <= 33) {
            backgroundTaskManager.runTaskInBackground(migrateCultivationTaxTask);
            version = 34;
        }

        if (app.getVersion() <= 34) {
            backgroundTaskManager.runTaskInBackground(migrateConnectProductTask);
            version = 35;
        }

        if (app.getVersion() <= 35) {
            backgroundTaskManager.runTaskInBackground(inventoryCleanupMigration);
            backgroundTaskManager.runTaskInBackground(inventoryActionMigration);
            version = 36;
        }

        if (app.getVersion() <= 36) {
            backgroundTaskManager.runTaskInBackground(migrateCompanyEmployeeTask);
            version = 37;
        }

        if (app.getVersion() <= 37) {
            backgroundTaskManager.runTaskInBackground(migrateCollectionIndexes);
            version = 38;
        }


        if (app.getVersion() <= 38) {
            backgroundTaskManager.runTaskInBackground(migrateCompanyBounceNumbers);
            version = 39;
        }

        if (app.getVersion() <= 39) {
            backgroundTaskManager.runTaskInBackground(migrateIntegrationSettings);
            version = 40;
        }

        if (app.getVersion() <= 40) {
            backgroundTaskManager.runTaskInBackground(migrateLinxIntegrationSettings);
            backgroundTaskManager.runTaskInBackground(createPaymentOptionsMigration);
            version = 41;
        }

        if (app.getVersion() <= 41) {
            backgroundTaskManager.runTaskInBackground(consumerUserMigration);
            version = 42;
        }

        if (app.getVersion() <= 42) {
            backgroundTaskManager.runTaskInBackground(companyLicenseMigration);
            version = 43;
        }

        if (app.getVersion() <= 43) {
            backgroundTaskManager.runTaskInBackground(terminalSalesMigration);
            version = 44;
        }

        if (app.getVersion() <= 44) {
            backgroundTaskManager.runTaskInBackground(migrateProductInStockTask);
            version = 45;
        }

        if (app.getVersion() <= 45) {
            backgroundTaskManager.runTaskInBackground(driverRoleMigration);
            version = 46;
        }

        if (app.getVersion() <= 46) {
            backgroundTaskManager.runTaskInBackground(terminalCheckoutMigration);
            version = 47;
        }
        if (app.getVersion() <= 47) {
            backgroundTaskManager.runTaskInBackground(migrateCultivationTaxTask);
            version = 48;
        }
        if (app.getVersion() <= 48) {
            backgroundTaskManager.runTaskInBackground(productMappingMigrationTask);
            version = 49;
        }
        if (app.getVersion() < version) {
            app.setVersion(version);
            appRepository.update(app.getId(), app);
        }

        //upgradeWeightTolerance();


        //backgroundTaskManager.runTaskInBackground(productUpdatePriceBreaksTask);
        //backgroundTaskManager.runTaskInBackground(productTypeMigration);
        // regen barcodes if needed
        //backgroundTaskManager.runTaskInBackground(generateBarcodesTask);
        // Migrate images possible
        //backgroundTaskManager.runTaskInBackground(productsToPublicTask);
        // gen thumbs
        //backgroundTaskManager.runTaskInBackground(generateThumbnailsTask);
        // batch quantities
        //backgroundTaskManager.runTaskInBackground(batchQuantityMigrationTask);
        // set employees that do not have a shop
        //backgroundTaskManager.runTaskInBackground(setShopForEmployeesTask);
        //Set type in marketing job
        // backgroundTaskManager.runTaskInBackground(marketingJobTask);

        //backgroundTaskManager.runTaskInBackground(compoundTaxTableMigration);
        //backgroundTaskManager.runTaskInBackground(setConsumerCleanPhoneNumberTask);
    }

    private void runMigration1() {
        // Update roles
        HashMap<String, Role> roleHashMap = new HashMap<>();
        for (Role role : defaultData.getRoles()) {
            roleHashMap.put(role.getName(), role);
        }

        Iterable<Role> dbRoles = roleRepository.list();
        boolean hasInactive = false;
        for (Role dbRole : dbRoles) {
            Role r = roleHashMap.get(dbRole.getName());
            dbRole.setPermissions(r.getPermissions());
            roleRepository.update(dbRole.getId(), dbRole);
            if (dbRole.getName().equalsIgnoreCase("Inactive")) {
                hasInactive = true;
            }
        }

        if (!hasInactive) {
            Iterable<Company> companies = companyRepository.list();
            Role inactiveRole = roleHashMap.get("Inactive");
            for (Company company : companies) {
                inactiveRole.setId(null);
                inactiveRole.setCompanyId(company.getId());
                roleRepository.save(inactiveRole);
            }
        }


    }

    private void runMigration2() {
        final ObjectMapper objectMapper = environment.getObjectMapper();
        InputStream stream = ConnectMigration.class.getResourceAsStream("/globaldata.json");

        try {
            GlobalData data = objectMapper.readValue(stream, GlobalData.class);

            ConnectProduct defaultProduct = null;
            long count = connectProductRepository.count();
            if (count == 0) {
                for (ConnectProduct product : data.getConnectProducts()) {
                    product.prepare();
                    if (product.getProductSKU().equalsIgnoreCase("deliveryp1")) {
                        defaultProduct = product;
                    }
                }
                connectProductRepository.save(data.getConnectProducts());
            }

            Iterable<Company> iters = companyRepository.iterator();
            if (defaultProduct != null) {
                for (Company company : iters) {
                    if (StringUtils.isBlank(company.getProductSKU())) {
                        company.setProductSKU(defaultProduct.getProductSKU());
                        companyRepository.update(company.getId(), company);
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void migrateProductPriceRanges() {
        List<Product> products = productRepository.list();
        for (Product product : products) {
            boolean shouldSave = false;
            for (ProductPriceRange range : product.getPriceRanges()) {
                shouldSave = true;
                range.setId(String.format("%s_%s", product.getId(), range.getWeightToleranceId()));
            }
            if (shouldSave) {
                productRepository.updateProductPriceRanges(product.getCompanyId(), product.getId(), product.getPriceRanges());
            }
        }
    }

    private void migrateRefundTransactions() {
        Iterable<Transaction> refundedItems = transactionRepository.getAllTransactionsByType(Transaction.TransactionType.Refund);

        int fixed = 0;
        for (Transaction transaction : refundedItems) {
            fixed++;
            double totalRefund = 0.0;
            double refundWithoutDiscount = 0.0;
            double subTotalWithoutDiscount = 0.0;
            double subTotalWithDiscount = 0.0;
            double discountedRefunded = 0.0;
            double totalDiscounts = 0.0;

            for (OrderItem orderItem : transaction.getCart().getItems()) {
                // If this order item is going to be refunded, just move along

                if (orderItem.getDiscountType() == OrderItem.DiscountType.Cash) {
                    totalDiscounts += orderItem.getDiscount().doubleValue();
                } else {
                    totalDiscounts += orderItem.getCost().doubleValue() * (orderItem.getDiscount().doubleValue() / 100);
                }

                if (orderItem.getStatus() == OrderItem.OrderItemStatus.Refunded) {
                    totalRefund += orderItem.getFinalPrice().doubleValue();
                    if (orderItem.getDiscount().doubleValue() == 0) {
                        refundWithoutDiscount += orderItem.getFinalPrice().doubleValue();
                    }
                    if (orderItem.getDiscountType() == OrderItem.DiscountType.Cash) {
                        discountedRefunded += orderItem.getDiscount().doubleValue();
                    } else {
                        discountedRefunded += orderItem.getCost().doubleValue() * (orderItem.getDiscount().doubleValue() / 100);
                    }
                    continue;
                }


                if (orderItem.getDiscount().doubleValue() <= 0) {
                    // already discounted
                    subTotalWithoutDiscount += orderItem.getFinalPrice().doubleValue();
                } else {
                    subTotalWithDiscount += orderItem.getFinalPrice().doubleValue();
                }
            }

            if (transaction.getTransType() != Transaction.TransactionType.Adjustment) {
                // calculate total refund
                if (transaction.getCart().getDiscountType() == OrderItem.DiscountType.Cash) {
                    if (refundWithoutDiscount > 0) {
                        // There are items in the refund without discounts, so let's minus the discount amount
                        totalRefund -= transaction.getCart().getDiscount().doubleValue();
                    }
                } else {
                    totalRefund -= refundWithoutDiscount * transaction.getCart().getDiscount().doubleValue() / 100;
                }

                totalRefund = totalRefund + totalRefund * transaction.getCart().getTax().doubleValue();

                double roundedRefundAmount = NumberUtils.round(totalRefund, 2);
                transaction.getCart().setRefundAmount(new BigDecimal(roundedRefundAmount)); //roundedRefundAmount);

                // Calculate subtotal
                double subTotalPreDiscount = subTotalWithoutDiscount + subTotalWithDiscount;
                transaction.getCart().setSubTotal(new BigDecimal(subTotalPreDiscount)); //NumberUtils.round(subTotalPreDiscount,2));

                // Calculate Discount
                if (subTotalWithoutDiscount > 0) {
                    if (transaction.getCart().getDiscountType() == OrderItem.DiscountType.Cash) {
                        subTotalWithoutDiscount = subTotalWithoutDiscount - transaction.getCart().getDiscount().doubleValue();
                        totalDiscounts = totalDiscounts + transaction.getCart().getDiscount().doubleValue();
                    } else {
                        double cartDiscount = (subTotalWithoutDiscount * transaction.getCart().getDiscount().doubleValue() / 100);
                        subTotalWithoutDiscount = subTotalWithoutDiscount - cartDiscount;

                        totalDiscounts = totalDiscounts + cartDiscount;
                    }
                }


                // Calculate subtotal with discounts
                double subTotal = subTotalWithoutDiscount + subTotalWithDiscount;
                transaction.getCart().setSubTotalDiscount(new BigDecimal(subTotal)); //NumberUtils.round(subTotal,2));

                // reset total discounts amount (adhering to refunded items)
                double currentDiscount = totalDiscounts; //transaction.getCart().getTotalDiscount();
                currentDiscount -= discountedRefunded;

                if (transaction.getCart().getTotal().doubleValue() <= 0) {
                    currentDiscount = 0;
                }
                // zero out just in case
                if (currentDiscount < 0) {
                    currentDiscount = 0;
                }


                transaction.getCart().setTotalDiscount(new BigDecimal(currentDiscount)); //currentDiscount);
            }
            transactionRepository.update(transaction.getId(), transaction);
        }

        LOG.info("Refund Transactions Migrated: " + fixed);
    }

    private void migrateRoles() {
        List<Role> roles = roleRepository.list();
        int fixed = 0;
        for (Role role : roles) {
            if (role.getName().equalsIgnoreCase("Delivery Driver") || role.getName().equalsIgnoreCase("Budtender")) {
                if (role.getPermissions() != null) {
                    role.getPermissions().add(Role.Permission.iPadApplyCartDiscounts);
                    role.getPermissions().add(Role.Permission.iPadApplyLineItemDiscounts);
                    roleRepository.update(role.getId(), role);
                    fixed++;
                }
            }
        }
        LOG.info("Roles Migrated: " + fixed);
    }

    private void fixMissingOrderItemId() {
        Iterable<Transaction> transactions = transactionRepository.getTransactionsWithoutOrderId();
        for (Transaction transaction : transactions) {
            boolean shouldUpdate = false;
            if (transaction.getCart() == null || transaction.getCart().getItems() == null) {
                continue;
            }
            for (OrderItem orderItem : transaction.getCart().getItems()) {
                if (StringUtils.isBlank(orderItem.getOrderItemId())) {
                    orderItem.setOrderItemId(UUID.randomUUID().toString());
                    shouldUpdate = true;
                }
            }
            if (shouldUpdate) {
                transactionRepository.update(transaction.getId(), transaction);
            }
        }
    }


    private void takeSnapshotsOfAllProducts() {
        Iterable<Product> products = productRepository.list();
        List<ProductChangeLog> productChangeLogs = new ArrayList<>();
        for (Product p : products) {
            productChangeLogs.add(createProductChangeLog("", p, "Inventory Snapshot"));
        }
        if (productChangeLogs.size() > 0) {
            productChangeLogRepository.save(productChangeLogs);
        }
    }

    private void fixMembers() {
        Iterable<Member> members = memberRepository.getMembersOlderThan(DateTime.now().getMillis());

        for (Member member : members) {
            if (member.getDob() != null) {
                long date = member.getDob();
                DateTime dateTime = new DateTime(date);
                if (dateTime.getYear() >= 2030) {
                    long newDB = dateTime.minusYears(100).getMillis();
                    member.setDob(newDB);
                    memberRepository.update(member.getCompanyId(), member.getId(), member);
                }
            }
        }

    }

    private void setShopTimeZone() {
        shopRepository.setShopTimeZone(ConnectAuthToken.DEFAULT_REQ_TIMEZONE);
    }

    private ProductChangeLog createProductChangeLog(String employeeId, Product product, String reference) {

        // Add change log
        ProductChangeLog changeLog = new ProductChangeLog();
        changeLog.prepare(product.getCompanyId());
        changeLog.setShopId(product.getShopId());
        changeLog.setProductId(product.getId());
        changeLog.setEmployeeId(employeeId);
        changeLog.setReference(reference);
        changeLog.setProductQuantities(product.getQuantities());
        return changeLog;
    }

    private void upgradeWeightTolerance() {
        Iterable<ProductWeightTolerance> tolerances = weightToleranceRepository.list();
        List<ProductWeightTolerance> myTolerances = Lists.newArrayList(tolerances);
        HashMap<String, ProductWeightTolerance> toleranceHashMap = new HashMap<>();
        for (ProductWeightTolerance tolerance : tolerances) {
            String key = tolerance.getCompanyId() + "_" + tolerance.getWeightKey().weightName;
            toleranceHashMap.put(key, tolerance);
        }
        Iterable<Company> companies = companyRepository.list();
        List<ProductWeightTolerance> toleranceToAdd = new ArrayList<>();
        for (Company company : companies) {
            List<ProductWeightTolerance> newTolerances = defaultData.getProductTolerances();
            for (ProductWeightTolerance tolerance : newTolerances) {
                String key = company.getId() + "_" + tolerance.getWeightKey().weightName;
                if (!toleranceHashMap.containsKey(key)) {
                    ProductWeightTolerance copy = (new Kryo()).copy(tolerance);
                    copy.setCompanyId(company.getId());
                    copy.setEnabled(false);
                    copy.setId(null);

                    toleranceToAdd.add(copy);
                    toleranceHashMap.put(key, copy);
                }
            }
        }

        // Now save the new tolerances
        LOG.info("New Tolerances: " + toleranceToAdd.size());
        weightToleranceRepository.save(toleranceToAdd);


        Iterable<ProductWeightTolerance> updatedTolerances = weightToleranceRepository.list();
        int updatedTolerancesCount = 0;
        for (ProductWeightTolerance tolerance : updatedTolerances) {
            for (ProductWeightTolerance defaultTolerance : defaultData.getProductTolerances()) {
                if (tolerance.getWeightKey() == defaultTolerance.getWeightKey()) {
                    if (tolerance.getPriority() != defaultTolerance.getPriority()) {
                        tolerance.setPriority(defaultTolerance.getPriority());
                        tolerance.setName(defaultTolerance.getName());
                        updatedTolerancesCount++;
                        weightToleranceRepository.update(tolerance.getCompanyId(), tolerance.getId(), tolerance);
                    }
                }
            }
        }


        LOG.info("Updated Tolerances with new priority: " + updatedTolerancesCount);
    }

    private void updateWeightTolerancesWithWeightKeys() {

        Iterable<ProductWeightTolerance> tolerances = weightToleranceRepository.list();
        HashMap<String, ProductWeightTolerance> toleranceHashMap = new HashMap<>();
        int totalTolerance = 0;
        for (ProductWeightTolerance tolerance : tolerances) {
            //if (tolerance.getWeightValue() == null) {
            ProductWeightTolerance.WeightKey weightKey = ProductWeightTolerance.WeightKey.getKey(tolerance.getName());
            tolerance.setWeightKey(weightKey);
            //tolerance.setStartWeight(tolera//NumberUtils.round(tolerance.getStartWeight().doubleValue(),2));
            //tolerance.setEndWeight(NumberUtils.round(tolerance.getEndWeight().doubleValue(),2));
            tolerance.setWeightValue(weightKey.weightValue);
            //tolerance.setUnitValue(NumberUtils.round(tolerance.getUnitValue(), 2));
            weightToleranceRepository.update(tolerance.getId(), tolerance);
            totalTolerance++;
            //}

            toleranceHashMap.put(tolerance.getId(), tolerance);
        }
        LOG.info("Tolerances updated: " + totalTolerance);


        int productCount = 0;
        List<Product> products = productRepository.list();
        for (Product product : products) {
            boolean shouldUpdate = false;
            if (product.getPriceRanges().size() > 0) {
                for (ProductPriceRange pr : product.getPriceRanges()) {
                    ProductWeightTolerance tolerance = toleranceHashMap.get(pr.getWeightToleranceId());
                    if (tolerance != null) {
                        pr.setWeightTolerance(tolerance);
                    }
                }
/*
                for (ProductQuantity productQuantity : product.getQuantities()) {
                    productQuantity.setQuantity(//NumberUtils.round(productQuantity.getQuantity(),2));
                }
  */
                shouldUpdate = true;
                productCount++;
            }

            if (shouldUpdate) {
                productRepository.updateProductPriceRanges(product.getCompanyId(), product.getId(), product.getPriceRanges());
            }
        }

        LOG.info("Products updated: " + productCount);
    }

    /*private void testGenPassword() {
        String password = "alliance1";
        String encrypted = securityUtil.encryptPassword(password);
        System.out.println(password);
    }*/

    /**
     * Migrate VENDOR_KEY to ApiKeyList for Weedmap Account
     */
    private void migrateWeedAccountApiKey() {

        List<WeedmapAccount> weedmapAccounts = weedmapAccountRepository.list();
        for (WeedmapAccount account : weedmapAccounts) {

            String apiKey = account.getApiKey();

            if (StringUtils.isNotEmpty(apiKey)) {
                WeedmapApiKeyMap weedmapApiKeyMap = new WeedmapApiKeyMap();
                weedmapApiKeyMap.setApiKey(apiKey);
                weedmapApiKeyMap.setName("apiKey1");

                List<WeedmapApiKeyMap> apiKeyList = new ArrayList<>();
                apiKeyList.add(weedmapApiKeyMap);
                account.setApiKeyList(apiKeyList);
                weedmapAccountRepository.save(account);
            }
        }
    }

    /**
     * Migrate DeliveryFee to List<DeliveryFee> for Shop
     */
    private void migrateShopDeliveryFee() {
        List<Shop> shops = shopRepository.list();
        for (Shop shop : shops) {
            BigDecimal deliveryFee = shop.getDeliveryFee();

            if (deliveryFee != null) {
                DeliveryFee fee = new DeliveryFee();
                fee.prepare(shop.getCompanyId());
                fee.setFee(deliveryFee);
                fee.setSubTotalThreshold(BigDecimal.ZERO);
                fee.setEnabled(shop.isEnableDeliveryFee()); // enable the delivery fee if it was previously enabled

                List<DeliveryFee> deliveryFees = new ArrayList<>();
                deliveryFees.add(fee);
                shop.setDeliveryFees(deliveryFees);
                shopRepository.save(shop);
            }
        }
    }

    /**
     * Update Products to Show in Widgets
     */
    private void updateProductToShowInWidget() {
        List<Product> products = productRepository.list();
        for (Product product : products) {
            product.setShowInWidget(true);
            productRepository.save(product);
        }
    }

    private void migrateStateLimits() {
        Long stateLimit = stateCannabisLimitRepository.count();
        if (stateLimit == 0) {
            LOG.info("Auto creating state limits");
            List<StateCannabisLimit> stateCannabisLimits = new ArrayList<>();
            for (USState state : USState.values()) {
                for (ConsumerType consumerType : ConsumerType.values()) {
                    StateCannabisLimit limit = new StateCannabisLimit();
                    limit.prepare();
                    limit.setState(state);
                    limit.setConsumerType(consumerType);
                    if (state == USState.CA) {
                        if (consumerType == ConsumerType.AdultUse) {
                            limit.setConcentrates(new BigDecimal(8));
                            limit.setNonConcentrates(new BigDecimal(28.5));
                            limit.setPlants(new BigDecimal(6));
                            limit.setDuration(1);
                        } else {
                            limit.setConcentrates(new BigDecimal(-1));
                            limit.setNonConcentrates(new BigDecimal(-1));
                            limit.setPlants(new BigDecimal(6));
                            limit.setMax(ProductWeightTolerance.WeightKey.OUNCE.weightValue.multiply(new BigDecimal(8))); // 8oz in grams
                            limit.setDuration(1);
                        }
                    }
                    stateCannabisLimits.add(limit);
                }
            }

            stateCannabisLimitRepository.save(stateCannabisLimits);
        }
    }
}
