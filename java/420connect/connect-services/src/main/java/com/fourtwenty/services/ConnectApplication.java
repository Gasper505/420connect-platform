package com.fourtwenty.services;

import com.blaze.clients.hypur.HypurModule;
import com.blaze.clients.metrcs.MetrcModule;
import com.blaze.scheduler.core.BlazeCoreModule;
import com.blaze.warehouse.services.BlazeWareHouseApplication;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fourtwenty.bundles.ConnectSwaggerBundle;
import com.fourtwenty.core.CoreModule;
import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.exceptions.mappers.*;
import com.fourtwenty.core.security.AuthenticationFilter;
import com.fourtwenty.dashboards.services.DashboardApplication;
import com.fourtwenty.installers.ResourceInstaller;
import com.fourtwenty.quickbook.desktop.soap.QBSyncService;
import com.google.common.collect.ImmutableMap;
import com.google.inject.AbstractModule;
import com.google.inject.Module;
import com.roskart.dropwizard.jaxws.EndpointBuilder;
import com.roskart.dropwizard.jaxws.JAXWSBundle;
import io.dropwizard.Application;
import io.dropwizard.jersey.errors.EarlyEofExceptionMapper;
import io.dropwizard.server.AbstractServerFactory;
import io.dropwizard.server.DefaultServerFactory;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.reflections.Reflections;
import org.reflections.util.ConfigurationBuilder;
import ru.vyarus.dropwizard.guice.GuiceBundle;
import ru.vyarus.dropwizard.guice.module.installer.feature.LifeCycleInstaller;
import ru.vyarus.dropwizard.guice.module.installer.feature.ManagedInstaller;
import ru.vyarus.dropwizard.guice.module.installer.feature.TaskInstaller;
import ru.vyarus.dropwizard.guice.module.installer.feature.eager.EagerSingletonInstaller;
import ru.vyarus.dropwizard.guice.module.installer.feature.health.HealthCheckInstaller;
import ru.vyarus.dropwizard.guice.module.installer.feature.jersey.JerseyFeatureInstaller;
import ru.vyarus.dropwizard.guice.module.installer.feature.jersey.provider.JerseyProviderInstaller;
import ru.vyarus.dropwizard.guice.module.installer.feature.plugin.PluginInstaller;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import javax.ws.rs.ext.ExceptionMapper;
import java.util.*;


/**
 * Created by mdo on 8/16/15.
 */
public class ConnectApplication extends Application<ConnectConfiguration> {

    static final Log LOG = LogFactory.getLog(ConnectApplication.class);
    GuiceBundle<ConnectConfiguration> guiceBundle = null;
    private JAXWSBundle jaxWsBundle = new JAXWSBundle();

    public static void main(String[] args) throws Exception {
        new ConnectApplication().run(args);
    }


    @Override
    public void initialize(Bootstrap<ConnectConfiguration> bootstrap) {

        // Find All Guice Modules via Reflection
        Module[] modules = autoDiscoverModules();


        bootstrap.getObjectMapper().registerSubtypes(DefaultServerFactory.class);
        //bootstrap.getObjectMapper().registerModule(new FCSerializerModule());
        bootstrap.getObjectMapper().enable(DeserializationFeature.READ_ENUMS_USING_TO_STRING);
        bootstrap.addBundle(jaxWsBundle);
        GuiceBundle.Builder builder = GuiceBundle.<ConnectConfiguration>builder()
                .modules(modules)
                .noDefaultInstallers()
                .installers(new Class[]{LifeCycleInstaller.class,
                        ManagedInstaller.class,
                        JerseyFeatureInstaller.class, ResourceInstaller.class,

                        JerseyProviderInstaller.class,
                        EagerSingletonInstaller.class,
                        HealthCheckInstaller.class,
                        TaskInstaller.class,
                        PluginInstaller.class
                })
                .enableAutoConfig(ConnectApplication.class.getPackage().getName(),
                        CoreModule.class.getPackage().getName(),
                        BlazeCoreModule.class.getPackage().getName(),
                        HypurModule.class.getPackage().getName(),
                        MetrcModule.class.getPackage().getName(),
                        BlazeWareHouseApplication.class.getPackage().getName(),
                        DashboardApplication.class.getPackage().getName());

        /*bootstrap.addBundle(new ConnectSwaggerBundle<ConnectConfiguration>() {
            @Override
            protected SwaggerBundleConfiguration getSwaggerBundleConfiguration(ConnectConfiguration configuration) {
                return configuration.swaggerBundleConfiguration;
            }
        });*/
        bootstrap.addBundle(new ConnectSwaggerBundle());
        postInitialize(bootstrap, builder);
        guiceBundle = builder.build();
        bootstrap.addBundle(guiceBundle);
    }

    @Override
    public void run(final ConnectConfiguration configuration, final Environment environment)
            throws Exception {
        System.setProperty("sun.net.http.allowRestrictedHeaders", "true");
        //     Endpoint.publish("/QBSyncService",new QBSyncService());


        AbstractServerFactory sf = (AbstractServerFactory) configuration.getServerFactory();
        sf.setRegisterDefaultExceptionMappers(false);
        environment.jersey().register(MultiPartFeature.class);
        environment.jersey().register(guiceBundle.getInjector().getInstance(AuthenticationFilter.class));
        //environment.jersey().register(guiceBundle.getInjector().getInstance(AppStartupListener.class));


        String corsURL = configuration.getCoreUrls();
        if (configuration.getEnv() == ConnectConfiguration.EnvironmentType.LOCAL) {
            corsURL = "*";
        }
        corsURL = "*";

        Map<String, String> corsInitParams = ImmutableMap.of(
                CrossOriginFilter.ALLOWED_ORIGINS_PARAM, corsURL,
                CrossOriginFilter.ALLOWED_METHODS_PARAM, "GET,PUT,POST,DELETE,OPTIONS",
                CrossOriginFilter.ALLOW_CREDENTIALS_PARAM, "true",
                CrossOriginFilter.ALLOWED_HEADERS_PARAM, "Content-Type,Authorization,X-Requested-With,Content-Length,Accept,Origin",
                CrossOriginFilter.ACCESS_CONTROL_ALLOW_CREDENTIALS_HEADER, "true"
        );
        jaxWsBundle.publishEndpoint(
                new EndpointBuilder("/QBSyncService", QBSyncService.getInstance()));

        String appContext = environment.getApplicationContext().getContextPath();
        FilterRegistration.Dynamic dFilter = environment.servlets().addFilter("CORSFilter", CrossOriginFilter.class);
        dFilter.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, environment.getApplicationContext().getContextPath() + "*");

        /*dFilter.setInitParameter(CrossOriginFilter.ALLOWED_METHODS_PARAM, "GET,PUT,POST,DELETE,OPTIONS");
        dFilter.setInitParameter(CrossOriginFilter.ALLOWED_ORIGINS_PARAM, configuration.getCoreUrls());
        dFilter.setInitParameter(CrossOriginFilter.ACCESS_CONTROL_ALLOW_ORIGIN_HEADER, "*");
        dFilter.setInitParameter(CrossOriginFilter.ALLOWED_HEADERS_PARAM, "Content-Type,Authorization,X-Requested-With,Content-Length,Accept,Origin");
        dFilter.setInitParameter(CrospostRunsOriginFilter.ACCESS_CONTROL_ALLOW_CREDENTIALS_HEADER, "true");
        */
        dFilter.setInitParameters(corsInitParams);
        /*FilterRegistration.Dynamic filter = environment.servlets().addFilter("CORSFilter", CrossOriginFilter.class);
        filter.addMappingForUrlPatterns(EnumSet.of(DispatcherType.REQUEST), true, environment.getApplicationContext().getContextPath() + "*");
        filter.setInitParameter(CrossOriginFilter.ALLOWED_METHODS_PARAM, "GET,PUT,POST,DELETE,OPTIONS");
        filter.setInitParameter(CrossOriginFilter.ALLOWED_ORIGINS_PARAM, "*");
        filter.setInitParameter(CrossOriginFilter.ALLOWED_HEADERS_PARAM, "Origin, Content-Type, Accept, X-Requested-With, Content-Length, Authorization");
        filter.setInitParameter(CrossOriginFilter.ALLOW_CREDENTIALS_PARAM, "true");*/


        environment.jersey().register(new ConnectExceptionMapper());
        environment.jersey().register(new JsonProcessingExceptionMapper());
        environment.jersey().register(new ConnectConstraintMapper());
        // restore other default exception mappers
        environment.jersey().register(new EarlyEofExceptionMapper());

        environment.jersey().register(new RuntimeExceptionMapper());
        environment.jersey().register(new ConnectLoggingExceptionMapper());


        LOG.info("App context " + appContext);


        // environment.getObjectMapper().registerModule()
        LOG.debug("in config");
        postRun(configuration, environment);

        LOG.info("****************STARTING APPLICATION ENVIRONMENT MODE: " + configuration.getEnv() + "*******************");
    }

    private void removeDefaultExceptionMappers(boolean deleteDefault, Environment environment) {
        if (deleteDefault) {
            ResourceConfig jrConfig = environment.jersey().getResourceConfig();
            Set<Object> dwSingletons = jrConfig.getSingletons();
            List<Object> singletonsToRemove = new ArrayList<Object>();

            for (Object singletons : dwSingletons) {
                if (singletons instanceof ExceptionMapper && !singletons.getClass().getName().contains("DropwizardResourceConfig")) {
                    singletonsToRemove.add(singletons);
                }
            }

            for (Object singletons : singletonsToRemove) {
                LOG.info("Deleting this ExceptionMapper: " + singletons.getClass().getName());
                jrConfig.getSingletons().remove(singletons);
            }
        }
    }

    protected void postRun(final ConnectConfiguration configuration, final Environment environment) throws Exception {
        // Sub-classes should
    }

    protected void postInitialize(Bootstrap<ConnectConfiguration> bootstrapm, GuiceBundle.Builder guiceBuilder) {
        // Sub-classes should
    }


    public Module[] autoDiscoverModules() {
        Reflections reflections =
                new Reflections(
                    new ConfigurationBuilder()
                        .forPackages(
                                "com.fourtwenty",
                                "com.warehouse",
                                "com.blaze.clients",
                                "com.blaze.scheduler",
                                "com.fourtwenty.reporting",
                                "com.fourtwenty.dashboards"));

        Set<Class<? extends AbstractModule>> classes = reflections.getSubTypesOf(AbstractModule.class);

        List<Module> discoveredModules = new ArrayList<>();
        for (Class clazz : classes) {
            try {
                AbstractModule module = (AbstractModule) clazz.newInstance();
                discoveredModules.add(module);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return discoveredModules.toArray(new Module[]{});
    }
}
