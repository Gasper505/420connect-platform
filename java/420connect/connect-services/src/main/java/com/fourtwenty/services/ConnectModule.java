package com.fourtwenty.services;

import com.blaze.clients.hypur.configs.HypurConfig;
import com.blaze.scheduler.core.lifecycle.BlazeSchedulerAppStartup;
import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.lifecycle.AppStartup;
import com.fourtwenty.core.lifecycle.BlazePluginStartup;
import com.fourtwenty.core.lifecycle.CoreAppStartup;
import com.fourtwenty.core.lifecycle.ElasticSearchStartup;
import com.fourtwenty.core.lifecycle.Migration;
import com.fourtwenty.core.lifecycle.TwilioStartup;
import com.fourtwenty.core.lifecycle.WepayStartup;
import com.fourtwenty.migrations.ConnectMigration;
import com.fourtwenty.reporting.lifecycle.ReportingAppStartup;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.multibindings.Multibinder;

/**
 * Created by mdo on 9/6/15.
 */
public class ConnectModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(Migration.class).to(ConnectMigration.class);

        Multibinder<AppStartup> mb = Multibinder.newSetBinder(binder(), AppStartup.class);
        mb.addBinding().to(CoreAppStartup.class);
        mb.addBinding().to(BlazeSchedulerAppStartup.class);
        mb.addBinding().to(WepayStartup.class);
        mb.addBinding().to(TwilioStartup.class);
        mb.addBinding().to(BlazePluginStartup.class);
        mb.addBinding().to(ElasticSearchStartup.class);
        mb.addBinding().to(ReportingAppStartup.class);
    }


    @Provides
    public HypurConfig getHypurConfig(ConnectConfiguration configuration) {
        HypurConfig hypurConfig = configuration.getHypurConfig();
        return hypurConfig;
    }
}
