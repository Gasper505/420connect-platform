package com.fourtwenty.services.health;

import com.codahale.metrics.health.HealthCheck;
import ru.vyarus.dropwizard.guice.module.installer.feature.health.NamedHealthCheck;

/**
 * Created by mdo on 8/16/15.
 */
public class ConnectHealth extends NamedHealthCheck {

    @Override
    public String getName() {
        return "ConnectHealth";
    }

    @Override
    protected HealthCheck.Result check() throws Exception {
        return HealthCheck.Result.healthy();
    }
}
