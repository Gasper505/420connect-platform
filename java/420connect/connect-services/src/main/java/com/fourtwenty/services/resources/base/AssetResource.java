package com.fourtwenty.services.resources.base;

import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.rest.dispensary.results.common.AssetStreamResult;
import com.fourtwenty.core.security.dispensary.Audit;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.mgmt.AssetService;
import com.google.common.io.ByteStreams;
import io.dropwizard.jersey.caching.CacheControl;
import io.swagger.annotations.ApiOperation;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataParam;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.concurrent.TimeUnit;

/**
 * Created by mdo on 10/27/15.
 */
public class AssetResource extends BaseResource {
    @Inject
    protected AssetService assetService;


    @POST
    @Path("/photo")
    @Secured
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Audit(action = "Upload Photo", json = false)
    @ApiOperation(value = "Upload Photo")
    public CompanyAsset uploadPhoto(@FormDataParam("file") InputStream inputStream,
                                    @FormDataParam("name") String name,
                                    @FormDataParam("file") final FormDataBodyPart body) {
        String mimeType = body.getMediaType().toString();
        return assetService.uploadAssetPrivate(inputStream, name, CompanyAsset.AssetType.Photo, mimeType);
    }

    @POST
    @Path("/photo/public")
    @Secured
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Audit(action = "Upload Photo", json = false)
    @ApiOperation(value = "Upload Photo")
    public CompanyAsset uploadPhotoPublic(@FormDataParam("file") InputStream inputStream, @FormDataParam("name") String name,
                                          @FormDataParam("file") final FormDataBodyPart body, @FormDataParam("type") final CompanyAsset.AssetType type) {
        String mimeType = body.getMediaType().toString();
        return assetService.uploadAssetPublic(inputStream, name, type != null ? type : CompanyAsset.AssetType.Photo, mimeType);
    }

    @POST
    @Path("/document")
    @Secured
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Audit(action = "Upload Document", json = false)
    @ApiOperation(value = "Upload Document")
    public CompanyAsset uploadDocument(@FormDataParam("file") InputStream inputStream,
                                       @FormDataParam("name") String name,
                                       @FormDataParam("file") final FormDataBodyPart body) {
        String mimeType = body.getMediaType().toString();
        return assetService.uploadAssetPrivate(inputStream, name, CompanyAsset.AssetType.Document, mimeType);
    }

    @GET
    @Path("/{assetKey}")
    @CacheControl(maxAge = 30, maxAgeUnit = TimeUnit.DAYS)
    @Produces({"image/jpeg", "application/pdf", "image/png"})
    @ApiOperation(value = "Get Asset")
    public Response getAssetStream(@PathParam("assetKey") String assetKey, @QueryParam("assetToken") String assetToken, @QueryParam("handleError") boolean handleError, @QueryParam("reportRequest") boolean reportRequest, @QueryParam("reportName") String reportName) {
        final AssetStreamResult result = assetService.getCompanyAssetStream(assetKey, assetToken, handleError, reportRequest, reportName);
        StreamingOutput streamOutput = new StreamingOutput() {
            @Override
            public void write(OutputStream os) throws IOException,
                    WebApplicationException {
                ByteStreams.copy(result.getStream(), os);
            }
        };

        Response.ResponseBuilder responseBuilder = Response.ok(streamOutput).type(reportRequest ? MediaType.APPLICATION_OCTET_STREAM : result.getContentType());
        if(reportRequest) {
            responseBuilder.header("Content-Disposition", "attachment; filename=\"" + result.getKey() + "\"");
        }
        return responseBuilder.build();
    }

    @GET
    @Path("/combinedContract/{contractAssetKey}/{signedContractAssetKey}/{contentType}")
    @CacheControl(maxAge = 30, maxAgeUnit = TimeUnit.DAYS)
    @Produces({"image/jpeg", "application/pdf"})
    @ApiOperation(value = "Get Combined Contract")
    public Response combinedContract(@PathParam("contractAssetKey") String contractAssetKey,
                                     @PathParam("signedContractAssetKey") String signedContractAssetKey,
                                     @PathParam("contentType") String contentType,
                                     @QueryParam("assetToken") String assetToken) throws IOException {

        File downloadablePDF = assetService.getCombinedContract(contractAssetKey, signedContractAssetKey, contentType, assetToken);
        Response.ResponseBuilder response = Response.ok(downloadablePDF);
        response.header("Content-Disposition", "attachment; filename=\"signed-contract.pdf\"");
        return response.build();
    }

    @GET
    @Path("/getAgreement/{signedContractId}/{contractAssetKey}")
    @CacheControl(maxAge = 30, maxAgeUnit = TimeUnit.DAYS)
    @Produces({"image/jpeg", "application/pdf"})
    @ApiOperation(value = "Get Agreement")
    public Response getAgreement(@PathParam("signedContractId") String signedContractId,
                                 @PathParam("contractAssetKey") String contractAssetKey,
                                 @QueryParam("assetToken") String assetToken) throws IOException {

        File agreement = assetService.getAgreement(signedContractId, contractAssetKey, assetToken);
        Response.ResponseBuilder response = Response.ok(agreement);
        response.header("Content-Disposition", "attachment; filename=\"signed-contract.pdf\"");
        return response.build();
    }

    @GET
    @Path("/members/{memberId}/contracts/{signedContractId}/download")
    @CacheControl(maxAge = 30, maxAgeUnit = TimeUnit.DAYS)
    @Produces({"image/jpeg", "application/pdf"})
    @ApiOperation(value = "Get Member's Combined Contract")
    public Response combinedMemberContract(@PathParam("memberId") String memberId,
                                           @PathParam("signedContractId") String signedContractId,
                                           @QueryParam("assetToken") String assetToken) throws IOException {
        File downloadablePDF = assetService.getCombinedContractByMember(memberId, signedContractId, assetToken);
        Response.ResponseBuilder response = Response.ok(downloadablePDF);
        response.header("Content-Disposition", "attachment; filename=\"signed-contract.pdf\"");
        return response.build();
    }
}