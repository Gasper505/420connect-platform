package com.fourtwenty.services.resources.base;

import com.fourtwenty.core.domain.models.company.CashDrawerSession;
import com.fourtwenty.core.domain.models.company.DrawerStartCashUpdateRequest;
import com.fourtwenty.core.domain.models.company.PaidInOutItem;
import com.fourtwenty.core.rest.dispensary.requests.company.EndDrawerRequest;
import com.fourtwenty.core.rest.dispensary.requests.company.PaidInOutRequest;
import com.fourtwenty.core.rest.dispensary.requests.company.StartDrawerRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.company.CashDrawerSessionActivityLogResult;
import com.fourtwenty.core.rest.dispensary.results.company.CashDrawerSessionResult;
import com.fourtwenty.core.security.dispensary.Audit;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.mgmt.CashDrawerActivityLogService;
import com.fourtwenty.core.services.mgmt.CashDrawerService;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;

/**
 * Created by mdo on 12/7/16.
 */
public class CashDrawerResource extends BaseResource {
    @Inject
    protected CashDrawerService cashDrawerService;
    @Inject
    protected CashDrawerActivityLogService cashDrawerActivityLogService;

    @GET
    @Path("/")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get Cash Drawer Sessions")
    public SearchResult<CashDrawerSessionResult> getCashDrawerLogs(@QueryParam("start") int start, @QueryParam("limit") int limit) {
        return cashDrawerService.getCashDrawerLogs(start, limit);
    }


    @GET
    @Path("/{cdSessionId}")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get Cash Drawer Session By Id")
    public CashDrawerSessionResult getCashDrawerById(@PathParam("cdSessionId") String cdSessionId) {
        return cashDrawerService.getCashDrawerLogById(cdSessionId);
    }

    @GET
    @Path("/{cdSessionId}/refresh")
    @Secured(requiredShop = true)
    @Audit(action = "Refresh Drawer")
    @ApiOperation(value = "Refresh Cash Drawer Session")
    public CashDrawerSessionResult refreshCashDrawerById(@PathParam("cdSessionId") String cdSessionId) {
        return cashDrawerService.refreshCashDrawer(cdSessionId);
    }

    @POST
    @Path("/")
    @Secured(requiredShop = true, checkTerminal = true)
    @Audit(action = "Start Drawer")
    @ApiOperation(value = "Start New Cash Drawer Session")
    public CashDrawerSessionResult startDrawer(@Valid StartDrawerRequest request) {
        return cashDrawerService.startCashDrawer(request);
    }


    @POST
    @Path("/{cdSessionId}/end")
    @Secured(requiredShop = true, checkTerminal = true)
    @Audit(action = "End Drawer")
    @ApiOperation(value = "End Cash Drawer Session")
    public CashDrawerSessionResult endDrawer(@PathParam("cdSessionId") String cdSessionId,
                                             @Valid EndDrawerRequest request) {
        return cashDrawerService.endCashDrawer(cdSessionId, request);
    }

    @POST
    @Path("/{cdSessionId}/open")
    @Secured(requiredShop = true)
    @Audit(action = "Reopen Drawer")
    @ApiOperation(value = "Reopen Cash Drawer Session")
    public CashDrawerSessionResult reopenCashDrawer(@PathParam("cdSessionId") String cdSessionId) {
        return cashDrawerService.reopenCashDrawer(cdSessionId);
    }

    @PUT
    @Path("/{cdSessionId}")
    @Secured(requiredShop = true, checkTerminal = true)
    @Audit(action = "Update Drawer")
    @ApiOperation(value = "Update Cash Drawer Session")
    public CashDrawerSessionResult updateDrawer(@PathParam("cdSessionId") String cdSessionId, @Valid CashDrawerSession cashDrawerSession) {
        return cashDrawerService.editCashDrawer(cdSessionId, cashDrawerSession);
    }

    @GET
    @Path("/{cdSessionId}/paidios")
    @Secured(requiredShop = true, checkTerminal = true)
    @Audit(action = "Get Paid In/Outs")
    @ApiOperation(value = "Get Paid In/Outs")
    public SearchResult<PaidInOutItem> getCashInOuts(@PathParam("cdSessionId") String cdSessionId,
                                                     @QueryParam("start") int start,
                                                     @QueryParam("limit") int limit) {
        return cashDrawerService.getPaidIOs(cdSessionId, start, limit);
    }

    @POST
    @Path("/{cdSessionId}/paidios")
    @Secured(requiredShop = true, checkTerminal = true)
    @Audit(action = "Add Cash In Out")
    @ApiOperation(value = "Add Paid In/Out")
    public PaidInOutItem addNewCashInOuts(@PathParam("cdSessionId") String cdSessionId, @Valid PaidInOutRequest request) {
        return cashDrawerService.enterPaidIO(cdSessionId, request);
    }

    @PUT
    @Path("/{cdSessionId}/paidios/{paidIOItemId}")
    @Secured(requiredShop = true)
    @Audit(action = "Update Cash In Out")
    @ApiOperation(value = "Update Paid In/Out")
    public PaidInOutItem updateCashInOut(@PathParam("cdSessionId") String cdSessionId, @PathParam("paidIOItemId") String paidIOItemId, @Valid PaidInOutItem log) {
        return cashDrawerService.editPaidIO(paidIOItemId, log);
    }

    @DELETE
    @Path("/{cdSessionId}/paidios/{paidIOItemId}")
    @Secured(requiredShop = true, checkTerminal = true)
    @Audit(action = "Delete Cash In Out")
    @ApiOperation(value = "Delete Paid In/Out")
    public Response deleteCashInOut(@PathParam("cdSessionId") String cdSessionId, @PathParam("paidIOItemId") String paidIOItemId) {
        cashDrawerService.deletePaidIO(paidIOItemId);
        return ok();
    }


    @PUT
    @Path("/{cdSessionId}/update-start-cash")
    @Secured(requiredShop = true)
    @Audit(action = "updateStartingCash")
    @ApiOperation(value = "Update starting cash for drawer")
    public CashDrawerSessionResult updateStartingCash(@PathParam("cdSessionId") String cdSessionId, @Valid DrawerStartCashUpdateRequest drawerStartCashUpdateRequest) {
        return cashDrawerService.updateStartingCash(cdSessionId, drawerStartCashUpdateRequest);
    }


    @GET
    @Path("/{cdSessionId}/cashDrawerActivityLog")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get cash drawer activity log")
    public SearchResult<CashDrawerSessionActivityLogResult> getActiveCashDrawerActivityLogs(@PathParam("cdSessionId") String cdSessionId,
                                                                                            @QueryParam("startDate") long startDate,
                                                                                            @QueryParam("endDate") long endDate,
                                                                                            @QueryParam("start") int start,
                                                                                            @QueryParam("limit") int limit) {
        return cashDrawerActivityLogService.getActiveCaseDrawerActivityLogs(cdSessionId, startDate, endDate, start, limit);
    }

    @GET
    @Path("/active")
    @Secured(requiredShop = true)
    @Audit(action = "getCurrentActiveCashDrawer")
    @ApiOperation(value = "Get current active cash drawer")
    public SearchResult<CashDrawerSessionResult> getCurrentActiveCashDrawer() {
        return cashDrawerService.getCurrentActiveCashDrawer();
    }

}
