package com.fourtwenty.services.resources.base;

import com.fourtwenty.core.domain.models.company.TimeCard;
import com.fourtwenty.core.rest.dispensary.requests.auth.PinRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.TimeCardCompositeResult;
import com.fourtwenty.core.security.dispensary.Audit;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.mgmt.TimeCardService;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;

/**
 * Created by mdo on 6/22/16.
 */
public class TimeCardResource extends BaseResource {
    @Inject
    protected TimeCardService timeCardService;

    @GET
    @Path("/")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get Current Time Cards")
    public SearchResult<TimeCardCompositeResult> getCurrentTimeCards() {
        return timeCardService.getActiveTimeCards();
    }

    @GET
    @Path("/history")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get Time Cards History")
    public SearchResult<TimeCardCompositeResult> getTimecardHistory(@QueryParam("start") int start, @QueryParam("limit") int limit) {
        return timeCardService.getTimecardHistory(start, limit);
    }


    @POST
    @Path("/")
    @Secured(requiredShop = true)
    @Audit(action = "Clock In")
    @ApiOperation(value = "Clock In")
    public TimeCardCompositeResult clockIn(@Valid PinRequest request) {
        return timeCardService.clockIn(request);
    }


    @POST
    @Path("/{timeCardId}")
    @Secured(requiredShop = true)
    @Audit(action = "Update time card")
    @ApiOperation(value = "Update time card")
    public TimeCardCompositeResult clockIn(@PathParam("timeCardId") String timeCardId, @Valid TimeCard timeCard) {
        return timeCardService.updateTimecard(timeCardId, timeCard);
    }


    @DELETE
    @Path("/{timeCardId}")
    @Secured(requiredShop = true)
    @Audit(action = "Clock Out")
    @ApiOperation(value = "Clock Out")
    public Response clockOut(@PathParam("timeCardId") String timeCardId, @Valid PinRequest request) {
        timeCardService.clockOut(timeCardId, request);
        return ok();
    }
}
