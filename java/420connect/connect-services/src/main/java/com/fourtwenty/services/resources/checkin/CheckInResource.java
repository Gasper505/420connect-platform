package com.fourtwenty.services.resources.checkin;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.checkin.CheckIn;
import com.fourtwenty.core.rest.checkin.request.CheckInApproveRequest;
import com.fourtwenty.core.rest.checkin.request.CheckInRequest;
import com.fourtwenty.core.rest.checkin.result.CheckInResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.dispensary.Audit;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.checkin.CheckInService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Api("Management - CheckIn")
@Path("/api/v1/mgmt/checkIn")
@Produces(MediaType.APPLICATION_JSON)
public class CheckInResource extends BaseResource {

    @Inject
    private CheckInService checkInService;

    @POST
    @Path("/")
    @Timed(name = "addCheckIn")
    @ApiOperation("Add member checkIn")
    @Audit(action = "addCheckIn")
    @Secured(requiredShop = true)
    public CheckIn addCheckIn(@Valid CheckInRequest request) {
        return checkInService.addCheckIn(request);
    }

    @PUT
    @Path("/{checkInId}")
    @Timed(name = "updateCheckIn")
    @ApiOperation("Update member checkIn")
    @Audit(action = "updateCheckIn")
    @Secured(requiredShop = true)
    public CheckIn updateCheckIn(@PathParam("checkInId") String CheckInId, @Valid CheckIn request) {
        return checkInService.updateCheckIn(CheckInId, request);
    }

    @GET
    @Path("/")
    @Timed(name = "getCheckIns")
    @ApiOperation("Get member checkIn")
    @Audit(action = "getCheckIns")
    @Secured(requiredShop = true)
    public SearchResult<CheckInResult> getCheckIns(@QueryParam("startDate") String startDate,
                                                   @QueryParam("endDate") String endDate,
                                                   @QueryParam("start") int start,
                                                   @QueryParam("limit") int limit) {
        return checkInService.getCheckIns(startDate, endDate, start, limit);
    }

    @GET
    @Path("/{checkInId}")
    @Timed(name = "getCheckInById")
    @ApiOperation("Get member checkIn by id")
    @Audit(action = "getCheckInById")
    @Secured(requiredShop = true)
    public CheckInResult getCheckInById(@PathParam("checkInId") String CheckInId) {
        return checkInService.getCheckIn(CheckInId);
    }

    @POST
    @Path("/{checkInId}/accept")
    @Timed(name = "acceptCheckIn")
    @ApiOperation("Approve/Decline member checkin")
    @Audit(action = "acceptCheckIn")
    @Secured(requiredShop = true)
    public CheckIn acceptCheckIn(@PathParam("checkInId") String CheckInId, @Valid CheckInApproveRequest request) {
        return checkInService.acceptCheckIn(CheckInId, request);
    }

    @GET
    @Path("/sync")
    @Timed(name = "getCheckIns")
    @ApiOperation("Get member checkIn Using timestamp")
    @Audit(action = "getCheckIns")
    @Secured(requiredShop = true)
    public SearchResult<CheckInResult> getCheckIns(@QueryParam("startDate") long startDate,
                                                   @QueryParam("endDate") long endDate,
                                                   @QueryParam("start") int start,
                                                   @QueryParam("limit") int limit) {
        return checkInService.getCheckIns(startDate, endDate, start, limit);
    }
}
