package com.fourtwenty.services.resources.developer;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.developer.BaseDeveloperResource;
import com.fourtwenty.core.security.developer.DeveloperSecured;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.developer.DeveloperInventoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/**
 * Created by mdo on 2/2/17.
 */
@Api("Developer - Inventory")
@Path("/api/v1/public/inventory")
@Produces(MediaType.APPLICATION_JSON)
public class DeveloperInventoryResource extends BaseDeveloperResource {
    @Inject
    DeveloperInventoryService inventoryService;

    @GET
    @Path("/categories")
    @Timed(name = "getProductCategories")
    @DeveloperSecured(appType = ConnectAuthToken.ConnectAppType.Developer)
    @ApiOperation(value = "Get Product Categories")
    public SearchResult<ProductCategory> getProductCategories() {
        return inventoryService.getProductCategories();
    }


    @GET
    @Path("/categories/{categoryId}")
    @Timed(name = "getCategoryById")
    @DeveloperSecured(appType = ConnectAuthToken.ConnectAppType.Developer)
    @ApiOperation(value = "Get Product Category By Id")
    public ProductCategory getCategoryById(@PathParam("categoryId") String categoryId) {
        return inventoryService.getProductCategoryById(categoryId);
    }

    @GET
    @Path("/products")
    @Timed(name = "searchProducts")
    @DeveloperSecured(appType = ConnectAuthToken.ConnectAppType.Developer)
    @ApiOperation(value = "Search Products")
    public SearchResult<Product> searchProducts(@QueryParam("categoryId") String categoryId,
                                                @QueryParam("productId") String productId,
                                                @QueryParam("start") int start,
                                                @QueryParam("limit") int limit) {
        return inventoryService.searchProducts(categoryId, productId, start, limit);
    }
}