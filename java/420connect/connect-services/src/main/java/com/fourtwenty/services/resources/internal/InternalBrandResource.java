package com.fourtwenty.services.resources.internal;


import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.product.Brand;
import com.fourtwenty.core.rest.dispensary.requests.inventory.BrandAddRequest;
import com.fourtwenty.core.rest.dispensary.results.BrandResult;
import com.fourtwenty.core.security.dispensary.Audit;
import com.fourtwenty.core.security.internal.BaseInternalApiResource;
import com.fourtwenty.core.security.internal.InternalSecured;
import com.fourtwenty.core.security.tokens.InternalApiAuthToken;
import com.fourtwenty.core.services.mgmt.BrandService;
import com.google.inject.Inject;
import com.google.inject.Provider;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.validation.Valid;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Api("Grow - Brands")
@Path("api/v1/internal/brands")
@Produces(MediaType.APPLICATION_JSON)
public class InternalBrandResource extends BaseInternalApiResource {

    @Inject
    BrandService brandService;

    @Inject
    public InternalBrandResource(Provider<InternalApiAuthToken> tokenProvider){
        super(tokenProvider);
    }

    @POST
    @Path("/addBrand")
    @Timed(name = "addBrand")
    @InternalSecured (required = true)
    @Audit(action = "Create Brand")
    @ApiOperation(value = "Create Brand")
    public Brand createProductBrand(@Valid BrandAddRequest addRequest) {
        return brandService.addBrand(addRequest);
    }


    @PUT
    @Path("/{brandId}/updateBrand")
    @InternalSecured(required = true)
    @Timed(name = "updateBrand")
    @Audit(action = "Update Brand")
    @ApiOperation(value = "Update  Brand")
    public Brand updateBrand(@PathParam("brandId") String brandId, @Valid Brand updateRequest) {
        return brandService.updateBrand(brandId, updateRequest);
    }

    @GET
    @Path("/{brandId}/getBrandById")
    @Timed(name = "getBrandById")
    @InternalSecured(required = true)
    @ApiOperation(value = "getBrand By Id")
    public BrandResult getBrandByBrandId(@PathParam("brandId") String brandId) {

        return brandService.getBrandByBrandId(brandId);
    }

    @DELETE
    @Path("/{brandId}/deleteBrand")
    @InternalSecured(required = true)
    @Timed(name = "deleteBrand")
    @Audit(action = "Delete Brand")
    @ApiOperation(value = "Delete Brand")
    public Response deleteBrand(@PathParam("brandId") String brandId) {
        brandService.deleteBrand(brandId);
        return ok();
    }


}

