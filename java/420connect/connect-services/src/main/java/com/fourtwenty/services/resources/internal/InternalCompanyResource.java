package com.fourtwenty.services.resources.internal;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.security.dispensary.Audit;
import com.fourtwenty.core.security.internal.BaseInternalApiResource;

import com.fourtwenty.core.security.internal.InternalSecured;
import com.fourtwenty.core.security.tokens.InternalApiAuthToken;
import com.fourtwenty.core.services.mgmt.CompanyService;
import com.google.inject.Inject;
import com.google.inject.Provider;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;



import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


@Api("Grow - Company")
@Path("/api/v1/internal/company")
@Produces(MediaType.APPLICATION_JSON)
public class InternalCompanyResource extends BaseInternalApiResource {

    @Inject
    CompanyService companyService;

    @Inject
    InternalCompanyResource(Provider<InternalApiAuthToken> tokenProvider){
        super(tokenProvider);
    }

    @GET
    @Path("/")
    @Timed(name = "getCurrentCompany")
    @InternalSecured(required = true)
    @ApiOperation(value = "Get Current Company")
    public Company getCurrentCompany() {
        return companyService.getCurrentCompany();
    }

    @POST
    @Path("/")
    @Timed(name = "updateCompany")
    @InternalSecured(required = true)
    @Audit(action = "Update Company")
    @ApiOperation(value = "Update Company")
    public Company updateCompany(Company company) {
        return companyService.updateCompany(company);
    }

}
