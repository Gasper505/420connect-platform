package com.fourtwenty.services.resources.internal;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.rest.dispensary.requests.employees.EmployeeAddRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.company.EmployeeCustomResult;
import com.fourtwenty.core.rest.dispensary.results.company.EmployeeResult;
import com.fourtwenty.core.security.dispensary.Audit;
import com.fourtwenty.core.security.internal.BaseInternalApiResource;
import com.fourtwenty.core.security.internal.InternalSecured;
import com.fourtwenty.core.security.tokens.InternalApiAuthToken;
import com.fourtwenty.core.services.mgmt.EmployeeService;
import com.google.inject.Inject;
import com.google.inject.Provider;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.validation.Valid;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Api("Grow - Employee")
@Path("/api/v1/internal/employee")
@Produces(MediaType.APPLICATION_JSON)
public class InternalEmployeeResource extends BaseInternalApiResource {
    @Inject
    EmployeeService employeeService;

    @Inject
    public InternalEmployeeResource(Provider<InternalApiAuthToken> tokenProvider) {
        super(tokenProvider);
    }

    @GET
    @Path("/{employeeId}")
    @Timed(name = "getEmployees")
    @ApiOperation(value = "Get Internal Employees By Id")
    @InternalSecured(required = true)
    public EmployeeCustomResult getEmployees(@PathParam("employeeId") String employeeId) {
        return employeeService.getCustomEmployeeById(employeeId);
    }

    @GET
    @Path("/employeeList")
    @InternalSecured(required = true)
    @Timed(name = "getEmployeeList")
    @Audit(action = "Get Internal employee list")
    @ApiOperation(value = "Get Internal employee list")
    public SearchResult<EmployeeResult> getEmployeeList(@QueryParam("term") String term, @QueryParam("start") int start, @QueryParam("limit") int limit) {
        return employeeService.getEmployeeList(term, start, limit);
    }

    @POST
    @Path("/")
    @InternalSecured(required = true)
    @Timed(name = "createEmployee")
    @Audit(action = "Create Internal Employee")
    @ApiOperation(value = "Create Internal Employees")
    public Employee createEmployee(@Valid EmployeeAddRequest request) {
        return employeeService.addEmployee(request);
    }

    @POST
    @Path("/{employeeId}")
    @InternalSecured(required = true)
    @Timed(name = "updateEmployee")
    @Audit(action = "Update Internal Employee")
    @ApiOperation(value = "Update Internal Employee")
    public Employee updateEmployee(@PathParam("employeeId") String employeeId, @Valid Employee request) {
        return employeeService.updateEmployee(employeeId, request);
    }

    @DELETE
    @Path("/{employeeId}")
    @InternalSecured(required = true)
    @Timed(name = "deleteEmployee")
    @Audit(action = "Delete Internal Employee")
    @ApiOperation(value = "Delete Internal Employee")
    public Response deleteEmployee(@PathParam("employeeId") String employeeId) {
        employeeService.deleteEmployee(employeeId);
        return ok();
    }

    @GET
    @Path("/list")
    @InternalSecured
    @Timed(name = "getAllEmployeeList")
    @Audit(action = "Get all employee list")
    @ApiOperation(value = "Get all employee list")
    public SearchResult<EmployeeCustomResult> getAllEmployeeList(@QueryParam("term") String term, @QueryParam("start") int start, @QueryParam("limit") int limit) {
        return employeeService.getAllEmployeeList(term, start, limit);
    }

}
