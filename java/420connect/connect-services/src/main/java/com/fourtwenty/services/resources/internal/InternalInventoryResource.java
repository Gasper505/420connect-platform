package com.fourtwenty.services.resources.internal;


import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.product.Inventory;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.rest.dispensary.requests.inventory.InventoryAddRequest;
import com.fourtwenty.core.rest.dispensary.requests.inventory.InventoryUpdateRequest;
import com.fourtwenty.core.rest.dispensary.requests.inventory.ProductCategoryAddRequest;
import com.fourtwenty.core.rest.dispensary.results.ListResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.dispensary.Audit;
import com.fourtwenty.core.security.internal.BaseInternalApiResource;
import com.fourtwenty.core.security.internal.InternalSecured;
import com.fourtwenty.core.security.tokens.InternalApiAuthToken;
import com.fourtwenty.core.services.mgmt.InventoryService;
import com.fourtwenty.core.services.mgmt.ProductCategoryService;
import com.google.inject.Inject;
import com.google.inject.Provider;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.validation.Valid;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Api("Grow - Inventory")
@Path("/api/v1/internal/inventory")
@Produces(MediaType.APPLICATION_JSON)
public class InternalInventoryResource  extends BaseInternalApiResource {

    @Inject
    ProductCategoryService productCategoryService;
    @Inject
    InventoryService inventoryService;

    @Inject
    InternalInventoryResource(Provider<InternalApiAuthToken> tokenProvider){
        super(tokenProvider);
    }


    @POST
    @Path("/categories")
    @InternalSecured(required = true)
    @Timed(name = "createProductCategory")
    @Audit(action = "Create Product Category")
    @ApiOperation(value = "Create Product Category")
    public ProductCategory createProductCategory(@Valid ProductCategoryAddRequest request) {
        return productCategoryService.createProductCategory(request);
    }

    @PUT
    @Path("/categories/{productCategoryId}")
    @InternalSecured(required = true)
    @Timed(name = "updateProductCategory")
    @Audit(action = "Update Product Category")
    @ApiOperation(value = "Update Product Category")
    public ProductCategory updateProductCategory(@PathParam("productCategoryId") String productCategoryId,
                                                 @Valid ProductCategory category) {
        return productCategoryService.updateProductCategory(productCategoryId, category);
    }


    @GET
    @Path("/categories")
    @InternalSecured(required = true)
    @Timed(name = "getProductCategories")
    @ApiOperation(value = "Get Product Categories")
    public ListResult<ProductCategory> getProductCategories(@QueryParam("shopId") String shopId) {
        return productCategoryService.getProductCategories(shopId);
    }

    @DELETE
    @Path("/categories/{productCategoryId}")
    @InternalSecured(required = true)
    @Timed(name = "deleteProductCategory")
    @Audit(action = "Delete Product Category")
    @ApiOperation(value = "Delete Product Category")
    public Response deleteProductCategory(@PathParam("productCategoryId") String productCategoryId) {
        productCategoryService.deleteProductCategory(productCategoryId);
        return ok();
    }

    @GET
    @Path("/categories/{productCategoryId}")
    @InternalSecured(required = true)
    @Timed(name = "get Product Category by Id")
    @ApiOperation(value = "Get Product Category By Id")
    public ProductCategory getProductCategory(@PathParam("productCategoryId") String productCategoryId) {
        return productCategoryService.getProductCategoryById(productCategoryId);
    }

    @POST
    @Path("/")
    @InternalSecured(required = true)
    @Timed(name = "createInternalInventory")
    @Audit(action = "Create Internal Inventory")
    @ApiOperation(value = "Create Internal Inventory")
    public Inventory createInternalInventory(@Valid InventoryAddRequest request) {
        return inventoryService.addInventory(request);
    }

    @PUT
    @Path("/{roomId}")
    @InternalSecured(required = true)
    @Timed(name = "updateInternalInventory")
    @Audit(action = "Update Internal Inventory")
    @ApiOperation(value = "Update Internal Inventory")
    public Inventory updateInternalInventory(@PathParam("roomId") String inventoryId,
                                             @Valid InventoryUpdateRequest inventory) {
        return inventoryService.updateInventory(inventoryId, inventory);
    }

    @GET
    @Path("/")
    @InternalSecured(required = true)
    @Timed(name = "getInternalInventory")
    @ApiOperation(value = "Get Internal Inventory")
    public SearchResult<Inventory> getInternalInventory(@QueryParam("shopId") String shopId) {
        return inventoryService.getShopInventories(shopId);
    }

    @DELETE
    @Path("/{roomId}")
    @InternalSecured(required = true)
    @Timed(name = "deleteProductCategory")
    @Audit(action = "Delete Product Category")
    @ApiOperation(value = "Delete Product Category")
    public Response deleteInternalInventory(@PathParam("roomId") String inventoryId) {
        inventoryService.deleteInventory(inventoryId);
        return ok();
    }
}
