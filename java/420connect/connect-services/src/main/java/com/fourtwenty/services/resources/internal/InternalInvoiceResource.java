package com.fourtwenty.services.resources.internal;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.security.dispensary.Audit;
import com.fourtwenty.core.security.dispensary.Secured;
import com.warehouse.core.domain.models.invoice.Invoice;
import com.warehouse.core.domain.models.invoice.ShippingManifest;
import com.warehouse.core.rest.invoice.request.AddInvoiceRequest;
import com.fourtwenty.core.security.internal.BaseInternalApiResource;
import com.fourtwenty.core.security.internal.InternalSecured;
import com.fourtwenty.core.security.tokens.InternalApiAuthToken;
import com.warehouse.core.rest.invoice.request.ShippingManifestAddRequest;
import com.warehouse.core.rest.invoice.result.ShippingManifestResult;
import com.warehouse.core.services.invoice.InvoiceService;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.warehouse.core.services.invoice.ShippingManifestService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.validation.Valid;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;


@Api("Grow - Invoice")
@Path("/api/v1/internal/invoice")
@Produces(MediaType.APPLICATION_JSON)
public class InternalInvoiceResource extends BaseInternalApiResource {

    @Inject
    InvoiceService invoiceService;

    @Inject
    private ShippingManifestService shippingManifestService;

    @Inject
    public InternalInvoiceResource(Provider<InternalApiAuthToken> tokenProvider) {
        super(tokenProvider);
    }

    @GET
    @Path("/{invoiceId}")
    @Timed(name="getInvoice")
    @ApiOperation(value="Get Internal Invoice By Id")
    @InternalSecured(required =true)
    public Invoice getInvoice(@PathParam("invoiceId") String invoiceId){
        return invoiceService.getInvoiceById(invoiceId);
    }

    @POST
    @Path("/")
    @InternalSecured(required = true)
    @Timed(name="createInvoice")
    @ApiOperation(value = "Create New Invoice")
    public Invoice createInvoice(@Valid AddInvoiceRequest request) {
        return invoiceService.createInvoice(request);
    }

    @PUT
    @Path("/{invoiceId}")
    @InternalSecured(required = true)
    @Timed(name = "updateInvoice")
    @Audit(action="Update Internal Invoice")
    @ApiOperation(value = "Update An Invoice By Id")
    public Invoice updateInvoiceById(@PathParam("invoiceId") String invoiceId, @Valid Invoice invoice) {
        return invoiceService.updateInvoice(invoiceId, invoice);
    }


    @DELETE
    @Path("/{invoiceId}")
    @Timed(name = "deleteInvoice")
    @InternalSecured(requiredShop = true)
    @ApiOperation(value = "Delete An Invoice By Id")
    public Response deleteInvoiceById(@PathParam("invoiceId") String invoiceId) {
        invoiceService.deleteInvoiceById(invoiceId);
        return Response.ok().build();
    }

    @POST
    @Path("/invoiceShipping/addShippingManifest")
    @Timed(name = "addShippingManifest")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Add Shipping Manifest")
    public List<ShippingManifestResult> addShippingManifest(@Valid ShippingManifestAddRequest request) {
        return shippingManifestService.addShippingManifestForSales(request);
    }

    @PUT
    @Path("/{shippingManifestId}/updateShippingManifest")
    @Timed(name = "updateShippingManifest")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Update Shipping Manifest By Id")
    public List<ShippingManifestResult> updateShippingManifest(@PathParam("shippingManifestId") String shippingManifestId, @Valid ShippingManifest shippingManifest) {
        return shippingManifestService.updateShippingManifestForSales(shippingManifestId, shippingManifest, null);
    }
}
