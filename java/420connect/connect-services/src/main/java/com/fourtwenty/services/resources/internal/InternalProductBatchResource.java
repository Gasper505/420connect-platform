package com.fourtwenty.services.resources.internal;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.product.ProductBatch;
import com.fourtwenty.core.importer.model.CustomProductBatchResult;
import com.fourtwenty.core.rest.dispensary.requests.inventory.BatchAddRequest;
import com.fourtwenty.core.rest.dispensary.requests.inventory.UpdateProductBatchRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.dispensary.Audit;
import com.fourtwenty.core.security.internal.BaseInternalApiResource;
import com.fourtwenty.core.security.internal.InternalSecured;
import com.fourtwenty.core.security.tokens.InternalApiAuthToken;
import com.fourtwenty.core.services.mgmt.InventoryService;
import com.fourtwenty.core.services.mgmt.ProductBatchService;
import com.google.inject.Inject;
import com.google.inject.Provider;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.validation.Valid;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


@Api("Grow - Product ProductBatch")
@Path("/api/v1/internal/batch")
@Produces(MediaType.APPLICATION_JSON)
public class InternalProductBatchResource extends BaseInternalApiResource {

    @Inject
    InventoryService inventoryService;

    @Inject
    ProductBatchService productBatchService;

    @Inject
    public InternalProductBatchResource(Provider<InternalApiAuthToken> tokenProvider) {
        super(tokenProvider);
    }

    @POST
    @Path("/")
    @InternalSecured(required = true)
    @Timed(name = "createBatch")
    @Audit(action = "Create Batch")
    @ApiOperation(value = "Create New Batch")
    public ProductBatch createBatch(@Valid BatchAddRequest request) {
        return inventoryService.addBatch(request);

    }

    @POST
    @Path("/{batchId}")
    @InternalSecured(required = true)
    @Timed(name = "updateBatch")
    @Audit(action = "Update Batch")
    @ApiOperation(value = "Update Batch")
    public ProductBatch updateBatch(@PathParam("batchId") String batchId,
                                    @Valid UpdateProductBatchRequest request) {
        return inventoryService.updateBatch(batchId, request);
    }

    @GET
    @Path("/{productBatchId}")
    @Timed(name = "getProductBatch")
    @InternalSecured(required = true)
    @ApiOperation(value = "Get Product Batch")
    public CustomProductBatchResult getProductBatch(@PathParam("productBatchId") String productBatchId) {
        return productBatchService.getProductBatchWithProduct(productBatchId);
    }

    @DELETE
    @Path("/{batchId}")
    @InternalSecured(required = true)
    @Timed(name = "deleteBatch")
    @Audit(action = "Delete Batch")
    @ApiOperation(value = "Delete Batch")
    public Response deleteBatch(@PathParam("batchId") String batchId) {
        inventoryService.deleteBatch(batchId);
        return ok();
    }


    @GET
    @Path("/getAllProductBatches")
    @Timed(name = "getAllProductBatches")
    @InternalSecured(required = true)
    @ApiOperation(value = "Get All Product Batches")
    public SearchResult<CustomProductBatchResult> getAllProductBatches(@QueryParam("start") int start, @QueryParam("limit") int limit, @QueryParam("term") String searchTerm, @QueryParam("filterOption") ProductBatch.BatchFilter filter) {
        return productBatchService.getAllProductBatchesWithProduct(start, limit, searchTerm, filter);
    }
}




