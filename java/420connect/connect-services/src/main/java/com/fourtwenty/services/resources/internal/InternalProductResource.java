package com.fourtwenty.services.resources.internal;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.company.FilterType;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.rest.dispensary.requests.inventory.ProductAddRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.ProductResult;
import com.fourtwenty.core.security.dispensary.Audit;
import com.fourtwenty.core.security.internal.BaseInternalApiResource;
import com.fourtwenty.core.security.internal.InternalSecured;
import com.fourtwenty.core.security.tokens.InternalApiAuthToken;
import com.fourtwenty.core.services.mgmt.ProductService;
import com.google.inject.Inject;
import com.google.inject.Provider;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.validation.Valid;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Api("Grow - Product")
@Path("/api/v1/internal/product")
@Produces(MediaType.APPLICATION_JSON)
public class InternalProductResource extends BaseInternalApiResource {

    @Inject
    ProductService productService;

    @Inject
    public InternalProductResource(Provider<InternalApiAuthToken> tokenProvider) {
        super(tokenProvider);
    }

    @GET
    @Path("/{productId}")
    @Timed(name = "searchProducts")
    @InternalSecured(required = true, requiredShop = true)
    @ApiOperation(value = "Get Internal Product By Id")
    public Product searchProducts(@PathParam("productId") String productId) {
        return productService.getProductById(productId);
    }

    @POST
    @Path("/")
    @Timed(name = "addProduct")
    @InternalSecured(required = true, requiredShop = true)
    @Audit(action = "Create Internal Product")
    @ApiOperation(value = "Create Internal Product")
    public Product addProduct(@Valid ProductAddRequest addRequest) {
        return productService.addProduct(addRequest);
    }

    @POST
    @Path("/{productId}")
    @InternalSecured(required = true, requiredShop = true)
    @Timed(name = "updateProduct")
    @Audit(action = "Update Internal Product")
    @ApiOperation(value = "Update Internal Product")
    public Product updateProduct(@PathParam("productId") String productId, @Valid Product updateRequest) {
        return productService.updateProduct(productId, updateRequest);
    }

    @DELETE
    @Path("/{productId}")
    @InternalSecured(required = true, requiredShop = true)
    @Timed(name = "deleteProduct")
    @Audit(action = "Delete Internal Product")
    @ApiOperation(value = "Delete Internal Product")
    public Response deleteProduct(@PathParam("productId") String productId) {
        productService.deleteProduct(productId);
        return ok();
    }

    @GET
    @Path("/")
    @Timed(name = "searchProducts")
    @InternalSecured(required = true, requiredShop = true)
    @ApiOperation(value = "Search Products")
    public SearchResult<ProductResult> searchProducts(
            @QueryParam("shopId") String shopId,
            @QueryParam("categoryId") String categoryId,
            @QueryParam("productId") String productId,
            @QueryParam("start") int start,
            @QueryParam("limit") int limit,
            @QueryParam("quantity") boolean quantity,
            @QueryParam("term") String term,
            @QueryParam("inventoryId") String inventoryId,
            @QueryParam("status") FilterType status,
            @QueryParam("batchSku") String batchSku) {
        return productService.searchProductsByCategoryId(shopId, categoryId, productId, start, limit, quantity, term, inventoryId, status, batchSku);
    }
}
