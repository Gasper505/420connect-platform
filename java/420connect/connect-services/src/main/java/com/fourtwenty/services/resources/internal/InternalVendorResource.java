package com.fourtwenty.services.resources.internal;


import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.rest.dispensary.requests.inventory.VendorAddRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.VendorResult;
import com.fourtwenty.core.security.dispensary.Audit;
import com.fourtwenty.core.security.internal.BaseInternalApiResource;

import com.fourtwenty.core.security.internal.InternalSecured;
import com.fourtwenty.core.security.tokens.InternalApiAuthToken;
import com.fourtwenty.core.services.mgmt.VendorService;
import com.google.inject.Inject;
import com.google.inject.Provider;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.List;
import javax.validation.Valid;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Api("Grow - Vendors")
@Path("/api/v1/internal/vendors")
@Produces(MediaType.APPLICATION_JSON)
public class InternalVendorResource extends BaseInternalApiResource {

    @Inject
    VendorService vendorService;

    @Inject
    public InternalVendorResource(Provider<InternalApiAuthToken> tokenProvider) {
        super(tokenProvider);
    }

    @POST
    @Path("/")
    @InternalSecured(required = true)
    @Timed(name = "createVendor")
    @Audit(action = "Create Vendor")
    @ApiOperation(value = "Create Vendor")
    public Vendor createVendor(@Valid VendorAddRequest request) {
        return vendorService.addVendor(request);
    }

    @PUT
    @Path("/{vendorId}")
    @InternalSecured(required = true)
    @Timed(name = "updateVendor")
    @Audit(action = "Update Vendor")
    @ApiOperation(value = "Update Vendor")
    public Vendor updateVendor(@PathParam("vendorId") String vendorId,
                               @Valid Vendor request) {
        return vendorService.updateVendor(vendorId, request);
    }

    @GET
    @Path("/{vendorId}")
    @Timed(name = "getVendor")
    @InternalSecured(required = true)
    @ApiOperation(value = "Get Vendor By Id")
    public VendorResult getVendor(@PathParam("vendorId") String vendorId) {
        return vendorService.getVendor(vendorId);
    }

    @DELETE
    @Path("/{vendorId}")
    @InternalSecured(required = true)
    @Timed(name = "deleteVendor")
    @Audit(action = "Delete Vendor")
    @ApiOperation(value = "Delete Vendor")
    public Response deleteVendor(@PathParam("vendorId") String vendorId) {
        vendorService.deleteVendor(vendorId);
        return ok();
    }

    @GET
    @Path("/")
    @InternalSecured(required = true)
    @Timed(name = "getVendors")
    @ApiOperation(value = "Search Vendors")
    public SearchResult<VendorResult> getAllVendors(@QueryParam("vendorId") String vendorId, @QueryParam("start") int start, @QueryParam("limit") int limit, @QueryParam("type") Vendor.VendorType vendorType, @QueryParam("term") String term, @QueryParam("companyType")
        List<Vendor.CompanyType> companyType) {
        return vendorService.getVendors(vendorId, start, limit, vendorType, term, companyType);
    }


}
