package com.fourtwenty.services.resources.mgmt;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.company.Adjustment;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.dispensary.Audit;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.mgmt.AdjustmentService;
import com.google.inject.Inject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.validation.Valid;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Api("Management - Adjustments")
@Path("/api/v1/mgmt/adjustments")
@Produces(MediaType.APPLICATION_JSON)
public class AdjustmentResource extends BaseResource {

    @Inject
    private AdjustmentService adjustmentService;

    @GET
    @Path("/{adjustmentId}")
    @Timed(name = "getAdjustmentById")
    @Secured(requiredShop = true)
    @Audit(action = "Get adjustment by id")
    @ApiOperation(value = "Get adjustment by id")
    public Adjustment getAdjustmentById(@PathParam("adjustmentId") String adjustmentId) {
        return adjustmentService.getAdjustmentById(adjustmentId);
    }

    @GET
    @Path("/")
    @Timed(name = "getAdjustments")
    @Secured(requiredShop = true)
    @Audit(action = "Get adjustments list")
    @ApiOperation(value = "Get adjustments list")
    public SearchResult<Adjustment> getAdjustments(@QueryParam("start") int start, @QueryParam("limit") int limit) {
        return adjustmentService.getAdjustments(start, limit);
    }

    @POST
    @Path("/")
    @Timed(name = "addAdjustment")
    @Secured(requiredShop = true)
    @Audit(action = "Add adjustment")
    @ApiOperation(value = "Add adjustment")
    public Adjustment addAdjustment(@Valid Adjustment adjustment) {
        return adjustmentService.addAdjustment(adjustment);
    }

    @PUT
    @Path("/{adjustmentId}")
    @Timed(name = "updateAdjustment")
    @Secured(requiredShop = true)
    @Audit(action = "Update adjustment")
    @ApiOperation(value = "Update adjustment")
    public Adjustment updateAdjustment(@PathParam("adjustmentId") String adjustmentId, @Valid Adjustment adjustment) {
        return adjustmentService.updateAdjustment(adjustmentId, adjustment);
    }

    @Deprecated
    @DELETE
    @Path("/{adjustmentId}")
    @Timed(name = "deleteAdjustment")
    @Secured(requiredShop = true)
    @Audit(action = "Delete adjustment")
    @ApiOperation(value = "Delete adjustment")
    public Response deleteAdjustment(@PathParam("adjustmentId") String adjustmentId) {
//        adjustmentService.deleteAdjustment(adjustmentId);
        return Response.ok().build();
    }

}
