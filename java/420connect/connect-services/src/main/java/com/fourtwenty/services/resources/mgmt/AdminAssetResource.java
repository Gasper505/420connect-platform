package com.fourtwenty.services.resources.mgmt;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.rest.dispensary.results.common.AssetStreamResult;
import com.fourtwenty.core.rest.dispensary.results.shop.ShopAssetResult;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.services.resources.base.AssetResource;
import com.google.common.io.ByteStreams;
import io.dropwizard.jersey.caching.CacheControl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.concurrent.TimeUnit;

/**
 * Created by mdo on 12/19/15.
 */
@Api("Management - Assets")
@Path("/api/v1/mgmt/assets")
@Produces(MediaType.APPLICATION_JSON)
public class AdminAssetResource extends AssetResource {
    @GET
    @Path("/consumer/{consumerUserId}/{assetKey}")
    @CacheControl(maxAge = 30, maxAgeUnit = TimeUnit.DAYS)
    @Produces({"image/jpeg", "application/pdf"})
    @ApiOperation(value = "Get Consumer Asset")
    public Response getConsumerAssetStream(@PathParam("consumerUserId") String consumerUserId,
                                           @PathParam("assetKey") String assetKey,
                                           @QueryParam("assetToken") String assetToken) {
        final AssetStreamResult result = assetService.getConsumerUserAssetStream(consumerUserId, assetKey, assetToken);
        StreamingOutput streamOutput = new StreamingOutput() {
            @Override
            public void write(OutputStream os) throws IOException,
                    WebApplicationException {
                ByteStreams.copy(result.getStream(), os);
            }
        };

        return Response.ok(streamOutput).type(result.getContentType()).build();
    }

    @GET
    @Path("/copy/membersAsset")
    @Timed(name = "copyMemberAssetByShop")
    @Secured
    @ApiOperation(value = "Copy all assets of member's by shop")
    public ShopAssetResult copyMemberAssetByShop(@QueryParam("shopId") String shopId) {
        return assetService.copyMemberAssetByShop(shopId);
    }

    @GET
    @Path("/download/membersAsset")
    @Timed(name = "exportMemberAssetByShop")
    @Secured
    @ApiOperation(value = "Get all assets of member's by shop")
    @Produces({"application/zip"})
    public Response exportMemberAssetByShop(@QueryParam("shopAssetToken") String shopAssetToken) {
        FileInputStream fileInputStream = assetService.exportMemberAssetByShop(shopAssetToken);

        StreamingOutput streamOutput = new StreamingOutput() {
            @Override
            public void write(OutputStream os) throws IOException,
                    WebApplicationException {
                ByteStreams.copy(fileInputStream, os);
            }
        };
        return Response.ok(streamOutput).build();
    }
}
