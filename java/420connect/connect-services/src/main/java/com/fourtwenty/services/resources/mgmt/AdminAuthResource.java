package com.fourtwenty.services.resources.mgmt;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.rest.dispensary.requests.auth.EmailLoginRequest;
import com.fourtwenty.core.rest.dispensary.requests.auth.PinRequest;
import com.fourtwenty.core.rest.dispensary.requests.auth.UpdateTokenRequest;
import com.fourtwenty.core.rest.dispensary.requests.company.CompanyRegisterRequest;
import com.fourtwenty.core.rest.dispensary.results.InitialLoginResult;
import com.fourtwenty.core.rest.dispensary.results.auth.LoginResult;
import com.fourtwenty.core.rest.dispensary.results.auth.SwitchApplicationResult;
import com.fourtwenty.core.security.dispensary.Audit;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.mgmt.AuthenticationService;
import com.fourtwenty.core.util.JsonSerializer;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;


/**
 * Created by mdo on 10/2/15.
 */
@Api("Management - Authentication")
@Path("/api/v1/mgmt/session")
@Produces(MediaType.APPLICATION_JSON)
public class AdminAuthResource {
    @Inject
    AuthenticationService service;
    private static final Log LOG = LogFactory.getLog(AdminAuthResource.class);

    @ApiOperation("Get Current Session")
    @GET
    @Path("/")
    @Timed(name = "getCurrentSessionUser")
    @Secured
    public InitialLoginResult getCurrentSessionUser() {
        return service.getCurrentActiveEmployee();
    }


    @GET
    @Path("/renew")
    @Timed(name = "renewToken")
    @Secured
    @Audit(action = "Renew Token")
    @ApiOperation(value = "Renew Token")
    public LoginResult renewToken() {
        return service.renewToken();
    }

    @POST
    @Path("/")
    @Timed(name = "adminLogin")
    @Audit(action = "Admin Login")
    @ApiOperation(value = "Admin Login")
    public InitialLoginResult adminLogin(@Valid EmailLoginRequest request, @Context HttpServletResponse response) {
        InitialLoginResult result = service.adminLogin(request);
        response.setHeader("X-Auth-Token", result.getAccessToken());
        return result;
    }

    @POST
    @Path("/pin")
    @Secured
    @Timed(name = "loginWithPin")
    @Audit(action = "Login With Pin")
    @ApiOperation(value = "Login With Pin")
    public InitialLoginResult loginWithPin(@Valid PinRequest request, @Context HttpServletResponse response) {
        InitialLoginResult result = service.loginWithPin(request);
        response.setHeader("X-Auth-Token", result.getAccessToken());
        return result;
    }


    @POST
    @Path("/shop")
    @Secured
    @Timed(name = "updateTokenWithShop")
    @Audit(action = "Update Shop Token")
    @ApiOperation(value = "Switch Shop Session")
    public InitialLoginResult updateTokenWithShop(@Valid UpdateTokenRequest request, @Context HttpServletResponse response) {
        InitialLoginResult result = service.updateTokenWithShop(request);
        response.setHeader("X-Auth-Token", result.getAccessToken());
        return result;
    }

    /*

    @NotEmpty
    private String email;
    @NotEmpty
    private String address;
    @NotEmpty
    private String city;
    @NotEmpty
    private String state;
    @NotEmpty
    private String zipCode;
    @NotEmpty
    private String firstName;
    @NotEmpty
    private String lastName;
    @NotEmpty
    private String password;
    private String website;*/
    @POST
    @Path("/register")
    @Timed(name = "adminLogin")
    @Audit(action = "Register Company")
    @ApiOperation(value = "Register Company")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public InitialLoginResult createAccount(@FormParam("productSKU") String productSKU,
                                            @FormParam("name") String name,
                                            @FormParam("phoneNumber") String phoneNumber,
                                            @FormParam("email") String email,
                                            @FormParam("address") String address,
                                            @FormParam("city") String city,
                                            @FormParam("state") String state,
                                            @FormParam("zipCode") String zipCode,
                                            @FormParam("firstName") String firstName,
                                            @FormParam("lastName") String lastName,
                                            @FormParam("password") String password,
                                            @FormParam("website") String website,
                                            @FormParam("shopType") String shopType,
                                            @FormParam("isId") String isId,
                                            @FormParam("maxShops") int numShops,
                                            @FormParam("maxEmployees") int numEmployees,
                                            @FormParam("maxTerminals") int numTerms,
                                            @FormParam("maxInventories") int numInventories,

                                            @Context HttpServletResponse response) {
        Shop.ShopType type = Shop.ShopType.toShopType(shopType);

        CompanyRegisterRequest registerRequest = new CompanyRegisterRequest();
        registerRequest.setName(name);
        registerRequest.setEmail(email);
        registerRequest.setProductSKU(productSKU);
        registerRequest.setPhoneNumber(phoneNumber);
        registerRequest.setAddress(address);
        registerRequest.setCity(city);
        registerRequest.setState(state);
        registerRequest.setZipCode(zipCode);
        registerRequest.setFirstName(firstName);
        registerRequest.setLastName(lastName);
        registerRequest.setPassword(password);
        registerRequest.setWebsite(website);
        registerRequest.setShopType(type);
        registerRequest.setIsId(isId);

        registerRequest.setNumShops(numShops);
        registerRequest.setNumEmployees(numEmployees);
        registerRequest.setNumTerms(numTerms);
        registerRequest.setNumInventories(numInventories);


        String data = JsonSerializer.toJson(registerRequest);
        LOG.info("CompanyRegInfo: " + data);

        InitialLoginResult result = service.registerCompany(registerRequest);
        response.setHeader("X-Auth-Token", result.getAccessToken());
        return result;
    }

    //get current session

    @GET
    @Path("/switch-app")
    @Timed(name = "switchApplication")
    @Audit(action = "Switch Application")
    @ApiOperation(value = "Switch Application")
    public SwitchApplicationResult switchApplication(@QueryParam("accessToken") String accessToken,
                                                     @QueryParam("appName") String appName, @Context HttpServletResponse response,
                                                     @QueryParam("shopId") String shopId,
                                                     @QueryParam("growAppType") ConnectAuthToken.GrowAppType growAppType) {
        SwitchApplicationResult result = service.switchApplication(accessToken, appName, shopId, growAppType);
        response.setHeader("X-Auth-Token", result.getAccessToken());
        return result;
    }

}
