package com.fourtwenty.services.resources.mgmt;

import com.fourtwenty.services.resources.base.CashDrawerResource;
import io.swagger.annotations.Api;

import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Created by mdo on 12/8/16.
 */
@Api("Management - Cash Drawers")
@Path("/api/v1/mgmt/cashdrawers")
@Produces(MediaType.APPLICATION_JSON)
public class AdminCashDrawerResource extends CashDrawerResource {
}
