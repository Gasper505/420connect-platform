package com.fourtwenty.services.resources.mgmt;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.credits.CreditSaleLineItem;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.features.RemainingCreditsResult;
import com.fourtwenty.core.rest.features.SMSBuyRequest;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.credits.CreditService;
import com.google.inject.Inject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/**
 * Created by mdo on 7/18/17.
 */
@Api("Management - SMS & Email Credits")
@Path("/api/v1/mgmt/credits")
@Produces(MediaType.APPLICATION_JSON)
public class AdminCreditsResource {

    @Inject
    CreditService creditService;

    @GET
    @Path("/")
    @Timed(name = "getCreditBuyLogs")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Return credit that were bought")
    public SearchResult<CreditSaleLineItem> getCreditBuyLogs(@QueryParam("start") int start, @QueryParam("limit") int limit) {
        return creditService.getCreditSaleLogs(start, limit);
    }

    @GET
    @Path("/remaining")
    @Timed(name = "getRemainingCredits")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get Remaining Credits")
    public RemainingCreditsResult getRemainingCredits() {
        return creditService.getRemainingCredits();
    }

    @PUT
    @Path("/")
    @Timed(name = "buySMSCredits")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Buy SMS Credits")
    public CreditSaleLineItem buySMSCredits(@Valid SMSBuyRequest request) {
        return creditService.buySMSCredit(request);
    }
}
