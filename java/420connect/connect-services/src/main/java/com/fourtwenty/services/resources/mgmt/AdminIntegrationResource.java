package com.fourtwenty.services.resources.mgmt;

import com.fourtwenty.core.security.dispensary.BaseResource;
import io.swagger.annotations.Api;

import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Created by mdo on 4/12/17.
 */
@Api("Management - Integrations")
@Path("/api/v1/mgmt/integrations")
@Produces(MediaType.APPLICATION_JSON)
public class AdminIntegrationResource extends BaseResource {
}
