package com.fourtwenty.services.resources.mgmt;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.company.DriverLicense;
import com.fourtwenty.core.domain.models.customer.CareGiver;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.customer.MemberActivity;
import com.fourtwenty.core.domain.models.customer.StatusRequest;
import com.fourtwenty.core.domain.models.views.MemberLimitedView;
import com.fourtwenty.core.rest.dispensary.requests.company.MembershipAddRequest;
import com.fourtwenty.core.rest.dispensary.requests.company.MembershipBulkUpdateRequest;
import com.fourtwenty.core.rest.dispensary.requests.company.MembershipUpdateRequest;
import com.fourtwenty.core.rest.dispensary.requests.employees.DriverLicenseRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.company.InactiveMemberResult;
import com.fourtwenty.core.rest.dispensary.results.company.MemberResult;
import com.fourtwenty.core.security.dispensary.Audit;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.mgmt.MemberActivityService;
import com.fourtwenty.core.services.mgmt.MemberService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created by mdo on 8/26/15.
 */
@Api("Management - Members")
@Path("/api/v1/mgmt/members")
@Produces(MediaType.APPLICATION_JSON)
public class AdminMemberResource extends BaseResource {
    @Inject
    MemberService membershipService;
    @Inject
    MemberActivityService memberActivityService;

    @GET
    @Path("/{memberId}")
    @Timed(name = "getMembership")
    @Secured
    @ApiOperation(value = "Get Member By Id")
    public MemberResult getMembership(@PathParam("memberId") String memberId) {
        return membershipService.getMembership(memberId,true,true);
    }

    @GET
    @Path("/")
    @Timed(name = "getMembers")
    @Secured
    @ApiOperation(value = "Search Members")
    public SearchResult<MemberLimitedView> getMemberships(@QueryParam("term") String term, @QueryParam("doctorId") String doctorId, @QueryParam("start") int start, @QueryParam("limit") int limit) {

        if (StringUtils.isNotBlank(doctorId)) {
            return membershipService.getMembersForDoctorId(doctorId, term, start, limit);
        }
        return membershipService.getMembershipsForActiveShop(term, start, limit);
    }

    @GET
    @Path("/inactive")
    @Timed(name = "getInactiveMembersCount")
    @Secured
    @ApiOperation(value = "Get Inactive Members count by day")
    public InactiveMemberResult getInactiveMembersCount(@QueryParam("inactiveDays") int days) {
        return membershipService.getInactiveMembers(days);
    }


    @POST
    @Path("/")
    @Secured(requiredShop = true)
    @Timed(name = "addNewMember")
    @Audit(action = "Add Member")
    @ApiOperation(value = "Add New Member")
    public Member addNewMember(@Valid MembershipAddRequest addMember) {
        return membershipService.addMembership(addMember);
    }


    @PUT
    @Path("/{memberId}")
    @Secured(requiredShop = true)
    @Timed(name = "updateMember")
    @Audit(action = "Update Member")
    @ApiOperation(value = "Update Member")
    public Member updateMember(@PathParam("memberId") String memberId, @Valid MembershipUpdateRequest member) {
        return membershipService.updateMembership(memberId, member);
    }

    @DELETE
    @Path("/{memberId}")
    @Timed(name = "deleteMembership")
    @Secured
    @Audit(action = "Delete Member")
    @ApiOperation(value = "Delete Member")
    public Response deleteMembership(@PathParam("memberId") String memberId) {
        membershipService.removeMembership(memberId);
        return ok();
    }


    @PUT
    @Path("/bulkupdate")
    @Secured(requiredShop = true)
    @Timed(name = "bulkUpdateMembers")
    @ApiOperation(value = "Bulk Update Members")
    public Response bulkUpdateMembers(@Valid MembershipBulkUpdateRequest membershipBulkUpdateRequest) {
        membershipService.bulkUpdateMembers(membershipBulkUpdateRequest);
        return ok();
    }

    @GET
    @Path("/careGiver")
    @Secured(requiredShop = true)
    @Timed(name = "getCareGivers")
    @ApiOperation(value = "Get care givers list")
    public SearchResult<CareGiver> getCareGivers(@QueryParam("start") int start, @QueryParam("limit") int limit) {
        return membershipService.getCareGivers(start, limit);
    }

    @GET
    @Path("/careGiver/{careGiverId}")
    @Secured(requiredShop = true)
    @Timed(name = "getCareGiverById")
    @ApiOperation(value = "Get care giver by id")
    public CareGiver getCareGiverById(@PathParam("careGiverId") String careGiverId) {
        return membershipService.getCareGiverById(careGiverId);
    }

    @POST
    @Path("/careGiver")
    @Secured(requiredShop = true)
    @Timed(name = "addCareGiver")
    @Audit(action = "Add Care Giver")
    @ApiOperation(value = "Add new Care Giver")
    public CareGiver addCareGiver(@Valid CareGiver careGiver) {
        return membershipService.addCareGiver(careGiver);
    }

    @GET
    @Path("/careGiver/{careGiverId}/member")
    @Secured(requiredShop = true)
    @Timed(name = "getMembersOfCareGiver")
    @ApiOperation(value = "Get member of care givers")
    public List<Member> getMembersOfCareGiver(@PathParam("careGiverId") String careGiverId) {
        return membershipService.getMembersOfCareGiver(careGiverId);
    }

    @PUT
    @Path("{careGiverId}/careGiver")
    @Secured(requiredShop = true)
    @Timed(name = "updateCareGiver")
    @Audit(action = "Update Care Giver")
    @ApiOperation(value = "Update care giver")
    public CareGiver updateCareGiver(@PathParam("careGiverId") String careGiverId, @Valid CareGiver careGiver) {
        return membershipService.updateCareGiver(careGiverId, careGiver);
    }

    @DELETE
    @Path("/{careGiverId}/careGiver")
    @Secured(requiredShop = true)
    @Timed(name = "deleteCareGiver")
    @Audit(action = "Delete Care Giver")
    @ApiOperation(value = "Delete care giver")
    public Response deleteCareGiver(@PathParam("careGiverId") String careGiverId) {
        membershipService.deleteCareGiver(careGiverId);
        return ok();
    }

    @PUT
    @Path("{memberId}/ban")
    @Secured(requiredShop = true)
    @Timed(name = "updateMemberBanStatus")
    @Audit(action = "Bad Member")
    @ApiOperation(value = "Update Member's Ban status")
    public Member updateMemberBanStatus(@PathParam("memberId") String memberId,
                                        @Valid StatusRequest statusRequest) {
        return membershipService.updateMemberBanStatus(memberId, statusRequest);
    }

    @GET
    @Path("{memberId}/memberActivity")
    @Secured(requiredShop = true)
    @Timed(name = "getMemberActivities")
    @ApiOperation(value = " Get member activity history")
    public SearchResult<MemberActivity> getMemberActivities(@PathParam("memberId") String memberId, @QueryParam("start") int start, @QueryParam("limit") int limit) {
        return memberActivityService.getMemberActivities(getToken().getCompanyId(), getToken().getShopId(), memberId, start, limit);
    }

    @POST
    @Path("/driverLicense")
    @Secured(requiredShop = true)
    @Timed(name = "parseDriverLicense")
    @ApiOperation(value = "")
    public DriverLicense parseDriverLicense(@Valid DriverLicenseRequest request) {
        return membershipService.parseDriverLicense(request);
    }

    @GET
    @Path("/memberTags")
    @Secured(requiredShop = true)
    @Timed(name = "getAllMemberTags")
    @ApiOperation(value = " Get list of all member tags")
    public List<String> getAllMemberTags() {
        return membershipService.getAllMemberTags();
    }

    @GET
    @Path("/email/{email}")
    @Timed(name = "getMemberByEmail")
    @ApiOperation(value = "Get Member By Email")
    @Secured(requiredShop = true)
    public Member getMemberByEmail(@PathParam("email") String email) {
        return membershipService.getMemberByEmail(email);
    }

    @GET
    @Path("/licenceNumber")
    @Timed(name = "getMembersByLicenceNo")
    @Audit(action = "Get members by licence number")
    @ApiOperation(value = "Get members by licence number")
    @Secured(requiredShop = true)
    public SearchResult<Member> getMembersByLicenceNo(@QueryParam("licenceNumber") String licenceNumber) {
        return membershipService.getMembersByLicenceNo(licenceNumber);
    }
}
