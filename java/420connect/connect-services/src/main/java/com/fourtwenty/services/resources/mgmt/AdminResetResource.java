package com.fourtwenty.services.resources.mgmt;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.rest.dispensary.requests.company.ResetDeleteRequest;
import com.fourtwenty.core.security.dispensary.Audit;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.mgmt.ResetDataService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by mdo on 9/3/17.
 */

@Api("Management - Reset Service")
@Path("/api/v1/mgmt/reset")
@Produces(MediaType.APPLICATION_JSON)
public class AdminResetResource extends BaseResource {
    @Inject
    ResetDataService resetDataService;


    @POST
    @Path("/delete/members")
    @Secured(requiredShop = true)
    @Timed(name = "deleteMembers")
    @Audit(action = "Delete Member")
    @ApiOperation(value = "Delete All Members")
    public Response deleteMembers(@Valid ResetDeleteRequest deleteRequest) {
        resetDataService.deleteAllMembers(deleteRequest);
        return ok();
    }


    @POST
    @Path("/delete/products")
    @Secured(requiredShop = true)
    @Timed(name = "deleteProducts")
    @Audit(action = "Delete Products")
    @ApiOperation(value = "Delete All Products")
    public Response deleteProducts(@Valid ResetDeleteRequest deleteRequest) {
        resetDataService.deleteAllProducts(deleteRequest);
        return ok();
    }

    @POST
    @Path("/delete/product/stock")
    @Secured(requiredShop = true)
    @Timed(name = "deleteProductStocks")
    @Audit(action = "Delete Product Stock")
    @ApiOperation(value = "Delete All Product Stocks")
    public Response deleteProductStocks(@Valid ResetDeleteRequest deleteRequest) {
        resetDataService.deleteAllProductStocks(deleteRequest);
        return ok();
    }


    @POST
    @Path("/delete/vendors")
    @Secured(requiredShop = true)
    @Timed(name = "deleteVendors")
    @Audit(action = "Delete Vendors")
    @ApiOperation(value = "Delete All Vendors")
    public Response deleteVendors(@Valid ResetDeleteRequest deleteRequest) {
        resetDataService.deleteAllVendors(deleteRequest);
        return ok();
    }


    @POST
    @Path("/delete/physicians")
    @Secured(requiredShop = true)
    @Timed(name = "deletePhysicians")
    @Audit(action = "Delete Physicians")
    @ApiOperation(value = "Delete All Physicians")
    public Response deletePhysicians(@Valid ResetDeleteRequest deleteRequest) {
        resetDataService.deleteAllDoctors(deleteRequest);
        return ok();
    }
}
