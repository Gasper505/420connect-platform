package com.fourtwenty.services.resources.mgmt;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.loyalty.LoyaltyActivityLog;
import com.fourtwenty.core.domain.models.loyalty.LoyaltyReward;
import com.fourtwenty.core.rest.dispensary.requests.promotions.RewardAddRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.company.LoyaltyActivityLogResult;
import com.fourtwenty.core.rest.dispensary.results.company.LoyaltyMemberReward;
import com.fourtwenty.core.security.dispensary.Audit;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.mgmt.LoyaltyRewardService;
import com.google.inject.Inject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by mdo on 8/8/17.
 */
@Api("Management - Loyalty Rewards")
@Path("/api/v1/mgmt/rewards")
@Produces(MediaType.APPLICATION_JSON)
public class AdminRewardsResource extends BaseResource {
    @Inject
    LoyaltyRewardService rewardService;

    @GET
    @Path("/")
    @Timed(name = "getRewards")
    @Secured
    @ApiOperation(value = "Get Rewards List")
    public SearchResult<LoyaltyReward> getRewards(@QueryParam("start") int start, @QueryParam("limit") int limit) {
        return rewardService.getAllRewards(start, limit);
    }

    @GET
    @Path("/{rewardId}")
    @Timed(name = "getRewardById")
    @Secured
    @ApiOperation(value = "Get Reward By Id")
    public LoyaltyReward getRewardById(@PathParam("rewardId") String rewardId) {
        return rewardService.getRewardById(rewardId);
    }

    @GET
    @Path("/members/{memberId}")
    @Timed(name = "getRewardsForMember")
    @Secured
    @ApiOperation(value = "Get Rewards for member")
    public SearchResult<LoyaltyMemberReward> getRewardsForMember(@PathParam("memberId") String memberId) {
        return rewardService.getRewardsForMembers(memberId);
    }

    @POST
    @Path("/")
    @Secured
    @Timed(name = "addNewReward")
    @Audit(action = "Add New Reward")
    @ApiOperation(value = "Add New Reward")
    public LoyaltyReward addNewReward(@Valid RewardAddRequest request) {
        return rewardService.addReward(request);
    }

    @PUT
    @Path("/{rewardId}")
    @Secured
    @Timed(name = "updateReward")
    @Audit(action = "Update Reward")
    @ApiOperation(value = "Update Reward")
    public LoyaltyReward updateReward(@PathParam("rewardId") String rewardId, @Valid LoyaltyReward reward) {
        return rewardService.updateReward(rewardId, reward);
    }

    @POST
    @Path("/{rewardId}/publish")
    @Secured
    @Timed(name = "publishReward")
    @Audit(action = "Publish Reward")
    @ApiOperation(value = "Publish Reward")
    public LoyaltyReward publishReward(@PathParam("rewardId") String rewardId) {
        return rewardService.publishReward(rewardId);
    }

    @POST
    @Path("/{rewardId}/unpublish")
    @Secured
    @Timed(name = "unpublishReward")
    @Audit(action = "Unpublish Reward")
    @ApiOperation(value = "Unpublish Reward")
    public LoyaltyReward unpublishReward(@PathParam("rewardId") String rewardId) {
        return rewardService.unpublishReward(rewardId);
    }

    @DELETE
    @Path("/{rewardId}")
    @Secured
    @Timed(name = "deleteReward")
    @Audit(action = "Delete Reward")
    @ApiOperation(value = "Delete Reward")
    public Response deleteReward(@PathParam("rewardId") String rewardId) {
        rewardService.deleteReward(rewardId);
        return ok();
    }


    @GET
    @Path("/activity/{activityType}")
    @Timed(name = "getRewardsActivity")
    @Secured
    @ApiOperation(value = "Get Rewards Activity")
    public SearchResult<LoyaltyActivityLogResult> getRewardsActivity(@PathParam("activityType") String activityType) {
        //LoyaltyActivityType
        LoyaltyActivityLog.LoyaltyActivityType type = LoyaltyActivityLog.LoyaltyActivityType.convert(activityType);
        return rewardService.getLoyaltyUsageReward(type, 0, 500);
    }

}
