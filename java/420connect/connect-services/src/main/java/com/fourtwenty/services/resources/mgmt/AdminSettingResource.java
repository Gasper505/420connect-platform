package com.fourtwenty.services.resources.mgmt;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.company.MemberGroup;
import com.fourtwenty.core.domain.models.customer.MedicalCondition;
import com.fourtwenty.core.domain.models.payment.ShopPaymentOption;
import com.fourtwenty.core.domain.models.product.ProductWeightTolerance;
import com.fourtwenty.core.rest.dispensary.requests.UpdateToleranceRequest;
import com.fourtwenty.core.rest.dispensary.requests.company.MemberGroupAddRequest;
import com.fourtwenty.core.rest.dispensary.results.ListResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.company.MemberGroupResult;
import com.fourtwenty.core.security.dispensary.Audit;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.mgmt.MedicalConditionService;
import com.fourtwenty.core.services.mgmt.SettingService;
import com.fourtwenty.core.services.mgmt.ShopPaymentOptionService;
import com.fourtwenty.core.thirdparty.elasticsearch.services.ElasticSearchService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by mdo on 10/25/15.
 */

@Api("Management - Settings")
@Path("/api/v1/mgmt/settings")
@Produces(MediaType.APPLICATION_JSON)
public class AdminSettingResource extends BaseResource {
    @Inject
    SettingService settingService;
    @Inject
    MedicalConditionService medicalConditionService;
    @Inject
    ShopPaymentOptionService shopPaymentOptionService;
    @Inject
    ElasticSearchService elasticSearchService;

    @GET
    @Path("/tolerances")
    @Secured
    @Timed(name = "getProductTolerance")
    @ApiOperation(value = "Get Product Weight Tolerances")
    public ListResult<ProductWeightTolerance> getProductTolerance() {
        return settingService.getProductTolerances();
    }

    @POST
    @Path("/tolerances/{toleranceId}")
    @Secured
    @Timed(name = "getProductTolerance")
    @Audit(action = "Update Tolerance")
    @ApiOperation(value = "Update Product Weight Tolerance")
    public Response updateTolerance(@PathParam("toleranceId") String toleranceId, @Valid UpdateToleranceRequest request) {
        settingService.updateTolerance(toleranceId, request);
        return ok();
    }

    @GET
    @Path("/medicalconditions")
    @Secured
    @Timed(name = "getMedicalConditions")
    @ApiOperation(value = "Get Medical Conditions")
    public ListResult<MedicalCondition> getMedicalConditions() {
        return medicalConditionService.getAllMedicalConditions();
    }

    @GET
    @Path("/memberGroups")
    @Timed(name = "getMemberGroups")
    @Secured
    @ApiOperation(value = "Get Member Groups")
    public SearchResult<MemberGroupResult> getMemberGroups(@QueryParam("active") boolean active) {
        if (active) {
            return settingService.getActiveMemberGroups();
        } else {
            return settingService.getMemberGroups();
        }
    }

    @POST
    @Path("/memberGroups/{memberGroupId}")
    @Timed(name = "updateMemberGroup")
    @Secured
    @Audit(action = "Update Member Group")
    @ApiOperation(value = "Update Member Group")
    public MemberGroup updateMemberGroup(@PathParam("memberGroupId") String memberGroupId, @Valid MemberGroup memberGroup) {
        return settingService.updateMemberGroup(memberGroupId, memberGroup);
    }

    /**
     * Create Member Group
     *
     * @param memberGroup
     * @return New Memeber Group Object
     */
    @POST
    @Path("/memberGroups")
    @Timed(name = "createMemberGroup")
    @Secured
    @Audit(action = "Create Member Group")
    @ApiOperation(value = "Create Member Group")
    public MemberGroup createMemberGroup(@Valid MemberGroupAddRequest memberGroup) {
        return settingService.createMemberGroup(memberGroup);
    }

    /**
     * Delete Member Group
     *
     * @param memberGroupId
     * @return A success response
     */
    @DELETE
    @Path("/memberGroups/{memberGroupId}")
    @Timed(name = "deleteMemberGroup")
    @Secured
    @Audit(action = "Delete Member Group")
    @ApiOperation(value = "Delete Member Group")
    public Response deleteMemberGroup(@PathParam("memberGroupId") String memberGroupId) {
        settingService.deleteMemberGroup(memberGroupId);
        return ok();
    }

    ///
    /// PaymentOption Settings
    ///
    @GET
    @Path("/paymentoptions")
    @Timed(name = "paymentoptions")
    @Secured
    @Audit(action = "Get all payment options for shop")
    @ApiOperation(value = "Get all payment options for shop")
    public ListResult<ShopPaymentOption> getPaymentOptions() {
        return new ListResult<>(shopPaymentOptionService.getPaymentOptions());
    }

    @POST
    @Path("/paymentoptions")
    @Timed(name = "paymentoptions")
    @Secured
    @Audit(action = "Save all payment options for shop")
    @ApiOperation(value = "Save all payment options for shop")
    public Response savePaymentOptions(ListResult<ShopPaymentOption> shopPaymentOptions) {
        // TODO: Nullcheck?
        shopPaymentOptionService.savePaymentOptions(shopPaymentOptions.getValues());
        return ok();
    }


    ///
    /// ElasticSearch resync methods
    ///

    /**
     * Resync Members to ElasticSearch
     *
     * @return A success response
     */
    @GET
    @Path("/elasticsearch/resyncMembers")
    @Timed(name = "resyncMembers")
    @Secured
    @Audit(action = "Resync Members to ElasticSearch")
    @ApiOperation(value = "Resync Members to ElasticSearch")
    public Response resyncMembers() {
        elasticSearchService.resyncMembers(token.get().getCompanyId());
        return ok();
    }

    /**
     * Resync Products to ElasticSearch
     *
     * @return A success response
     */
    @GET
    @Path("/elasticsearch/resyncProducts")
    @Timed(name = "resyncMembers")
    @Secured
    @Audit(action = "Resync Products to ElasticSearch")
    @ApiOperation(value = "Resync Products to ElasticSearch")
    public Response resyncProducts() {
        elasticSearchService.resyncProducts(token.get().getCompanyId());
        return ok();
    }

    /**
     * Resync Transactions to ElasticSearch
     *
     * @return A success response
     */
    @GET
    @Path("/elasticsearch/resyncTransactions")
    @Timed(name = "resyncTransactions")
    @Secured
    @Audit(action = "Resync Transactions to ElasticSearch")
    @ApiOperation(value = "Resync Transactions to ElasticSearch")
    public Response resyncTransactions() {
        elasticSearchService.resyncTransactions(token.get().getCompanyId());
        return ok();
    }


    /**
     * Resync Product Batches to ElasticSearch
     *
     * @return A success response
     */
    @GET
    @Path("/elasticsearch/resyncProductBatches")
    @Timed(name = "resyncProductBatches")
    @Secured
    @Audit(action = "Resync Product Batches to ElasticSearch")
    @ApiOperation(value = "Resync Product Batches to ElasticSearch")
    public Response resyncProductBatches() {
        elasticSearchService.resyncProductBatches(token.get().getCompanyId());
        return ok();
    }

}
