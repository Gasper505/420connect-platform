package com.fourtwenty.services.resources.mgmt;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.company.CompanyFeatures;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.transaction.QueuedTransaction;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.rest.dispensary.requests.queues.*;
import com.fourtwenty.core.rest.dispensary.requests.shop.ShopAddRequest;
import com.fourtwenty.core.rest.dispensary.requests.shop.ShopUpdateRequest;
import com.fourtwenty.core.rest.dispensary.results.shop.ShopResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.TransactionCountResult;
import com.fourtwenty.core.rest.dispensary.results.TransactionResult;
import com.fourtwenty.core.rest.dispensary.results.shop.AllQueueResult;
import com.fourtwenty.core.rest.dispensary.results.shop.ConsumerCartResult;
import com.fourtwenty.core.rest.dispensary.results.shop.CustomShopResult;
import com.fourtwenty.core.security.dispensary.Audit;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.mgmt.QueuePOSService;
import com.fourtwenty.core.services.mgmt.ShopService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created by mdo on 5/14/16.
 */

@Api("Management - Shops & Transactions")
@Path("/api/v1/mgmt/shops")
@Produces(MediaType.APPLICATION_JSON)
public class AdminShopResource extends BaseResource {
    @Inject
    ShopService shopService;
    @Inject
    QueuePOSService queuePOSService;

    @GET
    @Path("/")
    @Timed(name = "getShops")
    @Audit(action = "Get Shops")
    @ApiOperation(value = "Get Shops")
    public SearchResult<CustomShopResult> getShops(@QueryParam("appTarget") CompanyFeatures.AppTarget appTarget) {
        return shopService.searchShops(appTarget);
    }


    @POST
    @Path("/")
    @Timed(name = "addShop")
    @Secured(requiredShop = true)
    @Audit(action = "Add Shop")
    @ApiOperation(value = "Add Shop")
    public Shop addShop(@Valid ShopAddRequest request) {
        return shopService.addShop(request);
    }


    // Shop
    @POST
    @Path("/{shopId}")
    @Timed(name = "updateShopInfo")
    @Secured(requiredShop = true)
    @Audit(action = "Update Shop Info")
    @ApiOperation(value = "Update Shop Info")
    public Shop updateShopInfo(@PathParam("shopId") String shopId, @Valid Shop request) {
        return shopService.updateShop(shopId, request);
    }

    // Shop
    @GET
    @Path("/{shopId}/blazeConnections")
    @Timed(name = "getBlazeConnectionsInfo")
    @Secured(requiredShop = true)
    @Audit(action = "Get Blaze Connections Info")
    @ApiOperation(value = "Get Blaze Connections Info")
    public List<ShopResult> getBlazeConnectionsInfo(@PathParam("shopId") String shopId) {
        return shopService.getBlazeConnectionsByShopId(shopId);
    }


    // Queues & Transactions
    @GET
    @Path("/transactions/{transactionId}")
    @Timed(name = "getTransactionById")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get Transaction By Id")
    public Transaction getTransactionById(@PathParam("transactionId") String transactionId) {
        return queuePOSService.getTransactionById(transactionId);
    }

    // Queues & Transactions
    @GET
    @Path("/transactions/transno/{transNo}")
    @Timed(name = "getTransactionByTransNo")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get Transaction By Trans No")
    public Transaction getTransactionByTransNo(@PathParam("transNo") String transNo) {
        return queuePOSService.getTransactionByTransNo(transNo);
    }

    @POST
    @Path("/transactions")
    @Timed(name = "addToQueue")
    @Secured(requiredShop = true)
    @Audit(action = "Add To Queue")
    @ApiOperation(value = "Add To Queue")
    public Transaction addToQueue(@Valid AdminQueueAddMemberRequest request) {
        return queuePOSService.addToQueue(request.getQueueName(), request,-1);
    }

    // Transactions

    @GET
    @Path("/transactions/dashboard")
    @Timed(name = "getAllActiveTransactions")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get All Active Transactions")
    public AllQueueResult getAllActiveTransactions(@QueryParam("startDate") long startDate,
                                                   @QueryParam("endDate") long endDate) {
        return queuePOSService.getTransactionsWithDate(startDate, endDate);
    }

    @GET
    @Path("/transactions")
    @Timed(name = "getTransactionsForShopQueue")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Search Transactions")
    public SearchResult<Transaction> getTransactionsForShopQueue(@QueryParam("queue") String queueName,
                                                                 @QueryParam("transNo") String transNo,
                                                                 @QueryParam("transactionId") String transactionId,
                                                                 @QueryParam("memberId") String memberId,
                                                                 @QueryParam("active") boolean active,
                                                                 @QueryParam("start") int start,
                                                                 @QueryParam("limit") int limit) {
        return queuePOSService.getTransactions(queueName, transNo, transactionId, memberId, active, start, limit);
    }

    @GET
    @Path("/transactions/incoming")
    @Timed(name = "getIncomingOrders")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Retrieve Incoming Orders")
    public SearchResult<ConsumerCartResult> getIncomingOrders(@QueryParam("start") int start, @QueryParam("limit") int limit) {
        return queuePOSService.getIncomingOrders(start, limit);
    }


    @GET
    @Path("/transactions/incoming/rejected")
    @Timed(name = "getRejectedIncomingOrders")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Retrieve Incoming Rejected Orders")
    public SearchResult<ConsumerCartResult> getRejectedIncomingOrders(@QueryParam("start") int start, @QueryParam("limit") int limit) {
        return queuePOSService.getRejectedOrders(start, limit);
    }

    @GET
    @Path("/transactions/dates")
    @Timed(name = "getTransactionsForShopQueueDates")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Search Transactions with dates")
    public SearchResult<Transaction> getTransactionsForShopQueueDates(
            @QueryParam("transNo") String transNo,
            @QueryParam("transactionId") String transactionId,
            @QueryParam("memberId") String memberId,
            @QueryParam("startDate") String startDate,
            @QueryParam("endDate") String endDate,
            @QueryParam("timezoneOffset") int timezoneOffset) {
        return queuePOSService.getCompletedTransactionsWithDates(transNo, transactionId, memberId, startDate, endDate, timezoneOffset);
    }

    @DELETE
    @Path("/transactions/{transactionId}")
    @Timed(name = "deleteTransaction")
    @Secured(requiredShop = true)
    @Audit(action = "Delete Transaction")
    @ApiOperation(value = "Delete Transaction")
    public Response deleteTransaction(@PathParam("transactionId") String transactionId, @Valid TransactionDeleteRequest request) {
        queuePOSService.deleteFromQueue(transactionId, request);
        return ok();
    }

    @POST
    @Path("/transactions/{transactionId}/refund")
    @Timed(name = "holdTransaction")
    @Secured(requiredShop = true, checkTerminal = true)
    @Audit(action = "Refund Transaction")
    @ApiOperation(value = "Refund Transaction")
    public Transaction refundTransaction(@PathParam("transactionId") String transactionId, @Valid RefundRequest request) {
        return queuePOSService.refundTransaction(transactionId, request);
    }

    @POST
    @Path("/transactions/{transactionId}/time")
    @Timed(name = "updateProcessedTime")
    @Secured(requiredShop = true, checkTerminal = true)
    @Audit(action = "Transaction - Update Processed Time")
    @ApiOperation(value = "Update Transaction Processed Time")
    public Transaction updateProcessedTime(@PathParam("transactionId") String transactionId, @Valid UpdateProcessedTimeRequest request) {
        return queuePOSService.updateProcessedTime(transactionId, request);
    }

    @PUT
    @Path("/transactions/{transactionId}/memo")
    @Timed(name = "updateTransactionMemo")
    @Secured(requiredShop = true, checkTerminal = false)
    @Audit(action = "Transaction - Update Transaction Memo")
    @ApiOperation(value = "Update Transaction Memo")
    public Transaction updateTransactionMemo(@PathParam("transactionId") String transactionId, @Valid UpdateTransMemoRequest request) {
        return queuePOSService.updateTransactionNote(transactionId, request);
    }

    @GET
    @Path("/transactions/list")
    @Timed(name = "getTransactionsForShop")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get transactions for shop")
    public SearchResult<TransactionResult> getTransactionsForShop(@QueryParam("start") int start,
                                                                  @QueryParam("limit") int limit,
                                                                  @QueryParam("status") TransactionsRequest.QueryType status,
                                                                  @QueryParam("memberId") String memberId,
                                                                  @QueryParam("startDate") String startDate,
                                                                  @QueryParam("endDate") String endDate) {
        return queuePOSService.getTransactionsForShop(status, start, limit, memberId, startDate, endDate);
    }

    @GET
    @Path("/transactions/status")
    @Timed(name = "getTransactionByStatus")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get transactions by status")
    public SearchResult<Transaction> getTransactionByStatus(@QueryParam("start") int start,
                                                            @QueryParam("limit") int limit,
                                                            @QueryParam("status") TransactionsRequest.AssignStatus status,
                                                            @QueryParam("term") String term,
                                                            @QueryParam("afterDate") long afterDate,
                                                            @QueryParam("beforeDate") long beforeDate) {
        return queuePOSService.getTransactionByStatus(status, start, limit, term, afterDate, beforeDate);
    }

    @GET
    @Path("/transactions/count")
    @Timed(name = "getOrdersCount")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get transactions count")
    public TransactionCountResult getOrdersCount() {
        return queuePOSService.getOrdersCount();
    }

    @GET
    @Path("/queuedTransaction/{transactionId}")
    @Timed(name = "getQueuedTransaction")
    @Secured(requiredShop = true)
    @Audit(action = "Get Queued Transaction")
    @ApiOperation(value = "Get Queued Transaction")
    public SearchResult<QueuedTransaction> getQueuedTransactions(@PathParam("transactionId") String transactionId) {
        return queuePOSService.getQueuedTransactions(transactionId);
    }

    @POST
    @Path("/updateShop/{shopId}")
    @Timed(name = "updateShopData")
    @Secured(requiredShop = true)
    @Audit(action = "Update shop information")
    @ApiOperation(value = "Update shop information")
    public Shop updateShopData(@PathParam("shopId") String shopId, ShopUpdateRequest request) {
        return shopService.updateShopData(shopId, request);
    }

}
