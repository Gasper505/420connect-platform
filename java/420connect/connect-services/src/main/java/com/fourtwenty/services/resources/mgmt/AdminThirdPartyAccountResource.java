package com.fourtwenty.services.resources.mgmt;

import com.blaze.clients.metrcs.models.facilities.MetricsFacilityList;
import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.thirdparty.*;
import com.fourtwenty.core.event.BlazeEventBus;
import com.fourtwenty.core.event.linx.GetLinxAccountsEvent;
import com.fourtwenty.core.event.linx.SaveLinxAccountsEvent;
import com.fourtwenty.core.rest.dispensary.requests.thirdparty.TPAccountAddRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.thirdparty.VersionRequest;
import com.fourtwenty.core.rest.thirdparty.WeedmapTokenType;
import com.fourtwenty.core.rest.thirdparty.WeedmapUpdateAPIRequest;
import com.fourtwenty.core.security.dispensary.Audit;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.mgmt.ThirdPartyAccountService;
import com.fourtwenty.core.services.thirdparty.MetrcAccountService;
import com.fourtwenty.core.services.thirdparty.WeedmapService;
import com.fourtwenty.core.thirdparty.weedmap.models.ThirdPartyBrand;
import com.fourtwenty.core.thirdparty.weedmap.models.ThirdPartyProduct;
import com.fourtwenty.core.thirdparty.weedmap.result.WeedmapAccountResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by mdo on 2/12/17.
 */
@Api("Management - Third Party Accounts")
@Path("/api/v1/mgmt/thirdparty")
@Produces(MediaType.APPLICATION_JSON)
public class AdminThirdPartyAccountResource extends BaseResource {
    @Inject
    ThirdPartyAccountService service;
    @Inject
    WeedmapService weedmapService;
    @Inject
    MetrcAccountService metrcAccountService;
    @Inject
    private BlazeEventBus eventBus;

    @GET
    @Path("/")
    @Timed(name = "getAllThirdPartyAccounts")
    @Secured(required = true)
    @ApiOperation(value = "Get All ThirdPartyAccounts")
    public SearchResult<ThirdPartyAccount> getAllThirdPartyAccounts() {
        return service.getThirdPartyAccounts();
    }

    @GET
    @Path("/{accountId}")
    @Timed(name = "addThirdPartyAccount")
    @Secured(required = true)
    @ApiOperation(value = "Get Third Party Account By Id")
    public ThirdPartyAccount getAccountById(@PathParam("accountId") String accountId) {
        return service.getAccountById(accountId);
    }


    @POST
    @Path("/")
    @Timed(name = "addThirdPartyAccount")
    @Secured(required = true)
    @ApiOperation(value = "Add Third Party Account")
    public ThirdPartyAccount addThirdPartyAccount(@Valid TPAccountAddRequest request) {
        return service.addAccount(request);
    }

    @PUT
    @Path("/{accountId}")
    @Timed(name = "updateThirdPartyAccount")
    @Secured(required = true)
    @ApiOperation(value = "Add Third Party Account")
    public ThirdPartyAccount updateThirdPartyAccount(@PathParam("accountId") String accountId, @Valid ThirdPartyAccount request) {
        return service.updateAccount(accountId, request);
    }

    @DELETE
    @Path("/{accountId}")
    @Timed(name = "deleteAccount")
    @Secured(required = true)
    @Audit(action = "Delete Account")
    @ApiOperation(value = "Delete Account")
    public Response deleteAccount(@PathParam("accountId") String accountId) {
        service.deleteThirdPartyAccount(accountId);
        return ok();
    }

    @GET
    @Path("/weedmap")
    @Timed(name = "getCurrentWeedmapAccount")
    @Secured(required = true, requiredShop = true)
    @ApiOperation(value = "Get Weedmap Account")
    public WeedmapAccountResult getCurrentWeedmapAccount() {
        return weedmapService.getCurrentWeedmapAccount();
    }

    @POST
    @Path("/weedmap")
    @Timed(name = "addNewWeedmapAccount")
    @Secured(required = true, requiredShop = true)
    @Audit(action = "Add Weedmap Account")
    @ApiOperation(value = "Add Weedmap Account")
    public WeedmapAccount addNewWeedmapAccount(@Valid VersionRequest request) {
        return weedmapService.createWeedmapAccount(request);
    }

    @PUT
    @Path("/weedmap/apiKey")
    @Timed(name = "updateWeedmapAPIKey")
    @Secured(required = true, requiredShop = true)
    @Audit(action = "Weedmap Weedmap Key")
    @ApiOperation(value = "Update Weedmap Account API Key")
    public WeedmapAccount updateWeedmapAPIKey(@Valid WeedmapUpdateAPIRequest request) {
        return weedmapService.updateWeedmapAPIKey(request);
    }

    @PUT
    @Path("/weedmap")
    @Timed(name = "updateWeedmapAccount")
    @Secured(required = true, requiredShop = true)
    @Audit(action = "Weedmap Weedmap Account")
    @ApiOperation(value = "Update Weedmap Account")
    public WeedmapAccount updateWeedmapAccount(@Valid WeedmapAccount account) {
        return weedmapService.updateWeedmapAccount(account);
    }

    @POST
    @Path("/weedmap/sync")
    @Timed(name = "syncWeedmap")
    @Secured(required = true, requiredShop = true)
    @Audit(action = "Sync Weedmap Menu")
    @ApiOperation(value = "Sync Weedmap Menu")
    public WeedmapAccount syncWeedmap() {
        return weedmapService.syncMenu();
    }

    @POST
    @Path("/weedmap/reset")
    @Timed(name = "resetWeedmap")
    @Secured(required = true, requiredShop = true)
    @Audit(action = "Reset/Delete Weedmap Menu")
    @ApiOperation(value = "Reset & Delete Weedmap Menu")
    public Response resetWeedmap() {
        weedmapService.resetMenu(true);
        return ok();
    }
    @POST
    @Path("/weedmap/resetonly")
    @Timed(name = "resetonly")
    @Audit(action = "Reset Only Weedmap Menu")
    @Secured(required = true, requiredShop = true)
    @ApiOperation(value = " OnlyReset Sync Weedmap Menu")
    public Response resetWeedmapOnly() {
        weedmapService.resetMenu(false);
        return ok();
    }

    @DELETE
    @Path("/weedmap")
    @Timed(name = "deleteWeedmapAccount")
    @Secured(required = true, requiredShop = true)
    @Audit(action = "Delete Weedmap Account")
    @ApiOperation(value = "Delete Weedmap Account")
    public Response deleteWeedmapAccount() {
        weedmapService.deleteWeedmapAccount();
        return ok();
    }


    @GET
    @Path("/metrc")
    @Timed(name = "getMetrcAccount")
    @Secured(required = true, requiredShop = true)
    @ApiOperation(value = "Get Metrc Account")
    public List<MetrcAccount> getMetrcAccount() {
        return metrcAccountService.getMyMetrcAccount();
    }


    @GET
    @Path("/metrc/facilities")
    @Timed(name = "getMetrcFacilities")
    @Secured(required = true, requiredShop = true)
    @ApiOperation(value = "Get Metrc Account Facilities")
    public MetricsFacilityList getMetrcFacilities() {
        return metrcAccountService.getMetrcFacilities();
    }

    @GET
    @Path("/metrc/facilities/search")
    @Timed(name = "getMetrcFacilitiesSearch")
    @Secured(required = true, requiredShop = true)
    @ApiOperation(value = "Get Metrc Account Facilities")
    public Map<String, MetricsFacilityList> getMetrcFacilitiesSearch(@QueryParam("environment")String env, @QueryParam("stateCode") String stateCode, @QueryParam("apiKey") String apiKey) {
        return metrcAccountService.getMetrcFacilities(env,stateCode,apiKey);
    }

    @PUT
    @Path("/metrc")
    @Timed(name = "updateMetrcAccount")
    @Secured(required = true, requiredShop = true)
    @Audit(action = "Update Metrc Account")
    @ApiOperation(value = "Update Metrc Account")
    public List<MetrcAccount> updateMetrcAccount(@Valid List<MetrcAccount> metrcAccounts) {
        return metrcAccountService.updateMetrcAccount(metrcAccounts);
    }


    @GET
    @Path("/linx")
    @Timed(name = "getLinxAccounts")
    @Secured(required = true, requiredShop = true)
    @ApiOperation(value = "Get Linx Accounts")
    public List<LinxAccount> getLinxAccounts() {
        GetLinxAccountsEvent event = new GetLinxAccountsEvent();
        event.setPayload(token.get().getCompanyId());
        eventBus.post(event);
        if (event.getResponse() != null) {
            return event.getResponse().getLinxAccounts();
        }
        return new ArrayList();
    }

    @POST
    @Path("/linx")
    @Timed(name = "updateLinxAccounts")
    @Secured(required = true, requiredShop = true)
    @Audit(action = "Update Linx Account")
    @ApiOperation(value = "Update Linx Account")
    public List<LinxAccount> updateLinxAccounts(@Valid List<LinxAccount> linxAccount) {
        ConnectAuthToken authToken = token.get();
        linxAccount.forEach(la -> {
            la.prepare(authToken.getCompanyId());
        });

        SaveLinxAccountsEvent event = new SaveLinxAccountsEvent();
        event.setPayload(linxAccount);
        eventBus.post(event);
        if (event.getResponse() != null) {
            return event.getResponse().getLinxAccounts();
        }
        return new ArrayList();
    }

    @GET
    @Path("/metrc/facilitiesData")
    @Timed(name = "getMetrcFacilitiesData")
    @Secured(required = true, requiredShop = true)
    @ApiOperation(value = "Get Metrc Account Facility Data ")
    public Map<String, MetricsFacilityList> getMetrcFacilitiesByState() {
        return metrcAccountService.getMetrcFacilitiesByStateCode();
    }

    @POST
    @Path("/addMetrc")
    @Timed(name = "addMetrcAccount")
    @Secured(required = true, requiredShop = true)
    @Audit(action = "Add Metrc Account")
    @ApiOperation(value = "Add Metrc Account")
    public MetrcAccount addMetrcAccount(MetrcAccount metrcAccount) {
        return metrcAccountService.addMetrcAccount(metrcAccount);
    }

    @POST
    @Path("/weedmap/token")
    @Timed(name = "update token")
    @Secured(required = true, requiredShop = true)
    @Audit(action = "Update Token")
    @ApiOperation(value = "Update Token")
    public WeedmapAccount fetchOrRefreshToken(@Valid WeedmapTokenType type) {
        return weedmapService.fetchOrRefreshToken(type);
    }

    @GET
    @Path("/weedmap/generate")
    @Timed(name = "Generate code")
    @Secured(required = true, requiredShop = true)
    @Audit(action = "Generate Code")
    @ApiOperation(value = "Generate Weedmap Code")
    public WeedmapAccount generateCode(@QueryParam("code") String code) {
        return weedmapService.generateCode(code);
    }

    @GET
    @Path("/weedmap/tags")
    @Timed(name = "getWeedmapTags")
    @Secured(required = true, requiredShop = true)
    @Audit(action = "Get weedmap related tags")
    @ApiOperation(value = "Get weedmap related tags")
    public List<WmTagGroups> getWeedmapTags() {
        return weedmapService.getWeedmapTags();
    }

    @GET
    @Path("/brands")
    @Timed(name = "getThirdPartyBrands")
    @Secured(required = true, requiredShop = true)
    @Audit(action = "Get third party brands")
    @ApiOperation(value = "Get third party brands")
    public SearchResult<ThirdPartyBrand> getThirdPartyBrands(@QueryParam("brandId") String brandId, @QueryParam("start")int skip, @QueryParam("limit")int limit, @QueryParam("term")String term) {
        return weedmapService.getThirdPartyBrand(brandId, skip, limit, term);
    }

    @GET
    @Path("/products")
    @Timed(name = "getThirdPartyProducts")
    @Secured(required = true, requiredShop = true)
    @Audit(action = "Get third party products")
    @ApiOperation(value = "Get third party products")
    public SearchResult<ThirdPartyProduct> getThirdPartyProducts(@QueryParam("brandId") String brandId, @QueryParam("start")int skip, @QueryParam("limit")int limit, @QueryParam("term")String term) {
        return weedmapService.getThirdPartyProduct(brandId, skip, limit, term);
    }

    @PUT
    @Path("/weedmap/mapping")
    @Timed(name = "updateWmMapping")
    @Secured(requiredShop = true)
    @Audit(action = "Add/Update weedmap product mapping")
    @ApiOperation(value = "Add/Update product mapping")
    public List<WmProductMapping> updateWmMapping(@Valid List<WmProductMapping> wmMapping) {
        return weedmapService.updateWmMapping(wmMapping);
    }

    @PUT
    @Path("/weedmap/category/mapping")
    @Timed(name = "updateCategoryMapping")
    @Secured(requiredShop = true)
    @Audit(action = "Add/Update weedmap category mapping")
    @ApiOperation(value = "Add/Update category mapping")
    public void updateCategoryMapping(@Valid List<WmCategoryMapping> wmMapping) {
        weedmapService.updateCategoryMapping(wmMapping);
    }
}
