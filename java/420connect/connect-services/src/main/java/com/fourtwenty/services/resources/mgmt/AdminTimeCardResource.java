package com.fourtwenty.services.resources.mgmt;

import com.fourtwenty.core.rest.dispensary.results.TimeCardCompositeResult;
import com.fourtwenty.core.security.dispensary.Audit;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.services.resources.base.TimeCardResource;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by mdo on 7/2/16.
 */

@Api("Management - Timecards")
@Path("/api/v1/mgmt/timecards")
@Produces(MediaType.APPLICATION_JSON)
public class AdminTimeCardResource extends TimeCardResource {

    @POST
    @Path("/user")
    @Secured(requiredShop = true)
    @Audit(action = "Clock In")
    @ApiOperation(value = "Clock In")
    public TimeCardCompositeResult clockIn() {
        return timeCardService.userClockIn();
    }

    @DELETE
    @Path("/{timeCardId}/admin")
    @Secured(requiredShop = true)
    @Audit(action = "Clock Out")
    @ApiOperation(value = "Admin Clock Out User with TimeCard")
    public Response adminClockOut(@PathParam("timeCardId") String timeCardId) {
        timeCardService.adminClockOut(timeCardId);
        return ok();
    }

    @DELETE
    @Path("/user")
    @Secured(requiredShop = true)
    @Audit(action = "Clock Out")
    @ApiOperation(value = "User Clock Out")
    public Response userClockOut() {
        timeCardService.userClockOut();
        return ok();
    }
}
