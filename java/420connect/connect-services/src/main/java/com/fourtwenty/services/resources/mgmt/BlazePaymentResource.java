package com.fourtwenty.services.resources.mgmt;

import com.braintreegateway.Result;
import com.braintreegateway.Transaction;
import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.payment.Payment;
import com.fourtwenty.core.domain.models.payment.PaymentComponent;
import com.fourtwenty.core.domain.models.payment.PaymentProviderDetails;
import com.fourtwenty.core.rest.payments.BraintreeBuyRequest;
import com.fourtwenty.core.rest.payments.BraintreeTokenResult;
import com.fourtwenty.core.rest.payments.PaymentReceipt;
import com.fourtwenty.core.rest.payments.SmsBuyRequest;
import com.fourtwenty.core.rest.payments.stripe.StripeSMSBuyRequest;
import com.fourtwenty.core.rest.payments.wepay.WepayCreditCardCheckoutRequest;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.payments.BlazePaymentService;
import com.google.inject.Inject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/**
 * Created by mdo on 8/27/17.
 */
@Api("Management - Payments")
@Path("/api/v1/mgmt/payments")
@Produces(MediaType.APPLICATION_JSON)
public class BlazePaymentResource {
    @Inject
    BlazePaymentService blazePaymentService;


    @GET
    @Path("/braintree/token")
    @Timed(name = "getNewClientToken")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get New Braintree token")
    public BraintreeTokenResult getNewClientToken() {
        return blazePaymentService.createBraintreeToken();
    }

    @POST
    @Path("/braintree/sms/checkout")
    @Timed(name = "checkoutSMSBraintree")
    @Secured(requiredShop = true)
    @ApiOperation(value = "checkoutSMSBraintree")
    public Result<Transaction> checkoutSMSBraintree(@Valid BraintreeBuyRequest request) {
        return blazePaymentService.checkoutSMSBraintree(request);
    }

    @POST
    @Path("/wepay/sms/checkout")
    @Timed(name = "checkoutSMSWepay")
    @Secured(requiredShop = true)
    @ApiOperation(value = "checkout wepay SMS credit request")
    @Deprecated
    public Payment checkoutSMSWepay(@Valid WepayCreditCardCheckoutRequest request) {
        return blazePaymentService.checkoutSMSWepay(request);
    }

    @POST
    @Path("/stripe/sms/checkout")
    @Timed(name = "checkSMSStripe")
    @Secured(requiredShop = true)
    @ApiOperation(value = "checkout stripe SMS credit request")
    @Deprecated
    public Payment checkSMSStripe(@Valid StripeSMSBuyRequest stripeSMSBuyRequest) {
        return blazePaymentService.checkoutSMSStripe(stripeSMSBuyRequest);
    }

    @GET
    @Path("/sms/provider")
    @Timed(name = "getPaymentProvider")
    @Secured(requiredShop = true)
    @ApiOperation("Get sms payment provider[Providers = STRIPE, WEPAY]")
    public PaymentProviderDetails getPaymentProvider() {
        return blazePaymentService.getDefaultPaymentProvider();
    }


    @POST
    @Path("/sms/checkout")
    @Timed(name = "checkSMSPayment")
    @Secured(requiredShop = true)
    @ApiOperation(value = "checkout SMS credit request")
    public Payment checkSMSPayment(@Valid SmsBuyRequest smsBuyRequest) {
        return blazePaymentService.checkoutSMSPayment(smsBuyRequest);
    }

    @GET
    @Path("/receipt/{purchaseReferenceId}/{paymentComponent}")
    @Timed(name = "getPurchaseReceipt")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get purchase receipt")
    public PaymentReceipt getPurchaseReceipt(@PathParam("purchaseReferenceId") String purchaseReferenceId, @PathParam("paymentComponent") PaymentComponent paymentComponent) {
        return blazePaymentService.getPurchaseReceipt(purchaseReferenceId, paymentComponent);
    }

}
