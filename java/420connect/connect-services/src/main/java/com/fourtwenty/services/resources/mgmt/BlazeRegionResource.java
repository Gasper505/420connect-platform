package com.fourtwenty.services.resources.mgmt;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.product.BlazeRegion;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.mgmt.BlazeRegionService;
import com.google.inject.Inject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.validation.Valid;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Api("Management - Region")
@Path("/api/v1/mgmt/region")
@Produces(MediaType.APPLICATION_JSON)
public class BlazeRegionResource extends BaseResource {

    @Inject
    private BlazeRegionService blazeRegionService;

    @GET
    @Secured
    @Path("/{regionId}")
    @Timed(name = "getRegionById")
    @ApiOperation(value = "Get region by id")
    public BlazeRegion getRegionById(@PathParam("regionId") String regionId) {
        return blazeRegionService.getRegionById(regionId);
    }

    @POST
    @Secured
    @Path("/")
    @Timed(name = "createRegion")
    @ApiOperation(value = "Create region")
    public BlazeRegion createRegion(@Valid BlazeRegion blazeRegion) {
        return blazeRegionService.createRegion(blazeRegion);
    }

    @PUT
    @Secured
    @Path("/{regionId}")
    @Timed(name = "updateRegion")
    @ApiOperation(value = "Update region")
    public BlazeRegion updateRegion(@PathParam("regionId") String regionId, @Valid BlazeRegion blazeRegion) {
        return blazeRegionService.updateRegion(regionId, blazeRegion);
    }

    @GET
    @Path("/")
    @Secured
    @Timed(name = "getRegions")
    @ApiOperation(value = "Get list of regions")
    public SearchResult<BlazeRegion> getRegions(@QueryParam("start") int start, @QueryParam("limit") int limit) {
        return blazeRegionService.getRegions(start, limit);
    }

    @DELETE
    @Path("/{regionId}")
    @Secured
    @Timed(name = "deleteRegionById")
    @ApiOperation(value = "Delete region by id")
    public Response deleteRegionById(@PathParam("regionId") String regionId) {
        blazeRegionService.deleteRegionById(regionId);
        return ok();
    }


}
