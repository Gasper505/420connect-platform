package com.fourtwenty.services.resources.mgmt;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.product.Brand;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.rest.dispensary.requests.inventory.BrandAddRequest;
import com.fourtwenty.core.rest.dispensary.requests.inventory.BrandBulkUpdateRequest;
import com.fourtwenty.core.rest.dispensary.results.BrandResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.dispensary.Audit;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.mgmt.BrandService;
import com.google.inject.Inject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created by Jyoti Verma  on 14/03/2018.
 */
@Api("Management - Brands")
@Path("api/v1/mgmt/brands")
@Produces(MediaType.APPLICATION_JSON)
public class BrandResource extends BaseResource {
    @Inject
    BrandService brandService;

    @GET
    @Path("/list/brands")
    @Timed(name = "getAllBrands")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Search Brand")
    public SearchResult<BrandResult> getAllBrands(@QueryParam("term") String term,
                                                  @QueryParam("start") int start,
                                                  @QueryParam("limit") int limit) {

        return brandService.getAllBrand(term, start, limit);
    }

    @GET
    @Path("/{brandId}/getBrandById")
    @Timed(name = "getBrandById")
    @Secured(requiredShop = true)
    @ApiOperation(value = "getBrand By Id")
    public BrandResult getBrandByBrandId(@PathParam("brandId") String brandId) {

        return brandService.getBrandByBrandId(brandId);
    }

    @GET
    @Path("/list/deletedbrands")
    @Timed(name = "getAllDeletedBrands")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Search Deleted Brand")
    public SearchResult<BrandResult> getAllDeletedBrand(
            @QueryParam("start") int start,
            @QueryParam("limit") int limit) {
        return brandService.getAllDeletedBrand(start, limit);
    }


    @POST
    @Path("/addBrand")
    @Timed(name = "addBrand")
    @Secured(requiredShop = true)
    @Audit(action = "Create Brand")
    @ApiOperation(value = "Create Brand")
    public Brand createProductBrand(@Valid BrandAddRequest addRequest) {
        return brandService.addBrand(addRequest);
    }

    @PUT
    @Path("/{brandId}/updateBrand")
    @Secured(requiredShop = true)
    @Timed(name = "updateBrand")
    @Audit(action = "Update Brand")
    @ApiOperation(value = "Update  Brand")
    public Brand updateBrand(@PathParam("brandId") String brandId, @Valid Brand updateRequest) {
        return brandService.updateBrand(brandId, updateRequest);
    }

    @DELETE
    @Path("/{brandId}/deleteBrand")
    @Secured(requiredShop = true)
    @Timed(name = "deleteBrand")
    @Audit(action = "Delete Brand")
    @ApiOperation(value = "Delete Brand")
    public Response deleteBrand(@PathParam("brandId") String brandId) {
        brandService.deleteBrand(brandId);
        return ok();
    }

    @GET
    @Path("/{brandId}/vendors")
    @Secured(requiredShop = true)
    @Timed(name = "getVendorsByBrand")
    @ApiOperation(value = "Get vendor's list that sales the brand")
    public List<Vendor> getVendorsByBrand(@PathParam("brandId") String brandId) {
        return brandService.getVendorsByBrand(brandId);
    }

    @PUT
    @Path("/bulkBrandUpdate")
    @Secured(requiredShop = true)
    @Timed(name = "bulkBrandUpdate")
    @ApiOperation(value = "Bulk brand update")
    public Response bulkBrandUpdate(@Valid BrandBulkUpdateRequest request) {
        brandService.bulkBrandUpdate(request);
        return ok();
    }

}
