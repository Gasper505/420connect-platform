package com.fourtwenty.services.resources.mgmt;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.domain.models.company.CompanyFeatures;
import com.fourtwenty.core.rest.dispensary.requests.company.CompanyEmailRequest;
import com.fourtwenty.core.rest.dispensary.requests.company.StripeCustomerRequest;
import com.fourtwenty.core.rest.dispensary.requests.company.CompanyEmployeeSeatRequest;
import com.fourtwenty.core.rest.dispensary.requests.company.StripeSourceRequest;
import com.fourtwenty.core.rest.dispensary.requests.company.StripeSubscriptionSourceRequest;
import com.fourtwenty.core.security.dispensary.Audit;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.mgmt.CompanyService;
import com.fourtwenty.core.services.payments.StripeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.glassfish.jersey.media.multipart.FormDataParam;
import com.stripe.model.Customer;
import com.stripe.model.Subscription;
import com.stripe.model.Source;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.InputStream;
import java.util.List;

/**
 * Created by mdo on 5/15/16.
 */
@Api("Management - Company")
@Path("/api/v1/mgmt/company")
@Produces(MediaType.APPLICATION_JSON)
public class CompanyResource extends BaseResource {
    @Inject
    CompanyService companyService;
    @Inject
    StripeService stripeService;


    @GET
    @Path("/")
    @Timed(name = "getCurrentCompany")
    @Secured
    @ApiOperation(value = "Get Current Company")
    public Company getCurrentCompany() {
        return companyService.getCurrentCompany();
    }

    @GET
    @Path("/list")
    @Timed(name = "getCompanies")
    @Secured
    @ApiOperation(value = "Get Companies")
    public List<Company> getCompanies() {
        return companyService.getCompanies();
    }

    @POST
    @Path("/")
    @Timed(name = "updateCompany")
    @Secured
    @Audit(action = "Update Company")
    @ApiOperation(value = "Update Company")
    public Company updateCompany(Company company) {
        return companyService.updateCompany(company);
    }


    @POST
    @Path("/logo")
    @Secured
    @Timed(name = "updateCompanyLogo")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Audit(action = "Update Company Logo")
    @ApiOperation(value = "Update Company Logo")
    public Company updateCompanyLogo(@FormDataParam("file") InputStream inputStream) {
        return companyService.updateCompanyLogo(inputStream);
    }

    @GET
    @Path("/features")
    @Secured
    @Timed(name = "getCompanyFeatures")
    @ApiOperation(value = "Get Company Features")
    public CompanyFeatures getCompanyFeatures() {
        return companyService.getCompanyFeatures();
    }


    @POST
    @Path("/{companyId}/sendMail")
    @Secured
    @Timed(name = "sendEmailToCompany")
    @ApiOperation(value = "Send Email To Company")
    public Response sendEmailToCompany(@PathParam("companyId") String companyId, @Valid CompanyEmailRequest request) {
        companyService.sendEmailToCompany(companyId, request);
        return Response.ok().build();
    }

    @GET
    @Path("/{companyId}/onprem/config")
    @Secured
    @Timed(name = "getOnPremConfig")
    @ApiOperation(value = "Get Company OnPrem Configs")
    public Company.OnPremCompanyConfig getOnPremConfig(@PathParam("companyId") String companyId) {
        return companyService.getOnPremConfigs(companyId);
    }

    @GET
    @Path("/createStripeCustomers")
    @Timed(name = "createStripeCustomers")
    @Secured
    @ApiOperation(value = "Create Company Stripes")
    public List<Company> createStripeCustomers() {
        return stripeService.createCustomerAllCompany();
    }

    @GET
    @Path("/{companyId}/getCustomer")
    @Secured
    @Timed(name = "getCustomer")
    @ApiOperation(value = "Get Stripe Customer")
    public String getCustomer(@PathParam("companyId") String companyId) {
        return stripeService.getCompanyStripeCustomer(companyId);
    }

    @GET
    @Path("/payments/{customerId}")
    @Secured
    @Timed(name = "getStripeCustomerPayments")
    @ApiOperation(value = "Get Stripe Customer Payments")
    public String getStripeCustomerPayments(@PathParam("customerId") String customerId) {
        return stripeService.getStripeCustomerPayments(customerId);
    }

    @POST
    @Path("/updateStripeCustomerAddress")
    @Secured
    @Timed(name = "updateStripeCustomerAddress")
    @ApiOperation(value = "update Stripe Customer Address")
    public Customer updateStripeCustomerAddress(@Valid StripeCustomerRequest request) {
        return stripeService.updateStripeCustomerAddress(request);
    }

    @POST
    @Path("/updateCompanyEmployeeSeatSubscription")
    @Secured
    @Timed(name = "updateCompanyEmployeeSeatSubscription")
    @ApiOperation(value = "update Company Employee Seat Subscription")
    public Subscription updateCompanyEmployeeSeatSubscription(@Valid CompanyEmployeeSeatRequest request) {
        companyService.updateCompanyMaxEmployee(request);
        return stripeService.updateStripeSubscriptionEmployeeSeat(request);
    }

    @POST
    @Path("/attachCustomerSource")
    @Secured
    @Timed(name = "attachCustomerSource")
    @ApiOperation(value = "attach Customer Source")
    public Customer attachCustomerSource(@Valid StripeSourceRequest request) {
        return stripeService.attachCustomerSource(request);
    }

    @POST
    @Path("/setCustomerDefaultSource")
    @Secured
    @Timed(name = "setCustomerDefaultSource")
    @ApiOperation(value = "set Customer Default Source")
    public Customer setCustomerDefaultSource(@Valid StripeSourceRequest request) {
        return stripeService.setCustomerDefaultSource(request);
    }

    @POST
    @Path("/removeCustomerSource")
    @Secured
    @Timed(name = "removeCustomerSource")
    @ApiOperation(value = "remove Customer Source")
    public Source removeCustomerSource(@Valid StripeSourceRequest request) {
        return stripeService.removeCustomerSource(request);
    }

    @POST
    @Path("/updateSubscriptionDefaultPayment")
    @Secured
    @Timed(name = "updateSubscriptionDefaultPayment")
    @ApiOperation(value = "update Subscription Payment")
    public Subscription updateSubscriptionDefaultPayment(@Valid StripeSubscriptionSourceRequest request) {
        return stripeService.updateSubscriptionDefaultPayment(request);
    }
}
