package com.fourtwenty.services.resources.mgmt;


import com.blaze.clients.metrcs.models.strains.MetrcStrainRequest;
import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.compliance.*;
import com.fourtwenty.core.rest.common.LabelValuePair;
import com.fourtwenty.core.rest.compliance.IntakeMetrcPackageRequest;
import com.fourtwenty.core.rest.dispensary.results.ListResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.dispensary.Audit;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.compliance.ComplianceService;
import com.google.inject.Inject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.glassfish.jersey.media.multipart.FormDataParam;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.InputStream;
import java.util.List;

@Api("Management - Compliance")
@Path("/api/v1/mgmt/compliance")
@Produces(MediaType.APPLICATION_JSON)
public class ComplianceResource  extends BaseResource {
    @Inject
    ComplianceService complianceService;


    @GET
    @Path("/syncjobs")
    @Timed(name = "getComplianceSyncJobs")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get Sync Jobs")
    public SearchResult<ComplianceSyncJob> getComplianceSyncJobs(@QueryParam("skip") int skip,
                                                                 @QueryParam("limit") int limit) {
        return complianceService.getComplianceSyncJobs(skip,limit);
    }

    @GET
    @Path("/metrc/sync")
    @Timed(name = "syncMetrc")
    @Secured(requiredShop = true)
    @ApiOperation(value = "syncMetrc")
    public Response syncMetrc() {
        complianceService.sync();
        return ok();
    }

    @GET
    @Path("/metrc/resetSync")
    @Timed(name = "resetSync")
    @Secured(requiredShop = true)
    @ApiOperation(value = "resetSync")
    public Response resetSync() {
        return null;
    }


    /*
    BLAZE COMPLIANCE PACKAGES
     */
    @GET
    @Path("/metrc/blazepackages")
    @Timed(name = "getComplianceBlazeBatches")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get All Compliance Batches")
    public Response getComplianceBlazeBatches(@QueryParam("skip") int start, @QueryParam("limit") int limit) {
        return null;
    }

    @POST
    @Path("/metrc/batches/assign")
    @Timed(name = "assignMetrcTagsToBatches")
    @Secured(requiredShop = true)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Audit(action = "Batch - Assign Metrc Tags")
    @ApiOperation(value = "Batch - Assign Metrc Tags")
    public Response assignMetrcTagsToBatches(@FormDataParam("tagFile") InputStream tagFileInput) {
        complianceService.assignMetrcBatches(tagFileInput);
        return ok();
    }

    @POST
    @Path("/metrc/items/bulkcreate")
    @Timed(name = "assignMetrcTagsToBatches")
    @Secured(requiredShop = true)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Audit(action = "Batch - Assign Metrc Tags")
    @ApiOperation(value = "Batch - Assign Metrc Tags")
    public Response bulkCreateItemsStrains(@FormDataParam("tagFile") InputStream tagFileInput) {
        complianceService.bulkCreateItemsStrains(tagFileInput);
        return ok();
    }
    //
    // Metrc Packages
    //

    @GET
    @Path("/metrc/packages/active")
    @Timed(name = "getMetrcPackages")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get All Metrc Packages")
    public SearchResult<CompliancePackage> getMetrcActivePackages(@QueryParam("term") String term, @QueryParam("skip") int skip, @QueryParam("limit") int limit) {
        return complianceService.getCompliancePackages(CompliancePackage.PackageStatus.ACTIVE,term,skip,limit);
    }

    @GET
    @Path("/metrc/packages/tags")
    @Timed(name = "getMetrcPackages")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get All Metrc Packages")
    public ListResult<LabelValuePair> getMetrcActivePackagesTags(@QueryParam("term") String term, @QueryParam("skip") int skip, @QueryParam("limit") int limit) {
        return complianceService.getCompliancePackagesTags(CompliancePackage.PackageStatus.ACTIVE,term,skip,limit);
    }


    @GET
    @Path("/metrc/packages/onhold")
    @Timed(name = "getMetrcPackages")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get All OnHold Metrc Packages")
    public SearchResult<CompliancePackage> getMetrcOnHoldPackages(@QueryParam("term") String term,@QueryParam("skip") int skip, @QueryParam("limit") int limit) {
        return complianceService.getCompliancePackages(CompliancePackage.PackageStatus.ONHOLD,term,skip,limit);
    }

    @GET
    @Path("/metrc/packages/inactive")
    @Timed(name = "getMetrcPackages")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get All In-Active Metrc Packages")
    public SearchResult<CompliancePackage> getMetrcInActivePackages(@QueryParam("term") String term,@QueryParam("skip") int skip, @QueryParam("limit") int limit) {
        return complianceService.getCompliancePackages(CompliancePackage.PackageStatus.INACTIVE,term,skip,limit);
    }

    @POST
    @Path("/metrc/packages/{packageId}/intake")
    @Timed(name = "intakePackage")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Intake Metrc Package")
    @Audit(action = "Metrc - Intake Metrc Package")
    public CompliancePackage intakePackage(@PathParam("packageId") String packageId, @Valid IntakeMetrcPackageRequest request) {
        return complianceService.intakePackage(packageId,request);
    }

    //
    // Sales
    //

    @GET
    @Path("/metrc/sales")
    @Timed(name = "getMetrcSales")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get All Metrc Sales")
    public SearchResult<ComplianceSaleReceipt> getMetrcSales(@QueryParam("startDate") String startDate,
                                                             @QueryParam("endDate") String endDate,
                                                             @QueryParam("term") String term,
                                                             @QueryParam("skip") int skip,
                                                             @QueryParam("limit") int limit) {
        return complianceService.getSaleReceipts(term,startDate,endDate,skip,limit);
    }

    @GET
    @Path("/metrc/sales/{saleId}")
    @Timed(name = "getMetrcSaleById")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get Metrc Sale By Id")
    public ComplianceSaleReceipt getMetrcSaleById(@PathParam("saleId")String saleId) {
        return complianceService.getMetrcSaleById(saleId);
    }

    //
    // Transfers
    //
    @GET
    @Path("/metrc/transfers/incoming")
    @Timed(name = "getMetrcTransfers")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get All Active Metrc Transfers")
    public Response getMetrcTransfersActive(@QueryParam("skip") int skip,
                                  @QueryParam("limit") int limit) {
        return null;
    }

    @GET
    @Path("/metrc/transfers/completed")
    @Timed(name = "getMetrcTransfers")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get All Completed Metrc Transfers")
    public Response getMetrcTransfersCompleted(@QueryParam("skip") int skip,
                                      @QueryParam("limit") int limit) {
        return null;
    }

    @GET
    @Path("/metrc/transfers/{transferId}")
    @Timed(name = "getMetrcTransferById")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get Metrc Transfer By Id")
    public Response getMetrcTransferById(@PathParam("transferId")String saleId) {
        return null;
    }

    //
    // Items
    //
    @GET
    @Path("/metrc/items")
    @Timed(name = "getMetrcTransfers")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get All Items")
    public SearchResult<ComplianceItem> getMetrcItems(@QueryParam("term") String term,
                                                      @QueryParam("skip") int skip,
                                                      @QueryParam("limit") int limit) {
        return complianceService.getComplianceItems(term,skip,limit);
    }

    //
    // Items
    //
    @GET
    @Path("/metrc/strains")
    @Timed(name = "getMetrcStrains")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get All Metrc Strains")
    public SearchResult<ComplianceStrain> getMetrcStrains(@QueryParam("term") String term,
                                                          @QueryParam("skip") int skip,
                                                          @QueryParam("limit") int limit) {
        return complianceService.getComplianceStrains(term,skip,limit);
    }

    //
    // Items
    //
    @GET
    @Path("/metrc/categories")
    @Timed(name = "getMetrcCategories")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get All Metrc Categories")
    public SearchResult<ComplianceCategory> getMetrcCategories() {
        return complianceService.getComplianceCategories();
    }


    //metrc strain
    @POST
    @Path("/metrc/strains")
    @Timed(name = "createMetrcStrain")
    @Secured(requiredShop = true)
    @Audit(action = "Create- Metrc Strain")
    @ApiOperation(value = "Create Metrc Strains")
    public Response bulkCreateMetrcStarin(@Valid List<MetrcStrainRequest> metrcStrainRequests) {
         complianceService.bulkCreateMetrcStrain(metrcStrainRequests);
        return ok();
    }

    // metrc batches
    @POST
    @Path("/metrc/batches")
    @Timed(name = "createMetrcBatches")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Batch - Create Metrc Tags Batches")
    public Response createMetrcTagsBatches(@Valid List<ComplianceBatchPackagePair> metrcBatchesRequests) {
        complianceService.createNewMetrcBatches(metrcBatchesRequests);
        return ok();
    }

    // metrc items
    @POST
    @Path("/metrc/items")
    @Timed(name = "createMetrcItems")
    @Secured(requiredShop = true)
    @Audit(action = "Items - Create Metrc Tags Items")
    @ApiOperation(value = "Items - Create Metrc Tags Items")
    public Response bulkCreateMetrcTagsItems(@Valid List<ComplianceBatchPackagePair> metrcItems) {
        complianceService.bulkCreateItems(metrcItems);
        return ok();
    }

}
