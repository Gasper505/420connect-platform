package com.fourtwenty.services.resources.mgmt;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.company.Contract;
import com.fourtwenty.core.rest.dispensary.requests.company.ContractAddRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.dispensary.Audit;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.mgmt.ContractService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by Stephen Schmidt on 12/7/2015.
 */

@Api("Management - Contracts")
@Path("/api/v1/mgmt/contracts")
@Produces(MediaType.APPLICATION_JSON)
public class ContractResource extends BaseResource {
    @Inject
    ContractService contractService;

    //Only pass either an int for version or boolean for active,
    //Do not pass both or will error
    @GET
    @Path("/")
    @Secured(requiredShop = true)
    @Timed(name = "getContractsForShop")
    @ApiOperation(value = "Get Contracts For Shop")
    public SearchResult<Contract> getContractsForShop() {
        return contractService.getContractsForShop();
    }


    @GET
    @Path("/{contractId}")
    @Secured(requiredShop = true)
    @Timed(name = "getContractById")
    @ApiOperation(value = "Get Contracts By Id")
    public Contract getContractById(@PathParam("contractId") String contractId) {
        return contractService.getContract(contractId);
    }

    @POST
    @Path("/activate/{contractId}")
    @Secured(requiredShop = true)
    @Timed(name = "setContractActive")
    @Audit(action = "Set Active Contract")
    @ApiOperation(value = "Activate Contract")
    public void setActiveContract(@PathParam("contractId") String contractId) {
        contractService.setActiveContract(contractId);
    }


    //CRUD below

    @POST
    @Path("/")
    @Secured(requiredShop = true)
    @Timed(name = "createContract")
    @Audit(action = "Create Contract")
    @ApiOperation(value = "Create Contract")
    public Contract createContract(@Valid ContractAddRequest request) {
        return contractService.addContract(request);
    }

    @POST
    @Path("/{contractId}")
    @Secured(requiredShop = true)
    @Timed(name = "updateContract")
    @Audit(action = "Update Contract")
    @ApiOperation(value = "Update Contract")
    public Contract updateContract(@PathParam("contractId") String contractId, @Valid Contract request) {
        return contractService.updateContract(contractId, request);
    }

    @DELETE
    @Path("/{contractId}")
    @Secured(requiredShop = true)
    @Timed(name = "deleteContract")
    @Audit(action = "Delete Contract")
    @ApiOperation(value = "Delete Contract")
    public Response deleteContract(@PathParam("contractId") String contractId) {
        contractService.deleteContract(contractId);
        return ok();
    }

}
