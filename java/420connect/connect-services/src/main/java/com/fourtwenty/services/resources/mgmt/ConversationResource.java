package com.fourtwenty.services.resources.mgmt;


import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.marketing.MarketingJobLog;
import com.fourtwenty.core.domain.models.transaction.Conversation;
import com.fourtwenty.core.rest.features.ActiveConversationResult;
import com.fourtwenty.core.rest.features.ConversationResult;
import com.fourtwenty.core.rest.features.TwilioMessageRequest;
import com.fourtwenty.core.rest.features.TwilioResult;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.mgmt.ConversationService;
import com.fourtwenty.core.services.sms.IncomingSMSService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;
import java.io.StringWriter;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;


@Api("Management - Conversations")
@Path("/api/v1/mgmt/conversation")
@Produces(MediaType.APPLICATION_JSON)
public class ConversationResource {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConversationResource.class);

    @Inject
    private ConversationService conversationService;
    @Inject
    private IncomingSMSService incomingSMSService;

    @POST
    @Path("/send")
    @Timed(name = "sendTwilioMessage")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Send message through Transaction id")
    public TwilioResult<Conversation> sendTwilioMessage(@Valid TwilioMessageRequest request) {
        final Conversation conversationStatus = conversationService.sendConversation(request);
        TwilioResult result = new TwilioResult();
        result.setStatus(Boolean.TRUE);
        result.setData(conversationStatus);
        return result;

    }

    @POST
    @Path("/receive")
    @Timed(name = "receiveTwilioMessage")
    @ApiOperation(value = "Receive Twilio Message (Only for twilio)")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public void receiveTwilioMessage(@CookieParam("txId") String transactionId,
                                     @Context HttpServletRequest request,
                                     @FormParam("SmsMessageSid") String SmsMessageSid,
                                     @FormParam("From") String From,
                                     @FormParam("To") String To,
                                     @FormParam("MessageSid") String MessageSid,
                                     @FormParam("Body") String Body,
                                     @FormParam("SmsStatus") String SmsStatus) {
        final String toNumber = To;
        final String fromNumber = From;
        final String message = Body;
        final String sid = MessageSid;
        final String messageStatus = SmsStatus;

        String key = null;
        Enumeration<String> list = request.getParameterNames();
        while (list.hasMoreElements()) {
            key = list.nextElement();
            LOGGER.info("parameter: " + key + " value: " + request.getParameter(key));
        }

        LOGGER.info(String.format("Receive Message from Twilio: %s, status: %s, from: %s, to: %s,  txId: , body: %s",
                sid, messageStatus, fromNumber, toNumber, transactionId, Body));
        try {
            if (transactionId == null) {
                transactionId = conversationService.getTransactionToAndFromNumber(toNumber, fromNumber);
            }
        } catch (Exception e) {
            LOGGER.info(e.getMessage(), e);
        }

        try {
            MarketingJobLog.MSentStatus status = incomingSMSService.receiveSMSMessage(message, toNumber, fromNumber);

            LOGGER.info(fromNumber + " sent '" + status + "' message");
        } catch (Exception e) {
            LOGGER.info(e.getMessage(), e);
        }

        conversationService.receiveConversation(transactionId, message, sid);
        Response.ok("Conversation has been saved").cookie(new NewCookie("txId", transactionId)).build();
    }

    @POST
    @Path("/messageStatus")
    @Timed(name = "messageStatus")
    @ApiOperation(value = "get Twilio message status (Only for twilio)")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response messageStatus(@Context HttpServletRequest request, @Context HttpServletResponse response,
                                  @FormParam("SmsMessageSid") String SmsMessageSid,
                                  @FormParam("From") String From,
                                  @FormParam("To") String To,
                                  @FormParam("MessageSid") String MessageSid,
                                  @FormParam("Body") String Body,
                                  @FormParam("SmsStatus") String SmsStatus) {
        final String messageSid = MessageSid;
        final String messageStatus = SmsStatus;
        final String from = From;
        final String to = To;


        IncomingSMSService.TwilioMessageStatus status = incomingSMSService.receiveSMSStatus(messageStatus, to, from);

        LOGGER.info(from + " sent '" + status + "' message");

        conversationService.updateConversationByMessageId(messageSid, messageStatus);
        LOGGER.info(String.format("Status Message from Twilio: %s, status: %s, from: %s, to: %s, body: %s", messageSid, messageStatus, from, to, Body)); //"Message from Twilio: " + messageSid + " status: " + messageStatus + ", From: " + from + ", To: " + To);
        return Response.ok("Status has been updated").build();
    }

    @GET
    @Path("/messageStatus")
    @Timed(name = "messageStatus")
    @ApiOperation(value = "get Twilio message status (Only for twilio)")
    public Response messageStatusGET(@Context HttpServletRequest request, @Context HttpServletResponse response) {
        final String messageSid = request.getParameter("MessageSid");
        final String messageStatus = request.getParameter("MessageStatus");
        final String from = request.getParameter("From");
        final String body = request.getParameter("Body");

        try {
            StringWriter writer = new StringWriter();
            IOUtils.copy(request.getInputStream(), writer, "UTF-8");
            String theString = writer.toString();
            LOGGER.info("post body: " + theString);
        } catch (Exception e) {

        }

        conversationService.updateConversationByMessageId(messageSid, messageStatus);
        LOGGER.info("Message from Twilio: " + messageSid + " status: " + messageStatus + " from: " + from + " body: " + body);
        return Response.ok("Status has been updated").build();
    }

    @GET
    @Path("/messageList/{transactionId}")
    @Timed(name = "messageList")
    @Secured(requiredShop = true)
    @ApiOperation(value = "get Messages list between Employee and Member")
    public TwilioResult<List<Conversation>> messageList(@PathParam("transactionId") String transactionId,
                                                        @QueryParam("afterDate") long afterDate, @QueryParam("beforeDate") long beforeDate) {
        List<Conversation> transactionConversationList = conversationService.getTransactionConversation(transactionId, afterDate, beforeDate, Conversation.ConversationType.TWILLIO);
        final TwilioResult<List<Conversation>> twilioResult = new TwilioResult<>();
        try {
            if (!transactionConversationList.isEmpty()) {
                twilioResult.setStatus(Boolean.TRUE);
                twilioResult.setData(transactionConversationList);
            } else {
                twilioResult.setStatus(Boolean.FALSE);
                twilioResult.setData(Collections.<Conversation>emptyList());
            }
        } catch (Exception ex) {
            LOGGER.warn("Can't get list of conversations", ex);
            twilioResult.setStatus(Boolean.FALSE);
            twilioResult.setData(Collections.<Conversation>emptyList());
        }
        return twilioResult;
    }

    @POST
    @Path("/chat")
    @Timed(name = "chat")
    @Secured(requiredShop = true)
    @ApiOperation(value = "chat using Transaction id")
    public TwilioResult<Conversation> sendChatMessage(@Valid TwilioMessageRequest request) {
        final Conversation conversationStatus = conversationService.sendConversation(request);
        TwilioResult result = new TwilioResult();
        result.setStatus(Boolean.TRUE);
        result.setData(conversationStatus);
        return result;
   }


    @GET
    @Path("/chat")
    @Timed(name = "chat")
    @Secured(requiredShop = true)
    @ApiOperation(value = "get chat messages list between Employees")
    public TwilioResult<List<ConversationResult>> getAllConversationByCurrentUser(@QueryParam("afterDate") long afterDate,
                                                                                  @QueryParam("beforeDate") long beforeDate) {
        return conversationService.getTransactionConversationByCurrentUser(afterDate, beforeDate, Conversation.ConversationType.PUSHER);

    }


    @GET
    @Path("/chat/{transactionId}")
    @Timed(name = "chat")
    @Secured(requiredShop = true)
    @ApiOperation(value = "get all chat by transactionId between Employee and Member")
    public TwilioResult<List<ConversationResult>> getAllConversationByTransactionId(@PathParam("transactionId") String transactionId,
                                                                                    @QueryParam("afterDate") long afterDate, @QueryParam("beforeDate") long beforeDate) {
        return conversationService.getConversationByTransaction(transactionId, afterDate, beforeDate, Conversation.ConversationType.PUSHER);
    }

    @PUT
    @Path("/chat/{chatId}/{status}")
    @Timed(name = "updateStatus")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Update message status")
    public Conversation updateMessageStatus(@PathParam("chatId") String chatId, @PathParam("status") Conversation.Status status) {
        return conversationService.updateMessageStatus(chatId, status);
    }

    @GET
    @Path("/activeConversation")
    @Timed(name = "activeConversation")
    @Secured(requiredShop = true)
    @ApiOperation(value = "get all active Conversation ")
    public TwilioResult<List<ActiveConversationResult>> getActiveTransactionChat() {
        return conversationService.getActiveTransactionChat();
    }
}
