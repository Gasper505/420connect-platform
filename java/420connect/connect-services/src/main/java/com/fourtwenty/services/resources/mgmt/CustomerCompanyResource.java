package com.fourtwenty.services.resources.mgmt;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.models.company.CompanyContact;
import com.fourtwenty.core.domain.models.company.CompanyContactLog;
import com.fourtwenty.core.domain.models.company.CustomerCompany;
import com.fourtwenty.core.rest.dispensary.requests.company.CustomerCompanyAddRequest;
import com.fourtwenty.core.rest.dispensary.requests.company.CustomerCompanyCredit;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.company.CompanyContactResult;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.mgmt.CompanyContactLogService;
import com.fourtwenty.core.services.mgmt.CompanyContactService;
import com.fourtwenty.core.services.mgmt.CustomerCompanyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Api("Management - Customer Company")
@Path("/api/v1/mgmt/customerCompany")
@Produces(MediaType.APPLICATION_JSON)
public class CustomerCompanyResource extends BaseResource {

    private CustomerCompanyService customerCompanyService;
    private CompanyContactService companyContactService;
    private CompanyContactLogService companyContactLogService;

    @Inject
    public CustomerCompanyResource(CustomerCompanyService customerCompanyService,
                                   CompanyContactService companyContactService,
                                   CompanyContactLogService companyContactLogService) {
        this.customerCompanyService = customerCompanyService;
        this.companyContactService = companyContactService;
        this.companyContactLogService = companyContactLogService;
    }

    @Deprecated
    @POST
    @Path("/")
    @Timed(name = "createCustomerCompany")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Create Customer Company")
    public CustomerCompany createCustomerCompany(@Valid CustomerCompanyAddRequest request) {
        return customerCompanyService.addCustomerCompany(request);
    }

    @Deprecated
    @GET
    @Path("/{customerCompanyId}")
    @Timed(name = "getCustomerCompany")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get Customer Company by id")
    public CustomerCompany getCustomerCompany(@PathParam("customerCompanyId") String customerCompanyId) {
        return customerCompanyService.getCustomerCompany(customerCompanyId);
    }

    @Deprecated
    @PUT
    @Path("/{customerCompanyId}")
    @Timed(name = "updateCustomerCompany")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Update Customer Company")
    public CustomerCompany updateCustomerCompany(@PathParam("customerCompanyId") String customerCompanyId, @Valid CustomerCompany customerCompany) {
        return customerCompanyService.updateCustomerCompany(customerCompanyId, customerCompany);
    }

    @Deprecated
    @DELETE
    @Path("/{customerCompanyId}")
    @Timed(name = "deleteCustomerCompany")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Delete Customer Company by id")
    public Response deleteCustomerCompany(@PathParam("customerCompanyId") String customerCompanyId) {
        customerCompanyService.deleteCustomerCompany(customerCompanyId);
        return Response.ok().build();
    }

    @Deprecated
    @GET
    @Path("/list")
    @Timed(name = "getAllCustomerCompany")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get all customer company")
    public SearchResult<CustomerCompany> getAllCustomerCompany(@QueryParam("start") int start, @QueryParam("limit") int limit, @QueryParam("term") String term) {
        return customerCompanyService.getAllCustomerCompany(start, limit, term);
    }

    @Deprecated
    @GET
    @Path("/{customerCompanyId}/attachment/{attachmentId}")
    @Timed(name = "getCustomerCompanyAttachment")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get attachment of customer company by id")
    public CompanyAsset getCustomerCompanyAttachment(@PathParam("customerCompanyId") String customerCompanyId, @PathParam("attachmentId") String attachmentId) {
        return customerCompanyService.getCustomerCompanyAttachment(customerCompanyId, attachmentId);
    }

    @Deprecated
    @POST
    @Path("/{customerCompanyId}/attachment")
    @Timed(name = "addCustomerCompanyAttachment")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Add attachment of customer company")
    public CompanyAsset addCustomerCompanyAttachment(@PathParam("customerCompanyId") String customerCompanyId, @Valid CompanyAsset asset) {
        return customerCompanyService.addCustomerCompanyAttachment(customerCompanyId, asset);
    }

    @Deprecated
    @PUT
    @Path("/{customerCompanyId}/attachment/{attachmentId}")
    @Timed(name = "updateCustomerCompanyAttachment")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Update attachment by id")
    public CompanyAsset updateCustomerCompanyAttachment(@PathParam("customerCompanyId") String customerCompanyId, @PathParam("attachmentId") String attachmentId, @Valid CompanyAsset asset) {
        return customerCompanyService.updateCustomerCompanyAttachment(customerCompanyId, attachmentId, asset);
    }

    @Deprecated
    @DELETE
    @Path("/{customerCompanyId}/attachment/{attachmentId}")
    @Timed(name = "deleteCustomerCompanyAttachment")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Delete customer company's attachment by id")
    public Response deleteCustomerCompanyAttachment(@PathParam("customerCompanyId") String customerCompanyId, @PathParam("attachmentId") String attachmentId) {
        customerCompanyService.deleteCustomerCompanyAttachment(customerCompanyId, attachmentId);
        return Response.ok().build();
    }

    @POST
    @Path("/contact")
    @Timed(name = "createCompanyContact")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Create Company Contact")
    public CompanyContact createCompanyContact(@Valid CompanyContact companyContact) {
        return companyContactService.createCompanyContact(companyContact);
    }

    @PUT
    @Path("/contact/{contactId}")
    @Timed(name = "updateCompanyContact")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Update Company Contact by id")
    public CompanyContact updateCompanyContact(@PathParam("contactId") String contactId, @Valid CompanyContact companyContact) {
        return companyContactService.updateCompanyContact(contactId, companyContact);
    }

    @GET
    @Path("/contact/{contactId}")
    @Timed(name = "getCompanyContactById")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get company contact by id")
    public CompanyContactResult getCompanyContactById(@PathParam("contactId") String contactId) {
        return companyContactService.getCompanyContactById(contactId);
    }

    @DELETE
    @Path("/contact/{contactId}")
    @Timed(name = "deleteCompanyContact")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Delete company contact by id")
    public Response deleteCompanyContact(@PathParam("contactId") String contactId) {
        companyContactService.deleteCompanyContact(contactId);
        return Response.ok().build();
    }

    @GET
    @Path("/contact/list")
    @Timed(name = "getAllCompanyContact")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get all company contact")
    public SearchResult<CompanyContactResult> getAllCompanyContact(@QueryParam("customerCompanyId") String customerCompanyId, @QueryParam("start") int start, @QueryParam("limit") int limit, @QueryParam("term") String term) {
        return companyContactService.getAllCompanyContact(customerCompanyId, start, limit, term);
    }

    @GET
    @Path("/{customerCompanyId}/contactLog/list")
    @Timed(name = "getAllCompanyContactLogs")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get All Company Contact Logs")
    public SearchResult<CompanyContactLog> getAllCompanyContactLogs(@PathParam("customerCompanyId") String customerCompanyId, @QueryParam("start") int start, @QueryParam("limit") int limit) {
        return companyContactLogService.getAllCompanyContactLogsByCustomerCompany(customerCompanyId, start, limit);
    }

    @PUT
    @Path("/{customerCompanyId}/credit")
    @Timed(name = "addCreditInCustomerCompany")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Add Credit In Customer Company")
    public CustomerCompany addCreditInCustomerCompany(@PathParam("customerCompanyId") String customerCompanyId, @Valid CustomerCompanyCredit companyCredit) {
        return customerCompanyService.addCreditInCustomerCompany(customerCompanyId, companyCredit);
    }

    @PUT
    @Path("/{customerCompanyId}/creditUpdate")
    @Timed(name = "updateCreditInCustomerCompany")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Update Credit In Customer Company")
    public CustomerCompany updateCreditInCustomerCompany(@PathParam("customerCompanyId") String customerCompanyId, @Valid CustomerCompanyCredit companyCredit) {
        return customerCompanyService.updateCreditInCustomerCompany(customerCompanyId, companyCredit);
    }
}
