package com.fourtwenty.services.resources.mgmt;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.company.DeliveryZone;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.models.views.MemberDistanceResult;
import com.fourtwenty.core.rest.dispensary.results.DeliveryDriverEmployeeResult;
import com.fourtwenty.core.rest.dispensary.results.EmployeeLocationResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.TransactionMileageResult;
import com.fourtwenty.core.rest.dispensary.results.company.EmployeeDailyMileageResult;
import com.fourtwenty.core.rest.dispensary.results.company.EmployeeTransactionDistanceResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.InventoryByEmployeeResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.ProductPackagesResult;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.inventory.InventoryActionLog;
import com.fourtwenty.core.services.mgmt.DeliveryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created by decipher on 28/11/17 2:37 PM
 * Abhishek Samuel (Software Engineer)
 * abhishke.decipher@gmail.com
 * Decipher Zone Softwares
 * www.decipherzone.com
 */
@Api("Management - Delivery")
@Path("/api/v1/mgmt/delivery")
@Produces(MediaType.APPLICATION_JSON)
public class DeliveryResource extends BaseResource {

    private DeliveryService deliveryService;

    @Inject
    public DeliveryResource(DeliveryService deliveryService) {
        this.deliveryService = deliveryService;
    }

    @POST
    @Path("/")
    @Timed(name = "addDeliverZone")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Add delivery zone")
    public DeliveryZone addDeliverZone(@Valid DeliveryZone deliveryZone) {
        return deliveryService.addDeliverZone(deliveryZone);
    }

    @PUT
    @Path("/{deliveryZoneId}")
    @Timed(name = "updateDeliveryArea")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Update delivery area")
    public DeliveryZone updateDeliveryArea(@PathParam("deliveryZoneId") String deliveryZoneId, @Valid DeliveryZone deliveryZone) {
        return deliveryService.updateDeliveryArea(deliveryZoneId, deliveryZone);
    }

    @DELETE
    @Path("/{deliveryZoneId}")
    @Timed(name = "deleteDeliveryArea")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Delete delivery area")
    public Response deleteDeliveryArea(@PathParam("deliveryZoneId") String deliveryZoneId) {
        deliveryService.deleteDeliveryArea(deliveryZoneId);
        return ok();
    }

    @GET
    @Path("/list")
    @Timed(name = "getAllDeliveryZone")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get all delivery zone")
    public SearchResult<DeliveryZone> getAllDeliveryZone(@QueryParam("start") int start,
                                                         @QueryParam("limit") int limit,
                                                         @QueryParam("zoneType") DeliveryZone.DeliveryZoneType zoneType) {
        return deliveryService.getAllDeliveryZone(start, limit, zoneType);
    }

    @GET
    @Path("/freeDrivers")
    @Timed(name = "getFreeDrivers")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get free drivers")
    public SearchResult<EmployeeLocationResult> getFreeDrivers(@QueryParam("start") int start, @QueryParam("limit") int limit) {
        return deliveryService.getFreeDrivers(start, limit);
    }

    @GET
    @Path("/enRouteDrivers")
    @Timed(name = "getEnRouteDrivers")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get en route employee")
    public SearchResult<EmployeeLocationResult> getEnRouteDrivers() {
        return deliveryService.getEnRouteEmployee();
    }

    @GET
    @Path("/{employeeId}/driverMileage")
    @Timed(name = "getDailyMileageByDriver")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get driver's daily mileage")
    public EmployeeDailyMileageResult getDailyMileageByDriver(@PathParam("employeeId") String employeeId) {
        return deliveryService.getDailyMileageByDriver(employeeId);
    }

    @GET
    @Path("/{employeeId}/transactionMileage")
    @Timed(name = "getMileageByDriverAndTransaction")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get mileage of driver by transaction")
    public SearchResult<TransactionMileageResult> getMileageByDriverAndTransaction(@PathParam("employeeId") String employeeId, @QueryParam("start") int start, @QueryParam("limit") int limit) {
        return deliveryService.getMileageByDriverAndTransaction(employeeId, start, limit);
    }

    @GET
    @Path("/deliveryDrivers")
    @Timed(name = "getDeliveryDriverEmployee")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get delivery drivers list")
    public SearchResult<DeliveryDriverEmployeeResult> getDeliveryDriverEmployee(@QueryParam("term") String term,
                                                                                @QueryParam("start") int start,
                                                                                @QueryParam("limit") int limit) {
        return deliveryService.getDeliveryDriverEmployee(term, start, limit);
    }

    @GET
    @Path("/{deliveryZoneId}")
    @Timed(name = "getDeliveryZoneById")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get delivery zone by id")
    public DeliveryZone getDeliveryZoneById(@PathParam("deliveryZoneId") String deliveryZoneId) {
        return deliveryService.getDeliveryZoneById(deliveryZoneId);
    }

    @GET
    @Path("/{employeeId}/transactions")
    @Timed(name = "getTransactionByEmployeeId")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get transaction by employee id")
    public SearchResult<Transaction> getTransactionByEmployeeId(@PathParam("employeeId") String employeeId, @QueryParam("start") int start,
                                                                @QueryParam("limit") int limit) {
        return deliveryService.getTransactionByEmployeeId(employeeId, start, limit);
    }

    @GET
    @Path("/{employeeId}/inventory")
    @Timed(name = "getInventoryByEmployee")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get inventory by employee")
    public SearchResult<ProductPackagesResult> getInventoryByEmployee(@PathParam("employeeId") String employeeId, @QueryParam("start") int start,
                                                                      @QueryParam("limit") int limit, @QueryParam("term") String searchTerm) {
        return deliveryService.getInventoryByEmployee(employeeId, start, limit, searchTerm);
    }

    @GET
    @Path("/employeeByDistance/{memberId}")
    @Timed(name = "getEmployeesByDistance")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get employee by distance by member")
    public EmployeeTransactionDistanceResult getEmployeesByDistance(@PathParam("memberId") String memberId, @QueryParam("start") int start,
                                                                    @QueryParam("limit") int limit, @QueryParam("publicKey") String publicKey,
                                                                    @QueryParam("transactionId") String transactionId) {
        return deliveryService.getEmployeesByDistance(memberId, start, limit, publicKey, transactionId);
    }

    @GET
    @Path("/inventory/{productId}")
    @Timed(name = "getInventoryByEmployee")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get product's inventory by employee")
    public SearchResult<InventoryByEmployeeResult> getProductInventoryByEmployee(@PathParam("productId") String productId, @QueryParam("start") int start, @QueryParam("limit") int limit) {
        return deliveryService.getProductInventoryByEmployee(productId, start, limit);
    }

    @GET
    @Path("/transactions/{employeeId}")
    @Timed(name = "getEmployeeDistanceByTransactions")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Calculate Employee distance by transaction")
    public List<MemberDistanceResult> getEmployeeDistanceByTransaction(@PathParam ("employeeId") String employeeId, @QueryParam ("location") String location) {
        return deliveryService.getEmployeeDistanceByTranactions(employeeId, location);
    }

    @GET
    @Path("/employee/all/inventorylogs")
    @Secured(requiredShop = true)
    @Timed(name = "getEmployeeInventoryLog")
    @ApiOperation(value = "Get employee inventory  log")
    public SearchResult<InventoryActionLog> getAllInventoryLog(@QueryParam("employeeId") String employeeId, @QueryParam("start") int start, @QueryParam("limit") int limit, @QueryParam("term") String searchTerm, @QueryParam("afterDate") long afterDate,
                                                               @QueryParam("beforeDate") long beforeDate) {
        return deliveryService.getAllInventoryLog(employeeId, start, limit, searchTerm,afterDate, beforeDate);
    }
}
