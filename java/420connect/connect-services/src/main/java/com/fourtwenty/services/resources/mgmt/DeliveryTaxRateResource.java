package com.fourtwenty.services.resources.mgmt;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.company.DeliveryTaxRate;
import com.fourtwenty.core.rest.dispensary.results.company.DeliveryTaxRateResult;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.mgmt.DeliveryTaxRateService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Api("Management - Delivery tax rate")
@Path("/api/v1/mgmt/deliveryTaxRate")
@Produces(MediaType.APPLICATION_JSON)
public class DeliveryTaxRateResource extends BaseResource {

    @Inject
    private DeliveryTaxRateService deliveryTaxRateService;

    @GET
    @Path("/")
    @Secured(requiredShop = true)
    @Timed(name = "getDeliveryTaxRatesByShop")
    @ApiOperation(value = "Get delivery tax rate list by shop")
    public List<DeliveryTaxRateResult> getDeliveryTaxRatesByShop(@QueryParam("shopId") String shopId) {
        return deliveryTaxRateService.getDeliveryTaxRatesByShop(shopId);
    }

    @GET
    @Path("/{taxRateId}")
    @Secured(requiredShop = true)
    @Timed(name = "getDeliveryTaxRateById")
    @ApiOperation(value = "Get delivery tax rate by id")
    public DeliveryTaxRateResult getDeliveryTaxRateById(@PathParam("taxRateId") String taxRateId) {
        return deliveryTaxRateService.getDeliveryTaxRateById(taxRateId);
    }

    @DELETE
    @Path("/{taxRateId}")
    @Secured(requiredShop = true)
    @Timed(name = "deleteDeliveryTaxRateById")
    @ApiOperation(value = "Delete delivery tax rate by id")
    public Response deleteDeliveryTaxRateById(@PathParam("taxRateId") String taxRateId) {
        deliveryTaxRateService.deleteDeliveryTaxRateById(taxRateId);
        return ok();
    }

    @POST
    @Path("/")
    @Secured(requiredShop = true)
    @Timed(name = "createDeliveryTaxRate")
    @ApiOperation(value = "Create delivery tax rate")
    public DeliveryTaxRate createDeliveryTaxRate(DeliveryTaxRate deliveryTaxRate) {
        return deliveryTaxRateService.createDeliveryTaxRate(deliveryTaxRate);
    }

    @PUT
    @Path("/{taxRateId}")
    @Secured(requiredShop = true)
    @Timed(name = "updateDeliveryTaxRate")
    @ApiOperation(value = "Update delivery tax rate")
    public DeliveryTaxRate updateDeliveryTaxRate(@PathParam("taxRateId") String taxRateId, DeliveryTaxRate deliveryTaxRate) {
        return deliveryTaxRateService.updateDeliveryTaxRate(taxRateId, deliveryTaxRate);
    }

}
