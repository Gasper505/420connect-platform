package com.fourtwenty.services.resources.mgmt;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.developer.DeveloperKey;
import com.fourtwenty.core.rest.developer.request.DeveloperKeyAddRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.dispensary.Audit;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.developer.DeveloperKeyService;
import com.google.inject.Inject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by mdo on 2/14/17.
 */

@Api("Management - Developer Keys")
@Path("/api/v1/mgmt/devkeys")
@Produces(MediaType.APPLICATION_JSON)
public class DeveloperKeyResource {
    @Inject
    DeveloperKeyService developerKeyService;


    @GET
    @Path("/")
    @Secured
    @Timed(name = "getDeveloperKeys")
    @ApiOperation(value = "Get Active Developer Keys")
    public SearchResult<DeveloperKey> getDeveloperKeys() {
        return developerKeyService.getDeveloperKeys();
    }

    @POST
    @Path("/")
    @Secured
    @Timed(name = "createNewDeveloperKey")
    @Audit(action = "Create New Developer Key")
    @ApiOperation(value = "Create New Developer Key")
    public DeveloperKey createNewDeveloperKey(@Valid DeveloperKeyAddRequest request) {
        return developerKeyService.createNewDeveloperKey(request);
    }

    @PUT
    @Path("/{developerKeyId}")
    @Secured
    @Timed(name = "updateDeveloperKey")
    @ApiOperation(value = "Update Developer Key")
    public DeveloperKey updateDeveloperKey(@PathParam("developerKeyId") String developerKeyId,
                                           @Valid DeveloperKeyAddRequest request) {
        return developerKeyService.updateDeveloperKey(developerKeyId, request);
    }

    @DELETE
    @Path("/{developerKeyId}")
    @Secured
    @Timed(name = "deletedDeveloperKey")
    @Audit(action = "Delete Developer Key")
    @ApiOperation(value = "Deleted Developer Key")
    public Response deletedDeveloperKey(@PathParam("developerKeyId") String developerKeyId) {
        developerKeyService.deleteDeveloperKey(developerKeyId);
        return Response.ok().build();
    }

}
