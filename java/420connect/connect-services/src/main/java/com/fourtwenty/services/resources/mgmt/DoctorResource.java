package com.fourtwenty.services.resources.mgmt;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.customer.Doctor;
import com.fourtwenty.core.rest.dispensary.requests.customer.DoctorAddRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.dispensary.Audit;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.mgmt.DoctorService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by Stephen Schmidt on 11/14/2015.
 */
@Api("Management - Doctors")
@Path("/api/v1/mgmt/doctors")
@Produces(MediaType.APPLICATION_JSON)
public class DoctorResource extends BaseResource {
    @Inject
    DoctorService doctorService;

    @GET
    @Path("/{doctorId}")
    @Timed(name = "getDoctor")
    @Secured
    @ApiOperation(value = "Get Doctor By Id")
    public Doctor getDoctor(@PathParam("doctorId") String doctorId) {
        return doctorService.getDoctorById(doctorId);
    }


    @GET
    @Path("/")
    @Timed(name = "searchDoctors")
    @Secured
    @ApiOperation(value = "Search Doctors")
    public SearchResult<Doctor> searchDoctors(@QueryParam("term") String term,
                                              @QueryParam("physicianId") String physicianId,
                                              @QueryParam("start") int start,
                                              @QueryParam("limit") int limit) {
        return doctorService.searchDoctorsByTerm(term, physicianId, start, limit);
    }


    @POST
    @Path("/")
    @Secured
    @Timed(name = "createDoctor")
    @Audit(action = "Create Doctor")
    @ApiOperation(value = "Create Doctor")
    public Doctor createDoctor(@Valid DoctorAddRequest request) {
        return doctorService.addDoctor(request);
    }

    @POST
    @Path("/{doctorId}")
    @Timed(name = "updateDoctor")
    @Secured
    @Audit(action = "Update Doctor")
    @ApiOperation(value = "Update Doctor")
    public Doctor updateDoctor(@PathParam("doctorId") String doctorId, @Valid Doctor doctor) {
        return doctorService.updateDoctor(doctorId, doctor);
    }

    @DELETE
    @Path("/{doctorId}")
    @Secured
    @Timed(name = "deleteDoctor")
    @Audit(action = "Delete Doctor")
    @ApiOperation(value = "Delete Doctor")
    public Response deleteDoctor(@PathParam("doctorId") String doctorId) {
        doctorService.deleteDoctor(doctorId);
        return ok();
    }

}
