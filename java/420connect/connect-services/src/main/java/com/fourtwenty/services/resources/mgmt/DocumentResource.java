package com.fourtwenty.services.resources.mgmt;

import com.fourtwenty.core.security.dispensary.BaseResource;
import io.swagger.annotations.Api;

import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Created by mdo on 10/26/15.
 */

@Api("Management - Documents")
@Path("/api/v1/mgmt/documents")
@Produces(MediaType.APPLICATION_JSON)
public class DocumentResource extends BaseResource {
}
