package com.fourtwenty.services.resources.mgmt;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.rest.dispensary.results.company.EmployeeDistanceResult;
import com.fourtwenty.core.security.dispensary.Audit;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.services.mgmt.DeliveryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Api("Management - Driver Suggestion")
@Path("/api/v1/mgmt/driver")
@Produces(MediaType.APPLICATION_JSON)
public class DriverSuggestionResource extends BaseResource {

    @Inject
    private DeliveryService deliveryService;

    @GET
    @Path("/{transactionId}")
    @Timed(name = "getClockedInEmployees")
    @Audit(action = "Get Top 10 Employees")
    @ApiOperation(value = "Get top 10 clocked in employees")
    public List<EmployeeDistanceResult> getClockedInEmployees(@PathParam("transactionId") String transactionId, @QueryParam("sort") Employee.SortOption sort, @QueryParam("limit") int limit) {
        return deliveryService.getClockedInEmployees(transactionId, sort, limit);
    }


}
