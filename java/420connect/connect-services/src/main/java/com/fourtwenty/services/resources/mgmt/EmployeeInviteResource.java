package com.fourtwenty.services.resources.mgmt;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.company.InviteEmployee;
import com.fourtwenty.core.rest.dispensary.requests.employees.EmployeeInviteRequest;
import com.fourtwenty.core.rest.dispensary.requests.employees.EmployeeInviteTokenRequest;
import com.fourtwenty.core.rest.dispensary.requests.employees.RegisterInviteeRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.dispensary.Audit;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.mgmt.InviteEmployeeService;
import com.google.inject.Inject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created on 9/11/17 10:50 PM
 * Raja Dushyant Vashishtha (Sr. Software Engineer)
 * rajad@decipherzone.com
 * Decipher Zone Softwares
 * www.decipherzone.com
 */
@Api("Management - Employees Invitation")
@Path("/api/v1/mgmt/employee-invite")
@Produces(MediaType.APPLICATION_JSON)
public class EmployeeInviteResource extends BaseResource {

    private InviteEmployeeService inviteEmployeeService;

    @Inject
    public EmployeeInviteResource(InviteEmployeeService inviteEmployeeService) {
        this.inviteEmployeeService = inviteEmployeeService;
    }

    @POST
    @Path("/")
    @Secured
    @Timed(name = "inviteEmployee")
    @Audit(action = "Invite Employee")
    @ApiOperation(value = "Invite Employee")
    public InviteEmployee inviteEmployee(@Valid EmployeeInviteRequest request) {
        return inviteEmployeeService.inviteEmployee(request);
    }

    @PUT
    @Path("/")
    @Timed(name = "registerInvitee")
    @Audit(action = "Register Invitee")
    @ApiOperation(value = "Register Invitee")
    public InviteEmployee registerInvitee(@Valid RegisterInviteeRequest registerInviteeRequest) {
        return inviteEmployeeService.registerInvitee(registerInviteeRequest);
    }

    @POST
    @Path("/employee-info")
    @Timed(name = "getInviteeInfo")
    @Audit(action = "Get invitee information")
    @ApiOperation(value = "Get invitee information")
    public InviteEmployee getInviteeInfo(@Valid EmployeeInviteTokenRequest employeeInviteTokenRequest) {
        return inviteEmployeeService.getInviteeInfo(employeeInviteTokenRequest);
    }

    @GET
    @Path("/")
    @Secured
    @Timed(name = "getInviteEmployees")
    @ApiOperation(value = "List of invited employee")
    public SearchResult<InviteEmployee> getInviteEmployeeList(@QueryParam("start") int start,
                                                              @QueryParam("limit") int limit) {
        return inviteEmployeeService.getInviteEmployeeList(start, limit);
    }

    @GET
    @Path("/{employeeId}")
    @Secured
    @Timed(name = "getInviteEmployeeById")
    @ApiOperation(value = "Get invite employee by id")
    public InviteEmployee getInviteEmployeeById(@PathParam("employeeId") String employeeId) {
        return inviteEmployeeService.getInviteEmployeeById(employeeId);
    }

    @GET
    @Path("/{inviteEmployeeId}/resendInviteEmployee")
    @Secured
    @Timed(name = "resendInviteEmployee")
    @ApiOperation(value = "Resend Invite Employee")
    public Response resendInviteEmployee(@PathParam("inviteEmployeeId") String inviteEmployeeId) {
        inviteEmployeeService.resendInvite(inviteEmployeeId);
        return ok();
    }

}
