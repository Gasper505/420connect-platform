package com.fourtwenty.services.resources.mgmt;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.rest.dispensary.requests.employees.EmployeeAddRequest;
import com.fourtwenty.core.rest.dispensary.requests.employees.EmployeeCredentialUpdateRequest;
import com.fourtwenty.core.rest.dispensary.requests.employees.EmployeePinUpdateRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.company.EmployeeResult;
import com.fourtwenty.core.security.dispensary.Audit;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.mgmt.EmployeeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by mdo on 10/2/15.
 * <p>
 * GET     /employees          //Array of all of the employees in whatever order
 * POST    /employees          //Creates a new employee with json data
 * PATCH   /employees/:id      //Updates the employee of :id with json
 * DELETE  /employees/:id      //Deletes the employee of :id
 */
@Api("Management - Employees")
@Path("/api/v1/mgmt/employees")
@Produces(MediaType.APPLICATION_JSON)
public class EmployeeResource extends BaseResource {
    @Inject
    EmployeeService employeeService;

    @GET
    @Path("/{employeeId}")
    @Secured
    @Timed(name = "getEmployees")
    @ApiOperation(value = "Get Employees By Id")
    public Employee getEmployees(@PathParam("employeeId") String employeeId) {
        return employeeService.getEmployeeById(employeeId);
    }


    @GET
    @Path("/")
    @Secured
    @Timed(name = "getEmployees")
    @ApiOperation(value = "Search Employees")
    public SearchResult<EmployeeResult> getEmployees(@QueryParam("term") String term,
                                                     @QueryParam("start") int start,
                                                     @QueryParam("limit") int limit,
                                                     @QueryParam("accessConfig") String accessConfig,
                                                     @QueryParam("shopId") String shopIds,
                                                     @QueryParam("roleId") String roleId) {
        return employeeService.searchEmployees(term, start, limit, accessConfig, shopIds, roleId);
    }

    @POST
    @Path("/")
    @Secured
    @Timed(name = "createEmployee")
    @Audit(action = "Create Employee")
    @ApiOperation(value = "Create Employees")
    public Employee createEmployee(@Valid EmployeeAddRequest request) {
        return employeeService.addEmployee(request);
    }

    @POST
    @Path("/{employeeId}")
    @Secured
    @Timed(name = "updateEmployee")
    @Audit(action = "Update Employee")
    @ApiOperation(value = "Update Employee")
    public Employee updateEmployee(@PathParam("employeeId") String employeeId, @Valid Employee request) {
        return employeeService.updateEmployee(employeeId, request);
    }

    @POST
    @Path("/{employeeId}/credentials")
    @Secured
    @Timed(name = "updateEmployeeCredentials")
    @Audit(action = "Update Employee Credentials")
    @ApiOperation(value = "Update Employee Credentials")
    public Response updateEmployeeCredentials(@PathParam("employeeId") String employeeId, @Valid EmployeeCredentialUpdateRequest request) {
        employeeService.updateCredentials(employeeId, request);
        return ok();
    }

    @POST
    @Path("/{employeeId}/pin")
    @Secured
    @Timed(name = "updateEmployeePin")
    @Audit(action = "Update Employee Pin")
    @ApiOperation(value = "Update Employee Pin")
    public Response updateEmployeePin(@PathParam("employeeId") String employeeId, @Valid EmployeePinUpdateRequest request) {
        employeeService.updatePin(employeeId, request);
        return ok();
    }

    @DELETE
    @Path("/{employeeId}")
    @Secured
    @Timed(name = "deleteEmployee")
    @Audit(action = "Delete Employee")
    @ApiOperation(value = "Delete Employee")
    public Response deleteEmployee(@PathParam("employeeId") String employeeId) {
        employeeService.deleteEmployee(employeeId);
        return ok();
    }

    @GET
    @Path("/employeeList")
    @Secured
    @Timed(name = "getEmployeeList")
    @Audit(action = "Get employee list")
    @ApiOperation(value = "Get employee list")
    public SearchResult<EmployeeResult> getEmployeeList(@QueryParam("term") String term, @QueryParam("start") int start, @QueryParam("limit") int limit) {
        return employeeService.getEmployeeList(term, start, limit);
    }

}
