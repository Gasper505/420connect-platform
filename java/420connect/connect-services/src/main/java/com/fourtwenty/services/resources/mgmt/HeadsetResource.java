package com.fourtwenty.services.resources.mgmt;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.thirdparty.HeadsetLocation;
import com.fourtwenty.core.security.dispensary.Audit;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.thirdparty.HeadsetService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/**
 * Created by Gaurav Saini on 3/7/17.
 */
@Api("Management - Headset account")
@Path("/api/v1/mgmt/headset")
@Produces(MediaType.APPLICATION_JSON)
public class HeadsetResource extends BaseResource {

    @Inject
    HeadsetService headsetService;


    @GET
    @Path("/current")
    @Timed(name = "getCurrentHeadsetAccount")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get Current Headset Account.")
    public HeadsetLocation getCurrentHeadsetAccount() {
        return headsetService.getHeadsetAccount();
    }

    @POST
    @Path("/accept")
    @Timed(name = "acceptHeadsetTerm")
    @Audit(action = "Accept Headset Account")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Accept Headset Account.")
    public HeadsetLocation acceptHeadsetTerm() {
        return headsetService.acceptHeadsetTerms();
    }


    @POST
    @Path("/{headsetAccountId}")
    @Timed(name = "updateHeadsetAccountSync")
    @Audit(action = "Update Headset account sync and accept terms.")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Update Headset account sync and accept terms.")
    public HeadsetLocation updateHeadsetAccountSync(@PathParam("headsetAccountId") String headsetAccountId, @Valid HeadsetLocation headsetLocation) {
        return headsetService.updateHeadsetLocation(headsetAccountId, headsetLocation);
    }
}
