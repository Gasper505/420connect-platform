package com.fourtwenty.services.resources.mgmt;

import com.blaze.clients.hypur.models.CheckInTestUsersResponse;
import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.thirdparty.ThirdPartyShopAccount;
import com.fourtwenty.core.security.dispensary.Audit;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.thirdparty.HypurService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Api("Management - Hypur")
@Path("/api/v1/mgmt/hypur")
@Produces(MediaType.APPLICATION_JSON)
public class HypurPaymentResource extends BaseResource {

    @Inject
    HypurService hypurService;

    @GET
    @Path("/current")
    @Timed(name = "getCurrentHypurAccount")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get Current Hypur Account")
    public ThirdPartyShopAccount getCurrentHypurAccount() {
        return hypurService.getCurrentHypurAccount();
    }

    @POST
    @Path("/current")
    @Timed(name = "updateHypurAccount")
    @Audit(action = "Update Hypur Account")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Update Hypur Account")
    public ThirdPartyShopAccount update(@Valid ThirdPartyShopAccount thirdPartyShopAccount) {
        return hypurService.updateHypurAccount(thirdPartyShopAccount);
    }

    /*@GET
    @Path("/profiles")
    @Timed(name = "getHypurCheckinUserProfiles")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get Hypur Check in user profiles")
    public CheckedInUserListResponse getHypurCheckinUserProfiles(@QueryParam("thumnailSize") int thumnailSize) {
        return hypurService.getHypurCheckinUserProfiles(thumnailSize);
    }*/

    @GET
    @Path("/testUsers")
    @Timed(name = "generateHypurTestUsers")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get Test Users")
    public CheckInTestUsersResponse generateHypurTestUsers() {
        return hypurService.generateHypurTestUsers();
    }


}