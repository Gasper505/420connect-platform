package com.fourtwenty.services.resources.mgmt;

import com.fourtwenty.core.importer.model.ProductBatchResult;
import com.fourtwenty.core.security.dispensary.Audit;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.mgmt.ImportService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.glassfish.jersey.media.multipart.FormDataParam;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.InputStream;
import java.util.List;

/**
 * Created by Stephen Schmidt on 1/26/2016.
 */

@Api("Management - Import")
@Path("/api/v1/mgmt/import")
@Produces(MediaType.APPLICATION_JSON)
public class ImportResource extends BaseResource {
    @Inject
    ImportService importService;

    @POST
    @Path("/")
    @Secured(requiredShop = true)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Audit(action = "Import Data")
    @ApiOperation(value = "Import Data")
    public void uploadFiles(@FormDataParam("vendorFile") InputStream vendorInput,
                            @FormDataParam("doctorFile") InputStream doctorInput,
                            @FormDataParam("productFile") InputStream productInput,
                            @FormDataParam("customerFile") InputStream customerInput,
                            @FormDataParam("assetFolder") InputStream assetFolder,
                            @FormDataParam("promotionFile") InputStream promotionInput) {
        importService.importFiles(vendorInput, doctorInput, productInput, customerInput, assetFolder, promotionInput);
    }

    @POST
    @Path("/importInventory")
    @Secured(requiredShop = true)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Audit(action = "Import Product Inventory")
    @ApiOperation(value = "Import Product Inventory")
    public Response uploadFiles(@FormDataParam("productFile") InputStream productsList) {
        return importService.parseBatchImport(productsList);
    }

    @POST
    @Path("/saveImportedInventory")
    @Secured(requiredShop = true)
    @Audit(action = "Save imported product inventory")
    @ApiOperation(value = "Save imported product inventory")
    public Response importProductBatch(@Valid List<ProductBatchResult> productBatchResult) {
        return importService.importProductBatch(productBatchResult);
    }


    @POST
    @Path("/meadow")
    @Secured(requiredShop = true)
    @Audit(action = "Import Meadow Products")
    @ApiOperation(value = "Import Meadow Products")
    public Response importProductBatch(@FormDataParam("meadowFile") InputStream meadowFileInput) {
        return importService.importMeadowProducts(meadowFileInput);
    }

    @POST
    @Path("/assignUniqueId")
    @Secured(requiredShop = true)
    @Audit(action = "Assign New Unique Ids")
    @ApiOperation(value = "Assign New Unique Ids")
    public Response assignUniqueIds(@FormDataParam("batchFile") InputStream meadowFileInput) {
        return importService.assignUniqueIds(meadowFileInput);
    }
}
