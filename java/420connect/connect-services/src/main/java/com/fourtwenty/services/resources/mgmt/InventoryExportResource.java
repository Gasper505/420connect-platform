package com.fourtwenty.services.resources.mgmt;

import com.fourtwenty.core.security.dispensary.Audit;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.services.mgmt.ExportService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Api("Management - Export")
@Path("/api/v1/mgmt/export")
public class InventoryExportResource extends BaseResource {

    @Inject
    private ExportService exportService;

    @GET
    @Path("/products/{shopId}")
    @Audit(action = "Export products data for shop")
    @ApiOperation(value = "Export products data for shop")
    public Response exportProductsByShop(@PathParam("shopId") String shopId, @QueryParam("assetToken") String assetToken) {

        StringBuilder fileWriter = exportService.exportProductsByShop(shopId, assetToken);
        Response.ResponseBuilder response = Response.ok(fileWriter.toString().getBytes());
        response.type(MediaType.APPLICATION_OCTET_STREAM);
        response.header("Content-Disposition", "attachment; filename=\"product-batch-import-form.csv\"");
        return response.build();
    }

}
