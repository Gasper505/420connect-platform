package com.fourtwenty.services.resources.mgmt;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.company.FilterType;
import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.event.inventory.GetComplianceBatchesResult;
import com.fourtwenty.core.rest.dispensary.requests.inventory.*;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.ListResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.BatchByCategoryResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.BatchQuantityResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.InventoryTransferHistoryResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.ReconciliationHistoryResult;
import com.fourtwenty.core.security.dispensary.Audit;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.inventory.ProductWeightToleranceService;
import com.fourtwenty.core.services.mgmt.InventoryService;
import com.fourtwenty.core.services.mgmt.InventoryTransferHistoryService;
import com.fourtwenty.core.services.mgmt.ProductCategoryService;
import com.google.common.io.ByteStreams;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

/**
 * Created by mdo on 10/11/15.
 */
@Api("Management - Inventory")
@Path("/api/v1/mgmt/inventory")
@Produces(MediaType.APPLICATION_JSON)
public class InventoryResource extends BaseResource {
    @Inject
    ProductCategoryService productCategoryService;
    @Inject
    InventoryService inventoryService;
    @Inject
    private InventoryTransferHistoryService transferHistoryService;
    @Inject
    private ProductWeightToleranceService productWeightToleranceService;

    @GET
    @Path("/")
    @Secured(requiredShop = true)
    @Timed(name = "getInventories")
    @ApiOperation(value = "Get Inventories")
    public SearchResult<Inventory> getInventories(@QueryParam("shopId") String shopId) {
        return inventoryService.getShopInventories(shopId);
    }

    @POST
    @Path("/")
    @Secured(requiredShop = true)
    @Timed(name = "createInventory")
    @Audit(action = "Create Inventory")
    @ApiOperation(value = "Create Inventory")
    public Inventory createInventory(@Valid InventoryAddRequest request) {
        return inventoryService.addInventory(request);
    }

    @POST
    @Path("/{inventoryId}")
    @Secured(requiredShop = true)
    @Timed(name = "updateInventory")
    @Audit(action = "Update Inventory")
    @ApiOperation(value = "Update Inventory")
    public Inventory updateInventory(@PathParam("inventoryId") String inventoryId, @Valid InventoryUpdateRequest request) {
        return inventoryService.updateInventory(inventoryId, request);
    }

    @DELETE
    @Path("/{inventoryId}")
    @Secured(requiredShop = true)
    @Timed(name = "deleteInventory")
    @Audit(action = "Delete Inventory")
    @ApiOperation(value = "Delete Inventory")
    public Response deleteInventory(@PathParam("inventoryId") String inventoryId) {
        inventoryService.deleteInventory(inventoryId);
        return ok();
    }

    @POST
    @Path("/{inventoryId}/delete")
    @Secured(requiredShop = true)
    @Timed(name = "updateInventory")
    @Audit(action = "Update Inventory")
    @ApiOperation(value = "Update Inventory")
    public void deleteAndTransferInventory(@PathParam("inventoryId") String inventoryId, @Valid InventoryDeleteRequest request) {
        request.setDeleteInventoryId(inventoryId);
        inventoryService.deleteInventory(request);
    }


    @GET
    @Path("/categories")
    @Secured(requiredShop = true)
    @Timed(name = "getProductCategories")
    @ApiOperation(value = "Get Product Categories")
    public ListResult<ProductCategory> getProductCategories(@QueryParam("shopId") String shopId) {
        return productCategoryService.getProductCategories(shopId);
    }

    @POST
    @Path("/categories")
    @Secured(requiredShop = true)
    @Timed(name = "createProductCategory")
    @Audit(action = "Create Product Category")
    @ApiOperation(value = "Create Product Category")
    public ProductCategory createProductCategory(@Valid ProductCategoryAddRequest request) {
        return productCategoryService.createProductCategory(request);
    }

    @POST
    @Path("/categories/priorities")
    @Secured(requiredShop = true)
    @Timed(name = "updateProductCategoryOrder")
    @Audit(action = "Update Product Category Order")
    @ApiOperation(value = "Update Product Category Order")
    public ListResult<ProductCategory> updateProductCategoryOrder(@Valid ProductCategoryUpdateOrderRequest request) {
        return productCategoryService.updateCategoryOrders(request);
    }

    @POST
    @Path("/categories/{productCategoryId}")
    @Secured(requiredShop = true)
    @Timed(name = "updateProductCategory")
    @Audit(action = "Update Product Category")
    @ApiOperation(value = "Update Product Category")
    public ProductCategory updateProductCategory(@PathParam("productCategoryId") String productCategoryId,
                                                 @Valid ProductCategory category) {
        return productCategoryService.updateProductCategory(productCategoryId, category);
    }

    @DELETE
    @Path("/categories/{productCategoryId}")
    @Secured(requiredShop = true)
    @Timed(name = "deleteProductCategory")
    @Audit(action = "Delete Product Category")
    @ApiOperation(value = "Delete Product Category")
    public Response deleteProductCategory(@PathParam("productCategoryId") String productCategoryId) {
        productCategoryService.deleteProductCategory(productCategoryId);
        return ok();
    }

    // Misc
    @GET
    @Path("/batches")
    @Secured(requiredShop = true)
    @Timed(name = "getBatchesWithDates")
    @ApiOperation(value = "Get Batches With Dates")
    public DateSearchResult<ProductBatch> getBatchesWithDates(@QueryParam("afterDate") long afterDate, @QueryParam("beforeDate") long beforeDate) {
        return inventoryService.getBatchesWithDates(afterDate, beforeDate);
    }

    @GET
    @Path("/tolerances")
    @Secured(requiredShop = true)
    @Timed(name = "getWeightTolerances")
    @ApiOperation(value = "Get Weight Tolerances")
    public SearchResult<ProductWeightTolerance> getWeightTolerances() {
        return productWeightToleranceService.getWeightTolerances();
    }

    // Transfers
    @POST
    @Path("/transfer")
    @Secured(required = true, requiredShop = true)
    @Timed(name = "transferInventory")
    @Audit(action = "Transfer Inventory")
    @ApiOperation(value = "Transfer Inventory")
    public Response transferInventory(@Valid BulkInventoryTransferRequest request) {
        inventoryService.bulkTransferInventory(request);
        return ok();
    }

    // Transfers
    @POST
    @Path("/loss")
    @Secured(required = true, requiredShop = true)
    @Timed(name = "reportLoss")
    @Audit(action = "Report Loss")
    @ApiOperation(value = "Report Loss")
    public Response reportLoss(@Valid ReportLossRequest request) {
        inventoryService.reportLoss(request);
        return ok();
    }

    // Transfers
    @POST
    @Path("/inventoryReconciliation")
    @Secured(required = true, requiredShop = true)
    @Timed(name = "inventoryReconciliation")
    @Audit(action = "Inventory Reconciliation")
    @ApiOperation(value = "Inventory Reconciliation")
    public Response inventoryReconciliation(@Valid ReportLossListRequest request) {
        inventoryService.inventoryReconciliation(request);
        return ok();
    }

    @POST
    @Path("/inventoryReconciliationByBatch")
    @Secured(required = true, requiredShop = true)
    @Timed(name = "inventoryReconciliationByBatch")
    @Audit(action = "Inventory Reconciliation by batch")
    @ApiOperation(value = "Inventory Reconciliation by batch")
    public Response inventoryReconciliationByBatch(@Valid ReportLossListRequest request) {
        inventoryService.inventoryReconciliationByBatch(request);
        return ok();
    }

    @POST
    @Path("/snapshot")
    @Secured(required = true, requiredShop = true)
    @Timed(name = "takeSnapshot")
    @Audit(action = "Take Inventory Snapshot")
    @ApiOperation(value = "Take Inventory Snapshot")
    public Response takeSnapshot() {
        inventoryService.takeSnapshot();
        return ok();
    }

    // Transfers

    /**
     * Transfer All quantity(Prepackage and Regular) from Inventory to Safe Inventory
     *
     * @param request
     * @return
     */
    @POST
    @Path("/resetToSafe")
    @Secured(required = true, requiredShop = true)
    @Timed(name = "resetToSafe")
    @Audit(action = "Transfer All Product Inventory to Safe")
    @ApiOperation(value = "Transfer All Product Inventory to Safe")
    public Response resetToSafe(@Valid ResetInventoryRequest request) {
        inventoryService.resetInventoryToSafe(request);
        return ok();
    }

    @GET
    @Path("/batch")
    @Secured(required = true, requiredShop = true)
    @Timed(name = "getBatchByLabel")
    @ApiOperation(value = "Get product batch from label")
    public SearchResult<BatchQuantityResult> getBatchByLabel(@QueryParam("productId") String productId, @QueryParam("label") String label, @QueryParam("inventoryId") String inventoryId,
                                                             @QueryParam("start") int start, @QueryParam("limit") int limit) {
        return inventoryService.getBatchByLabel(inventoryId, label, productId, start, limit);
    }

    @GET
    @Path("/reconciliationHistory")
    @Secured(required = true, requiredShop = true)
    @Timed(name = "getReconciliationHistory")
    @ApiOperation(value = "Get all reconciliation history")
    public SearchResult<ReconciliationHistoryResult> getReconciliationHistory(@QueryParam("start") int start, @QueryParam("limit") int limit) {
        return inventoryService.getAllReconciliationHistory(start, limit);
    }

    @GET
    @Path("/reconciliationHistory/{historyId}")
    @Secured(required = true, requiredShop = true)
    @Timed(name = "getReconciliationHistoryGetById")
    @ApiOperation(value = "Get reconciliation history by history id")
    public ReconciliationHistoryResult getReconciliationHistoryGetById(@PathParam("historyId") String historyId) {
        return inventoryService.getReconciliationHistoryById(historyId);
    }

    //Inventory transfer
    @GET
    @Path("/{historyId}/inventoryHistory")
    @Secured(requiredShop = true)
    @Timed(name = "getInventoryTransferHistoryById")
    @ApiOperation(value = "Get inventory transfer history by id")
    public InventoryTransferHistoryResult getInventoryTransferHistoryById(@PathParam("historyId") String historyId) {
        return transferHistoryService.getInventoryTransferHistoryById(historyId);
    }

    @GET
    @Path("/inventoryHistory")
    @Secured(requiredShop = true)
    @Timed(name = "getInventoryHistories")
    @ApiOperation(value = "Get inventory transfer history")
    public SearchResult<InventoryTransferHistoryResult> getInventoryHistories(@QueryParam("start") int start, @QueryParam("limit") int limit, @QueryParam("status") String status, @QueryParam("startDate") String startDate, @QueryParam("endDate") String endDate) {
        return transferHistoryService.getInventoryHistories(start, limit, status, startDate, endDate);
    }

    @PUT
    @Path("/{historyId}/inventoryHistory")
    @Secured(requiredShop = true)
    @Timed(name = "updateInventoryHistoryResult")
    @ApiOperation(value = "Update inventory history")
    public InventoryTransferHistory updateInventoryHistoryResult(@PathParam("historyId") String historyId, @Valid InventoryTransferHistory transferHistory) {
        return transferHistoryService.updateInventoryHistoryResult(historyId, transferHistory);
    }

    @POST
    @Path("/inventoryTransfer")
    @Secured(required = true, requiredShop = true)
    @Timed(name = "inventoryTransferHistory")
    @ApiOperation(value = "Add inventory transfer history")
    public InventoryTransferHistory inventoryTransferHistory(@Valid InventoryTransferHistory request) {
        return transferHistoryService.bulkTransferInventory(request);
    }

    @POST
    @Path("/{historyId}/inventoryHistory")
    @Secured(required = true, requiredShop = true)
    @Timed(name = "updateInventoryTransferStatus")
    @ApiOperation(value = "Update inventory transfer history's status")
    public InventoryTransferHistory updateInventoryTransferStatus(@PathParam("historyId") String historyId, @Valid InventoryTransferStatusRequest status) {
        return transferHistoryService.updateInventoryTransferStatus(historyId, status);
    }


    @POST
    @Path("/inventoryTransferByBatches")
    @Secured(required = true, requiredShop = true)
    @Timed(name = "inventoryTransferHistoryByBatches")
    @ApiOperation(value = "Add inventory transfer history only for batches")
    public InventoryTransferHistory inventoryTransferHistoryByBatches(@Valid InventoryTransferHistory request) {
        return transferHistoryService.bulkTransferInventoryByBatches(request);
    }


    @GET
    @Path("/reconciliation/reconciliationNo/{reconciliationNo}/")
    @Secured(requiredShop = true)
    @Timed(name = "getReconciliationByNo")
    @ApiOperation(value = "Get Reconciliation by number")
    public ReconciliationHistoryResult getReconciliationByNo(@PathParam("reconciliationNo") String reconciliationNo) {
        return inventoryService.getReconciliationByNo(reconciliationNo);
    }

    @GET
    @Path("transferNo/{transferNo}/inventoryHistory")
    @Secured(requiredShop = true)
    @Timed(name = "getInventoryTransferHistoryByNo")
    @ApiOperation(value = "Get inventory transfer history by transfer number")
    public InventoryTransferHistoryResult getInventoryTransferHistoryByNo(@PathParam("transferNo") String transferNo) {
        return transferHistoryService.getInventoryTransferHistoryByNo(transferNo);
    }

    @GET
    @Path("queuedTransaction/status")
    @Secured(requiredShop = true)
    @Timed(name = "getPendingStatusForQueuedTransaction")
    @ApiOperation("Get pending status for queued transaction")
    public long getPendingStatusForQueuedTransaction() {
        return inventoryService.getPendingStatusForQueuedTransaction();
    }


    @PUT
    @Path("/batch/{batchId}/unArchive")
    @Secured(requiredShop = true)
    @Timed(name = "unarchiveBatch")
    @ApiOperation(value = "Unarchive product batch by id")
    public void unarchiveProductBatch(@PathParam("batchId") String batchId) {
        inventoryService.unArchiveBatch(batchId);
    }

    @GET
    @Path("/batch/list")
    @Secured(requiredShop = true)
    @Timed(name = "getAllBatchesByCategory")
    @ApiOperation(value = "Get all by batches by category and inventory")
    public List<BatchByCategoryResult> getAllBatchesByCategory(@QueryParam("categoryId") String categoryId, @QueryParam("inventoryId") String inventoryId, @QueryParam("status") FilterType status) {
        return inventoryService.getProductBatchesByCategory(categoryId, inventoryId, status);
    }


    @GET
    @Path("/compliance/batch/list")
    @Secured(requiredShop = true)
    @Timed(name = "getAllComplianceBatches")
    @ApiOperation(value = "Get all compliance batches")
    public GetComplianceBatchesResult getAllComplianceBatches(
            @QueryParam("activeBatches") Boolean activeBatches,
            @QueryParam("onHoldBatches") Boolean onHoldBatches,
            @QueryParam("inactiveBatches") Boolean inactiveBatches) {
        return inventoryService.getAllComplianceBatches(
                activeBatches != null ? activeBatches : true,
                onHoldBatches != null ? onHoldBatches : true,
                inactiveBatches != null ? inactiveBatches : true);
    }

    @GET
    @Path("/lastReconciliation")
    @Secured(requiredShop = true)
    @Timed(name = "getLastReconciliation")
    @ApiOperation(value = "Get last reconciliation time of shop")
    public Long getLastReconciliation(@QueryParam("shopId") String shopId) {
        return inventoryService.getLastReconciliation(shopId);
    }

    @POST
    @Path("/derivedProductBatch")
    @Secured(requiredShop = true)
    @Timed(name = "createDerivedProductBatch")
    @ApiOperation(value = "Create derived product batch")
    public ProductBatch createDerivedProductBatch(@Valid DerivedBatchAddRequest request) {
        return inventoryService.createDerivedProductBatch(request);
    }

    @GET
    @Path("/shippingManifest/{transferId}/generatePDF")
    @Secured(requiredShop = true)
    @Timed(name = "createPdfForShippingManifest")
    @ApiOperation(value = "Create PDF For Transfer Manifest")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response createPdfForShippingManifest(@PathParam("transferId") String transferId) {
        InputStream inputStream = transferHistoryService.createPdfForShippingManifest(transferId);
        if (inputStream != null) {
            StreamingOutput streamOutput = new StreamingOutput() {
                @Override
                public void write(OutputStream os) throws IOException,
                        WebApplicationException {
                    ByteStreams.copy(inputStream, os);
                }
            };

            return Response.ok(streamOutput).type("application/pdf").build();
        }
        return Response.ok().build();
    }

    @GET
    @Path("/shippingManifest/transfer/generateZip")
    @Secured(requiredShop = true)
    @Timed(name = "createPdfForShippingManifest")
    @ApiOperation(value = "Create Bulk PDF For Transfer Manifest")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response createManifestForBulk(@QueryParam("transferIds") String transferId) {
        InputStream inputStream = transferHistoryService.createManifestForBulk(transferId);
        if (inputStream != null) {
            StreamingOutput streamOutput = new StreamingOutput() {
                @Override
                public void write(OutputStream os) throws IOException,
                        WebApplicationException {
                    ByteStreams.copy(inputStream, os);
                }
            };
            Response.ResponseBuilder responseBuilder = Response.ok(streamOutput).type(MediaType.APPLICATION_OCTET_STREAM);
            responseBuilder.header("Content-Disposition", "attachment; filename=\"Bulk_Transfer_manifest.zip\"");
            return responseBuilder.build();
        }
        return Response.ok().build();
    }


    @GET
    @Path("/inventoryHistory/employee")
    @Secured(requiredShop = true)
    @Timed(name = "getEmployeeInventoryHistories")
    @ApiOperation(value = "Get employee inventory transfer history")
    public SearchResult<InventoryTransferHistoryResult> getAllInventoryHistories(@QueryParam("start") int start, @QueryParam("limit") int limit, @QueryParam("status") String status, @QueryParam("startDate") String startDate, @QueryParam("endDate") String endDate, @QueryParam("term") String searchTerm) {
        return transferHistoryService.getAllInventoryHistories(start, limit, status, startDate, endDate, searchTerm);
    }

    @POST
    @Path("{productId}/resetToSafe")
    @Secured(requiredShop = true)
    @Timed(name = "resetToSafe")
    @Audit(action = "Reset product inventory to safe")
    @ApiOperation(value = "Reset product inventory to safe")
    public Response resetToSafe(@PathParam("productId")String productId) {
        transferHistoryService.resetInventoryToSafe(productId);
        return ok();
    }
}
