package com.fourtwenty.services.resources.mgmt;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.product.InventoryTransferKit;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.mgmt.InventoryTransferKitService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Api("Management - Inventory Transfer Kit")
@Path("/api/v1/mgmt/inventoryTransferKit")
@Produces(MediaType.APPLICATION_JSON)
public class InventoryTransferKitResource extends BaseResource {

    @Inject
    private InventoryTransferKitService inventoryTransferKitService;

    @GET
    @Path("/")
    @Secured(requiredShop = true)
    @Timed(name = "getInventoryTransferKits")
    @ApiOperation(value = "Get list of inventory transfer kits")
    public SearchResult<InventoryTransferKit> getInventoryTransferKits(@QueryParam("start") int start, @QueryParam("limit") int limit) {
        return inventoryTransferKitService.getInventoryTransferKits(start, limit);
    }

    @GET
    @Path("/{id}")
    @Secured(requiredShop = true)
    @Timed(name = "getInventoryTransferKitById")
    @ApiOperation(value = "Get inventory transfer kit by id")
    public InventoryTransferKit getInventoryTransferKitById(@PathParam("id") String id) {
        return inventoryTransferKitService.getInventoryTransferKitById(id);
    }

    @POST
    @Path("/")
    @Secured(requiredShop = true)
    @Timed(name = "createInventoryTransferKit")
    @ApiOperation(value = "Create inventory transfer kit")
    public InventoryTransferKit createInventoryTransferKit(@Valid InventoryTransferKit kit) {
        return inventoryTransferKitService.createInventoryTransferKit(kit);
    }

    @PUT
    @Path("/{id}")
    @Secured(requiredShop = true)
    @Timed(name = "updateInventoryTransferKit")
    @ApiOperation(value = "Update inventory transfer kit")
    public InventoryTransferKit updateInventoryTransferKit(@PathParam("id") String id,
                                                           @Valid InventoryTransferKit kit) {
        return inventoryTransferKitService.updateInventoryTransferKit(id, kit);
    }

    @DELETE
    @Path("/{id}")
    @Secured(requiredShop = true)
    @Timed(name = "deleteInventoryTransferKit")
    @ApiOperation(value = "deleteInventoryTransferKit")
    public Response deleteInventoryTransferKit(@PathParam("id") String id) {
        inventoryTransferKitService.deleteInventoryTransferKit(id);
        return Response.ok().build();
    }

}
