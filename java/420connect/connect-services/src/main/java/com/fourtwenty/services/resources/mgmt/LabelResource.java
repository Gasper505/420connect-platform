package com.fourtwenty.services.resources.mgmt;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.rest.dispensary.requests.inventory.LabelsQueryRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.LabelItem;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.common.CommonLabelService;
import com.fourtwenty.core.services.labels.LabelService;
import com.google.common.io.ByteStreams;
import io.dropwizard.jersey.caching.CacheControl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by mdo on 3/14/17.
 */

@Api("Management - Labels")
@Path("/api/v1/mgmt/labels")
public class LabelResource extends BaseResource {
    @Inject
    LabelService labelService;


    @GET
    @Path("/barcodes/{barcodeId}")
    @Timed(name = "createLabelWithBarcodeId")
    @CacheControl(maxAge = 30, maxAgeUnit = TimeUnit.DAYS)
    @ApiOperation(value = "Create Label with BarcodeId")
    @Secured(required = true, requiredShop = true)
    @Produces({"image/jpeg", "image/png", "application/pdf"})
    public Response createLabelWithBarcodeId(@PathParam("barcodeId") String barcodeId, @QueryParam("angle") int angle,
                                             @QueryParam("height") int height, @QueryParam("type") String barcodeType) {
        final File file = labelService.getBarcodeImage(barcodeId, angle, height, barcodeType);
        StreamingOutput output = new StreamingOutput() {
            @Override
            public void write(OutputStream output) throws IOException, WebApplicationException {
                ByteStreams.copy(new FileInputStream(file), output);
                file.delete();
            }
        };
        return Response.ok(output).type("image/png").build();
    }

    @GET
    @Path("/sku/{sku}")
    @Timed(name = "createLabelWithSKU")
    @CacheControl(maxAge = 30, maxAgeUnit = TimeUnit.DAYS)
    @ApiOperation(value = "Create Label with SKU")
    @Produces({"image/jpeg", "image/png", "application/pdf"})
    public Response createLabelWithSKU(@PathParam("sku") String sku, @QueryParam("angle") int angle,
                                       @QueryParam("height") int height, @QueryParam("type") String barcodeType, @QueryParam("skuPosition") String skuPosition) {

        CommonLabelService.SKUPosition skuPos = CommonLabelService.SKUPosition.Bottom;
        if (StringUtils.isNotBlank(skuPosition)) {
            try {
                skuPos = Enum.valueOf(CommonLabelService.SKUPosition.class, skuPosition);
            } catch (Exception e) {
                // ignore
            }
        }

        final File file = labelService.generateBarcode(sku, angle, height, barcodeType, skuPos);
        StreamingOutput output = new StreamingOutput() {
            @Override
            public void write(OutputStream output) throws IOException, WebApplicationException {
                ByteStreams.copy(new FileInputStream(file), output);
                file.delete();
            }
        };
        return Response.ok(output).type("image/png").build();
    }

    @POST
    @Path("/query")
    @Secured(requiredShop = true)
    @Timed(name = "queryLabels")
    @ApiOperation(value = "Query Labels")
    public SearchResult<LabelItem> queryLabels(@Valid LabelsQueryRequest request) {
        return labelService.getLabelItems(request);
    }

    @POST
    @Path("/query/qr")
    @Secured(requiredShop = true)
    @Timed(name = "queryQrCode")
    @ApiOperation(value = "Generate QR codes")
    public List<CompanyAsset> queryLabelQrCode(@Valid LabelsQueryRequest request) {
        return labelService.getQrCodeLabelItems(request);
    }



}
