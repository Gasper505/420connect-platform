package com.fourtwenty.services.resources.mgmt;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.thirdparty.leafly.LeaflyAccount;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.dispensary.Audit;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.thirdparty.LeaflyAccountService;
import com.fourtwenty.core.thirdparty.leafly.result.LeaflyAccountResult;
import com.google.inject.Inject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Api("Management - Leafly account setting")
@Path("/api/v1/mgmt/leafly")
@Produces(MediaType.APPLICATION_JSON)
public class LeaflyAccountResource extends BaseResource {

    @Inject
    private LeaflyAccountService leaflyAccountService;

    @GET
    @Path("/")
    @Timed(name = "searchLeaflyAccounts")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Search leafly accounts")
    public SearchResult<LeaflyAccountResult> searchLeaflyAccounts(@QueryParam("start") int start, @QueryParam("limit") int limit) {
        return leaflyAccountService.searchLeaflyAccounts(start, limit);
    }

    @GET
    @Path("/{id}")
    @Timed(name = "getLeaflyAccountById")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get leafly account by id")
    public LeaflyAccountResult getLeaflyAccountById(@PathParam("id") String id) {
        return leaflyAccountService.getLeaflyAccountById(id);
    }

    @POST
    @Path("/")
    @Timed(name = "createLeaflyAccount")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Create leafly account")
    public LeaflyAccount createLeaflyAccount(LeaflyAccount account) {
        return leaflyAccountService.createLeaflyAccount(account);
    }

    @PUT
    @Path("/{id}")
    @Timed(name = "updateLeaflyAccount")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Update leafly account")
    public LeaflyAccount updateLeaflyAccount(LeaflyAccount account, @PathParam("id") String id) {
        return leaflyAccountService.updateLeaflyAccount(account, id);
    }

    @DELETE
    @Path("/{id}")
    @Timed(name = "deleteLeaflyAccount")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Delete leafly account")
    public Response deleteLeaflyAccount(@PathParam("id") String id) {
        leaflyAccountService.deleteLeaflyAccount(id);
        return Response.ok().build();
    }

    @POST
    @Path("/leafly/reset")
    @Timed(name = "resetLeafly")
    @Secured(required = true, requiredShop = true)
    @Audit(action = "Reset/Delete Leafly Menu")
    @ApiOperation(value = "Reset & Delete Leafly Menu")
    public Response resetLeaflymap() {
        leaflyAccountService.resetMenu(true);
        return ok();
    }

    @POST
    @Path("/leafly/resetonly")
    @Timed(name = "resetonly")
    @Audit(action = "Reset Only Leafly Menu")
    @Secured(required = true, requiredShop = true)
    @ApiOperation(value = " OnlyReset Sync Leafly Menu")
    public Response resetLeaflyOnly() {
        leaflyAccountService.resetMenu(false);
        return ok();
    }
}
