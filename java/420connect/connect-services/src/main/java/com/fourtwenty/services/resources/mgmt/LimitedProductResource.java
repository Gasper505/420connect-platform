package com.fourtwenty.services.resources.mgmt;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.company.FilterType;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.LimitedProductResult;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.mgmt.ProductService;
import com.google.inject.Inject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Api("Management Limited - Products")
@Path("/api/v1/mgmt/limited")
@Produces(MediaType.APPLICATION_JSON)
public class LimitedProductResource extends BaseResource {

    @Inject
    ProductService productService;

    @GET
    @Path("/products/")
    @Timed(name = "searchProducts")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Search Limited Products")
    public SearchResult<LimitedProductResult> searchLimitedProducts(
            @QueryParam("shopId") String shopId,
            @QueryParam("categoryId") String categoryId,
            @QueryParam("productId") String productId,
            @QueryParam("start") int start,
            @QueryParam("limit") int limit,
            @QueryParam("quantity") boolean quantity,
            @QueryParam("term") String term,
            @QueryParam("inventoryId") String inventoryId,
            @QueryParam("status") FilterType status,
            @QueryParam("batchSku") String batchSku) {
        return productService.searchLimitedProductsByCategoryId(shopId, categoryId, productId,
                start, limit, quantity, term, inventoryId, status, batchSku);
    }
}
