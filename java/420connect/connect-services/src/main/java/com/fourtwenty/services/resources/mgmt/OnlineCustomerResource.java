package com.fourtwenty.services.resources.mgmt;


import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.consumer.ConsumerUser;
import com.fourtwenty.core.rest.dispensary.requests.consumeruser.request.OnlineConsumerUserRequest;
import com.fourtwenty.core.rest.dispensary.requests.queues.DeclineRequest;
import com.fourtwenty.core.rest.dispensary.results.OnlineUserResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.dispensary.Audit;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.mgmt.OnlineCustomerService;
import com.google.inject.Inject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.validation.Valid;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Api("Management - Consumer User")
@Path("/api/v1/mgmt/consumerUser")
@Produces(MediaType.APPLICATION_JSON)
public class OnlineCustomerResource extends BaseResource {

    @Inject
    private OnlineCustomerService customerService;

    @GET
    @Path("/{userId}")
    @Timed(name = "getConsumerUserById")
    @Audit(action = "getConsumerUserById")
    @ApiOperation(value = "Get consumer user by id")
    @Secured
    public ConsumerUser getConsumerUserById(@PathParam("userId") String userId) {
        return customerService.getConsumerUserById(userId);
    }

    @GET
    @Path("/")
    @Timed(name = "findConsumerUser")
    @Audit(action = "findConsumerUser")
    @ApiOperation(value = "Find consumer user")
    @Secured
    public SearchResult<OnlineUserResult> findConsumerUser(@QueryParam("start") int start,
                                                           @QueryParam("limit") int limit,
                                                           @QueryParam("type")OnlineConsumerUserRequest.OnlineUserQueryType queryType,
                                                           @QueryParam("marketingSource") String marketingSource,
                                                           @QueryParam("term") String term) {
        return customerService.findConsumerUser(start, limit, queryType, marketingSource, term);
    }

    @POST
    @Path("/{userId}/accept")
    @Timed(name = "acceptConsumerUser")
    @Audit(action = "acceptConsumerUser")
    @ApiOperation(value = "Accept consumer user")
    @Secured
    public ConsumerUser acceptConsumerUser(@PathParam("userId")String userId, @Valid ConsumerUser consumerUser) {
        return customerService.acceptConsumerUser(userId, consumerUser);
    }

    @POST
    @Path("/{userId}/decline")
    @Timed(name = "declineConsumerUser")
    @Audit(action = "declineConsumerUser")
    @ApiOperation(value = "Decline consumer user")
    @Secured
    public ConsumerUser declineConsumerUser(@PathParam("userId")String userId, @Valid DeclineRequest request) {
        return customerService.declineConsumerUser(userId, request);
    }
}
