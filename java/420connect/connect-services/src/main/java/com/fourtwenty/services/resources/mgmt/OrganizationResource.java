package com.fourtwenty.services.resources.mgmt;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.company.Organization;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.mgmt.OrganizationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;
@Api("Management - Organization")
@Path("/api/v1/mgmt/organization")
@Produces(MediaType.APPLICATION_JSON)
public class OrganizationResource extends BaseResource {
    @Inject
    OrganizationService organizationService;

    @GET
    @Path("/")
    @Timed(name = "getOrganizations")
    @Secured
    @ApiOperation(value = "Get Organizations")
    public List<Organization> getOrganizations() {
        return organizationService.getCurrentEmployeeOrganizations();
    }

    @POST
    @Path("/")
    @Timed(name = "addOrganization")
    @Secured
    @ApiOperation(value = "Add Organization")
    public Organization addOrganization(Organization organization) {
        return organizationService.addOrganization(organization);
    }
}