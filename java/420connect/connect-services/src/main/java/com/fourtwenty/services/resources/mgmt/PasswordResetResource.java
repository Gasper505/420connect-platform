package com.fourtwenty.services.resources.mgmt;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.company.PasswordReset;
import com.fourtwenty.core.rest.dispensary.requests.auth.PasswordResetRequest;
import com.fourtwenty.core.rest.dispensary.requests.auth.PasswordResetUpdateRequest;
import com.fourtwenty.core.services.mgmt.PasswordResetService;
import com.google.inject.Inject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.validation.Valid;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by mdo on 8/3/16.
 */

@Api("Management - Password Reset")
@Path("/api/v1/mgmt/password")
@Produces(MediaType.APPLICATION_JSON)
public class PasswordResetResource {
    @Inject
    PasswordResetService passwordResetService;

    @POST
    @Path("/reset")
    @Timed(name = "resetPasswordRequest")
    @ApiOperation(value = "Request Reset Password")
    public PasswordReset resetPasswordRequest(@Valid PasswordResetRequest request) {
        return passwordResetService.requestPasswordReset(request);
    }

    @POST
    @Path("/update")
    @Timed(name = "resetPasswordUpdateRequest")
    @ApiOperation(value = "Update Password")
    public Response resetPasswordUpdateRequest(@Valid PasswordResetUpdateRequest request) {
        passwordResetService.updatePassword(request);
        return Response.ok().build();
    }
}
