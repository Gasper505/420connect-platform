package com.fourtwenty.services.resources.mgmt;

import com.codahale.metrics.annotation.Timed;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fourtwenty.core.domain.models.thirdparty.PatientVerificationStatus;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.mgmt.VerificationSiteService;
import com.google.inject.Inject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.sf.json.JSONObject;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 * Created by Gaurav Saini on 16/5/17.
 */
@Api("Management - Patient Verification")
@Path("/api/v1/mgmt/patient")
@Produces(MediaType.APPLICATION_JSON)
@JsonAutoDetect
public class PatientVerificationResource extends BaseResource {
    @Inject
    VerificationSiteService verificationSiteService;
    @Inject
    PatientVerificationStatus patientVerificationStatus;

    /**
     * @param verificationCode
     * @return PatientVerificationStatus
     */
    @GET
    @Path("/verify")
    @Timed(name = "getVerificationDetails")
    @Secured
    @ApiOperation(value = "verifications code")
    public PatientVerificationStatus hellomdPatient(@QueryParam("recNo") String verificationCode) {

        String response = verificationSiteService.getHelloMDPatient(verificationCode);
        JSONObject jsonObject = JSONObject.fromObject(response);
        patientVerificationStatus.setPatientName(jsonObject.getString("name"));
        patientVerificationStatus.setRecommendationExpDate(jsonObject.getString("latest_medical_recommendation_expiration"));
        patientVerificationStatus.setRecommendationIssueDate(jsonObject.getString("latest_medical_recommendation_publication"));
        patientVerificationStatus.setRecommendationStatus(jsonObject.getString("latest_medical_recommendation_status"));
        return patientVerificationStatus;
    }
}
