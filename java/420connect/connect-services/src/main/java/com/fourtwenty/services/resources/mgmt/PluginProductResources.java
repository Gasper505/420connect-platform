package com.fourtwenty.services.resources.mgmt;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.plugins.BlazePluginProduct;
import com.fourtwenty.core.domain.models.plugins.KioskPluginCompanySetting;
import com.fourtwenty.core.domain.models.plugins.MessagingPluginCompanySetting;
import com.fourtwenty.core.domain.models.plugins.TVPluginCompanySetting;
import com.fourtwenty.core.rest.plugins.KioskPluginCompanySettingRequest;
import com.fourtwenty.core.rest.plugins.MessagingPluginCompanySettingRequest;
import com.fourtwenty.core.rest.plugins.TVPluginCompanySettingRequest;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.plugins.BlazePluginProductService;
import com.fourtwenty.core.services.plugins.KioskPluginService;
import com.fourtwenty.core.services.plugins.MessagingPluginService;
import com.fourtwenty.core.services.plugins.TVPluginService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Api("Management - Plugin Products")
@Path("/api/v1/plugins")
@Produces(MediaType.APPLICATION_JSON)
public class PluginProductResources {

    private MessagingPluginService messagingPluginService;
    private KioskPluginService kioskPluginService;
    private TVPluginService tvPluginService;
    private BlazePluginProductService blazePluginProductService;

    @Inject
    public PluginProductResources(MessagingPluginService messagingPluginService,
                                  KioskPluginService kioskPluginService, TVPluginService tvPluginService, BlazePluginProductService blazePluginProductService) {
        this.messagingPluginService = messagingPluginService;
        this.kioskPluginService = kioskPluginService;
        this.tvPluginService = tvPluginService;
        this.blazePluginProductService = blazePluginProductService;
    }

    @GET
    @Path("/list")
    @Timed(name = "getAllPlugins")
    @Secured(requiredShop = true)
    @ApiOperation(value = "get loaded plugin list")
    public List<BlazePluginProduct> getAllPlugins() {
        return blazePluginProductService.getAllBlazePlugins();
    }

    @GET
    @Path("/settings/messaging")
    @Timed(name = "getCompanyMessagingPlugin")
    @Secured(requiredShop = true)
    @ApiOperation(value = "get messaging plugin details for company", notes = "plugin id can be null in response which means plugin for logged in company just initialized with default values")
    public MessagingPluginCompanySetting getCompanyMessagingPlugin() {
        return messagingPluginService.getCompanyMessagingPlugin();
    }

    @POST
    @Path("/settings/messaging")
    @Timed(name = "updateCompanyMessagingPlugin")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Update messaging setting for Company & Shop")
    public MessagingPluginCompanySetting updateCompanyMessagingPlugin(@Valid final MessagingPluginCompanySettingRequest companyMessagingPluginSettingRequest) {
        return messagingPluginService.updateMessagingPluginSetting(companyMessagingPluginSettingRequest);
    }

    @GET
    @Path("/settings/kiosk")
    @Timed(name = "getCompanyKioskPlugin")
    @Secured(requiredShop = true)
    @ApiOperation(value = "get kiosk plugin details for company", notes = "plugin id can be null in response which means plugin for logged in company just initialized with default values")
    public KioskPluginCompanySetting getCompanyKioskPlugin() {
        return kioskPluginService.getCompanyConsumerKioskPlugin();
    }

    @POST
    @Path("/settings/kiosk")
    @Timed(name = "updateCompanyKioskPlugin")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Update kiosk setting for Company & Shop")
    public KioskPluginCompanySetting updateCompanyKioskPlugin(@Valid final KioskPluginCompanySettingRequest companyKioskPluginSettingRequest) {
        return kioskPluginService.updateCompanyKioskPlugin(companyKioskPluginSettingRequest);
    }

    @GET
    @Path("/settings/tv")
    @Timed(name = "getCompanyTVPlugin")
    @Secured(requiredShop = true)
    @ApiOperation(value = "get TV plugin details for company", notes = "plugin id can be null in response which means plugin for logged in company just initialized with default values")
    public TVPluginCompanySetting getCompanyTVPlugin() {
        return tvPluginService.getCompanyTVPlugin();
    }

    @POST
    @Path("/settings/tv")
    @Timed(name = "updateCompanyTVPlugin")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Update TV setting for Company")
    public TVPluginCompanySetting updateCompanyTVPlugin(@Valid final TVPluginCompanySettingRequest tvPluginCompanySetting) {
        return tvPluginService.updateTVPluginSetting(tvPluginCompanySetting);
    }

}
