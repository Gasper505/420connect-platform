package com.fourtwenty.services.resources.mgmt;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.product.Prepackage;
import com.fourtwenty.core.domain.models.product.PrepackageProductItem;
import com.fourtwenty.core.rest.dispensary.requests.inventory.PrepackageAddRequest;
import com.fourtwenty.core.rest.dispensary.requests.inventory.PrepackageItemAddRequest;
import com.fourtwenty.core.rest.dispensary.requests.inventory.PrepackageItemReduceRequest;
import com.fourtwenty.core.rest.dispensary.requests.inventory.PrepackageItemUpdateStatusRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.PrepackageGroupResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.PrepackageLineItemInfo;
import com.fourtwenty.core.security.dispensary.Audit;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.mgmt.PrepackageProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Map;

/**
 * Created by mdo on 3/9/17.
 */
@Api("Management - Product Prepackages")
@Path("/api/v1/mgmt/prepackages")
@Produces(MediaType.APPLICATION_JSON)
public class PrepackageProductResource extends BaseResource {

    @Inject
    PrepackageProductService prepackageProductService;

    @GET
    @Path("/")
    @Secured(requiredShop = true)
    @Timed(name = "getPrepackagesGroup")
    @ApiOperation(value = "Get Prepackages Grouped")
    public SearchResult<PrepackageGroupResult> getPrepackagesGroup(@QueryParam("productId") String productId, @QueryParam("shopId") String shopId) {
        return prepackageProductService.getPrepackagesGroup(productId, shopId);
    }

    @PUT
    @Path("/")
    @Secured(requiredShop = true)
    @Audit(action = "Add Prepackage Group")
    @Timed(name = "addPrepackage")
    @ApiOperation(value = "Add Prepackage")
    public Prepackage addPrepackage(@Valid PrepackageAddRequest prepackageItemAddRequest) {
        return prepackageProductService.addPrepackage(prepackageItemAddRequest);
    }

    @POST
    @Path("/{prepackageId}")
    @Secured(requiredShop = true)
    @Audit(action = "Update Prepackage Group")
    @Timed(name = "updatePrepackageGroup")
    @ApiOperation(value = "Update PrepackageGroup")
    public Prepackage updatePrepackageGroup(@PathParam("prepackageId") String prepackageId,
                                            @Valid Prepackage request) {
        return prepackageProductService.updatePrepackage(prepackageId, request);
    }

    @DELETE
    @Path("/{prepackageId}")
    @Secured(requiredShop = true)
    @Timed(name = "deletePrepackageGroup")
    @Audit(action = "Delete Prepackage Group")
    @ApiOperation(value = "Delete Prepackage Group")
    public Response deletePrepackageGroup(@PathParam("prepackageId") String prepackageId) {
        prepackageProductService.deletePrepackage(prepackageId);
        return ok();
    }


    @PUT
    @Path("/{prepackageId}/items")
    @Secured(requiredShop = true)
    @Timed(name = "addPrepackageLineItem")
    @Audit(action = "Add Prepackage Line Item")
    @ApiOperation(value = "Add Prepackage Line Item")
    public PrepackageProductItem addPrepackageLineItem(@PathParam("prepackageId") String prepackageId,
                                                       @Valid PrepackageItemAddRequest prepackageItemAddRequest) {
        return prepackageProductService.addPrepackageLineItem(prepackageId, prepackageItemAddRequest);
    }

    @POST
    @Path("/{prepackageId}/items/{itemId}/reduce")
    @Secured(requiredShop = true)
    @Audit(action = "Reduce Prepackage Quantity")
    @Timed(name = "reducePrepackageLineItem")
    @ApiOperation(value = "Reduce Prepackage Quantity")
    public PrepackageProductItem reducePrepackages(@PathParam("prepackageId") String prepackageId,
                                                   @PathParam("itemId") String prepackageItemId,
                                                   @Valid PrepackageItemReduceRequest reduceRequest) {
        return prepackageProductService.reducePrepackageLineItem(prepackageItemId, reduceRequest);
    }

    @POST
    @Path("/{prepackageId}/items/{itemId}/status")
    @Secured(requiredShop = true)
    @Timed(name = "updatePrepackageItemStatus")
    @Audit(action = "Update Prepackage Item Group")
    @ApiOperation(value = "Update Prepackage Item Status")
    public PrepackageProductItem updatePrepackageItemStatus(@PathParam("prepackageId") String prepackageId,
                                                            @PathParam("itemId") String prepackageItemId,
                                                            @Valid PrepackageItemUpdateStatusRequest reduceRequest) {
        return prepackageProductService.updatePrepackageItemStatus(prepackageItemId, reduceRequest);
    }

    @DELETE
    @Path("/{prepackageId}/items/{itemId}")
    @Secured(requiredShop = true)
    @Timed(name = "deletePrepackageItem")
    @Audit(action = "Delete Prepackage Item")
    @ApiOperation(value = "Delete Prepackage Item")
    public PrepackageProductItem deletePrepackageItem(@PathParam("prepackageId") String prepackageId,
                                                      @PathParam("itemId") String itemId) {
        return prepackageProductService.deletePrepackageLineItem(itemId);
    }


    @GET
    @Path("/{categoryId}")
    @Secured(requiredShop = true)
    @Timed(name = "getPrepackagesByCategoryId")
    @Audit(action = "Get Prepackages by Category Id")
    @ApiOperation(value = "Get Prepackages by Category Id")
    public Map<String, SearchResult<PrepackageGroupResult>> getPrepackagesByCategoryId(@PathParam("categoryId") String categoryId) {
        return prepackageProductService.getPrepackagesByCategoryId(categoryId);
    }

    @GET
    @Path("/prepackageItemInfo")
    @Secured(requiredShop = true)
    @Timed(name = "getPrepackageLineItemInfoById")
    @Audit(action = "Get prepackage items info")
    @ApiOperation(value = "Get prepackage items info")
    public Map<String, PrepackageLineItemInfo> getPrepackageLineItemInfoById(@QueryParam("prepackageItemsIds") String prepackageItemsIds) {
        return prepackageProductService.getPrepackageLineItemInfoById(prepackageItemsIds);
    }
}
