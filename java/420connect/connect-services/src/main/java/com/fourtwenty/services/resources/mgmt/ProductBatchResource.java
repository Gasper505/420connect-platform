package com.fourtwenty.services.resources.mgmt;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.models.product.BatchQuantity;
import com.fourtwenty.core.domain.models.product.ProductBatch;
import com.fourtwenty.core.domain.models.testsample.TestSample;
import com.fourtwenty.core.event.inventory.ComplianceBatch;
import com.fourtwenty.core.importer.model.ProductBatchResult;
import com.fourtwenty.core.rest.dispensary.requests.inventory.*;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.common.AssetStreamResult;
import com.fourtwenty.core.security.dispensary.Audit;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.inventory.BatchQuantityService;
import com.fourtwenty.core.services.mgmt.InventoryService;
import com.fourtwenty.core.services.mgmt.ProductBatchService;
import com.fourtwenty.core.services.testsample.request.TestSampleAddRequest;
import com.fourtwenty.core.services.testsample.request.TestSampleAttachmentRequest;
import com.fourtwenty.core.services.testsample.request.TestSampleStatusRequest;
import com.fourtwenty.core.services.thirdparty.MetrcAccountService;
import com.google.common.io.ByteStreams;
import com.warehouse.core.services.batch.TestSampleService;
import io.dropwizard.jersey.caching.CacheControl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Created by mdo on 10/11/15.
 */
@Api("Management - Product ProductBatch")
@Path("/api/v1/mgmt/batch")
@Produces(MediaType.APPLICATION_JSON)
public class ProductBatchResource extends BaseResource {
    @Inject
    InventoryService inventoryService;
    @Inject
    BatchQuantityService batchQuantityService;
    @Inject
    ProductBatchService productBatchService;
    @Inject
    TestSampleService testSampleService;
    @Inject
    private MetrcAccountService metrcAccountService;

    @GET
    @Path("/")
    @Secured(requiredShop = true)
    @Timed(name = "getProductBatches")
    @ApiOperation(value = "Get Product Batches")
    public SearchResult<ProductBatch> getBatches(@QueryParam("productId") String productId,
                                                 @QueryParam("start") int start,
                                                 @QueryParam("limit") int limit,
                                                 @QueryParam("term") String term,
                                                 @QueryParam("status") ProductBatch.BatchStatus status) {
        return inventoryService.getBatches(productId, start, limit, term, status);
    }


    @POST
    @Path("/")
    @Secured(requiredShop = true)
    @Timed(name = "createBatch")
    @Audit(action = "Create Batch")
    @ApiOperation(value = "Create New Batch")
    public ProductBatch createBatch(@Valid BatchAddRequest request) {
        return inventoryService.addBatch(request);
    }

    @POST
    @Path("/{batchId}")
    @Secured(requiredShop = true)
    @Timed(name = "updateBatch")
    @Audit(action = "Update Batch")
    @ApiOperation(value = "Update Batch")
    public ProductBatch updateBatch(@PathParam("batchId") String batchId,
                                    @Valid UpdateProductBatchRequest request) {
        return inventoryService.updateBatch(batchId, request);
    }

    @PUT
    @Path("/markAsArchived")
    @Secured(requiredShop = true)
    @Timed(name = "archiveProductBatch")
    @Audit(action = "Archive Product Batch")
    @ApiOperation(value = "Archive Product Batch")
    public ProductBatch archiveProductBatch(@Valid BatchArchiveAddRequest request) {
        return inventoryService.archive(request);
    }

    @DELETE
    @Path("/{batchId}")
    @Secured(requiredShop = true)
    @Timed(name = "deleteBatch")
    @Audit(action = "Delete Batch")
    @ApiOperation(value = "Delete Batch")
    public Response deleteBatch(@PathParam("batchId") String batchId) {
        inventoryService.deleteBatch(batchId);
        return ok();
    }

    @GET
    @Path("/quantities")
    @Secured(requiredShop = true)
    @Timed(name = "getBatchQuantities")
    @ApiOperation(value = "Get Batch Quantities for Product")
    public SearchResult<BatchQuantity> getBatchQuantities(@QueryParam("productId") String productId,
                                                          @QueryParam("inventoryId") String inventoryId) {
        return batchQuantityService.getBatchQuantitiesForProduct(getToken().getCompanyId(), getToken().getShopId(), productId, inventoryId, BatchQuantity.class, 0, Integer.MAX_VALUE);
    }

    @GET
    @Path("/oldProductBatch")
    @Timed(name = "getOldestBatchByProductId")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get oldest batch by product id")
    public ProductBatch getOldestBatchByProductId(@QueryParam("productId") String productId, @QueryParam("transactionId") String transactionId) {
        return inventoryService.getOldestBatchByProductId(productId, transactionId);
    }

    @POST
    @Path("/calculateExciseTax")
    @Timed(name = "calculateExciseTaxByUnitCost")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Calculate excise tax for unit cost")
    public BigDecimal calculateExciseTaxByUnitCost(@Valid ExciseTaxCalculateRequest request) {
        return inventoryService.calculateExciseTaxByUnitCost(request);
    }

    @GET
    @Path("/{productBatchId}")
    @Timed(name = "getProductBatch")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get Product Batch")
    public ProductBatchResult getProductBatch(@PathParam("productBatchId") String productBatchId) {
        return productBatchService.getProductBatch(productBatchId);
    }

    @GET
    @Path("/getAllProductBatches")
    @Timed(name = "getAllProductBatches")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get All Product Batches")
    public SearchResult<ProductBatchResult> getAllProductBatches(@QueryParam("start") int start, @QueryParam("limit") int limit, @QueryParam("term") String searchTerm, @QueryParam("filterOption") ProductBatch.BatchFilter filter) {
        return productBatchService.getAllProductBatches(start, limit, searchTerm, filter);
    }

    @GET
    @Path("/list/archived")
    @Timed(name = "getAllArchivedProductBatches")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get All Archived Product Batches")
    public SearchResult<ProductBatchResult> getAllArchivedProductBatches(@QueryParam("start") int start, @QueryParam("limit") int limit) {
        return productBatchService.getAllArchivedProductBatches(start, limit);
    }

    @PUT
    @Path("/{productBatchId}/status")
    @Timed(name = "updateProductBatchStatus")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Update Product Batch Status")
    public ProductBatch updateProductBatchStatus(@PathParam("productBatchId") String productBatchId, @Valid ProductBatchStatusRequest request) {
        return productBatchService.updateProductBatchStatus(productBatchId, request);
    }

    @POST
    @Path("/testSample/")
    @Timed(name = "addTestSample")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Add Test Sample")
    public TestSample addTestSample(@Valid TestSampleAddRequest request) {
        return testSampleService.addTestSample(request);
    }

    @PUT
    @Path("/testSample/{testSampleId}")
    @Timed(name = "updateTestSample")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Update Test Sample")
    public TestSample updateTestSample(@PathParam("testSampleId") String testSampleId, @Valid TestSample testSample) {
        return testSampleService.updateTestSample(testSampleId, testSample);
    }

    @PUT
    @Path("/testSample/{testSampleId}/status")
    @Timed(name = "updateTestSampleStatus")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Update Test Sample Status")
    public TestSample updateTestSampleStatus(@PathParam("testSampleId") String testSampleId, @Valid TestSampleStatusRequest request) {
        return testSampleService.updateTestSampleStatus(testSampleId, request);
    }

    @GET
    @Path("/{productBatchId}/testSample/list")
    @Timed(name = "getAllTestSamplesByProductBatchId")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get All Test Samples By Product Batch Id")
    public SearchResult<TestSample> getAllTestSampleByBatchId(@PathParam("productBatchId") String productBatchId, @QueryParam("start") int start, @QueryParam("limit") int limit) {
        return testSampleService.getAllTestSamplesByBatchId(productBatchId, start, limit);
    }

    @GET
    @Path("/productBatchesByState")
    @Timed(name = "getAllProductBatchesByState")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get All Product Batches By State")
    public SearchResult<ProductBatchResult> getAllProductBatchesByState(@QueryParam("start") int start, @QueryParam("limit") int limit, @QueryParam("state") boolean state) {
        return productBatchService.getAllProductBatchesByState(start, limit, state);
    }

    @GET
    @Path("/productBatchesByStatus")
    @Timed(name = "getAllProductBatchesByStatus")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get All Product Batches By Status")
    public SearchResult<ProductBatchResult> getAllProductBatchesByStatus(@QueryParam("start") int start, @QueryParam("limit") int limit, @QueryParam("status") ProductBatch.BatchStatus status, @QueryParam("term") String searchTerm, @QueryParam("filterOption") ProductBatch.BatchFilter filter) {
        return productBatchService.getAllProductBatchesByStatus(start, limit, status, searchTerm, filter);
    }

    @POST
    @Path("/scanningProductBatch")
    @Timed(name = "getScannedProductBatch")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get Scanned Product Batch")
    public SearchResult<ProductBatchResult> getAllScannedProductBatch(@Valid ProductBatchScannedRequest request, @QueryParam("start") int start, @QueryParam("limit") int limit) {
        return productBatchService.getAllScannedProductBatch(request, start, limit);
    }

    @GET
    @Path("/list/deletedProductBatch")
    @Timed(name = "getAllDeletedProductBatches")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get All Deleted Product Batches")
    public SearchResult<ProductBatchResult> getAllDeletedProductBatches(@QueryParam("start") int start, @QueryParam("limit") int limit) {
        return productBatchService.getAllDeletedProductBatch(start, limit);
    }

    @PUT
    @Path("/bulkBatchUpdate")
    @Timed(name = "bulkProductBatchUpdate")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Bulk Product Batch Update")
    public Response bulkProductBatchUpdate(@Valid BulkProductBatchUpdateRequest request) {
        productBatchService.bulkProductBatchUpdate(request);
        return Response.ok().build();
    }

    @DELETE
    @Path("/{testSampleId}/deleteTestSample")
    @Timed(name = "deleteTestSampleById")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Delete TestSample By Id")
    public Response deleteTestSample(@PathParam("testSampleId") String testSampleId) {
        testSampleService.deleteTestSampleById(testSampleId);
        return ok();
    }

    @PUT
    @Path("/{productBatchId}/voidStatus")
    @Timed(name = "updateProductBatchVoidStatus")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Update Product Batch Void Status")
    public ProductBatch updateProductBatchVoidStatus(@PathParam("productBatchId") String productBatchId, @QueryParam("voidStatus") Boolean voidStatus) {
        return productBatchService.updateProductBatchVoidStatus(productBatchId, voidStatus);

    }

    @GET
    @Path("/list/voidStatus")
    @Timed(name = "getAllProductBatchesByVoidStatus")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get All Product Batches By Void Status")
    public SearchResult<ProductBatchResult> getAllProductBatchesByVoidStatus(@QueryParam("start") int start, @QueryParam("limit") int limit, @QueryParam("voidStatus") Boolean voidStatus) {
        return productBatchService.getAllProductBatchesByVoidStatus(start, limit, voidStatus);
    }

    @POST
    @Path("/testSample/addAttachment")
    @Timed(name = "addTestSampleAttachment")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Add attachment for test sample")
    public TestSample addTestSampleAttachment(@Valid TestSampleAttachmentRequest request) {
        return testSampleService.addAttachments(request);
    }

    @GET
    @Path("/testSample/{testSampleId}/getAttachments")
    @Timed(name = "getTestSampleAttachment")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get all attachments for test sample")
    public List<CompanyAsset> getTestSampleAttachment(@PathParam("testSampleId") String testSampleId) {
        return testSampleService.getAllTestSampleAttachment(testSampleId);
    }

    @GET
    @Path("/testSample/{testSampleId}/getAttachment/{attachmentId}")
    @Timed(name = "getTestSampleAttachmentById")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get  attachment for test sample by Id")
    public CompanyAsset getTestSampleAttachment(@PathParam("testSampleId") String testSampleId, @PathParam("attachmentId") String attachmentId) {
        return testSampleService.getTestSampleAttachmentById(testSampleId, attachmentId);
    }

    @DELETE
    @Path("/testSample/{testSampleId}/deleteAttachment/{attachmentId}")
    @Timed(name = "deleteTestSampleAttachmentById")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Delete attachment for test sample by Id")
    public Response deleteTestSampleAttachment(@PathParam("testSampleId") String testSampleId, @PathParam("attachmentId") String attachmentId) {
        testSampleService.deleteTestSampleAttachmentById(testSampleId, attachmentId);
        return Response.ok().build();
    }

    @GET
    @Path("/list")
    @Secured(requiredShop = true)
    @Timed(name = "getProductBatchesWithQuantity")
    @ApiOperation(value = "Get Product Batches with Quantity")
    public SearchResult<ProductBatchQuantityResult> getProductBatchesWithQuantity(@QueryParam("productId") String productId,
                                                                                  @QueryParam("start") int start,
                                                                                  @QueryParam("limit") int limit) {
        return inventoryService.getProductBatchesWithQuantity(productId, start, limit);
    }

    @GET
    @Path("/asset/qr/{batchId}")
    @CacheControl(maxAge = 30, maxAgeUnit = TimeUnit.DAYS)
    @Produces({"image/jpeg", "application/pdf"})
    @Timed(name = "getProductBatchQRAsset")
    @ApiOperation(value = "Get Product Batch QR Asset")
    public Response getProductBatchAsset(@PathParam("batchId") String batchId, @QueryParam("assetToken") String assetToken) {
        final AssetStreamResult result = inventoryService.getProductBatchQRAsset(batchId, assetToken);
        StreamingOutput streamOutput = new StreamingOutput() {
            @Override
            public void write(OutputStream os) throws IOException,
                    WebApplicationException {
                if (result != null && result.getStream() != null) {
                    ByteStreams.copy(result.getStream(), os);
                }
            }
        };

        return Response.ok(streamOutput).type(result.getContentType()).build();
    }


    @GET
    @Path("/metrc/{label}")
    @Secured(requiredShop = true)
    @Timed(name = "getMetrcBatchByLabel")
    @ApiOperation(value = "Get metrc batch by label")
    public ComplianceBatch getMetrcBatchByLabel(@PathParam("label") String label,
                                                @QueryParam("productId") String productId) {
        return metrcAccountService.getMetrcBatchByLabel(label,productId);
    }

    @POST
    @Path("/bundle/cogs")
    @Secured(requiredShop = true)
    @Timed(name = "getDefaultCogsForBundles")
    @ApiOperation(value = "Get estimated COGS for bundle batches")
    public BigDecimal calculateEstimateCogs(@Valid List<BatchBundleItemsRequest> items) {
        return inventoryService.calculateEstimateCogs(items);
    }

    @GET
    @Path("/batchIdList")
    @Secured(requiredShop = true)
    @Timed(name = "getBatchesbyBatchIdList")
    @ApiOperation(value = "get Batches by Batch Id List")
    public Map<String, ProductBatch> getBatchesbyBatchIdList(@QueryParam("batchIdList") List<String> batchIdList) {
        return productBatchService.getBatchesByBatchesId(batchIdList);
    }

}
