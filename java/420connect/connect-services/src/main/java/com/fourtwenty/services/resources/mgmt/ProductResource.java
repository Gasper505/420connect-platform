package com.fourtwenty.services.resources.mgmt;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.company.FilterType;
import com.fourtwenty.core.domain.models.product.MemberGroupPrices;
import com.fourtwenty.core.domain.models.product.PricingTemplate;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductQuantityInfo;
import com.fourtwenty.core.rest.dispensary.requests.inventory.*;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.*;
import com.fourtwenty.core.security.dispensary.Audit;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.mgmt.PricingTemplateService;
import com.fourtwenty.core.services.mgmt.ProductService;
import com.google.inject.Inject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created by Stephen Schmidt on 10/8/2015.
 */
@Api("Management - Products")
@Path("/api/v1/mgmt/products")
@Produces(MediaType.APPLICATION_JSON)
public class ProductResource extends BaseResource {
    @Inject
    private ProductService productService;
    @Inject
    private PricingTemplateService pricingTemplateService;

    @GET
    @Path("/")
    @Timed(name = "searchProducts")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Search Products")
    public SearchResult<ProductResult> searchProducts(
            @QueryParam("shopId") String shopId,
            @QueryParam("categoryId") String categoryId,
            @QueryParam("productId") String productId,
            @QueryParam("start") int start,
            @QueryParam("limit") int limit,
            @QueryParam("quantity") boolean quantity,
            @QueryParam("term") String term,
            @QueryParam("inventoryId") String inventoryId,
            @QueryParam("status") FilterType status,
            @QueryParam("batchSku") String batchSku) {
        return productService.searchProductsByCategoryId(shopId, categoryId, productId, start, limit, quantity, term, inventoryId, status, batchSku);
    }


    @GET
    @Path("/list")
    @Timed(name = "getProductsListByIds")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get Product List By Ids")
    public SearchResult<Product> getProductsListByIds(@QueryParam("productIds") List<String> productIds) {
        return productService.getProductsByIds(productIds);
    }

    @GET
    @Path("/productByBatchSKU")
    @Timed(name = "getProductByBatchSKU")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get Product By Product Batch SKU")
    public Product getProductByBatchSKU(@QueryParam("batchSKU") String batchSKU) {
        return productService.getProductByBatchSKU(batchSKU);
    }

    @GET
    @Path("/list/limited")
    @Timed(name = "getProductsListByIds")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get Product List By Ids")
    public SearchResult<ProductLimitedResult> getProductsListByIds() {
        return productService.getProductListLimited();
    }

    @GET
    @Path("/list/vendors/{vendorId}")
    @Timed(name = "getProductsByVendorId")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get Products by vendor id")
    public SearchResult<Product> getProductsByVendorId(@PathParam("vendorId") String vendorId,
                                                       @QueryParam("active") boolean active) {
        return productService.getProductsByVendorId(vendorId, active);
    }

    @GET
    @Path("/{productId}")
    @Timed(name = "searchProducts")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get Product By Id")
    public ProductResult searchProducts(@PathParam("productId") String productId) {
        return productService.getProductById(productId);
    }

    @POST
    @Path("/")
    @Timed(name = "addProduct")
    @Secured(requiredShop = true)
    @Audit(action = "Create Product")
    @ApiOperation(value = "Create Product")
    public Product addProduct(@Valid ProductAddRequest addRequest) {
        return productService.addProduct(addRequest);
    }

    @POST
    @Path("/{productId}")
    @Secured(requiredShop = true)
    @Timed(name = "updateProduct")
    @Audit(action = "Update Product")
    @ApiOperation(value = "Update Product")
    public Product updateProduct(@PathParam("productId") String productId, @Valid Product updateRequest) {
        return productService.updateProduct(productId, updateRequest);
    }

    @DELETE
    @Path("/{productId}")
    @Secured(requiredShop = true)
    @Timed(name = "deleteProduct")
    @Audit(action = "Delete Product")
    @ApiOperation(value = "Delete Product")
    public Response deleteProduct(@PathParam("productId") String productId) {
        productService.deleteProduct(productId);
        return ok();
    }

    @POST
    @Path("/{productId}/convert")
    @Secured(requiredShop = true)
    @Timed(name = "convertToProduct")
    @Audit(action = "Convert Product")
    @ApiOperation(value = "Convert Product")
    public Product convertToProduct(@PathParam("productId") String productId, @Valid InventoryConvertToProductRequest request) {
        return productService.convertToProduct(productId, request);
    }

    @POST
    @Path("/copy")
    @Secured(requiredShop = true)
    @Timed(name = "copyProducts")
    @Audit(action = "Copy Products")
    @ApiOperation(value = "Copy Products")
    public SearchResult<Product> copyProducts(@Valid CopyProductRequest request) {
        return productService.copyProductsToShop(request);
    }


    @GET
    @Path("/{productId}/groupPrices")
    @Secured(requiredShop = true)
    @Timed(name = "getGroupPrices")
    @ApiOperation(value = "Member Group Prices - Get Prices")
    public SearchResult<MemberGroupPricesResult> getGroupPrices(@PathParam("productId") String productId) {
        return productService.getMemberGroupPrices(productId);
    }

    @GET
    @Path("/{productId}/groupPrices/member/{memberId}")
    @Secured(requiredShop = true)
    @Timed(name = "getMemberGroupPricesForMember")
    @ApiOperation(value = "Member Group Prices - Get Prices")
    public MemberGroupPricesResult getGroupPricesForMember(@PathParam("productId") String productId,
                                                           @PathParam("memberId") String memberId) {
        return productService.getGroupPricesForMember(productId, memberId);
    }


    @PUT
    @Path("/{productId}/groupPrices")
    @Secured(requiredShop = true)
    @Timed(name = "addGroupPrices")
    @Audit(action = "Add Group Price")
    @ApiOperation(value = "Member Group Prices - Add New Group Prices")
    public MemberGroupPricesResult addGroupPrices(@PathParam("productId") String productId, @Valid MemberGroupPricesAddRequest addRequest) {
        return productService.addMemberGroupPrices(productId, addRequest);
    }

    @POST
    @Path("/{productId}/groupPrices/{groupPricesId}")
    @Secured(requiredShop = true)
    @Timed(name = "updateGroupPrices")
    @Audit(action = "Update Group Price")
    @ApiOperation(value = "Member Group Prices - Update Group Prices")
    public MemberGroupPricesResult updateGroupPrices(@PathParam("productId") String productId, @PathParam("groupPricesId") String groupPricesId,
                                                     MemberGroupPrices groupPrices) {
        return productService.updatePrices(productId, groupPricesId, groupPrices);
    }


    @DELETE
    @Path("/{productId}/groupPrices/{groupPricesId}")
    @Secured(requiredShop = true)
    @Timed(name = "deleteMemberGroupPrices")
    @Audit(action = "Delete Group Price")
    @ApiOperation(value = "Member Group Prices - Delete Group Prices")
    public Response deleteMemberGroupPrices(@PathParam("productId") String productId, @PathParam("groupPricesId") String groupPricesId) {
        productService.deleteMemberGroupPrices(productId, groupPricesId);
        return ok();
    }

    @GET
    @Path("/search")
    @Timed(name = "searchAllProducts")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Search all Products")
    public SearchResult<ProductCustomResult> searchAllProducts(@QueryParam("shopId") String shopId,
                                                               @QueryParam("categoryId") String categoryId,
                                                               @QueryParam("start") int start,
                                                               @QueryParam("limit") int limit,
                                                               @QueryParam("term") String term,
                                                               @QueryParam("status") FilterType active) {
        return productService.searchAllProducts(shopId, categoryId, start, limit, term, active);
    }

    @PUT
    @Path("/bulkupdate")
    @Secured(requiredShop = true)
    @Timed(name = "bulkProductUpdates")
    @ApiOperation(value = "Bulk product updates")
    public Response bulkProductUpdates(@Valid ProductBulkUpdateRequest productUpdateRequest) {
        productService.bulkProductUpdates(productUpdateRequest);
        return ok();
    }

    @POST
    @Path("/copyProduct")
    @Secured(requiredShop = true)
    @Timed(name = "createProductFromExisting")
    @Audit(action = "Create Product from existing")
    @ApiOperation(value = "Create new product from existing product")
    public Product createProductFromExisting(@Valid CreateProductFromExisting request) {
        return productService.createProductFromExisting(request);
    }

    @GET
    @Path("/{productId}/quantityInfo")
    @Secured(requiredShop = true)
    @Timed(name = "getQuantityInfo")
    @ApiOperation(value = "Get Product Quantity Info")
    public ProductQuantityInfo getQuantityInfo(@PathParam("productId") String productId) {
        return productService.getQuantityInfo(productId);
    }

    @GET
    @Path("/list/{vendorId}/vendor/brand")
    @Secured(requiredShop = true)
    @Timed(name = "getAllProductsByVendorBrand")
    @ApiOperation(value = "Get all products by vendor's brand")
    public SearchResult<ProductResult> getAllProductsByVendorBrand(@PathParam("vendorId") String vendorId, @QueryParam("start") int start, @QueryParam("limit") int limit) {
        return productService.getAllProductsByVendorBrand(vendorId, start, limit);
    }

    @POST
    @Path("/pricingTemplate")
    @Secured(requiredShop = true)
    @Timed(name = "createPricingTemplate")
    @Audit(action = "Create pricing template")
    @ApiOperation(value = "Create pricing template")
    public PricingTemplate createPricingTemplate(@Valid PricingTemplate pricingTemplate) {
        return pricingTemplateService.createPricingTemplate(pricingTemplate);
    }

    @PUT
    @Path("/{pricingTemplateId}/pricingTemplate")
    @Secured(requiredShop = true)
    @Timed(name = "updatePricingTemplate")
    @Audit(action = "Update pricing template")
    @ApiOperation(value = "Update pricing template")
    public PricingTemplate updatePricingTemplate(@PathParam("pricingTemplateId") String pricingTemplateId, @Valid PricingTemplate pricingTemplate) {
        return pricingTemplateService.updatePricingTemplate(pricingTemplateId, pricingTemplate);
    }

    @GET
    @Path("/{pricingTemplateId}/pricingTemplate")
    @Secured(requiredShop = true)
    @Timed(name = "getPricingTemplateById")
    @ApiOperation(value = "Get pricing template by id")
    public PricingTemplate getPricingTemplateById(@PathParam("pricingTemplateId") String pricingTemplateId) {
        return pricingTemplateService.getPricingTemplateById(pricingTemplateId);
    }

    @DELETE
    @Path("/{pricingTemplateId}/pricingTemplate")
    @Secured(requiredShop = true)
    @Timed(name = "deletePricingTemplateById")
    @Audit(action = "Delete pricing template by id")
    @ApiOperation(value = "Delete pricing template by id")
    public Response deletePricingTemplateById(@PathParam("pricingTemplateId") String pricingTemplateId) {
        pricingTemplateService.deletePricingTemplateById(pricingTemplateId);
        return ok();
    }

    @GET
    @Path("/pricingTemplates")
    @Secured(requiredShop = true)
    @Timed(name = "getPricingTemplates")
    @ApiOperation(value = "Get pricing template list")
    public SearchResult<PricingTemplate> getPricingTemplates(@QueryParam("start") int start, @QueryParam("limit") int limit) {
        return pricingTemplateService.getPricingTemplates(start, limit);
    }

    @GET
    @Path("/pricingTemplates/priceBreaks")
    @Secured(requiredShop = true)
    @Timed(name = "getPricingTemplates")
    @ApiOperation(value = "Get pricing template list")
    public ProductPriceBreakResult getPriceBreaksForUnitType(@QueryParam("weightPerUnit") String weightPerUnit) {
        return pricingTemplateService.getPriceBreaksForUnitType(weightPerUnit);
    }

}
