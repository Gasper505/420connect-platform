package com.fourtwenty.services.resources.mgmt;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.product.ProductWeightTolerance;
import com.fourtwenty.core.rest.dispensary.requests.inventory.ProductWeightToleranceAddRequest;
import com.fourtwenty.core.rest.dispensary.requests.inventory.ToleranceEnableRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.inventory.ProductWeightToleranceService;
import com.google.inject.Inject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.validation.Valid;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Api("Management - Weight Tolerance")
@Path("/api/v1/mgmt/tolerances")
@Produces(MediaType.APPLICATION_JSON)
public class ProductWeightToleranceResource extends BaseResource {

    @Inject
    private ProductWeightToleranceService productWeightToleranceService;

    @GET
    @Path("/")
    @Secured(requiredShop = true)
    @Timed(name = "getWeightTolerances")
    @ApiOperation(value = "Get Weight Tolerances")
    public SearchResult<ProductWeightTolerance> getWeightTolerances() {
        return productWeightToleranceService.getWeightTolerances();
    }

    @GET
    @Path("/{toleranceId}")
    @Secured(requiredShop = true)
    @Timed(name = "getWeightToleranceById")
    @ApiOperation(value = "Get Weight Tolerance By Id")
    public ProductWeightTolerance getWeightToleranceById(@PathParam("toleranceId")String toleranceId) {
        return productWeightToleranceService.getWeightToleranceById(toleranceId);
    }

    @POST
    @Path("/")
    @Secured(requiredShop = true)
    @Timed(name = "addWeightTolerance")
    @ApiOperation(value = "Add weight tolerance")
    public ProductWeightTolerance addWeightTolerance(@Valid ProductWeightToleranceAddRequest request) {
        return productWeightToleranceService.addWeightTolerance(request);
    }

    @PUT
    @Path("/{toleranceId}")
    @Secured(requiredShop = true)
    @Timed(name = "updateWeightTolerance")
    @ApiOperation(value = "Update weight tolerance")
    public ProductWeightTolerance updateWeightTolerance(@PathParam("toleranceId")String toleranceId, @Valid ProductWeightTolerance request) {
        return productWeightToleranceService.updateWeightTolerance(toleranceId, request);
    }

    @DELETE
    @Path("/{toleranceId}")
    @Secured(requiredShop = true)
    @Timed(name = "deleteWeightTolerance")
    @ApiOperation(value = "Delete weight tolerance")
    public void deleteWeightTolerance(@PathParam("toleranceId")String toleranceId, @Valid ProductWeightTolerance request) {
        productWeightToleranceService.deleteWeightTolerance(toleranceId);
    }

    @POST
    @Path("/{toleranceId}/enable")
    @Secured(requiredShop = true)
    @Timed(name = "enableWeightTolerance")
    @ApiOperation(value = "Enable weight tolerance")
    public ProductWeightTolerance enableWeightTolerance(@PathParam("toleranceId")String toleranceId, @Valid ToleranceEnableRequest request) {
        return productWeightToleranceService.enableWeightTolerance(toleranceId, request);
    }



}
