package com.fourtwenty.services.resources.mgmt;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.loyalty.Promotion;
import com.fourtwenty.core.rest.dispensary.requests.promotions.PromotionAddRequest;
import com.fourtwenty.core.rest.dispensary.requests.promotions.PromotionRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.promotion.PromotionResult;
import com.fourtwenty.core.security.dispensary.Audit;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.mgmt.PromotionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/**
 * Created by mdo on 1/3/17.
 */
@Api("Management - Promotions")
@Path("/api/v1/mgmt/promotions")
@Produces(MediaType.APPLICATION_JSON)
public class PromotionResource extends BaseResource {
    @Inject
    PromotionService promotionService;

    // Getters
    @GET
    @Path("/{promotionId}")
    @Secured
    @Timed(name = "getPromotionById")
    @ApiOperation(value = "Get Promotion By Id")
    public PromotionResult getPromotionById(@PathParam("promotionId") String promotionId) {
        return promotionService.getPromotionById(promotionId);
    }

    @GET
    @Path("/")
    @Secured
    @Timed(name = "getPromotions")
    @ApiOperation(value = "Get Promotions")
    public SearchResult<Promotion> getPromotions(@QueryParam("start") int start, @QueryParam("limit") int limit) {
        return promotionService.getPromotions(start, limit);
    }

    @POST
    @Path("/")
    @Secured
    @Timed(name = "addPromotion")
    @Audit(action = "Add Promotion")
    @ApiOperation(value = "Add Promotions")
    public Promotion addPromotion(@Valid PromotionAddRequest request) {
        return promotionService.addPromotion(request);
    }

    @PUT
    @Path("/{promotionId}")
    @Secured
    @Timed(name = "updatePromotion")
    @Audit(action = "Update Promotion")
    @ApiOperation(value = "Update Promotions")
    public PromotionResult updatePromotion(@PathParam("promotionId") String promotionId, @Valid PromotionRequest promotionRequest) {
        return promotionService.updatePromotion(promotionId, promotionRequest);
    }

    @DELETE
    @Path("/{promotionId}")
    @Secured
    @Timed(name = "deletePromotion")
    @Audit(action = "Delete Promotion")
    @ApiOperation(value = "Delete Promotions")
    public void deletePromotion(@PathParam("promotionId") String promotionId) {
        promotionService.deletePromotion(promotionId);
    }

}
