package com.fourtwenty.services.resources.mgmt;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.comments.UserActivity;
import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.models.purchaseorder.POActivity;
import com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrder;
import com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrderLimitedResult;
import com.fourtwenty.core.rest.comments.ActivityCommentRequest;
import com.fourtwenty.core.rest.comments.UserActivityAddRequest;
import com.fourtwenty.core.rest.common.EmailRequest;
import com.fourtwenty.core.rest.dispensary.requests.inventory.*;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.ShipmentBillResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.PurchaseOrderItemResult;
import com.fourtwenty.core.rest.purchaseorders.POCompleteRequest;
import com.fourtwenty.core.security.dispensary.Audit;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.comments.UserActivityService;
import com.fourtwenty.core.services.mgmt.POActivityService;
import com.fourtwenty.core.services.mgmt.PurchaseOrderService;
import com.fourtwenty.core.services.thirdparty.models.MetrcVerifiedPackage;
import com.google.common.io.ByteStreams;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

/**
 * Created by decipher on 3/10/17 3:24 PM
 * Abhishek Samuel (Software Engineer)
 * abhsihek.decipher@decipherzone.com
 * Decipher Zone Softwares
 * www.decipherzone.com
 */
@Api("Management - Purchase Order")
@Path("/api/v1/mgmt/purchaseOrder")
@Produces(MediaType.APPLICATION_JSON)
public class PurchaseOrderResource extends BaseResource {

    private PurchaseOrderService purchaseOrderService;

    private POActivityService poActivityService;
    private UserActivityService userActivityService;

    @Inject
    public PurchaseOrderResource(PurchaseOrderService purchaseOrderService, POActivityService poActivityService,
                                 UserActivityService userActivityService) {
        this.purchaseOrderService = purchaseOrderService;
        this.poActivityService = poActivityService;
        this.userActivityService = userActivityService;
    }

    @POST
    @Path("/")
    @Secured(requiredShop = true)
    @Timed(name = "addPurchaseOrder")
    @Audit(action = "Create PO")
    @ApiOperation(value = "Create Purchase Order")
    public PurchaseOrder addPurchaseOrder(@Valid PurchaseOrderAddRequest purchaseOrderAddRequest) {
        return purchaseOrderService.addPurchaseOrder(purchaseOrderAddRequest);
    }

    @GET
    @Path("/{purchaseOrderId}")
    @Secured(requiredShop = true)
    @Timed(name = "getPurchaseOrderById")
    @ApiOperation(value = "Get Purchase Order by id")
    public PurchaseOrderItemResult getPurchaseOrderById(@PathParam("purchaseOrderId") String purchaseOrderId) {
        return purchaseOrderService.getPurchaseOrderItemById(purchaseOrderId);
    }


    @GET
    @Path("/list")
    @Secured(requiredShop = true)
    @Timed(name = "getPurchaseOrderByCompany")
    @ApiOperation(value = "List of purchase order by company id")
    public SearchResult<PurchaseOrderItemResult> getAllPurchaseOrder(@QueryParam("start") int start, @QueryParam("limit") int limit, @QueryParam("customerType") PurchaseOrder.CustomerType customerType, @QueryParam("term") String searchTerm, @QueryParam("vendorId") String vendorId,@QueryParam("sortOption") PurchaseOrder.PurchaseOrderSort sortOption) {
        return purchaseOrderService.getAllPurchaseOrder(start, limit, customerType, searchTerm, vendorId, sortOption);
    }

    @GET
    @Path("/list/{status}")
    @Secured(requiredShop = true)
    @Timed(name = "getPurchaseOrderByCompany")
    @ApiOperation(value = "List of purchase order by company id and status")
    public SearchResult<PurchaseOrderItemResult> getAllPurchaseOrder(@QueryParam("start") int start, @QueryParam("limit") int limit, @PathParam("status") String status, @QueryParam("customerType") PurchaseOrder.CustomerType customerType, @QueryParam("term") String searchTerm, @QueryParam("sortOption") PurchaseOrder.PurchaseOrderSort sortOption) {
        return purchaseOrderService.getPOListByStatus(start, limit, status, customerType, searchTerm, sortOption);
    }


    @GET
    @Path("/{purchaseOrderId}/logsList")
    @Secured(requiredShop = true)
    @Timed(name = "getAllPOActivityList")
    @ApiOperation(value = "List of PO activity by purchase order")
    public SearchResult<POActivity> getAllPOActivityList(@QueryParam("start") int start, @QueryParam("limit") int limit, @PathParam("purchaseOrderId") String purchaseOrderId) {
        return poActivityService.getAllPOActivityList(start, limit, purchaseOrderId);
    }

    @GET
    @Path("/{purchaseOrderId}/activities")
    @Secured(requiredShop = true)
    @Timed(name = "getAllPOActivityList")
    @ApiOperation(value = "List of PO activity by purchase order")
    public SearchResult<POActivity> getAllPOActivities(@QueryParam("start") int start, @QueryParam("limit") int limit, @PathParam("purchaseOrderId") String purchaseOrderId) {
        return poActivityService.getAllPOActivityList(start, limit, purchaseOrderId);
    }

    @PUT
    @Path("/{purchaseOrderId}/products/status")
    @Secured(requiredShop = true)
    @Timed(name = "updatePOProductRequestStatus")
    @Audit(action = "Update PO Status")
    @ApiOperation(value = "Update PO product request status", notes = "If status is true then shipment is accepted otherwise declined")
    public PurchaseOrderItemResult updatePOProductRequestStatus(@PathParam("purchaseOrderId") String poID, @Valid POProductRequestUpdate poProductRequestUpdate) {
        return purchaseOrderService.updatePOProductRequestStatus(poID, poProductRequestUpdate);
    }

    @PUT
    @Path("/{purchaseOrderId}/status")
    @Secured(requiredShop = true)
    @Timed(name = "updatePurchaseOrderStatus")
    @Audit(action = "Update PO Status")
    @ApiOperation(value = "Update PO status", notes = "Status can be RequiredApproval/Cancel/Decline")
    public PurchaseOrder updatePurchaseOrderStatus(@PathParam("purchaseOrderId") String poID, @Valid POStatusUpdateRequest poStatusUpdateRequest) {
        return purchaseOrderService.updatePurchaseOrderStatus(poID, poStatusUpdateRequest);
    }

    @POST
    @Path("/{purchaseOrderId}/approval")
    @Secured(requiredShop = true)
    @Timed(name = "updatePOManagerApproval")
    @Audit(action = "Update PO Approval Status")
    @ApiOperation(value = "Manager approval", notes = "Approval status value can be Approved/Decline/Cancel")
    public PurchaseOrder updatePOManagerApproval(@PathParam("purchaseOrderId") String poID, @Valid POManagerApprovalRequest poManagerApprovalRequest) {
        return purchaseOrderService.updatePOManagerApproval(poID, poManagerApprovalRequest);
    }

    @PUT
    @Path("/{purchaseOrderId}")
    @Secured(requiredShop = true)
    @Timed(name = "updatePurchaseOrder")
    @ApiOperation(value = "Update PO")
    @Audit(action = "Update PO")
    public PurchaseOrder updatePurchaseOrder(@PathParam("purchaseOrderId") String poID, @Valid PurchaseOrder purchaseOrder) {
        return purchaseOrderService.updatePurchaseOrder(poID, purchaseOrder);
    }

    @POST
    @Path("/{purchaseOrderId}/attachments")
    @Secured(requiredShop = true)
    @Timed(name = "addPOAttachment")
    @ApiOperation(value = "Add PO Attachment")
    @Audit(action = "Add Attachment")
    public PurchaseOrder addPOAttachment(@PathParam("purchaseOrderId") String poID, @Valid POAttachmentRequest poAttachmentRequest) {
        return purchaseOrderService.addPOAttachment(poID, poAttachmentRequest);
    }

    @GET
    @Path("/{purchaseOrderId}/attachments/{attachmentId}")
    @Secured(requiredShop = true)
    @Timed(name = "getPOAttachment")
    @ApiOperation(value = "Get PO Attachment")
    public CompanyAsset getPOAttachment(@PathParam("purchaseOrderId") String purchaseOrderId, @PathParam("attachmentId") String attachmentId) {
        return purchaseOrderService.getPOAttachment(purchaseOrderId, attachmentId);
    }

    @PUT
    @Path("/{purchaseOrderId}/attachments/{attachmentId}")
    @Secured(requiredShop = true)
    @Timed(name = "updateAttachment")
    @ApiOperation(value = "Update PO attachment")
    @Audit(action = "Update Attachment")
    public PurchaseOrder updatePOAttachment(@PathParam("purchaseOrderId") String purchaseOrderId,
                                            @PathParam("attachmentId") String attachmentId,
                                            @Valid POAttachmentRequest poAttachmentRequest) {
        return purchaseOrderService.updatePOAttachment(purchaseOrderId, attachmentId, poAttachmentRequest);
    }

    @DELETE
    @Path("/{purchaseOrderId}/attachments/{attachmentId}")
    @Secured(requiredShop = true)
    @Timed(name = "deleteAttachment")
    @ApiOperation(value = "Delete PO Attachment")
    @Audit(action = "Delete Attachment")
    public Response deleteAttachment(@PathParam("purchaseOrderId") String purchaseOrderId, @PathParam("attachmentId") String attachmentId) {
        purchaseOrderService.deletePOAttachment(purchaseOrderId, attachmentId);
        return ok();
    }

    @POST
    @Path("/{purchaseOrderId}/receive")
    @Secured(requiredShop = true)
    @Timed(name = "receivePurchaseOrder")
    @ApiOperation(value = "Receive PO")
    @Audit(action = "Receive PO")
    public PurchaseOrder receivePurchaseOrder(@PathParam("purchaseOrderId") String purchaseOrderId) {
        return purchaseOrderService.receivePurchaseOrder(purchaseOrderId);
    }

    @POST
    @Path("/{purchaseOrderId}/mark")
    @Secured(requiredShop = true)
    @Timed(name = "markPurchaseOrder")
    @ApiOperation(value = "Mark PO")
    @Audit(action = "Mark PO As Waiting Shipment")
    public PurchaseOrder markPurchaseOrder(@PathParam("purchaseOrderId") String purchaseOrderId) {
        return purchaseOrderService.markPurchaseOrder(purchaseOrderId);
    }

    @POST
    @Path("/{purchaseOrderId}/complete")
    @Timed(name = "completePurchaseOrder")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Complete Shipment Arrival Details")
    @Audit(action = "Complete PO")
    public ShipmentBillResult completePurchaseOrder(@PathParam("purchaseOrderId") String poId, @Valid POCompleteRequest request) {
        return purchaseOrderService.completeShipmentArrival(poId, request);
    }

    @POST
    @Path("/{purchaseOrderId}/sendToAccounting")
    @Timed(name = "sendEmailToAccounting")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Send Email to Account")
    @Audit(action = "Update PO Approval Status")
    public Response sendEmailToAccounting(@PathParam("purchaseOrderId") String poId, @Valid EmailRequest emailRequest) {
        purchaseOrderService.sendEmailToAccounting(poId, emailRequest);
        return ok();
    }

    @POST
    @Path("/{purchaseOrderId}/saveAsPending")
    @Timed(name = "saveAsPending")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Save as pending PO")
    @Audit(action = "Save PO As Pending")
    public PurchaseOrder saveAsPending(@PathParam("purchaseOrderId") String poId, @Valid PurchaseOrder purchaseOrder) {
        return purchaseOrderService.saveAsPendingPO(poId, purchaseOrder);
    }

    @GET
    @Path("/getMetricByLabel/{label}")
    @Timed(name = "getMetricPackageByLabel")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get metrics packages by label")
    public MetrcVerifiedPackage getMetricPackageByLabel(@PathParam("label") String label) {
        return purchaseOrderService.getMetricPackageByLabel(label);
    }

    @GET
    @Path("/list/archive")
    @Secured(requiredShop = true)
    @Timed(name = "getArchivedPurchaseOrder")
    @ApiOperation(value = "List of archived purchase order")
    public SearchResult<PurchaseOrderItemResult> getArchivedPurchaseOrder(@QueryParam("start") int start, @QueryParam("limit") int limit, @QueryParam("customerType") PurchaseOrder.CustomerType customerType, @QueryParam("term") String searchTerm) {
        return purchaseOrderService.getArchivedPurchaseOrder(start, limit, customerType, searchTerm);
    }

    @GET
    @Path("/{productId}/poList")
    @Timed(name = "getPOByProductId")
    @ApiOperation(value = "Get PO By product Id")
    public SearchResult<PurchaseOrder> getPurchaseOrdersByProductId(@PathParam("productId") String productId, @QueryParam("start") int start, @QueryParam("limit") int limit, @QueryParam("term") String searchTerm) {
        return purchaseOrderService.getPurchaseOrdersByProductId(start, limit, searchTerm, productId);
    }

    @POST
    @Path("/{purchaseOrderId}/markAsArchived")
    @Timed(name = "archivePurchaseOrder")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Mark PO as archive")
    @Audit(action = "Archive PO")
    public PurchaseOrder archivePurchaseOrder(@PathParam("purchaseOrderId") String purchaseOrderId) {
        return purchaseOrderService.archivePurchaseOrder(purchaseOrderId);
    }

    @POST
    @Path("/{purchaseOrderId}/emailToVendor")
    @Timed(name = "emailPOToVendor")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Email PO To Vendor")
    @Audit(action = "Email to Vendor")
    public Response emailPOToVendor(@PathParam("purchaseOrderId") String purchaseOrderId) {
        purchaseOrderService.emailPOToVendor(purchaseOrderId);
        return ok();
    }

    @PUT
    @Path("/{purchaseOrderId}/submitAll")
    @Secured(requiredShop = true)
    @Timed(name = "updateAllPOProductRequestStatus")
    @ApiOperation(value = "Update All PO product request status", notes = "If status is true then shipment is accepted otherwise declined")
    @Audit(action = "Submit Receiving PO")
    public PurchaseOrderItemResult updatePOProductRequestStatus(@PathParam("purchaseOrderId") String poID, @Valid List<POProductRequestUpdate> poProductRequestUpdates) {
        return purchaseOrderService.updateAllPOProductRequestStatus(poID, poProductRequestUpdates);
    }

    @POST
    @Path("/{purchaseOrderId}/emailToCustomerCompany")
    @Timed(name = "emailPOToCustomerCompany")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Email PO To Customer Company")
    @Audit(action = "Email to Company")
    public Response emailPOToCustomerCompany(@PathParam("purchaseOrderId") String purchaseOrderId) {
        purchaseOrderService.emailPOToCustomerCompany(purchaseOrderId);
        return ok();
    }

    @Deprecated
    @POST
    @Path("/POComments/comment")
    @Timed(name = "addPOComment")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Add PO Comments")
    public UserActivity addPOComment(@Valid UserActivityAddRequest request) {
        return userActivityService.addUserActivity(request);
    }

    @Deprecated
    @GET
    @Path("/{purchaseOrderId}/allCommentsOfPO")
    @Timed(name = "getAllCommentsOfPO")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get All Comments Of PO")
    public SearchResult<UserActivity> getAllCommentsOfPO(@PathParam("purchaseOrderId") String purchaseOrderId, @QueryParam("start") int start, @QueryParam("limit") int limit) {
        return userActivityService.getAllCommentsByReferenceId(purchaseOrderId, start, limit);
    }

    @POST
    @Path("/activity/comment")
    @Timed(name = "addActivityComment")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Add activity comment")
    @Audit(action = "Add Comment")
    public POActivity addActivityComment(@Valid ActivityCommentRequest request) {
        return poActivityService.addActivityComment(request);
    }

    @POST
    @Path("/preparePurchaseOrder")
    @Secured(requiredShop = true)
    @Timed(name = "preparePurchaseOrder")
    @ApiOperation(value = "Prepare Purchase Order")
    public PurchaseOrder preparePurchaseOrder(@Valid PurchaseOrderAddRequest purchaseOrderAddRequest) {
        return purchaseOrderService.preparePurchaseOrder(purchaseOrderAddRequest);
    }

    @GET
    @Path("/{purchaseOrderId}/generatePDF")
    @Secured(requiredShop = true)
    @Timed(name = "createPdfForPurchaseOrder")
    @ApiOperation(value = "Create PDF For Purchase Order")
    public Response createPdfForPurchaseOrder(@PathParam("purchaseOrderId") String purchaseOrderId) {
        InputStream inputStream = purchaseOrderService.createPdfForPurchaseOrder(purchaseOrderId);
        if (inputStream != null) {
            StreamingOutput streamOutput = new StreamingOutput() {
                @Override
                public void write(OutputStream os) throws IOException,
                        WebApplicationException {
                    ByteStreams.copy(inputStream, os);
                }
            };
            return Response.ok(streamOutput).type("application/pdf").build();
        }
        return Response.ok().build();
    }


    @POST
    @Path("/returnToVendor")
    @Timed(name = "returnToVendor")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Return To vendor")
    @Audit(action = "Return to Vendor")
    public Response returnToVendor(@Valid VendorReturnRequest vendorReturnRequest) {
        purchaseOrderService.returnToVendorFromPOs(vendorReturnRequest);
        return ok();
    }

    @GET
    @Path("/{poNumber}/poNumber")
    @Timed(name = "getPurchaseOrderByNo")
    @Secured(requiredShop = true)
    @Audit(action = "Get purchase order by po number")
    @ApiOperation(value = "Get purchase order by po number")
    public PurchaseOrderItemResult getPurchaseOrderByNo(@PathParam("poNumber") String poNumber) {
        return purchaseOrderService.getPurchaseOrderByNo(poNumber);
    }

    @GET
    @Path("/{purchaseOrderId}/poShippingManifest")
    @Secured(requiredShop = true)
    @Timed(name = "generatePoShippingManifest")
    @ApiOperation(value = "Create PDF For Purchase Order Shipping Manifest")
    public Response generatePoShippingManifest(@PathParam("purchaseOrderId") String purchaseOrderId) {
        InputStream inputStream = purchaseOrderService.generatePoShippingManifest(purchaseOrderId);
        if (inputStream != null) {
            StreamingOutput streamOutput = new StreamingOutput() {
                @Override
                public void write(OutputStream os) throws IOException,
                        WebApplicationException {
                    ByteStreams.copy(inputStream, os);
                }
            };
            return Response.ok(streamOutput).type("application/pdf").build();
        }
        return Response.ok().build();
    }

    @GET
    @Path("/limited/{status}")
    @Secured(requiredShop = true)
    @Timed(name = "getLimitedPurchaseOrder")
    @ApiOperation("Get limited purchase order details")
    public List<PurchaseOrderLimitedResult> getPoLimitedDetails(@PathParam("status") PurchaseOrder.PurchaseOrderStatus status, @QueryParam("vendorIds") List<String> vendorIds, @QueryParam("productIds") List<String> productIds){
        return purchaseOrderService.getPoLimitedDetails(status, vendorIds, productIds);
    }

    @POST
    @Path("/copyPurchaseOrder/{purchaseOrderId}")
    @Secured(requiredShop = true)
    @Timed(name = "copyPurchaseOrder ")
    @ApiOperation("Create new purchase order from existing")
    public PurchaseOrder copyPurchaseOrder(@PathParam("purchaseOrderId") String purchaseOrderId){
        return purchaseOrderService.copyPurchaseOrder(purchaseOrderId);
    }

}
