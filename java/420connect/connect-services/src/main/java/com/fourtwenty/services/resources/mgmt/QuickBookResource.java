package com.fourtwenty.services.resources.mgmt;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.customer.StatusRequest;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.ErrorLogs;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QBDesktopOperation;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickBookAccountDetails;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickBookEnabledRequest;
import com.fourtwenty.quickbook.models.QWCPassword;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookCustomEntities;
import com.fourtwenty.core.domain.models.thirdparty.quickbook.QuickbookSyncDetails;
import com.fourtwenty.quickbook.desktop.request.QBDesktopOperationRequest;
import com.fourtwenty.quickbook.desktop.response.QuickBookDesktopResponse;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.quickbook.online.helperservices.QuickbookSettings;
import com.fourtwenty.quickbook.online.quickbookservices.QuickbookService;
import com.fourtwenty.core.security.dispensary.Audit;
import com.fourtwenty.core.security.dispensary.BaseResource;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

import static org.eclipse.jetty.server.handler.gzip.GzipHttpOutputInterceptor.LOG;


@Api("QuickBook - Resources")
@Path("/api/v1/quickbookservices")
@Produces(MediaType.APPLICATION_JSON)
public class QuickBookResource extends BaseResource {
    @Inject
    QuickbookService quickbookService;

    @GET
    @Path("/oauth/token")
    @Timed(name = "getAuthCode")
    @Audit(action = "Get Authcode")
    public String getAuthCode() {
        LOG.debug("inside oauth2redirect call");
        return quickbookService.getAuthorizationCode();

    }


    @GET
    @Path("/quickbookredirect")
    @Timed(name = "getAuthCode")
    @Audit(action = "Get Authcode")
    public Object getcallback(@QueryParam("code") String authCode, @QueryParam("state") String shopId, @QueryParam(value = "realmId") String realmId) {
        return quickbookService.getRefreshToken(authCode, shopId, realmId);
    }


    @GET
    @Path("/syncEntity")
    @Timed(name = "SyncEntity")
    @Audit(action = "Sync Quickbook Entities")
    public Object syncQuickbookEntities() {
        return quickbookService.syncCustomQuickbookEntities();
    }


    @POST
    @Path("/quickbookCustomEntities")
    @Timed(name = "SaveEntity")
    @Audit(action = "Save Quickbook Entities")
    public QuickbookCustomEntities saveQuickbookEntities(@Valid QuickbookCustomEntities quickbookCustomEntities) {
        return quickbookService.saveQuickbookCustomEntities(quickbookCustomEntities);
    }


    @GET
    @Path("/disconnect/quickbook")
    @Timed(name = "disconnectQuickbook")
    @Audit(action = "Disconnect  Quickbook Entities")
    public Object disconnectQuickbook(@QueryParam("qbtypes") String qbtypes) {
        return quickbookService.disconnectQuickbook(qbtypes);
    }


    @GET
    @Path("/getQuickbookEntity")
    @Timed(name = "quickbookEntity")
    @Audit(action = "Quickbook  Entity")
    public QuickbookCustomEntities getQuickbookEntity() {
        return quickbookService.getQuickbookEntity();
    }

    @GET
    @Path("/getConfigurations")
    @Timed(name = "quickbookAllDetails")
    @Audit(action = "Quickbook  details")
    public QuickbookSettings getQbDetailResponse() {
        return quickbookService.getAllDetailEntityReponse();

    }

    @POST
    @Path("/getQwcFileURL")
    @Timed(name = "getQwcFileURL")
    @Audit(action = "get QWC file URL")
    public String downloadQwcFile(@Valid QWCPassword qwcPassword) {
        return quickbookService.getQwcFileURL(qwcPassword);

    }

    @GET
    @Path("/getQuickBookOperations")
    @Timed(name = "quickBookOperations")
    @Audit(action = "QuickBook Operations")
    public QuickBookDesktopResponse getQuickBookOperations() {
        return quickbookService.getQuickBookDesktopOperations();
    }


    @PUT
    @Path("/updateQuickBookOperations")
    @Timed(name = "updateQuickBookOperations")
    @Audit(action = "updateQuickBook Operations")
    public List<QBDesktopOperation> updateQuickBookOperations(@Valid List<QBDesktopOperationRequest> request) {
        return quickbookService.updateQuickBookDesktopOperations(request);
    }

    @GET
    @Path("/getQuickBookSyncDetails")
    @Timed(name = "quickBookSyncDetails")
    @Audit(action = "QuickBook Sync Details")
    public SearchResult<QuickbookSyncDetails> getQuickBookSyncDetails(@QueryParam("start") int start, @QueryParam("limit") int limit) {
        return quickbookService.getLimitedQuickBookSyncDetails(start, limit);
    }

    @GET
    @Path("/getQuickBookErrorLogs")
    @Timed(name = "quickBookErrorLogs")
    @Audit(action = "QuickBook Error Logs")
    public SearchResult<ErrorLogs> getQuickBookErrorLogs(@QueryParam("start") int start, @QueryParam("limit") int limit, @QueryParam("reference") String reference) {
        return quickbookService.getLimitedQuickBookErrorLogs(start, limit, reference);
    }

    @POST
    @Path("/updateSyncStatus/{operationId}")
    @Timed(name = "updateSyncStatus")
    @Audit(action = "Update sync status")
    @ApiOperation(value = "Update sync status")
    @Secured
    public QBDesktopOperation updateSyncStatus(@PathParam("operationId") String operationId, @Valid StatusRequest request) {
        return quickbookService.updateSyncStatus(operationId, request);
    }

    @POST
    @Path("/saveQuickBookAccountDetails")
    @Timed(name = "saveQuickBookAccountDetails")
    @Audit(action = "Save QuickBook Account Details")
    @ApiOperation(value = "Save QuickBook Account Details")
    @Secured
    public QuickBookAccountDetails saveQuickBookAccountDetails(@Valid QuickBookAccountDetails quickBookAccountDetails) {
        return quickbookService.saveQuickBookAccountDetails(quickBookAccountDetails);
    }

    @PUT
    @Path("/updateQuickBookAccountDetails/{referenceId}")
    @Timed(name = "updateQuickBookAccountDetails")
    @Audit(action = "Update QuickBook Account Details")
    @ApiOperation(value = "Update QuickBook Account Details")
    @Secured
    public QuickBookAccountDetails updateQuickBookAccountDetails(@PathParam("referenceId") String referenceId, @Valid QuickBookAccountDetails quickBookAccountDetails) {
        return quickbookService.updateQuickBookAccountDetails(referenceId, quickBookAccountDetails);
    }

    @GET
    @Path("/getQuickBookAccountDetails/{referenceId}")
    @Timed(name = "getQuickBookAccountDetails")
    @Audit(action = "Get QuickBook Account Details")
    @ApiOperation(value = "Get QuickBook Account Details")
    @Secured
    public QuickBookAccountDetails getQuickBookAccountDetails(@PathParam("referenceId") String referenceId) {
        return quickbookService.getQuickBookAccountDetailsByReferenceId(referenceId);
    }

    @POST
    @Path("/hardResetQuickBookData")
    @Timed(name = "hardResetQuickBookData")
    @Audit(action = "Hard Reset QuickBook Data")
    @ApiOperation(value = "Hard Reset QuickBook Data")
    @Secured
    public Response hardResetQuickBookData() {
        quickbookService.hardResetQuickBookData();
        return ok();
    }

    @POST
    @Path("/enabledQuickBook")
    @Timed(name = "enabledQuickBook")
    @Audit(action = "QuickBook Enabled")
    @ApiOperation(value = "QuickBook Enabled")
    public String enabledQuickBook(@Valid QuickBookEnabledRequest quickBookEnabledRequest) {
        return quickbookService.getEnabledQuickBook(quickBookEnabledRequest);
    }
}
