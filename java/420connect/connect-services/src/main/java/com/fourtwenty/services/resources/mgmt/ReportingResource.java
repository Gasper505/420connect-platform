package com.fourtwenty.services.resources.mgmt;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.company.ReportTrack;
import com.fourtwenty.core.domain.models.reportrequest.ReportRequest;
import com.fourtwenty.core.reporting.model.Report;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.common.AssetStreamResult;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.mgmt.ReportingService;
import com.fourtwenty.core.services.reportrequest.ReportRequestService;
import com.google.common.io.ByteStreams;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

/**
 * Created by Stephen Schmidt on 3/31/2016.
 */
@Api("Management - Reports")
@Path("/api/v1/mgmt/reporting")
public class ReportingResource extends BaseResource {
    @Inject
    ReportingService reportingService;
    @Inject
    private ReportRequestService reportRequestService;

    @GET
    @Path("/")
    @Timed(name = "reporting")
    @Secured
    @ApiOperation(value = "Get Reports")
    public Response getReport(@QueryParam("type") String reportType,
                              @QueryParam("startDate") String startDate,
                              @QueryParam("endDate") String endDate,
                              @QueryParam("filter") String filter,
                              @QueryParam("format") String format,
                              @QueryParam("section") ReportTrack.ReportSectionType section,
                              @QueryParam("email") String email,
                              @QueryParam("generate") boolean generate) {

        if (StringUtils.isBlank(format)) {
            format = "JSON";
        }

        final Report report = reportingService.getReport(reportType, startDate, endDate, filter, format, section, email,generate);

        if (format.equalsIgnoreCase("CSV")) {
            StreamingOutput streamingOutput = new StreamingOutput() {
                @Override
                public void write(OutputStream output) throws IOException, WebApplicationException {
                    ByteStreams.copy((InputStream) report.getData(), output);
                }
            };
            return Response.ok(streamingOutput, MediaType.APPLICATION_OCTET_STREAM)
                    .header("Content-Disposition", "attachment; filename=\"" + report.getReportName() + ".csv\"").type(report.getContentType()).build();
        } else if (format.equalsIgnoreCase("JSON")) {
            return Response.ok(report.getData(), MediaType.APPLICATION_JSON).build();
        } else if (format.equalsIgnoreCase("PDF")) {
            return null;
        } else {
            return null;
        }
    }

    @GET
    @Path("/frequentReport")
    @Timed(name = "frequentlyUsedReport")
    @Secured
    @ApiOperation(value = "Get count of frequently used report")
    @Produces(MediaType.APPLICATION_JSON)
    public List<ReportTrack> getFrequentlyUsedReport() {
        return reportingService.getFrequentlyUsedReport();
    }

    @GET
    @Path("/{terminalId}/daySummary")
    @Timed(name = "getDailySummaryReceipt")
    @Secured
    @ApiOperation(value = "Get Daily Summary Receipt")
    @Produces(MediaType.TEXT_PLAIN)
    public String getDailySummaryReceipt(@PathParam("terminalId") String terminalId,
                                         @QueryParam("date") String date, @QueryParam("width") int width) {
        return reportingService.getDailySummaryReport(terminalId, date, width);

    }

    @GET
    @Path("/reportrequest/list")
    @Timed(name = "getReportRequestList")
    @ApiOperation(value = "Get Report Request Details")
    @Secured
    @Produces(MediaType.APPLICATION_JSON)
    public SearchResult<ReportRequest> getReportRequestList(@QueryParam("start") int start,
                                                            @QueryParam("limit") int limit) {
        ConnectAuthToken token = getToken();
        if (token != null) {
            return reportRequestService.getReportRequestList(token.getCompanyId(), token.getShopId(), token.getActiveTopUser().getUserId(), start, limit);
        }

        return new SearchResult<>();
    }

    @GET
    @Path("/generated/{requestId}")
    @Timed(name = "downloadGenerateReport")
    @ApiOperation(value = "Download generated report")
    @Secured
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response downloadGenerateReport(@PathParam("requestId") String requestId) {
        ConnectAuthToken token = getToken();
        if (token != null) {
            AssetStreamResult result = reportRequestService.downloadReport(token.getCompanyId(), token.getShopId(), token.getActiveTopUser().getUserId(), requestId);
            if (result != null && result.getStream() != null){
                StreamingOutput streamOutput = new StreamingOutput() {
                    @Override
                    public void write(OutputStream os) throws IOException,
                            WebApplicationException {
                        ByteStreams.copy(result.getStream(), os);
                    }
                };
                return Response.ok(streamOutput, MediaType.APPLICATION_OCTET_STREAM)
                        .header("Content-Disposition", "attachment; filename=\"" + result.getKey()).type(MediaType.APPLICATION_OCTET_STREAM).build();
            }
        }
        return Response.ok().build();
    }


}
