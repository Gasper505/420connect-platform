package com.fourtwenty.services.resources.mgmt;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.company.AppRolePermission;
import com.fourtwenty.core.domain.models.company.Role;
import com.fourtwenty.core.rest.dispensary.requests.company.AppRolePermissionRequest;
import com.fourtwenty.core.rest.dispensary.requests.company.RoleAddRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.company.AppRolePermissionResult;
import com.fourtwenty.core.rest.dispensary.results.company.RolePermissionResult;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.mgmt.RoleService;
import com.google.inject.Inject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * Created by mdo on 6/15/16.
 */
@Api("Management - Roles")
@Path("/api/v1/mgmt/roles")
@Produces(MediaType.APPLICATION_JSON)
public class RoleResource extends BaseResource {
    @Inject
    RoleService roleService;

    @GET
    @Path("/")
    @Secured
    @Timed(name = "getRoles")
    @ApiOperation(value = "Get Roles")
    public SearchResult<Role> getRoles() {
        return roleService.getAllRoles();
    }

    @POST
    @Path("/assignPermission")
    @Secured
    @Timed(name = "assignGranularPermission")
    @ApiOperation(value = "Assign permission by role for app")
    public AppRolePermission assignPermissionByRole(@Valid AppRolePermissionRequest request) {
        return roleService.assignPermissionByRole(request);
    }

    @GET
    @Path("/permissionsByCompany")
    @Secured
    @Timed(name = "getPermissionsByCompany")
    @ApiOperation(value = "Get permission list by company")
    public List<AppRolePermissionResult> getPermissionsByCompany() {
        return roleService.getPermissionsByCompany();
    }

    @GET
    @Path("/permissions")
    @Secured
    @Timed(name = "getAllPermissions")
    @ApiOperation(value = "Get all permissions")
    public List<RolePermissionResult> getAllPermissions() {
        return roleService.getAllPermissions();
    }

    @POST
    @Path("/")
    @Secured
    @Timed(name = "addRole")
    @ApiOperation(value = "Add Role")
    public Role addRole(@Valid RoleAddRequest request) {
        return roleService.addRole(request);
    }

    @PUT
    @Path("/{roleId}/updateRole")
    @Secured
    @Timed(name = "updateRole")
    @ApiOperation(value = "Update Role")
    public Role updateRole(@PathParam("roleId") String roleId,
                           @Valid Role role) {
        return roleService.updateRole(roleId, role);
    }

}
