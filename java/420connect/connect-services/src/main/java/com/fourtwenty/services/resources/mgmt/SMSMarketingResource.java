package com.fourtwenty.services.resources.mgmt;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.marketing.MarketingJob;
import com.fourtwenty.core.rest.dispensary.requests.marketing.MarketingJobAddRequest;
import com.fourtwenty.core.rest.dispensary.requests.marketing.MarketingJobMemberCountRequest;
import com.fourtwenty.core.rest.dispensary.results.MarketingMemberCountResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.marketing.MarketingJobResult;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.mgmt.MarketingJobService;
import com.google.inject.Inject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by Gaurav Saini on 29/5/17.
 */
@Api("Management - SMS")
@Path("/api/v1/mgmt/sms")
@Produces(MediaType.APPLICATION_JSON)
public class SMSMarketingResource extends BaseResource {

    @Inject
    MarketingJobService marketingJobService;

    @GET
    @Path("/completed")
    @Timed(name = "getCompletedJobs")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get In Progress/Completed Marketing Jobs")
    public SearchResult<MarketingJobResult> getCompletedJobs(@QueryParam("start") int start,
                                                             @QueryParam("limit") int limit) {
        return marketingJobService.getCompletedJobs(start, limit);
    }

    @GET
    @Path("/pending")
    @Timed(name = "getPendingJobs")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get Pending Marketing Jobs")
    public SearchResult<MarketingJobResult> getPendingJobs(@QueryParam("start") int start,
                                                           @QueryParam("limit") int limit) {
        return marketingJobService.getPendingJobs(start, limit);
    }


    @POST
    @Path("/")
    @Timed(name = "createMarketingJob")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Create New Marketing Job")
    public MarketingJob createMarketingJob(@Valid MarketingJobAddRequest addRequest) {
        return marketingJobService.createMarketingJob(addRequest);
    }

    @PUT
    @Path("/{marketingJobId}")
    @Timed(name = "updateMarketingJob")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Update New Marketing Job")
    public MarketingJob updateMarketingJob(@PathParam("marketingJobId") String marketingJobId, @Valid MarketingJob marketingJob) {
        return marketingJobService.updateMarketingJob(marketingJobId, marketingJob);
    }

    @DELETE
    @Path("/{marketingJobId}")
    @Timed(name = "removeMarketingJob")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Remove Marketing Job")
    public Response removeMarketingJob(@PathParam("marketingJobId") String marketingJobId) {
        marketingJobService.removeMarketingJob(marketingJobId);
        return ok();
    }

    @POST
    @Path("/memberCount")
    @Timed(name = "getMemberCountForMarketingJob")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get count of member for marketing job")
    public MarketingMemberCountResult getMemberCountForMarketingJob(@Valid MarketingJobMemberCountRequest request) {
        return marketingJobService.getMemberCountForMarketingJob(request);
    }
}


