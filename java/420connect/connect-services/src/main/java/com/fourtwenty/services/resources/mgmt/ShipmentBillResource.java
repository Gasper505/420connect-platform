package com.fourtwenty.services.resources.mgmt;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrder;
import com.fourtwenty.core.domain.models.purchaseorder.ShipmentBill;
import com.fourtwenty.core.rest.common.EmailRequest;
import com.fourtwenty.core.rest.dispensary.requests.inventory.ShipmentBillPaymentStatusRequest;
import com.fourtwenty.core.rest.dispensary.requests.inventory.ShipmentBillUpdateRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.ShipmentBillRequestResult;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.mgmt.ShipmentBillService;
import com.google.common.io.ByteStreams;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

@Api("Management - Shipment Bills")
@Path("/api/v1/mgmt/shipmentBill")
@Produces(MediaType.APPLICATION_JSON)
public class ShipmentBillResource extends BaseResource {
    private ShipmentBillService shipmentBillService;

    @Inject
    public ShipmentBillResource(ShipmentBillService shipmentBillService) {
        this.shipmentBillService = shipmentBillService;
    }

    @GET
    @Path("/")
    @Timed(name = "getAllShipmentBills")
    @Secured(requiredShop = true)
    @ApiOperation(value = "List of shipment bills by company Id")
    public SearchResult<ShipmentBillRequestResult> getAllShipmentBills(@QueryParam("start") int start, @QueryParam("limit") int limit, @QueryParam("customerType") PurchaseOrder.CustomerType customerType) {
        return shipmentBillService.getAllShipmentBills(start, limit, customerType);
    }


    @PUT
    @Path("/{shipmentBillId}")
    @Timed(name = "shipmentBillUpdate")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Update shipment bill information")
    public ShipmentBill shipmentBillUpdate(@PathParam("shipmentBillId") String shipmentBillId, @Valid ShipmentBill shipmentBill) {
        return shipmentBillService.updateShipmentBill(shipmentBillId, shipmentBill);
    }

    @GET
    @Path("/{shipmentBillId}")
    @Timed(name = "getShipmentBillById")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get shipment bill by id")
    public ShipmentBillRequestResult getShipmentBillById(@PathParam("shipmentBillId") String shipmentBillId) {
        return shipmentBillService.getShipmentBillById(shipmentBillId);
    }

    @GET
    @Path("/{purchaseOrderId}/getBillByPurchaseOrder")
    @Timed(name = "getShipmentBillByPoId")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get shipment bill Details with PO")
    public ShipmentBill getShipmentBillByPoId(@PathParam("purchaseOrderId") String poId) {
        return shipmentBillService.getShipmentBillByPoId(poId);
    }

    @PUT
    @Path("/{shipmentBillId}/addPayment")
    @Timed(name = "addShipmentBillPayment")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Add shipment bill payment")
    public ShipmentBill addShipmentBillPayment(@PathParam("shipmentBillId") String shipmentBillId, @Valid ShipmentBillPaymentStatusRequest request) {
        return shipmentBillService.addShipmentBillPayment(shipmentBillId, request);
    }

    @GET
    @Path("/list/archive")
    @Timed(name = "getArchivedShipmentBills")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get list of archived shipment bill")
    public SearchResult<ShipmentBillRequestResult> getArchivedShipmentBills(@QueryParam("start") int start, @QueryParam("limit") int limit) {
        return shipmentBillService.getArchivedShipmentBills(start, limit);
    }

    @POST
    @Path("/{shipmentBillId}/markAsArchived")
    @Timed(name = "archiveShipmentBill")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Mark shipment bill as archived")
    public ShipmentBill archiveShipmentBill(@PathParam("shipmentBillId") String shipmentBillId) {
        return shipmentBillService.archiveShipmentBill(shipmentBillId);
    }

    @POST
    @Path("/{shipmentBillId}/sendToAccounting")
    @Timed(name = "emailShipmentBillToAccounting")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Email shipment bill to accounting")
    public Response emailShipmentBillToAccounting(@PathParam("shipmentBillId") String shipmentBillId, @Valid EmailRequest emailRequest) {
        shipmentBillService.emailShipmentBillToAccounting(shipmentBillId, emailRequest);
        return ok();
    }

    @POST
    @Path("/{shipmentBillId}/updatePayment/{paymentId}")
    @Timed(name = "updateShipmentBillPayment")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Update shipment bill payment")
    public ShipmentBill updateShipmentBillPayment(@PathParam("shipmentBillId") String shipmentBillId, @PathParam("paymentId") String paymentId, @Valid ShipmentBillPaymentStatusRequest request) {
        return shipmentBillService.updateShipmentBillPayment(shipmentBillId, paymentId, request);
    }

    @DELETE
    @Path("/{shipmentBillId}/deletePayment/{paymentId}")
    @Timed(name = "deleteShipmentBillPayment")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Delete shipment bill payment")
    public Response deleteShipmentBillPayment(@PathParam("shipmentBillId") String shipmentbillId, @PathParam("paymentId") String paymentId) {
        shipmentBillService.deleteShipmentBillPayment(shipmentbillId, paymentId);
        return ok();
    }

    @PUT
    @Path("/{shipmentBillId}/status")
    @Timed(name = "closeShipmentBill")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Close shipment bill")
    public Response closeShipmentBill(@PathParam("shipmentBillId") String shipmentBillId) {
        shipmentBillService.closeShipmentBill(shipmentBillId);
        return ok();
    }

    @POST
    @Path("/{shipmentBillId}/receive")
    @Timed(name = "updateReceivedDate")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Update bill received date")
    public ShipmentBill updateReceivedDate(@PathParam("shipmentBillId") String shipmentBillId, @Valid ShipmentBillUpdateRequest request) {
        return shipmentBillService.updateReceivedDate(shipmentBillId, request);
    }

    @POST
    @Path("/{purchaseOrderId}/incomplete")
    @Timed(name = "incomplete")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Mark PO incomplete")
    public Response updatePurchaseIncompleteOrder(@PathParam("purchaseOrderId") String purchaseOrderId) {
        shipmentBillService.updatePurchaseIncompleteOrder(purchaseOrderId);
        return ok();
    }

    @GET
    @Path("/{shipmentBillId}/shipmentBillPDF")
    @Secured(requiredShop = true)
    @Timed(name = "createPdfForShipmentBill")
    @ApiOperation(value = "Create PDF For Shipment Bill")
    public Response createPdfForShipmentBill(@PathParam("shipmentBillId") String shipmentBillId) {
        InputStream inputStream = shipmentBillService.createPdfForShipmentBill(shipmentBillId);
        if (inputStream != null) {
            StreamingOutput streamOutput = new StreamingOutput() {
                @Override
                public void write(OutputStream os) throws IOException,
                        WebApplicationException {
                    ByteStreams.copy(inputStream, os);
                }
            };
            return Response.ok(streamOutput).type("application/pdf").build();
        }
        return Response.ok().build();
    }
}
