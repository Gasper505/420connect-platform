package com.fourtwenty.services.resources.mgmt;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.domain.models.purchaseorder.Shipment;
import com.fourtwenty.core.rest.dispensary.requests.inventory.ShipmentAddRequest;
import com.fourtwenty.core.rest.dispensary.requests.inventory.ShipmentStatusUpdateRequest;
import com.fourtwenty.core.services.mgmt.ShipmentService;

import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.security.dispensary.Audit;
import com.fourtwenty.core.security.dispensary.Secured;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Api("Management - Shipment")
@Path("/api/v1/mgmt/shipment")
@Produces(MediaType.APPLICATION_JSON)
public class ShipmentResource extends BaseResource {

    private ShipmentService shipmentService;

    @Inject
    public ShipmentResource(ShipmentService shipmentService) {
        this.shipmentService = shipmentService;
    }

    @POST
    @Path("/")
    @Secured(requiredShop = true)
    @Timed(name = "addShipment")
    @Audit(action="Create Shipment")
    @ApiOperation(value = "Create Shipment")
    public Shipment addShipment(@Valid ShipmentAddRequest shipmentAddRequest) {
        return shipmentService.addShipment(shipmentAddRequest);
    }

    @GET
    @Path("/{shipmentId}")
    @Secured(requiredShop = true)
    @Timed(name = "getShipmentById")
    @ApiOperation(value = "Get Shipment by id")
    public Shipment getShipmentById(@PathParam("shipmentId") String shipmentId) {
        return shipmentService.getShipmentById(shipmentId);
    }

    @GET
    @Path("/list")
    @Secured(requiredShop = true)
    @Timed(name = "getShipmentList")
    @ApiOperation(value = "List of shipments")
    public SearchResult<Shipment> getAllShipment(@QueryParam("start") int start, @QueryParam("limit") int limit, @QueryParam("term") String searchTerm,@QueryParam("sortOption") Shipment.ShipmentSort sortOption) {
        return shipmentService.getAllShipment(start, limit, searchTerm, sortOption);
    }

    @GET
    @Path("/list/{status}")
    @Secured(requiredShop = true)
    @Timed(name = "getShipmentListByStatus")
    @ApiOperation(value = "List of shipments by status")
    public SearchResult<Shipment> getAllShipmentByStatus(@QueryParam("start") int start, @QueryParam("limit") int limit, @PathParam("status") String status, @QueryParam("term") String searchTerm, @QueryParam("sortOption") Shipment.ShipmentSort sortOption) {
        return shipmentService.getShipmentListByStatus(start, limit, status, searchTerm, sortOption);
    }

    @PUT
    @Path("/{shipmentId}/status")
    @Secured(requiredShop = true)
    @Timed(name = "updateShipmentStatus")
    @ApiOperation(value = "Update Shipment Status")
    public Shipment updateShipmentStatus(@PathParam("shipmentId") String shipmentId, @Valid ShipmentStatusUpdateRequest shipmentStatusUpdateRequest){
        return shipmentService.updateShipmentStatus(shipmentId, shipmentStatusUpdateRequest);
    }

    @GET
    @Path("/listAvailable")
    @Secured(requiredShop = true)
    @Timed(name = "listAvailable")
    @ApiOperation(value = "List Available")
    public List<Shipment> getAvailableShipments() {
        return shipmentService.getAvailableShipments();
    }

}  