package com.fourtwenty.services.resources.mgmt;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.company.SignedContract;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.mgmt.SignedContractService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * Created by Stephen Schmidt on 12/29/2015.
 */
@Api("Management - Member Contract Agreements")
@Path("/api/v1/mgmt/contracts/agreements")
@Produces(MediaType.APPLICATION_JSON)
public class SignedContractResource extends BaseResource {

    @Inject
    SignedContractService signedContractService;


    @GET
    @Path("/")
    @Timed(name = "findSignedContract")
    @Secured
    @ApiOperation(value = "Get Signed Contracts")
    public List<SignedContract> getSignedContracts() {
        return signedContractService.getSignedContracts();
    }

    @GET
    @Path("/{membershipId}")
    @Timed(name = "getSignedContractByMembership")
    @Secured
    @ApiOperation(value = "Get Signed Contract By Member Id")
    public SignedContract getSignedContractByMembership(@PathParam("membershipId") String membershipId,
                                                        @PathParam("active") boolean active) {
        return signedContractService.getSignedContractByMembership(membershipId, active);
    }
}
