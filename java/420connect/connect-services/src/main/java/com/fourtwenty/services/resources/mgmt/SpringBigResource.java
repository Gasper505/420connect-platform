package com.fourtwenty.services.resources.mgmt;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.thirdparty.SpringBigInfo;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.mgmt.SpringBigService;
import com.fourtwenty.core.thirdparty.springbig.models.SpringMemberResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;


@Api("Management - SpringBig")
@Path("/api/v1/mgmt/springbig")
@Produces(MediaType.APPLICATION_JSON)
public class SpringBigResource extends BaseResource {

    @Inject
    private SpringBigService springBigService;

    @GET
    @Path("/{id}")
    @Secured
    @Timed(name = "getSpringInfoById")
    @ApiOperation(value = "Get spring info by id")
    public SpringBigInfo getSpringInfoById(@PathParam("id") String id) {
        return springBigService.getSpringInfoById(id);
    }

    @GET
    @Path("/list")
    @Secured
    @Timed(name = "getSpringBigInfoList")
    @ApiOperation(value = "Get shop's springbig info of current company")
    public List<SpringBigInfo> getSpringBigInfoList() {
        return springBigService.getSpringBigInfoList();
    }

    @POST
    @Path("/create")
    @Secured
    @Timed(name = "createSpringBigInfoByShop")
    @ApiOperation(value = "Create springbig info by shop")
    public SpringBigInfo createSpringBigInfoByShop(SpringBigInfo springBigInfo) {
        return springBigService.createSpringBigInfoByShop(springBigInfo);
    }

    @PUT
    @Path("/update")
    @Secured
    @Timed(name = "updateSpringBigInfoByShop")
    @ApiOperation(value = "Update springbig info by shop")
    public SpringBigInfo updateSpringBigInfoByShop(SpringBigInfo springBigInfo) {
        return springBigService.updateSpringBigInfoByShop(springBigInfo);
    }

    @GET
    @Path("/shop/{shopId}")
    @Secured
    @Timed(name = "getSpringBigInfoByShop")
    @ApiOperation(value = "Get springbig info by shop")
    public SpringBigInfo getSpringBigInfoByShop(@PathParam("shopId") String shopId) {
        return springBigService.getSpringBigInfoByShop(shopId);
    }

    @GET
    @Path("/members/{memberId}")
    @Secured
    @Timed(name = "getSpringBigMember")
    @ApiOperation(value = "Get spring member by id")
    public SpringMemberResponse getSpringBigMember(@PathParam("memberId") String id) {
        return springBigService.getSpringBigMemberById(id);
    }

}
