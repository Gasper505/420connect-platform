package com.fourtwenty.services.resources.mgmt;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.store.StoreWidgetKey;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.dispensary.Audit;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.store.StoreWidgetKeyService;
import com.google.inject.Inject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Created by mdo on 2/14/17.
 */

@Api("Management - Store Widget Keys")
@Path("/api/v1/mgmt/storekeys")
@Produces(MediaType.APPLICATION_JSON)
public class StoreWidgetKeyResource {
    @Inject
    StoreWidgetKeyService keyService;


    @GET
    @Path("/")
    @Secured
    @Timed(name = "getStoreWidgetKeys")
    @ApiOperation(value = "Get Active Store Keys")
    public SearchResult<StoreWidgetKey> getStoreWidgetKeys() {
        return keyService.getStoreKeys();
    }

    @GET
    @Path("/current")
    @Secured
    @Timed(name = "getCurrentStoreKey")
    @ApiOperation(value = "Get Current Active Store Keys")
    public StoreWidgetKey getCurrentStoreKey() {
        return keyService.getCurrentStoreKey();
    }

    @POST
    @Path("/")
    @Secured
    @Timed(name = "createNewStoreKey")
    @Audit(action = "Create New Store Key")
    @ApiOperation(value = "Create New Store Key")
    public StoreWidgetKey createNewStoreKey() {
        return keyService.createStoreKey();
    }

}
