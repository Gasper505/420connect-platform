package com.fourtwenty.services.resources.mgmt;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.loyalty.StorewideSale;
import com.fourtwenty.core.rest.dispensary.requests.promotions.StorewideSaleAddRequest;
import com.fourtwenty.core.rest.dispensary.requests.promotions.StorewideSaleRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.StorewideSaleResult;
import com.fourtwenty.core.security.dispensary.Audit;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.mgmt.StorewideSaleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/**
 * Created on 9/11/19.
 */
@Api("Management - Storewide Sales")
@Path("/api/v1/mgmt/storewideSales")
@Produces(MediaType.APPLICATION_JSON)
public class StorewideSaleResource extends BaseResource {
    @Inject
    StorewideSaleService storewideSaleService;

    // Getters
    @GET
    @Path("/{storewideSaleId}")
    @Secured
    @Timed(name = "getStorewideSaleById")
    @ApiOperation(value = "Get Storewide Sale By Id")
    public StorewideSaleResult getStorewideSaleById(@PathParam("storewideSaleId") String storewideSaleId) {
        return storewideSaleService.getStorewideSaleById(storewideSaleId);
    }

    @GET
    @Path("/")
    @Secured
    @Timed(name = "getStorewideSales")
    @ApiOperation(value = "Get StorewideSales")
    public SearchResult<StorewideSale> getStorewideSales(@QueryParam("start") int start, @QueryParam("limit") int limit) {
        return storewideSaleService.getStorewideSales(start, limit);
    }

    @POST
    @Path("/")
    @Secured
    @Timed(name = "addStorewideSale")
    @Audit(action = "Add StorewideSale")
    @ApiOperation(value = "Add StorewideSales")
    public StorewideSale addStorewideSale(@Valid StorewideSaleAddRequest request) {
        return storewideSaleService.addStorewideSale(request);
    }

    @PUT
    @Path("/{storewideSaleId}")
    @Secured
    @Timed(name = "updateStorewideSale")
    @Audit(action = "Update StorewideSale")
    @ApiOperation(value = "Update StorewideSales")
    public StorewideSaleResult updateStorewideSale(@PathParam("storewideSaleId") String storewideSaleId, @Valid StorewideSaleRequest storewideSaleRequest) {
        return storewideSaleService.updateStorewideSale(storewideSaleId, storewideSaleRequest);
    }

    @DELETE
    @Path("/{storewideSaleId}")
    @Secured
    @Timed(name = "deleteStorewideSale")
    @Audit(action = "Delete StorewideSale")
    @ApiOperation(value = "Delete StorewideSales")
    public void deleteStorewideSale(@PathParam("storewideSaleId") String storewideSaleId) {
        storewideSaleService.deleteStorewideSale(storewideSaleId);
    }
    
    @POST
    @Path("/{storewideSaleId}/enable")
    @Secured
    @Timed(name = "enableStorewideSale")
    @ApiOperation(value = "Enable Storewide Sale")
    public StorewideSaleResult enableStorewideSale(@PathParam("storewideSaleId") String storewideSaleId) {
        return storewideSaleService.enableStorewideSale(storewideSaleId);
    }
    
    @POST
    @Path("/{storewideSaleId}/disable")
    @Secured
    @Timed(name = "disableStorewideSale")
    @ApiOperation(value = "Disable Storewide Sale")
    public StorewideSaleResult disableStorewideSale(@PathParam("storewideSaleId") String storewideSaleId) {
        return storewideSaleService.disableStorewideSale(storewideSaleId);
    }

}
