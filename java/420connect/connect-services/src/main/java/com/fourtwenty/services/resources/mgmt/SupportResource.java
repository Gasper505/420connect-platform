package com.fourtwenty.services.resources.mgmt;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.rest.dispensary.requests.support.SupportSendRequest;
import com.fourtwenty.core.security.dispensary.Audit;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.mgmt.SupportService;
import com.google.inject.Inject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.validation.Valid;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by mdo on 8/10/16.
 */
@Api("Management - Support")
@Path("/api/v1/mgmt/support")
@Produces(MediaType.APPLICATION_JSON)
public class SupportResource extends BaseResource {
    @Inject
    SupportService supportService;

    @POST
    @Path("/")
    @Timed(name = "sendSupportEmail")
    @Secured(required = true)
    @Audit(action = "Send Support Email")
    @ApiOperation(value = "Send Support Email")
    public Response sendSupportEmail(@Valid SupportSendRequest request) {
        supportService.sendSupportEmail(request);
        return Response.ok().build();
    }
}
