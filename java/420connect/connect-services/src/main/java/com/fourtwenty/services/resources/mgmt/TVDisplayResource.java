package com.fourtwenty.services.resources.mgmt;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.plugins.TVDisplay;
import com.fourtwenty.core.rest.dispensary.requests.plugins.TVDisplayAddRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.mgmt.TVDisplayService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by decipher on 14/10/17 2:19 PM
 * Abhishek Samuel (Software Engineer)
 * abhishke.decipher@gmail.com
 * Decipher Zone Softwares
 * www.decipherzone.com
 */
@Api("Management - TV Display")
@Path("/api/v1/mgmt/tv")
@Produces(MediaType.APPLICATION_JSON)
public class TVDisplayResource extends BaseResource {

    private TVDisplayService tvDisplayService;

    @Inject
    public TVDisplayResource(TVDisplayService tvDisplayService) {
        this.tvDisplayService = tvDisplayService;
    }

    @POST
    @Path("/")
    @Secured(requiredShop = true)
    @Timed(name = "addTVDisplay")
    @ApiOperation(value = "Add TV Display")
    public TVDisplay addTVDisplay(@Valid TVDisplayAddRequest tvDisplayAddRequest) {
        return tvDisplayService.addTVDisplay(tvDisplayAddRequest);
    }

    @PUT
    @Path("/{tvDisplayId}")
    @Secured(requiredShop = true)
    @Timed(name = "updateTVDisplay")
    @ApiOperation(value = "Update TV Display")
    public TVDisplay updateTVDisplay(@PathParam("tvDisplayId") String tvDisplayId, @Valid TVDisplay tvDisplay) {
        return tvDisplayService.updateTVDisplay(tvDisplayId, tvDisplay);
    }

    @GET
    @Path("/list")
    @Secured(requiredShop = true)
    @Timed(name = "getAllTVDisplay")
    @ApiOperation(value = "List of TV display by company id")
    public SearchResult<TVDisplay> getAllTVDisplay(@QueryParam("start") int start, @QueryParam("limit") int limit) {
        return tvDisplayService.getAllTVDisplay(start, limit);
    }

    @GET
    @Path("/{tvDisplayId}")
    @Secured(requiredShop = true)
    @Timed(name = "getTVDisplayById")
    @ApiOperation(value = "Get TV Display by id")
    public TVDisplay getTVDisplayById(@PathParam("tvDisplayId") String tvDisplayId) {
        return tvDisplayService.getTVDisplayById(tvDisplayId);
    }

    @DELETE
    @Path("/{tvDisplayId}")
    @Secured(requiredShop = true)
    @Timed(name = "deleteTVDisplay")
    @ApiOperation(value = "Delete TV display")
    public Response deleteTVDisplay(@PathParam("tvDisplayId") String tvDisplayId) {
        tvDisplayService.deleteTVDisplay(tvDisplayId);
        return ok();
    }
}
