package com.fourtwenty.services.resources.mgmt;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.company.Terminal;
import com.fourtwenty.core.rest.dispensary.requests.terminals.TerminalAdminAddRequest;
import com.fourtwenty.core.rest.dispensary.requests.terminals.TerminalUpdateNameRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.dispensary.Audit;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.pos.TerminalService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by mdo on 6/16/16.
 */

@Api("Management - Terminals")
@Path("/api/v1/mgmt/terminals")
@Produces(MediaType.APPLICATION_JSON)
public class TerminalResource extends BaseResource {
    @Inject
    TerminalService terminalService;

    @GET
    @Path("/")
    @Timed(name = "getAvailableTerminals")
    @Secured(required = true)
    @ApiOperation(value = "Get Available Terminals")
    public SearchResult<Terminal> getAvailableTerminals() {
        return terminalService.getAvailableTerminals(getToken().getShopId());
    }

    @GET
    @Path("/{terminalId}/refreshlocation")
    @Timed(name = "getAvailableTerminals")
    @Secured(required = true)
    @Audit(action = "Refresh Terminal Location")
    @ApiOperation(value = "Request Terminal Location Refresh")
    public Response refreshTerminalLocation(@PathParam("terminalId") String terminalId) {
        terminalService.refreshTerminalLocation(terminalId);
        return ok();
    }

    @POST
    @Path("/")
    @Timed(name = "AddTerminal")
    @Secured(required = true)
    @Audit(action = "Create Terminal")
    @ApiOperation(value = "Create Terminal")
    public Terminal addTerminal(@Valid TerminalAdminAddRequest request) {
        return terminalService.addTerminal(getToken().getShopId(), request);
    }

    @POST
    @Path("/{terminalId}")
    @Timed(name = "updateTerminalName")
    @Secured(required = true)
    @Audit(action = "Update Terminal")
    @ApiOperation(value = "Update Terminal")
    public Response updateTerminalName(@PathParam("terminalId") String terminalId, @Valid TerminalUpdateNameRequest request) {
        terminalService.updateTerminalName(getToken().getShopId(), terminalId, request);
        return ok();
    }

    @DELETE
    @Path("/{terminalId}")
    @Timed(name = "deleteTerminal")
    @Secured(required = true)
    @Audit(action = "Delete Terminal")
    @ApiOperation(value = "Delete Terminal")
    public Response deleteTerminal(@PathParam("terminalId") String terminalId) {
        terminalService.deleteTerminal(terminalId);
        return ok();
    }

    @GET
    @Path("/{shopId}/shopTerminal")
    @Timed(name = "getShopAvailableTerminals")
    @Secured(required = true)
    @ApiOperation(value = "Get Available Terminals of Shop")
    public SearchResult<Terminal> getShopAvailableTerminals(@PathParam("shopId") String shopId, @QueryParam("start") int start, @QueryParam("limit") int limit) {
        return terminalService.getShopAvailableTerminals(shopId, start, limit);
    }
}
