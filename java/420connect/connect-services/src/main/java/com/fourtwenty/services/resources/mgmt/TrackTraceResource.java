package com.fourtwenty.services.resources.mgmt;

import com.blaze.clients.metrcs.models.packages.MetricsPackagesList;
import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.thirdparty.MetrcAccountService;
import com.fourtwenty.core.services.thirdparty.models.MetrcVerifiedPackage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


@Api("Management - Metrc Track and Trace")
@Path("/api/v1/mgmt/tracktrace")
@Produces(MediaType.APPLICATION_JSON)
public class TrackTraceResource {
    @Inject
    MetrcAccountService metrcAccountService;

    @GET
    @Path("/metrcs/packages")
    @Timed(name = "getAllPackagesFromMetrcs")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get all Packages from Metrcs")
    public MetricsPackagesList getAllPackagesFromMetrcs() {
        return metrcAccountService.getMetrcPackagesForCurrentShop();
    }

    @GET
    @Path("/metrcs/packages/{packageLabel}")
    @Timed(name = "getMetrcVerifiedPackage")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get Verified Package from Metrcs")
    public MetrcVerifiedPackage getMetrcVerifiedPackage(@PathParam("packageLabel") String packageLabel) {
        return metrcAccountService.getMetrcVerifiedPackage(packageLabel);
    }
}
