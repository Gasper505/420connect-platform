package com.fourtwenty.services.resources.mgmt;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.product.Brand;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.rest.dispensary.requests.inventory.VendorAddRequest;
import com.fourtwenty.core.rest.dispensary.requests.inventory.VendorBulkUpdateRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.VendorResult;
import com.fourtwenty.core.security.dispensary.Audit;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.mgmt.VendorService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created by mdo on 10/11/15.
 */
@Api("Management - Vendors")
@Path("/api/v1/mgmt/vendors")
@Produces(MediaType.APPLICATION_JSON)
public class VendorResource extends BaseResource {
    @Inject
    VendorService vendorService;

    @GET
    @Path("/")
    @Secured
    @Timed(name = "getVendors")
    @ApiOperation(value = "Search Vendors")
    public SearchResult<VendorResult> getVendors(@QueryParam("vendorId") String vendorId, @QueryParam("start") int start, @QueryParam("limit") int limit, @QueryParam("type") Vendor.VendorType vendorType, @QueryParam("term") String term, @QueryParam("companyType") List<Vendor.CompanyType> companyType) {
        return vendorService.getVendors(vendorId, start, limit, vendorType, term, companyType);
    }

    @GET
    @Path("/{vendorId}")
    @Timed(name = "getVendor")
    @Secured
    @ApiOperation(value = "Get Vendor By Id")
    public VendorResult getVendor(@PathParam("vendorId") String vendorId) {
        return vendorService.getVendor(vendorId);
    }

    @POST
    @Path("/")
    @Secured
    @Timed(name = "createVendor")
    @Audit(action = "Create Vendor")
    @ApiOperation(value = "Create Vendor")
    public Vendor createVendor(@Valid VendorAddRequest request) {
        return vendorService.addVendor(request);
    }

    @POST
    @Path("/{vendorId}")
    @Secured
    @Timed(name = "updateVendor")
    @Audit(action = "Update Vendor")
    @ApiOperation(value = "Update Vendor")
    public Vendor updateVendor(@PathParam("vendorId") String vendorId,
                               @Valid Vendor request) {
        return vendorService.updateVendor(vendorId, request);
    }

    @DELETE
    @Path("/{vendorId}")
    @Secured
    @Timed(name = "deleteVendor")
    @Audit(action = "Delete Vendor")
    @ApiOperation(value = "Delete Vendor")
    public Response deleteVendor(@PathParam("vendorId") String vendorId) {
        vendorService.deleteVendor(vendorId);
        return ok();
    }

    @GET
    @Path("/{categoryId}/vendors")
    @Timed(name = "getVendorsList")
    @Secured
    @ApiOperation(value = "Get Vendors By category Id")
    public SearchResult<VendorResult> getVendorListByCategoryId(@PathParam("categoryId") String categoryId) {
        return vendorService.getAllVendorByCategoryId(categoryId);
    }

    @GET
    @Path("/{vendorId}/brands")
    @Timed(name = "getBrandsByVendorId")
    @Secured
    @ApiOperation(value = "Get brand list sale by vendor")
    public SearchResult<Brand> getBrandsByVendorId(@PathParam("vendorId") String vendorId, @QueryParam("start") int start, @QueryParam("limit") int limit, @QueryParam("term") String term) {
        return vendorService.getBrandsByVendorId(vendorId, start, limit, term);
    }

    @PUT
    @Path("/bulkUpdate")
    @Timed(name = "bulkUpdateVendor")
    @Secured
    @ApiOperation(value = "Bulk update for vendors")
    public Response updateBulkDataByVendorId(@Valid VendorBulkUpdateRequest vendorBulkUpdateRequest) {
        vendorService.bulkVendorUpdates(vendorBulkUpdateRequest);
        return ok();
    }

}
