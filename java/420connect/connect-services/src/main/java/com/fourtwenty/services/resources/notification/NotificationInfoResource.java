package com.fourtwenty.services.resources.notification;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.notification.NotificationInfo;
import com.fourtwenty.core.rest.dispensary.requests.notification.NotificationInfoAddRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.dispensary.Audit;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.notification.NotificationInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Api("Management - Notifications")
@Path("/api/v1/mgmt/notification")
@Produces(MediaType.APPLICATION_JSON)
public class NotificationInfoResource extends BaseResource {

    @Inject
    private NotificationInfoService notificationInfoService;

    @POST
    @Path("/")
    @Timed(name = "addNotification")
    @Secured(requiredShop = true)
    @Audit(action = "Add Notification")
    @ApiOperation(value = "Add Notification")
    public NotificationInfo addNotification(@Valid NotificationInfoAddRequest request) {
        return notificationInfoService.addNotification(request);
    }


    @PUT
    @Path("/{notificationId}")
    @Timed(name = "updateNotification")
    @Secured(requiredShop = true)
    @Audit(action = "Update Notification")
    @ApiOperation(value = "Update Notification")
    public NotificationInfo updateNotification(@PathParam("notificationId") String notificationId, @Valid NotificationInfo request) {
        return notificationInfoService.updateNotification(notificationId, request);
    }

    @GET
    @Path("/")
    @Timed(name = "getNotification")
    @Secured(requiredShop = true)
    @Audit(action = "get Notification")
    @ApiOperation(value = "Update Notification")
    public SearchResult<NotificationInfo> getNotification(@QueryParam("notificationId") String notificationId,
                                                          @QueryParam("shopId") String shopId,
                                                          @QueryParam("type") NotificationInfo.NotificationType type,
                                                          @QueryParam("start") int start,
                                                          @QueryParam("limit") int limit) {
        return notificationInfoService.getNotification(notificationId, shopId, type, start, limit);
    }

    @DELETE
    @Path("/{notificationId}")
    @Timed(name = "deleteNotification")
    @Secured(requiredShop = true)
    @Audit(action = "delete Notification")
    @ApiOperation(value = "Delete Notification")
    public Response deleteNotification(@QueryParam("notificationId") String notificationId) {
        notificationInfoService.deleteNotification(notificationId);
        return Response.ok().build();
    }

    @PUT
    @Path("/list")
    @Timed(name = "updateNotifications")
    @Secured(requiredShop = true)
    @Audit(action = "Update Notification List")
    @ApiOperation(value = "Update Notification List")
    public List<NotificationInfo> updateNotificationList(@Valid List<NotificationInfo> notificationInfoList) {
        return notificationInfoService.updateNotificationList(notificationInfoList);
    }

    @GET
    @Path("/list")
    @Timed(name = "getNotifications")
    @Secured(requiredShop = true)
    @Audit(action = "Get Notification List For Shop")
    @ApiOperation(value = "Get Notification List For Shop")
    public List<NotificationInfo> getNotificationInfoList() {
        return notificationInfoService.getNotificationInfoList();
    }


}
