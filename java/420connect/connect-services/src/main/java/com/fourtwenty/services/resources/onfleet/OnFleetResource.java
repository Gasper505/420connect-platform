package com.fourtwenty.services.resources.onfleet;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.customer.StatusRequest;
import com.fourtwenty.core.domain.models.thirdparty.OnFleetErrorLog;
import com.fourtwenty.core.domain.models.thirdparty.OnFleetTeams;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.thirdparty.onfleet.models.OnFleetTask;
import com.fourtwenty.core.thirdparty.onfleet.models.request.*;
import com.fourtwenty.core.thirdparty.onfleet.models.response.*;
import com.fourtwenty.core.thirdparty.onfleet.services.OnFleetErrorLogService;
import com.fourtwenty.core.thirdparty.onfleet.services.OnFleetService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Api("Management - OnFleet")
@Path(OnFleetResource.API_ENDPOINT)
@Produces(MediaType.APPLICATION_JSON)
public class OnFleetResource extends BaseResource {

    public static final String API_ENDPOINT = "/api/v1/mgmt/onfleet";

    @Inject
    private OnFleetErrorLogService onFleetErrorLogService;

    @Inject
    private OnFleetService onFleetService;

    @POST
    @Path("/shop/{shopId}")
    @Secured
    @Timed(name = "updateOnFleetInformation")
    @ApiOperation(value = "Update OnFleet information for shop")
    public UpdateOnFleetShopInfoResult updateOnFleetInformation(@PathParam("shopId") String shopId, @Valid OnFleetAddRequest addRequest) {
        return onFleetService.updateShopOnFleetInformation(shopId, addRequest);
    }

    @GET
    @Path("/authenticate-apiKey")
    @Secured(requiredShop = true)
    @Timed(name = "authenticateApiKey")
    @ApiOperation(value = "Test OnFleet API Key")
    public AuthenticateAPIKeyResult authenticateApiKey(@QueryParam("apiKey") String apiKey) {
        return onFleetService.authenticateApiKey(apiKey);
    }

    @GET
    @Path("/organization")
    @Secured(requiredShop = true)
    @Timed(name = "getOrganizationInfoByApiKey")
    @ApiOperation(value = "Get company info by API key")
    public OrganizationResult getOrganizationInfoByApiKey(@QueryParam("apiKey") String apiKey) {
        return onFleetService.getOrganizationInfoByApiKey(apiKey);
    }

    @GET
    @Path("/hub")
    @Secured(requiredShop = true)
    @Timed(name = "getHubInfoByApiKey")
    @ApiOperation(value = "Get shop info by API key")
    public HubAssignmentResult getHubInfoByApiKey(@QueryParam("apiKey") String apiKey) {
        return onFleetService.getHubInfoByApiKey(apiKey);

    }

    @GET
    @Path("/task/{taskId}")
    @Secured(requiredShop = true)
    @Timed(name = "getTaskById")
    @ApiOperation(value = "Get OnFleet Task")
    public OnFleetTask getTaskById(@PathParam("taskId") String taskId,
                                   @QueryParam("apiKey") String apiKey) {
        return onFleetService.getOnFleetTaskById(apiKey, taskId);
    }

    @GET
    @Path("/task/list")
    @Secured(requiredShop = true)
    @Timed(name = "getAllOnFleetTask")
    @ApiOperation(value = "Get all OnFleet task")
    public SearchResult<OnFleetTransactionResult> getAllOnFleetTask(@QueryParam("shopId") String shopId,
                                                                    @QueryParam("from") long from) {
        return onFleetService.getAllTask(from, shopId);
    }

    @POST
    @Path("/task/{taskId}")
    @Secured(requiredShop = true)
    @Timed(name = "updateTask")
    @ApiOperation(value = "Update OnFleet task")
    public OnFleetTaskResponse updateTask(@QueryParam("apiKey") String apiKey, @PathParam("taskId") String taskId, @Valid OnFleetTask onFleetTask) {
        return onFleetService.updateOnFleetTask(apiKey, taskId, onFleetTask);
    }

    @DELETE
    @Path("/task/{onfleet-taskId}")
    @Secured(requiredShop = true)
    @Timed(name = "updateTask")
    @ApiOperation(value = "Update OnFleet task")
    public Response deleteTask(@QueryParam("apiKey") String apiKey, @PathParam("onfleet-taskId") String onfleetTaskId) {
        return onFleetService.deleteTask(apiKey, onfleetTaskId);
    }

    @GET
    @Path("/worker/list")
    @Secured(requiredShop = true)
    @Timed(name = "getAllWorker")
    @ApiOperation(value = "Get all workers/employee of OnFleet")
    public List<WorkerResult> getAllWorker(@QueryParam("apiKey") String apiKey,
                                           @QueryParam("states") String states) {
        return onFleetService.getAllWorker(apiKey, states);
    }

    @GET
    @Path("/worker/{workerId}")
    @Secured(requiredShop = true)
    @Timed(name = "getWorkerById")
    @ApiOperation(value = "Get worker by id")
    public WorkerResult getWorkerById(@PathParam("workerId") String workerId,
                                      @QueryParam("apiKey") String apiKey) {
        return onFleetService.getWorkerById(workerId, apiKey);
    }

    @POST
    @Path("/employee/{employeeId}")
    @Secured(requiredShop = true)
    @Timed(name = "syncEmployeeAtOnFleet")
    @ApiOperation(value = "Synchronize employee at OnFleet")
    public EmployeeSynchronizeResult syncEmployeeAtOnFleet(@PathParam("employeeId") String employeeId, @Valid SynchronizeEmployeeRequest request) {
        return onFleetService.syncEmployeeAtOnFleet(employeeId, request);
    }

    @POST
    @Path("/employee/{employeeId}/worker")
    @Secured(requiredShop = true)
    @Timed(name = "createWorkerForEmployee")
    @ApiOperation(value = "Create employee at OnFleet")
    public EmployeeSynchronizeResult createWorkerForEmployee(@PathParam("employeeId") String employeeId, @Valid CreateEmployeeRequest employeeRequest) {
        return onFleetService.createWorkerForEmployee(employeeId, employeeRequest);
    }

    @Deprecated
    @GET
    @Path("/teams")
    @Secured(requiredShop = true)
    @Timed(name = "getAllTeams")
    @ApiOperation(value = "Get all teams by an API key")
    public List<TeamResult> getAllTeams(@QueryParam("apiKey") String apiKey) {
        return onFleetService.getAllTeams(apiKey);
    }

    @Deprecated
    @GET
    @Path("/company/teams")
    @Secured(requiredShop = true)
    @Timed(name = "getAllTeamsByCompany")
    @ApiOperation(value = "Get all teams of current company")
    public List<TeamByCompanyResult> getAllTeamsByCompany() {
        return onFleetService.getAllTeamsByCompany();
    }

    @POST
    @Path("/employee/{employeeId}/team")
    @Secured(requiredShop = true)
    @Timed(name = "updateEmployeeOnFleetDetails")
    @ApiOperation(value = "Update employee's OnFleet information")
    public Employee updateEmployeeOnFleetDetails(@PathParam("employeeId") String employeeId, @Valid CreateEmployeeRequest request) {
        return onFleetService.updateEmployeeOnFleetDetails(employeeId, request);
    }

    @POST
    @Path("/employee/{employeeId}/team/remove")
    @Secured(requiredShop = true)
    @Timed(name = "removeWorkerDetailOfEmployee")
    @ApiOperation(value = "Remove worker details of employee by shop")
    public Employee removeWorkerDetailOfEmployee(@PathParam("employeeId") String employeeId, @Valid CreateEmployeeRequest request) {
        return onFleetService.removeWorkerDetailOfEmployee(employeeId, request);
    }

    @POST
    @Path("/webhook")
    @Secured(requiredShop = true)
    @Timed(name = "createOnfleetWebhook")
    @ApiOperation(value = "Create webhook")
    public WebHookResponse createOnfleetWebhook(@Valid CreateWebhookRequest request) {
        return onFleetService.createOnfleetWebhook(request, API_ENDPOINT);
    }

    @GET
    @Path("/webhook/receiver")
    @Timed(name = "handleOnFleetWebhookRequest")
    @ApiOperation(value = "OnFleet webhook reciever")
    public Response handleOnFleetWebhookRequest(@QueryParam(value = "check") String request) {
        onFleetService.handleOnFleetWebHookRequest(request, null);
        return Response.ok().entity(request).build();
    }


    @POST
    @Path("/webhook/receiver")
    @Timed(name = "handleOnFleetWebHookRequestReceiver")
    @ApiOperation(value = "OnFleet webhook receiver")
    public Response handleOnFleetWebHookRequestReceiver(@QueryParam(value = "check") String request, WebHookRequest data) {
        onFleetService.handleOnFleetWebHookRequest("POST : " + request, null);
        return Response.ok().entity(request).build();
    }

    @GET
    @Path("/webhook/receiver/taskArrival")
    @Timed(name = "handleOnFleetWebHookTaskArrival")
    @ApiOperation(value = "Task arrival OnFleet receiver")
    public Response handleOnFleetWebHookTaskArrival(@QueryParam(value = "check") String request) {
        return Response.ok().entity(request).build();
    }


    @POST
    @Path("/webhook/receiver/taskArrival")
    @Timed(name = "handleOnFleetWebHookTaskArrivalReceiver")
    @ApiOperation(value = "Task arrival OnFleet receiver")
    public Response handleOnFleetWebHookTaskArrivalReceiver(@QueryParam(value = "check") String request, WebHookRequest data) {
        onFleetService.handleOnFleetWebHookTaskArrival("POST : " + request, data);
        return Response.ok().entity(request).build();
    }

    @GET
    @Path("/webhook/receiver/taskCompleted")
    @Timed(name = "handleOnFleetWebHookTaskCompleted")
    @ApiOperation(value = "Task arrival OnFleet completed")
    public Response handleOnFleetWebHookTaskCompleted(@QueryParam(value = "check") String request) {
        return Response.ok().entity(request).build();
    }


    @POST
    @Path("/webhook/receiver/taskCompleted")
    @Timed(name = "handleOnFleetWebHookTaskCompletedReceiver")
    @ApiOperation(value = "Task arrival OnFleet completed")
    public Response handleOnFleetWebHookTaskCompletedReceiver(@QueryParam(value = "check") String request, WebHookRequest data) {
        onFleetService.handleOnFleetWebHookTaskCompleted("POST : " + request, data);
        return Response.ok().entity(request).build();
    }

    @GET
    @Path("/webhook/receiver/taskFailed")
    @Timed(name = "handleOnFleetWebHookTaskFailed")
    @ApiOperation(value = "Task arrival OnFleet failed")
    public Response handleOnFleetWebHookTaskFailed(@QueryParam(value = "check") String request) {
        return Response.ok().entity(request).build();
    }


    @POST
    @Path("/webhook/receiver/taskFailed")
    @Timed(name = "handleOnFleetWebHookTaskFailedReceiver")
    @ApiOperation(value = "Task arrival OnFleet failed")
    public Response handleOnFleetWebHookTaskFailedReceiver(@QueryParam(value = "check") String request, WebHookRequest data) {
        onFleetService.handleOnFleetWebHookTaskFailed("POST : " + request, data);
        return Response.ok().entity(request).build();
    }

    @POST
    @Path("/synchronizeTeam")
    @Secured(requiredShop = true)
    @Timed(name = "synchronizeTeamForShop")
    @ApiOperation(value = "Synchronize team for shop")
    public Response synchronizeTeamForShop(@Valid SynchronizeTeamRequest request) {
        onFleetService.synchronizeTeamForShop(request);
        return ok();
    }

    @GET
    @Path("/onFleetTeamsByShop")
    @Secured(requiredShop = true)
    @Timed(name = "getOnFleetTeamListByShop")
    @ApiOperation(value = "Get synchronized team list by shop")
    public OnFleetTeams getOnFleetTeamListByShop(@QueryParam("shopId") String shopId) {
        return onFleetService.getOnFleetTeamListByShop(shopId);
    }

    @GET
    @Path("/onFleetTeamsByCompany")
    @Secured(requiredShop = true)
    @Timed(name = "getOnFleetTeamListByCompany")
    @ApiOperation(value = "Get synchronized team list by company")
    public List<OnFleetTeamResult> getOnFleetTeamListByCompany(@QueryParam("companyId") String companyId) {
        return onFleetService.getOnFleetTeamListByCompany(companyId);
    }

    @GET
    @Path("/company/onFleetErrorLog")
    @Secured(requiredShop = true)
    @Timed(name = "getOnFleetErrorLogByCompany")
    @ApiOperation(value = "Get onFleet error log by company")
    public SearchResult<OnFleetErrorLog> getOnFleetErrorLogByCompany(@QueryParam("start") int start,
                                                                     @QueryParam("limit") int limit) {
        return onFleetErrorLogService.getOnFleetErrorLogByCompany(start, limit);
    }

    @GET
    @Path("/shop/onFleetErrorLog")
    @Secured(requiredShop = true)
    @Timed(name = "getOnFleetErrorLogByShop")
    @ApiOperation(value = "Get onFleet error log by shop")
    public SearchResult<OnFleetErrorLog> getOnFleetErrorLogByShop(@QueryParam("start") int start,
                                                                  @QueryParam("limit") int limit,
                                                                  @QueryParam("shopId") String shopId) {
        return onFleetErrorLogService.getOnFleetErrorLogByShop(start, limit, shopId);
    }

    @POST
    @Path("/transaction/{transactionId}")
    @Secured(requiredShop = true)
    @Timed(name = "completeTransactionById")
    @ApiOperation(value = "Complete transaction by id")
    public Response completeTransactionById(@PathParam("transactionId") String transactionId, @Valid StatusRequest request) {
        onFleetService.completeTransactionById(transactionId, request);
        return ok();
    }

}
