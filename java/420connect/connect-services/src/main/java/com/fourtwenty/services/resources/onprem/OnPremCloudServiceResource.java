package com.fourtwenty.services.resources.onprem;


import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.rest.onprem.DownloadRequest;
import com.fourtwenty.core.rest.onprem.OnPremUploadRequest;
import com.fourtwenty.core.security.onprem.OnPremBaseResource;
import com.fourtwenty.core.security.onprem.OnPremSecured;
import com.fourtwenty.core.security.tokens.OnPremToken;
import com.fourtwenty.core.services.onprem.OnPremCloudSyncService;
import com.google.inject.Provider;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Api("On-Prem & On-Cloud Sync")
@Path("/api/v1/onprem")
@Produces(MediaType.APPLICATION_JSON)
public class OnPremCloudServiceResource extends OnPremBaseResource {

    @Inject
    private OnPremCloudSyncService onPremCloudSyncService;

    @Inject
    public OnPremCloudServiceResource(Provider<OnPremToken> tokenProvider) {
        super(tokenProvider);
    }

    @GET
    @Path("/download/{entityName}")
    @ApiOperation(value = "Api to download data per entity")
    @OnPremSecured
    @Timed(name = "download")
    public Response download(@PathParam("entityName") String entityName, @QueryParam("shopId") String shopId,
                             @QueryParam("afterDate") long afterDate, @QueryParam("beforeDate") long beforeDate,
                             @QueryParam("isForcefulDownload") boolean isForcefulDownload) {
        return Response.ok()
                .entity(onPremCloudSyncService.sendEntityData(new DownloadRequest(afterDate, beforeDate, isForcefulDownload, entityName, shopId)))
                .build();
    }

    @GET
    @Path("/downloadIndexes/{entityName}")
    @ApiOperation(value = "Api to download indexes from db")
    @OnPremSecured
    @Timed(name = "download")
    public Response downloadIndexes(@PathParam("entityName") String entityName, @QueryParam("shopId") String shopId,
                                    @QueryParam("afterDate") long afterDate, @QueryParam("beforeDate") long beforeDate,
                                    @QueryParam("isForcefulDownload") boolean isForcefulDownload) {
        return Response.ok()
                .entity(onPremCloudSyncService.sendEntityIndexes(new DownloadRequest(afterDate, beforeDate, isForcefulDownload, entityName, shopId)))
                .build();
    }


    @POST
    @Path("/upload/{entityName}")
    @ApiOperation(value = "Api to upload changes to cloud")
    @OnPremSecured
    @Timed(name = "download")
    public Response upload(@PathParam("entityName") String entityName, OnPremUploadRequest uploadRequest) {
        onPremCloudSyncService.uploadOnPremChanges(entityName,uploadRequest);
        return Response.ok().build();
    }

}
