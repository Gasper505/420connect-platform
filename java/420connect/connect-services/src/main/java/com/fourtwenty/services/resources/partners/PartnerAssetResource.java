package com.fourtwenty.services.resources.partners;

import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.rest.dispensary.results.common.AssetStreamResult;
import com.fourtwenty.core.security.dispensary.Audit;
import com.fourtwenty.core.security.partner.BasePartnerDevResource;
import com.fourtwenty.core.security.partner.PartnerDeveloperSecured;
import com.fourtwenty.core.security.store.StoreSecured;
import com.fourtwenty.core.security.tokens.PartnerAuthToken;
import com.fourtwenty.core.services.partners.PartnerAssetService;
import com.google.common.io.ByteStreams;
import com.google.inject.Provider;
import io.dropwizard.jersey.caching.CacheControl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataParam;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.concurrent.TimeUnit;

@Api("Blaze Partner Store - Asset")
@Path("/api/v1/partner/asset")
@Produces(MediaType.APPLICATION_JSON)
@StoreSecured
public class PartnerAssetResource extends BasePartnerDevResource {

    @Inject
    private PartnerAssetService assetService;

    @Inject
    public PartnerAssetResource(Provider<PartnerAuthToken> tokenProvider) {
        super(tokenProvider);
    }

    @POST
    @Path("/upload/private")
    @PartnerDeveloperSecured(userRequired = false)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Audit(action = "Upload Document", json = false)
    @ApiOperation(value = "Upload Document")
    public CompanyAsset uploadDocument(@FormDataParam("file") InputStream inputStream,
                                       @FormDataParam("name") String name,
                                       @FormDataParam("assetType") CompanyAsset.AssetType assetType,
                                       @FormDataParam("file") final FormDataBodyPart body) {
        if (body == null) {
            throw new BlazeInvalidArgException("Asset", "No asset found.");
        }
        String mimeType = body.getMediaType().toString();
        return assetService.uploadAssetPrivate(inputStream, name, assetType, mimeType);
    }

    @GET
    @Path("/{assetKey}")
    @CacheControl(maxAge = 30, maxAgeUnit = TimeUnit.DAYS)
    @Produces({"image/jpeg", "application/pdf", "image/png"})
    @ApiOperation(value = "Get Asset")
    public Response getAssetStream(@PathParam("assetKey") String assetKey, @QueryParam("handleError") boolean handleError) {
        final AssetStreamResult result = assetService.getAssetStream(assetKey, handleError);
        StreamingOutput streamOutput = new StreamingOutput() {
            @Override
            public void write(OutputStream os) throws IOException,
                    WebApplicationException {
                ByteStreams.copy(result.getStream(), os);
            }
        };

        Response.ResponseBuilder responseBuilder = Response.ok(streamOutput).type(result.getContentType());
        return responseBuilder.build();
    }
}
