package com.fourtwenty.services.resources.partners;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.product.BatchQuantity;
import com.fourtwenty.core.domain.models.product.InventoryTransferHistory;
import com.fourtwenty.core.domain.models.product.ProductBatch;
import com.fourtwenty.core.rest.dispensary.requests.partners.PartnerInventoryTransferHistory;
import com.fourtwenty.core.rest.dispensary.requests.partners.PartnerInventoryTransferStatusRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.partner.BasePartnerDevResource;
import com.fourtwenty.core.security.partner.PartnerDeveloperSecured;
import com.fourtwenty.core.security.store.StoreSecured;
import com.fourtwenty.core.security.tokens.PartnerAuthToken;
import com.fourtwenty.core.services.store.StoreInventoryService;
import com.google.inject.Provider;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Api("Blaze Partner Store - Product Resource")
@Path("/api/v1/partner/store/batches")
@Produces(MediaType.APPLICATION_JSON)
@StoreSecured
public class PartnerBatchResource extends BasePartnerDevResource {

    @Inject
    private StoreInventoryService storeInventoryService;

    @Inject
    public PartnerBatchResource(Provider<PartnerAuthToken> tokenProvider) {
        super(tokenProvider);
    }

    @GET
    @Path("/")
    @Timed(name = "getBatchesByProduct")
    @ApiOperation(value = "Get Batches")
    @PartnerDeveloperSecured(userRequired = false, storeRequired = false)
    public SearchResult<ProductBatch> getBatches(@QueryParam("batchId") String batchId,
                                                 @QueryParam("productId") String productId,
                                                 @QueryParam("start") int start,
                                                 @QueryParam("limit") int limit,
                                                 @QueryParam("term") String term,
                                                 @QueryParam("status") ProductBatch.BatchStatus status) {
        return storeInventoryService.getAllBatches(batchId, productId, status, start, limit, term);
    }

    @GET
    @Path("/dates")
    @Timed(name = "getBatchesByDate")
    @ApiOperation(value = "Get Batches By Dates")
    @PartnerDeveloperSecured(userRequired = false, storeRequired = false)
    public SearchResult<ProductBatch> getBatchesByDate(
            @QueryParam("startDate") long startDate,
            @QueryParam("endDate") long endDate,
            @QueryParam("start") int start,
            @QueryParam("limit") int limit) {
        return storeInventoryService.getAllBatches(startDate,endDate,start,limit);
    }

    @GET
    @Path("/{productId}/batchQuantityInfo")
    @Timed(name = "getBatchQuantityByProduct")
    @ApiOperation(value = "Get Batch Quantity By Product")
    @PartnerDeveloperSecured(userRequired = false, storeRequired = false)
    public List<BatchQuantity> getBatchQuantity(@PathParam("productId") String productId) {
        return storeInventoryService.getQuantityInfo(productId);
    }

    @POST
    @Path("/transferInventory")
    @Timed(name = "transferInventory")
    @ApiOperation(value = "Inventory transfer For Partner")
    @PartnerDeveloperSecured(userRequired = false, storeRequired = false)
    public InventoryTransferHistory inventoryTransferHistory(@Valid PartnerInventoryTransferHistory transferHistory) {
        return storeInventoryService.addTransferHistory(transferHistory);
    }

    @PUT
    @Path("/transferInventory/{historyId}")
    @Timed(name = "transferInventory")
    @ApiOperation(value = "Inventory transfer For Partner")
    @PartnerDeveloperSecured(userRequired = false)
    public InventoryTransferHistory updateInventoryTransferHistory(@PathParam("historyId") String historyId, @Valid PartnerInventoryTransferHistory transferHistory) {
        return storeInventoryService.updateTransferHistory(historyId, transferHistory);
    }

    @POST
    @Path("/transferInventory/{historyId}/accept")
    @Timed(name = "acceptInventoryTransferStatus")
    @ApiOperation(value = "Accept inventory transfer history's status for partner")
    @PartnerDeveloperSecured(userRequired = false, storeRequired = false)
    public InventoryTransferHistory acceptInventoryTransferStatus(@PathParam("historyId") String historyId, @Valid PartnerInventoryTransferStatusRequest statusRequest) {
        return storeInventoryService.updateInventoryTransferStatus(historyId, statusRequest, true);
    }

    @POST
    @Path("/transferInventory/{historyId}/decline")
    @Timed(name = "declineInventoryTransferStatus")
    @ApiOperation(value = "Decline inventory transfer history's status for partner")
    @PartnerDeveloperSecured(userRequired = false, storeRequired = false)
    public InventoryTransferHistory declineInventoryTransferStatus(@PathParam("historyId") String historyId, @Valid PartnerInventoryTransferStatusRequest statusRequest) {
        return storeInventoryService.updateInventoryTransferStatus(historyId, statusRequest, false);
    }
}
