package com.fourtwenty.services.resources.partners;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.company.CompanyContact;
import com.fourtwenty.core.rest.dispensary.requests.partners.PartnerCompanyContactRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.company.CompanyContactResult;
import com.fourtwenty.core.security.dispensary.Audit;
import com.fourtwenty.core.security.partner.BasePartnerDevResource;
import com.fourtwenty.core.security.partner.PartnerDeveloperSecured;
import com.fourtwenty.core.security.store.StoreSecured;
import com.fourtwenty.core.security.tokens.PartnerAuthToken;
import com.fourtwenty.core.services.partners.PartnerCompanyContactService;
import com.google.inject.Inject;
import com.google.inject.Provider;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.validation.Valid;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Api("Blaze Partner Company Contact")
@Path("/api/v1/partner/companycontacts")
@Produces(MediaType.APPLICATION_JSON)
@StoreSecured
public class PartnerCompanyContactResource extends BasePartnerDevResource {

    @Inject
    private PartnerCompanyContactService partnerCompanyContactService;

    @Inject
    public PartnerCompanyContactResource(Provider<PartnerAuthToken> tokenProvider) {
        super(tokenProvider);
    }

    @POST
    @Path("/contact")
    @Timed(name = "createCompanyContact")
    @ApiOperation(value = "Create company contact")
    @Audit(action = "Create company contact")
    @PartnerDeveloperSecured(userRequired = false, storeRequired = false)
    public CompanyContact createCompanyContact(@Valid PartnerCompanyContactRequest companyContact) {
        return partnerCompanyContactService.createCompanyContact(companyContact);
    }

    @PUT
    @Path("/contact/{contactId}")
    @Timed(name = "updateCompanyContact")
    @ApiOperation(value = "Update company contact by id")
    @Audit(action = "Update company contact by id")
    @PartnerDeveloperSecured(userRequired = false, storeRequired = false)
    public CompanyContact updateCompanyContact(@PathParam("contactId") String contactId, @Valid PartnerCompanyContactRequest companyContact) {
        return partnerCompanyContactService.updateCompanyContact(contactId, companyContact);
    }

    @GET
    @Path("/contact/{contactId}")
    @Timed(name = "getCompanyContactById")
    @ApiOperation(value = "Get company contact by id")
    @Audit(action = "Get company contact by id")
    @PartnerDeveloperSecured(userRequired = false, storeRequired = false)
    public CompanyContactResult getCompanyContactById(@PathParam("contactId") String contactId) {
        return partnerCompanyContactService.getCompanyContactById(contactId);
    }

    @GET
    @Path("/")
    @Timed(name = "getVendors")
    @Audit(action = "Search vendor")
    @ApiOperation(value = "Search vendors")
    @PartnerDeveloperSecured(userRequired = false, storeRequired = false)
    public SearchResult<CompanyContact> getCompanyContacts(@QueryParam("startDate") String startDate,
                                                   @QueryParam("endDate") String endDate,
                                                   @QueryParam("skip") int skip,
                                                   @QueryParam("limit") int limit) {
        return partnerCompanyContactService.getCompanyContacts(startDate, endDate, skip, limit);
    }

}
