package com.fourtwenty.services.resources.partners;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.consumer.ConsumerUser;
import com.fourtwenty.core.rest.consumer.requests.ConsumerCreateRequest;
import com.fourtwenty.core.rest.consumer.requests.ConsumerLoginRequest;
import com.fourtwenty.core.rest.consumer.results.ConsumerAuthResult;
import com.fourtwenty.core.security.partner.BasePartnerDevResource;
import com.fourtwenty.core.security.partner.PartnerDeveloperSecured;
import com.fourtwenty.core.security.tokens.PartnerAuthToken;
import com.fourtwenty.core.services.store.ConsumerUserService;
import com.google.inject.Provider;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

/**
 * Created by mdo on 12/19/16.
 */
@Api("Blaze PARTNER Store - Consumer Authentication")
@Path("/api/v1/partner/user")
@Produces(MediaType.APPLICATION_JSON)
@PartnerDeveloperSecured
public class PartnerConsumerAuthResource extends BasePartnerDevResource {

    @Inject
    ConsumerUserService service;

    @Inject
    public PartnerConsumerAuthResource(Provider<PartnerAuthToken> tokenProvider) {
        super(tokenProvider);
    }


    @GET
    @Path("/{cuid}")
    @Timed(name = "getUserById")
    @ApiOperation(value = "Get User By Id")
    @PartnerDeveloperSecured
    public ConsumerUser getUserById(@PathParam("cuid") String cuid) {
        return service.getUserById(cuid);
    }

    @GET
    @Path("/find")
    @Timed(name = "findUser")
    @ApiOperation(value = "Find User")
    @PartnerDeveloperSecured
    public ConsumerUser findUser(@QueryParam("email") String email,
                                 @QueryParam("phoneNumber") String phoneNumber,
                                 @QueryParam("country") String country) {
        return service.findUser(email, phoneNumber, country);
    }

    @POST
    @Path("/register")
    @Timed(name = "registerConsumer")
    @ApiOperation(value = "Register Consumer")
    @PartnerDeveloperSecured
    public ConsumerUser registerConsumer(@Valid ConsumerCreateRequest request) {
        ConsumerUser result = service.createNewUser(request);
        return result;
    }

    @POST
    @Path("/login")
    @Timed(name = "loginConsumer")
    @ApiOperation(value = "Consumer Login")
    @PartnerDeveloperSecured
    public ConsumerAuthResult loginConsumer(@Valid ConsumerLoginRequest request, @Context HttpServletResponse response) {
        ConsumerAuthResult result = service.login(request);
        response.setHeader("X-Auth-Token", result.getAccessToken());
        return result;
    }

}
