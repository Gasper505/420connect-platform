package com.fourtwenty.services.resources.partners;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.store.ConsumerCart;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.shop.ConsumerCartResult;
import com.fourtwenty.core.rest.store.requests.ProductCostRequest;
import com.fourtwenty.core.rest.store.results.ProductCostResult;
import com.fourtwenty.core.security.partner.BasePartnerDevResource;
import com.fourtwenty.core.security.partner.PartnerDeveloperSecured;
import com.fourtwenty.core.security.tokens.PartnerAuthToken;
import com.fourtwenty.core.services.store.ConsumerCartService;
import com.google.inject.Provider;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/**
 * Created by mdo on 5/10/17.
 */
@Api("Blaze PARTNER  Store - Store Cart")
@Path("/api/v1/partner/store/cart")
@Produces(MediaType.APPLICATION_JSON)
public class PartnerConsumerCartResource extends BasePartnerDevResource {

    @Inject
    ConsumerCartService consumerCartService;

    @Inject
    public PartnerConsumerCartResource(Provider<PartnerAuthToken> tokenProvider) {
        super(tokenProvider);
    }

    @GET
    @Path("/history")
    @Timed(name = "getConsumerOrderHistory")
    @ApiOperation(value = "Get Consumer Order History")
    @PartnerDeveloperSecured(userRequired = true)
    public SearchResult<ConsumerCart> getConsumerOrderHistory(@QueryParam("cuid") String cuid,
                                                              @QueryParam("start") int start,
                                                              @QueryParam("limit") int limit) {

        return consumerCartService.getOrderHistory(start, limit);
    }

    @GET
    @Path("/{consumerCartId}")
    @Timed(name = "getCartById")
    @ApiOperation(value = "Get Cart by id.")
    @PartnerDeveloperSecured(userRequired = true)
    public ConsumerCart getCartById(@PathParam("consumerCartId") String consumerCartId, @QueryParam("cuid") String cuid) {
        return consumerCartService.getCartById(consumerCartId);
    }


    @GET
    @Path("/active")
    @Timed(name = "getActiveCart")
    @ApiOperation(value = "Get Active Cart")
    @PartnerDeveloperSecured(userRequired = false)
    public ConsumerCart getActiveCart(@QueryParam("cuid") String cuid,
                                      @QueryParam("sessionId") String sessionId) {
        return consumerCartService.getCurrentActiveCart(sessionId);
    }


    @POST
    @Path("/prepare")
    @Timed(name = "prepareActiveCart")
    @ApiOperation(value = "Prepare Active Cart")
    @PartnerDeveloperSecured(userRequired = false)
    public ConsumerCart prepareActiveCart(@QueryParam("cuid") String cuid,
                                          @Valid ConsumerCart consumerCart) {
        return consumerCartService.prepareCart(consumerCart);
    }

    @POST
    @Path("/updateCart/{consumerCartId}")
    @Timed(name = "updateCart")
    @ApiOperation(value = "Update Cart to the platform.")
    @PartnerDeveloperSecured(userRequired = false)
    public ConsumerCart updateCart(@PathParam("consumerCartId") String consumerCartId, @Valid ConsumerCart consumerCart, @QueryParam("cuid") String cuid) {
        return consumerCartService.updateCart(consumerCartId, consumerCart);
    }

    @DELETE
    @Path("/cancelCart/{consumerCartId}")
    @Timed(name = "cancel submitted order")
    @ApiOperation(value = "Cancel submitted order to the platform.")
    @PartnerDeveloperSecured(userRequired = true)
    public ConsumerCart cancelOrder(@PathParam("consumerCartId") String consumerCartId) {
        return consumerCartService.cancelCart(consumerCartId);
    }

    @POST
    @Path("/submitCart/{consumerCartId}")
    @Timed(name = "submitCart")
    @ApiOperation(value = "Submit Cart to be completed.")
    @PartnerDeveloperSecured(userRequired = true)
    public ConsumerCart submitCart(@PathParam("consumerCartId") String consumerCartId, @Valid ConsumerCart consumerCart, @QueryParam("cuid") String cuid) {
        return consumerCartService.submitCart(consumerCartId, consumerCart, ConsumerCart.TransactionSource.Widget);
    }

    @POST
    @Path("/product/findCost")
    @Timed(name = "findProductCost")
    @ApiOperation(value = "Find Product cost")
    @PartnerDeveloperSecured(userRequired = false)
    public ProductCostResult findProductCost(@Valid ProductCostRequest request, @QueryParam("cuid") String cuid) {
        return consumerCartService.findCost(request);
    }

    @GET
    @Path("/public/{publicKey}")
    @Timed(name = "findCartByPublicKey")
    @ApiOperation(value = "Find Cart by public key")
    @PartnerDeveloperSecured(userRequired = false)
    public ConsumerCartResult findCartByPublicKey(@PathParam("publicKey") String publicKey) {
        return consumerCartService.getCartByPublicKey(publicKey);
    }

    @POST
    @Path("/item/{orderItemId}/finalize")
    @Timed(name = "finalizeItemCart")
    @ApiOperation(value = "Finalize Item Cart")
    @PartnerDeveloperSecured(userRequired = false)
    public ConsumerCart finalizeItemCart(@QueryParam("cuid") String cuid, @PathParam("orderItemId") String orderItemId,ConsumerCart consumerCart) {
        return consumerCartService.finalizeItemCart(orderItemId, consumerCart);
    }

    @POST
    @Path("/item/{orderItemId}/unfinalize")
    @Timed(name = "unfinalizeItemCart")
    @ApiOperation(value = "Unfinalize Item Cart")
    @PartnerDeveloperSecured(userRequired = false)
    public ConsumerCart unfinalizeItemCart(@QueryParam("cuid") String cuid, @PathParam("orderItemId") String orderItemId, ConsumerCart consumerCart) {
        return consumerCartService.unfinalizeItemCart(orderItemId, consumerCart);
    }

    @POST
    @Path("/items/finalize")
    @Timed(name = "finalizeAllOrderItems")
    @ApiOperation(value = "Finalize All Items in Cart")
    @PartnerDeveloperSecured(userRequired = false)
    public ConsumerCart finalizeAllOrderItems(@QueryParam("cuid") String cuid, ConsumerCart consumerCart) {
        return consumerCartService.finalizeAllOrderItems(consumerCart);
    }
}
