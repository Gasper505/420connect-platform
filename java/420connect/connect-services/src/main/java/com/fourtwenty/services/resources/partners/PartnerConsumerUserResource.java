package com.fourtwenty.services.resources.partners;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.consumer.ConsumerUser;
import com.fourtwenty.core.domain.models.thirdparty.PatientVerificationStatus;
import com.fourtwenty.core.rest.dispensary.results.common.AssetStreamResult;
import com.fourtwenty.core.rest.store.requests.ContractSignRequest;
import com.fourtwenty.core.security.partner.BasePartnerDevResource;
import com.fourtwenty.core.security.partner.PartnerDeveloperSecured;
import com.fourtwenty.core.security.tokens.PartnerAuthToken;
import com.fourtwenty.core.services.store.ConsumerUserService;
import com.fourtwenty.core.verificationsite.WebsiteManager;
import com.google.common.io.ByteStreams;
import com.google.inject.Provider;
import io.dropwizard.jersey.caching.CacheControl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.sf.json.JSONObject;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataParam;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.concurrent.TimeUnit;

/**
 * Created by mdo on 5/17/17.
 */
@Api("Blaze PARTNER Store - Consumer User")
@Path("/api/v1/partner/store/user")
@Produces(MediaType.APPLICATION_JSON)
public class PartnerConsumerUserResource extends BasePartnerDevResource {
    @Inject
    ConsumerUserService consumerUserService;
    @com.google.inject.Inject
    PatientVerificationStatus patientVerificationStatus;

    @com.google.inject.Inject
    WebsiteManager websiteManager;

    @Inject
    public PartnerConsumerUserResource(Provider<PartnerAuthToken> tokenProvider) {
        super(tokenProvider);
    }

    @GET
    @Path("/")
    @Timed(name = "getCurrentActiveUser")
    @ApiOperation(value = "Get Active User Info")
    @PartnerDeveloperSecured(userRequired = true)
    public ConsumerUser getCurrentActiveUser(@QueryParam("cuid") String cuid) {
        return consumerUserService.getCurrentUser();
    }

    @GET
    @Path("/verify")
    @Timed(name = "verifyPatient")
    @PartnerDeveloperSecured(userRequired = false)
    @ApiOperation(value = "store - verify patient rec code")
    public PatientVerificationStatus verifyPatient(@QueryParam("recNo") String verificationCode,
                                                   @QueryParam("websiteType") String webSiteType) {

        String websiteResponse = websiteManager.getWebsiteDataForType(webSiteType, verificationCode);

        JSONObject jsonObject = JSONObject.fromObject(websiteResponse);
        patientVerificationStatus.setPatientName(jsonObject.getString("name"));
        patientVerificationStatus.setRecommendationExpDate(jsonObject.getString("latest_medical_recommendation_expiration"));
        patientVerificationStatus.setRecommendationIssueDate(jsonObject.getString("latest_medical_recommendation_publication"));
        patientVerificationStatus.setRecommendationStatus(jsonObject.getString("latest_medical_recommendation_status"));
        return patientVerificationStatus;
    }


    @POST
    @Path("/")
    @Timed(name = "updateUserInfo")
    @ApiOperation(value = "Update User Info")
    @PartnerDeveloperSecured(userRequired = true)
    public ConsumerUser updateUserInfo(@QueryParam("cuid") String cuid, @Valid ConsumerUser consumerUser) {
        return consumerUserService.updateUser(consumerUser, Boolean.FALSE);
    }

    @POST
    @Path("/sign")
    @Timed(name = "signContract")
    @ApiOperation(value = "Sign Contract Info")
    @PartnerDeveloperSecured(userRequired = true)
    public ConsumerUser signContract(@QueryParam("cuid") String cuid, @Valid ContractSignRequest signRequest) {
        return consumerUserService.signContract(signRequest);
    }


    @POST
    @Path("/dlPhoto")
    @Timed(name = "uploadDLPhoto")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @ApiOperation(value = "Upload DL Photo")
    @PartnerDeveloperSecured(userRequired = true)
    public ConsumerUser uploadDLPhoto(@QueryParam("cuid") String cuid,
                                      @FormDataParam("file") InputStream inputStream,
                                      @FormDataParam("name") String name,
                                      @FormDataParam("file") final FormDataBodyPart body) {
        String mimeType = body.getMediaType().toString();
        return consumerUserService.updateDLPhoto(inputStream, name);
    }

    @POST
    @Path("/recPhoto")
    @Timed(name = "uploadDLPhoto")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @ApiOperation(value = "Upload Recommendation Photo")
    @PartnerDeveloperSecured(userRequired = true)
    public ConsumerUser uploadRecPhoto(@QueryParam("cuid") String cuid,
                                       @FormDataParam("file") InputStream inputStream,
                                       @FormDataParam("name") String name,
                                       @FormDataParam("file") final FormDataBodyPart body) {
        String mimeType = body.getMediaType().toString();
        return consumerUserService.updateRecPhoto(inputStream, name);
    }

    @GET
    @Path("/assets/{assetKey}")
    @CacheControl(maxAge = 30, maxAgeUnit = TimeUnit.DAYS)
    @Produces({"image/jpeg", "application/pdf", "image/png"})
    @ApiOperation(value = "Get Asset")
    @PartnerDeveloperSecured(userRequired = false)
    public Response getAssetStream(@QueryParam("cuid") String cuid, @PathParam("assetKey") String assetKey) {
        final AssetStreamResult result = consumerUserService.getConsumerAssetStream(assetKey, null);
        StreamingOutput streamOutput = new StreamingOutput() {
            @Override
            public void write(OutputStream os) throws IOException,
                    WebApplicationException {
                ByteStreams.copy(result.getStream(), os);
            }
        };

        return Response.ok(streamOutput).type(result.getContentType()).build();
    }
}
