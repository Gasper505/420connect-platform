package com.fourtwenty.services.resources.partners;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.company.EmployeeResult;
import com.fourtwenty.core.security.partner.BasePartnerDevResource;
import com.fourtwenty.core.security.partner.PartnerDeveloperSecured;
import com.fourtwenty.core.security.store.StoreSecured;
import com.fourtwenty.core.security.tokens.PartnerAuthToken;
import com.fourtwenty.core.services.store.StoreInfoService;
import com.google.inject.Provider;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Api("Blaze Partner Store - Employee")
@Path("/api/v1/partner/employees")
@Produces(MediaType.APPLICATION_JSON)
@StoreSecured
public class PartnerEmployeeResource extends BasePartnerDevResource {
    @Inject
    StoreInfoService storeInfoService;

    @Inject
    public PartnerEmployeeResource(Provider<PartnerAuthToken> tokenProvider) {
        super(tokenProvider);
    }

    @GET
    @Path("/")
    @Timed(name = "getEmployees")
    @ApiOperation(value = "Search Employees")
    @PartnerDeveloperSecured(userRequired = false, storeRequired = false)
    public SearchResult<EmployeeResult> getEmployees(@QueryParam("start") int start,
                                                     @QueryParam("limit") int limit,
                                                     @QueryParam("currentEmployeeId")String currentEmployeeId) {
        return storeInfoService.searchEmployees(currentEmployeeId, start, limit);
    }

    @GET
    @Path("/{employeeId}")
    @Timed(name = "getEmployeeById")
    @ApiOperation(value = "Search Employees")
    @PartnerDeveloperSecured(userRequired = false, storeRequired = false)
    public Employee getEmployeeById(@PathParam("employeeId") String employeeId) {
        return storeInfoService.getEmployeeById(employeeId);
    }


}
