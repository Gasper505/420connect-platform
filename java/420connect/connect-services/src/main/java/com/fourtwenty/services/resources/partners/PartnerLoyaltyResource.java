package com.fourtwenty.services.resources.partners;


import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.loyalty.LoyaltyReward;
import com.fourtwenty.core.domain.models.loyalty.Promotion;
import com.fourtwenty.core.rest.dispensary.requests.promotions.PartnerAddPromotionRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.company.LoyaltyMemberReward;
import com.fourtwenty.core.security.dispensary.Audit;
import com.fourtwenty.core.security.partner.BasePartnerDevResource;
import com.fourtwenty.core.security.partner.PartnerDeveloperSecured;
import com.fourtwenty.core.security.store.StoreSecured;
import com.fourtwenty.core.security.tokens.PartnerAuthToken;
import com.fourtwenty.core.services.partners.PartnerLoyaltyService;
import com.google.inject.Provider;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Api("Blaze Partner Store - Loyalty")
@Path("/api/v1/partner/loyalty")
@Produces(MediaType.APPLICATION_JSON)
@StoreSecured
public class PartnerLoyaltyResource extends BasePartnerDevResource {
    @Inject
    PartnerLoyaltyService partnerLoyaltyService;


    @Inject
    public PartnerLoyaltyResource(Provider<PartnerAuthToken> tokenProvider) {
        super(tokenProvider);
    }

    @GET
    @Path("/rewards")
    @Timed(name = "getRewards")
    @PartnerDeveloperSecured(userRequired = false, storeRequired = true)
    @ApiOperation(value = "Get Rewards List")
    public SearchResult<LoyaltyReward> getRewards(@QueryParam("start") int start, @QueryParam("limit") int limit) {
        return partnerLoyaltyService.getRewards(start, limit);
    }

    @POST
    @Path("/promotions")
    @Timed(name = "addPromotion")
    @PartnerDeveloperSecured(userRequired = false, storeRequired = true)
    @ApiOperation(value = "Add Promotions")
    public Promotion addPromotion(@Valid PartnerAddPromotionRequest request) {
        return partnerLoyaltyService.addPromotion(request);
    }

    @GET
    @Path("/promotions")
    @Timed(name = "getPromotions")
    @PartnerDeveloperSecured(userRequired = false, storeRequired = true)
    @ApiOperation(value = "Get Promotion List")
    public SearchResult<Promotion> getPromotions(@QueryParam("start") int start, @QueryParam("limit") int limit) {
        return partnerLoyaltyService.getPromotions(start, limit);
    }

    @GET
    @Path("/promotions/code/{promocode}")
    @Timed(name = "getPromotions")
    @PartnerDeveloperSecured(userRequired = false, storeRequired = true)
    @ApiOperation(value = "Get Promotion List")
    public Promotion getPromotions(@PathParam("promocode") String promocode) {
        return partnerLoyaltyService.findPromotion(promocode);
    }

    @PUT
    @Path("/promotions/{promotionId}")
    @PartnerDeveloperSecured(userRequired = false, storeRequired = true)
    @Timed(name = "updatePromotion")
    @Audit(action = "Update Promotion")
    @ApiOperation(value = "Update Promotions")
    public Promotion updatePromotion(@PathParam("promotionId") String promotionId, @Valid Promotion promotion) {
        return partnerLoyaltyService.updatePromotion(promotionId, promotion);
    }

    @DELETE
    @Path("/promotions/{promotionId}")
    @PartnerDeveloperSecured(userRequired = false, storeRequired = true)
    @Timed(name = "deletePromotion")
    @Audit(action = "Delete Promotion")
    @ApiOperation(value = "Delete Promotions")
    public void deletePromotion(@PathParam("promotionId") String promotionId) {
        partnerLoyaltyService.deletePromotion(promotionId);
    }

    @GET
    @Path("/rewards/members/{memberId}")
    @Timed(name = "getRewardsForMember")
    @ApiOperation(value = "Get Rewards for member")
    @StoreSecured
    public SearchResult<LoyaltyMemberReward> getAllRewards(@PathParam("memberId") String memberId) {
        return partnerLoyaltyService.getRewardsForMembers(memberId);
    }
}
