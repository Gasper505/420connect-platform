package com.fourtwenty.services.resources.partners;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.rest.consumer.results.MemberLimit;
import com.fourtwenty.core.rest.dispensary.requests.company.MembershipAddRequest;
import com.fourtwenty.core.rest.dispensary.requests.company.MembershipUpdateRequest;
import com.fourtwenty.core.rest.dispensary.results.ListResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.dispensary.Audit;
import com.fourtwenty.core.security.partner.BasePartnerDevResource;
import com.fourtwenty.core.security.partner.PartnerDeveloperSecured;
import com.fourtwenty.core.security.store.StoreSecured;
import com.fourtwenty.core.security.tokens.PartnerAuthToken;
import com.fourtwenty.core.services.partners.PartnerMemberService;
import com.google.inject.Provider;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

@Api("Blaze Partner Memberships")
@Path("/api/v1/partner/members")
@Produces(MediaType.APPLICATION_JSON)
@StoreSecured
public class PartnerMemberResource extends BasePartnerDevResource {
    //@Inject
    @Inject
    PartnerMemberService partnerMemberService;



    @Inject
    public PartnerMemberResource(Provider<PartnerAuthToken> tokenProvider) {
        super(tokenProvider);
    }


    @GET
    @Path("/{memberId}")
    @Timed(name = "getMemberById")
    @ApiOperation(value = "Get Member By Id")
    @PartnerDeveloperSecured(userRequired = false, storeRequired = false)
    public Member getMemberById(@PathParam("memberId") String memberId) {
        return partnerMemberService.getMemberById(memberId);
    }

    @GET
    @Path("/email/{email}")
    @Timed(name = "getMemberByEmail")
    @ApiOperation(value = "Get Member By Email")
    @PartnerDeveloperSecured(userRequired = false, storeRequired = false)
    public Member getMemberByEmail(@PathParam("email") String email) {
        return partnerMemberService.getMemberByEmail(email);
    }

    @POST
    @Path("/")
    @Timed(name = "addNewMember")
    @Audit(action = "Add Member")
    @ApiOperation(value = "Add New Member")
    @PartnerDeveloperSecured(userRequired = false, storeRequired = false)
    public Member addNewMember(@Valid MembershipAddRequest addMember) {
        return partnerMemberService.addMember(addMember);
    }


    @PUT
    @Path("/{memberId}")
    @Timed(name = "updateMember")
    @Audit(action = "Update Member")
    @ApiOperation(value = "Update Member")
    @PartnerDeveloperSecured(userRequired = false, storeRequired = false)
    public Member updateMember(@PathParam("memberId") String memberId, @Valid MembershipUpdateRequest member) {
        return partnerMemberService.updateMembership(memberId, member);
    }

    @GET
    @Path("/")
    @Timed(name = "getMemberList")
    @Audit(action = "Get member list")
    @ApiOperation(value = "Get member list")
    @PartnerDeveloperSecured(userRequired = false, storeRequired = false)
    public SearchResult<Member> getMemberList(@QueryParam("startDate") long startDate, @QueryParam("endDate") long endDate, @QueryParam("skip") int skip, @QueryParam("limit") int limit) {
        return partnerMemberService.getMemberList(startDate, endDate, skip, limit);
    }

    @GET
    @Path("/{memberId}/limits")
    @Timed(name = "getMemberLimit")
    @Audit(action = "Get member limit")
    @ApiOperation(value = "Get member limit")
    @PartnerDeveloperSecured(userRequired = false, storeRequired = false)
    public HashMap<Product.CannabisType, BigDecimal> getMemberLimit(@PathParam("memberId") String memberId) {
        return partnerMemberService.getMemberLimit(memberId);
    }

    @GET
    @Path("/{memberId}/limits/remaining")
    @Timed(name = "getMemberLimit")
    @Audit(action = "Get remaining member limit")
    @ApiOperation(value = "Get remaining member limit")
    @PartnerDeveloperSecured(userRequired = false, storeRequired = false)
    public MemberLimit getMemberLimitRemaining(@PathParam("memberId") String memberId) {
        return partnerMemberService.getRemainingMemberLimit(memberId);
    }

    @GET
    @Path("/search")
    @Timed(name = "getMembersByLicenceNo")
    @Audit(action = "Get members by licence number")
    @ApiOperation(value = "Get members by licence number")
    @PartnerDeveloperSecured(userRequired = false, storeRequired = false)
    public ListResult<Member> getMembersByLicenceNo(@QueryParam("licenceNumber") String licenceNumber) {
        ListResult<Member> listResult = new ListResult<>();
        List<Member> memberList =  partnerMemberService.getMembersByLicenceNo(licenceNumber);
        listResult.setValues(memberList);
        return listResult;
    }

}
