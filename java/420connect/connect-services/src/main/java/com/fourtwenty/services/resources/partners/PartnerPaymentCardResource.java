package com.fourtwenty.services.resources.partners;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.paymentcard.PaymentCardType;
import com.fourtwenty.core.event.BlazeEventBus;
import com.fourtwenty.core.event.paymentcard.LoadPaymentCardEvent;
import com.fourtwenty.core.event.paymentcard.LoadPaymentCardResult;
import com.fourtwenty.core.event.paymentcard.PaymentCardBalanceCheckEvent;
import com.fourtwenty.core.event.paymentcard.PaymentCardBalanceCheckResult;
import com.fourtwenty.core.rest.paymentcard.LoadPaymentCardRequest;
import com.fourtwenty.core.rest.paymentcard.LoadPaymentCardResponse;
import com.fourtwenty.core.rest.paymentcard.PaymentCardBalanceCheckRequest;
import com.fourtwenty.core.rest.paymentcard.PaymentCardBalanceCheckResponse;
import com.fourtwenty.core.security.partner.BasePartnerDevResource;
import com.fourtwenty.core.security.partner.PartnerDeveloperSecured;
import com.fourtwenty.core.security.store.StoreSecured;
import com.fourtwenty.core.security.tokens.PartnerAuthToken;
import com.fourtwenty.integrations.linx.subscriber.LinxSubscriber;
import com.google.inject.Provider;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Api("Blaze PARTNER Payments")
@Path("/api/v1/partner/payments")
@Produces(MediaType.APPLICATION_JSON)
@StoreSecured
public class PartnerPaymentCardResource extends BasePartnerDevResource {
    private static final Log LOG = LogFactory.getLog(LinxSubscriber.class);

    @Inject
    private BlazeEventBus eventBus;

    @Inject
    public PartnerPaymentCardResource(Provider<PartnerAuthToken> tokenProvider) {
        super(tokenProvider);
    }

    @POST
    @Path("/load")
    @Timed(name = "loadPaymentCard")
    @PartnerDeveloperSecured(userRequired = false)
    @ApiOperation(value = "Load a Payment Card")
    public LoadPaymentCardResponse loadPaymentCard(@Valid LoadPaymentCardRequest request) {


        if (StringUtils.isBlank(request.getPaymentCardNumber()) && StringUtils.isNotBlank(request.getLinxCardNumber())) {
            request.setPaymentCardNumber(request.getLinxCardNumber());
            request.setType(PaymentCardType.Linx);
        }

        LOG.info("Card Info: " + request);

        LoadPaymentCardEvent event = new LoadPaymentCardEvent();
        event.setPayload(token, request);
        eventBus.post(event);
        LoadPaymentCardResult result = event.getResponse();
        return new LoadPaymentCardResponse(result);
    }

    @POST
    @Path("/balance")
    @Timed(name = "paymentCardBalanceCheck")
    @PartnerDeveloperSecured(userRequired = false)
    @ApiOperation(value = "Check balance of a Payment Card")
    public PaymentCardBalanceCheckResponse paymentCardBalanceCheck(@Valid PaymentCardBalanceCheckRequest request) {


        // Backward compatibility check
        if (StringUtils.isBlank(request.getPaymentCardNumber()) && StringUtils.isNotBlank(request.getLinxCardNumber())) {
            request.setPaymentCardNumber(request.getLinxCardNumber());
            request.setType(PaymentCardType.Linx);
        }

        LOG.info("Card Info: " + request);

        PaymentCardBalanceCheckEvent event = new PaymentCardBalanceCheckEvent();
        event.setPayload(token, request);
        eventBus.post(event);
        PaymentCardBalanceCheckResult result = event.getResponse();
        return new PaymentCardBalanceCheckResponse(result);
    }
}
