package com.fourtwenty.services.resources.partners;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.rest.dispensary.requests.inventory.ProductAddRequest;
import com.fourtwenty.core.rest.dispensary.requests.partners.PartnerProductUpdateRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.dispensary.Audit;
import com.fourtwenty.core.security.partner.BasePartnerDevResource;
import com.fourtwenty.core.security.partner.PartnerDeveloperSecured;
import com.fourtwenty.core.security.store.StoreSecured;
import com.fourtwenty.core.security.tokens.PartnerAuthToken;
import com.fourtwenty.core.services.partners.PartnerProductService;
import com.google.inject.Inject;
import com.google.inject.Provider;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Api("Blaze Partner Products")
@Path("/api/v1/partner/products")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@StoreSecured
public class PartnerProductResource extends BasePartnerDevResource {

    @Inject
    private PartnerProductService productService;

    @Inject
    public PartnerProductResource(Provider<PartnerAuthToken> tokenProvider) {
        super(tokenProvider);
    }

    @GET
    @Path("/{productId}")
    @Timed(name = "searchProducts")
    @Audit(action = "Get product by id")
    @ApiOperation(value = "Get product by id")
    @PartnerDeveloperSecured(userRequired = false, storeRequired = false)
    public Product searchProducts(@PathParam("productId") String productId) {
        return productService.getProductById(productId);
    }

    @POST
    @Path("/")
    @Timed(name = "addProduct")
    @Audit(action = "Create product")
    @ApiOperation(value = "Create product")
    @PartnerDeveloperSecured(userRequired = false, storeRequired = false)
    public Product addProduct(@Valid ProductAddRequest addRequest) {
        return productService.addProduct(addRequest);
    }

    @PUT
    @Path("/{productId}")
    @Timed(name = "updateProduct")
    @Audit(action = "Update Product")
    @ApiOperation(value = "Update Product")
    @PartnerDeveloperSecured(userRequired = false, storeRequired = false)
    public Product updateProduct(@PathParam("productId") String productId, @Valid PartnerProductUpdateRequest updateRequest) {
        return productService.updateProduct(productId, updateRequest);
    }

    @GET
    @Path("/")
    @Timed(name = "getProducts")
    @Audit(action = "Get list of products")
    @ApiOperation(value = "Get list of products")
    @PartnerDeveloperSecured(userRequired = false, storeRequired = false)
    public SearchResult<Product> getProducts(@QueryParam("startDate") String startDate,
                                             @QueryParam("endDate") String endDate,
                                             @QueryParam("skip") int skip,
                                             @QueryParam("limit") int limit) {
        return productService.getProducts(startDate, endDate, skip, limit);
    }

    @GET
    @Path("/modified")
    @Timed(name = "getProducts")
    @Audit(action = "Get list of products by modified date")
    @ApiOperation(value = "Get list of products by modified date")
    @PartnerDeveloperSecured(userRequired = false, storeRequired = false)
    public SearchResult<Product> getProductsModified(@QueryParam("startDate") long startDate,
                                             @QueryParam("endDate") long endDate,
                                             @QueryParam("skip") int skip,
                                             @QueryParam("limit") int limit) {
        return productService.getProducts(startDate, endDate, skip, limit);
    }
}
