package com.fourtwenty.services.resources.partners;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrder;
import com.fourtwenty.core.rest.dispensary.requests.partners.purchaseorder.PartnerPurchaseOrderAddRequest;
import com.fourtwenty.core.rest.dispensary.requests.partners.purchaseorder.PartnerPurchaseOrderUpdateRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.PurchaseOrderItemResult;
import com.fourtwenty.core.security.partner.BasePartnerDevResource;
import com.fourtwenty.core.security.partner.PartnerDeveloperSecured;
import com.fourtwenty.core.security.store.StoreSecured;
import com.fourtwenty.core.security.tokens.PartnerAuthToken;
import com.fourtwenty.core.services.store.StorePurchaseOrderService;
import com.google.inject.Provider;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Api("Blaze Partner Store - Purchase Order")
@Path("/api/v1/partner/purchaseorders")
@Produces(MediaType.APPLICATION_JSON)
@StoreSecured
public class PartnerPurchaseOrderResource extends BasePartnerDevResource {

    @Inject
    private StorePurchaseOrderService storePurchaseOrderService;

    @Inject
    public PartnerPurchaseOrderResource(Provider<PartnerAuthToken> tokenProvider) {
        super(tokenProvider);
    }

    @POST
    @Path("/")
    @Timed(name = "addPurchaseOrder")
    @ApiOperation(value = "Create Purchase Order")
    @PartnerDeveloperSecured(userRequired = false, storeRequired = false)
    public PurchaseOrder addPurchaseOrder(@Valid PartnerPurchaseOrderAddRequest request) {
        return storePurchaseOrderService.addPurchaseOrder(request);
    }

    @POST
    @Path("/prepare")
    @Timed(name = "preparePurchaseOrder")
    @ApiOperation(value = "Prepare Purchase Order")
    @PartnerDeveloperSecured(userRequired = false, storeRequired = false)
    public PurchaseOrder preparePurchaseOrder(@Valid PartnerPurchaseOrderAddRequest request) {
        return storePurchaseOrderService.preparePurchaseOrder(request);
    }

    @PUT
    @Path("/{purchaseOrderId}")
    @Timed(name = "updatePurchaseOrder")
    @ApiOperation(value = "Update Purchase Order")
    @PartnerDeveloperSecured(userRequired = false, storeRequired = false)
    public PurchaseOrder updatePurchaseOrder(@PathParam("purchaseOrderId") String purchaseOrderId, @Valid PartnerPurchaseOrderUpdateRequest request) {
        return storePurchaseOrderService.updatePurchaseOrder(purchaseOrderId, request);
    }

    @GET
    @Path("/{purchaseOrderId}")
    @Timed(name = "getPurchaseOrderById")
    @ApiOperation(value = "Get Purchase Order by id")
    @PartnerDeveloperSecured(userRequired = false, storeRequired = false)
    public PurchaseOrderItemResult getPurchaseOrderById(@PathParam("purchaseOrderId") String purchaseOrderId) {
        return storePurchaseOrderService.getPurchaseOrderById(purchaseOrderId);
    }


    @GET
    @Path("/list")
    @Timed(name = "getPurchaseOrderList")
    @ApiOperation(value = "Get purchase order list")
    @PartnerDeveloperSecured(userRequired = false, storeRequired = false)
    public SearchResult<PurchaseOrderItemResult> getAllPurchaseOrder(@QueryParam("startDate") String startDate, @QueryParam("endDate") String endDate, @QueryParam("start") int start, @QueryParam("limit") int limit) {
        return storePurchaseOrderService.getAllPurchaseOrder(startDate, endDate, start, limit);
    }


}
