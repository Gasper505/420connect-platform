package com.fourtwenty.services.resources.partners;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.rest.dispensary.results.BrandResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.store.results.ProductWithInfo;
import com.fourtwenty.core.security.partner.BasePartnerDevResource;
import com.fourtwenty.core.security.partner.PartnerDeveloperSecured;
import com.fourtwenty.core.security.store.StoreSecured;
import com.fourtwenty.core.security.tokens.PartnerAuthToken;
import com.fourtwenty.core.services.store.StoreInventoryService;
import com.google.inject.Provider;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * Created by mdo on 4/21/17.
 */
@Api("Blaze PARTNER Store - Inventory")
@Path("/api/v1/partner/store/inventory")
@Produces(MediaType.APPLICATION_JSON)
@StoreSecured
public class PartnerStoreInventoryResource extends BasePartnerDevResource {

    @Inject
    StoreInventoryService storeInventoryService;

    @Inject
    public PartnerStoreInventoryResource(Provider<PartnerAuthToken> tokenProvider) {
        super(tokenProvider);
    }

    @GET
    @Path("/categories")
    @Timed(name = "getAllCategories")
    @ApiOperation(value = "Get All Categories")
    @PartnerDeveloperSecured(userRequired = false)
    public SearchResult<ProductCategory> getAllCategories() {
        return storeInventoryService.getStoreCategories();
    }

    @GET
    @Path("/products")
    @Timed(name = "getAllProducts")
    @ApiOperation(value = "Get All Products")
    @PartnerDeveloperSecured(userRequired = false)
    public SearchResult<ProductWithInfo> getAllProducts(@QueryParam("categoryId") String categoryId,
                                                        @QueryParam("strain") String strain,
                                                        @QueryParam("term") String term,
                                                        @QueryParam("start") int start, @QueryParam("limit") int limit,
                                                        @QueryParam("tags") List<String> tags, @QueryParam("vendorId") String vendorId, @QueryParam("brandId") String brandId) {
        return storeInventoryService.getAllProducts(categoryId, strain, term, tags, vendorId, start, limit, brandId);
    }

    @GET
    @Path("/products/terminals/{terminalId}")
    @Timed(name = "getProductsByTerminal")
    @ApiOperation(value = "Get Products By Terminal")
    @PartnerDeveloperSecured(userRequired = false)
    public SearchResult<Product> getProductsByTerminal(@PathParam("terminalId") String terminalId,
                                                       @QueryParam("start") int start, @QueryParam("limit") int limit) {
        return storeInventoryService.getProductsByTerminalId(terminalId, start, limit);
    }

    @GET
    @Path("/products/dates")
    @Timed(name = "getProductsByDates")
    @ApiOperation(value = "Get Products By Modified Dates")
    @PartnerDeveloperSecured(userRequired = false)
    public SearchResult<Product> getProductsByDates(
            @QueryParam("start") long startDate,
            @QueryParam("limit") long endDate) {
        return storeInventoryService.getProductsByDates(startDate,endDate);
    }

    @GET
    @Path("/products/{productId}")
    @Timed(name = "getProductById")
    @ApiOperation(value = "Get Product By Id")
    @PartnerDeveloperSecured(userRequired = false)
    public ProductWithInfo getProductById(@PathParam("productId") String productId) {
        return storeInventoryService.getProductById(productId);
    }

    @GET
    @Path("/brands")
    @Timed(name = "getAllBrands")
    @ApiOperation(value = "Get All Brands")
    @PartnerDeveloperSecured(userRequired = false)
    public SearchResult<BrandResult> getAllBrands(@QueryParam("term") String term, @QueryParam("start") int start, @QueryParam("limit") int limit) {
        return storeInventoryService.getAllBrands(term, start, limit);
    }


}
