package com.fourtwenty.services.resources.partners;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.company.Terminal;
import com.fourtwenty.core.domain.models.customer.Doctor;
import com.fourtwenty.core.domain.models.partner.PartnerWebHook;
import com.fourtwenty.core.domain.models.product.Inventory;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.partners.request.PartnerWebHookAddRequest;
import com.fourtwenty.core.rest.store.results.StoreInfoResult;
import com.fourtwenty.core.security.partner.BasePartnerDevResource;
import com.fourtwenty.core.security.partner.PartnerDeveloperSecured;
import com.fourtwenty.core.security.store.StoreSecured;
import com.fourtwenty.core.security.tokens.PartnerAuthToken;
import com.fourtwenty.core.services.store.StoreInfoService;
import com.google.inject.Provider;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * Created by mdo on 5/9/17.
 */
@Api("Blaze PARTNER Store - Store Info")
@Path("/api/v1/partner/store")
@Produces(MediaType.APPLICATION_JSON)
@StoreSecured
public class PartnerStoreResource extends BasePartnerDevResource {
    @Inject
    StoreInfoService storeInfoService;

    @Inject
    public PartnerStoreResource(Provider<PartnerAuthToken> tokenProvider) {
        super(tokenProvider);
    }

    @GET
    @Path("/")
    @Timed(name = "getStoreInfo")
    @ApiOperation(value = "Get Store Info")
    @PartnerDeveloperSecured(userRequired = false)
    public StoreInfoResult getStoreInfo() {
        PartnerAuthToken partnerAuthToken = getPartnerToken();
        return storeInfoService.getStoreInfo();
    }

    @GET
    @Path("/doctors")
    @Timed(name = "searchDoctor")
    @ApiOperation(value = "Get Store Info")
    @PartnerDeveloperSecured(userRequired = false)
    public SearchResult<Doctor> searchDoctor(@QueryParam("term") String term) {
        return storeInfoService.searchDoctor(term);
    }


    @GET
    @Path("/terminals")
    @Timed(name = "activeTerminal")
    @ApiOperation(value = "Get all active terminals")
    @PartnerDeveloperSecured(userRequired = false)
    public SearchResult<Terminal> getActiveTerminals(@QueryParam("start") int start, @QueryParam("limit") int limit) {
        return storeInfoService.getActiveTerminalByShop(start, limit);
    }

    @GET
    @Path("/inventories")
    @Timed(name = "getInventories")
    @ApiOperation(value = "Get all active Inventories")
    @PartnerDeveloperSecured(userRequired = false)
    public SearchResult<Inventory> getInventories(@QueryParam("start") int start, @QueryParam("limit") int limit) {
        return storeInfoService.getActiveInventoriesByShop(start, limit);
    }

    @POST
    @Path("/webHook")
    @Timed(name = "createWebHookForPlugin")
    @ApiOperation(value = "Create web hook for plugin")
    @PartnerDeveloperSecured(userRequired = false)
    public List<PartnerWebHook> createPartnerWebHookForPlugIn(@Valid List<PartnerWebHookAddRequest> request) {
        return storeInfoService.createPartnerWebHookForPlugIn(request);
    }
}
