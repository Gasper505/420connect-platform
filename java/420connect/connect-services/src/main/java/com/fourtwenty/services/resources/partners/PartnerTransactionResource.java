package com.fourtwenty.services.resources.partners;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.rest.dispensary.requests.partners.PartnerEmployeeReassignRequest;
import com.fourtwenty.core.rest.dispensary.requests.partners.PartnerQueueAddMemberRequest;
import com.fourtwenty.core.rest.dispensary.requests.partners.PartnerTransactionDeleteRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.dispensary.Audit;
import com.fourtwenty.core.security.partner.BasePartnerDevResource;
import com.fourtwenty.core.security.partner.PartnerDeveloperSecured;
import com.fourtwenty.core.security.store.StoreSecured;
import com.fourtwenty.core.security.tokens.PartnerAuthToken;
import com.fourtwenty.core.services.partners.PartnerTransactionService;
import com.google.inject.Provider;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataParam;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.InputStream;

@Api("Blaze PARTNER Transaction - Transaction/Sales")
@Path("/api/v1/partner/transactions")
@Produces(MediaType.APPLICATION_JSON)
@StoreSecured
public class PartnerTransactionResource extends BasePartnerDevResource {

    @Inject
    private PartnerTransactionService partnerTransactionService;

    @Inject
    public PartnerTransactionResource(Provider<PartnerAuthToken> tokenProvider) {
        super(tokenProvider);
    }

    @GET
    @Path("/")
    @Timed(name = "getAllTransaction")
    @ApiOperation(value = "Get All Transaction")
    @PartnerDeveloperSecured(userRequired = false)
    public SearchResult<Transaction> getAllTransaction(@QueryParam("startDate") String startDate,
                                                       @QueryParam("endDate") String endDate,
                                                        @QueryParam("skip") int skip,
                                                        @QueryParam("limit") int limit) {
        return partnerTransactionService.getAllTransaction(startDate, endDate, skip, limit);
    }

    @GET
    @Path("/active")
    @Timed(name = "getAllActiveTransaction")
    @ApiOperation(value = "Get All Transaction")
    @PartnerDeveloperSecured(userRequired = false)
    public SearchResult<Transaction> getAllActiveTransaction() {
        return partnerTransactionService.getActiveTransactions();
    }

    @GET
    @Path("/{transactionId}")
    @Timed(name = "getTransactionById")
    @ApiOperation(value = "Get Transaction by Id")
    @PartnerDeveloperSecured(userRequired = false)
    public Transaction getTransactionById(@PathParam("transactionId") String transactionId) {
        return partnerTransactionService.getTransactionById(transactionId);
    }

    @POST
    @Path("/queues/{queueName}")
    @Timed(name = "addToQueue")
    @Audit(action = "Add Member to Queue")
    @ApiOperation(value = "Add Member to Queue")
    @PartnerDeveloperSecured(userRequired = false)
    public Transaction addToQueue(@PathParam("queueName") String queueName, @Valid PartnerQueueAddMemberRequest request) {
        return partnerTransactionService.addToQueue(queueName,request);
    }


    @DELETE
    @Path("/{transactionId}")
    @Timed(name = "deleteTransaction")
    @Audit(action = "Delete Transaction From Queue")
    @ApiOperation(value = "Delete Transaction from Queue")
    @PartnerDeveloperSecured(userRequired = false)
    public Response deleteTransaction(@PathParam("transactionId") String transactionId, @Valid PartnerTransactionDeleteRequest request) {
        partnerTransactionService.deleteTransaction(transactionId,request);
        return ok();
    }

    // No need to audit as we're are just preparing cart
    @POST
    @Path("/{transactionId}/prepare")
    @Timed(name = "prepareCart")
    @ApiOperation(value = "Prepare Cart")
    @PartnerDeveloperSecured(userRequired = false)
    public Transaction prepareCart(@PathParam("transactionId") String transactionId, @Valid Transaction transaction) {

        return partnerTransactionService.prepareCart(transactionId,transaction);
    }

    @POST
    @Path("/{transactionId}/sendToFulfill")
    @Timed(name = "setTransactionToFulfill")
    @PartnerDeveloperSecured(userRequired = false)
    @Audit(action = "Send Transaction To Fulfill")
    @ApiOperation(value = "Set Transaction to Fulfill")
    public Transaction setTransactionToFulfill(@PathParam("transactionId") String transactionId, @Valid Transaction transaction) {
        return partnerTransactionService.setTransactionToFulfill(transaction, transactionId);
    }

    @POST
    @Path("/{transactionId}/complete")
    @Timed(name = "completeTransaction")
    @Audit(action = "Complete Transaction")
    @ApiOperation(value = "Complete Transaction")
    @PartnerDeveloperSecured(userRequired = false)
    public Transaction processTransction(@PathParam("transactionId") String transactionId, @Valid Transaction transaction) {
        if (StringUtils.isBlank(transaction.getMemberId())) {
            throw new BlazeInvalidArgException("Transaction","MemberId cannot be empty");
        }
        if (StringUtils.isBlank(transaction.getSellerId())) {
            throw new BlazeInvalidArgException("Transaction","SellerId cannot be empty");
        }
        if (StringUtils.isBlank(transaction.getTerminalId())) {
            throw new BlazeInvalidArgException("Transaction","TerminalId cannot be empty");
        }
        return partnerTransactionService.completeTransaction(transactionId, transaction);
    }


    @POST
    @Path("/{transactionId}/markAsPaid")
    @Timed(name = "markAsPaid")
    @Audit(action = "Mark Transaction as Paid")
    @ApiOperation(value = "Mark Transaction as Paid")
    @PartnerDeveloperSecured(userRequired = false)
    public Transaction markAsPaid(@PathParam("transactionId") String transactionId,
                                  @QueryParam("paymentOption") Cart.PaymentOption paymentOption) {
        return partnerTransactionService.markAsPaid(transactionId, paymentOption);
    }



    @POST
    @Path("/{transactionId}/sign")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Audit(action = "Sign Transaction", json = false)
    @ApiOperation(value = "Sign Transaction")
    @PartnerDeveloperSecured(userRequired = false)
    public Transaction signTransaction(@PathParam("transactionId") String transactionId, @FormDataParam("file") InputStream inputStream,
                                       @FormDataParam("name") String name,
                                       @FormDataParam("file") final FormDataBodyPart body) {
        return partnerTransactionService.signTransaction(transactionId,inputStream,name,body);
    }

    @POST
    @Path("/{transactionId}/hold")
    @Timed(name = "holdTransaction")
    @Audit(action = "Hold Transaction")
    @ApiOperation(value = "Hold Transaction")
    @PartnerDeveloperSecured(userRequired = false)
    public Transaction holdTransaction(@PathParam("transactionId") String transactionId, @Valid Transaction transaction) {
        if (StringUtils.isBlank(transaction.getMemberId())) {
            throw new BlazeInvalidArgException("Transaction","MemberId cannot be empty");
        }
        if (StringUtils.isBlank(transaction.getSellerId())) {
            throw new BlazeInvalidArgException("Transaction","SellerId cannot be empty");
        }
        if (StringUtils.isBlank(transaction.getTerminalId())) {
            throw new BlazeInvalidArgException("Transaction","TerminalId cannot be empty");
        }
        return partnerTransactionService.holdTransaction(transactionId,transaction);
    }

    @POST
    @Path("/{transactionId}/address")
    @Timed(name = "updateDeliveryAddress")
    @PartnerDeveloperSecured(userRequired = false)
    @Audit(action = "Update delivery address for transaction")
    @ApiOperation(value = "Update delivery address for transaction")
    public Transaction updateDeliveryAddress(@PathParam("transactionId") String transactionId, @Valid Address request) {
        return partnerTransactionService.updateDeliveryAddress(transactionId, request);

    }

    @POST
    @Path("/{transactionId}/employee")
    @Timed(name = "reassignTransactionEmployee")
    @Audit(action = "Reassign Transaction Employee")
    @ApiOperation(value = "Reassign Transaction Employee")
    @PartnerDeveloperSecured(userRequired = false)
    public Transaction reassignTransactionEmployee(@PathParam("transactionId") String transactionId, @Valid PartnerEmployeeReassignRequest request) {

        return partnerTransactionService.reassignTransactionEmployee(transactionId,request);
    }


    @GET
    @Path("/{transactionId}/unassign")
    @Timed(name = "unassignTransaction")
    @Audit(action = "Unassign Transaction")
    @ApiOperation(value = "Unassign Transaction")
    @PartnerDeveloperSecured(userRequired = false)
    public Transaction unassignTransaction(@PathParam("transactionId") String transactionId) {
        return partnerTransactionService.unassignTransaction(transactionId);
    }

    @GET
    @Path("/{transactionId}/start")
    @Timed(name = "startTransaction")
    @Audit(action = "Start Transaction")
    @ApiOperation(value = "Start Transaction")
    @PartnerDeveloperSecured(userRequired = false)
    public Transaction startTransaction(@PathParam("transactionId") String transactionId) {
        return partnerTransactionService.startTransaction(transactionId);
    }


    @GET
    @Path("/{transactionId}/stop")
    @Timed(name = "stopTransaction")
    @Audit(action = "Stop Transaction")
    @ApiOperation(value = "Stop Transaction")
    @PartnerDeveloperSecured(userRequired = false)
    public Transaction stopTransaction(@PathParam("transactionId") String transactionId) {
        return partnerTransactionService.stopTransaction(transactionId);
    }

    @GET
    @Path("/members/{memberId}/completed")
    @Timed(name = "getCompletedTransactionsForMember")
    @Audit(action = "Get completed transactions for member")
    @ApiOperation(value = "Get completed transactions for member")
    @PartnerDeveloperSecured(userRequired = false)
    public SearchResult<Transaction> getCompletedTransactionsForMember(@PathParam("memberId") String memberId, @QueryParam("start")int start, @QueryParam("limit")int limit) {
        return partnerTransactionService.getCompletedTransactionsForMember(memberId, start, limit);
    }

    @GET
    @Path("/members/{memberId}/active")
    @Timed(name = "getMemberActiveTransactions")
    @Audit(action = "Get active transactions for member")
    @ApiOperation(value = "Get active transactions for member")
    @PartnerDeveloperSecured(userRequired = false)
    public SearchResult<Transaction> getMemberActiveTransactions(@PathParam("memberId") String memberId) {
        return partnerTransactionService.getMemberActiveTransactions(memberId);
    }

    @POST
    @Path("/{transactionId}/finalize")
    @Timed(name = "finalizeTransaction")
    @Audit(action = "Finalize all transaction items")
    @ApiOperation(value = "Finalize all transaction items")
    @PartnerDeveloperSecured(userRequired = false)
    public Transaction finalizeTransaction(@PathParam("transactionId") String transactionId, Transaction transaction) {
        return partnerTransactionService.finalizeTransaction(transactionId, transaction, true);
    }

    @POST
    @Path("/{transactionId}/unfinalize")
    @Timed(name = "unfinalizeTransaction")
    @Audit(action = "Unfinalize all transaction items")
    @ApiOperation(value = "Unfinalize all transaction items")
    @PartnerDeveloperSecured(userRequired = false)
    public Transaction unfinalizeTransaction(@PathParam("transactionId") String transactionId, @Valid Transaction transaction) {
        return partnerTransactionService.finalizeTransaction(transactionId, transaction, false);
    }
}
