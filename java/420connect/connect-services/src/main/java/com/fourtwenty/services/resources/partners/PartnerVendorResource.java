package com.fourtwenty.services.resources.partners;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.product.Vendor;
import com.fourtwenty.core.rest.dispensary.requests.inventory.VendorAddRequest;
import com.fourtwenty.core.rest.dispensary.requests.partners.PartnerVendorUpdateRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.VendorResult;
import com.fourtwenty.core.security.dispensary.Audit;
import com.fourtwenty.core.security.partner.BasePartnerDevResource;
import com.fourtwenty.core.security.partner.PartnerDeveloperSecured;
import com.fourtwenty.core.security.store.StoreSecured;
import com.fourtwenty.core.security.tokens.PartnerAuthToken;
import com.fourtwenty.core.services.partners.PartnerVendorService;
import com.google.inject.Inject;
import com.google.inject.Provider;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.validation.Valid;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Api("Blaze Partner Vendor")
@Path("/api/v1/partner/vendors")
@Produces(MediaType.APPLICATION_JSON)
@StoreSecured
public class PartnerVendorResource extends BasePartnerDevResource {

    @Inject
    private PartnerVendorService vendorService;

    @Inject
    public PartnerVendorResource(Provider<PartnerAuthToken> tokenProvider) {
        super(tokenProvider);
    }

    @GET
    @Path("/{vendorId}")
    @Timed(name = "getVendor")
    @Audit(action = "Get vendor by id")
    @ApiOperation(value = "Get vendor by id")
    @PartnerDeveloperSecured(userRequired = false, storeRequired = false)
    public VendorResult getVendor(@PathParam("vendorId") String vendorId) {
        return vendorService.getVendor(vendorId);
    }

    @GET
    @Path("/")
    @Timed(name = "getVendors")
    @Audit(action = "Search vendor")
    @ApiOperation(value = "Search vendors")
    @PartnerDeveloperSecured(userRequired = false, storeRequired = false)
    public SearchResult<Vendor> getVendors(@QueryParam("startDate") String startDate,
                                                 @QueryParam("endDate") String endDate,
                                                 @QueryParam("skip") int skip,
                                                 @QueryParam("limit") int limit) {
        return vendorService.getVendors(startDate, endDate, skip, limit);
    }

    @POST
    @Path("/")
    @Timed(name = "createVendor")
    @Audit(action = "Create Vendor")
    @ApiOperation(value = "Create Vendor")
    @PartnerDeveloperSecured(userRequired = false, storeRequired = false)
    public Vendor createVendor(@Valid VendorAddRequest request) {
        return vendorService.addVendor(request);
    }

    @PUT
    @Path("/{vendorId}")
    @Timed(name = "updateVendor")
    @Audit(action = "Update Vendor")
    @ApiOperation(value = "Update Vendor")
    @PartnerDeveloperSecured(userRequired = false, storeRequired = false)
    public Vendor updateVendor(@PathParam("vendorId") String vendorId, @Valid PartnerVendorUpdateRequest request) {
        return vendorService.updateVendor(vendorId, request);
    }
}
