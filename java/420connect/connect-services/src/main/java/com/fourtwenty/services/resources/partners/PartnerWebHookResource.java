package com.fourtwenty.services.resources.partners;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.partner.PartnerWebHook;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.partners.request.PartnerWebHookAddRequest;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.partners.PartnerWebHookService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Api("Management - PARTNER WebHook")
@Path("/api/v1/partner/webHooks")
@Produces(MediaType.APPLICATION_JSON)
public class PartnerWebHookResource {

    @Inject
    PartnerWebHookService partnerWebHookService;

    @POST
    @Path("/")
    @Timed(name = "createPartnerWebHooks")
    @ApiOperation(value = "Create PARTNER WebHooks")
    @Secured
    public PartnerWebHook createPartnerWebHooks(@Valid PartnerWebHookAddRequest request) {
        return partnerWebHookService.createPartnerWebHooks(request);
    }

    @PUT
    @Path("/{webHookId}")
    @Timed(name = "updatePartnerWebHooks")
    @ApiOperation(value = "Update PARTNER WebHooks")
    @Secured
    public PartnerWebHook updatePartnerWebHooks(@PathParam("webHookId") String webHookId, @Valid PartnerWebHook request) {
        return partnerWebHookService.updatePartnerWebHooks(webHookId, request);
    }

    @GET
    @Path("/{webHookId}")
    @Timed(name = "getPartnerWebHooksById")
    @ApiOperation(value = "Get PARTNER WebHooks By Id")
    @Secured
    public PartnerWebHook getPartnerWebHookById(@PathParam("webHookId") String webHookId) {
        return partnerWebHookService.getPartnerWebHookById(webHookId);
    }

    @GET
    @Path("/")
    @Timed(name = "getPartnerWebHooksById")
    @ApiOperation(value = "Get PARTNER WebHooks list")
    @Secured
    public SearchResult<PartnerWebHook> getPartnerWebHook(@QueryParam("type") PartnerWebHook.PartnerWebHookType type, @QueryParam("start") int start, @QueryParam("limit") int limit, @QueryParam("shopId") String shopId) {
        return partnerWebHookService.getPartnerWebHook(type, start, limit, shopId);
    }

    @DELETE
    @Path("/{id}")
    @Timed(name = "deletePartnerWebHook")
    @ApiOperation(value = "Delete partner webhook by id")
    @Secured
    public Response deletePartnerWebHook(@PathParam("id") String id) {
        partnerWebHookService.deletePartnerWebHook(id);
        return Response.ok().build();
    }


}

