package com.fourtwenty.services.resources.pos;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.rest.dispensary.requests.auth.CheckPermissionRequest;
import com.fourtwenty.core.rest.dispensary.requests.auth.InitTerminalRequest;
import com.fourtwenty.core.rest.dispensary.requests.auth.ManagerAccessRequest;
import com.fourtwenty.core.rest.dispensary.requests.auth.QuickPinLoginRequest;
import com.fourtwenty.core.rest.dispensary.requests.terminals.TerminalAssignRequest;
import com.fourtwenty.core.rest.dispensary.requests.terminals.TerminalSetupRequest;
import com.fourtwenty.core.rest.dispensary.results.InitialLoginResult;
import com.fourtwenty.core.rest.dispensary.results.auth.ManagerAccessResult;
import com.fourtwenty.core.security.dispensary.Audit;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.services.mgmt.AuthenticationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by mdo on 8/16/15.
 */
@Api("POS - Session")
@Path("/api/v1/session")
@Produces(MediaType.APPLICATION_JSON)
public class AuthenticationResource extends BaseResource {
    @Inject
    AuthenticationService service;

    @GET
    @Path("/")
    @Timed(name = "getSession")
    @Secured
    @ApiOperation(value = "Get Current Session")
    public InitialLoginResult getSession() {
        return service.getCurrentActiveEmployee();
    }


    @GET
    @Path("/renew")
    @Timed(name = "renewToken")
    @Secured
    @Audit(action = "Renew Token")
    @ApiOperation(value = "Renew Token")
    public InitialLoginResult renewToken(@QueryParam("growAppType")ConnectAuthToken.GrowAppType growAppType) {
        growAppType = (growAppType == null) ? ConnectAuthToken.GrowAppType.None : growAppType;
        return service.renewToken(growAppType);
    }

    @POST
    @Path("/permission/check")
    @Timed(name = "checkPermission")
    @Secured
    @Audit(action = "Check Permission")
    @ApiOperation(value = "Check Permission")
    public Response checkPermission(@Valid CheckPermissionRequest pinRequest) {
        service.checkPermission(pinRequest);
        return ok();
    }

    @POST
    @Path("/terminal/init")
    @Timed(name = "initTerminalByEmail")
    @Audit(action = "Initialize Terminal")
    @ApiOperation(value = "Initialize Terminal - Login with Email / Password")
    public InitialLoginResult initTerminalByEmail(@Valid InitTerminalRequest request, @Context HttpServletResponse response) {
        InitialLoginResult result = service.terminalManagerLogin(request);
        response.setHeader("X-Auth-Token", result.getAccessToken());
        return result;
    }

    @POST
    @Path("/terminal/setup")
    @Timed(name = "setupTerminal")
    @Secured
    @Audit(action = "Setup Terminal")
    @ApiOperation(value = "Setup Terminal")
    public InitialLoginResult setupTerminal(@Valid TerminalSetupRequest request, @Context HttpServletResponse response) {
        InitialLoginResult result = service.setupTerminal(request);
        response.setHeader("X-Auth-Token", result.getAccessToken());
        return result;
    }

    @POST
    @Path("/terminal/assign")
    @Timed(name = "assignTerminal")
    @Secured
    @Audit(action = "Assign Terminal")
    @ApiOperation(value = "Assign Terminal")
    public InitialLoginResult assignTerminal(@Valid TerminalAssignRequest request, @Context HttpServletResponse response) {
        InitialLoginResult result = service.terminalAssignEmployee(request);
        response.setHeader("X-Auth-Token", result.getAccessToken());
        return result;
    }

    @POST
    @Path("/terminal/settings")
    @Timed(name = "accessManagerSettings")
    @Secured(requiredShop = true)
    @Audit(action = "Request Manager Settings Access")
    @ApiOperation(value = "Request Manager Settings Access")
    public ManagerAccessResult accessManagerSettings(@Valid ManagerAccessRequest request, @Context HttpServletResponse response) {
        ManagerAccessResult result = service.managerSettingsAccess(request);
        response.setHeader("X-Auth-Token", result.getAccessToken());
        return result;
    }


    @POST
    @Path("/terminal/signin")
    @Timed(name = "signinTerminal")
    @Audit(action = "Employee QuickPin Login")
    @ApiOperation(value = "Employee QuickPin Login")
    @Secured(requiredShop = true, checkTerminal = true)
    public InitialLoginResult signinTerminal(@Valid QuickPinLoginRequest request, @Context HttpServletResponse response) {
        InitialLoginResult result = service.terminalEmployeeLogin(request);
        response.setHeader("X-Auth-Token", result.getAccessToken());
        return result;
    }

    @DELETE
    @Path("/terminal/logout")
    @Timed(name = "logoutTerminal")
    @Secured(requiredShop = true)
    @Audit(action = "Employee Logout")
    @ApiOperation(value = "Employee Logout")
    public Response logoutTerminal(@Context HttpServletResponse response) {
        service.terminalEmployeeLogout();
        return ok();
    }


}
