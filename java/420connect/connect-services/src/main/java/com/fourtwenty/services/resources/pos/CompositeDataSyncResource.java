package com.fourtwenty.services.resources.pos;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.rest.dispensary.results.common.CompositeInventorySyncResult;
import com.fourtwenty.core.rest.dispensary.results.common.CompositeProductInfoSyncResult;
import com.fourtwenty.core.rest.dispensary.results.common.CompositeShopSyncResult;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.pos.CompositeDataSyncService;
import com.google.inject.Inject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 * Created by mdo on 5/26/18.
 */
@Api("POS - Composite Data Sync")
@Path("/api/v1/pos/compositeSync")
@Produces(MediaType.APPLICATION_JSON)
public class CompositeDataSyncResource {

    @Inject
    CompositeDataSyncService compositeDataSyncService;

    @GET
    @Path("/shop")
    @Timed(name = "getCompositeShopSyncResult")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get composite sync result for shop")
    public CompositeShopSyncResult getCompositeShopSyncResult(@QueryParam("afterDate") long afterDate) {
        return compositeDataSyncService.getShopData(afterDate);

    }

    @GET
    @Path("/inventory")
    @Timed(name = "getCompositeInventorySyncResult")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get composite sync result for inventory")
    public CompositeInventorySyncResult getCompositeInventorySyncResult(@QueryParam("afterDate") long afterDate) {
        return compositeDataSyncService.getInventorySyncData(afterDate);

    }

    @GET
    @Path("/product")
    @Timed(name = "getCompositeProductSyncResult")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get composite sync result for product")
    public CompositeProductInfoSyncResult getCompositeProductInfoSyncResult(@QueryParam("afterDate") long afterDate) {
        return compositeDataSyncService.getProductInfoSyncData(afterDate);
    }
}
