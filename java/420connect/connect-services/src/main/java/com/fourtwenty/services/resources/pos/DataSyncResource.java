package com.fourtwenty.services.resources.pos;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.company.*;
import com.fourtwenty.core.domain.models.loyalty.LoyaltyActivityLog;
import com.fourtwenty.core.domain.models.loyalty.LoyaltyReward;
import com.fourtwenty.core.domain.models.loyalty.Promotion;
import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.models.thirdparty.ThirdPartyAccount;
import com.fourtwenty.core.domain.models.thirdparty.ThirdPartyShopAccount;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.company.CashDrawerSessionResult;
import com.fourtwenty.core.rest.dispensary.results.shop.ConsumerCartResult;
import com.fourtwenty.core.rest.dispensary.results.shop.ShopResult;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.inventory.BatchQuantityService;
import com.fourtwenty.core.services.mgmt.*;
import com.fourtwenty.core.services.pos.POSStoreService;
import com.fourtwenty.core.services.pos.TerminalService;
import com.google.inject.Inject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.joda.time.DateTime;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/**
 * Created by mdo on 6/18/16.
 */
@Api("POS - Data Sync")
@Path("/api/v1/pos/sync")
@Produces(MediaType.APPLICATION_JSON)
public class DataSyncResource extends BaseResource {
    @Inject
    ShopService shopService;
    @Inject
    QueuePOSService queuePOSService;
    @Inject
    EmployeeService employeeService;
    @Inject
    TerminalService terminalService;
    @Inject
    InventoryService inventoryService;
    @Inject
    ContractService contractService;
    @Inject
    MemberGroupService memberGroupService;
    @Inject
    CashDrawerService cashDrawerService;
    @Inject
    PromotionService promotionService;
    @Inject
    ThirdPartyAccountService thirdPartyAccountService;
    @Inject
    ThirdPartyShopAccountService thirdPartyShopAccountService;
    @Inject
    PrepackageProductService prepackageProductService;
    @Inject
    BarcodeService barcodeService;
    @javax.inject.Inject
    ProductService productService;
    @javax.inject.Inject
    POSStoreService posStoreService;
    @Inject
    LoyaltyRewardService rewardService;
    @Inject
    BatchQuantityService batchQuantityService;


    @GET
    @Path("/shops")
    @Timed(name = "syncShops")
    @Secured
    @ApiOperation(value = "Sync Shops")
    public DateSearchResult<ShopResult> syncShops(@QueryParam("afterDate") long afterDate, @QueryParam("beforeDate") long beforeDate) {
        return shopService.getShops(afterDate, DateTime.now().getMillis());
    }

    @GET
    @Path("/contracts")
    @Timed(name = "syncContracts")
    @Secured
    @ApiOperation(value = "Sync Contracts")
    public DateSearchResult<Contract> syncContracts(@QueryParam("afterDate") long afterDate, @QueryParam("beforeDate") long beforeDate) {
        return contractService.getCompanyContracts(afterDate, DateTime.now().getMillis());
    }


    @GET
    @Path("/terminals")
    @Timed(name = "syncTerminals")
    @Secured
    @ApiOperation(value = "Sync Terminals")
    public DateSearchResult<Terminal> syncTerminals(@QueryParam("afterDate") long afterDate, @QueryParam("beforeDate") long beforeDate) {
        return terminalService.getTerminals(afterDate, DateTime.now().getMillis());
    }

    @GET
    @Path("/inventories")
    @Timed(name = "syncInventories")
    @Secured
    @ApiOperation(value = "Sync Inventories")
    public DateSearchResult<Inventory> syncInventories(@QueryParam("afterDate") long afterDate, @QueryParam("beforeDate") long beforeDate) {
        return inventoryService.getCompanyInventories(afterDate, DateTime.now().getMillis());
    }

    @GET
    @Path("/batchquantities")
    @Timed(name = "syncBatchQuantities")
    @Secured
    @ApiOperation(value = "Sync Batch Quantities")
    public DateSearchResult<BatchQuantity> syncBatchQuantities(@QueryParam("afterDate") long afterDate, @QueryParam("beforeDate") long beforeDate) {
        return batchQuantityService.retrieveBatchQuantities(getToken().getCompanyId(), getToken().getShopId(), afterDate, DateTime.now().getMillis());
    }

    @GET
    @Path("/employees")
    @Timed(name = "syncEmployees")
    @Secured
    @ApiOperation(value = "Sync Employees")
    public DateSearchResult<Employee> syncEmployees(@QueryParam("afterDate") long afterDate, @QueryParam("beforeDate") long beforeDate) {
        return employeeService.findEmployees(afterDate, DateTime.now().getMillis());
    }

    @GET
    @Path("/memberGroups")
    @Timed(name = "syncMemberGroups")
    @Secured
    @ApiOperation(value = "Sync Member Groups")
    public DateSearchResult<MemberGroup> syncMemberGroups(@QueryParam("afterDate") long afterDate, @QueryParam("beforeDate") long beforeDate) {
        return memberGroupService.getMemberGroupsByDate(afterDate, DateTime.now().getMillis());
    }

    @GET
    @Path("/transactions")
    @Timed(name = "syncTransactions")
    @Secured
    @ApiOperation(value = "Sync Transactions")
    public DateSearchResult<Transaction> syncTransactions(@QueryParam("afterDate") long afterDate, @QueryParam("beforeDate") long beforeDate) {
        return queuePOSService.getTransactions(afterDate, beforeDate);
    }

    @POST
    @Path("/transactions")
    @Timed(name = "syncTransactions")
    @Secured
    @ApiOperation(value = "Sync Transactions")
    public DateSearchResult<Transaction> getRecentActive(@QueryParam("afterDate") long afterDate, @QueryParam("beforeDate") long beforeDate) {
        return queuePOSService.getTransactions(afterDate, beforeDate);
    }

    @GET
    @Path("/cashDrawers")
    @Timed(name = "syncCashDrawers")
    @Secured
    @ApiOperation(value = "Sync Cash Drawers")
    public DateSearchResult<CashDrawerSessionResult> syncCashDrawers(@QueryParam("afterDate") long afterDate, @QueryParam("beforeDate") long beforeDate) {
        return cashDrawerService.getCashDrawersByDate(afterDate, beforeDate);
    }

    @GET
    @Path("/paidios")
    @Timed(name = "syncPaidIos")
    @Secured
    @ApiOperation(value = "Sync Paid In/Outs")
    public DateSearchResult<PaidInOutItem> syncPaidIos(@QueryParam("afterDate") long afterDate, @QueryParam("beforeDate") long beforeDate) {
        return cashDrawerService.getPaidIOsByDate(afterDate, beforeDate);
    }

    @GET
    @Path("/promotions")
    @Timed(name = "syncPromotions")
    @Secured
    @ApiOperation(value = "Sync Promotions")
    public DateSearchResult<Promotion> syncPromotions(@QueryParam("afterDate") long afterDate, @QueryParam("beforeDate") long beforeDate) {
        return promotionService.getPromotionsByDate(afterDate, beforeDate);
    }

    @GET
    @Path("/thirdparty")
    @Timed(name = "syncThirdPartyAccount")
    @Secured
    @ApiOperation(value = "Sync Third Party Accounts")
    public DateSearchResult<ThirdPartyAccount> syncThirdPartyAccount(@QueryParam("afterDate") long afterDate, @QueryParam("beforeDate") long beforeDate) {
        return thirdPartyAccountService.getThirdPartyAccounts(afterDate, beforeDate);
    }


    @GET
    @Path("/thirdparty/shops")
    @Timed(name = "syncThirdPartyAccount")
    @Secured
    @ApiOperation(value = "Sync Third Party Accounts")
    public DateSearchResult<ThirdPartyShopAccount> syncThirdPartyShopAccount(@QueryParam("afterDate") long afterDate, @QueryParam("beforeDate") long beforeDate) {
        return thirdPartyShopAccountService.getThirdPartyShopAccounts(afterDate, beforeDate);
    }

    @GET
    @Path("/prepackages")
    @Timed(name = "syncPrepackageProducts")
    @Secured
    @ApiOperation(value = "Sync Prepackages")
    public DateSearchResult<Prepackage> syncPrepackageProducts(@QueryParam("afterDate") long afterDate, @QueryParam("beforeDate") long beforeDate) {
        return prepackageProductService.getPrepackagesWithDate(afterDate, beforeDate);
    }

    @GET
    @Path("/prepackages/items")
    @Timed(name = "syncPrepackageProductItems")
    @Secured
    @ApiOperation(value = "Sync PrepackageProducts Items")
    public DateSearchResult<PrepackageProductItem> syncPrepackageProductsItems(@QueryParam("afterDate") long afterDate, @QueryParam("beforeDate") long beforeDate) {
        return prepackageProductService.getPrepackagesWithDateItems(afterDate, beforeDate);
    }

    @GET
    @Path("/barcodes")
    @Timed(name = "syncBarcodeItems")
    @Secured
    @ApiOperation(value = "Sync Barcode Items")
    public DateSearchResult<BarcodeItem> syncBarcodeItems(@QueryParam("afterDate") long afterDate, @QueryParam("beforeDate") long beforeDate) {
        return barcodeService.getBarcodesWithDates(getToken().getCompanyId(),getToken().getShopId(),afterDate, beforeDate);
    }

    @GET
    @Path("/prepackages/quantities")
    @Timed(name = "syncBarcodeItems")
    @Secured
    @ApiOperation(value = "Sync Barcode Items")
    public DateSearchResult<ProductPrepackageQuantity> syncPrepackageQuantities(@QueryParam("afterDate") long afterDate, @QueryParam("beforeDate") long beforeDate) {
        return prepackageProductService.getPrepackageQuantitiesWithDate(afterDate, beforeDate);
    }

    @GET
    @Path("/incoming/orders")
    @Timed(name = "syncConsumerIncomingOrders")
    @Secured
    @ApiOperation(value = "Sync Incoming Orders")
    public DateSearchResult<ConsumerCartResult> syncConsumerIncomingOrders(@QueryParam("afterDate") long afterDate, @QueryParam("beforeDate") long beforeDate) {
        return queuePOSService.getConsumerOrders(afterDate, beforeDate);
    }

    @GET
    @Path("/products/groupPrices")
    @Timed(name = "syncProductGroupPrices")
    @Secured
    @ApiOperation(value = "Sync Product Group Prices")
    public DateSearchResult<MemberGroupPrices> syncProductGroupPrices(@QueryParam("afterDate") long afterDate, @QueryParam("beforeDate") long beforeDate) {
        return productService.getMemberGroupPrices(afterDate, beforeDate);
    }

    @GET
    @Path("/consumer/carts")
    @Timed(name = "syncConsumerCarts")
    @Secured
    @ApiOperation(value = "Sync Consumer Carts")
    public DateSearchResult<ConsumerCartResult> syncConsumerCarts(@QueryParam("afterDate") long afterDate, @QueryParam("beforeDate") long beforeDate) {
        return posStoreService.getConsumerCarts(afterDate, beforeDate);
    }

    @Deprecated
    @GET
    @Path("/loyalty/rewards")
    @Timed(name = "syncLoyaltyRewards")
    @Secured
    @ApiOperation(value = "Sync Company Rewards")
    public DateSearchResult<LoyaltyReward> syncLoyaltyRewardsOld(@QueryParam("afterDate") long afterDate, @QueryParam("beforeDate") long beforeDate) {
        return rewardService.getRewardsByDate(afterDate, beforeDate);
    }

    @Deprecated
    @GET
    @Path("/loyalty/rewards/usage")
    @Timed(name = "syncLoyaltyRewardUsage")
    @Secured
    @ApiOperation(value = "Sync Company Rewards Usage")
    public DateSearchResult<LoyaltyActivityLog> syncLoyaltyRewardUsageOld(@QueryParam("afterDate") long afterDate, @QueryParam("beforeDate") long beforeDate) {
        return rewardService.getLoyaltyUsageReward(afterDate, beforeDate);
    }


    @GET
    @Path("/paymentcard/rewards")
    @Timed(name = "syncLoyaltyRewards")
    @Secured
    @ApiOperation(value = "Sync Company Rewards")
    public DateSearchResult<LoyaltyReward> syncLoyaltyRewards(@QueryParam("afterDate") long afterDate, @QueryParam("beforeDate") long beforeDate) {
        return rewardService.getRewardsByDate(afterDate, beforeDate);
    }

    @GET
    @Path("/paymentcard/rewards/usage")
    @Timed(name = "syncLoyaltyRewardUsage")
    @Secured
    @ApiOperation(value = "Sync Company Rewards Usage")
    public DateSearchResult<LoyaltyActivityLog> syncLoyaltyRewardUsage(@QueryParam("afterDate") long afterDate, @QueryParam("beforeDate") long beforeDate) {
        return rewardService.getLoyaltyUsageReward(afterDate, beforeDate);
    }


}
