package com.fourtwenty.services.resources.pos;

import com.fourtwenty.services.resources.base.AssetResource;
import io.swagger.annotations.Api;

import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Created by mdo on 12/19/15.
 */
@Api("POS - Assets")
@Path("/api/v1/pos/assets")
@Produces(MediaType.APPLICATION_JSON)
public class POSAssetResource extends AssetResource {
}
