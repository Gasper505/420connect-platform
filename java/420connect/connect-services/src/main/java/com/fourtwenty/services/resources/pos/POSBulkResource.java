package com.fourtwenty.services.resources.pos;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.rest.dispensary.requests.company.POSMembershipAddMember;
import com.fourtwenty.core.rest.dispensary.requests.queues.QueueAddMemberBulkRequest;
import com.fourtwenty.core.rest.dispensary.requests.shop.BulkAssetResponse;
import com.fourtwenty.core.rest.dispensary.requests.shop.BulkShopRequest;
import com.fourtwenty.core.rest.dispensary.requests.shop.BulkShopResponse;
import com.fourtwenty.core.rest.dispensary.requests.transaction.TransactionBulkRequest;
import com.fourtwenty.core.security.dispensary.Audit;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.pos.PosBulkService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.io.InputStream;
import java.util.List;


@Api("POS - Data Sync")
@Path("/api/v1/pos/bulk")
@Produces(MediaType.APPLICATION_JSON)
public class POSBulkResource extends BaseResource {

    private static final Logger LOG = LoggerFactory.getLogger(POSBulkResource.class);

    @javax.inject.Inject
    private PosBulkService posBulkService;


    @POST
    @Path("/members")
    @Secured(requiredShop = true, checkTerminal = true)
    @Timed(name = "addNewMemberBulk")
    @Audit(action = "Create Member in Bulk")
    @ApiOperation(value = "Create Member in Bulk")
    public List<Member> addNewMember(@Valid List<POSMembershipAddMember> addMemberlst) {
        return posBulkService.addMember(addMemberlst);
    }

    @POST
    @Path("/queues")
    @Timed(name = "addToQueueBulk")
    @Secured(requiredShop = true, checkTerminal = true)
    @Audit(action = "Add Member to Queue in Bulk")
    @ApiOperation(value = "Add Member to Queue in Bulk")
    public List<Transaction> addToQueue(@Valid List<QueueAddMemberBulkRequest> queueAddMemberLst) {
        return posBulkService.addToQueue(queueAddMemberLst);
    }

    @POST
    @Path("/transactions")
    @Timed(name = "completeTransactionBulk")
    @Secured(requiredShop = true, checkTerminal = true)
    @Audit(action = "Complete Transaction in Bulk")
    @ApiOperation(value = "Complete Transaction in Bulk")
    public List<Transaction> processTransction(@Valid List<TransactionBulkRequest> transactionlst) {
        return posBulkService.processTransaction(transactionlst);
    }

    @POST
    @Path("/syncdata")
    @Timed(name = "syncdata")
    @Secured(requiredShop = true, checkTerminal = true)
    @Audit(action = "Complete adding data in Bulk")
    @ApiOperation(value = "Complete dding data in Bulk")
    public BulkShopResponse syncData(@Valid BulkShopRequest bulkShopRequests) {
        return posBulkService.syncData(bulkShopRequests);
    }

    @POST
    @Path("/signature")
    @Secured
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Audit(action = "Upload signaturesignature", json = false)
    @ApiOperation(value = "Upload signature")
    public List<BulkAssetResponse> uploadPhoto(@FormDataParam("file") InputStream inputStream,
                                               @FormDataParam("name") List<String> name,
                                               @FormDataParam("id") List<String> id,
                                               @FormDataParam("type") List<String> type,
                                               @FormDataParam("signed_type") List<String> signed_type,
                                               @FormDataParam("file") final List<FormDataBodyPart> body) {
        return posBulkService.uploadPhoto(inputStream, name, id, type, signed_type, body);
    }

}