package com.fourtwenty.services.resources.pos;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.security.dispensary.Audit;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.services.resources.base.CashDrawerResource;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 * Created by mdo on 12/7/16.
 */
@Api("POS - Cash Drawers")
@Path("/api/v1/pos/cashdrawers")
@Produces(MediaType.APPLICATION_JSON)
public class POSCashDrawerResource extends CashDrawerResource {

    @GET
    @Path("/{cdSessionId}/paidios/{paidIOItemId}/receipt")
    @Secured(requiredShop = true, checkTerminal = true)
    @Timed(name = "getPaidInOutItemReceipt")
    @Audit(action = "Print PaidIn/Out Receipt")
    @ApiOperation(value = "Print PaidIn/Out Receipt")
    @Produces(MediaType.TEXT_PLAIN)
    public String getPaidInOutItemReceipt(@PathParam("cdSessionId") String cdSessionId, @PathParam("paidIOItemId") String paidIOItemId,
                                          @QueryParam("width") int width) {
        return cashDrawerService.getPaidInOutItemReceipt(cdSessionId, paidIOItemId, width);
    }

}
