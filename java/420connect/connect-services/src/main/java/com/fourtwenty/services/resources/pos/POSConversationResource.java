package com.fourtwenty.services.resources.pos;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.transaction.Conversation;
import com.fourtwenty.core.rest.features.TwilioMessageRequest;
import com.fourtwenty.core.rest.features.TwilioResult;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.mgmt.ConversationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Collections;
import java.util.List;

/**
 * Created by mdo on 10/12/17.
 */
@Api("POS - Conversations")
@Path("/api/v1/pos/conversation")
@Produces(MediaType.APPLICATION_JSON)
public class POSConversationResource {
    private static final Logger LOGGER = LoggerFactory.getLogger(POSConversationResource.class);


    @Inject
    private ConversationService conversationService;

    @POST
    @Path("/send")
    @Timed(name = "sendTwilioMeesage")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Send message through Transaction id")
    public TwilioResult<Conversation> sendTwilioMessage(@Valid TwilioMessageRequest request) {
        final Conversation conversationStatus = conversationService.sendConversation(request);
        TwilioResult result = new TwilioResult();
        result.setStatus(Boolean.TRUE);
        result.setData(conversationStatus);
        return result;

    }

    @GET
    @Path("/messageList/{transactionId}")
    @Timed(name = "messageList")
    @Secured(requiredShop = true)
    @ApiOperation(value = "get Messages list between Employee and Member")
    public TwilioResult<List<Conversation>> messageList(@PathParam("transactionId") String transactionId, @QueryParam("afterDate") long afterDate, @QueryParam("beforeDate") long beforeDate) {
        List<Conversation> transactionConversationList = conversationService.getTransactionConversation(transactionId, afterDate, beforeDate);
        final TwilioResult<List<Conversation>> twilioResult = new TwilioResult<>();
        try {
            if (!transactionConversationList.isEmpty()) {
                twilioResult.setStatus(Boolean.TRUE);
                twilioResult.setData(transactionConversationList);
            } else {
                twilioResult.setStatus(Boolean.FALSE);
                twilioResult.setData(Collections.<Conversation>emptyList());
            }
        } catch (Exception ex) {
            LOGGER.warn("Can't get list of conversations", ex);
            twilioResult.setStatus(Boolean.FALSE);
            twilioResult.setData(Collections.<Conversation>emptyList());
        }
        return twilioResult;
    }
}
