package com.fourtwenty.services.resources.pos;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.customer.Doctor;
import com.fourtwenty.core.rest.dispensary.requests.customer.DoctorAddRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.dispensary.Audit;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.mgmt.DoctorService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/**
 * Created by mdo on 3/30/16.
 */
@Api("POS - Doctor")
@Path("/api/v1/pos/doctors")
@Produces(MediaType.APPLICATION_JSON)
public class POSDoctorResource extends BaseResource {
    @Inject
    DoctorService doctorService;

    @GET
    @Path("/")
    @Timed(name = "getDoctors")
    @Secured
    @ApiOperation(value = "Get Doctors By Dates")
    public SearchResult<Doctor> getDoctors(@QueryParam("afterDate") long afterDate,
                                           @QueryParam("beforeDate") long beforeDate) {
        return doctorService.getDoctorsByDate(afterDate, beforeDate);
    }

    @POST
    @Path("/")
    @Secured(requiredShop = true)
    @Timed(name = "createDoctor")
    @Audit(action = "Create Doctor")
    @ApiOperation(value = "Create Doctor")
    public Doctor createDoctor(@Valid DoctorAddRequest request) {
        request.setActive(true);
        return doctorService.addDoctor(request);
    }

}
