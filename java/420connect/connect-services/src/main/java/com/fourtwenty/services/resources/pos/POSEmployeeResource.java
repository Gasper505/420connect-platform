package com.fourtwenty.services.resources.pos;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.company.EmployeeManifestResult;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.mgmt.EmployeeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 * Created by mdo on 5/7/16.
 */
@Api("POS - Employees")
@Path("/api/v1/pos/employees")
@Produces(MediaType.APPLICATION_JSON)
public class POSEmployeeResource extends BaseResource {
    @Inject
    EmployeeService employeeService;

    @GET
    @Path("/")
    @Timed(name = "getEmployees")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get Employees By Dates")
    public SearchResult<Employee> getEmployees(@QueryParam("afterDate") long afterDate,
                                               @QueryParam("beforeDate") long beforeDate) {
        return employeeService.findEmployees(afterDate, beforeDate);
    }

    @GET
    @Path("/manifest")
    @Timed(name = "getEmployeeManifest")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get Employee Manifest")
    public EmployeeManifestResult getEmployeeManifest(@QueryParam("date") String date) {
        return employeeService.getEmployeeManifest(date);
    }
}
