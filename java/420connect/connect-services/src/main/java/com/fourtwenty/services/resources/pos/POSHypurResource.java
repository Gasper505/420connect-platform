package com.fourtwenty.services.resources.pos;

import com.blaze.clients.hypur.models.CheckedInUserListResponse;
import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.thirdparty.HypurService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 * Created by Gaurav Saini on 09/25/17.
 */
@Api("POS - Hypur")
@Path("/api/v1/pos/hypur")
@Produces(MediaType.APPLICATION_JSON)
public class POSHypurResource {

    @Inject
    HypurService hypurService;

    @GET
    @Path("/profiles")
    @Timed(name = "getHypurCheckinUserProfiles")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get Hypur Check in user profiles")
    public CheckedInUserListResponse getHypurCheckinUserProfiles(@QueryParam("thumnailSize") int thumnailSize) {
        return hypurService.getHypurCheckinUserProfiles(thumnailSize);
    }


}
