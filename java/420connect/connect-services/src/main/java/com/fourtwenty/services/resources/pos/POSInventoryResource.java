package com.fourtwenty.services.resources.pos;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.VendorResult;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.inventory.ProductWeightToleranceService;
import com.fourtwenty.core.services.mgmt.InventoryService;
import com.fourtwenty.core.services.mgmt.ProductCategoryService;
import com.fourtwenty.core.services.mgmt.ProductService;
import com.fourtwenty.core.services.mgmt.VendorService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 * Created by mdo on 1/24/16.
 */
@Api("POS - Inventory")
@Path("/api/v1/pos/inventory")
@Produces(MediaType.APPLICATION_JSON)
public class POSInventoryResource extends BaseResource {
    @Inject
    ProductCategoryService productCategoryService;
    @Inject
    ProductService productService;
    @Inject
    VendorService vendorService;
    @Inject
    InventoryService inventoryService;
    @Inject
    private ProductWeightToleranceService productWeightToleranceService;

    @GET
    @Path("/categories")
    @Secured(requiredShop = true, checkTerminal = true)
    @Timed(name = "getProductCategories")
    @ApiOperation(value = "Get Product Categories By Dates")
    public SearchResult<ProductCategory> getProductCategories(@QueryParam("afterDate") long afterDate,
                                                              @QueryParam("beforeDate") long beforeDate) {
        return productCategoryService.getProductCategories(afterDate, beforeDate);
    }


    @GET
    @Path("/products")
    @Timed(name = "getProducts")
    @Secured(requiredShop = true, checkTerminal = true)
    @ApiOperation(value = "Get Products By Dates")
    public SearchResult<Product> getProducts(@QueryParam("afterDate") long afterDate,
                                             @QueryParam("beforeDate") long beforeDate) {
        return productService.getProducts(afterDate, beforeDate);
    }

    @GET
    @Path("/brands")
    @Timed(name = "getBrands")
    @Secured(requiredShop = true, checkTerminal = true)
    @ApiOperation(value = "Get Brands By Dates")
    public SearchResult<Brand> getBrands(@QueryParam("afterDate") long afterDate,
                                         @QueryParam("beforeDate") long beforeDate) {
        return productService.getBrandsByDates(afterDate, beforeDate);
    }

    @GET
    @Path("/vendors")
    @Timed(name = "getVendors")
    @Secured(requiredShop = true, checkTerminal = true)
    @ApiOperation(value = "Get Vendors By Dates")
    public SearchResult<VendorResult> getVendors(@QueryParam("afterDate") long afterDate,
                                                 @QueryParam("beforeDate") long beforeDate) {
        return vendorService.getVendorsByDate(afterDate, beforeDate);
    }

    @GET
    @Path("/products/batches")
    @Timed(name = "getVendors")
    @Secured(requiredShop = true, checkTerminal = true)
    @ApiOperation(value = "Get Batches By Dates")
    public SearchResult<ProductBatch> getProductBatches(@QueryParam("afterDate") long afterDate,
                                                        @QueryParam("beforeDate") long beforeDate) {
        return inventoryService.getBatchesWithDates(afterDate, beforeDate);
    }

    @GET
    @Path("/tolerances")
    @Timed(name = "getVendors")
    @Secured(requiredShop = true, checkTerminal = true)
    @ApiOperation(value = "Get Vendors By Dates")
    public SearchResult<ProductWeightTolerance> getWeightTolerances(@QueryParam("afterDate") long afterDate,
                                                                    @QueryParam("beforeDate") long beforeDate) {
        return productWeightToleranceService.getWeightTolerances(afterDate, beforeDate);
    }
}
