package com.fourtwenty.services.resources.pos;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.customer.Member;
import com.fourtwenty.core.domain.models.views.MemberLimitedViewResult;
import com.fourtwenty.core.rest.dispensary.requests.company.MembershipUpdateRequest;
import com.fourtwenty.core.rest.dispensary.requests.company.POSMembershipAddMember;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.company.MemberResult;
import com.fourtwenty.core.security.dispensary.Audit;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.pos.POSMembershipService;
import com.google.inject.Inject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/**
 * Created by mdo on 12/13/15.
 */
@Api("POS - Memberships")
@Path("/api/v1/pos/memberships")
@Produces(MediaType.APPLICATION_JSON)
public class POSMembershipResource extends BaseResource {

    @Inject
    POSMembershipService membershipService;

    @GET
    @Path("/")
    @Timed(name = "getMembershipsForShop")
    @Secured(requiredShop = true, checkTerminal = true)
    @ApiOperation(value = "Get Members By Dates for Active Shop")
    public SearchResult<Member> getMembershipsForActiveShop(@QueryParam("beforeDate") long beforeDate,
                                                            @QueryParam("afterDate") long afterDate,
                                                            @QueryParam("start") int start,
                                                            @QueryParam("limit") int limit) {
        SearchResult<Member> results = membershipService.getMembershipsForCompanyDate(beforeDate, afterDate, start, limit);
        return results;
    }

    @POST
    @Path("/")
    @Secured(requiredShop = true, checkTerminal = true)
    @Timed(name = "addNewMember")
    @Audit(action = "Create Member")
    @ApiOperation(value = "Create Member")
    public Member addNewMember(@Valid POSMembershipAddMember addMember) {
        addMember.setTextOptIn(true); // auto opt-in
        addMember.setEmailOptIn(true); // auto opt-in
        return membershipService.addMembership(addMember);
    }

    @PUT
    @Path("/{memberId}")
    @Secured(requiredShop = true, checkTerminal = true)
    @Timed(name = "updateMember")
    @Audit(action = "Update Member")
    @ApiOperation(value = "Update Member")
    public Member updateMember(@PathParam("memberId") String memberId, @Valid MembershipUpdateRequest member) {
        return membershipService.updateMember(memberId, member);
    }

    @GET
    @Path("/limited")
    @Timed(name = "getMembers")
    @Secured(requiredShop = true, checkTerminal = true)
    @ApiOperation(value = "Search Members for limited view")
    @Audit(action = "Search Members for limited view")
    public SearchResult<MemberLimitedViewResult> getMemberships(@QueryParam("term") String term,
                                                                @QueryParam("doctorId") String doctorId,
                                                                @QueryParam("start") int start,
                                                                @QueryParam("limit") int limit,
                                                                @QueryParam("beforeDate") long beforeDate,
                                                                @QueryParam("afterDate") long afterDate) {

        if (StringUtils.isNotBlank(doctorId)) {
            return membershipService.getMembersForDoctorId(doctorId, term, start, limit);
        }
        return membershipService.getMembershipsForActiveShop(term, start, limit,beforeDate,afterDate);
    }

    @GET
    @Path("/{memberId}")
    @Timed(name = "getMembership")
    @Secured(requiredShop = true, checkTerminal = true)
    @ApiOperation(value = "Get Member By Id")
    @Audit(action = "Get Member By Id")
    public MemberResult getMembership(@PathParam("memberId") String memberId) {
        return membershipService.getMembership(memberId);
    }
}
