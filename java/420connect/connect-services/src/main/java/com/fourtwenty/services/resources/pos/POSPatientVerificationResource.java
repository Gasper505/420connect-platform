package com.fourtwenty.services.resources.pos;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.thirdparty.PatientVerificationStatus;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.verificationsite.WebsiteManager;
import com.google.inject.Inject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.sf.json.JSONObject;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 * Created by Gaurav Saini on 16/5/17.
 */

@Api("POS - Patient Verification")
@Path("/api/v1/pos/patient")
@Produces(MediaType.APPLICATION_JSON)
public class POSPatientVerificationResource extends BaseResource {
    @Inject
    PatientVerificationStatus patientVerificationStatus;

    @Inject
    WebsiteManager websiteManager;

    /**
     * @param verificationCode
     * @param webSiteType
     * @return PatientVerificationStatus
     */
    @GET
    @Path("/verify")
    @Timed(name = "verifyPatient")
    @ApiOperation(value = "pos - verify patient rec code")
    public PatientVerificationStatus verifyPatient(@QueryParam("recNo") String verificationCode,
                                                   @QueryParam("websiteType") String webSiteType) {

        String websiteResponse = websiteManager.getWebsiteDataForType(webSiteType, verificationCode);

        JSONObject jsonObject = JSONObject.fromObject(websiteResponse);
        patientVerificationStatus.setPatientName(jsonObject.getString("name"));
        patientVerificationStatus.setRecommendationExpDate(jsonObject.getString("latest_medical_recommendation_expiration"));
        patientVerificationStatus.setRecommendationIssueDate(jsonObject.getString("latest_medical_recommendation_publication"));
        patientVerificationStatus.setRecommendationStatus(jsonObject.getString("latest_medical_recommendation_status"));
        return patientVerificationStatus;
    }
}
