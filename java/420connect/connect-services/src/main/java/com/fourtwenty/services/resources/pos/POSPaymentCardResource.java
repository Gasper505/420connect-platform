package com.fourtwenty.services.resources.pos;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.paymentcard.PaymentCardType;
import com.fourtwenty.core.event.BlazeEventBus;
import com.fourtwenty.core.event.clover.CloverEvent;
import com.fourtwenty.core.event.mtrac.MtracVirtualEvent;
import com.fourtwenty.core.event.paymentcard.LoadPaymentCardEvent;
import com.fourtwenty.core.event.paymentcard.LoadPaymentCardResult;
import com.fourtwenty.core.event.paymentcard.PaymentCardBalanceCheckEvent;
import com.fourtwenty.core.event.paymentcard.PaymentCardBalanceCheckResult;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.rest.paymentcard.*;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.integrations.clover.response.CloverPaymentResponse;
import com.fourtwenty.integrations.linx.subscriber.LinxSubscriber;
import com.fourtwenty.integrations.mtrac.response.MtracTransactionResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.validation.Valid;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Api("POS - Assets")
@Path("/api/v1/pos/paymentcard")
@Produces(MediaType.APPLICATION_JSON)
public class POSPaymentCardResource extends BaseResource {
    private static final Log LOG = LogFactory.getLog(LinxSubscriber.class);

    @Inject
    private BlazeEventBus eventBus;

    private ConnectAuthToken token;

    @Inject
    public POSPaymentCardResource(Provider<ConnectAuthToken> tokenProvider) {
        token = tokenProvider.get();
    }

    @POST
    @Path("/load")
    @Timed(name = "loadPaymentCard")
    @Secured
    @ApiOperation(value = "Load a Payment Card")
    public LoadPaymentCardResponse loadPaymentCard(@Valid LoadPaymentCardRequest request) {


        if (StringUtils.isBlank(request.getPaymentCardNumber()) && StringUtils.isNotBlank(request.getLinxCardNumber())) {
            request.setPaymentCardNumber(request.getLinxCardNumber());
            request.setType(PaymentCardType.Linx);
        }

        LOG.info("Card Info: " + request);

        LoadPaymentCardEvent event = new LoadPaymentCardEvent();
        event.setPayload(token, request);
        eventBus.post(event);
        LoadPaymentCardResult result = event.getResponse();
        return new LoadPaymentCardResponse(result);
    }

    @POST
    @Path("/balance")
    @Timed(name = "paymentCardBalanceCheck")
    @Secured
    @ApiOperation(value = "Check balance of a Payment Card")
    public PaymentCardBalanceCheckResponse paymentCardBalanceCheck(@Valid PaymentCardBalanceCheckRequest request) {


        // Backward compatibility check
        if (StringUtils.isBlank(request.getPaymentCardNumber()) && StringUtils.isNotBlank(request.getLinxCardNumber())) {
            request.setPaymentCardNumber(request.getLinxCardNumber());
            request.setType(PaymentCardType.Linx);
        }

        LOG.info("Card Info: " + request);

        PaymentCardBalanceCheckEvent event = new PaymentCardBalanceCheckEvent();
        event.setPayload(token, request);
        eventBus.post(event);
        PaymentCardBalanceCheckResult result = event.getResponse();
        return new PaymentCardBalanceCheckResponse(result);
    }

    @POST
    @Path("/pay")
    @Timed(name = "Payment via Mtrac or Clover")
    @Secured
    @ApiOperation(value = "Payment via Mtrac or Clover")
    public Object pay(@Valid BasePaymentRequest request) {
        //String json = JsonSerializer.toJson(request);
        //LOG.info("payment request: " + json);
        if (request.getPaymentType() == BasePaymentRequest.PaymentType.MTRAC) {
            MtracVirtualEvent event = new MtracVirtualEvent();
            event.setPayload(token.getCompanyId(), token.getShopId(), request.getMtracRequest());
            eventBus.post(event);
            event.setResponseTimeout(30000);
            MtracTransactionResponse response = (MtracTransactionResponse) event.getResponse();
            if (response != null) {
                if (response.isSuccess() == Boolean.FALSE) {
                    throw new BlazeInvalidArgException(response.getStatus(), response.getMessage());
                } else {
                    return response;
                }
            } else {
                throw new BlazeInvalidArgException("Mtrac", "Error in mtrac payment api");
            }
        } else {
            CloverEvent event = new CloverEvent();
            event.setPayload(token.getCompanyId(), token.getShopId(), request.getCloverRequest());
            event.setTerminalId(token.getTerminalId());
            event.setEmployeeId(token.getActiveTopUser().getUserId());
            eventBus.post(event);
            CloverPaymentResponse response = (CloverPaymentResponse)event.getResponse();
            if (response != null) {
                 if (response.getResult().equalsIgnoreCase("DECLINED")) {
                    throw new BlazeInvalidArgException(response.getResult(), "Clover payment is not approved");
                } else {
                    return response;
                }
            } else {
                throw new BlazeInvalidArgException("Clover", "Error in clover payment api");
            }
        }
    }

}
