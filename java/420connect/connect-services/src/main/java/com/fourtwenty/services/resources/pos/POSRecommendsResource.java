package com.fourtwenty.services.resources.pos;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.engine.SuggestedPromotion;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.engine.RecommendEngineService;
import com.google.inject.Inject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.validation.Valid;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Created by mdo on 1/25/18.
 */
@Api("POS - Recommendation Engine Resource")
@Path("/api/v1/pos/recommends")
@Produces(MediaType.APPLICATION_JSON)
public class POSRecommendsResource extends BaseResource {
    @Inject
    RecommendEngineService recommendEngineService;

    @POST
    @Path("/promotions")
    @Timed(name = "getSuggestedPromotions")
    @Secured(requiredShop = true, checkTerminal = false)
    @ApiOperation(value = "Get Suggested Promotions")
    public SearchResult<SuggestedPromotion> getSuggestedPromotions(@Valid Transaction transaction) {
        SearchResult<SuggestedPromotion> results = recommendEngineService.getSuggestedPromotions(transaction);
        return results;
    }
}
