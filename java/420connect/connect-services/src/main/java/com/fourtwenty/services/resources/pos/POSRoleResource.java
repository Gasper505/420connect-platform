package com.fourtwenty.services.resources.pos;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.company.Role;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.mgmt.RoleService;
import com.google.inject.Inject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 * Created by mdo on 6/15/16.
 */

@Api("POS - Roles")
@Path("/api/v1/pos/roles")
@Produces(MediaType.APPLICATION_JSON)
public class POSRoleResource extends BaseResource {
    @Inject
    RoleService roleService;

    @GET
    @Path("/")
    @Secured
    @Timed(name = "getRoles")
    @ApiOperation(value = "Get Roles By Dates")
    public SearchResult<Role> getRoles(@QueryParam("afterDate") long afterDate, @QueryParam("beforeDate") long beforeDate) {
        return roleService.getRoles(afterDate, beforeDate);
    }
}
