package com.fourtwenty.services.resources.pos;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.company.Terminal;
import com.fourtwenty.core.domain.models.customer.StatusRequest;
import com.fourtwenty.core.domain.models.generic.Address;
import com.fourtwenty.core.domain.models.receipt.ReceiptInfo;
import com.fourtwenty.core.domain.models.transaction.Cart;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.models.transaction.TransactionLimitedResult;
import com.fourtwenty.core.rest.common.EmailNotificationRequest;
import com.fourtwenty.core.rest.common.EmailRequest;
import com.fourtwenty.core.rest.dispensary.requests.queues.*;
import com.fourtwenty.core.rest.dispensary.requests.shop.SendReceiptRequest;
import com.fourtwenty.core.rest.dispensary.requests.terminals.DeliveryLocationRequest;
import com.fourtwenty.core.rest.dispensary.requests.terminals.TerminalAddRequest;
import com.fourtwenty.core.rest.dispensary.requests.terminals.TerminalUpdateNameRequest;
import com.fourtwenty.core.rest.dispensary.requests.terminals.UpdateTerminalLocationRequest;
import com.fourtwenty.core.rest.dispensary.requests.transaction.AddOrderTagsRequest;
import com.fourtwenty.core.rest.dispensary.requests.transaction.OrderScheduleUpdateRequest;
import com.fourtwenty.core.rest.dispensary.requests.transaction.PaymentOptionUpdateRequest;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.ListResult;
import com.fourtwenty.core.rest.dispensary.results.MemberSalesDetail;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.queue.CompositeMemberTransResult;
import com.fourtwenty.core.rest.dispensary.results.shop.ConsumerCartResult;
import com.fourtwenty.core.security.dispensary.Audit;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.mgmt.QueuePOSService;
import com.fourtwenty.core.services.mgmt.ShopService;
import com.fourtwenty.core.services.pos.TerminalService;
import com.fourtwenty.core.services.store.ConsumerCartService;
import com.fourtwenty.core.util.BLStringUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataParam;
import javax.ws.rs.core.StreamingOutput;
import com.google.common.io.ByteStreams;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.OutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by mdo on 9/23/15.
 */
@Api("POS - Shops")
@Path("/api/v1/pos/shops")
@Produces(MediaType.APPLICATION_JSON)
public class POSShopResource extends BaseResource {
    @Inject
    ShopService shopService;
    @Inject
    QueuePOSService queuePOSService;
    @Inject
    TerminalService terminalService;
    @Inject
    private ConsumerCartService consumerCartService;

    @GET
    @Path("/")
    @Timed(name = "getShops")
    @Secured
    @ApiOperation(value = "Get Shops")
    public ListResult<Shop> getShops() {
        return shopService.list();
    }

    @POST
    @Path("/queues/{queueName}")
    @Timed(name = "addToQueue")
    @Secured(requiredShop = true, checkTerminal = true)
    @Audit(action = "Add Member to Queue")
    @ApiOperation(value = "Add Member to Queue")
    public Transaction addToQueue(@PathParam("queueName") String queueName, @Valid QueueAddMemberRequest request) {
        return queuePOSService.addToQueue(queueName, request,-1);
    }

    @POST
    @Path("/queues/{queueName}/anonymous")
    @Timed(name = "addToQueueAnonymous")
    @Secured(requiredShop = true, checkTerminal = true)
    @Audit(action = "Add Anonymous Member to Queue")
    @ApiOperation(value = "Add Anonymous Member to Queue")
    public CompositeMemberTransResult addToQueueAnonymous(@PathParam("queueName") String queueName, @Valid AnonymousQueueAddRequest request) {
        return queuePOSService.addAnonymousUserToQueue(queueName,request);
    }

    // Transactions
    @GET
    @Path("/transactions")
    @Timed(name = "getTransationsForShopQueue")
    @Secured(requiredShop = true, checkTerminal = true)
    @ApiOperation(value = "Get Transactions For Active Shop")
    public SearchResult<Transaction> getTransationsForShopQueue(@QueryParam("afterDate") long afterDate, @QueryParam("beforeDate") long beforeDate) {
        return queuePOSService.getTransactions(afterDate, beforeDate);
    }

    @GET
    @Path("/transactions/members/{memberId}")
    @Timed(name = "getTransationsForMembers")
    @Secured(requiredShop = true, checkTerminal = true)
    @ApiOperation(value = "Get Transactions For Members")
    public DateSearchResult<Transaction> getTransationsForMembers(@PathParam("memberId") String memberId, @QueryParam("afterDate") long afterDate, @QueryParam("beforeDate") long beforeDate) {
        return queuePOSService.getTransactionsForMembers(memberId, afterDate, beforeDate);
    }


    @GET
    @Path("/transactions/search")
    @Timed(name = "findRecentTransactions")
    @Secured(requiredShop = true, checkTerminal = true)
    @ApiOperation(value = "Find Recent Transactions")
    public SearchResult<Transaction> findRecentTransactions(@QueryParam("term") String term) {
        // Term is transNo for now
        return queuePOSService.findRecentTransactions(term);
    }

    @DELETE
    @Path("/transactions/{transactionId}")
    @Timed(name = "deleteTransaction")
    @Secured(requiredShop = true, checkTerminal = true)
    @Audit(action = "Delete Transaction From Queue")
    @ApiOperation(value = "Delete Transaction from Queue")
    public Response deleteTransaction(@PathParam("transactionId") String transactionId, @Valid TransactionDeleteRequest request) {
        queuePOSService.deleteFromQueue(transactionId, request);
        return ok();
    }

    @POST
    @Path("/transactions/{transactionId}")
    @Timed(name = "completeTransaction")
    @Secured(requiredShop = true, checkTerminal = true)
    @Audit(action = "Complete Transaction")
    @ApiOperation(value = "Complete Transaction")
    public Transaction processTransction(@PathParam("transactionId") String transactionId, @Valid Transaction transaction) {
        return queuePOSService.completeTransaction(transactionId, transaction, false);
    }

    @POST
    @Path("/transactions/{transactionId}/markAsPaid")
    @Timed(name = "markAsPaid")
    @Secured(requiredShop = true, checkTerminal = true)
    @ApiOperation(value = "Mark As Paid")
    @Audit(action = "Mark As Paid")
    public Transaction markAsPaid(@PathParam("transactionId") String transactionId,
                                  @QueryParam("paymentOption") Cart.PaymentOption paymentOption) {
        return queuePOSService.markAsPaid(transactionId, paymentOption);
    }

    @POST
    @Path("/transactions/{transactionId}/markAsUnpaid")
    @Timed(name = "markAsUnpaid")
    @Secured(requiredShop = true, checkTerminal = true)
    @ApiOperation(value = "Mark As Unpaid")
    @Audit(action = "Mark As Unpaid")
    public Transaction markAsUnpaid(@PathParam("transactionId") String transactionId,
                                    @QueryParam("paymentOption") Cart.PaymentOption paymentOption) {
        return queuePOSService.markAsUnpaid(transactionId, paymentOption);
    }

    // No need to audit as we're are just preparing cart
    @POST
    @Path("/transactions/{transactionId}/prepare")
    @Timed(name = "prepareCart")
    @Secured(requiredShop = true, checkTerminal = true)
    @ApiOperation(value = "Prepare Cart")
    public Transaction prepareCart(@PathParam("transactionId") String transactionId, @Valid Transaction transaction) {
        return queuePOSService.prepareCart(transactionId, transaction);
    }

    @POST
    @Path("/transactions/{transactionId}/sign")
    @Secured
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Audit(action = "Sign Transaction", json = false)
    @ApiOperation(value = "Sign Transaction")
    public Transaction signTransaction(@PathParam("transactionId") String transactionId, @FormDataParam("file") InputStream inputStream,
                                       @FormDataParam("name") String name,
                                       @FormDataParam("file") final FormDataBodyPart body) {
        String mimeType = body.getMediaType().toString();
        return queuePOSService.signTransaction(transactionId, inputStream, name, mimeType);
    }

    @POST
    @Path("/transactions/{transactionId}/hold")
    @Timed(name = "holdTransaction")
    @Secured(requiredShop = true, checkTerminal = true)
    @Audit(action = "Hold Transaction")
    @ApiOperation(value = "Hold Transaction")
    public Transaction holdTransaction(@PathParam("transactionId") String transactionId, @Valid Transaction transaction) {
        return queuePOSService.holdTransaction(transactionId, transaction);
    }

    @POST
    @Path("/transactions/{transactionId}/claim")
    @Timed(name = "claimTransaction")
    @Secured(requiredShop = true, checkTerminal = true)
    @Audit(action = "Claim Transaction")
    @ApiOperation(value = "Claim Transaction")
    public Transaction claimTransaction(@PathParam("transactionId") String transactionId) {
        return queuePOSService.claimTransaction(transactionId);
    }

    @POST
    @Path("/transactions/{transactionId}/employee")
    @Timed(name = "reassignTransactionEmployee")
    @Secured(requiredShop = true, checkTerminal = true)
    @Audit(action = "Reassign Transaction Employee")
    @ApiOperation(value = "Reassign Transaction Employee")
    public Transaction reassignTransactionEmployee(@PathParam("transactionId") String transactionId, @Valid EmployeeReassignRequest request) {
        return queuePOSService.reassignEmployee(transactionId, request);
    }

    @GET
    @Path("/transactions/{transactionId}/exit")
    @Timed(name = "exitTransaction")
    @Secured(requiredShop = true, checkTerminal = true)
    @Audit(action = "Exit Transaction")
    @ApiOperation(value = "Exit Transaction")
    public Transaction exitTransaction(@PathParam("transactionId") String transactionId) {
        return queuePOSService.exitTransaction(transactionId);
    }

    @GET
    @Path("/transactions/{transactionId}/start")
    @Timed(name = "startTransaction")
    @Secured(requiredShop = true, checkTerminal = true)
    @Audit(action = "Start Transaction")
    @ApiOperation(value = "Start Transaction")
    public Transaction startTransaction(@PathParam("transactionId") String transactionId) {
        return queuePOSService.startTransaction(transactionId,true);
    }

    @GET
    @Path("/transactions/{transactionId}/start/dispatch")
    @Timed(name = "startTransactionDispatch")
    @Secured(requiredShop = true, checkTerminal = true)
    @Audit(action = "Start Transaction Dispatch")
    @ApiOperation(value = "Start Transaction Dispatch")
    public Transaction startTransactionDispatch(@PathParam("transactionId") String transactionId) {
        return queuePOSService.startTransaction(transactionId,false);
    }

    @GET
    @Path("/transactions/{transactionId}/unassign")
    @Timed(name = "unassignTransaction")
    @Secured(requiredShop = true, checkTerminal = true)
    @Audit(action = "Unassign Transaction")
    @ApiOperation(value = "Unassign Transaction")
    public Transaction unassignTransaction(@PathParam("transactionId") String transactionId) {
        return queuePOSService.unassignedTransaction(transactionId);
    }

    @GET
    @Path("/transactions/{transactionId}/stop")
    @Timed(name = "stopTransaction")
    @Secured(requiredShop = true, checkTerminal = true)
    @Audit(action = "Stop Transaction")
    @ApiOperation(value = "Stop Transaction")
    public Transaction stopTransaction(@PathParam("transactionId") String transactionId) {
        return queuePOSService.stopTransaction(transactionId);
    }

    @GET
    @Path("/transactions/{transactionId}/refund/options")
    @Timed(name = "getRefundPaymentOptions")
    @Secured(requiredShop = true, checkTerminal = true)
    @Audit(action = "Get Refund Payment Options")
    @ApiOperation(value = "Get Refund Payment Options")
    public LinkedHashMap<String, String> getRefundPaymentOptions(@PathParam("transactionId") String transactionId) {
        List<Cart.PaymentOption> paymentOptions = queuePOSService.getRefundPaymentOptions(transactionId);
        LinkedHashMap<String, String> keyValuePairs = new LinkedHashMap<>();
        paymentOptions
                .stream()
                .forEach(po -> keyValuePairs.put(po.name(), BLStringUtils.splitCamelCase(po.name())));
        return keyValuePairs;
    }

    @POST
    @Path("/transactions/{transactionId}/refund")
    @Timed(name = "refundTransaction")
    @Secured(requiredShop = true, checkTerminal = true)
    @Audit(action = "Refund Transaction")
    @ApiOperation(value = "Refund Transaction")
    public Transaction refundTransaction(@PathParam("transactionId") String transactionId, @Valid RefundRequest request) {
        return queuePOSService.refundTransaction(transactionId, request);
    }

    @POST
    @Path("/transactions/{transactionId}/refund/prepare")
    @Timed(name = "prepareRefundTransaction")
    @Secured(requiredShop = true, checkTerminal = true)
    @Audit(action = "Prepare Refund Transaction")
    @ApiOperation(value = "Prepare Refund Transaction")
    public Transaction prepareRefundTransaction(@PathParam("transactionId") String transactionId, @Valid RefundRequest request) {
        return queuePOSService.prepareRefundCart(transactionId, request);
    }


    @POST
    @Path("/transactions/{transactionId}/receipt")
    @Timed(name = "sendTransactionReceipt")
    @Secured(requiredShop = true, checkTerminal = true)
    @Audit(action = "Send Transaction Receipt")
    public Response sendTransactionReceipt(@PathParam("transactionId") String transactionId, @Valid SendReceiptRequest request,
                                           @QueryParam("isDeliveryManifest") boolean isDeliveryManifest) {
        queuePOSService.sendTransactionReceipt(transactionId, request, isDeliveryManifest);
        return ok();
    }

    @POST
    @Path("/transactions/{transactionId}/submitMetrc")
    @Timed(name = "sendTransactionReceiptMetrc")
    @Secured(requiredShop = true, checkTerminal = true)
    @Audit(action = "Send Transaction Receipt to Metrc")
    public Response sendTransactionReceiptMetrc(@PathParam("transactionId") String transactionId) {
        queuePOSService.sendToMetrc(transactionId);
        return ok();
    }

    @GET
    @Path("/transactions/resubmitMetrc")
    @Timed(name = "resendTransactionReceiptMetrc")
    @Secured(requiredShop = true, checkTerminal = true)
    @Audit(action = "Resend send Transaction Receipts to Metrc")
    public Response resendTransactionReceiptMetrc(@QueryParam("startDate") long startDate, @QueryParam("endDate") long endDate, @QueryParam("errors") boolean errors) {
        queuePOSService.resendToMetrc(startDate, endDate,errors);
        return ok();
    }

    // Incoming Orders
    @POST
    @Path("/transactions/incoming/{consumerCartId}/members/accept")
    @Timed(name = "acceptIncomingMember")
    @Secured(requiredShop = true, checkTerminal = true)
    @Audit(action = "Incoming Orders -- Accept New Member")
    @ApiOperation(value = "Accept new incoming member")
    public ConsumerCartResult acceptIncomingMember(@PathParam("consumerCartId") String consumerCartId, @Valid ConsumerCartResult consumerCart) {
        return queuePOSService.acceptIncomingMemberStatus(consumerCartId, consumerCart);
    }

    @POST
    @Path("/transactions/incoming/{consumerCartId}/members/decline")
    @Timed(name = "declineIncomingMember")
    @Secured(requiredShop = true, checkTerminal = true)
    @Audit(action = "Incoming Orders -- Decline New Member")
    @ApiOperation(value = "Decline new incoming member")
    public ConsumerCartResult declineIncomingMember(@PathParam("consumerCartId") String consumerCartId, @Valid DeclineRequest request) {
        return queuePOSService.declineIncomingMemberStatus(consumerCartId, request);
    }

    @POST
    @Path("/transactions/incoming/{consumerCartId}/orders/undo")
    @Timed(name = "undoRejectedOrder")
    @Secured(requiredShop = true)
    @Audit(action = "Incoming Orders -- Undo Rejected Order")
    @ApiOperation(value = "Undo Rejected incoming member")
    public ConsumerCartResult undoRejectedOrder(@PathParam("consumerCartId") String consumerCartId) {
        return queuePOSService.undoRejectedIncomingOrder(consumerCartId);
    }

    @POST
    @Path("/transactions/incoming/{consumerCartId}/orders/accept")
    @Timed(name = "acceptIncomingOrder")
    @Secured(requiredShop = true, checkTerminal = true)
    @Audit(action = "Incoming Orders -- Accept New Member")
    @ApiOperation(value = "Accept new incoming member")
    public Transaction acceptIncomingOrder(@PathParam("consumerCartId") String consumerCartId, @Valid EmployeeReassignRequest request) {
        return queuePOSService.acceptIncomingOrder(consumerCartId, request);
    }

    @POST
    @Path("/transactions/incoming/{consumerCartId}/orders/decline")
    @Timed(name = "declineIncomingOrder")
    @Secured(requiredShop = true, checkTerminal = true)
    @Audit(action = "Incoming Orders -- Decline incoming order")
    @ApiOperation(value = "Decline new incoming order")
    public ConsumerCartResult declineIncomingOrder(@PathParam("consumerCartId") String consumerCartId, @Valid DeclineRequest request) {
        return queuePOSService.declineIncomingOrder(consumerCartId, request);
    }

    @POST
    @Path("/transactions/incoming/{consumerCartId}/orders/tracking")
    @Timed(name = "updateTrackingStatus")
    @Secured(requiredShop = true, checkTerminal = true)
    @Audit(action = "Incoming Orders -- Update tracking status")
    @ApiOperation(value = "Update tracking status")
    public ConsumerCartResult updateTrackingStatus(@PathParam("consumerCartId") String consumerCartId,
                                                   @Valid ConsumerOrderUpdateETAStatusRequest request) {
        return queuePOSService.updateTrackingStatus(consumerCartId, request);
    }

    @POST
    @Path("/transactions/{transactionId}/unlock")
    @Timed(name = "unlockTransaction")
    @Secured(requiredShop = true)
    @Audit(action = "Unlock transaction")
    @ApiOperation(value = "Unlock transaction")
    public Transaction unlockTransaction(@PathParam("transactionId") String transactionId, @Valid StatusRequest request) {
        return queuePOSService.unlockTransaction(transactionId, request);
    }

    // TERMINAL RELATED STUFF
    @GET
    @Path("/{shopId}/terminals")
    @Timed(name = "getAvailableTerminals")
    @Secured
    public SearchResult<Terminal> getAvailableTerminals(@PathParam("shopId") String shopId) {
        return terminalService.getAvailableTerminals(shopId);
    }

    @POST
    @Path("/{shopId}/terminals")
    @Timed(name = "AddTerminal")
    @Secured
    @Audit(action = "Create Terminal")
    public Terminal addTerminal(@PathParam("shopId") String shopId, TerminalAddRequest request) {
        return terminalService.addTerminal(shopId, request);
    }

    @POST
    @Path("/{shopId}/terminals/{terminalId}")
    @Timed(name = "updateTerminalName")
    @Secured
    @Audit(action = "Update Terminal")
    public Response updateTerminalName(@PathParam("shopId") String shopId, @PathParam("terminalId") String terminalId, TerminalUpdateNameRequest request) {
        terminalService.updateTerminalName(shopId, terminalId, request);
        return ok();
    }

    @POST
    @Path("/{shopId}/terminals/{terminalId}/loc")
    @Timed(name = "updateTerminalLocation")
    @Secured(requiredShop = true)
    @Audit(action = "Update Terminal Location")
    public Response updateTerminalLocation(@PathParam("shopId") String shopId, @PathParam("terminalId") String terminalId, UpdateTerminalLocationRequest request) {
        terminalService.updateCurrentTerminalLocation(request);
        return ok();
    }

    @POST
    @Path("/transactions/{transactionId}/queueType")
    @Timed(name = "changeQueueType")
    @Secured(requiredShop = true, checkTerminal = true)
    @Audit(action = "Change Queue Type for Transaction")
    @ApiOperation(value = "Change Queue Type for Transaction")
    public Transaction changeQueueType(@PathParam("transactionId") String transactionId, @Valid Transaction transaction) {
        return queuePOSService.changeQueueType(transactionId, transaction);
    }

    @POST
    @Path("/transactions/{transactionId}/sendToFulfill")
    @Timed(name = "setTransactionToFulfill")
    @Secured(requiredShop = true)
    @Audit(action = "Send to Fulfillment")
    @ApiOperation(value = "Set to Transaction to Fulfill")
    public Transaction setTransactionToFulfill(@PathParam("transactionId") String transactionId, @Valid Transaction transaction) {
        return queuePOSService.setTransactionToFulfill(transaction, transactionId);
    }

    @POST
    @Path("/transactions/{transactionId}/startRoute")
    @Timed(name = "updateDeliveryStartRoute")
    @Secured(requiredShop = true)
    @Audit(action = "Start routing")
    @ApiOperation(value = "Update Delivery starting route")
    public Transaction updateDeliveryStartRoute(@PathParam("transactionId") String transactionId, @Valid DeliveryLocationRequest locationRequest) {
        return queuePOSService.updateDeliveryStartRoute(transactionId, locationRequest);
    }

    @POST
    @Path("/transactions/{transactionId}/endRoute")
    @Timed(name = "updateDeliveryEndRoute")
    @Secured(requiredShop = true)
    @Audit(action = "End Routing")
    @ApiOperation(value = "Update Delivery ending route")
    public Transaction updateDeliveryEndRoute(@PathParam("transactionId") String transactionId, @Valid DeliveryLocationRequest locationRequest) {
        return queuePOSService.updateDeliveryEndRoute(transactionId, locationRequest);
    }

    @PUT
    @Path("/transactions/{transactionId}/memo")
    @Timed(name = "updateTransactionMemo")
    @Secured(requiredShop = true)
    @Audit(action = "Transaction - Update Transaction Memo")
    @ApiOperation(value = "Update Transaction Memo")
    public Transaction updateTransactionMemo(@PathParam("transactionId") String transactionId, @Valid UpdateTransMemoRequest request) {
        return queuePOSService.updateTransactionNote(transactionId, request);
    }

    @GET
    @Path("/transactions/{transactionId}/receipt")
    @Timed(name = "receiptTransaction")
    @Secured(requiredShop = true, checkTerminal = true)
    @Audit(action = "Receipt Transaction")
    @ApiOperation(value = "Receipt Transaction")
    @Produces(MediaType.TEXT_PLAIN)
    public String receiptTransaction(@PathParam("transactionId") String transactionId,
                                     @QueryParam("receiptType") ReceiptInfo.ReceiptType receiptType,
                                     @QueryParam("width") int width) {
        return queuePOSService.getTransactionReceipt(transactionId, width, receiptType);
    }

    @POST
    @Path("/prepareConsumerCart")
    @Timed(name = "prepareConsumerCart")
    @ApiOperation(value = "Prepare Consumer Cart")
    @Secured(requiredShop = true)
    @Audit(action = "Transaction - Prepare Consumer Cart")
    public ConsumerCartResult prepareConsumerCart(ConsumerCartResult incomingCart) {
        return consumerCartService.prepareConsumerCart(incomingCart);
    }

    @POST
    @Path("/dailySummaryData")
    @Timed(name = "dailySummaryData")
    @ApiOperation(value = "Daily Summary Data")
    @Secured(requiredShop = true)
    @Audit(action = "Daily Summary Data")
    public Response sendSummaryDataByEmail(@Valid EmailNotificationRequest request, @QueryParam("shopId") String shopId) {
        shopService.sendDailySummaryDataByEmail(request, shopId);
        return ok();
    }

    @GET
    @Path("/{memberId}/salesDetails")
    @Timed(name = "getMemberSalesDetail")
    @Secured(requiredShop = true)
    @Audit(action = "Get sales by member details")
    @ApiOperation(value = "Get sales by member details")
    public MemberSalesDetail getMemberSalesDetail(@PathParam("memberId") String memberId) {
        return queuePOSService.getMemberSalesDetail(memberId);
    }

    @POST
    @Path("/lowInventoryNotification")
    @Timed(name = "lowInventoryNotification")
    @ApiOperation(value = "Low Inventory Notification")
    @Secured(requiredShop = true)
    @Audit(action = "Low Inventory Notification")
    public Response sendLowInventoryNotificationByEmail(@Valid EmailRequest request) {
        shopService.sendLowInventoryNotificationByEmail(request);
        return ok();
    }

    @POST
    @Path("/transactions/{transactionId}/holdUnAssigned")
    @Timed(name = "holdUnAssignedTransaction")
    @Secured(requiredShop = true, checkTerminal = true)
    @Audit(action = "Hold Unassigned Transaction")
    @ApiOperation(value = "Hold Unassigned Transaction")
    public Transaction holdUnAssignedTransaction(@PathParam("transactionId") String transactionId, @Valid Transaction transaction) {
        return queuePOSService.holdUnAssignedTransaction(transactionId, transaction);
    }

    @POST
    @Path("/addOrUpdateOrderTag/{transactionId}")
    @Timed(name = "addOrUpdateOrderTag")
    @Secured(requiredShop = true)
    @Audit(action = "Add or update order tags")
    @ApiOperation(value = "Add or update order tags")
    public Transaction addOrUpdateOrderTag(@PathParam("transactionId") String transactionId, @Valid AddOrderTagsRequest request) {
        return queuePOSService.addOrUpdateOrderTag(transactionId, request);
    }

    @POST
    @Path("/transactions/{transactionId}/packedBy")
    @Timed(name = "updatePackedByForTransaction")
    @Secured(requiredShop = true, checkTerminal = true)
    @Audit(action = "Update Pack By Employee")
    @ApiOperation(value = "Update Pack By Employee")
    public Transaction updatePackedByForTransaction(@PathParam("transactionId") String transactionId, @Valid EmployeePackByRequest request) {
        return queuePOSService.updatePackedBy(transactionId, request);
    }

    @POST
    @Path("/transactions/{transactionId}/schedule")
    @Timed(name = "updateScheduleTimeForTransaction")
    @Secured(requiredShop = true, checkTerminal = true)
    @Audit(action = "Update schedule pickup/delivery time for transaction")
    @ApiOperation(value = "Update schedule pickup/delivery time for transaction")
    public Transaction updateScheduleTimeForTransaction(@PathParam("transactionId") String transactionId, @Valid OrderScheduleUpdateRequest request) {
        return queuePOSService.updateScheduleTimeForTransaction(transactionId, request);

    }

    @POST
    @Path("/transactions/{transactionId}/address")
    @Timed(name = "updateDeliveryAddress")
    @Secured(requiredShop = true, checkTerminal = true)
    @Audit(action = "Update delivery address for transaction")
    @ApiOperation(value = "Update delivery address for transaction")
    public Transaction updateDeliveryAddress(@PathParam("transactionId") String transactionId, @Valid Address request) {
        return queuePOSService.updateDeliveryAddress(transactionId, request);

    }

    @GET
    @Path("/transactions/{transactionId}/refund/paymentOptions")
    @Timed(name = "getRefundPaymentOptionList")
    @Secured(requiredShop = true, checkTerminal = true)
    @Audit(action = "Get Refund Payment Option List")
    @ApiOperation(value = "Get Refund Payment Option List")
    public List<LinkedHashMap<String, String>>  getRefundPaymentOptionsList(@PathParam("transactionId") String transactionId) {
        List<Cart.PaymentOption> paymentOptions = queuePOSService.getRefundPaymentOptions(transactionId);
        Set<Cart.PaymentOption> paymentOptionSet = new HashSet<>(paymentOptions);

        List<LinkedHashMap<String, String>> keyList = new ArrayList<>();
        keyList.addAll(paymentOptionSet
                .stream().map(paymentOption -> {
                    LinkedHashMap<String, String> keyValuePairs = new LinkedHashMap<>();
                    keyValuePairs.put("paymentOption", paymentOption.name());
                    keyValuePairs.put("name", BLStringUtils.splitCamelCase(paymentOption.name()));
                    return keyValuePairs;
                }).collect(Collectors.toList()));
        return keyList;
    }

    @POST
    @Path("/transactions/{transactionId}/item/{orderItemId}/finalize")
    @Timed(name = "Finalize Item Cart")
    @Secured(requiredShop = true, checkTerminal = true)
    @Audit(action = "Finalize Item Cart")
    @ApiOperation(value = "Finalize Item Cart")
    public ConsumerCartResult finalizeItemCart(@PathParam("transactionId") String transactionId, @PathParam("orderItemId") String orderItemId, ConsumerCartResult incomingCart) {
        return consumerCartService.finalizeItemCart(transactionId, orderItemId, incomingCart);
    }

    @POST
    @Path("/transactions/{transactionId}/item/{orderItemId}/unfinalize")
    @Timed(name = "Unfinalize Item Cart")
    @Secured(requiredShop = true, checkTerminal = true)
    @Audit(action = "Unfinalize Item Cart")
    @ApiOperation(value = "Unfinalize Item Cart")
    public ConsumerCartResult unfinalizeItemCart(@PathParam("transactionId") String transactionId, @PathParam("orderItemId") String orderItemId, ConsumerCartResult incomingCart) {
        return consumerCartService.unfinalizeItemCart(transactionId, orderItemId, incomingCart);
    }

    @POST
    @Path("/transactions/{transactionId}/items/finalize")
    @Timed(name = "Finalize All Items in Cart")
    @Secured(requiredShop = true, checkTerminal = true)
    @Audit(action = "Finalize All Items in Cart")
    @ApiOperation(value = "Finalize All Items in Cart")
    public ConsumerCartResult finalizeAllOrderItems(@PathParam("transactionId") String transactionId, ConsumerCartResult incomingCart) {
        return consumerCartService.finalizeAllOrderItems(transactionId, incomingCart);
    }

    @POST
    @Path("/transactions/{transactionId}/finalize")
    @Timed(name = "Finalize all transaction items")
    @Secured(requiredShop = true)
    @Audit(action = "Finalize all transaction items")
    @ApiOperation(value = "Finalize all transaction items")
    public Transaction finalizeTransaction(@PathParam("transactionId") String transactionId) {
        return queuePOSService.finalizeTransaction(transactionId, true);
    }

    @POST
    @Path("/transactions/{transactionId}/unfinalize")
    @Timed(name = "Unfinalize all transaction items")
    @Secured(requiredShop = true)
    @Audit(action = "Unfinalize all transaction items")
    @ApiOperation(value = "Unfinalize all transaction items")
    public Transaction unfinalizeTransaction(@PathParam("transactionId") String transactionId) {
        return queuePOSService.finalizeTransaction(transactionId, false);
    }
    @GET
    @Path("/limited/transactions")
    @Timed(name = "getTransactionsWithLimitedInformation")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Transactions with filter")
    public SearchResult<TransactionLimitedResult> getTransactionsWithLimitedResult(@QueryParam("afterDate") long afterDate,
                                                                            @QueryParam("beforeDate") long beforeDate,
                                                                            @QueryParam("start") int start,
                                                                            @QueryParam("limit") int limit) {
        return queuePOSService.getTransactionsWithLimitedResult(afterDate, beforeDate, start, limit);
    }


    @POST
    @Path("/transactions/{transactionId}/paymentMethod")
    @Timed(name = "updatePaymentMethod")
    @Secured(requiredShop = true, checkTerminal = true)
    @Audit(action = "Update payment method for transaction")
    @ApiOperation(value = "Update payment method for transaction")
    public Transaction updatePaymentMethod(@PathParam("transactionId") String transactionId, @Valid PaymentOptionUpdateRequest request) {
        return queuePOSService.updatePaymentMethod(transactionId, request, false);
    }

    @POST
    @Path("/transactions/bulkPdfForTransactions")
    @Timed(name = "bulkPdfForTransactions")
    @Secured(requiredShop = true, checkTerminal = true)
    @ApiOperation(value = "Bulk PDF generation for transactions.")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response bulkPdfForTransactions(@FormDataParam("transactionIds") String transactionIds) {
        InputStream inputStream = queuePOSService.bulkPdfForTransactions(transactionIds);
        if (inputStream != null) {
            StreamingOutput streamOutput = new StreamingOutput() {
                @Override
                public void write(OutputStream os) throws IOException,
                        WebApplicationException {
                    ByteStreams.copy(inputStream, os);
                }
            };
            Response.ResponseBuilder responseBuilder = Response.ok(streamOutput).type("application/pdf");
            responseBuilder.header("Content-Disposition", "attachment; filename=\"Bulk_dispatch_orders.pdf\"");
            return responseBuilder.build();
        }
        return Response.ok().build();
    }

    @GET
    @Path("/transactions/all/{employeeId}")
    @Timed(name = "getAllTransactionsByEmployeeId")
    @Secured(requiredShop = true)
    @ApiOperation(value = "Get All transactions by employee id")
    public SearchResult<Transaction> getAllTransactionByEmployee(@PathParam("employeeId") String employeeId, @QueryParam("start") int start,
                                                                 @QueryParam("term") String searchTerm, @QueryParam("limit") int limit) {
        return queuePOSService.getAllTransactionByEmployee(employeeId, start, limit, searchTerm);
    }


    @GET
    @Path("/transactionManifest/{transactionId}/generatePDF")
    @Secured(requiredShop = true)
    @Timed(name = "createPdfForTransaction")
    @ApiOperation(value = "Create PDF For tranasction")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response createPdfForShippingManifest(@PathParam("transactionId") String transactionId) {
        InputStream inputStream = queuePOSService.createPdfForTransaction(transactionId);
        if (inputStream != null) {
            StreamingOutput streamOutput = new StreamingOutput() {
                @Override
                public void write(OutputStream os) throws IOException,
                        WebApplicationException {
                    ByteStreams.copy(inputStream, os);
                }
            };

            return Response.ok(streamOutput).type("application/pdf").build();
        }
        return Response.ok().build();
    }

    @POST
    @Path("/transactions/{transactionId}/paymentMethod/prepare")
    @Timed(name = "preparePaymentMethod")
    @Secured(requiredShop = true, checkTerminal = true)
    @Audit(action = "Prepare payment method for transaction")
    @ApiOperation(value = "Prepare payment method for transaction")
    public Transaction prepareCartForPayment(@PathParam("transactionId") String transactionId, @Valid PaymentOptionUpdateRequest request) {
        return queuePOSService.updatePaymentMethod(transactionId, request, true);
    }

}
