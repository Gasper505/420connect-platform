package com.fourtwenty.services.resources.pos;

import com.fourtwenty.services.resources.base.TimeCardResource;
import io.swagger.annotations.Api;

import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Created by mdo on 7/2/16.
 */
@Api("POS - Timecards")
@Path("/api/v1/pos/timecards")
@Produces(MediaType.APPLICATION_JSON)
public class POSTimeCardResource extends TimeCardResource {
}
