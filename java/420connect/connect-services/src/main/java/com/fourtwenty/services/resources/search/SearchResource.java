package com.fourtwenty.services.resources.search;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.product.ProductBatch;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.models.views.MemberLimitedView;
import com.fourtwenty.core.importer.model.ProductBatchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.ProductCustomResult;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.thirdparty.elasticsearch.services.ElasticSearchService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Api("Search")
@Path("/api/v2/search")
@Produces(MediaType.APPLICATION_JSON)
public class SearchResource {

    private ElasticSearchService elasticSearchService;

    @Inject
    public SearchResource(ElasticSearchService elasticSearchService) {
        this.elasticSearchService = elasticSearchService;
    }

    @GET
    @Path("/transactions")
    @Timed(name = "searchTransaction")
    @Secured(requiredShop = true)
    @ApiOperation("Search Transaction")
    public SearchResult<Transaction> searchTransactions(@QueryParam("term") String term, @QueryParam("start") int start, @QueryParam("limit") int limit, @QueryParam("sortBy") String sortBy, @QueryParam("sortByDirection") String sortByDirection) {
        return elasticSearchService.searchTransactions(term, start, limit, sortBy, sortByDirection);
    }


    @GET
    @Path("/members")
    @Timed(name = "searchMember")
    @Secured(requiredShop = true)
    @ApiOperation("Search Member")
    public SearchResult<MemberLimitedView> searchMembers(@QueryParam("term") String term, @QueryParam("start") int start, @QueryParam("limit") int limit, @QueryParam("sortBy") String sortBy, @QueryParam("sortByDirection") String sortByDirection) {
        return elasticSearchService.searchMembers(term, start, limit, sortBy, sortByDirection);
    }

    @GET
    @Path("/products")
    @Timed(name = "searchProducts")
    @Secured(requiredShop = true)
    @ApiOperation("Search Products")
    public SearchResult<ProductCustomResult> searchProducts(@QueryParam("term") String term, @QueryParam("start") int start, @QueryParam("limit") int limit, @QueryParam("sortBy") String sortBy, @QueryParam("sortByDirection") String sortByDirection) {
        return elasticSearchService.searchProducts(term, start, limit, sortBy, sortByDirection);
    }


    @GET
    @Path("/batches")
    @Timed(name = "searchProductBatches")
    @Secured(requiredShop = true)
    @ApiOperation("Search Product Batches")
    public SearchResult<ProductBatchResult> searchProductBatches(@QueryParam("term") String term, @QueryParam("start") int start, @QueryParam("limit") int limit, @QueryParam("sortBy") String sortBy, @QueryParam("sortByDirection") String sortByDirection, @QueryParam("status") ProductBatch.BatchStatus status) {
        return elasticSearchService.searchProductBatches(term, start, limit, sortBy, sortByDirection, status);
    }
}
