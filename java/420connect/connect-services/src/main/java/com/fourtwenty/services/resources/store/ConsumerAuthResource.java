package com.fourtwenty.services.resources.store;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.consumer.ConsumerUser;
import com.fourtwenty.core.domain.models.store.ConsumerPasswordReset;
import com.fourtwenty.core.rest.consumer.requests.ConsumerLoginRequest;
import com.fourtwenty.core.rest.consumer.requests.ConsumerRegisterRequest;
import com.fourtwenty.core.rest.consumer.results.ConsumerAuthResult;
import com.fourtwenty.core.rest.dispensary.requests.auth.ConsumerPasswordRequest;
import com.fourtwenty.core.rest.dispensary.requests.auth.PasswordResetRequest;
import com.fourtwenty.core.rest.dispensary.requests.auth.PasswordResetUpdateRequest;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.store.StoreSecured;
import com.fourtwenty.core.services.store.ConsumerAuthService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by mdo on 12/19/16.
 */
@Api("Blaze Store - Consumer Authentication")
@Path("/api/v1/store/auth")
@Produces(MediaType.APPLICATION_JSON)
@StoreSecured
public class ConsumerAuthResource extends BaseResource {

    @Inject
    ConsumerAuthService service;

    @POST
    @Path("/login")
    @Timed(name = "loginConsumer")
    @ApiOperation(value = "Consumer Login")
    @StoreSecured
    public ConsumerAuthResult loginConsumer(@Valid ConsumerLoginRequest request, @Context HttpServletResponse response) {
        ConsumerAuthResult result = service.login(request);
        response.setHeader("X-Auth-Token", result.getAccessToken());
        return result;
    }

    @POST
    @Path("/register")
    @Timed(name = "registerConsumer")
    @ApiOperation(value = "Register Consumer")
    @StoreSecured
    public ConsumerAuthResult registerConsumer(@Valid ConsumerRegisterRequest request, @Context HttpServletResponse response) {
        ConsumerAuthResult result = service.register(request);
        response.setHeader("X-Auth-Token", result.getAccessToken());
        return result;
    }

    @POST
    @Path("/createFromProfile")
    @Timed(name = "getConsumerOrCreateFromProfile")
    @ApiOperation(value = "Find a consumer user or create one from member profile if exist.")
    @StoreSecured(authRequired = false)
    public ConsumerUser getConsumerOrCreateFromProfile(@Valid PasswordResetRequest request) {
        return service.getConsumerOrCreateFromProfile(request);
    }

    @GET
    @Path("/renew")
    @Timed(name = "renewSession")
    @ApiOperation(value = "Renew Session")
    @StoreSecured(authRequired = true)
    public ConsumerAuthResult renewSession(@Context HttpServletResponse response) {
        ConsumerAuthResult result = service.renewSession();
        response.setHeader("X-Auth-Token", result.getAccessToken());
        return result;
    }

    @GET
    @Path("/session")
    @Timed(name = "getCurrentSession")
    @ApiOperation(value = "Renew Session")
    @StoreSecured(authRequired = true)
    public ConsumerAuthResult getCurrentSession(@Context HttpServletResponse response) {
        ConsumerAuthResult result = service.getCurrentSession();
        response.setHeader("X-Auth-Token", result.getAccessToken());
        return result;
    }

    @POST
    @Path("/password/reset")
    @Timed(name = "resetPasswordRequest")
    @ApiOperation(value = "Request Reset Password")
    @StoreSecured(authRequired = false)
    public ConsumerPasswordReset resetPasswordRequest(@Valid PasswordResetRequest request) {
        return service.requestPasswordReset(request);
    }

    @POST
    @Path("/password/update")
    @Timed(name = "resetPasswordUpdateRequest")
    @ApiOperation(value = "Update Password")
    @StoreSecured(authRequired = false)
    public Response resetPasswordUpdateRequest(@Valid PasswordResetUpdateRequest request) {
        service.updatePassword(request);
        return Response.ok().build();
    }

    @POST
    @Path("/resetPassword")
    @Timed(name = "resetConsumerPassword")
    @ApiOperation(value = "Reset Consumer Password")
    @StoreSecured(authRequired = false)
    public void resetConsumerPassword(@Valid ConsumerPasswordRequest request) {
        service.resetConsumerPassword(request);
        Response.ok().build();
    }
}
