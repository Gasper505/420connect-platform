package com.fourtwenty.services.resources.store;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.store.ConsumerCart;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.shop.ConsumerCartResult;
import com.fourtwenty.core.rest.store.requests.ProductCostRequest;
import com.fourtwenty.core.rest.store.requests.WooCartPrepareRequest;
import com.fourtwenty.core.rest.store.requests.WooCartRequest;
import com.fourtwenty.core.rest.store.results.ProductCostResult;
import com.fourtwenty.core.rest.store.results.WooComCartResult;
import com.fourtwenty.core.security.store.StoreSecured;
import com.fourtwenty.core.services.store.ConsumerCartService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/**
 * Created by mdo on 5/10/17.
 */
@Api("Blaze Store - Store Cart")
@Path("/api/v1/store/cart")
@Produces(MediaType.APPLICATION_JSON)
public class ConsumerCartResource {

    @Inject
    ConsumerCartService consumerCartService;

    @GET
    @Path("/history")
    @Timed(name = "getConsumerOrderHistory")
    @ApiOperation(value = "Get Consumer Order History")
    @StoreSecured(authRequired = true)
    public SearchResult<ConsumerCart> getConsumerOrderHistory(@QueryParam("start") int start,
                                                              @QueryParam("limit") int limit) {

        return consumerCartService.getOrderHistory(start, limit);
    }


    @GET
    @Path("/active")
    @Timed(name = "getActiveCart")
    @ApiOperation(value = "Get Active Cart")
    @StoreSecured
    public ConsumerCart getActiveCart(@QueryParam("sessionId") String sessionId) {
        return consumerCartService.getCurrentActiveCart(sessionId);
    }


    @POST
    @Path("/prepare")
    @Timed(name = "prepareActiveCart")
    @ApiOperation(value = "Prepare Active Cart")
    @StoreSecured
    public ConsumerCart prepareActiveCart(@Valid ConsumerCart consumerCart) {
        return consumerCartService.prepareCart(consumerCart);
    }

    @POST
    @Path("/updateCart/{consumerCartId}")
    @Timed(name = "updateCart")
    @ApiOperation(value = "Update Cart to the platform.")
    @StoreSecured
    public ConsumerCart updateCart(@PathParam("consumerCartId") String consumerCartId, @Valid ConsumerCart consumerCart) {
        return consumerCartService.updateCart(consumerCartId, consumerCart);
    }

    @DELETE
    @Path("/cancelCart/{consumerCartId}")
    @Timed(name = "cancel submitted order")
    @ApiOperation(value = "Cancel submitted order to the platform.")
    @StoreSecured
    public ConsumerCart cancelOrder(@PathParam("consumerCartId") String consumerCartId) {
        return consumerCartService.cancelCart(consumerCartId);
    }

    @POST
    @Path("/submitCart/{consumerCartId}")
    @Timed(name = "submitCart")
    @ApiOperation(value = "Submit Cart to be completed.")
    @StoreSecured(authRequired = true)
    public ConsumerCart submitCart(@PathParam("consumerCartId") String consumerCartId, @Valid ConsumerCart consumerCart) {
        return consumerCartService.submitCart(consumerCartId, consumerCart, ConsumerCart.TransactionSource.Widget);
    }

    @POST
    @Path("/product/findCost")
    @Timed(name = "findProductCost")
    @ApiOperation(value = "Find Product cost")
    @StoreSecured
    public ProductCostResult findProductCost(@Valid ProductCostRequest request) {
        return consumerCartService.findCost(request);
    }

    @GET
    @Path("/public/{publicKey}")
    @Timed(name = "findCartByPublicKey")
    @ApiOperation(value = "Find Cart by public key")
    public ConsumerCartResult findCartByPublicKey(@PathParam("publicKey") String publicKey) {
        return consumerCartService.getCartByPublicKey(publicKey);
    }

    @POST
    @Path("/woocommerce/info")
    @Timed(name = "prepareWoocommerceInfo")
    @ApiOperation(value = "Prepare cart taxes, pricing & other info")
    @StoreSecured
    public WooComCartResult prepareWoocommerceInfo(@QueryParam("sessionId") String sessionId, @Valid WooCartPrepareRequest cartPrepareRequest) {
        return consumerCartService.prepareWoocommerceInfo(cartPrepareRequest, sessionId);
    }

    @POST
    @Path("/woocommerce/cart/submit")
    @Timed(name = "submitWooOrder")
    @ApiOperation(value = "Submit order for woo commerce")
    @StoreSecured
    public ConsumerCart submitWooOrder(@Valid WooCartRequest wooCartRequest) {
        return consumerCartService.submitWooOrder(wooCartRequest);
    }

    @GET
    @Path("/woocommerce/orders/list")
    @Timed(name = "listAllPastOrders")
    @ApiOperation(value = "List all consumer user's orders for woo commerce")
    @StoreSecured
    public SearchResult<ConsumerCart> listAllPastOrders(@QueryParam("start") int start, @QueryParam("limit") int limit) {
        return consumerCartService.getLastOrders(start, limit);
    }

}
