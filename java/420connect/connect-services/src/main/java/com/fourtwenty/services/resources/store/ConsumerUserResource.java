package com.fourtwenty.services.resources.store;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.consumer.ConsumerUser;
import com.fourtwenty.core.domain.models.thirdparty.PatientVerificationStatus;
import com.fourtwenty.core.rest.dispensary.requests.consumeruser.request.ConsumerUserAddRequest;
import com.fourtwenty.core.rest.dispensary.requests.consumeruser.response.ConsumerUserImportResult;
import com.fourtwenty.core.rest.dispensary.results.ConsumerUserResult;
import com.fourtwenty.core.rest.dispensary.results.common.AssetStreamResult;
import com.fourtwenty.core.rest.store.requests.ContractSignRequest;
import com.fourtwenty.core.security.store.StoreSecured;
import com.fourtwenty.core.services.store.ConsumerUserService;
import com.fourtwenty.core.verificationsite.WebsiteManager;
import com.google.common.io.ByteStreams;
import io.dropwizard.jersey.caching.CacheControl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.sf.json.JSONObject;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataParam;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by mdo on 5/17/17.
 */
@Api("Blaze Store - Consumer User")
@Path("/api/v1/store/user")
@Produces(MediaType.APPLICATION_JSON)
public class ConsumerUserResource {
    @Inject
    ConsumerUserService consumerUserService;
    @com.google.inject.Inject
    PatientVerificationStatus patientVerificationStatus;

    @com.google.inject.Inject
    WebsiteManager websiteManager;

    @GET
    @Path("/")
    @Timed(name = "getCurrentActiveUser")
    @ApiOperation(value = "Get Active User Info")
    @StoreSecured(authRequired = true)
    public ConsumerUser getCurrentActiveUser() {
        return consumerUserService.getCurrentUser();
    }

    @GET
    @Path("/verify")
    @Timed(name = "verifyPatient")
    @StoreSecured(authRequired = false)
    @ApiOperation(value = "store - verify patient rec code")
    public PatientVerificationStatus verifyPatient(@QueryParam("recNo") String verificationCode,
                                                   @QueryParam("websiteType") String webSiteType) {

        String websiteResponse = websiteManager.getWebsiteDataForType(webSiteType, verificationCode);

        JSONObject jsonObject = JSONObject.fromObject(websiteResponse);
        patientVerificationStatus.setPatientName(jsonObject.getString("name"));
        patientVerificationStatus.setRecommendationExpDate(jsonObject.getString("latest_medical_recommendation_expiration"));
        patientVerificationStatus.setRecommendationIssueDate(jsonObject.getString("latest_medical_recommendation_publication"));
        patientVerificationStatus.setRecommendationStatus(jsonObject.getString("latest_medical_recommendation_status"));
        return patientVerificationStatus;
    }


    @POST
    @Path("/")
    @Timed(name = "updateUserInfo")
    @ApiOperation(value = "Update User Info")
    @StoreSecured(authRequired = true)
    public ConsumerUser updateUserInfo(@Valid ConsumerUser consumerUser) {
        return consumerUserService.updateUser(consumerUser, Boolean.FALSE);
    }

    @POST
    @Path("/sign")
    @Timed(name = "signContract")
    @ApiOperation(value = "Sign Contract Info")
    @StoreSecured(authRequired = true)
    public ConsumerUser signContract(@Valid ContractSignRequest signRequest) {
        return consumerUserService.signContract(signRequest);
    }


    @POST
    @Path("/dlPhoto")
    @Timed(name = "uploadDLPhoto")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @ApiOperation(value = "Upload DL Photo")
    @StoreSecured(authRequired = true)
    public ConsumerUser uploadDLPhoto(@FormDataParam("file") InputStream inputStream,
                                      @FormDataParam("name") String name,
                                      @FormDataParam("file") final FormDataBodyPart body) {
        String mimeType = body.getMediaType().toString();
        return consumerUserService.updateDLPhoto(inputStream, name);
    }

    @POST
    @Path("/recPhoto")
    @Timed(name = "uploadDLPhoto")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @ApiOperation(value = "Upload Recommendation Photo")
    @StoreSecured(authRequired = true)
    public ConsumerUser uploadRecPhoto(@FormDataParam("file") InputStream inputStream,
                                       @FormDataParam("name") String name,
                                       @FormDataParam("file") final FormDataBodyPart body) {
        String mimeType = body.getMediaType().toString();
        return consumerUserService.updateRecPhoto(inputStream, name);
    }

    @GET
    @Path("/assets/{assetKey}")
    @CacheControl(maxAge = 30, maxAgeUnit = TimeUnit.DAYS)
    @Produces({"image/jpeg", "application/pdf", "image/png"})
    @ApiOperation(value = "Get Asset")
    public Response getAssetStream(@PathParam("assetKey") String assetKey, @QueryParam("assetToken") String assetToken) {
        final AssetStreamResult result = consumerUserService.getConsumerAssetStream(assetKey, assetToken);
        StreamingOutput streamOutput = new StreamingOutput() {
            @Override
            public void write(OutputStream os) throws IOException,
                    WebApplicationException {
                ByteStreams.copy(result.getStream(), os);
            }
        };

        return Response.ok(streamOutput).type(result.getContentType()).build();
    }

    @GET
    @Path("/consumerByEmail")
    @Timed(name = "getConsumerByEmailId")
    @ApiOperation(value = "Get Consumer by email id")
    @StoreSecured
    public ConsumerUserResult getConsumerByEmailId(@QueryParam("email") String email) {
        return consumerUserService.getConsumerByEmailId(email);
    }

    @POST
    @Path("/importConsumer")
    @Timed(name = "importConsumerData")
    @ApiOperation(value = "Import Consumer Data")
    @StoreSecured
    public ConsumerUserImportResult addConsumerUsers(@Valid List<ConsumerUserAddRequest> requestList) {
        return consumerUserService.parseConsumerDataImport(requestList);
    }

    @POST
    @Path("/updateMemberWithEmailAndDob")
    @Timed(name = "updateMemberWithEmailAndDob")
    @ApiOperation(value = "Update members with email address and dob")
    @StoreSecured
    public Response updateMemberWithEmailAndDob() {
        consumerUserService.updateMemberWithEmailAndDob();
        return Response.ok().build();
    }

    @POST
    @Path("/updateConsumerUser")
    @Timed(name = "updateConsumerUser")
    @ApiOperation(value = "Update User Info with Email", hidden = true)
    @StoreSecured(authRequired = true)
    public ConsumerUser updateConsumerUser(@Valid ConsumerUser consumerUser) {
        return consumerUserService.updateUser(consumerUser, Boolean.TRUE);
    }
}
