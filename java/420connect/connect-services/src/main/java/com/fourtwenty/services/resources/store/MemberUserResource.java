package com.fourtwenty.services.resources.store;


import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.views.MemberLimitedView;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.services.store.MemberUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 * Created by on 2019-10-31
 */
@Api("Member - User")
@Path("/api/v1/store/member/")
@Produces(MediaType.APPLICATION_JSON)
public class MemberUserResource {
    @Inject
    MemberUserService memberUserService;

    @GET
    @Path("getMember")
    @Timed(name = "getMemberData")
    @ApiOperation(value = "Get Consumer Data")
    public SearchResult<MemberLimitedView> getConsumer(@QueryParam("start") int start, @QueryParam("limit") int limit){
       return memberUserService.getMembershipsForActiveShop(start,limit);
    }
}
