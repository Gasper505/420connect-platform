package com.fourtwenty.services.resources.store;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.product.ProductCategory;
import com.fourtwenty.core.rest.dispensary.results.BrandResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.store.results.ProductWithInfo;
import com.fourtwenty.core.security.store.BaseStoreResource;
import com.fourtwenty.core.security.store.StoreSecured;
import com.fourtwenty.core.services.store.StoreInventoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * Created by mdo on 4/21/17.
 */
@Api("Blaze Store - Inventory")
@Path("/api/v1/store/inventory")
@Produces(MediaType.APPLICATION_JSON)
@StoreSecured
public class StoreInventoryResource extends BaseStoreResource {

    @Inject
    StoreInventoryService storeInventoryService;

    @GET
    @Path("/categories")
    @Timed(name = "getAllCategories")
    @ApiOperation(value = "Get All Categories")
    @StoreSecured
    public SearchResult<ProductCategory> getAllCategories() {
        return storeInventoryService.getStoreCategories();
    }

    @GET
    @Path("/products")
    @Timed(name = "getAllProducts")
    @ApiOperation(value = "Get All Products")
    @StoreSecured
    public SearchResult<ProductWithInfo> getAllProducts(@QueryParam("categoryId") String categoryId,
                                                        @QueryParam("strain") String strain,
                                                        @QueryParam("term") String term,
                                                        @QueryParam("start") int start, @QueryParam("limit") int limit,
                                                        @QueryParam("tags") List<String> tags, @QueryParam("vendorId") String vendorId,
                                                        @QueryParam("afterDate") long afterDate, @QueryParam("beforeDate") long beforeDate,
                                                        @QueryParam("modified") boolean modified,
                                                        @QueryParam("brandId") String brandId) {
        if (modified) {
            return storeInventoryService.getAllProductsByDates(categoryId, strain, tags, vendorId, afterDate, beforeDate);
        } else {
            return storeInventoryService.getAllProducts(categoryId, strain, term, tags, vendorId, start, limit, brandId);
        }
    }

    @GET
    @Path("/products/{productId}")
    @Timed(name = "getProductById")
    @ApiOperation(value = "Get Product By Id")
    @StoreSecured
    public ProductWithInfo getProductById(@PathParam("productId") String productId) {
        return storeInventoryService.getProductById(productId);
    }

    @GET
    @Path("/products/sync")
    @Timed(name = "getAllProductsByDate")
    @ApiOperation(value = "Get All Products by Date")
    @StoreSecured
    public SearchResult<ProductWithInfo> getAllProducts(@QueryParam("categoryId") String categoryId,
                                                        @QueryParam("strain") String strain,
                                                        @QueryParam("tags") List<String> tags, @QueryParam("vendorId") String vendorId,
                                                        @QueryParam("afterDate") long afterDate, @QueryParam("beforeDate") long beforeDate) {
        return storeInventoryService.getAllProductsByDates(categoryId, strain, tags, vendorId, afterDate, beforeDate);
    }

    @GET
    @Path("/brands")
    @Timed(name = "getAllBrands")
    @ApiOperation(value = "Get All Brands")
    @StoreSecured
    public SearchResult<BrandResult> getAllBrands(@QueryParam("term") String term, @QueryParam("start") int start, @QueryParam("limit") int limit) {
        return storeInventoryService.getAllBrands(term, start, limit);
    }

}
