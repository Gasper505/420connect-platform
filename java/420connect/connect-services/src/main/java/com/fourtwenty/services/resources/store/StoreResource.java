package com.fourtwenty.services.resources.store;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.customer.Doctor;
import com.fourtwenty.core.domain.models.payment.ShopPaymentOption;
import com.fourtwenty.core.rest.dispensary.results.ListResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.store.results.StoreInfoResult;
import com.fourtwenty.core.security.dispensary.Audit;
import com.fourtwenty.core.security.store.BaseStoreResource;
import com.fourtwenty.core.security.store.StoreSecured;
import com.fourtwenty.core.services.store.StoreInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 * Created by mdo on 5/9/17.
 */
@Api("Blaze Store - Store Info")
@Path("/api/v1/store")
@Produces(MediaType.APPLICATION_JSON)
@StoreSecured
public class StoreResource extends BaseStoreResource {
    @Inject
    StoreInfoService storeInfoService;

    @GET
    @Path("/")
    @Timed(name = "getStoreInfo")
    @ApiOperation(value = "Get Store Info")
    @StoreSecured
    public StoreInfoResult getStoreInfo() {
        return storeInfoService.getStoreInfo();
    }

    @GET
    @Path("/doctors")
    @Timed(name = "searchDoctor")
    @ApiOperation(value = "Get Store Info")
    @StoreSecured
    public SearchResult<Doctor> searchDoctor(@QueryParam("term") String term) {
        return storeInfoService.searchDoctor(term);
    }

    @GET
    @Path("/paymentoptions")
    @Timed(name = "paymentoptions")
    @StoreSecured
    @Audit(action = "Get all payment options for shop")
    @ApiOperation(value = "Get all payment options for shop")
    public ListResult<ShopPaymentOption> getPaymentOptions() {
        return new ListResult<>(storeInfoService.getPaymentOptions());
    }

}
