package com.fourtwenty.services.resources.tests;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.test.SMSTestService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by mdo on 6/20/17.
 */
@Api("Management - SMS Test Resource")
@Path("/api/v1/tests/sms")
@Produces(MediaType.APPLICATION_JSON)
public class SMSTestResource extends BaseResource {
    @Inject
    SMSTestService testService;

    @GET
    @Path("/")
    @Secured
    @Timed(name = "testSMSJob")
    @ApiOperation(value = "testSMS")
    public Response testSMSJob() {
        testService.test();
        return ok();
    }


}
