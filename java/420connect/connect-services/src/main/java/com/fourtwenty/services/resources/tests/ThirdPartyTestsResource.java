package com.fourtwenty.services.resources.tests;

import com.blaze.clients.hypur.HypurPaymentAPIService;
import com.blaze.clients.hypur.models.CheckedInUserListResponse;
import com.codahale.metrics.annotation.Timed;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Created by mdo on 8/23/17.
 */
@Api("TESTS - SMS Test Resource")
@Path("/api/v1/tests/thirdparties")
@Produces(MediaType.APPLICATION_JSON)
public class ThirdPartyTestsResource {
    @Inject
    HypurPaymentAPIService paymentAPIService;

    @GET
    @Path("/hypur/checkins")
    @Timed(name = "getCheckedInUsers")
    @ApiOperation(value = "getCheckedInUsers")
    public CheckedInUserListResponse getCheckedInUsers() {
        return paymentAPIService.getCheckedInUsers("66d54dae-985a-4a24-befb-170803bf58c4", 300);
    }
}
