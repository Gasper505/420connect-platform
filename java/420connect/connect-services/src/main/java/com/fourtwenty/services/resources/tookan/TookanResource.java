package com.fourtwenty.services.resources.tookan;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.thirdparty.TookanErrorLog;
import com.fourtwenty.core.domain.models.tookan.TookanAccount;
import com.fourtwenty.core.domain.models.tookan.TookanTeams;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.security.dispensary.Secured;
import com.fourtwenty.core.services.tookan.TookanService;
import com.fourtwenty.core.thirdparty.onfleet.models.request.SynchronizeTeamRequest;
import com.fourtwenty.core.thirdparty.tookan.model.request.TookanAccountRequest;
import com.fourtwenty.core.thirdparty.tookan.model.request.fleets.AgentSynchronizeRequest;
import com.fourtwenty.core.thirdparty.tookan.model.request.team.TeamAddRequest;
import com.fourtwenty.core.thirdparty.tookan.model.response.EmployeeTookanInfoResult;
import com.fourtwenty.core.thirdparty.tookan.model.response.TookanTaskResult;
import com.fourtwenty.core.thirdparty.tookan.service.TookanErrorLogService;
import com.google.inject.Inject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Api("Management - Tookan")
@Path("/api/v1/mgmt/tookan")
@Produces(MediaType.APPLICATION_JSON)
public class TookanResource extends BaseResource {

    @Inject
    private TookanService tookanService;
    @Inject
    private TookanErrorLogService tookanErrorLogService;

    @POST
    @Path("/")
    @Secured(requiredShop = true)
    @Timed(name = "addTookanAccount")
    @ApiOperation(value = "Add tookan account")
    public TookanAccount addTookanAccount(@Valid TookanAccountRequest request) {
        return tookanService.addAccount(request);
    }

    @PUT
    @Path("/{tookanAccountId}")
    @Secured(requiredShop = true)
    @Timed(name = "updateTookanAccount")
    @ApiOperation(value = "Update tookan account")
    public TookanAccount updateTookanAccount(@PathParam("tookanAccountId") String tookanAccountId, @Valid TookanAccountRequest request) {
        return tookanService.updateAccount(tookanAccountId, request);
    }


    @GET
    @Path("/")
    @Secured(requiredShop = true)
    @Timed(name = "getTookanAccount")
    @ApiOperation(value = "Get tookan account")
    public SearchResult<TookanAccount> getTookanAccount(@QueryParam("shopId") String shopId, @QueryParam("start") int start, @QueryParam("limit") int limit) {
        return tookanService.getAccount(shopId, start, limit);
    }

    @DELETE
    @Path("/{tookanAccountId}")
    @Secured(requiredShop = true)
    @Timed(name = "deleteTookanAccount")
    @ApiOperation(value = "Delete tookan account")
    public Response deleteTookanAccount(@PathParam("tookanAccountId") String tookanAccountId) {
        tookanService.deleteAccount(tookanAccountId);
        return Response.ok().build();
    }


    @POST
    @Path("/team")
    @Secured(requiredShop = true)
    @Timed(name = "tookanTeamRequest")
    @ApiOperation(value = "Tookan Team Request")
    public TookanTeams addOrUpdateTookanTeams(@Valid TeamAddRequest request) {
        return tookanService.teamOperationRequest(request);
    }

    @POST
    @Path("/syncTeam")
    @Secured(requiredShop = true)
    @Timed(name = "synchronizeTeam")
    @ApiOperation(value = "Sysnchronize team")
    public TookanTeams synchronizeTeam(@Valid SynchronizeTeamRequest request) {
        return tookanService.synchronizeTeam(request);
    }

    @POST
    @Path("/employee/{employeeId}")
    @Secured(requiredShop = true)
    @Timed(name = "verifyAgentRequest")
    @ApiOperation(value = "Agent/Employee synchronize")
    public EmployeeTookanInfoResult synchronizeAgents(@PathParam("employeeId") String employeeId, @Valid AgentSynchronizeRequest request) {
        return tookanService.synchronizeAgents(employeeId, request);
    }

    @POST
    @Path("/employee/{employeeId}/deleteAgent")
    @Secured(requiredShop = true)
    @Timed(name = "deleteAgentRequest")
    @ApiOperation(value = "Remove agent/employee from tookan")
    public Response removeAgent(@PathParam("employeeId") String employeeId, @Valid SynchronizeTeamRequest request) {
        tookanService.removeAgent(employeeId, request);
        return Response.ok().build();
    }

    @POST
    @Path("/employee/{employeeId}/agent")
    @Secured(requiredShop = true)
    @Timed(name = "createAgentRequest")
    @ApiOperation(value = "Create agent at tookan")
    public Employee createAgent(@PathParam("employeeId") String employeeId, @Valid AgentSynchronizeRequest request) {
        return tookanService.createTookanAgent(employeeId, request);
    }

    @GET
    @Path("/teams/list")
    @Secured(requiredShop = true)
    @Timed(name = "getTookanTeamsList")
    @ApiOperation(value = "Get all tookan teams")
    public SearchResult<TookanTeams> getTookanTeams(@QueryParam("shopId") String shopId, @QueryParam("start") int start, @QueryParam("limit") int limit) {
        return tookanService.getTookanTeams(shopId, start, limit);
    }

    @GET
    @Path("/tasks/list")
    @Secured(requiredShop = true)
    @Timed(name = "getTookanList")
    @ApiOperation(value = "Get tookan tasks list")
    public DateSearchResult<TookanTaskResult> getTookanTasks(@QueryParam("afterDate") long afterDate, @QueryParam("beforeDate") long beforeDate, @QueryParam("status") TookanTaskResult.TookanTaskStatus status, @QueryParam("taskType") TookanTaskResult.TookanTaskType taskType) {
        return tookanService.getTookanTasks(afterDate, beforeDate, taskType, status);
    }

    @GET
    @Path("/errorLogs")
    @Secured(requiredShop = true)
    @Timed(name = "getTookanErrorLog")
    @ApiOperation(value = "Get tookan error logs")
    public SearchResult<TookanErrorLog> getTookanErrorLog(@QueryParam("shopId") String shopId, @QueryParam("start") int start, @QueryParam("limit") int limit) {
        return tookanErrorLogService.getTookanErrorLog(shopId, start, limit);
    }


}
