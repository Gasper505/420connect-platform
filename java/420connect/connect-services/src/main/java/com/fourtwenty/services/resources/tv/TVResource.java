package com.fourtwenty.services.resources.tv;

import com.codahale.metrics.annotation.Timed;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.models.views.MemberLimitedView;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.rest.dispensary.results.tvdisplay.EmployeeStatsResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.marketing.TVDisplayResult;
import com.fourtwenty.core.security.dispensary.BaseResource;
import com.fourtwenty.core.services.mgmt.TVDisplayService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.Map;

/**
 * Created by decipher on 16/10/17 12:23 PM
 * Abhishek Samuel (Software Engineer)
 * abhishke.decipher@gmail.com
 * Decipher Zone Softwares
 * www.decipherzone.com
 */
@Api("Public - TV Display")
@Path("/api/v1/public/tv")
@Produces(MediaType.APPLICATION_JSON)
public class TVResource extends BaseResource {

    private TVDisplayService tvDisplayService;

    @Inject
    public TVResource(TVDisplayService tvDisplayService) {
        this.tvDisplayService = tvDisplayService;
    }

    @GET
    @Path("/{uniqueIdentifier}/settings")
    @Timed(name = "getTVDisplaySetting")
    @ApiOperation(value = "Get TV display setting by TV number")
    public TVDisplayResult getTVDisplaySetting(@PathParam("uniqueIdentifier") String uniqueIdentifier, @QueryParam("tvno") Long tvNo) {
        TVDisplayResult tvDisplay = tvDisplayService.getTVDisplaySetting(uniqueIdentifier, tvNo);
        if (!tvDisplay.isPublished()) {
            throw new BlazeInvalidArgException("TVDisplay", "TVDisplay is not published.");
        }
        return tvDisplay;
    }


    @GET
    @Path("/{uniqueIdentifier}/products")
    @Timed(name = "getProductByCategoryAndIdentifier")
    @ApiOperation(value = "Get product by category")
    public SearchResult<Product> getProductByCategory(@PathParam("uniqueIdentifier") String uniqueIdentifier, @QueryParam("categoryId") String categoryId, @QueryParam("start") int start, @QueryParam("limit") int limit) {
        return tvDisplayService.getProductByCategoryAndIdentifier(uniqueIdentifier, categoryId, start, limit);
    }

    @GET
    @Path("/{uniqueIdentifier}/waitlist")
    @Timed(name = "getWaitListForAllActiveTransactions")
    @ApiOperation(value = "Get wait list queue member")
    public List<MemberLimitedView> getWaitListForAllActiveTransactions(@PathParam("uniqueIdentifier") String uniqueIdentifier) {
        final List<MemberLimitedView> waitListForAllActiveTransactions = tvDisplayService.getWaitListForAllActiveTransactions(uniqueIdentifier);
        return waitListForAllActiveTransactions;
    }

    @GET
    @Path("/{uniqueIdentifier}/transaction")
    @Timed(name = "getTvDisplayTransactionsByContent")
    @ApiOperation(value = "Get tv display data by content")
    public Map<String, SearchResult<Transaction>> getTvDisplayTransactionsByContent(@PathParam("uniqueIdentifier") String uniqueIdentifier, @QueryParam("contentId") String contentId, @QueryParam("start") int start, @QueryParam("limit") int limit) {
        return tvDisplayService.getTvDisplayTransactionsByContent(uniqueIdentifier, contentId, start, limit);
    }

    @GET
    @Path("/{uniqueIdentifier}/employeeStats")
    @Timed(name = "getEmployeeStatsDetail")
    @ApiOperation(value = "Get employee stats for tv display")
    public EmployeeStatsResult getEmployeeStatsDetail(@PathParam("uniqueIdentifier") String uniqueIdentifier, @QueryParam("contentId") String contentId, @QueryParam("start") int start, @QueryParam("limit") int limit, @QueryParam("timeZoneOffset") int timeZoneOffSet) {
        return tvDisplayService.getEmployeeStatsDetail(uniqueIdentifier, contentId, start, limit, timeZoneOffSet);
    }

}
