package com.fourtwenty.services.tests;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.math.BigDecimal;

/**
 * Created by mdo on 3/31/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class TestRequest {
    private BigDecimal money;

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }
}
