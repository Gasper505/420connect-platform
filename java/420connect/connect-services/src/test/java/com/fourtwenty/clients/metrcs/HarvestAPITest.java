package com.fourtwenty.clients.metrcs;

import com.blaze.clients.metrcs.MetrcAuthorization;
import com.blaze.clients.metrcs.MetricsHarvestAPIService;
import com.blaze.clients.metrcs.models.harvests.MetricsHarvestList;
import com.fourtwenty.test.BaseTest;
import org.junit.Test;

/**
 * Created by mdo on 8/23/17.
 */
public class HarvestAPITest extends BaseTest {

    @Test
    public void getActiveHarvests() {

        // Check if there's any existing members in queue, if so, just use existing one
        MetrcAuthorization authorization = rule.getBean(MetrcAuthorization.class);
        MetricsHarvestAPIService apiService = new MetricsHarvestAPIService(authorization);

        MetricsHarvestList items = apiService.getActiveHarvests("020-X0001");

        System.out.println(items);
    }

    @Test
    public void getOnHoldHarvests() {

        // Check if there's any existing members in queue, if so, just use existing one
        MetrcAuthorization authorization = rule.getBean(MetrcAuthorization.class);
        MetricsHarvestAPIService apiService = new MetricsHarvestAPIService(authorization);

        MetricsHarvestList items = apiService.getOnHoldHarvests("020-X0001");

        System.out.println(items);
    }

    @Test
    public void getInactiveHarvests() {

        // Check if there's any existing members in queue, if so, just use existing one
        MetrcAuthorization authorization = rule.getBean(MetrcAuthorization.class);
        MetricsHarvestAPIService apiService = new MetricsHarvestAPIService(authorization);

        MetricsHarvestList items = apiService.getInactiveHarvests("020-X0001");

        System.out.println(items);
    }

    @Test
    public void createHarvest() {

        // Check if there's any existing members in queue, if so, just use existing one
        MetrcAuthorization authorization = rule.getBean(MetrcAuthorization.class);
        MetricsHarvestAPIService apiService = new MetricsHarvestAPIService(authorization);

        MetricsHarvestList items = apiService.getInactiveHarvests("020-X0001");

        System.out.println(items);
    }

}
