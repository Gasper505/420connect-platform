package com.fourtwenty.clients.metrcs;

import com.blaze.clients.metrcs.MetrcAuthorization;
import com.blaze.clients.metrcs.MetricsFacilitiesApiService;
import com.blaze.clients.metrcs.models.facilities.MetricsFacility;
import com.blaze.clients.metrcs.models.facilities.MetricsFacilityList;
import com.fourtwenty.test.BaseTest;
import org.junit.Test;

/**
 * Created by mdo on 8/22/17.
 */
public class MetrcAPITest extends BaseTest {


    @Test
    public void testGetFacilities() {

        // Check if there's any existing members in queue, if so, just use existing one
        MetrcAuthorization authorization = rule.getBean(TestMetrcAuthorizationImpl.class);
        MetricsFacilitiesApiService apiService = new MetricsFacilitiesApiService(authorization);

        MetricsFacilityList facilities = apiService.getFacilities();

        System.out.println(facilities);
        for (MetricsFacility facility : facilities) {
            System.out.println(facility.getName() + " - " + facility.getLicense().getNumber() + " - " + facility.getLicense().getLicenseType());
        }
    }
}
