package com.fourtwenty.clients.metrcs;

import com.amazonaws.services.dynamodbv2.xspec.B;
import com.blaze.clients.metrcs.*;
import com.blaze.clients.metrcs.models.facilities.MetricsFacility;
import com.blaze.clients.metrcs.models.facilities.MetricsFacilityList;
import com.blaze.clients.metrcs.models.items.MetricsItem;
import com.blaze.clients.metrcs.models.items.MetricsItemCategory;
import com.blaze.clients.metrcs.models.items.MetricsItemCategoryList;
import com.blaze.clients.metrcs.models.items.MetricsItemsList;
import com.blaze.clients.metrcs.models.packages.MetricsPackage;
import com.blaze.clients.metrcs.models.packages.MetricsPackageAdjustment;
import com.blaze.clients.metrcs.models.packages.MetricsPackages;
import com.blaze.clients.metrcs.models.packages.MetricsPackagesList;
import com.blaze.clients.metrcs.models.strains.MetricsStrain;
import com.blaze.clients.metrcs.models.strains.MetricsStrainList;
import com.fourtwenty.core.util.JsonSerializer;
import com.fourtwenty.test.BaseTest;
import org.apache.commons.codec.binary.Base64;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by mdo on 10/3/17.
 */
public class PackagesAPITests extends BaseTest {

    @Test
    public void testRetrievePackages() {

        // Check if there's any existing members in queue, if so, just use existing one
        MetrcAuthorization authorization = rule.getBean(TestMetrcAuthorizationImpl.class);
        MetricsPackagesAPIService apiService = new MetricsPackagesAPIService(authorization); //rule.getBean(MetricsPackagesAPIService.class);
        //MetricsPackagesList packagesList = apiService.getActivePackages("3a-13217");

        String binaryData = authorization.getVendorAPIKey() + ":" + authorization.getUserAPIKey();
        String bearerToken =  Base64.encodeBase64String(binaryData.getBytes());
        System.out.println("Token: Basic " + bearerToken);

        MetricsFacilitiesApiService facilityService = new MetricsFacilitiesApiService(authorization);
        MetricsFacilityList facilities = facilityService.getFacilities();

        for (MetricsFacility facility : facilities) {
            System.out.println(facility.getName() + " - LIC#: " + facility.getLicense().getNumber() + " (" + facility.getLicense().getLicenseType() + ")");
            //MetricsPackagesList packagesList = apiService.https://bitbucket.org/flomanage/flomanagegetActivePackages(facility.getLicense().getNumber());
            //MetricsPackagesList onholdPackages = apiService.getOnholdPackages("050-X0002");

            //MetricsPackages metricsPacpackagesListkages = apiService.getPackageLabel("ABCDEF012345670000013817");
/*
            for (MetricsPackages packages : packagesList) {
                System.out.println(packages.getLabel() + " : " + packages.getQuantity() + " " + packages.getUnitOfMeasureAbbreviation() + " : " + packages.getUnitOfMeasureName() + " : " + packages.getProductId() + " : " + packages.getProductName() + " : " + packages.getProductCategoryName() + " : " + packages.getProductionBatchNumber());
            }*/
        }


        //System.out.println(metricsPacpackagesListkages);
    }

    @Test
    public void testAdjustPackage() throws InterruptedException, ExecutionException, IOException {
        MetrcAuthorization authorization = rule.getBean(TestMetrcAuthorizationImpl.class);
        MetricsPackagesAPIService apiService = new MetricsPackagesAPIService(authorization);

        MetricsPackagesList list = apiService.getActivePackages(authorization.getFacilityLicense());
        Assert.assertFalse(list.isEmpty());

        MetricsPackages packages = list.get(0);


        List<MetricsPackageAdjustment> adjustments = new ArrayList<>();

        MetricsPackageAdjustment adjustment = new MetricsPackageAdjustment();
        adjustment.setLabel("");
        String string = apiService.adjustPackage(adjustments, authorization.getFacilityLicense());
        System.out.println(string);

    }

    @Test
    public void testCreatePackage() throws InterruptedException, ExecutionException, IOException {
        MetrcAuthorization authorization = rule.getBean(TestMetrcAuthorizationImpl.class);
        MetricsPackagesAPIService apiService = new MetricsPackagesAPIService(authorization);
//1A4FF0100000028000000152
        String testTag = "1A4FF0100000028000000014";
        List<MetricsPackage> packages = new ArrayList<>();
        MetricsPackage metricsPackage = new MetricsPackage();
        metricsPackage.setQuantity(100);
        metricsPackage.setItem("Cannabis Full Plant - Cinderella 99"); // must be a valid item name
        metricsPackage.setUnitOfMeasure("Ounces");
        metricsPackage.setActualDate("2019-05-10");
        metricsPackage.setProductionBatch(true);
        metricsPackage.setProductionBatchNumber("AFDA-32-131");
        metricsPackage.setPatientLicenseNumber("");
        metricsPackage.setTag(testTag);
        metricsPackage.setIngredients(new ArrayList<>());
        packages.add(metricsPackage);


        MetricsItemsAPIService itemsAPIService = new MetricsItemsAPIService(authorization);


        MetricsItemsList itemsList = itemsAPIService.getActiveItems(authorization.getFacilityLicense());

        MetricsStrainsAPIService strainsAPIService = new MetricsStrainsAPIService(authorization);

        MetricsStrainList strainList = strainsAPIService.getActiveStrains(authorization.getFacilityLicense());

        // 1. Create Strain
        /*List<MetricsStrain> strains = new ArrayList<>();
        MetricsStrain strain = new MetricsStrain();
        strain.setName("BL-MECHA-01");
        strain.setTestingStatus("None");
        strain.setThcLevel(0.10);
        strain.setIndicaPercentage(90.0);
        strain.setSativaPercentage(10.0);
        strains.add(strain);
        try {
            String strainNew = strainsAPIService.createStrains(strains, authorization.getFacilityLicense());

            MetricsStrainList strainList2 = strainsAPIService.getActiveStrains(authorization.getFacilityLicense());

            System.out.println("");
        } catch (Exception e) {

            System.out.println("error creating strains: " + e.getMessage() + " : " + e.getStackTrace());
            throw e;
        }*/
        // 2. Create Item for the strain
        /*
         "ItemCategory": "Buds",
    "Name": "Buds Item",
    "UnitOfMeasure": "Ounces",
    "Strain": "Spring Hill Kush",
         */

        MetricsItemCategoryList categories = itemsAPIService.getItemCategories();
        for (MetricsItemCategory category : categories) {
            System.out.println(category.getName());
        }

        List<MetricsItem> items = new ArrayList<>();
        MetricsItem metricsItem = new MetricsItem();
        //metricsItem.setStrain("BL-MECHA-01");
        metricsItem.setItemCategory("Topical (weight - each)");
        metricsItem.setName("BL-MECHA P2");
        metricsItem.setUnitOfMeasure("Each");
        metricsItem.setUnitThcContent(new BigDecimal(10));
        metricsItem.setUnitThcContentUnitOfMeasure("Milligrams");
        metricsItem.setUnitWeight(new BigDecimal(10));
        metricsItem.setUnitWeightUnitOfMeasure("Milligrams");
        /*metricsItem.setIngredients("");
        metricsItem.setUnitCbdContent(10);
        metricsItem.setUnitCbdContentUnitOfMeasure("Milligrams");
*/

        items.add(metricsItem);
        try {
            String jsonRequest = JsonSerializer.toJson(items);
            String retVal = itemsAPIService.createItems(items, authorization.getFacilityLicense());
            MetricsItemsList itemsList2  = itemsAPIService.getActiveItems(authorization.getFacilityLicense());

            System.out.println("");
        } catch (Exception e) {

            System.out.println("error creating items: " + e.getMessage() + " : " + e.getStackTrace());
            throw e;
        }

        // 3. Create Package for the Item to sell

        // Strain: Gorilla Glue
        try {
            String jsonRequest = JsonSerializer.toJson(packages);
            System.out.println("json: " + jsonRequest);
            String str = apiService.createPackages(packages, authorization.getFacilityLicense());

            MetricsPackages myPackage = apiService.getPackageLabel(testTag);
            System.out.println("result: " + str);
        } catch (Exception e) {
            System.out.println("error creating packages: " + e.getMessage() + " : " + e.getStackTrace());
            throw e;
        }

    }
}
