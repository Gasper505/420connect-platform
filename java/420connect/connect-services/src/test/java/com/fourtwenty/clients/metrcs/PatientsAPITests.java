package com.fourtwenty.clients.metrcs;

import com.blaze.clients.metrcs.MetrcAuthorization;
import com.blaze.clients.metrcs.MetricsPatientsAPIService;
import com.blaze.clients.metrcs.MetricsSalesAPIService;
import com.blaze.clients.metrcs.models.sales.MetricsSaleList;
import com.fourtwenty.test.BaseTest;
import org.apache.commons.codec.binary.Base64;
import org.junit.Test;

public class PatientsAPITests extends BaseTest {

    @Test
    public void getBearerToken() {

        MetrcAuthorization authorization = rule.getBean(MetrcAuthorization.class);
        String binaryData = authorization.getVendorAPIKey() + ":" + authorization.getUserAPIKey();
        String bearerToken =  Base64.encodeBase64String(binaryData.getBytes());


        System.out.println(bearerToken);
    }

    @Test
    public void getActivePatients() {

        // Check if there's any existing members in queue, if so, just use existing one
        MetrcAuthorization authorization = rule.getBean(MetrcAuthorization.class);
        MetricsPatientsAPIService apiService = new MetricsPatientsAPIService(authorization);

        String items = apiService.getActivePatients("PC-000252");

        System.out.println(items);
    }

}
