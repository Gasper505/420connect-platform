package com.fourtwenty.clients.metrcs;

import com.blaze.clients.metrcs.MetrcAuthorization;
import com.blaze.clients.metrcs.MetricsPlantsAPIService;
import com.blaze.clients.metrcs.models.plants.MetricsPlantList;
import com.fourtwenty.test.BaseTest;
import org.junit.Test;

/**
 * Created by mdo on 8/23/17.
 */
public class PlantsAPITests extends BaseTest {
    @Test
    public void testGetFloweringPlants() {


        // Check if there's any existing members in queue, if so, just use existing one
        MetrcAuthorization authorization = rule.getBean(MetrcAuthorization.class);
        MetricsPlantsAPIService apiService = new MetricsPlantsAPIService(authorization);

        MetricsPlantList items = apiService.getFloweringPlants("020-X0001");
        System.out.println(items);
    }

    @Test
    public void getVegetativePlants() {


        // Check if there's any existing members in queue, if so, just use existing one
        MetrcAuthorization authorization = rule.getBean(MetrcAuthorization.class);
        MetricsPlantsAPIService apiService = new MetricsPlantsAPIService(authorization);

        MetricsPlantList items = apiService.getVegetativePlants("020-X0001");
        System.out.println(items);
    }

}
