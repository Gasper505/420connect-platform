package com.fourtwenty.clients.metrcs;

import com.blaze.clients.metrcs.MetrcAuthorization;
import com.blaze.clients.metrcs.MetricsSalesAPIService;
import com.blaze.clients.metrcs.models.sales.MetricsReceipt;
import com.blaze.clients.metrcs.models.sales.MetricsReceiptList;
import com.blaze.clients.metrcs.models.sales.MetricsSaleList;
import com.blaze.clients.metrcs.models.sales.MetricsSaleTransactionList;
import com.fourtwenty.core.util.DateUtil;
import com.fourtwenty.test.BaseTest;
import org.joda.time.DateTime;
import org.junit.Test;

import java.util.List;

/**
 * Created by mdo on 8/23/17.
 */
public class SalesAPITest extends BaseTest {
    @Test
    public void getSaleCustomerTypes() {

        // Check if there's any existing members in queue, if so, just use existing one
        MetrcAuthorization authorization = rule.getBean(MetrcAuthorization.class);
        MetricsSalesAPIService apiService = new MetricsSalesAPIService(authorization);

        List<String> items = apiService.getSaleCustomerTypes();

        System.out.println(items);
    }

    @Test
    public void getSalesDeliveries() {

        // Check if there's any existing members in queue, if so, just use existing one
        MetrcAuthorization authorization = rule.getBean(MetrcAuthorization.class);
        MetricsSalesAPIService apiService = new MetricsSalesAPIService(authorization);

        MetricsSaleList items = apiService.getSaleDeliveries("050-X0001");

        System.out.println(items);
    }

    @Test
    public void getReceipts() {

        // Check if there's any existing members in queue, if so, just use existing one
        MetrcAuthorization authorization = rule.getBean(MetrcAuthorization.class);
        MetricsSalesAPIService apiService = new MetricsSalesAPIService(authorization);


        DateTime dateTime2 = new DateTime("2019-02-19T11:36:36.066Z");

        DateTime dateTime = DateUtil.nowUTC().minusDays(1);

        String dt2 = dateTime2.toString();
        MetricsReceipt receipt = apiService.getSaleReceipt("727814");
        MetricsReceiptList items = apiService.getSaleReceipts("PC-000252", dateTime.toString());

        MetricsReceipt myReceipt = null;
        for (MetricsReceipt transaction : items) {
            DateTime saleTime = new DateTime(transaction.getSalesDateTime());

            long st = saleTime.getMillis() / 1000;

            long dt22 = dateTime2.getMillis() / 1000;

            if (dt22 == st) {
                myReceipt = transaction;
            }
            System.out.print(st);
        }
        System.out.println(items);
    }

    @Test
    public void getTransactions() {

        // Check if there's any existing members in queue, if so, just use existing one
        MetrcAuthorization authorization = rule.getBean(MetrcAuthorization.class);
        MetricsSalesAPIService apiService = new MetricsSalesAPIService(authorization);

        MetricsSaleTransactionList items = apiService.getSaleTransactions("050-X0001");

        System.out.println(items);
    }
}
