package com.fourtwenty.clients.metrcs;

import com.blaze.clients.metrcs.MetrcAuthorization;
import com.blaze.clients.metrcs.MetricsStrainsAPIService;
import com.blaze.clients.metrcs.models.strains.MetricsStrainList;
import com.fourtwenty.test.BaseTest;
import org.junit.Test;

/**
 * Created by mdo on 8/23/17.
 */
public class StrainsTests extends BaseTest {

    @Test
    public void testGetActiveStrains() {

        // Check if there's any existing members in queue, if so, just use existing one
        MetrcAuthorization authorization = rule.getBean(MetrcAuthorization.class);
        MetricsStrainsAPIService apiService = new MetricsStrainsAPIService(authorization);

        MetricsStrainList facilities = apiService.getActiveStrains("020-X0001");

        System.out.println(facilities);
    }
}
