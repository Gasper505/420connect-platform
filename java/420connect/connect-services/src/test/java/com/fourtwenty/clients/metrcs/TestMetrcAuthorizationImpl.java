package com.fourtwenty.clients.metrcs;

import com.blaze.clients.metrcs.MetrcAuthorization;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import org.apache.commons.codec.binary.Base64;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;

/**
 * Created by mdo on 10/3/17.
 */
public class TestMetrcAuthorizationImpl implements MetrcAuthorization {


    public String getCompanyId() {
        return "";
    }

    @Override
    public String getFacilityLicense() {
        return "M10-0000004-LIC";
    }

    @Override
    public MultivaluedMap<String, Object> makeHeaders() {

        if (this.getUserAPIKey() == null) {
            throw new BlazeInvalidArgException("Metrc", "User API Key is not specified.");
        }
        MultivaluedMap<String, Object> headers = new MultivaluedHashMap<>();
        headers.putSingle("Content-Type", MediaType.APPLICATION_JSON);
        String binaryData = this.getVendorAPIKey() + ":" + this.getUserAPIKey();
        headers.putSingle("Authorization", "Basic " + Base64.encodeBase64String(binaryData.getBytes()));
        return headers;
    }

    @Override
    public String getHost() {
        return "https://sandbox-api-ca.metrc.com";
    }

    @Override
    public String getVendorAPIKey() {
        return "9KT4ofNlRe36Nwbcb56JyigjdT8l3dFPpbgG1f0aP4LzynGd";
    }

    @Override
    public String getUserAPIKey() {
        return "FusVbe4Yv6W1DGNuxKNhByXU6RO6jSUPcbRCoRDD98VNXc4D";
    }
}
