package com.fourtwenty.importer;

import com.fourtwenty.core.importer.meadow.MeadowParser;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.thirdparty.onfleet.models.response.AuthenticateAPIKeyResult;
import com.fourtwenty.core.thirdparty.onfleet.services.OnFleetService;
import com.fourtwenty.test.BaseTest;
import com.fourtwenty.test.TestModule;
import org.junit.Test;

import java.io.InputStream;

public class MeadowImporterTest extends BaseTest {


    /**
     * Test for getting inventory history by id
     */
    @Test
    public void testImportMeadow() {
        InputStream stream = TestModule.class.getResourceAsStream("/sespe-products.json");


        MeadowParser result = rule.getBean(MeadowParser.class);

        ConnectAuthToken connectAuthToken = rule.getBean(ConnectAuthToken.class);
        result.importMeadowProducts(connectAuthToken.getCompanyId(),connectAuthToken.getShopId(),connectAuthToken.getActiveTopUser().getUserId(), stream);
    }
}
