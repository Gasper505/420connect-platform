package com.fourtwenty.services;

import com.fourtwenty.core.domain.models.product.Inventory;
import com.fourtwenty.core.domain.models.product.Product;
import com.fourtwenty.core.domain.models.product.ProductPrepackageQuantity;
import com.fourtwenty.core.domain.models.transaction.OrderItem;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.domain.models.views.MemberLimitedView;
import com.fourtwenty.core.domain.repositories.dispensary.ProductPrepackageQuantityRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.rest.dispensary.requests.queues.QueueAddMemberRequest;
import com.fourtwenty.core.rest.dispensary.results.DateSearchResult;
import com.fourtwenty.core.rest.dispensary.results.InitialLoginResult;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.shop.AllQueueResult;
import com.fourtwenty.core.services.mgmt.*;
import com.fourtwenty.test.BaseTest;
import org.junit.Test;

import java.math.BigDecimal;

/**
 * Created by mdo on 4/2/17.
 */
public class CheckoutFlowTest extends BaseTest {


    private Transaction getTransaction() {
        // Get a list o fmembers

        // Check if there's any existing members in queue, if so, just use existing one
        ShopService shopService = rule.getBean(ShopService.class);
        QueuePOSService queuePOSService = rule.getBean(QueuePOSService.class);

        AllQueueResult result = queuePOSService.getTransactionsWithDate();
        // Check delivery
        Transaction trans = null;
        if (result.getDelivery().getValues().size() > 0) {
            trans = result.getDelivery().getValues().get(0);
        } else if (result.getWalkin().getValues().size() > 0) {
            trans = result.getWalkin().getValues().get(0);
        }

        return trans;
    }

    @Test
    public void testCreateTransaction() {
        Transaction trans = getTransaction();

        if (trans != null) {
            return;
        }
        SearchResult<MemberLimitedView> memberList = rule.getBean(MemberService.class).getMembershipsForActiveShop("", 0, 100);

        if (memberList.getValues().size() == 0) {
            throw new BlazeInvalidArgException("Member", "No member.");
        }
        // Non member exist
        InitialLoginResult currentSession = rule.getBean(AuthenticationService.class).getCurrentActiveEmployee();

        QueueAddMemberRequest addMemberRequest = new QueueAddMemberRequest();
        addMemberRequest.setEmployeeId(currentSession.getEmployee().getId());
        addMemberRequest.setMemberId(memberList.getValues().get(0).getId());

        QueuePOSService queuePOSService = rule.getBean(QueuePOSService.class);
        ShopService shopService = rule.getBean(ShopService.class);
        trans = queuePOSService.addToQueue(Transaction.QueueType.Delivery.name(), addMemberRequest,-1);
        System.out.print(trans);
    }

    @Test
    public void testPrepackageCart() {
        testCreateTransaction();
        Transaction trans = getTransaction();

        // Add item to cart
        // Get a list of products
        DateSearchResult<Product> productDateSearchResult = rule.getBean(ProductService.class).getProducts(0, 0);

        // Find Alaskan Ice
        Product alaskanIce = null;
        for (Product product : productDateSearchResult.getValues()) {
            if (product.getName().equalsIgnoreCase("Alaskan Ice")) {
                alaskanIce = product;
                break;
            }
        }

        SearchResult<Inventory> inventories = rule.getBean(InventoryService.class).getShopInventories(null);
        Inventory safeInventory = null;
        for (Inventory inventory : inventories.getValues()) {
            if (inventory.getType() == Inventory.InventoryType.Storage) {
                safeInventory = inventory;
                break;
            }
        }


        Iterable<ProductPrepackageQuantity> prepackageQuantities = rule.getBean(ProductPrepackageQuantityRepository.class).getQuantitiesForProduct(trans.getCompanyId(), trans.getShopId(), alaskanIce.getId());
        // Find a product that has
        // Find any prepackages with quantity

        ProductPrepackageQuantity quantity = null;
        for (ProductPrepackageQuantity qty : prepackageQuantities) {
            if (qty.getInventoryId().equalsIgnoreCase(safeInventory.getId()) && qty.getQuantity() > 0) {
                quantity = qty;
                break;
            }
        }


        // Now let's add the quantity to the cart
        OrderItem orderItem = new OrderItem();
        orderItem.setProductId(alaskanIce.getId());
        orderItem.setQuantity(new BigDecimal(3));
        orderItem.setPrepackageItemId(quantity.getPrepackageItemId());
        trans.getCart().getItems().clear();
        trans.getCart().getItems().add(orderItem);

        QueuePOSService queuePOSService = rule.getBean(QueuePOSService.class);
        ShopService shopService = rule.getBean(ShopService.class);
        Transaction preparedTrans = queuePOSService.prepareCart(trans.getId(), trans);

        System.out.print(preparedTrans);
    }


    @Test
    public void testCompleteTransaction() {
        testCreateTransaction();
        Transaction trans = getTransaction();

        // Add item to cart
        // Get a list of products
        DateSearchResult<Product> productDateSearchResult = rule.getBean(ProductService.class).getProducts(0, 0);

        // Find Alaskan Ice
        Product alaskanIce = null;
        for (Product product : productDateSearchResult.getValues()) {
            if (product.getName().equalsIgnoreCase("Alaskan Ice")) {
                alaskanIce = product;
                break;
            }
        }

        SearchResult<Inventory> inventories = rule.getBean(InventoryService.class).getShopInventories(null);
        Inventory safeInventory = null;
        for (Inventory inventory : inventories.getValues()) {
            if (inventory.getType() == Inventory.InventoryType.Storage) {
                safeInventory = inventory;
                break;
            }
        }


        Iterable<ProductPrepackageQuantity> prepackageQuantities = rule.getBean(ProductPrepackageQuantityRepository.class).getQuantitiesForProduct(trans.getCompanyId(), trans.getShopId(), alaskanIce.getId());
        // Find a product that has
        // Find any prepackages with quantity

        ProductPrepackageQuantity quantity = null;
        for (ProductPrepackageQuantity qty : prepackageQuantities) {
            if (qty.getInventoryId().equalsIgnoreCase(safeInventory.getId()) && qty.getQuantity() > 0) {
                quantity = qty;
                break;
            }
        }


        // Now let's add the quantity to the cart
        OrderItem orderItem = new OrderItem();
        orderItem.setProductId(alaskanIce.getId());
        orderItem.setQuantity(new BigDecimal(3));
        orderItem.setPrepackageItemId(quantity.getPrepackageItemId());
        trans.getCart().getItems().clear();
        trans.getCart().getItems().add(orderItem);

        QueuePOSService queuePOSService = rule.getBean(QueuePOSService.class);
        ShopService shopService = rule.getBean(ShopService.class);
        Transaction preparedTrans = queuePOSService.completeTransaction(trans.getId(), trans, false);

        System.out.print(preparedTrans);

    }

}
