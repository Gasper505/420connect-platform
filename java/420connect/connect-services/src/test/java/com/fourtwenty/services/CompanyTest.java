package com.fourtwenty.services;

import com.fourtwenty.core.domain.models.company.Company;
import com.fourtwenty.core.domain.models.developer.PartnerKey;
import com.fourtwenty.core.domain.repositories.dispensary.CompanyRepository;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.security.tokens.TerminalUser;
import com.fourtwenty.core.services.mgmt.CompanyService;
import com.fourtwenty.core.services.thirdparty.GoogleAPIService;
import com.fourtwenty.core.services.thirdparty.models.GoogleShortURLResult;
import com.fourtwenty.core.util.JsonSerializer;
import com.fourtwenty.core.util.SecurityUtil;
import com.fourtwenty.test.BaseTest;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.junit.Test;
import sun.awt.image.FileImageSource;
import sun.awt.image.ImageFormatException;
import sun.awt.image.JPEGImageDecoder;

import javax.inject.Inject;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mdo on 9/4/16.
 */

public class CompanyTest extends BaseTest {


    @Test
    public void testService() {
        Company company = rule.getBean(CompanyRepository.class).getCompanyByEmail("support@med.420connect.com");
        System.out.println(company);

        Company company2 = rule.getBean(CompanyService.class).getCurrentCompany();
        System.out.println(company);
    }

    @Test
    public void createPartnerKey() {

        SecurityUtil securityUtil = rule.getBean(SecurityUtil.class);
        PartnerKey partnerKey = new PartnerKey();
        partnerKey.prepare();
        partnerKey.setName("Strainge");
        partnerKey.setKey(securityUtil.generateKey());
        partnerKey.setActive(true);

        String json = JsonSerializer.toJson(partnerKey);
        System.out.println(json);
    }

    @Test
    public void generatePassword() {
        SecurityUtil securityUtil = rule.getBean(SecurityUtil.class);
        String encryptedPassword = securityUtil.encryptPassword("4UMwx@PUr5H");

        System.out.println(encryptedPassword);
    }

    @Test
    public void testGoogleURL() {

        //@Inject
        //GoogleAPIService googleAPIService;
        GoogleAPIService service = rule.getBean(GoogleAPIService.class);
        GoogleShortURLResult shortURL = service.requestShortUrl("www.blaze.me");
        System.out.println(shortURL);
    }


    @Test
    public void testCreateToken() {
        SecurityUtil securityUtil = rule.getBean(SecurityUtil.class);

        String companyId = "5b8ee53288bae70891525070";
        String shopId = "5b8ee53288bae70891525086";
        String employeeId = "5b8ee53288bae708915250ac";

        String terminalId = "5b8f1437f385140897e5899b";
        String employeeFirstName = "Adie";
        String employeeLastName = "Meiri";

        ConnectAuthToken connectAuthToken = createNewToken(companyId, shopId, employeeId, terminalId, employeeFirstName, employeeLastName);

        String accessToken = securityUtil.createAccessToken(connectAuthToken);
        System.out.println("AccessToken: \n" + accessToken);


        for (int i = 0; i < 2; i++) {
            ObjectId myObjectId = new ObjectId();
            System.out.println("objectId2: " + myObjectId.toString());
        }
    }


    public static final String UTF8_BOM = "\uFEFF";

    private static String removeUTF8BOM(String s) {
        ///if (s.startsWith(UTF8_BOM)) {
        s = s.substring(1);
        //}
        return s;
    }

    private static void checkCorruptImage(String filePath) throws IOException, ImageFormatException {
        JPEGImageDecoder decoder = new JPEGImageDecoder(new FileImageSource(filePath), new FileInputStream(filePath));
        decoder.produceImage();
    }

    private static void convertStringToImageByteArray(String imageString) throws UnsupportedEncodingException {

        OutputStream outputStream = null;

        byte[] imageInByteArray = imageString.getBytes();
        //byte [] imageInByteArray = Base64.decodeBase64(imageString);
        try {
            outputStream = new FileOutputStream("/Users/mdo/Downloads/Spring3.jpg");
            outputStream.write(imageInByteArray);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    private ConnectAuthToken createNewToken(String companyId,
                                            String shopId,
                                            String managerId,
                                            String terminalId,
                                            String firstName,
                                            String lastName) {
        ConnectAuthToken newToken = new ConnectAuthToken();
        newToken.setCompanyId(companyId);
        newToken.setShopId(shopId);
        newToken.setManagerId(managerId);
        newToken.setTerminalId(terminalId);
        List<TerminalUser> userList = new ArrayList<>();
        userList.add(new TerminalUser(managerId, firstName, lastName));
        newToken.setActiveUsers(userList);
        newToken.setInitDate(DateTime.now().getMillis());
        newToken.setExpirationDate(DateTime.now().plusMinutes(30).getMillis());
        return newToken;
    }
}
