package com.fourtwenty.services;

import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.domain.models.company.Shop;
import com.fourtwenty.core.domain.models.transaction.Conversation;
import com.fourtwenty.core.domain.repositories.dispensary.ShopRepository;
import com.fourtwenty.core.managed.AmazonServiceManager;
import com.fourtwenty.core.rest.features.TwilioMessageRequest;
import com.fourtwenty.core.services.mgmt.ConversationService;
import com.fourtwenty.test.BaseTest;
import com.google.common.collect.Lists;
import com.twilio.Twilio;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.List;

public class ConversationTest extends BaseTest {
    private static final Logger logger = LoggerFactory.getLogger(ConversationTest.class);

    @Inject
    ConnectConfiguration connectConfiguration;
    @Inject
    private AmazonServiceManager amazonServiceManager;
    @Inject
    private ShopRepository shopRepository;


    public ConversationTest() {

        Twilio.init("AC3d36852ff4ea8b94d5f309c5bc505df1", "adad42c6ad637ec917a739ff978df808");

    }


    @Test
    public void testSendConversation() {
        //sendConversation 57ff4fed3a56523077515fda   57849f645c6b351f2b08c4fa
        TwilioMessageRequest request = new TwilioMessageRequest();
        request.setTransactionId("588854ec3a56521b4dd9891e");
        request.setMessage("hello ravi");
        rule.getBean(ConversationService.class).sendConversation(request);
        logger.info("conversation has been sent");
    }

    @Test
    public void testReceiveConversation() {
        //receiveConversation
        rule.getBean(ConversationService.class).receiveConversation("588854ec3a56521b4dd9891e", "hello twilio", "SM00f2a55e5187423795aff8df9d2bbdbf");
        logger.info("Conversation has been received");
    }

    @Test
    public void testUpdateConversation() {
        //updateConversationByMessageId
        rule.getBean(ConversationService.class).updateConversationByMessageId("SM00f2a55e5187423795aff8df9d2bbdbf", "sent");
        logger.info("Conversation has been  updated for this messageId " + "messageId");
    }

    @Test
    public void testGetConversationByMessageId() {
        //getConversationByMessageId
        final Conversation conversation = rule.getBean(ConversationService.class).getConversationByMessageId("SM00f2a55e5187423795aff8df9d2bbdbf");
        logger.info("Conversation test for sid" + conversation.getSid());
        Assert.assertTrue(true);
    }

    @Test
    public void testGetTransactionConversation() {
        //getTransactionConversation
        final List<Conversation> conversationTransaction = rule.getBean(ConversationService.class).getTransactionConversation("57ff4fed3a56523077515fda", 0, 0);

        for (Conversation conversation : conversationTransaction) {
            logger.info("**************message and sid of this transation************" + conversation.getMessage() + " / " + conversation.getSid());

        }
        logger.info("size of Conversation for transactionId" + conversationTransaction.size());
    }

    @Test
    public void testGetTransactionToAndFromNumber() {
        //getTransactionToAndFromNumber
        final String transactionId = rule.getBean(ConversationService.class).getTransactionToAndFromNumber("+19099395721", "+917737784640");
        logger.info("transactionId for toNUmber and from NUmber :" + transactionId);
    }

    @Test
    public void testSendSmsMessage() {
        try {
            Shop shop = shopRepository.getById("56cf846ee38179985229e59e");
            amazonServiceManager.sendSMSMessage(Lists.newArrayList("15592307500"),
                    String.format("Hello world: %s", "1234"), shop);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
