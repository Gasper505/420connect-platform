package com.fourtwenty.services;

import com.fourtwenty.core.lifecycle.TwilioStartup;
import com.fourtwenty.core.services.sms.IncomingSMSService;
import com.fourtwenty.core.services.twilio.TwilioMessageService;
import com.fourtwenty.test.BaseTest;
import org.junit.Test;

public class IncomingSMSTest extends BaseTest {

    @Test
    public void testService() {

        IncomingSMSService incomingSMSService = rule.getBean(IncomingSMSService.class);

        //incomingSMSService.receiveSMSMessage("remove", "+15592307500","+14159805158");
        //incomingSMSService.receiveSMSMessage("start", "+15592307500","+14159805158");
        //incomingSMSService.receiveSMSMessage("stop", "+15592307500","+14159805158");
        //incomingSMSService.receiveSMSMessage("unstop", "+15592307500","+14159805158");
        incomingSMSService.receiveSMSMessage("start me please", "+15592307500", "+14159805158");

    }

    @Test
    public void testCreateTwilioNumber() {
        TwilioStartup twilioStartup = rule.getBean(TwilioStartup.class);
        twilioStartup.run();
        TwilioMessageService service = rule.getBean(TwilioMessageService.class);

        String phoneNumber = service.findAndCreatePhoneNumber("Blaze Test", "US", "CA");

        System.out.println("New Number: " + phoneNumber);
    }

}
