package com.fourtwenty.services;

import com.fourtwenty.core.domain.models.product.*;
import com.fourtwenty.core.domain.repositories.dispensary.ProductPrepackageQuantityRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.rest.dispensary.requests.inventory.InventoryTransferStatusRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.InventoryTransferHistoryResult;
import com.fourtwenty.core.services.mgmt.InventoryTransferHistoryService;
import com.fourtwenty.core.services.mgmt.ProductService;
import com.fourtwenty.test.BaseTest;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.LinkedHashSet;

import static org.assertj.core.api.Assertions.assertThat;

public class InventoryTransferTest extends BaseTest {

    public static final Logger LOGGER = LoggerFactory.getLogger(InventoryTransferTest.class);

    public InventoryTransferTest() {
    }

    /**
     * Test for getting inventory history by id
     */
    @Test
    public void testGetInventoryHistoryById() {
        InventoryTransferHistoryResult inventoryTransferHistoryById = rule.getBean(InventoryTransferHistoryService.class).getInventoryTransferHistoryById("5a4b63a93a565221d0e87437");
        LOGGER.info("Inventory Transfer History Id " + inventoryTransferHistoryById.getId());
        Assert.assertEquals("5a4b63a93a565221d0e87437", inventoryTransferHistoryById.getId());
    }

    /**
     * Test for getting inventory history list of inventory history
     */
    @Test
    public void testGetInventoryHistoryList() {
        SearchResult<InventoryTransferHistoryResult> inventoryHistories = rule.getBean(InventoryTransferHistoryService.class).getInventoryHistories(0, 10, "ACCEPTED,DECLINED", "01/01/2000", "01/01/2018");
        LOGGER.info("Total number of inventory histories : " + inventoryHistories.getValues().size());
    }

    /**
     * Test for updating inventory history
     */
    @Test
    public void testUpdateInventoryHistory() {
        InventoryTransferHistoryResult inventoryTransferHistoryById = rule.getBean(InventoryTransferHistoryService.class).getInventoryTransferHistoryById("5a4b63a93a565221d0e87437");
        Assert.assertNotNull(inventoryTransferHistoryById);
        for (InventoryTransferLog inventoryTransferLog : inventoryTransferHistoryById.getTransferLogs()) {
            inventoryTransferLog.setTransferAmount(new BigDecimal(5));
        }
        InventoryTransferHistory inventoryTransferHistoryResult = rule.getBean(InventoryTransferHistoryService.class).updateInventoryHistoryResult("5a4b63a93a565221d0e87437", inventoryTransferHistoryById);
        LOGGER.info("Inventory transfer result update " + inventoryTransferHistoryResult.getId());
        for (InventoryTransferLog transferLog : inventoryTransferHistoryResult.getTransferLogs()) {
            Assert.assertEquals(new BigDecimal(5), transferLog.getTransferAmount());
        }

    }


    /**
     * Test for transfer quantity by productQuantity
     */
    @Test
    public void testByProductQuantityTransfer() {
        Product product = rule.getBean(ProductService.class).getProductById("57849f625c6b351f2b08bd01");
        BigDecimal fromQuantity = new BigDecimal(0);
        BigDecimal toQuantity = new BigDecimal(0);
        BigDecimal transferAmount = new BigDecimal(1);
        String fromInventoryId = "59bbee2f3a56527c9f18c0bf";
        String toInventoryId = "59c1f17d3a56523c33d4c06f";

        for (ProductQuantity productQuantity : product.getQuantities()) {
            if (productQuantity.getInventoryId().equals(fromInventoryId)) {
                fromQuantity = productQuantity.getQuantity();
            } else if (productQuantity.getInventoryId().equals(toInventoryId)) {
                toQuantity = productQuantity.getQuantity();
            }
        }

        LinkedHashSet<InventoryTransferLog> inventoryTransferLogLinkedHashSet = new LinkedHashSet<>();
        InventoryTransferLog inventoryTransferLog = createInventoryLog(product.getId(), transferAmount, fromQuantity, toQuantity, null);
        inventoryTransferLogLinkedHashSet.add(inventoryTransferLog);
        InventoryTransferHistory transfer = createTransfer(product, fromInventoryId, toInventoryId, inventoryTransferLogLinkedHashSet);
        Assert.assertNotNull(transfer);
        Assert.assertEquals(InventoryTransferHistory.TransferStatus.PENDING, transfer.getStatus());

        if (transfer.getStatus().equals(InventoryTransferHistory.TransferStatus.PENDING)) {

            //Check update higher amount
            updateHigherAmount(transfer, fromQuantity);

            //check update lower amount
            updateLowerAmount(transfer, fromQuantity);

            //check status decline
            InventoryTransferHistory declinedInventoryTransferHistory = statusDecline(transfer.getId(), fromQuantity, toQuantity);
            Assert.assertEquals(declinedInventoryTransferHistory.getStatus(), InventoryTransferHistory.TransferStatus.DECLINED);

        }

        //check status accepted
        LinkedHashSet<InventoryTransferLog> transferLogLinkedHashSet = new LinkedHashSet<>();
        InventoryTransferLog inventoryTransferLogs = createInventoryLog(product.getId(), transferAmount, fromQuantity, toQuantity, null);
        transferLogLinkedHashSet.add(inventoryTransferLogs);
        InventoryTransferHistory transferForAccepted = createTransfer(product, fromInventoryId, toInventoryId, transferLogLinkedHashSet);
        Assert.assertNotNull(transferForAccepted);
        Assert.assertEquals(InventoryTransferHistory.TransferStatus.PENDING, transferForAccepted.getStatus());
        if (transferForAccepted.getStatus().equals(InventoryTransferHistory.TransferStatus.PENDING)) {
            InventoryTransferHistory acceptedInventoryTransferHistory = statusAccepted(transferForAccepted.getId(), fromQuantity, toQuantity);
            Assert.assertEquals(acceptedInventoryTransferHistory.getStatus(), InventoryTransferHistory.TransferStatus.ACCEPTED);

        }
    }

    /**
     * Test for transfer quantity By prepackageQuantity
     */
    @Test
    public void testByPrepackageQuantity() {
        Product product = rule.getBean(ProductService.class).getProductById("57849f625c6b351f2b08bd01");
        BigDecimal fromQuantity;
        BigDecimal toQuantity;
        BigDecimal transferAmount = new BigDecimal(1);
        String fromInventoryId = "59bbee2f3a56527c9f18c0bf";
        String toInventoryId = "5a167b7c3a56522ff3c2518e";
        String prepackageItemId = "5a42459a3a565275f2df00a9";

        ProductPrepackageQuantity fromPreQuantity = rule.getBean(ProductPrepackageQuantityRepository.class).getQuantity("56ce8bf389288da6df3a6602",
                "56cf846ee38179985229e59e",
                fromInventoryId,
                prepackageItemId);
        fromQuantity = new BigDecimal(fromPreQuantity.getQuantity());

        ProductPrepackageQuantity toPreQuantity = rule.getBean(ProductPrepackageQuantityRepository.class).getQuantity("56ce8bf389288da6df3a6602",
                "56cf846ee38179985229e59e",
                toInventoryId,
                prepackageItemId);
        toQuantity = new BigDecimal(toPreQuantity.getQuantity());


        //For prepackage

        LinkedHashSet<InventoryTransferLog> linkedHashSet = new LinkedHashSet<>();
        InventoryTransferLog inventoryTransferLogResult = createInventoryLog(product.getId(), transferAmount, fromQuantity, toQuantity, prepackageItemId);
        linkedHashSet.add(inventoryTransferLogResult);
        InventoryTransferHistory transferHistory = createTransfer(product, fromInventoryId, toInventoryId, linkedHashSet);
        Assert.assertNotNull(transferHistory);
        Assert.assertEquals(InventoryTransferHistory.TransferStatus.PENDING, transferHistory.getStatus());
        for (InventoryTransferLog inventoryTransferLog : transferHistory.getTransferLogs()) {
            if (inventoryTransferLog.getPrepackageItemId().equals(prepackageItemId)) {
                // check update higher Amount
                updateHigherAmount(transferHistory, fromQuantity);

                // check update lower amount
                updateLowerAmount(transferHistory, fromQuantity);

                // check status declined
                statusDecline(transferHistory.getId(), fromQuantity, toQuantity);

            }
        }

        // check status accepted

        LinkedHashSet<InventoryTransferLog> linkedHashSetResult = new LinkedHashSet<>();
        InventoryTransferLog inventoryTransferLogLinked = createInventoryLog(product.getId(), transferAmount, fromQuantity, toQuantity, prepackageItemId);
        linkedHashSetResult.add(inventoryTransferLogLinked);
        InventoryTransferHistory inventoryTransferHistory = createTransfer(product, fromInventoryId, toInventoryId, linkedHashSetResult);
        Assert.assertNotNull(inventoryTransferHistory);
        Assert.assertEquals(InventoryTransferHistory.TransferStatus.PENDING, inventoryTransferHistory.getStatus());
        for (InventoryTransferLog inventoryTransferLog : inventoryTransferHistory.getTransferLogs()) {
            if (inventoryTransferLog.getPrepackageItemId().equals(prepackageItemId)) {
                if (inventoryTransferHistory.getStatus().equals(InventoryTransferHistory.TransferStatus.PENDING)) {
                    InventoryTransferHistory acceptTransferHistory = statusAccepted(inventoryTransferHistory.getId(), fromQuantity, toQuantity);
                    Assert.assertEquals(acceptTransferHistory.getStatus(), InventoryTransferHistory.TransferStatus.ACCEPTED);

                }
            }
        }
    }

    private InventoryTransferHistory createTransfer(Product product, String fromInventoryId, String toInventoryId, LinkedHashSet<InventoryTransferLog> transferLogLinkedHashSet) {
        InventoryTransferHistory inventoryTransferHistory = new InventoryTransferHistory();
        inventoryTransferHistory.setFromShopId(product.getShopId());
        inventoryTransferHistory.setToShopId(product.getShopId());
        inventoryTransferHistory.setFromInventoryId(fromInventoryId);
        inventoryTransferHistory.setToInventoryId(toInventoryId);
        inventoryTransferHistory.setCompleteTransfer(false);
        inventoryTransferHistory.setTransferLogs(transferLogLinkedHashSet);
        InventoryTransferHistory inventoryTransferHistoryResult = rule.getBean(InventoryTransferHistoryService.class).bulkTransferInventory(inventoryTransferHistory);

        return inventoryTransferHistoryResult;
    }

    private InventoryTransferLog createInventoryLog(String productId, BigDecimal transferAmount, BigDecimal fromQuantity, BigDecimal toQuantity, String prepackageItemId) {
        InventoryTransferLog inventoryTransferLog = new InventoryTransferLog();
        inventoryTransferLog.setProductId(productId);
        inventoryTransferLog.setPrepackageItemId(prepackageItemId);
        inventoryTransferLog.setTransferAmount(transferAmount);
        inventoryTransferLog.setFinalInventory(toQuantity.add(transferAmount));
        inventoryTransferLog.setFromBatchId("");
        inventoryTransferLog.setToBatchId("");
        inventoryTransferLog.setOrigFromQty(fromQuantity);
        inventoryTransferLog.setOrigToQty(toQuantity);
        inventoryTransferLog.setFinalFromQty(fromQuantity.subtract(transferAmount));

        return inventoryTransferLog;
    }

    /**
     * Test for update inventory transfer status
     */
    @Test
    public void testUpdateInventoryTransferStatus() {
        InventoryTransferHistoryResult inventoryTransferHistoryById = rule.getBean(InventoryTransferHistoryService.class).getInventoryTransferHistoryById("5a7565795be6b6275476908d");
        Assert.assertNotNull(inventoryTransferHistoryById);
        Assert.assertEquals(InventoryTransferHistory.TransferStatus.PENDING, inventoryTransferHistoryById.getStatus());
        InventoryTransferStatusRequest inventoryTransferStatusRequest = new InventoryTransferStatusRequest();
        inventoryTransferStatusRequest.setStatus(false);

        InventoryTransferHistory inventoryTransferHistoryResult = rule.getBean(InventoryTransferHistoryService.class).updateInventoryTransferStatus("5a7565795be6b6275476908d", inventoryTransferStatusRequest);
        Assert.assertEquals(InventoryTransferHistory.TransferStatus.DECLINED, inventoryTransferHistoryResult.getStatus());
    }

    /**
     * Test Update Transfer with higher transfer amt
     *
     * @param inventoryTransferHistoryResult
     * @param fromQuantity
     */
    public void updateHigherAmount(InventoryTransferHistory inventoryTransferHistoryResult, BigDecimal fromQuantity) {
        Assert.assertNotNull(inventoryTransferHistoryResult);
        Assert.assertNotNull(fromQuantity);
        BigDecimal updateHigherAmount;
        BigDecimal updatePrepackageHigherAmount;
        for (InventoryTransferLog transferLog : inventoryTransferHistoryResult.getTransferLogs()) {

            if (StringUtils.isNotBlank(transferLog.getPrepackageItemId()) && ObjectId.isValid(transferLog.getPrepackageItemId())) {
                updatePrepackageHigherAmount = fromQuantity.add(new BigDecimal(10));
                transferLog.setTransferAmount(updatePrepackageHigherAmount);
                //Check update higher amount
                try {
                    InventoryTransferHistory updateInventoryTransferHistory = rule.getBean(InventoryTransferHistoryService.class).updateInventoryHistoryResult(inventoryTransferHistoryResult.getId(), inventoryTransferHistoryResult);
                } catch (Exception e) {
                    assertThat(e)
                            .isInstanceOf(BlazeInvalidArgException.class)
                            .hasMessage("Transfer amount is grater than prePackageQuantity ");
                }
            } else if (inventoryTransferHistoryResult.getFromInventoryId().equals(inventoryTransferHistoryResult.getFromInventoryId())) {
                updateHigherAmount = fromQuantity.add(new BigDecimal(10));
                transferLog.setTransferAmount(updateHigherAmount);
                //Check update higher amount
                try {
                    InventoryTransferHistory updateInventoryTransferHistory = rule.getBean(InventoryTransferHistoryService.class).updateInventoryHistoryResult(inventoryTransferHistoryResult.getId(), inventoryTransferHistoryResult);
                } catch (Exception e) {
                    assertThat(e)
                            .isInstanceOf(BlazeInvalidArgException.class)
                            .hasMessage("Transfer amount is greater than amount available in inventory");
                }
            }
        }
    }


    /**
     * Test update Transfer with lower transfer amt
     *
     * @param inventoryTransferHistoryResult
     * @param fromQuantity
     */
    public void updateLowerAmount(InventoryTransferHistory inventoryTransferHistoryResult, BigDecimal fromQuantity) {
        Assert.assertNotNull(inventoryTransferHistoryResult);
        Assert.assertNotNull(fromQuantity);
        BigDecimal updateLowerAmount;
        BigDecimal updatePrepackageLowerAmount;
        for (InventoryTransferLog transferLog : inventoryTransferHistoryResult.getTransferLogs()) {
            if (StringUtils.isNotBlank(transferLog.getPrepackageItemId()) && ObjectId.isValid(transferLog.getPrepackageItemId())) {
                updatePrepackageLowerAmount = fromQuantity.subtract(fromQuantity.add(new BigDecimal(10)));
                transferLog.setTransferAmount(updatePrepackageLowerAmount);
            } else if (inventoryTransferHistoryResult.getFromInventoryId().equals(inventoryTransferHistoryResult.getFromInventoryId())) {
                updateLowerAmount = fromQuantity.subtract(fromQuantity.add(new BigDecimal(10)));
                transferLog.setTransferAmount(updateLowerAmount);
            }
        }
        try {
            InventoryTransferHistory updateInventory = rule.getBean(InventoryTransferHistoryService.class).updateInventoryHistoryResult(inventoryTransferHistoryResult.getId(), inventoryTransferHistoryResult);
        } catch (Exception e) {
            assertThat(e)
                    .isInstanceOf(BlazeInvalidArgException.class)
                    .hasMessage("Transfer amount must be greater than 0.");
        }
    }

    /**
     * Test status decline in inventory transfer history
     *
     * @param inventoryTransferHistoryId
     * @param fromQuantity
     * @param toQuantity
     * @return
     */
    public InventoryTransferHistory statusDecline(String inventoryTransferHistoryId, BigDecimal fromQuantity, BigDecimal toQuantity) {
        Assert.assertNotNull(inventoryTransferHistoryId);
        Assert.assertNotNull(fromQuantity);
        Assert.assertNotNull(toQuantity);
        InventoryTransferStatusRequest inventoryTransferStatusRequest = new InventoryTransferStatusRequest();
        inventoryTransferStatusRequest.setStatus(false);

        InventoryTransferHistory inventoryTransferHistoryResult = rule.getBean(InventoryTransferHistoryService.class).updateInventoryTransferStatus(inventoryTransferHistoryId, inventoryTransferStatusRequest);
        Assert.assertNotNull(inventoryTransferHistoryResult);
        BigDecimal updatedFromQuantity;
        BigDecimal updatedToQuantity;
        BigDecimal updateFromPreQuantity;
        BigDecimal updateToPreQuantity;

        for (InventoryTransferLog transferLog : inventoryTransferHistoryResult.getTransferLogs()) {
            if (StringUtils.isNotBlank(transferLog.getPrepackageItemId()) && ObjectId.isValid(transferLog.getPrepackageItemId())) {

                ProductPrepackageQuantity fromPreQuantity = rule.getBean(ProductPrepackageQuantityRepository.class).getQuantity(inventoryTransferHistoryResult.getCompanyId(),
                        inventoryTransferHistoryResult.getFromShopId(),
                        inventoryTransferHistoryResult.getFromInventoryId(),
                        transferLog.getPrepackageItemId());


                ProductPrepackageQuantity toPreQuantity = rule.getBean(ProductPrepackageQuantityRepository.class).getQuantity(inventoryTransferHistoryResult.getCompanyId(),
                        inventoryTransferHistoryResult.getFromShopId(),
                        inventoryTransferHistoryResult.getFromInventoryId(),
                        transferLog.getPrepackageItemId());
                if (fromPreQuantity.getInventoryId().equals(inventoryTransferHistoryResult.getFromInventoryId())) {
                    updateFromPreQuantity = new BigDecimal(fromPreQuantity.getQuantity());
                    Assert.assertTrue(updateFromPreQuantity.equals(fromQuantity));
                }
                if (toPreQuantity.getInventoryId().equals(inventoryTransferHistoryResult.getToInventoryId())) {
                    updateToPreQuantity = new BigDecimal(toPreQuantity.getQuantity());
                    Assert.assertTrue(updateToPreQuantity.equals(toQuantity));
                }

            } else {
                Product updatedProduct = rule.getBean(ProductService.class).getProductById(transferLog.getProductId());
                for (ProductQuantity productQuantity : updatedProduct.getQuantities()) {
                    if (productQuantity.getInventoryId().equals(inventoryTransferHistoryResult.getFromInventoryId())) {
                        updatedFromQuantity = productQuantity.getQuantity();
                        Assert.assertTrue(updatedFromQuantity.equals(fromQuantity));
                    } else if (productQuantity.getInventoryId().equals(inventoryTransferHistoryResult.getToInventoryId())) {
                        updatedToQuantity = productQuantity.getQuantity();
                        Assert.assertTrue(updatedToQuantity.equals(toQuantity));
                    }
                }
            }
        }
        return inventoryTransferHistoryResult;
    }

    /**
     * Test status accepted in inventory transfer history
     *
     * @param inventoryTransferHistoryId
     * @param fromQuantity
     * @param toQuantity
     * @return
     */
    public InventoryTransferHistory statusAccepted(String inventoryTransferHistoryId, BigDecimal fromQuantity, BigDecimal toQuantity) {
        Assert.assertNotNull(inventoryTransferHistoryId);
        Assert.assertNotNull(fromQuantity);
        Assert.assertNotNull(toQuantity);
        InventoryTransferStatusRequest inventoryTransferStatusRequest = new InventoryTransferStatusRequest();
        inventoryTransferStatusRequest.setStatus(true);

        InventoryTransferHistory updateInventoryTransferStatus = rule.getBean(InventoryTransferHistoryService.class).updateInventoryTransferStatus(inventoryTransferHistoryId, inventoryTransferStatusRequest);
        Assert.assertNotNull(updateInventoryTransferStatus);
        BigDecimal finalFromQuantity;
        BigDecimal finalToQuantity;
        BigDecimal finalFromPreQuantity;
        BigDecimal finalToPreQuantity;
        for (InventoryTransferLog inventoryTransferLog : updateInventoryTransferStatus.getTransferLogs()) {
            if (StringUtils.isNotBlank(inventoryTransferLog.getPrepackageItemId()) && ObjectId.isValid(inventoryTransferLog.getPrepackageItemId())) {
                ProductPrepackageQuantity fromPreQuantity = rule.getBean(ProductPrepackageQuantityRepository.class).getQuantity(updateInventoryTransferStatus.getCompanyId(),
                        updateInventoryTransferStatus.getFromShopId(),
                        updateInventoryTransferStatus.getFromInventoryId(),
                        inventoryTransferLog.getPrepackageItemId());
                ProductPrepackageQuantity toPreQuantity = rule.getBean(ProductPrepackageQuantityRepository.class).getQuantity(updateInventoryTransferStatus.getCompanyId(),
                        updateInventoryTransferStatus.getToShopId(),
                        updateInventoryTransferStatus.getToInventoryId(),
                        inventoryTransferLog.getPrepackageItemId());
                if (fromPreQuantity.getInventoryId().equals(updateInventoryTransferStatus.getFromInventoryId())) {
                    finalFromPreQuantity = new BigDecimal(fromPreQuantity.getQuantity());
                    Assert.assertTrue(finalFromPreQuantity.doubleValue() == (fromQuantity.doubleValue() - (inventoryTransferLog.getTransferAmount().doubleValue())));
                    if (toPreQuantity.getInventoryId().equals(updateInventoryTransferStatus.getToInventoryId())) {
                        finalToPreQuantity = new BigDecimal(toPreQuantity.getQuantity());
                        Assert.assertTrue(finalToPreQuantity.doubleValue() == (toQuantity.doubleValue() + (inventoryTransferLog.getTransferAmount().doubleValue())));
                    }

                } else {
                    Product productById = rule.getBean(ProductService.class).getProductById(inventoryTransferLog.getProductId());
                    for (ProductQuantity productQuantity : productById.getQuantities()) {
                        if (productQuantity.getInventoryId().equals(fromQuantity)) {
                            finalFromQuantity = productQuantity.getQuantity();
                            Assert.assertTrue(finalFromQuantity.equals(fromQuantity.subtract(inventoryTransferLog.getTransferAmount())));
                        } else if (productQuantity.getInventoryId().equals(toQuantity)) {
                            finalToQuantity = productQuantity.getQuantity();
                            Assert.assertTrue(finalToQuantity.equals(toQuantity.add(inventoryTransferLog.getTransferAmount())));
                        }
                    }
                }
            }
        }
        return updateInventoryTransferStatus;
    }
}
