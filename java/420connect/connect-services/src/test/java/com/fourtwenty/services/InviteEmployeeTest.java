package com.fourtwenty.services;

import com.fourtwenty.core.domain.models.company.InviteEmployee;
import com.fourtwenty.core.rest.dispensary.requests.employees.EmployeeInviteRequest;
import com.fourtwenty.core.rest.dispensary.requests.employees.EmployeeInviteTokenRequest;
import com.fourtwenty.core.rest.dispensary.requests.employees.RegisterInviteeRequest;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.services.mgmt.InviteEmployeeService;
import com.fourtwenty.test.BaseTest;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class InviteEmployeeTest extends BaseTest {

    public static final Logger LOGGER = LoggerFactory.getLogger(InviteEmployeeTest.class);

    public InviteEmployeeTest() {
    }

    /**
     * Test for invite employee
     */
    @Test
    public void testAddInviteEmployee() {
        EmployeeInviteRequest employeeInviteRequest = new EmployeeInviteRequest();
        employeeInviteRequest.setFirstName("Raja");
        employeeInviteRequest.setLastName("D Vyas");
        employeeInviteRequest.setRoleId("57849f415c6b351f2b08ba64");
        employeeInviteRequest.setPhoneNumber("9876543210");
        employeeInviteRequest.setAddress("Jaipur");
        employeeInviteRequest.setState("Rajasthan");
        employeeInviteRequest.setZipcode("303030");
        employeeInviteRequest.setCity("Mansarover");
        employeeInviteRequest.setEmail("testjava03@gmail.com");
        List<String> shops = new ArrayList<>();
        shops.add("56cf846ee38179985229e59e");
        employeeInviteRequest.setShops(shops);
        InviteEmployee inviteEmployee = rule.getBean(InviteEmployeeService.class).inviteEmployee(employeeInviteRequest);
        LOGGER.info("Test Invite Employee " + inviteEmployee.getId());
        Assert.assertNotNull(inviteEmployee);
    }

    /**
     * Test for getting invite info
     */
    @Test
    public void testGetInviteInformation() {
        EmployeeInviteTokenRequest employeeInviteTokenRequest = new EmployeeInviteTokenRequest();
        employeeInviteTokenRequest.setToken("SXfyiRBhYsbs9NrzyMj/OYQbUUtubqLdfC/7pIRsxbAPDnJKZ/gKbpKA7e9t1gqxJ2KRvgAgyjCmZzx7RKsKZMl4RAOleT75uFvrSDusofSoLFjq6lnE7rEghmUX4LZxazgJfYmO4jnQHjw+sWGv/oMVP2pW+szMZpVMbZh8Lrko3TKWSaEspdGljeQG6tQcDpxj+ljft9FJ+s7eEwvd3niRqCBZMGH9xvZCiwhEcSs=");
        InviteEmployee inviteeInfo = rule.getBean(InviteEmployeeService.class).getInviteeInfo(employeeInviteTokenRequest);
        LOGGER.info("Invite Employee information " + inviteeInfo.getId());
        Assert.assertNotNull(inviteeInfo);
        RegisterInviteeRequest registerInviteeRequest = new RegisterInviteeRequest();
        registerInviteeRequest.setEmail(inviteeInfo.getEmail());
        registerInviteeRequest.setPassword("rajadv@123");
        registerInviteeRequest.setPin("302020");
        registerInviteeRequest.setCompanyId(inviteeInfo.getCompanyId());
        registerInviteeRequest.setDriversLicense("dasfdvgxghsdvswg");
        registerInviteeRequest.setDlExpirationDate("9/8/2020");
        registerInviteeRequest.setVehicleMake("dfasghsghj");
        registerInviteeRequest.setToken("SXfyiRBhYsbs9NrzyMj/OYQbUUtubqLdfC/7pIRsxbAPDnJKZ/gKbpKA7e9t1gqxJ2KRvgAgyjCmZzx7RKsKZMl4RAOleT75uFvrSDusofSoLFjq6lnE7rEghmUX4LZxazgJfYmO4jnQHjw+sWGv/oMVP2pW+szMZpVMbZh8Lrko3TKWSaEspdGljeQG6tQcDpxj+ljft9FJ+s7eEwvd3niRqCBZMGH9xvZCiwhEcSs=");
        InviteEmployee inviteEmployee = rule.getBean(InviteEmployeeService.class).registerInvitee(registerInviteeRequest);
        LOGGER.info("Invite Employee Info " + inviteEmployee.getId());
        Assert.assertEquals(true, inviteEmployee.isRegistered());
    }

    /**
     * Test for getting invite employee by id
     */
    @Test
    public void testGetInviteEmployeeById() {
        InviteEmployee inviteEmployeeById = rule.getBean(InviteEmployeeService.class).getInviteEmployeeById("5a5f3e445be6b6408475a513");
        LOGGER.info("Invite Employee test for get Invite Employee id " + inviteEmployeeById.getId());
        Assert.assertEquals("5a5f3e445be6b6408475a513", inviteEmployeeById.getId());

    }

    /**
     * Test for getting all invite employee list
     */
    @Test
    public void testGetInviteEmployeeList() {
        SearchResult<InviteEmployee> inviteEmployeeList = rule.getBean(InviteEmployeeService.class).getInviteEmployeeList(0, 10);
        LOGGER.info("Total no of invite employee " + inviteEmployeeList.getValues().size());
        Assert.assertNotNull(inviteEmployeeList);
    }


}
