package com.fourtwenty.services;

import com.fourtwenty.core.domain.models.company.Employee;
import com.fourtwenty.core.domain.models.transaction.Transaction;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.thirdparty.onfleet.models.OnFleetTask;
import com.fourtwenty.core.thirdparty.onfleet.models.request.CreateEmployeeRequest;
import com.fourtwenty.core.thirdparty.onfleet.models.request.OnFleetAddRequest;
import com.fourtwenty.core.thirdparty.onfleet.models.request.SynchronizeEmployeeRequest;
import com.fourtwenty.core.thirdparty.onfleet.models.response.*;
import com.fourtwenty.core.thirdparty.onfleet.services.OnFleetService;
import com.fourtwenty.test.BaseTest;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class OnFleetTest extends BaseTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(OnFleetTest.class);
    private static final String apiKey = "fe9fee64cc0ce2c8ad2fc9fe34fecc1d";

    public OnFleetTest() {

    }

    /**
     * Test API Key
     */
    @Test
    public void testApiKey() {
        AuthenticateAPIKeyResult result = rule.getBean(OnFleetService.class).authenticateApiKey(apiKey);
        LOGGER.info("API key validation status " + result.isValid());
        Assert.assertTrue(result.isValid());
    }

    /**
     * Test for get worker by ID
     */
    @Test
    public void testGetWorkerById() {
        String workerId = "sOS4REKWU7n0qY~C6gJcsD*m";
        WorkerResult workerResult = rule.getBean(OnFleetService.class).getWorkerById(workerId, apiKey);
        LOGGER.info("Worker Info " + workerResult.getId());
        Assert.assertEquals("sOS4REKWU7n0qY~C6gJcsD*m", workerResult.getId());
    }

    /**
     * Test create employee at onFleet
     */
    @Test
    public void testCreateEmployee() {
        String employeeId = "59c8f49f3a565236f2c7ae76";
        CreateEmployeeRequest request = new CreateEmployeeRequest();
        request.setTeamId("jwAhNk3ez0dD9n11ehx*fg~8");
        EmployeeSynchronizeResult result = rule.getBean(OnFleetService.class).createWorkerForEmployee(employeeId, request);
        LOGGER.info("Create Employee @onFleet :" + result.isSynced());
        Assert.assertTrue(result.isSynced());
    }

    /**
     * Test for update on-fleet information for shop
     */
    @Test
    public void testUpdateOnFleetForShop() {
        String shopId = "597ed4513a56521d8c70a979";
        OnFleetAddRequest addRequest = new OnFleetAddRequest();
        addRequest.setShopId(shopId);
        addRequest.setApiKey(apiKey);
        addRequest.setOrganizationId("l84xl*XH0p1IC2pOtRjuuIo3");
        addRequest.setOrganizationName("Jennifer's Java test account");
        addRequest.setHubId("5s6TUUygFhb15H1PeXW02QGs");
        addRequest.setHubName("");
        addRequest.setEnableOnFleet(false);
        UpdateOnFleetShopInfoResult result = rule.getBean(OnFleetService.class).updateShopOnFleetInformation(shopId, addRequest);
        LOGGER.info("Updated Shop " + result.isOnFleetInfoUpdated());
        Assert.assertFalse(result.getShop().isEnableOnFleet());
    }

    /**
     * Test for update employee's on-fleet information
     */
    @Test
    public void updateEmployeeOnFleet() {
        String employeeId = "57a011dddad1b4671a347810";
        CreateEmployeeRequest request = new CreateEmployeeRequest();
        request.setTeamId("jwAhNk3ez0dD9n11ehx*fg~8");
        Employee employee = rule.getBean(OnFleetService.class).updateEmployeeOnFleetDetails(employeeId, request);
        LOGGER.info("Updated Employee " + employee.getId());
        Assert.assertEquals("57a011dddad1b4671a347810", employee.getId());
    }

    @Test
    public void testCreateOnFleetTask() {
        Transaction transaction = new Transaction();
        transaction.prepare();
        transaction.setCreateOnfleetTask(true);
        transaction.setAssignedEmployeeId("591697b13a5652713a9daf8f");
        transaction.setMemberId("57849f645c6b351f2b08c3e0");
        transaction.setQueueType(Transaction.QueueType.Delivery);
        rule.getBean(OnFleetService.class).reAssignTask(null, transaction, "Gm6e5SeV585PKlwN~fobmN*G", "", null, null, null);
    }

    /**
     * Test for get company info by API key
     */
    @Test
    public void testGetCompanyByAPI() {
        OrganizationResult organizationResult = rule.getBean(OnFleetService.class).getOrganizationInfoByApiKey(apiKey);
        LOGGER.info("Organization Id " + organizationResult.getId());
        Assert.assertEquals("l84xl*XH0p1IC2pOtRjuuIo3", organizationResult.getId());
    }

    /**
     * Test for get shop info by API key
     */
    @Test
    public void getShopByApi() {
        HubAssignmentResult hub = rule.getBean(OnFleetService.class).getHubInfoByApiKey(apiKey);
        LOGGER.info("Hubs quantity : ", hub.getHubResults().size());
        Assert.assertNotNull(hub.getHubResults());
    }

    /**
     * Test for get all teams for current company
     */
    @Test
    public void getAllTeamsFromCompany() {
        List<TeamByCompanyResult> teamList = rule.getBean(OnFleetService.class).getAllTeamsByCompany();
        LOGGER.info("Team size " + teamList.size());
        Assert.assertNotNull(teamList);
    }

    /**
     * Test for get all workers/employees of on fleet
     */
    @Test
    public void testGetAllWorkers() {
        List<WorkerResult> workerList = rule.getBean(OnFleetService.class).getAllWorker(apiKey, "0,1,2");
        LOGGER.info("Workers Result " + workerList.size());
        Assert.assertNotNull(workerList);
    }

    /**
     * Test for get all team by API key
     */
    @Test
    public void testGetAllTeamByAPI() {
        List<TeamResult> teamResultList = rule.getBean(OnFleetService.class).getAllTeams(apiKey);
        LOGGER.info("Team size " + teamResultList.size());
        Assert.assertNotNull(teamResultList.size());
    }

    /**
     * Test for syncronize employee at on-fleet
     */
    @Test
    public void testSyncronizeEmployee() {
        SynchronizeEmployeeRequest request = new SynchronizeEmployeeRequest();
        request.setTeamId("YCVPtwiw*eKNAB6tsWUixd3U");
        request.setShopId("589a49e13a565223dba31424");
        EmployeeSynchronizeResult employeeSyncResult = rule.getBean(OnFleetService.class).syncEmployeeAtOnFleet("591697b13a5652713a9daf8f", request);
        LOGGER.info("Employee Sync status " + employeeSyncResult.isSynced());
        Assert.assertEquals("m6G4LkiMOEO5hizA69BZ164u", employeeSyncResult.getOnFleetWorkerId());

    }

    /**
     * Test for get all on-fleet tasks
     */
    @Test
    public void getAllOnFleetTasks() {
        long fromDate = new DateTime().withDate(2018, 1, 1).getMillis();
        SearchResult<OnFleetTransactionResult> onFleetTaskList = rule.getBean(OnFleetService.class).getAllTask(fromDate, apiKey);
        LOGGER.info("Tasks List " + onFleetTaskList.getValues().size());
        Assert.assertNotNull(onFleetTaskList);
    }

    /**
     * Test for get on-fleet task by taskId
     */
    @Test
    public void getTaskById() {
        OnFleetTask onFleetTask = rule.getBean(OnFleetService.class).getOnFleetTaskById(apiKey, "CaoGXwtHMWJRJj5X77C128y3");
        LOGGER.info("OnFleet Task " + onFleetTask.getId());
        Assert.assertEquals("CaoGXwtHMWJRJj5X77C128y3", onFleetTask.getId());
    }
}
