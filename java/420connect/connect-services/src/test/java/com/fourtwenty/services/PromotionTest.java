package com.fourtwenty.services;

import com.fourtwenty.core.domain.models.loyalty.Promotion;
import com.fourtwenty.core.domain.repositories.dispensary.PromotionRepository;
import com.fourtwenty.core.exceptions.BlazeInvalidArgException;
import com.fourtwenty.core.rest.dispensary.requests.promotions.PromotionAddRequest;
import com.fourtwenty.core.services.mgmt.PromotionService;
import com.fourtwenty.test.BaseTest;
import com.google.common.collect.Sets;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Random;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created on 23/11/17 9:11 PM by Raja Dushyant Vashishtha
 * Sr. Software Developer
 * email : rajad@decipherzone.com
 * www.decipherzone.com
 */
public class PromotionTest extends BaseTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(PromotionTest.class);

    public PromotionTest() {
    }

    @Test(expected = BlazeInvalidArgException.class)
    public void testAddPromotion() {
        PromotionService promotionService = rule.getBean(PromotionService.class);
        Promotion promotion = addPromotion(0);
        Promotion updatePromotion = promotionService.getPromotionById(promotion.getId());
        assertThat(updatePromotion.getPromoCodes().size()).isEqualTo(promotion.getPromoCodes().size());
    }

    private Promotion addPromotion(int i) {
        PromotionService promotionService = rule.getBean(PromotionService.class);
        PromotionAddRequest promotionAddRequest = new PromotionAddRequest();
        promotionAddRequest.setName("Test Raja" + i);
        promotionAddRequest.setPromotionType(Promotion.PromotionType.Cart);
        Promotion promotion = promotionService.addPromotion(promotionAddRequest);
        promotion.setPromoCodes(Sets.newHashSet("This is test" + i, "This is another test" + i));
        //return promotionService.updatePromotion(promotion.getId(), promotion);
        return promotion;
    }

    @Test
    public void testGetPromotion() {

        PromotionService promotionService = rule.getBean(PromotionService.class);
        Promotion promotion = promotionService.findPromotionWithCode("This is test0");
        PromotionRepository promotionRepository = rule.getBean(PromotionRepository.class);
        List<Promotion> promotionList = promotionRepository.getPromotionByPromoCode(promotion.getCompanyId(), promotion.getShopId(), Sets.newHashSet("This is test"));

        assertThat(promotionList.size()).isEqualTo(1);
        assertThat(promotionList.stream().anyMatch(promotion1 -> promotion.getPromoCodes().contains("This is another test0"))).isTrue();
    }

    @Test(expected = BlazeInvalidArgException.class)
    public void testDuplicatePromoException() {
        int i = new Random().nextInt();
        Promotion promotionI = addPromotion(i);
        Promotion promotionIPP = addPromotion(++i);
        PromotionService promotionService = rule.getBean(PromotionService.class);
        promotionI.setPromoCodes(promotionIPP.getPromoCodes());
        //promotionService.updatePromotion(promotionI.getId(), promotionI);
    }

    @Test(expected = BlazeInvalidArgException.class)
    public void testUniquePromotion() {
        int i = new Random().nextInt();
        Promotion promotionI = addPromotion(i);
        Promotion promotionIPP = addPromotion(++i);
        PromotionService promotionService = rule.getBean(PromotionService.class);
        promotionI.setPromoCodes(Sets.newHashSet(promotionI.getPromoCodes()));
        promotionI.getPromoCodes().remove(0);
        promotionI.getPromoCodes().add(promotionIPP.getPromoCodes().stream().findFirst().get());
        //promotionService.updatePromotion(promotionI.getId(), promotionI);
    }

}
