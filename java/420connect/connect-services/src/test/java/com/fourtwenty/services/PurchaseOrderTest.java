package com.fourtwenty.services;

import com.fourtwenty.core.domain.models.company.CompanyAsset;
import com.fourtwenty.core.domain.models.generic.Asset;
import com.fourtwenty.core.domain.models.purchaseorder.POActivity;
import com.fourtwenty.core.domain.models.purchaseorder.POProductRequest;
import com.fourtwenty.core.domain.models.purchaseorder.PurchaseOrder;
import com.fourtwenty.core.domain.models.purchaseorder.ShipmentBill;
import com.fourtwenty.core.rest.common.EmailRequest;
import com.fourtwenty.core.rest.dispensary.requests.inventory.*;
import com.fourtwenty.core.rest.dispensary.results.SearchResult;
import com.fourtwenty.core.rest.dispensary.results.ShipmentBillResult;
import com.fourtwenty.core.rest.dispensary.results.inventory.PurchaseOrderItemResult;
import com.fourtwenty.core.rest.purchaseorders.POCompleteRequest;
import com.fourtwenty.core.services.mgmt.POActivityService;
import com.fourtwenty.core.services.mgmt.PurchaseOrderService;
import com.fourtwenty.core.services.mgmt.ShipmentBillService;
import com.fourtwenty.test.BaseTest;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by decipher on 8/10/17 12:38 PM
 * Abhishek Samuel (Software Engineer)
 * abhishke.decipher@gmail.com
 * Decipher Zone Softwares
 * www.decipherzone.com
 */
public class PurchaseOrderTest extends BaseTest {

    public static final Logger LOGGER = LoggerFactory.getLogger(PurchaseOrderTest.class);

    public PurchaseOrderTest() {
    }

    /**
     * Test for getting purchase order(po) by id
     */
    @Test
    public void testGetPurchaseOrderById() throws Exception {
        PurchaseOrder purchaseOrderById = rule.getBean(PurchaseOrderService.class).getPurchaseOrderById("5b17d26c74398b439a42a9be");
        LOGGER.info("PO test for get by id : " + purchaseOrderById.getId());
        Assert.assertTrue(true);
    }

    /**
     * Test for getting list of purchase order
     */
    @Test
    public void testGetPurchaseOrderList() throws Exception {
        SearchResult<PurchaseOrderItemResult> allPurchaseOrder = rule.getBean(PurchaseOrderService.class).getAllPurchaseOrder(0, 10, null, "", "", PurchaseOrder.PurchaseOrderSort.DATE);
        LOGGER.info("Total number of PO : " + allPurchaseOrder.getValues().size());
    }

    /**
     * Test for adding PO
     */
    @Test
    public void testAddPurchaseOrder() throws Exception {
        PurchaseOrderAddRequest purchaseOrderRequest = createPurchaseOrderRequest();

        PurchaseOrder purchaseOrder = rule.getBean(PurchaseOrderService.class).addPurchaseOrder(purchaseOrderRequest);
        LOGGER.info("Purchase order added " + purchaseOrder.getId());

    }

    /**
     * Test for getting activity list by po activity id
     */
    @Test
    public void testGetPOActivityList() throws Exception {
        SearchResult<POActivity> allPOActivityList = rule.getBean(POActivityService.class).getAllPOActivityList(0, 10, "59d89b43e1b10818ec4efbcf");
        LOGGER.info("PO Activity List size : " + allPOActivityList.getValues().size());
    }

    /**
     * Test for adding back order
     */
    @Test
    public void testAddBackOrderPO() throws Exception {
    }

    /**
     * Test update PO product request update status
     */
    @Test
    public void testUpdatePOProductRequest() throws Exception {
        POProductRequestUpdate poProductRequestUpdate = new POProductRequestUpdate();
        poProductRequestUpdate.setProductRequestId("59da015c6d21a6194c25a782");
        poProductRequestUpdate.setReceivedQuantity(BigDecimal.valueOf(2));
        poProductRequestUpdate.setStatus(true);

        PurchaseOrderItemResult purchaseOrderItemResult = rule.getBean(PurchaseOrderService.class).updatePOProductRequestStatus("59da015c6d21a6194c25a781", poProductRequestUpdate);
        LOGGER.info(purchaseOrderItemResult.getPoNumber());
    }

    /**
     * Test update PO status
     */
    @Test
    public void testUpdatePurchaseOrderStatus() throws Exception {
        POStatusUpdateRequest updateRequest = createPOStatusUpdateRequest("59da015c6d21a6194c25a781", "Decline");
        PurchaseOrder purchaseOrder = rule.getBean(PurchaseOrderService.class).updatePurchaseOrderStatus("59da015c6d21a6194c25a781", updateRequest);
        LOGGER.info("PO status : " + purchaseOrder.getPurchaseOrderStatus().toString());
    }

    /**
     * Test Manager approval
     */
    @Test
    public void testUpdatePOManagerApproval() throws Exception {
        POManagerApprovalRequest managerApprovalRequest = createManagerApprovalRequest("59da015c6d21a6194c25a781", "Approved");

        PurchaseOrder purchaseOrder = rule.getBean(PurchaseOrderService.class).updatePOManagerApproval("59da015c6d21a6194c25a781", managerApprovalRequest);
        LOGGER.info("PO status : " + purchaseOrder.getPurchaseOrderStatus() + "\tmanager approved date : " + purchaseOrder.getApprovedDate());
    }

    /**
     * Test update purchase order
     */
    @Test
    public void testUpdatePurchaseOrder() throws Exception {
//        PurchaseOrderUpdateRequest updateRequest = updatePurchaseOrderRequest("59da015c6d21a6194c25a781", "59da0b4e6d21a61e81493517");

//        PurchaseOrder purchaseOrder = rule.getBean(PurchaseOrderService.class).updatePurchaseOrder("59da015c6d21a6194c25a781", updateRequest);
//        LOGGER.info("Purchase order : " + purchaseOrder);

    }

    /**
     * Test add PO attachment
     */
    @Test
    public void testAddPOAttachment() throws Exception {
        POAttachmentRequest poAttachmentRequest = new POAttachmentRequest();

        CompanyAsset asset = new CompanyAsset();
        asset.setSecured(true);
        asset.setActive(true);
        asset.setType(Asset.AssetType.Photo);
        asset.setPublicURL("www.google.com");
        asset.setName("Signature");
        asset.setKey("Asset - key");

        poAttachmentRequest.setAsset(asset);

        PurchaseOrder purchaseOrder = rule.getBean(PurchaseOrderService.class).addPOAttachment("59da015c6d21a6194c25a781", poAttachmentRequest);
        LOGGER.info("Attachment size " + purchaseOrder.getCompanyAssetList().size());

    }

    /**
     * Test for get PO attachment by id
     */
    @Test
    public void testGetAttachmentById() throws Exception {
        CompanyAsset poAttachment = rule.getBean(PurchaseOrderService.class).getPOAttachment("59da015c6d21a6194c25a781", "59da0dbe6d21a620e8af8dec");
        LOGGER.info("PO attachment : " + poAttachment.getPublicURL());
    }

    /**
     * Test for update attachment
     */
    @Test
    public void testUpdatePOAttachment() throws Exception {
        POAttachmentRequest request = new POAttachmentRequest();

        CompanyAsset asset = new CompanyAsset();
        asset.setSecured(true);
        asset.setActive(true);
        asset.setType(Asset.AssetType.Photo);
        asset.setPublicURL("www.blaze.com");
        asset.setName("Signature");
        asset.setKey("Asset - key");
        request.setAsset(asset);

        PurchaseOrder purchaseOrder = rule.getBean(PurchaseOrderService.class).updatePOAttachment("59da015c6d21a6194c25a781", "59da0dbe6d21a620e8af8dec", request);
        LOGGER.info("PO attachment : " + purchaseOrder.getCompanyAssetList().size());
    }

    /**
     * Test delete PO attachment
     */
    @Test
    public void testDeletePOAttachment() throws Exception {
        rule.getBean(PurchaseOrderService.class).deletePOAttachment("59da015c6d21a6194c25a781", "59da0dbe6d21a620e8af8dec");
    }

    /**
     * Test set status of PO as ReceivingShipment
     */
    @Test
    public void testReceivePOStatus() throws Exception {
        PurchaseOrder purchaseOrder = rule.getBean(PurchaseOrderService.class).receivePurchaseOrder("59da015c6d21a6194c25a781");
        LOGGER.info("Purchase order status : " + purchaseOrder.getPurchaseOrderStatus().toString());
    }

    /**
     * Test mark status of PO as waiting shipment
     */
    @Test
    public void testMarkPOStatus() throws Exception {
        PurchaseOrder purchaseOrder = rule.getBean(PurchaseOrderService.class).markPurchaseOrder("59da015c6d21a6194c25a781");
        LOGGER.info("Purchase order status : " + purchaseOrder.getPurchaseOrderStatus().toString());
    }

    private PurchaseOrderAddRequest createPurchaseOrderRequest() {
        PurchaseOrderAddRequest purchaseOrderAddRequest = new PurchaseOrderAddRequest();
        purchaseOrderAddRequest.setNotes("PO creation via unit test");
        purchaseOrderAddRequest.setPaymentType("Cash");
        purchaseOrderAddRequest.setPoPaymentTerms("Net 30");
        purchaseOrderAddRequest.setVendorId("57849f615c6b351f2b08bb72");

        List<POProductAddRequest> poProductAddRequestList = new ArrayList<>();
        POProductAddRequest poProductAddRequest = new POProductAddRequest();
        poProductAddRequest.setProductId("57849f625c6b351f2b08bce7");
        poProductAddRequest.setNotes("Product 1");
        poProductAddRequest.setRequestQuantity(BigDecimal.valueOf(7));
        poProductAddRequest.setDiscount(BigDecimal.valueOf(7));

        purchaseOrderAddRequest.setPoProductAddRequestList(poProductAddRequestList);

        return purchaseOrderAddRequest;
    }

    private PurchaseOrder updatePurchaseOrderRequest(String poId, String prId) {
        PurchaseOrder purchaseOrder = new PurchaseOrder();
        purchaseOrder.setNotes("Updated PO");
        purchaseOrder.setPoPaymentTerms(PurchaseOrder.POPaymentTerms.COD);
        purchaseOrder.setVendorId("57849f615c6b351f2b08bb71");

        List<POProductRequest> poProductUpdateRequestList = new ArrayList<>();
        POProductRequest poProductUpdateRequest = new POProductRequest();
        poProductUpdateRequest.setNotes("Updated PR");
        poProductUpdateRequest.setId(prId);
        poProductUpdateRequest.setProductId("59da015c6d21a6194c25a782");
        poProductUpdateRequest.setRequestQuantity(BigDecimal.valueOf(12));
        poProductUpdateRequest.setDiscount(BigDecimal.valueOf(12));

        poProductUpdateRequestList.add(poProductUpdateRequest);
        purchaseOrder.setPoProductRequestList(poProductUpdateRequestList);

        return purchaseOrder;
    }

    private POStatusUpdateRequest createPOStatusUpdateRequest(String poId, String status) {
        POStatusUpdateRequest poStatusUpdateRequest = new POStatusUpdateRequest();
        poStatusUpdateRequest.setStatus(PurchaseOrder.PurchaseOrderStatus.Decline);
        poStatusUpdateRequest.setDeclineMessage("Not required anymore");

        return poStatusUpdateRequest;
    }

    private POManagerApprovalRequest createManagerApprovalRequest(String poId, String status) {
        POManagerApprovalRequest poManagerApprovalRequest = new POManagerApprovalRequest();
        poManagerApprovalRequest.setStatus(status);

        CompanyAsset asset = new CompanyAsset();
        asset.setSecured(true);
        asset.setActive(true);
        asset.setType(Asset.AssetType.Photo);
        asset.setPublicURL("www.google.com");
        asset.setName("Signature");
        asset.setKey("Asset - key");

        poManagerApprovalRequest.setCompanyAsset(asset);

        return poManagerApprovalRequest;
    }

    private POCompleteRequest createPOCompleteRequest() {
        POCompleteRequest completeShipmentArrival = new POCompleteRequest();
        completeShipmentArrival.setDeliveredBy("Eart Delivery services");
        ShipmentBillAddRequest shipmentBillAddRequest = new ShipmentBillAddRequest();

        CompanyAsset asset = new CompanyAsset();
        asset.setSecured(true);
        asset.setActive(true);
        asset.setType(Asset.AssetType.Photo);
        asset.setPublicURL("www.google.com");
        asset.setName("Signature");
        asset.setKey("Asset - key");

        List<CompanyAsset> companyAssetList = new ArrayList<>();
        companyAssetList.add(asset);
        shipmentBillAddRequest.setAssets(companyAssetList);
        shipmentBillAddRequest.setCompletedByEmployeeId("56cfab01e38179985229e5a8");
        shipmentBillAddRequest.setDeliveredBy("Ekart");
        shipmentBillAddRequest.setPaymentStatus("Paid");

        return completeShipmentArrival;
    }

    @Test
    public void testPurchaseOrder() throws Exception {
        PurchaseOrderAddRequest purchaseOrderRequest = createPurchaseOrderRequest();
        PurchaseOrder createdPurchaseOrder = rule.getBean(PurchaseOrderService.class).addPurchaseOrder(purchaseOrderRequest);
        LOGGER.info("After creation of PO status : " + createdPurchaseOrder.getPurchaseOrderStatus().toString());

        PurchaseOrder updatePurchaseOrderRequest = updatePurchaseOrderRequest(createdPurchaseOrder.getId(), "");

        PurchaseOrder updatedPurchaseOrder = rule.getBean(PurchaseOrderService.class).updatePurchaseOrder(createdPurchaseOrder.getId(), updatePurchaseOrderRequest);

        LOGGER.info("Order updated : " + updatedPurchaseOrder.getPurchaseOrderStatus().toString());

        POStatusUpdateRequest waitingApprovalRequest = createPOStatusUpdateRequest(updatedPurchaseOrder.getId(), "RequiredApproval");

        PurchaseOrder waitingApprovedPO = rule.getBean(PurchaseOrderService.class).updatePurchaseOrderStatus(updatedPurchaseOrder.getId(), waitingApprovalRequest);

        LOGGER.info("waiting Approval Status update PO : " + waitingApprovedPO.getPurchaseOrderStatus().toString());

        POManagerApprovalRequest managerApprovalRequest = createManagerApprovalRequest(updatedPurchaseOrder.getId(), "Approved");

        PurchaseOrder poManagerApproved = rule.getBean(PurchaseOrderService.class).updatePOManagerApproval(updatedPurchaseOrder.getId(), managerApprovalRequest);

        LOGGER.info("Manager approval status : " + poManagerApproved.getPurchaseOrderStatus().toString());

        PurchaseOrder markedPurchaseOrder = rule.getBean(PurchaseOrderService.class).markPurchaseOrder(updatedPurchaseOrder.getId());

        LOGGER.info("Marked PO : " + markedPurchaseOrder.getPurchaseOrderStatus().toString());

        PurchaseOrder receivePurchaseOrder = rule.getBean(PurchaseOrderService.class).receivePurchaseOrder(updatedPurchaseOrder.getId());

        LOGGER.info("Receive PO status : " + receivePurchaseOrder.getPurchaseOrderStatus().toString());

        POCompleteRequest pocOmpleteRequest = createPOCompleteRequest();
        ShipmentBillResult shipmentBillResult = rule.getBean(PurchaseOrderService.class).completeShipmentArrival(updatedPurchaseOrder.getId(), pocOmpleteRequest);

        LOGGER.info("Complete Shipment PO : " + shipmentBillResult.getStatus());

    }

    /**
     * Test for shipment bill
     */
    @Test
    public void testShipmentBill() throws Exception {
        ShipmentBill shipmentBillById = rule.getBean(ShipmentBillService.class).getShipmentBillById("59ddb7eb0a50581d22088c56");

        LOGGER.info("Shipment bill id : " + shipmentBillById.getId());

        ShipmentBillPaymentStatusRequest billPaymentRequest = createShipmentBillPaymentRequest();

        ShipmentBill shipmentBill = rule.getBean(ShipmentBillService.class).addShipmentBillPayment(shipmentBillById.getId(), billPaymentRequest);
        LOGGER.info("Shipment bill : " + shipmentBill.getPaymentHistory().size());


    }

    private ShipmentBillPaymentStatusRequest createShipmentBillPaymentRequest() {
        ShipmentBillPaymentStatusRequest request = new ShipmentBillPaymentStatusRequest();
        request.setAmountPaid(BigDecimal.valueOf(5000));
        request.setNotes("Amount paid via test");
        request.setPaidDate(1507701832940l);

        return request;
    }

    @Test
    public void testEmailToAccount() throws Exception {
        EmailRequest emailRequest = new EmailRequest();
        emailRequest.setEmail("abhishek.decipher@gmail.com");
        rule.getBean(PurchaseOrderService.class).sendEmailToAccounting("59ddb7e90a50581d22088c4b", emailRequest);
    }
}
