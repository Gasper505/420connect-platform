package com.fourtwenty.services;

import com.fourtwenty.core.services.mgmt.ShopService;
import com.fourtwenty.core.services.thirdparty.AmazonSQSFifoService;
import com.fourtwenty.test.BaseTest;
import org.junit.Test;

/**
 * Created by mdo on 10/13/17.
 */
public class SQSTests extends BaseTest {


    @Test
    public void testSQS() {
        // Get a list o fmembers

        // Check if there's any existing members in queue, if so, just use existing one
        ShopService shopService = rule.getBean(ShopService.class);
        AmazonSQSFifoService queuePOSService = rule.getBean(AmazonSQSFifoService.class);

        queuePOSService.publishItemToQueue();
    }
}
