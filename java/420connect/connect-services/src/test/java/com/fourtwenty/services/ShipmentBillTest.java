package com.fourtwenty.services;

import com.fourtwenty.test.BaseTest;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ShipmentBillTest extends BaseTest {
    private static final Logger logger = LoggerFactory.getLogger(ShipmentBillTest.class);

    public ShipmentBillTest() {

    }

    @Test
    public void shipmentBillCompleteClosed() {
//        rule.getBean ( ShipmentBillService.class ).completeShipmentArrival ( "" ,"");
        logger.info("Purchase Order has been closed.");
    }
}
