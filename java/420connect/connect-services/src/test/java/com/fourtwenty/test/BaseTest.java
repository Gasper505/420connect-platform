package com.fourtwenty.test;

import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.lifecycle.AppStartup;
import org.junit.Before;
import org.junit.Rule;
import ru.vyarus.dropwizard.guice.test.GuiceyAppRule;
import ru.vyarus.dropwizard.guice.test.spock.UseGuiceyApp;

/**
 * Created by mdo on 9/5/16.
 */
@UseGuiceyApp(value = TestApplication.class)
public class BaseTest {
    @Rule
    public GuiceyAppRule<ConnectConfiguration> rule = new GuiceyAppRule<>(TestApplication.class, "connect-test.yml");

    @Before
    public void doBefore() {
        rule.getBean(AppStartup.class).run();
    }
}
