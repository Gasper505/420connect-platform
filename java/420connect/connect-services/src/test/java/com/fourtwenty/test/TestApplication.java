package com.fourtwenty.test;

import com.blaze.clients.hypur.HypurModule;
import com.blaze.clients.metrcs.MetrcModule;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fourtwenty.core.CoreModule;
import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.exceptions.mappers.ConnectConstraintMapper;
import com.fourtwenty.core.exceptions.mappers.ConnectExceptionMapper;
import com.fourtwenty.core.exceptions.mappers.ConnectLoggingExceptionMapper;
import com.fourtwenty.core.exceptions.mappers.JsonProcessingExceptionMapper;
import com.fourtwenty.core.exceptions.mappers.RuntimeExceptionMapper;
import com.fourtwenty.core.lifecycle.AppStartupListener;
import com.fourtwenty.core.security.AuthenticationFilter;
import com.fourtwenty.installers.ResourceInstaller;
import com.fourtwenty.integrations.linx.LinxModule;
import com.fourtwenty.integrations.weedmap.WeedMapModule;
import com.fourtwenty.quickbook.QuickbookModule;
import com.fourtwenty.reporting.services.ReportCoreModule;
import com.google.common.collect.ImmutableMap;
import com.google.inject.util.Modules;
import com.warehouse.core.BlazeWareHouseCoreModule;
import io.dropwizard.Application;
import io.dropwizard.jersey.errors.EarlyEofExceptionMapper;
import io.dropwizard.server.AbstractServerFactory;
import io.dropwizard.server.DefaultServerFactory;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;
import ru.vyarus.dropwizard.guice.GuiceBundle;
import ru.vyarus.dropwizard.guice.module.installer.feature.LifeCycleInstaller;
import ru.vyarus.dropwizard.guice.module.installer.feature.ManagedInstaller;
import ru.vyarus.dropwizard.guice.module.installer.feature.TaskInstaller;
import ru.vyarus.dropwizard.guice.module.installer.feature.eager.EagerSingletonInstaller;
import ru.vyarus.dropwizard.guice.module.installer.feature.health.HealthCheckInstaller;
import ru.vyarus.dropwizard.guice.module.installer.feature.jersey.JerseyFeatureInstaller;
import ru.vyarus.dropwizard.guice.module.installer.feature.jersey.provider.JerseyProviderInstaller;
import ru.vyarus.dropwizard.guice.module.installer.feature.plugin.PluginInstaller;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import javax.ws.rs.ext.ExceptionMapper;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by mdo on 9/4/16.
 */
public class TestApplication extends Application<ConnectConfiguration> {
    static final Log LOG = LogFactory.getLog(TestApplication.class);
    GuiceBundle<ConnectConfiguration> guiceBundle = null;


    public static void main(String[] args) throws Exception {
        new TestApplication().run(args);
    }

    @Override
    public void initialize(Bootstrap<ConnectConfiguration> bootstrap) {

        bootstrap.getObjectMapper().registerSubtypes(DefaultServerFactory.class);
        bootstrap.getObjectMapper().enable(DeserializationFeature.READ_ENUMS_USING_TO_STRING);

        GuiceBundle.Builder builder = GuiceBundle.<ConnectConfiguration>builder()
                .noDefaultInstallers()
                .installers(new Class[]{LifeCycleInstaller.class,
                        ManagedInstaller.class,
                        JerseyFeatureInstaller.class,
                        JerseyProviderInstaller.class,
                        ResourceInstaller.class,
                        EagerSingletonInstaller.class,
                        HealthCheckInstaller.class,
                        TaskInstaller.class,
                        PluginInstaller.class
                })
                .modules(
                        Modules.override(new CoreModule()).with(new TestModule()),
                        Modules.override(new BlazeWareHouseCoreModule()).with(new TestModule()),
                        new ReportCoreModule(),
                        new MetrcModule(),
                        new HypurModule(),
                        new WeedMapModule(),
                        new LinxModule()
                        )
                .enableAutoConfig(TestApplication.class.getPackage().getName(),
                        CoreModule.class.getPackage().getName());

        postInitialize(bootstrap, builder);
        guiceBundle = builder.build();
        bootstrap.addBundle(guiceBundle);
    }

    @Override
    public void run(final ConnectConfiguration configuration, final Environment environment)
            throws Exception {
        System.setProperty("sun.net.http.allowRestrictedHeaders", "true");

        AbstractServerFactory sf = (AbstractServerFactory) configuration.getServerFactory();
        sf.setRegisterDefaultExceptionMappers(false);
        environment.jersey().register(MultiPartFeature.class);
        environment.jersey().register(guiceBundle.getInjector().getInstance(AuthenticationFilter.class));
        environment.jersey().register(AppStartupListener.class);


        String corsURL = configuration.getCoreUrls();
        if (configuration.getEnv() == ConnectConfiguration.EnvironmentType.LOCAL) {
            corsURL = "*";
        }
        corsURL = "*";

        Map<String, String> corsInitParams = ImmutableMap.of(
                CrossOriginFilter.ALLOWED_ORIGINS_PARAM, corsURL,
                CrossOriginFilter.ALLOWED_METHODS_PARAM, "GET,PUT,POST,DELETE,OPTIONS",
                CrossOriginFilter.ALLOW_CREDENTIALS_PARAM, "true",
                CrossOriginFilter.ALLOWED_HEADERS_PARAM, "Content-Type,Authorization,X-Requested-With,Content-Length,Accept,Origin",
                CrossOriginFilter.ACCESS_CONTROL_ALLOW_CREDENTIALS_HEADER, "true"
        );

        String appContext = environment.getApplicationContext().getContextPath();
        FilterRegistration.Dynamic dFilter = environment.servlets().addFilter("CORSFilter", CrossOriginFilter.class);
        dFilter.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, environment.getApplicationContext().getContextPath() + "*");


        dFilter.setInitParameters(corsInitParams);

        environment.jersey().register(new ConnectExceptionMapper());
        environment.jersey().register(new JsonProcessingExceptionMapper());
        environment.jersey().register(new ConnectConstraintMapper());
        // restore other default exception mappers
        environment.jersey().register(new EarlyEofExceptionMapper());

        environment.jersey().register(new RuntimeExceptionMapper());
        environment.jersey().register(new ConnectLoggingExceptionMapper());


        LOG.info("App context " + appContext);


        // environment.getObjectMapper().registerModule()
        LOG.debug("in config");
        postRun(configuration, environment);

        LOG.info("****************STARTING APPLICATION ENVIRONMENT MODE: " + configuration.getEnv() + "*******************");
    }

    private void removeDefaultExceptionMappers(boolean deleteDefault, Environment environment) {
        if (deleteDefault) {
            ResourceConfig jrConfig = environment.jersey().getResourceConfig();
            Set<Object> dwSingletons = jrConfig.getSingletons();
            List<Object> singletonsToRemove = new ArrayList<Object>();

            for (Object singletons : dwSingletons) {
                if (singletons instanceof ExceptionMapper && !singletons.getClass().getName().contains("DropwizardResourceConfig")) {
                    singletonsToRemove.add(singletons);
                }
            }

            for (Object singletons : singletonsToRemove) {
                LOG.info("Deleting this ExceptionMapper: " + singletons.getClass().getName());
                jrConfig.getSingletons().remove(singletons);
            }
        }
    }

    protected void postRun(final ConnectConfiguration configuration, final Environment environment) throws Exception {
        // Sub-classes should
    }

    protected void postInitialize(Bootstrap<ConnectConfiguration> bootstrapm, GuiceBundle.Builder guiceBuilder) {
        // Sub-classes should
    }
}
