package com.fourtwenty.test;

import com.blaze.clients.hypur.configs.HypurConfig;
import com.blaze.clients.metrcs.MetrcAuthorization;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fourtwenty.clients.metrcs.TestMetrcAuthorizationImpl;
import com.fourtwenty.core.config.ConnectConfiguration;
import com.fourtwenty.core.domain.db.MongoDb;
import com.fourtwenty.core.domain.repositories.payment.CloverReceiptRepository;
import com.fourtwenty.core.domain.repositories.payment.impl.CloverReceiptRepositoryImpl;
import com.fourtwenty.core.lifecycle.AppStartup;
import com.fourtwenty.core.lifecycle.CoreAppStartup;
import com.fourtwenty.core.lifecycle.Migration;
import com.fourtwenty.core.lifecycle.TwilioStartup;
import com.fourtwenty.core.lifecycle.model.DefaultData;
import com.fourtwenty.core.managed.MongoManager;
import com.fourtwenty.core.security.tokens.ConnectAuthToken;
import com.fourtwenty.core.security.tokens.TerminalUser;
import com.fourtwenty.core.util.AmazonServiceFactory;
import com.fourtwenty.core.util.SecurityUtil;
import com.fourtwenty.integrations.clover.repository.CloverRepository;
import com.fourtwenty.integrations.clover.repository.impl.CloverRepositoryImpl;
import com.fourtwenty.integrations.clover.service.CloverService;
import com.fourtwenty.integrations.clover.service.impl.CloverServiceImpl;
import com.fourtwenty.integrations.mtrac.service.MtracService;
import com.fourtwenty.integrations.mtrac.service.impl.MtracServiceImpl;
import com.fourtwenty.migrations.ConnectMigration;
import com.fourtwenty.quickbook.online.quickbookservices.QuickbookService;
import com.fourtwenty.quickbook.online.quickbookservices.QuickbookSync;
import com.fourtwenty.quickbook.online.quickbookservices.impl.QuickbookServiceimpl;
import com.fourtwenty.quickbook.online.quickbookservices.impl.QuickbookSyncImpl;
import com.fourtwenty.quickbook.repositories.*;
import com.fourtwenty.quickbook.repositories.impl.*;
import com.google.common.collect.Lists;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.multibindings.Multibinder;
import com.warehouse.core.domain.repositories.invoice.InvoicePaymentsRepository;
import com.warehouse.core.domain.repositories.invoice.InvoiceRepository;
import com.warehouse.core.domain.repositories.invoice.impl.InvoicePaymentRepositoryImpl;
import com.warehouse.core.domain.repositories.invoice.impl.InvoiceRepositoryImpl;
import io.dropwizard.setup.Environment;
import net.greghaines.jesque.Config;
import net.greghaines.jesque.ConfigBuilder;
import net.greghaines.jesque.client.Client;
import net.greghaines.jesque.client.ClientImpl;
import org.glassfish.hk2.api.MultiException;
import org.glassfish.hk2.api.ServiceLocator;
import org.joda.time.DateTime;

import javax.ws.rs.container.ContainerRequestContext;
import java.io.InputStream;

/**
 * Created by mdo on 9/4/16.
 */
public class TestModule extends AbstractModule {
    static SecurityUtil securityUtil;
    static DefaultData defaultData;
    static AmazonServiceFactory s3Factory;
    static MongoManager mongoDBManager;
    static Client jesClient;


    @Override
    protected void configure() {
        bind(AppStartup.class).to(CoreAppStartup.class);
        bind(Migration.class).to(ConnectMigration.class);

        bind(InvoicePaymentsRepository.class).to(InvoicePaymentRepositoryImpl.class);
        bind(InvoiceRepository.class).to(InvoiceRepositoryImpl.class);
        bind(QuickbookAccountRepository.class).to(QuickbookAccountRepositoryImpl.class);
        bind(QuickBookAccountDetailsRepository.class).to(QuickBookAccountDetailsRepositoryImpl.class);
        bind(QuickBookSessionDataRepository.class).to(QuickBookSessionDataRepositoryImpl.class);
        bind(QuickbookSyncDetailsRepository.class).to(QuickbookSyncDetailsRepositoryImpl.class);
        bind(QuickbookCustomEntitiesRepository.class).to(QuickbookCustomEntitiesRepositoryImpl.class);
        bind(QuickbookAccountRepository.class).to(QuickbookAccountRepositoryImpl.class);
        bind(QuickbookEntityRepository.class).to(QuickbookEntityRepositoryImpl.class);
        bind(QuickbookDesktopAccountRepository.class).to(QuickbookDesktopAccountRepositoryImpl.class);
        bind(QbDesktopOperationRepository.class).to(QbDesktopOperationRepositoryImpl.class);
        bind(QbAccountRepository.class).to(QbDesktopAccountRepositoryImpl.class);
        bind(ErrorLogsRepository.class).to(ErrorLogsRepositoryImpl.class);
        bind(QbDesktopCurrentSyncRepository.class).to(QbDesktopCurrentRepositoryImpl.class);
        bind(QBDesktopSyncReferenceRepository.class).to(QBDesktopSyncReferenceRepositoryImpl.class);
        bind(QBUniqueSequenceRepository.class).to(QBUniqueSequenceRepositoryImpl.class);
        bind(QuickbookService.class).to(QuickbookServiceimpl.class);
        bind(QuickbookSync.class).to(QuickbookSyncImpl.class);
        bind(CloverService.class).to(CloverServiceImpl.class);
        bind(CloverRepository.class).to(CloverRepositoryImpl.class);
        bind(CloverReceiptRepository.class).to(CloverReceiptRepositoryImpl.class);
        bind(MtracService.class).to(MtracServiceImpl.class);



        Multibinder<AppStartup> mb = Multibinder.newSetBinder(binder(), AppStartup.class);
        mb.addBinding().to(CoreAppStartup.class);
        mb.addBinding().to(TwilioStartup.class);
        //mb.addBinding().to(BlazeSchedulerAppStartup.class);
        bind(MetrcAuthorization.class).to(TestMetrcAuthorizationImpl.class);
    }

    @Provides
    public ConnectAuthToken getAuthToken() {
        ConnectAuthToken testToken = new ConnectAuthToken();
        testToken.setCompanyId("56ce8bf389288da6df3a6602");
        testToken.setShopId("56cf846ee38179985229e59e");
        testToken.setManagerId("56cfa9c8e38179985229e5a4");
        testToken.setTerminalId("58d4962c8bc4bc203f05fc9a");
        TerminalUser terminalUser = new TerminalUser("56cfa9c8e38179985229e5a4", "test", "test");
        testToken.setActiveUsers(Lists.newArrayList(terminalUser));
        testToken.setInitDate(DateTime.now().getMillis());

        return testToken;
    }

    @Provides
    private synchronized Client getJedisClient(ConnectConfiguration connectConfig) {
        if (jesClient == null) {
            int port = 6379;
            try {
                port = Integer.valueOf(connectConfig.getRedisPort());
            } catch (Exception e) {
                // ignore
            }
            Config config = new ConfigBuilder().withHost(connectConfig.getRedisHost()).withPort(port).withDatabase(0).build();
            jesClient = new ClientImpl(config);
        }
        return jesClient;
    }

    @Provides
    private synchronized SecurityUtil getSecurityUtil(ConnectConfiguration config) {
        if (securityUtil == null) {
            securityUtil = new SecurityUtil(config);
        }
        return securityUtil;
    }

    @Provides
    private synchronized AmazonServiceFactory getAmazonFactory(ConnectConfiguration config) {
        if (s3Factory == null) {
            s3Factory = new AmazonServiceFactory(config);
        }
        return s3Factory;
    }

    private static ContainerRequestContext getContainerRequestContext(ServiceLocator serviceLocator) {
        try {
            return serviceLocator.getService(ContainerRequestContext.class);
        } catch (MultiException e) {
            if (e.getCause() instanceof IllegalStateException) {
                return null;
            } else {
                throw new ExceptionInInitializerError(e);
            }
        }
    }

    @Provides
    public synchronized DefaultData getDefaultData(Environment environment) {
        if (defaultData == null) {

            final ObjectMapper objectMapper = environment.getObjectMapper();
            InputStream stream = TestModule.class.getResourceAsStream("/defaultdata.json");

            try {
                defaultData = objectMapper.readValue(stream, DefaultData.class);
            } catch (Exception e) {
                System.out.println(e.getMessage());
                return new DefaultData();
            }
        }
        return defaultData;
    }


    @Provides
    private synchronized MongoDb getMongoManager(ConnectConfiguration config) {
        if (mongoDBManager == null) {
            mongoDBManager = new MongoManager(config);
        }
        return mongoDBManager;
    }

    @Provides
    public HypurConfig getHypurConfig(ConnectConfiguration configuration) {
        HypurConfig hypurConfig = configuration.getHypurConfig();
        return hypurConfig;
    }
}
