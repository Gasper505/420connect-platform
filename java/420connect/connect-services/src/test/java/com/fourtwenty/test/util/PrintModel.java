package com.fourtwenty.test.util;

import com.google.common.collect.ImmutableSet;
import com.google.common.reflect.ClassPath;
import org.junit.Test;

import java.io.IOException;

/**
 * Created by mdo on 9/3/15.
 */
public class PrintModel {
    @Test
    public void printModelClasses() {
        final ClassLoader loader = Thread.currentThread().getContextClassLoader();

        try {
            ClassPath classPath = ClassPath.from(loader);
            ImmutableSet<ClassPath.ClassInfo> classSet = classPath.getTopLevelClassesRecursive("com.fourtwenty.core.domain.models");
            for (ClassPath.ClassInfo classInfo : classSet) {
                System.out.println("<class>" + classInfo.getName() + "</class>");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
