package com.blaze.clients.hypur;

import com.blaze.clients.hypur.models.HypurInvoice;
import com.blaze.clients.hypur.models.HypurInvoiceList;
import com.blaze.clients.hypur.models.InvoiceResponse;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by Gaurav Saini on 8/7/17.
 */
public interface HypurInvoiceAPIService {

    String getInvoiceErrorCodes();

    HypurInvoiceList addInvoices(List<HypurInvoice> hypurInvoice, String merchantToken) throws InterruptedException, ExecutionException, IOException;

    HypurInvoice getInvoiceDetails(String merchantToken, String invoiceNumber);

    HypurInvoiceList getInvoices(String merchantToken, String pageNumber, String itemsPerPage);

    InvoiceResponse addInvoice(HypurInvoice hypurInvoice, String merchantToken) throws InterruptedException, ExecutionException, IOException;

    InvoiceResponse updateInvoice(HypurInvoice hypurInvoice, String merchantToken) throws InterruptedException, ExecutionException, IOException;

}
