package com.blaze.clients.hypur;

import com.blaze.clients.hypur.impl.HypurInvoiceAPIServiceImpl;
import com.blaze.clients.hypur.impl.HypurPaymentAPIServiceImpl;
import com.google.inject.AbstractModule;

/**
 * Created by mdo on 8/21/17.
 */
public class HypurModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(HypurInvoiceAPIService.class).to(HypurInvoiceAPIServiceImpl.class);
        bind(HypurPaymentAPIService.class).to(HypurPaymentAPIServiceImpl.class);
    }
}
