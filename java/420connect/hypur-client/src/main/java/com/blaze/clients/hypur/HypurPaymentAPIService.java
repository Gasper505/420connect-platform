package com.blaze.clients.hypur;

import com.blaze.clients.hypur.models.CheckInTestUsersResponse;
import com.blaze.clients.hypur.models.CheckedInUserListResponse;
import com.blaze.clients.hypur.models.HypurPayment;
import com.blaze.clients.hypur.models.HypurRefund;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

/**
 * Created by Gaurav Saini on 8/7/17.
 */
public interface HypurPaymentAPIService {

    CheckedInUserListResponse getCheckedInUsers(String merchantToken, int thumnailSize);

    CheckInTestUsersResponse genCheckedInTestUsers(String merchantToken);

    CheckInTestUsersResponse processPayment(HypurPayment hypurPayment, String merchantToken) throws InterruptedException, ExecutionException, IOException;

    CheckInTestUsersResponse refundPayment(HypurRefund refund, String merchantToken) throws InterruptedException, ExecutionException, IOException;

}
