package com.blaze.clients.hypur.configs;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by mdo on 8/23/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class HypurConfig {
    private String host = "https://tst-api.hypur.com";
    private String apiKey = "023AA5FD-3F6E-4606-BEE4-FF623ECA297F"; // Blaze APIs Test Token

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getPaymentHost() {
        return String.format("%s/%s", host, "Payments");
    }

    public String getInvoiceHost() {
        return String.format("%s/%s", host, "InvoiceCollector");
    }
}
