package com.blaze.clients.hypur.impl;

import com.blaze.clients.hypur.HypurInvoiceAPIService;
import com.blaze.clients.hypur.configs.HypurConfig;
import com.blaze.clients.hypur.models.CheckedInUserListResponse;
import com.blaze.clients.hypur.models.HypurInvoice;
import com.blaze.clients.hypur.models.HypurInvoiceList;
import com.blaze.clients.hypur.models.InvoiceResponse;
import com.google.inject.Inject;
import com.mdo.pusher.SimpleRestUtil;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class HypurInvoiceAPIServiceImpl implements HypurInvoiceAPIService {

    @Inject
    private HypurConfig config;
    public static final String host = "https://tst-api.hypur.com/InvoiceCollector/";

    @Override
    public String getInvoiceErrorCodes() {
        String url = host + "ErrorCodes";
        return SimpleRestUtil.get(url, String.class, setHeaders());
    }

    @Override
    public HypurInvoiceList addInvoices(List<HypurInvoice> hypurInvoice, String merchantToken) throws InterruptedException, ExecutionException, IOException {

        String url = buildURL("/Invoice/Batch", merchantToken, null);
        return SimpleRestUtil.post(url, hypurInvoice, HypurInvoiceList.class, CheckedInUserListResponse.class, setHeaders());
    }

    @Override
    public HypurInvoice getInvoiceDetails(String merchantToken, String invoiceNumber) {

        LinkedHashMap<String, String> params = new LinkedHashMap<>();
        params.put("InvoiceNumber", invoiceNumber);

        String url = buildURL("/Invoice", merchantToken, params);

        return SimpleRestUtil.get(url, HypurInvoice.class, setHeaders());
    }

    @Override
    public HypurInvoiceList getInvoices(String merchantToken, String pageNumber, String itemsPerPage) {
        LinkedHashMap<String, String> params = new LinkedHashMap<>();
        params.put("PageNumber", pageNumber);
        params.put("ItemsPerPage", itemsPerPage);

        String url = buildURL("/Invoice", merchantToken, params);

        return SimpleRestUtil.get(url, HypurInvoiceList.class, setHeaders());
    }

    @Override
    public InvoiceResponse addInvoice(HypurInvoice hypurInvoice, String merchantToken) throws InterruptedException, ExecutionException, IOException {
        String url = buildURL("/Invoice", merchantToken, null);
        return SimpleRestUtil.post(url, hypurInvoice, InvoiceResponse.class, CheckedInUserListResponse.class, setHeaders());
    }

    @Override
    public InvoiceResponse updateInvoice(HypurInvoice hypurInvoice, String merchantToken) throws InterruptedException, ExecutionException, IOException {
        String url = buildURL("/Invoice", merchantToken, null);
        return SimpleRestUtil.put(url, hypurInvoice, InvoiceResponse.class, setHeaders());
    }

    private MultivaluedMap<String, Object> setHeaders() {
        MultivaluedMap<String, Object> headers = new MultivaluedHashMap<>();
        headers.putSingle("Content-Type", MediaType.APPLICATION_JSON);
        return headers;
    }

    private String buildURL(String path,
                            String merchantToken,
                            LinkedHashMap<String, String> params) {
        StringBuilder url = new StringBuilder(config.getInvoiceHost());
        url.append(String.format("%s/?PosToken=%s&MerchantToken=%s",
                path,
                config.getApiKey(),
                merchantToken));
        if (params != null) {
            for (String key : params.keySet()) {
                url.append(String.format("&%s=%s", key, params.get(key)));
            }
        }
        return url.toString();
    }
}
