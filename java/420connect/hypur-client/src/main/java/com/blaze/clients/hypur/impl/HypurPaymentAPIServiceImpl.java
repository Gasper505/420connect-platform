package com.blaze.clients.hypur.impl;

import com.blaze.clients.hypur.HypurPaymentAPIService;
import com.blaze.clients.hypur.configs.HypurConfig;
import com.blaze.clients.hypur.models.CheckInTestUsersResponse;
import com.blaze.clients.hypur.models.CheckedInUserListResponse;
import com.blaze.clients.hypur.models.HypurPayment;
import com.blaze.clients.hypur.models.HypurRefund;
import com.google.inject.Inject;
import com.mdo.pusher.SimpleRestUtil;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.concurrent.ExecutionException;

/**
 * Created by Gaurav Saini on 8/7/17.
 */
public class HypurPaymentAPIServiceImpl implements HypurPaymentAPIService {

    @Inject
    private HypurConfig config;


    @Override
    public CheckedInUserListResponse getCheckedInUsers(String merchantToken, int thumnailSize) {
        LinkedHashMap<String, String> params = new LinkedHashMap<>();
        params.put("ThumbnailSize", thumnailSize + "");

        String url = buildURL("/Payment/GetCheckedInUsers", merchantToken, params);
        CheckedInUserListResponse result = SimpleRestUtil.get(url, CheckedInUserListResponse.class, setHeaders());

        return result;
    }

    @Override
    public CheckInTestUsersResponse genCheckedInTestUsers(String merchantToken) {
        String url = buildURL("/Payment/CheckInTestUsers", merchantToken, null);
        return SimpleRestUtil.get(url, CheckInTestUsersResponse.class, setHeaders());
    }

    @Override
    public CheckInTestUsersResponse processPayment(HypurPayment hypurPayment, String merchantToken) throws InterruptedException, ExecutionException, IOException {
        String url = buildURL("/Payment", merchantToken, null);
        return SimpleRestUtil.post(url, hypurPayment, CheckInTestUsersResponse.class, CheckedInUserListResponse.class, setHeaders());
    }

    @Override
    public CheckInTestUsersResponse refundPayment(HypurRefund refund, String merchantToken) throws InterruptedException, ExecutionException, IOException {
        String url = buildURL("/Refund", merchantToken, null);
        return SimpleRestUtil.post(url, refund, CheckInTestUsersResponse.class, CheckedInUserListResponse.class, setHeaders());
    }

    private MultivaluedMap<String, Object> setHeaders() {
        MultivaluedMap<String, Object> headers = new MultivaluedHashMap<>();
        headers.putSingle("Content-Type", MediaType.APPLICATION_JSON);
        return headers;
    }

    private String buildURL(String path,
                            String merchantToken,
                            LinkedHashMap<String, String> params) {
        StringBuilder url = new StringBuilder(config.getPaymentHost());
        url.append(String.format("%s/?ApiToken=%s&MerchantToken=%s", path, config.getApiKey(), merchantToken));
        if (params != null) {
            for (String key : params.keySet()) {
                url.append(String.format("&%s=%s", key, params.get(key)));
            }
        }
        return url.toString();
    }
}
