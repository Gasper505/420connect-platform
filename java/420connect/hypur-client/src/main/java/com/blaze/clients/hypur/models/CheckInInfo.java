package com.blaze.clients.hypur.models;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Date;

/**
 * Created by mdo on 8/6/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY,
        getterVisibility = JsonAutoDetect.Visibility.NONE,
        setterVisibility = JsonAutoDetect.Visibility.NONE)
public class CheckInInfo {

    private int CheckInISN;
    private int IndividualISN;
    private int BusinessISN;
    private String BusinessName;
    private double Longitude;
    private double Latitude;
    private Date CheckInDate;
    private String IndividualName;
    private String ConsumerType;
    private boolean IsMember;
    private boolean HasId;
    private int NumberOfBankAccounts;
    private Date BirthDate;
    private Date IdExpirationDate;
    private String IdState;
    private String IndividualBase64Thumbnail;
    private boolean CanCheckOut;

    public int getCheckInISN() {
        return CheckInISN;
    }

    public void setCheckInISN(int checkInISN) {
        CheckInISN = checkInISN;
    }

    public int getIndividualISN() {
        return IndividualISN;
    }

    public void setIndividualISN(int individualISN) {
        IndividualISN = individualISN;
    }

    public int getBusinessISN() {
        return BusinessISN;
    }

    public void setBusinessISN(int businessISN) {
        BusinessISN = businessISN;
    }

    public String getBusinessName() {
        return BusinessName;
    }

    public void setBusinessName(String businessName) {
        BusinessName = businessName;
    }

    public double getLongitude() {
        return Longitude;
    }

    public void setLongitude(double longitude) {
        Longitude = longitude;
    }

    public double getLatitude() {
        return Latitude;
    }

    public void setLatitude(double latitude) {
        Latitude = latitude;
    }

    public Date getCheckInDate() {
        return CheckInDate;
    }

    public void setCheckInDate(Date checkInDate) {
        CheckInDate = checkInDate;
    }

    public String getIndividualName() {
        return IndividualName;
    }

    public void setIndividualName(String individualName) {
        IndividualName = individualName;
    }

    public String getConsumerType() {
        return ConsumerType;
    }

    public void setConsumerType(String consumerType) {
        ConsumerType = consumerType;
    }

    public boolean isMember() {
        return IsMember;
    }

    public void setMember(boolean member) {
        IsMember = member;
    }

    public boolean isHasId() {
        return HasId;
    }

    public void setHasId(boolean hasId) {
        HasId = hasId;
    }

    public int getNumberOfBankAccounts() {
        return NumberOfBankAccounts;
    }

    public void setNumberOfBankAccounts(int numberOfBankAccounts) {
        NumberOfBankAccounts = numberOfBankAccounts;
    }

    public Date getBirthDate() {
        return BirthDate;
    }

    public void setBirthDate(Date birthDate) {
        BirthDate = birthDate;
    }

    public Date getIdExpirationDate() {
        return IdExpirationDate;
    }

    public void setIdExpirationDate(Date idExpirationDate) {
        IdExpirationDate = idExpirationDate;
    }

    public String getIdState() {
        return IdState;
    }

    public void setIdState(String idState) {
        IdState = idState;
    }

    public String getIndividualBase64Thumbnail() {
        return IndividualBase64Thumbnail;
    }

    public void setIndividualBase64Thumbnail(String individualBase64Thumbnail) {
        IndividualBase64Thumbnail = individualBase64Thumbnail;
    }

    public boolean isCanCheckOut() {
        return CanCheckOut;
    }

    public void setCanCheckOut(boolean canCheckOut) {
        CanCheckOut = canCheckOut;
    }
}
