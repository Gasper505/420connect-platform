package com.blaze.clients.hypur.models;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

/**
 * Created by Gaurav Saini on 22/8/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY,
        getterVisibility = JsonAutoDetect.Visibility.NONE,
        setterVisibility = JsonAutoDetect.Visibility.NONE)
public class CheckInTestUsersResponse {

    private boolean Item;
    private List<Integer> ErrorCodes;

    public boolean isItem() {
        return Item;
    }

    public void setItem(boolean item) {
        Item = item;
    }

    public List<Integer> getErrorCodes() {
        return ErrorCodes;
    }

    public void setErrorCodes(List<Integer> errorCodes) {
        ErrorCodes = errorCodes;
    }
}
