package com.blaze.clients.hypur.models;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mdo on 8/6/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY,
        getterVisibility = JsonAutoDetect.Visibility.NONE,
        setterVisibility = JsonAutoDetect.Visibility.NONE)
public class CheckedInUserListResponse {
    private List<CheckInInfo> Items = new ArrayList<>();
    private List<CheckInInfo> Item = new ArrayList<>();
    private List<Integer> ErrorCodes = new ArrayList<>();

    public List<Integer> getErrorCodes() {
        return ErrorCodes;
    }

    public void setErrorCodes(List<Integer> errorCodes) {
        ErrorCodes = errorCodes;
    }

    public List<CheckInInfo> getItem() {
        return Item;
    }

    public void setItem(List<CheckInInfo> item) {
        Item = item;
    }

    public List<CheckInInfo> getItems() {
        return Items;
    }

    public void setItems(List<CheckInInfo> items) {
        Items = items;
    }
}
