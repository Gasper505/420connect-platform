package com.blaze.clients.hypur.models;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.joda.time.DateTime;

import java.util.Collection;

/**
 * Created by Gaurav Saini on 8/7/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY,
        getterVisibility = JsonAutoDetect.Visibility.NONE,
        setterVisibility = JsonAutoDetect.Visibility.NONE)
public class HypurInvoice {

    private String InvoiceNumber;

    public enum InvoiceType {
        None(0),
        MrbMedical(1),
        MrbRetail(2),
        Retail(3);

        private int type;

        InvoiceType(int type) {
            this.type = type;
        }
    }

    public enum Status {
        Paid(1),
        Returned(2),
        Unpaid(3),
        Void(4);

        private int status;

        Status(int status) {
            this.status = status;
        }
    }

    private DateTime InvoiceDate;
    private DateTime DueDate;
    private DateTime PaidDate;
    private String CurrencyCode;
    private double SubTotal;
    private double Total;
    private double DiscountPercent;
    private double DiscountAmount;
    private boolean IsTaxCalculatedAfterDiscount;
    private boolean TaxInclusive;
    private double InvoiceTax;
    private double Cash;
    private double Change;
    private int PosId;
    private String PosName;
    private String TerminalId;
    private String CreatedBy;
    private String Note;
    private String MerchantMemo;
    private Collection<Byte> InvoiceImage;
    private Collection<InvoiceItemDTO> InvoiceItems;
    private Collection<InvoicePaymentDTO> InvoicePayments;

    public String getInvoiceNumber() {
        return InvoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        InvoiceNumber = invoiceNumber;
    }

    public DateTime getInvoiceDate() {
        return InvoiceDate;
    }

    public void setInvoiceDate(DateTime invoiceDate) {
        InvoiceDate = invoiceDate;
    }

    public DateTime getDueDate() {
        return DueDate;
    }

    public void setDueDate(DateTime dueDate) {
        DueDate = dueDate;
    }

    public DateTime getPaidDate() {
        return PaidDate;
    }

    public void setPaidDate(DateTime paidDate) {
        PaidDate = paidDate;
    }

    public String getCurrencyCode() {
        return CurrencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        CurrencyCode = currencyCode;
    }

    public double getSubTotal() {
        return SubTotal;
    }

    public void setSubTotal(double subTotal) {
        SubTotal = subTotal;
    }

    public double getTotal() {
        return Total;
    }

    public void setTotal(double total) {
        Total = total;
    }

    public double getDiscountPercent() {
        return DiscountPercent;
    }

    public void setDiscountPercent(double discountPercent) {
        DiscountPercent = discountPercent;
    }

    public double getDiscountAmount() {
        return DiscountAmount;
    }

    public void setDiscountAmount(double discountAmount) {
        DiscountAmount = discountAmount;
    }

    public boolean isTaxCalculatedAfterDiscount() {
        return IsTaxCalculatedAfterDiscount;
    }

    public void setTaxCalculatedAfterDiscount(boolean taxCalculatedAfterDiscount) {
        IsTaxCalculatedAfterDiscount = taxCalculatedAfterDiscount;
    }

    public boolean isTaxInclusive() {
        return TaxInclusive;
    }

    public void setTaxInclusive(boolean taxInclusive) {
        TaxInclusive = taxInclusive;
    }

    public double getInvoiceTax() {
        return InvoiceTax;
    }

    public void setInvoiceTax(double invoiceTax) {
        InvoiceTax = invoiceTax;
    }

    public double getCash() {
        return Cash;
    }

    public void setCash(double cash) {
        Cash = cash;
    }

    public double getChange() {
        return Change;
    }

    public void setChange(double change) {
        Change = change;
    }

    public int getPosId() {
        return PosId;
    }

    public void setPosId(int posId) {
        PosId = posId;
    }

    public String getPosName() {
        return PosName;
    }

    public void setPosName(String posName) {
        PosName = posName;
    }

    public String getTerminalId() {
        return TerminalId;
    }

    public void setTerminalId(String terminalId) {
        TerminalId = terminalId;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getNote() {
        return Note;
    }

    public void setNote(String note) {
        Note = note;
    }

    public String getMerchantMemo() {
        return MerchantMemo;
    }

    public void setMerchantMemo(String merchantMemo) {
        MerchantMemo = merchantMemo;
    }

    public Collection<Byte> getInvoiceImage() {
        return InvoiceImage;
    }

    public void setInvoiceImage(Collection<Byte> invoiceImage) {
        InvoiceImage = invoiceImage;
    }

    public Collection<InvoiceItemDTO> getInvoiceItems() {
        return InvoiceItems;
    }

    public void setInvoiceItems(Collection<InvoiceItemDTO> invoiceItems) {
        InvoiceItems = invoiceItems;
    }

    public Collection<InvoicePaymentDTO> getInvoicePayments() {
        return InvoicePayments;
    }

    public void setInvoicePayments(Collection<InvoicePaymentDTO> invoicePayments) {
        InvoicePayments = invoicePayments;
    }
}
