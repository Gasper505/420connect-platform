package com.blaze.clients.hypur.models;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

/**
 * Created by Gaurav Saini on 22/8/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY,
        getterVisibility = JsonAutoDetect.Visibility.NONE,
        setterVisibility = JsonAutoDetect.Visibility.NONE)
public class HypurInvoiceList {

    private String TotalPageCount;
    private String TotalRecordCount;
    private List<HypurInvoice> Items;
    private List<String> ErrorInvoiceNumbers;
    private List<Integer> ErrorCodes;

    public String getTotalPageCount() {
        return TotalPageCount;
    }

    public void setTotalPageCount(String totalPageCount) {
        TotalPageCount = totalPageCount;
    }

    public String getTotalRecordCount() {
        return TotalRecordCount;
    }

    public void setTotalRecordCount(String totalRecordCount) {
        TotalRecordCount = totalRecordCount;
    }

    public List<HypurInvoice> getItems() {
        return Items;
    }

    public void setItems(List<HypurInvoice> items) {
        Items = items;
    }

    public List<String> getErrorInvoiceNumbers() {
        return ErrorInvoiceNumbers;
    }

    public void setErrorInvoiceNumbers(List<String> errorInvoiceNumbers) {
        ErrorInvoiceNumbers = errorInvoiceNumbers;
    }

    public List<Integer> getErrorCodes() {
        return ErrorCodes;
    }

    public void setErrorCodes(List<Integer> errorCodes) {
        ErrorCodes = errorCodes;
    }
}
