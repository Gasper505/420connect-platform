package com.blaze.clients.hypur.models;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Gaurav Saini on 8/7/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY,
        getterVisibility = JsonAutoDetect.Visibility.NONE,
        setterVisibility = JsonAutoDetect.Visibility.NONE)
public class HypurPayment {

    private int EmployeeId;
    private int MerchantId;
    private int CheckInISN;
    private int FromEntityISN;
    private String PAC;
    private double Amount;
    private String Note;
    private String DeviceOS;
    private int TransactionTypeId;
    private boolean IsHypurPayment = true;


    public int getEmployeeId() {
        return EmployeeId;
    }

    public void setEmployeeId(int employeeId) {
        EmployeeId = employeeId;
    }

    public int getMerchantId() {
        return MerchantId;
    }

    public void setMerchantId(int merchantId) {
        MerchantId = merchantId;
    }

    public int getCheckInISN() {
        return CheckInISN;
    }

    public void setCheckInISN(int checkInISN) {
        CheckInISN = checkInISN;
    }

    public int getFromEntityISN() {
        return FromEntityISN;
    }

    public void setFromEntityISN(int fromEntityISN) {
        FromEntityISN = fromEntityISN;
    }

    public String getPAC() {
        return PAC;
    }

    public void setPAC(String PAC) {
        this.PAC = PAC;
    }

    public double getAmount() {
        return Amount;
    }

    public void setAmount(double amount) {
        Amount = amount;
    }

    public String getNote() {
        return Note;
    }

    public void setNote(String note) {
        Note = note;
    }

    public String getDeviceOS() {
        return DeviceOS;
    }

    public void setDeviceOS(String deviceOS) {
        DeviceOS = deviceOS;
    }

    public int getTransactionTypeId() {
        return TransactionTypeId;
    }

    public void setTransactionTypeId(int transactionTypeId) {
        TransactionTypeId = transactionTypeId;
    }

    public boolean isHypurPayment() {
        return IsHypurPayment;
    }

    public void setHypurPayment(boolean hypurPayment) {
        IsHypurPayment = hypurPayment;
    }
}
