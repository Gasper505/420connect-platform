package com.blaze.clients.hypur.models;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.joda.time.DateTime;

/**
 * Created by Gaurav Saini on 8/7/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY,
        getterVisibility = JsonAutoDetect.Visibility.NONE,
        setterVisibility = JsonAutoDetect.Visibility.NONE)
public class InvoicePaymentDTO {

    private int InvoiceId;
    private double Amount;

    public enum PaymentType {
        Cash(1),
        Hypur(2),
        Other(3);

        private int paymentType;

        PaymentType(int paymentType) {
            this.paymentType = paymentType;
        }
    }

    private DateTime PaymentDate;
    private int TransactionId;

    public int getInvoiceId() {
        return InvoiceId;
    }

    public void setInvoiceId(int invoiceId) {
        InvoiceId = invoiceId;
    }

    public double getAmount() {
        return Amount;
    }

    public void setAmount(double amount) {
        Amount = amount;
    }

    public DateTime getPaymentDate() {
        return PaymentDate;
    }

    public void setPaymentDate(DateTime paymentDate) {
        PaymentDate = paymentDate;
    }

    public int getTransactionId() {
        return TransactionId;
    }

    public void setTransactionId(int transactionId) {
        TransactionId = transactionId;
    }
}
