package com.blaze.clients.hypur.models;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

/**
 * Created by Gaurav Saini on 22/8/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY,
        getterVisibility = JsonAutoDetect.Visibility.NONE,
        setterVisibility = JsonAutoDetect.Visibility.NONE)
public class InvoiceResponse {

    private InvoiceItemDTO invoiceItemDTO;
    private List<Integer> ErrorCodes;

    public InvoiceItemDTO getInvoiceItemDTO() {
        return invoiceItemDTO;
    }

    public void setInvoiceItemDTO(InvoiceItemDTO invoiceItemDTO) {
        this.invoiceItemDTO = invoiceItemDTO;
    }

    public List<Integer> getErrorCodes() {
        return ErrorCodes;
    }

    public void setErrorCodes(List<Integer> errorCodes) {
        ErrorCodes = errorCodes;
    }
}
