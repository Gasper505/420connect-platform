package com.mdo.pusher;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.HashMap;

/**
 * The PChannelListResult object.
 * User: mdo
 * Date: 12/31/13
 * Time: 9:30 PM
 * <p>
 * {
 * "channels": {
 * "presence-foobar": {
 * user_count: 42
 * },
 * "presence-another": {
 * user_count: 123
 * }
 * }
 * }
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PChannelListResult {
    private HashMap<String, PChannelResult> channels;

    public HashMap<String, PChannelResult> getChannels() {
        return channels;
    }

    public void setChannels(HashMap<String, PChannelResult> channels) {
        this.channels = channels;
    }
}
