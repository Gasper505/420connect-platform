package com.mdo.pusher;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The PChannelResult object.
 * User: mdo
 * Date: 12/31/13
 * Time: 10:30 PM
 */
/*
{
  occupied: true,
  user_count: 42,
  subscription_count: 42
}
 */
public class PChannelResult {
    private boolean occupied;
    @JsonProperty("user_count")
    private int userCount;
    @JsonProperty("subscription_count")
    private int subscriptionCount;

    public boolean isOccupied() {
        return occupied;
    }

    public void setOccupied(boolean occupied) {
        this.occupied = occupied;
    }

    public int getUserCount() {
        return userCount;
    }

    public void setUserCount(int userCount) {
        this.userCount = userCount;
    }

    public int getSubscriptionCount() {
        return subscriptionCount;
    }

    public void setSubscriptionCount(int subscriptionCount) {
        this.subscriptionCount = subscriptionCount;
    }
}
