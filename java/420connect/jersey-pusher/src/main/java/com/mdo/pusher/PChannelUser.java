package com.mdo.pusher;

/**
 * The PChannelUser object.
 * User: mdo
 * Date: 12/31/13
 * Time: 10:39 PM
 */
public class PChannelUser {
    private long id;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
