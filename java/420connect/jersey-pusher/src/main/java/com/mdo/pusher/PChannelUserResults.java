package com.mdo.pusher;

import java.util.List;

/**
 * The PChannelUserResults object.
 * User: mdo
 * Date: 12/31/13
 * Time: 10:40 PM
 */
public class PChannelUserResults {
    private List<PChannelUser> users;

    public List<PChannelUser> getUsers() {
        return users;
    }

    public void setUsers(List<PChannelUser> users) {
        this.users = users;
    }
}
