package com.mdo.pusher;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * The PEventRequest object.
 * User: mdo
 * Date: 12/31/13
 * Time: 7:41 PM
 */
public class PEventRequest {
    private String name;
    private String data;
    private List<String> channels;
    @JsonProperty("socket_id")
    private String socketId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public List<String> getChannels() {
        return channels;
    }

    public void setChannels(List<String> channels) {
        this.channels = channels;
    }


    public String getSocketId() {
        return socketId;
    }

    public void setSocketId(String socketId) {
        this.socketId = socketId;
    }
}
