package com.mdo.pusher;

import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import org.apache.commons.codec.binary.Hex;
import org.joda.time.DateTime;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import java.util.List;

/**
 * The Pusher object.
 * User: mdo
 * Date: 12/31/13
 * Time: 4:27 PM
 */
public class Pusher {
    private static ObjectMapper mapper = new ObjectMapper();
    private static final String PUSHER_API_HOST = "https://api.pusherapp.com";
    private static final String AUTH_VERSION = "1.0";

    private final String appId;
    private final String authKey;
    private final String authSecret;

    public Pusher(String appId, String authKey, String authSecret) {
        this.appId = appId;
        this.authKey = authKey;
        this.authSecret = authSecret;
    }

    public void registerSerializer(Module module) {
        mapper.registerModule(module);
    }

    public String getAppId() {
        return appId;
    }

    public String getAuthKey() {
        return authKey;
    }

    public String getAuthSecret() {
        return authSecret;
    }

    //POST /apps/[app_id]/events
    public void trigger(String channel, String event, Object data) {
        trigger(Lists.newArrayList(channel), event, data);
    }

    public void trigger(List<String> channels, String event, Object data) {
        PEventRequest request = new PEventRequest();
        request.setName(event);
        request.setChannels(channels);
        try {
            String encodedData = mapper.writeValueAsString(data);
            request.setData(encodedData);

            String s = mapper.writeValueAsString(request);
            String md5Body = Pusher.generateMD5Hash(s);

            String path = String.format("/apps/%s/events", appId);

			String url = createAuthUrl("POST", path, md5Body, null);
			MultivaluedMap<String, Object> headers = new MultivaluedHashMap<>();
			headers.putSingle("Content-Type", MediaType.APPLICATION_JSON);
			String result = SimpleRestUtil.post(url, request, String.class, headers);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}


    //GET /apps/[app_id]/channels
    public PChannelListResult getChannels(String filter, String attributes) {
        try {
            if (filter == null) {
                filter = "";
            }
            if (attributes == null) {
                attributes = "";
            }
            String path = String.format("/apps/%s/channels", appId);

            String parameters = String.format("&filter_by_prefix=%s&info=%s", filter, attributes);
            String url = createAuthUrl("GET", path, null, parameters);
            MultivaluedMap<String, Object> headers = new MultivaluedHashMap<>();
            headers.putSingle("Content-Type", MediaType.APPLICATION_JSON);
            String result = SimpleRestUtil.get(url, String.class, headers);
            return mapper.readValue(result, PChannelListResult.class);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    //GET /apps/[app_id]/channels/[channel_name]
    public PChannelResult getChannelData(String channel, String attributes) {
        try {
            if (channel == null) return null;
            String path = String.format("/apps/%s/channels/%s", appId, channel);

            String parameters = String.format("&info=%s", attributes);
            String url = createAuthUrl("GET", path, null, parameters);
            String result = SimpleRestUtil.get(url, String.class);
            return mapper.readValue(result, PChannelResult.class);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    //GET /apps/[app_id]/channels/[channel_name]/users
    public PChannelUserResults getChannelUsers(String channel) {
        try {
            if (channel == null) return null;
            String path = String.format("/apps/%s/channels/%s/users", appId, channel);

            String url = createAuthUrl("GET", path, null, null);
            String result = SimpleRestUtil.get(url, String.class);
            return mapper.readValue(result, PChannelUserResults.class);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private String createAuthUrl(String method, String path, String md5Body, String extraParamters) {
        long timeStamp = DateTime.now().getMillis() / 1000;
        String parameters = null;

        if ("POST".equals(method)) {
            parameters = String.format("auth_key=%s&auth_timestamp=%d&auth_version=%s&body_md5=%s",
                    authKey,
                    timeStamp,
                    AUTH_VERSION,
                    md5Body);
        } else {
            parameters = String.format("auth_key=%s&auth_timestamp=%d&auth_version=%s%s",
                    authKey,
                    timeStamp,
                    AUTH_VERSION,
                    extraParamters);
        }

        String pathToSign = String.format("%s\n%s\n%s",
                method,
                path,
                parameters);

        String signature = Pusher.signPhrase(authSecret, pathToSign);

        String url = String.format("%s%s?%s&auth_signature=%s", PUSHER_API_HOST, path, parameters, signature);
        return url;
    }

    public static String signPhrase(String key, String phrase) {
        try {
            Mac hmacSHA256 = Mac.getInstance("HmacSHA256");
            SecretKeySpec secretKeySpec = new SecretKeySpec(key.getBytes(), "HmacSHA256");
            hmacSHA256.init(secretKeySpec);

            return Hex.encodeHexString(hmacSHA256.doFinal(phrase.getBytes()));
        } catch (Exception e) {
        }
        return null;
    }

    public static String generateMD5Hash(String string) {
        try {
            java.security.MessageDigest md5 = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md5.digest(string.getBytes());
            return Hex.encodeHexString(array);
        } catch (java.security.NoSuchAlgorithmException e) {
        }
        return null;
    }
}
