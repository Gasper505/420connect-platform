package com.mdo.pusher;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import javax.ws.rs.WebApplicationException;

/**
 * Created by mdo on 7/18/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class RestErrorException extends WebApplicationException {
    private Object errorResponse;

    public RestErrorException() {
    }

    public RestErrorException(Object errorResponse) {
        this.errorResponse = errorResponse;
    }

    public Object getErrorResponse() {
        return errorResponse;
    }

    public void setErrorResponse(Object errorResponse) {
        this.errorResponse = errorResponse;
    }
}
