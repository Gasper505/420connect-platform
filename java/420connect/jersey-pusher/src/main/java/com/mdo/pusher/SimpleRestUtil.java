package com.mdo.pusher;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.glassfish.jersey.client.ClientProperties;
import org.glassfish.jersey.client.HttpUrlConnectorProvider;
import org.glassfish.jersey.client.JerseyClientBuilder;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ExecutionException;

/**
 * The SimpleRestUtil object.
 * User: mdo
 * Date: 12/31/13
 * Time: 4:28 PM
 */
public final class SimpleRestUtil {
    private static final Log LOG = LogFactory.getLog(SimpleRestUtil.class);

    public static <T> T get(String url, Class<T> clazz) throws IOException, ExecutionException, InterruptedException {
        MultivaluedMap<String, Object> headers = new MultivaluedHashMap<>();
        headers.putSingle("Content-Type", MediaType.APPLICATION_JSON);
        return get(url, clazz, headers);
    }

    public static <T> T get(String url, Class<T> clazz, MultivaluedMap<String, Object> headers) {
        Client client = JerseyClientBuilder.newClient().register(JacksonJsonProvider.class);
        WebTarget webTarget = client.target(url);
        return webTarget.request().accept(MediaType.APPLICATION_JSON).headers(headers).get(clazz);
    }

    public static <T> T get(Class<T> clazz, WebTarget webTarget) throws IOException, ExecutionException, InterruptedException {
        MultivaluedMap<String, Object> headers = new MultivaluedHashMap<>();
        headers.putSingle("Content-Type", MediaType.APPLICATION_JSON);
        return get(clazz, headers, webTarget);
    }

    public static <T> T get(Class<T> clazz, MultivaluedMap<String, Object> headers, WebTarget webTarget) {
        return webTarget.request().accept(MediaType.APPLICATION_JSON).headers(headers).get(clazz);
    }

    public static <T, E> T get(String url, Class<T> clazz, Class<E> errorClazz, MultivaluedMap<String, Object> headers) {
        Client client = JerseyClientBuilder.newClient().register(JacksonJsonProvider.class);
        WebTarget webTarget = client.target(url);
        Response response = webTarget.request().accept(MediaType.APPLICATION_JSON).headers(headers).get();

        if (response.getStatus() == 200) {
            return response.readEntity(clazz);
        } else {
            E errorObj = response.readEntity(errorClazz);
            // throw error
            throw new RestErrorException(errorObj);
        }
    }

    public static <T> T getWithSsl(String url, Class<T> clazz, MultivaluedMap<String, Object> headers) {
        Client client = null;
        try {
            if (checkSslURL(url)) {
                client = SslClientHelper.configureClient();
            } else {
                client = JerseyClientBuilder.newClient().register(JacksonJsonProvider.class);
            }
            WebTarget webTarget = client.target(url);
            client.getSslContext().getSupportedSSLParameters();
            printSSLInfo(client);
            return webTarget.request().accept(MediaType.APPLICATION_JSON).headers(headers).get(clazz);
        } finally {
            if (client != null) {
                client.close();
            }
        }
    }

    private static void printSSLInfo(Client client) {
        if (client.getSslContext() != null && client.getSslContext().getSupportedSSLParameters() != null) {

            String[] ciphers = client.getSslContext().getSupportedSSLParameters().getCipherSuites();
            String[] protocols = client.getSslContext().getSupportedSSLParameters().getProtocols();

            LOG.info("Ciphers:");
            for (String cipher : ciphers) {
                LOG.info(cipher);
            }

            LOG.info("Protocols:");
            for (String proto : protocols) {
                LOG.info(proto);
            }
        }
    }

    public static <T> T post(String url, Object jsonBody, Class<T> clazz, MultivaluedMap<String, Object> headers) throws IOException, ExecutionException, InterruptedException {
        Client client = JerseyClientBuilder.newClient().register(JacksonJsonProvider.class);
        WebTarget webTarget = client.target(url);
        Entity entity = Entity.entity(jsonBody, MediaType.APPLICATION_JSON);
        Response response = webTarget.request().accept(MediaType.APPLICATION_JSON).headers(headers).post(entity);
        if (response.getStatus() == 200) {
            return response.readEntity(clazz);
        } else {
            // throw error
            throw new RestErrorException(response.readEntity(String.class));
        }
    }

	public static <T> T postWithSsl(String url, Object jsonBody, Class<T> clazz, MultivaluedMap<String, Object> headers) throws IOException, ExecutionException, InterruptedException {
        Client client = null;
        try {

            if (checkSslURL(url)) {
                client = SslClientHelper.configureClient();
            } else {
                client = JerseyClientBuilder.newClient().register(JacksonJsonProvider.class);
            }

            WebTarget webTarget = client.target(url);
            Entity entity = Entity.entity(jsonBody, MediaType.APPLICATION_JSON);
            Response response = webTarget.request().accept(MediaType.APPLICATION_JSON).headers(headers).post(entity);
            if (response.getStatus() == 200) {
                return response.readEntity(clazz);
            } else {
                // throw error
                throw new RestErrorException(response.readEntity(String.class));
            }
        } finally {
            if (client != null) {
                client.close();
            }
        }
	}

    public static <T, E> T post(String url, Object jsonBody, Class<T> clazz, Class<E> errorClazz, MultivaluedMap<String, Object> headers) throws IOException, ExecutionException, InterruptedException {
        Client client = JerseyClientBuilder.newClient().register(JacksonJsonProvider.class);
        WebTarget webTarget = client.target(url);
        Entity entity = Entity.entity(jsonBody, MediaType.APPLICATION_JSON);
        Response response = webTarget.request().accept(MediaType.APPLICATION_JSON).headers(headers).post(entity);
        if (response.getStatus() == 200 || response.getStatus() == 201) {
            return response.readEntity(clazz);
        } else {
            E errorObj = response.readEntity(errorClazz);
            // throw error
            throw new RestErrorException(errorObj);
        }
    }

    public static <T, E> T postWithSsl2(String url, Object jsonBody, Class<T> clazz, Class<E> errorClazz, MultivaluedMap<String, Object> headers) throws IOException, ExecutionException, InterruptedException {
        Client client = null;
        try {
            if (checkSslURL(url)) {
                client = SslClientHelper.configureClient();
            } else {
                client = JerseyClientBuilder.newClient().register(JacksonJsonProvider.class);
            }
            JerseyClientBuilder.newClient().register(JacksonJsonProvider.class);
            WebTarget webTarget = client.target(url);
            Entity entity = Entity.entity(jsonBody, MediaType.APPLICATION_JSON);
            Response response = webTarget.request().accept(MediaType.APPLICATION_JSON).headers(headers).post(entity);
            if (response.getStatus() == 200 || response.getStatus() == 201) {
                return response.readEntity(clazz);
            } else {
                E errorObj = response.readEntity(errorClazz);
                // throw error
                throw new RestErrorException(errorObj);
            }
        } finally {
            if (client != null) {
                client.close();
            }
        }
    }


    public static <T, E> T put(String url, Object jsonBody, Class<T> clazz, Class<E> errorClazz, MultivaluedMap<String, Object> headers) throws IOException, ExecutionException, InterruptedException {
        Client client = JerseyClientBuilder.newClient().register(JacksonJsonProvider.class);
        WebTarget webTarget = client.target(url);
        Entity entity = Entity.entity(jsonBody, MediaType.APPLICATION_JSON);
        Response response = webTarget.request().accept(MediaType.APPLICATION_JSON).headers(headers).put(entity);
        if (response.getStatus() == 200 || response.getStatus() == 201) {
            return response.readEntity(clazz);
        } else {
            E errorObj = response.readEntity(errorClazz);
            // throw error
            throw new RestErrorException(errorObj);
        }
    }

    public static <T, E> T putWithSsl(String url, Object jsonBody, Class<T> clazz, Class<E> errorClazz, MultivaluedMap<String, Object> headers) throws IOException, ExecutionException, InterruptedException {
        Client client = null;
        try {
            if (checkSslURL(url)) {
                client = SslClientHelper.configureClient();
            } else {
                client = JerseyClientBuilder.newClient().register(JacksonJsonProvider.class);
            }
            WebTarget webTarget = client.target(url);
            Entity entity = Entity.entity(jsonBody, MediaType.APPLICATION_JSON);
            Response response = webTarget.request().accept(MediaType.APPLICATION_JSON).headers(headers).put(entity);
            if (response.getStatus() == 200 || response.getStatus() == 201) {
                return response.readEntity(clazz);
            } else {
                E errorObj = response.readEntity(errorClazz);
                // throw error
                throw new RestErrorException(errorObj);
            }
        } finally {
            if (client != null) {
                client.close();
            }
        }
    }

    public static <T> T put(String url, Object jsonBody, Class<T> clazz, MultivaluedMap<String, Object> headers) throws IOException, ExecutionException, InterruptedException {
        Client client = JerseyClientBuilder.newClient().register(JacksonJsonProvider.class);
        WebTarget webTarget = client.target(url);
        Entity entity = Entity.entity(jsonBody, MediaType.APPLICATION_JSON);
        Response response = webTarget.request().accept(MediaType.APPLICATION_JSON).headers(headers).put(entity);
        if (response.getStatus() == 200) {
            return response.readEntity(clazz);
        } else {
            // throw error
            throw new RestErrorException(response.getEntity());
        }
    }

    public static <T> T delete(String url, Class<T> clazz, MultivaluedMap<String, Object> headers) {
        Client client = JerseyClientBuilder.newClient().register(JacksonJsonProvider.class);
        WebTarget webTarget = client.target(url);
        return webTarget.request().accept(MediaType.APPLICATION_JSON).headers(headers).delete(clazz);
    }

    public static InputStream getFile(String url) throws IOException, ExecutionException, InterruptedException {
        return null;
    }

    public static <T> T get(String url, Class<T> clazz, MultivaluedMap<String, Object> queryParams, MultivaluedMap<String, Object> headers) {
        Client client = JerseyClientBuilder.newClient().register(JacksonJsonProvider.class);
        WebTarget webTarget = client.target(url);

        Iterator<String> iterator = queryParams.keySet().iterator();

        while (iterator.hasNext()) {
            String name = iterator.next();
            webTarget = webTarget.queryParam(name, queryParams.getFirst(name));
        }

        return webTarget.request().accept(MediaType.APPLICATION_JSON).headers(headers).get(clazz);
    }

    public static boolean checkSslURL(String url) {
        String pattern = "^(https)://.*$";
        return url.matches(pattern);
    }

    public static String postWithSsl(String url, String jsonBody, Class<String> clazz, MultivaluedMap<String, Object> headers, HashMap<String, Object> queryParams, String mediaType, int statusCode) {
        Client client = null;
        try {

            if (checkSslURL(url)) {
                client = SslClientHelper.configureClient();
            } else {
                client = JerseyClientBuilder.newClient().register(JacksonJsonProvider.class);
            }

            WebTarget webTarget = client.target(url);
            for (Map.Entry<String, Object> entry : queryParams.entrySet()) {
                webTarget = webTarget.queryParam(entry.getKey(), entry.getValue());
            }
            Entity entity = Entity.entity(jsonBody, mediaType);
            Response response = webTarget.request().accept(mediaType).headers(headers).post(entity);
            if (response.getStatus() == statusCode) {
                return response.readEntity(clazz);
            } else {
                // throw error
                throw new RestErrorException(response.readEntity(String.class));
            }
        } finally {
            if (client != null) {
                client.close();
            }
        }
    }

    public static String patchWithSsl(String url, String jsonBody, Class<String> clazz, MultivaluedMap<String, Object> headers, HashMap<String, Object> queryParams, String mediaType, int statusCode) {
        Client client = null;
        try {

            if (checkSslURL(url)) {
                client = SslClientHelper.configureClient();
            } else {
                client = JerseyClientBuilder.newClient().register(JacksonJsonProvider.class);
            }

            WebTarget webTarget = client.target(url).property(HttpUrlConnectorProvider.SET_METHOD_WORKAROUND, true);
            for (Map.Entry<String, Object> entry : queryParams.entrySet()) {
                webTarget = webTarget.queryParam(entry.getKey(), entry.getValue());
            }
            Entity entity = Entity.entity(jsonBody, mediaType);
            Response response = webTarget.request().accept(mediaType).headers(headers).build("PATCH", entity)
                    .invoke();
            if (response.getStatus() == statusCode) {
                return response.readEntity(clazz);
            } else {
                // throw error
                throw new RestErrorException(response.readEntity(String.class));
            }
        } finally {
            if (client != null) {
                client.close();
            }
        }
    }

    public static Response deleteWithSsl(String url, String body, MultivaluedMap<String, Object> headers, HashMap<String, Object> queryParams, String mediaType, int statusCode) {
        Client client = null;
        try {

            if (checkSslURL(url)) {
                client = SslClientHelper.configureClient();
            } else {
                client = JerseyClientBuilder.newClient().register(JacksonJsonProvider.class);
            }

            WebTarget webTarget = client.target(url).property(ClientProperties.SUPPRESS_HTTP_COMPLIANCE_VALIDATION, true);
            for (Map.Entry<String, Object> entry : queryParams.entrySet()) {
                webTarget = webTarget.queryParam(entry.getKey(), entry.getValue());
            }
            Response response;
            if (StringUtils.isNotBlank(body)) {
                response = webTarget.request(body).headers(headers).build("DELETE", Entity.entity(body, mediaType)).invoke();
            } else {
                response = webTarget.request().headers(headers).delete();
            }
            return response;
        } finally {
            if (client != null) {
                client.close();
            }
        }
    }

    public static <T> T getWithSsl(String url, Class<T> clazz, MultivaluedMap<String, Object> headers, HashMap<String, Object> queryParams, String mediaType) {
        Client client = null;
        try {
            if (checkSslURL(url)) {
                client = SslClientHelper.configureClient();
            } else {
                client = JerseyClientBuilder.newClient().register(JacksonJsonProvider.class);
            }

            WebTarget webTarget = client.target(url);
            for (Map.Entry<String, Object> entry : queryParams.entrySet()) {
                webTarget = webTarget.queryParam(entry.getKey(), entry.getValue());
            }
            client.getSslContext().getSupportedSSLParameters();
            printSSLInfo(client);
            return webTarget.request().accept(mediaType).headers(headers).get(clazz);
        } finally {
            if (client != null) {
                client.close();
            }
        }
    }

    public static String putWithSsl(String url, String jsonBody, Class<String> clazz, MultivaluedMap<String, Object> headers, HashMap<String, Object> queryParams, String mediaType, int statusCode) throws IOException, ExecutionException, InterruptedException {
        Client client = null;
        try {

            if (checkSslURL(url)) {
                client = SslClientHelper.configureClient();
            } else {
                client = JerseyClientBuilder.newClient().register(JacksonJsonProvider.class);
            }

            WebTarget webTarget = client.target(url);
            for (Map.Entry<String, Object> entry : queryParams.entrySet()) {
                webTarget = webTarget.queryParam(entry.getKey(), entry.getValue());
            }
            Entity entity = Entity.entity(jsonBody, mediaType);
            Response response = webTarget.request().headers(headers).put(entity);
            if (response.getStatus() == statusCode) {
                return response.readEntity(clazz);
            } else {
                // throw error
                throw new RestErrorException(response.readEntity(String.class));
            }
        } finally {
            if (client != null) {
                client.close();
            }
        }
    }
}


