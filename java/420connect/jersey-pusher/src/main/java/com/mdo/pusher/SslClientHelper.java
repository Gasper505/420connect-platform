package com.mdo.pusher;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import org.glassfish.jersey.client.JerseyClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Enumeration;
import java.util.Properties;

public class SslClientHelper {
    private static final Logger LOG = LoggerFactory.getLogger(SslClientHelper.class);

    public static Client configureClient() {

        TrustManager[] certs = new TrustManager[]{
                new X509TrustManager() {
                    @Override
                    public X509Certificate[] getAcceptedIssuers() {
                        return new X509Certificate[] {};
                    }

                    @Override
                    public void checkServerTrusted(X509Certificate[] chain, String authType)
                            throws CertificateException {
                    }

                    @Override
                    public void checkClientTrusted(X509Certificate[] chain, String authType)
                            throws CertificateException {
                    }
                }
        };
        try {
            SSLContext sc = SSLContext.getInstance("TLSv1.2");
            sc.init(null, certs, new java.security.SecureRandom());
            ClientBuilder clientBuilder = JerseyClientBuilder.newBuilder();
            clientBuilder.hostnameVerifier((s, sslSession) -> true);
            return clientBuilder.sslContext(sc).register(JacksonJsonProvider.class).build();
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }

        return JerseyClientBuilder.newClient().register(JacksonJsonProvider.class);
//        try {
//            SSLContext ctx = SSLContext.getInstance("TLSv1.2");
//            //ctx.init(null, certs, new SecureRandom());
//            SSLContext.setDefault(ctx);
//            HttpsURLConnection.setDefaultSSLSocketFactory(ctx.getSocketFactory());
//            //return JerseyClientBuilder.newBuilder().sslContext(ctx).hostnameVerifier(new NoopHostnameVerifier()).register(JacksonJsonProvider.class).build();
//            return JerseyClientBuilder.newClient().register(JacksonJsonProvider.class);
//        } catch (java.security.GeneralSecurityException ex) {
//            LOG.error("Error while Webhook create task :" + ex.getMessage());
//            return JerseyClientBuilder.newClient().register(JacksonJsonProvider.class);
//        }

    }

    private static void printSystemProps() {
        Properties p = System.getProperties();
        Enumeration keys = p.keys();
        while (keys.hasMoreElements()) {
            String key = (String) keys.nextElement();
            String value = (String) p.get(key);
            System.out.println(key + ": " + value);
        }
    }
}