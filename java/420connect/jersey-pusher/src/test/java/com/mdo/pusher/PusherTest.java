package com.mdo.pusher;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * The PusherTest object.
 * User: mdo
 * Date: 12/31/13
 * Time: 7:01 PM
 */
public class PusherTest {

    @Test
    public void testMD5() {
        String s = "{\"name\":\"foo\",\"channels\":[\"project-3\"],\"data\":\"{\\\"some\\\":\\\"data\\\"}\"}";

        String result = Pusher.generateMD5Hash(s);
        System.out.println(result);

        assertTrue(result.equals("ec365a775a4cd0599faeb73354201b6f"));
    }

    @Test
    public void testSignHMACSHA256() {
        String s = "POST\n/apps/3/events\nauth_key=278d425bdf160c739803&auth_timestamp=1353088179&auth_version=1.0&body_md5=ec365a775a4cd0599faeb73354201b6f";

        String result = Pusher.signPhrase("7ad3773142a6692b25b8", s);
        System.out.println(result);
        assertTrue("signature must be equal", result.equals("da454824c97ba181a32ccc17a72625ba02771f50b50e1e7430e47a1f3f457e6c"));
    }

    @Test
    public void triggerTest() {
        Pusher pusher = new Pusher("62726", "5264227e5c4f4003f821", "74f1f5eee268a121ddf6");

        MyData data = new MyData();
        data.setText("another data");
        data.setUserId("123");
        pusher.trigger("flipcard-test-notification", "feed", data);
    }

    @Test
    public void getChannelListTest() {
        Pusher pusher = new Pusher("62726", "5264227e5c4f4003f821", "74f1f5eee268a121ddf6");
        PChannelListResult result = pusher.getChannels(null, null);

        System.out.println(result);
    }

    @Test
    public void getChannelData() {
        Pusher pusher = new Pusher("62726", "5264227e5c4f4003f821", "74f1f5eee268a121ddf6");
        PChannelResult result = pusher.getChannelData("flipcard-test-notification", "");

        System.out.println(result);
    }


    public class MyData {
        private String text;
        private String userId;

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }
    }
}
