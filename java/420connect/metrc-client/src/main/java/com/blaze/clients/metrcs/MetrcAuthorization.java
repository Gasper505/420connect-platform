package com.blaze.clients.metrcs;

import javax.ws.rs.core.MultivaluedMap;

/**
 * Created by Gaurav Saini on 7/20/17.
 */
public interface MetrcAuthorization {

    MultivaluedMap<String, Object> makeHeaders();

    String getHost();

    String getVendorAPIKey();

    String getUserAPIKey();

    String getFacilityLicense();
}
