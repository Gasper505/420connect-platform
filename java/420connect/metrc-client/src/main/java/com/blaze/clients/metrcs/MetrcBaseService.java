package com.blaze.clients.metrcs;

/**
 * Created by mdo on 8/25/17.
 */
public abstract class MetrcBaseService {
    protected MetrcAuthorization metrcAuthorization;

    public MetrcBaseService(MetrcAuthorization metrcAuthorization) {
        this.metrcAuthorization = metrcAuthorization;
    }
}
