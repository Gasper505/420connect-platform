package com.blaze.clients.metrcs;

/*
[
  {
    "QuantityType": "CountBased",
    "Name": "Each",
    "Abbreviation": "ea"
  },
  {
    "QuantityType": "WeightBased",
    "Name": "Ounces",
    "Abbreviation": "oz"
  },
  {
    "QuantityType": "WeightBased",
    "Name": "Pounds",
    "Abbreviation": "lb"
  },
  {
    "QuantityType": "WeightBased",
    "Name": "Grams",
    "Abbreviation": "g"
  },
  {
    "QuantityType": "WeightBased",
    "Name": "Milligrams",
    "Abbreviation": "mg"
  },
  {
    "QuantityType": "WeightBased",
    "Name": "Kilograms",
    "Abbreviation": "kg"
  }
]*/
public enum MetrcMeasurement {
    Each("Each","ea",MetrcMeasurementType.CountBased),
    Ounces("Ounces","oz",MetrcMeasurementType.WeightBased),
    Pounds("Pounds","lb",MetrcMeasurementType.WeightBased),
    Milligrams("Milligrams","mg",MetrcMeasurementType.WeightBased),
    Kilograms("Ounces","kg",MetrcMeasurementType.WeightBased),
    Grams("Grams","g",MetrcMeasurementType.WeightBased),
    Milliliters("Milliliters","ml",MetrcMeasurementType.VolumeBased),
    Liters("Liters","l",MetrcMeasurementType.VolumeBased);

    MetrcMeasurement(String value, String abbr, MetrcMeasurementType measurementType) {
        this.measurementName = value;
        this.measurementType = measurementType;
        this.abbr = abbr;
    }

    public String measurementName;
    public String abbr;
    public MetrcMeasurementType measurementType;

}

