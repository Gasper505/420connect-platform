package com.blaze.clients.metrcs;

public enum  MetrcMeasurementType {
    CountBased,
    WeightBased,
    VolumeBased
}
