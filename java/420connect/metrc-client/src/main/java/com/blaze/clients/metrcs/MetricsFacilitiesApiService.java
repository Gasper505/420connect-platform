package com.blaze.clients.metrcs;

import com.blaze.clients.metrcs.models.facilities.MetricsFacilityList;
import com.mdo.pusher.SimpleRestUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Created by mdo on 7/18/17.
 */
public class MetricsFacilitiesApiService extends MetrcBaseService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MetricsFacilitiesApiService.class);


    public MetricsFacilitiesApiService(MetrcAuthorization metrcAuthorization) {
        super(metrcAuthorization);
    }

    public String getEmployees(String licenseNumber) {
        String url = metrcAuthorization.getHost() + "/employees/v1?licenseNumber=" + licenseNumber;
        return SimpleRestUtil.get(url, String.class, metrcAuthorization.makeHeaders());
    }

    public MetricsFacilityList getFacilities() {
        String url = metrcAuthorization.getHost() + "/facilities/v1";
        MetricsFacilityList results = SimpleRestUtil.get(url, MetricsFacilityList.class, metrcAuthorization.makeHeaders());
        return results;
    }


}
