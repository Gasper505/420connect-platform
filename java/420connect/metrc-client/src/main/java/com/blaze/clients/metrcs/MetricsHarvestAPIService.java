package com.blaze.clients.metrcs;

import com.blaze.clients.metrcs.models.MetricsErrorList;
import com.blaze.clients.metrcs.models.harvests.*;
import com.mdo.pusher.SimpleRestUtil;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by mdo on 7/18/17.
 */
public class MetricsHarvestAPIService extends MetrcBaseService {


    public MetricsHarvestAPIService(MetrcAuthorization metrcAuthorization) {
        super(metrcAuthorization);
    }

    public MetricsHarvest getHarvestDetails(String id) {
        String url = metrcAuthorization.getHost() + "/harvests/v1/" + id;
        return SimpleRestUtil.get(url, MetricsHarvest.class, metrcAuthorization.makeHeaders());
    }

    public MetricsHarvestList getActiveHarvests(String licenseNumber) {
        String url = metrcAuthorization.getHost() + "/harvests/v1/active?licenseNumber=" + licenseNumber;
        return SimpleRestUtil.get(url, MetricsHarvestList.class, metrcAuthorization.makeHeaders());
    }

    public MetricsHarvestList getOnHoldHarvests(String licenseNumber) {
        String url = metrcAuthorization.getHost() + "/harvests/v1/onhold?licenseNumber=" + licenseNumber;
        return SimpleRestUtil.get(url, MetricsHarvestList.class, metrcAuthorization.makeHeaders());
    }


    public MetricsHarvestList getInactiveHarvests(String licenseNumber) {
        String url = metrcAuthorization.getHost() + "/harvests/v1/inactive?licenseNumber=" + licenseNumber;
        return SimpleRestUtil.get(url, MetricsHarvestList.class, metrcAuthorization.makeHeaders());
    }


    public String createPackages(List<MetricsHarvestPackage> metricsPackage, String licenseNumber) throws InterruptedException, ExecutionException, IOException {
        String url = metrcAuthorization.getHost() + "/harvests/v1/createpackages?licenseNumber=" + licenseNumber;
        return SimpleRestUtil.post(url, metricsPackage, String.class, MetricsErrorList.class, metrcAuthorization.makeHeaders());
    }


    public String removeWaste(List<MetricsWaste> metricsWastes, String licenseNumber) throws InterruptedException, ExecutionException, IOException {
        String url = metrcAuthorization.getHost() + "/harvests/v1/removewaste?licenseNumber=" + licenseNumber;
        return SimpleRestUtil.post(url, metricsWastes, String.class, MetricsErrorList.class, metrcAuthorization.makeHeaders());
    }


    public String harvestFinish(List<MetricsHarvestFinish> metricsFinishes, String licenseNumber) throws InterruptedException, ExecutionException, IOException {
        String url = metrcAuthorization.getHost() + "/harvests/v1/finish?licenseNumber=" + licenseNumber;
        return SimpleRestUtil.post(url, metricsFinishes, String.class, MetricsErrorList.class, metrcAuthorization.makeHeaders());
    }


    public String harvestUnfinish(List<MetricsHarvestFinish> metricsFinishes, String licenseNumber) throws InterruptedException, ExecutionException, IOException {
        String url = metrcAuthorization.getHost() + "/harvests/v1/unfinish?licenseNumber" + licenseNumber;
        return SimpleRestUtil.post(url, metricsFinishes, String.class, MetricsErrorList.class, metrcAuthorization.makeHeaders());
    }
}