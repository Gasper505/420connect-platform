package com.blaze.clients.metrcs;

import com.blaze.clients.metrcs.models.MetricsErrorList;
import com.blaze.clients.metrcs.models.items.*;
import com.blaze.clients.metrcs.models.packages.MetricsPackagesList;
import com.mdo.pusher.SimpleRestUtil;

import javax.ws.rs.core.MultivaluedMap;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by mdo on 7/18/17.
 */
public class MetricsItemsAPIService extends MetrcBaseService {
    public MetricsItemsAPIService(MetrcAuthorization metrcAuthorization) {
        super(metrcAuthorization);
    }


    public MetricsItems getItemDetails(String id) {
        String url = metrcAuthorization.getHost() + "/items/v1/" + id;
        return SimpleRestUtil.get(url, MetricsItems.class, metrcAuthorization.makeHeaders());
    }


    public MetricsItemsList getActiveItems(String licenseNumber) {
        String url = metrcAuthorization.getHost() + "/items/v1/active?licenseNumber=" + licenseNumber;
        return SimpleRestUtil.get(url, MetricsItemsList.class, metrcAuthorization.makeHeaders());
    }

    public MetricsItemsList getActiveItems(String licenseNumber, String startDate, String endDate) {
        //String url = metrcAuthorization.getHost() + "/packages/v1/inactive?licenseNumber=" + licenseNumber;

        String url = String.format("%s/items/v1/active?licenseNumber=%s&lastModifiedStart=%sZ&lastModifiedEnd=%sZ",
                metrcAuthorization.getHost(),
                licenseNumber,
                startDate,
                endDate);
        return SimpleRestUtil.get(url, MetricsItemsList.class, String.class,metrcAuthorization.makeHeaders());
    }


    public MetricsItemCategoryList getItemCategories() {
        String url = metrcAuthorization.getHost() + "/items/v1/categories";
        return SimpleRestUtil.get(url, MetricsItemCategoryList.class, metrcAuthorization.makeHeaders());
    }


    public String createItems(List<MetricsItem> metricsItems, String licenseNumber) throws InterruptedException, ExecutionException, IOException {
        String url = metrcAuthorization.getHost() + "/items/v1/create?licenseNumber=" + licenseNumber;
        return SimpleRestUtil.post(url, metricsItems, String.class, MetricsErrorList.class, metrcAuthorization.makeHeaders());
    }


    public String updateItems(List<MetricsItem> metricsItems, String licenseNumber) throws InterruptedException, ExecutionException, IOException {
        String url = metrcAuthorization.getHost() + "/items/v1/update?licenseNumber=" + licenseNumber;
        MultivaluedMap<String, Object> headers = metrcAuthorization.makeHeaders();
        return SimpleRestUtil.post(url, metricsItems, String.class, MetricsErrorList.class, headers);
    }


    public String deleteItem(String id, String licenseNumber) {
        String url = metrcAuthorization.getHost() + "/items/v1/" + id + "?licenseNumber=" + licenseNumber;
        return SimpleRestUtil.delete(url, String.class, metrcAuthorization.makeHeaders());
    }

}
