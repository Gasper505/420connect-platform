package com.blaze.clients.metrcs;

import com.blaze.clients.metrcs.models.MetricsErrorList;
import com.blaze.clients.metrcs.models.labtests.MetricsLabTest;
import com.blaze.clients.metrcs.models.labtests.MetricsLabTestType;
import com.mdo.pusher.SimpleRestUtil;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by mdo on 7/18/17.
 */
public class MetricsLabTestAPIService extends MetrcBaseService {

    public MetricsLabTestAPIService(MetrcAuthorization metrcAuthorization) {
        super(metrcAuthorization);
    }


    public List<String> getLabTestStates() {
        String url = metrcAuthorization.getHost() + "/labtests/v1/states";
        return SimpleRestUtil.get(url, List.class, metrcAuthorization.makeHeaders());
    }


    public MetricsLabTestType getLabTestTypes() {
        String url = metrcAuthorization.getHost() + "/labtests/v1/types";
        return SimpleRestUtil.get(url, MetricsLabTestType.class, metrcAuthorization.makeHeaders());
    }


    public String labTestRecord(List<MetricsLabTest> metricsLabTests, String licenseNumber) throws InterruptedException, ExecutionException, IOException {
        String url = metrcAuthorization.getHost() + "/labtests/v1/record?licenseNumber=" + licenseNumber;
        return SimpleRestUtil.post(url, metricsLabTests, String.class, MetricsErrorList.class, metrcAuthorization.makeHeaders());
    }

}
