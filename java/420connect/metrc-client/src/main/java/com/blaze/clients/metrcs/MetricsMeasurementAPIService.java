package com.blaze.clients.metrcs;

import com.blaze.clients.metrcs.models.measurements.MetricsUnitOfMeasureList;
import com.mdo.pusher.SimpleRestUtil;

/**
 * Created by mdo on 7/18/17.
 */
public class MetricsMeasurementAPIService extends MetrcBaseService {
    public MetricsMeasurementAPIService(MetrcAuthorization metrcAuthorization) {
        super(metrcAuthorization);
    }


    public MetricsUnitOfMeasureList getUnitsOfMeasurements() {
        String url = metrcAuthorization.getHost() + "/unitsofmeasure/v1/active";
        return SimpleRestUtil.get(url, MetricsUnitOfMeasureList.class, metrcAuthorization.makeHeaders());
    }
}
