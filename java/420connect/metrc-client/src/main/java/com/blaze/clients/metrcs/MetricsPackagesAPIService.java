package com.blaze.clients.metrcs;

import com.blaze.clients.metrcs.models.MetricsErrorList;
import com.blaze.clients.metrcs.models.packages.*;
import com.mdo.pusher.SimpleRestUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by mdo on 7/18/17.
 */
public class MetricsPackagesAPIService extends MetrcBaseService {
    private static final Log LOG = LogFactory.getLog(MetricsPackagesAPIService.class);



    public MetricsPackagesAPIService(MetrcAuthorization metrcAuthorization) {
        super(metrcAuthorization);
    }


    public MetricsPackages getPackage(String id) {
        String url = metrcAuthorization.getHost() + "/packages/v1/" + id;
        return SimpleRestUtil.get(url, MetricsPackages.class, metrcAuthorization.makeHeaders());
    }


    public MetricsPackages getPackageLabel(String label) {
        String url = metrcAuthorization.getHost() + "/packages/v1/" + label;
        return SimpleRestUtil.get(url, MetricsPackages.class, metrcAuthorization.makeHeaders());
    }


    public MetricsPackagesList getActivePackages(String licenseNumber) {
        LOG.info("metrc auth host: " + metrcAuthorization.getHost());
        String url = metrcAuthorization.getHost()+"/packages/v1/active?licenseNumber="+licenseNumber;
        return SimpleRestUtil.get(url, MetricsPackagesList.class, metrcAuthorization.makeHeaders());
    }


    public MetricsPackagesList getOnholdPackages(String licenseNumber) {
        String url = metrcAuthorization.getHost() + "/packages/v1/onhold?licenseNumber=" + licenseNumber;
        return SimpleRestUtil.get(url, MetricsPackagesList.class, metrcAuthorization.makeHeaders());
    }


    public MetricsPackagesList getInActivePackages(String licenseNumber) {
        String url = metrcAuthorization.getHost() + "/packages/v1/inactive?licenseNumber=" + licenseNumber;
        return SimpleRestUtil.get(url, MetricsPackagesList.class, metrcAuthorization.makeHeaders());
    }

    public MetricsPackagesList getPackagesByStatus(String licenseNumber, String status, String startDate, String endDate) {
        //String url = metrcAuthorization.getHost() + "/packages/v1/inactive?licenseNumber=" + licenseNumber;

        String url = String.format("%s/packages/v1/%s?licenseNumber=%s&lastModifiedStart=%sZ&lastModifiedEnd=%sZ",
                metrcAuthorization.getHost(),
                status,
                licenseNumber,
                startDate,
                endDate);
        return SimpleRestUtil.get(url, MetricsPackagesList.class, String.class,metrcAuthorization.makeHeaders());
    }


    public List<String> getPackageTypes() {
        String url = metrcAuthorization.getHost() + "/packages/v1/types";
        return SimpleRestUtil.get(url, List.class, metrcAuthorization.makeHeaders());
    }


    public String createPackages(List<MetricsPackage> metricsPackages, String licenseNumber) throws InterruptedException, ExecutionException, IOException {
        String url = metrcAuthorization.getHost() + "/packages/v1/create?licenseNumber=" + licenseNumber;
        return SimpleRestUtil.post(url, metricsPackages, String.class, String.class, metrcAuthorization.makeHeaders());
    }


    public String createPackagesTest(List<MetricsPackage> metricsPackages, String licenseNumber) throws InterruptedException, ExecutionException, IOException {
        String url = metrcAuthorization.getHost() + "/packages/v1/create/testing?licenseNumber=" + licenseNumber;
        return SimpleRestUtil.post(url, metricsPackages, String.class, MetricsErrorList.class, metrcAuthorization.makeHeaders());
    }


    public String createPackagesPlantings(List<MetricsPackagePlanting> metricsPackagePlantings, String licenseNumber) throws InterruptedException, ExecutionException, IOException {
        String url = metrcAuthorization.getHost() + "/packages/v1/create/plantings?licenseNumber=" + licenseNumber;
        return SimpleRestUtil.post(url, metricsPackagePlantings, String.class, MetricsErrorList.class, metrcAuthorization.makeHeaders());
    }


    public String changePackageItem(List<MetricsPackageItem> metricsPackageItems, String licenseNumber) throws InterruptedException, ExecutionException, IOException {
        String url = metrcAuthorization.getHost() + "/packages/v1/change/item?licenseNumber=" + licenseNumber;
        return SimpleRestUtil.post(url, metricsPackageItems, String.class, MetricsErrorList.class, metrcAuthorization.makeHeaders());
    }


    public String adjustPackage(List<MetricsPackageAdjustment> metricsPackageAdjustments, String licenseNumber) throws InterruptedException, ExecutionException, IOException {
        String url = metrcAuthorization.getHost() + "/packages/v1/adjust?licenseNumber=" + licenseNumber;
        return SimpleRestUtil.post(url, metricsPackageAdjustments, String.class, String.class, metrcAuthorization.makeHeaders());
    }


    public String packageFinish(List<MetricsPackageFinish> metricsPackageFinishes, String licenseNumber) throws InterruptedException, ExecutionException, IOException {
        String url = metrcAuthorization.getHost() + "/packages/v1/finish?licenseNumber=" + licenseNumber;
        return SimpleRestUtil.post(url, metricsPackageFinishes, String.class, MetricsErrorList.class, metrcAuthorization.makeHeaders());
    }


    public String packageUnfinish(List<MetricsPackageFinish> metricsPackageFinishes, String licenseNumber) throws InterruptedException, ExecutionException, IOException {
        String url = metrcAuthorization.getHost() + "/packages/v1/unfinish?licenseNumber=" + licenseNumber;
        return SimpleRestUtil.post(url, metricsPackageFinishes, String.class, MetricsErrorList.class, metrcAuthorization.makeHeaders());
    }


    public String remediatePackage(List<MetricsPackageRemediate> metricsPackageRemediates, String licenseNumber) throws InterruptedException, ExecutionException, IOException {
        String url = metrcAuthorization.getHost() + "/packages/v1/remediate?licenseNumber=" + licenseNumber;
        return SimpleRestUtil.post(url, metricsPackageRemediates, String.class, MetricsErrorList.class, metrcAuthorization.makeHeaders());
    }
}
