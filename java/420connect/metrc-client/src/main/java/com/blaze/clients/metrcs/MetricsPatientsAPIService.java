package com.blaze.clients.metrcs;

import com.blaze.clients.metrcs.models.MetricsErrorList;
import com.blaze.clients.metrcs.models.patients.MetrcPatientResult;
import com.blaze.clients.metrcs.models.patients.MetricsPatient;
import com.mdo.pusher.SimpleRestUtil;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by mdo on 7/18/17.
 */
public class MetricsPatientsAPIService extends MetrcBaseService {

    public MetricsPatientsAPIService(MetrcAuthorization metrcAuthorization) {
        super(metrcAuthorization);
    }


    public String getPatient(String id) {
        String url = metrcAuthorization.getHost() + "/patients/v1/" + id;
        return SimpleRestUtil.get(url, String.class, metrcAuthorization.makeHeaders());
    }

    public String getActivePatients(String licenseNumber) {
        String url = metrcAuthorization.getHost() + "/patients/v1/active?licenseNumber=" + licenseNumber;
        return SimpleRestUtil.get(url, String.class, metrcAuthorization.makeHeaders());
    }


    public MetrcPatientResult getPatientStatus(String ptnLicenseNumber, String licenseNumber) {
        String url = metrcAuthorization.getHost() + String.format("/patients/v1/status/%s/?licenseNumber=%s",ptnLicenseNumber,licenseNumber);//"/patients/v1/status/?licenseNumber=" + licenseNumber;
        return SimpleRestUtil.get(url, MetrcPatientResult.class, metrcAuthorization.makeHeaders());
    }


    public String addPatient(List<MetricsPatient> metricsPatients, String licenseNumber) throws InterruptedException, ExecutionException, IOException {
        String url = metrcAuthorization.getHost() + "/patients/v1/add?licenseNumber=" + licenseNumber;
        return SimpleRestUtil.post(url, metricsPatients, String.class, MetricsErrorList.class, metrcAuthorization.makeHeaders());
    }


    public String updatePatient(List<MetricsPatient> metricsPatients, String licenseNumber) throws InterruptedException, ExecutionException, IOException {
        String url = metrcAuthorization.getHost() + "/patients/v1/update?licenseNumber=" + licenseNumber;
        return SimpleRestUtil.post(url, metricsPatients, String.class, MetricsErrorList.class, metrcAuthorization.makeHeaders());
    }


    public String deletePatient(String licenseNumber) {
        String url = metrcAuthorization.getHost() + "/patients/v1/1?licenseNumber=" + licenseNumber;
        return SimpleRestUtil.delete(url, String.class, metrcAuthorization.makeHeaders());
    }

}
