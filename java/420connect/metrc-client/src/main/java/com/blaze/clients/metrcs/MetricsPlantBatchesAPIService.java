package com.blaze.clients.metrcs;

import com.blaze.clients.metrcs.models.MetricsErrorList;
import com.blaze.clients.metrcs.models.plants.*;
import com.mdo.pusher.SimpleRestUtil;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by mdo on 7/18/17.
 */
public class MetricsPlantBatchesAPIService extends MetrcBaseService {

    public MetricsPlantBatchesAPIService(MetrcAuthorization metrcAuthorization) {
        super(metrcAuthorization);
    }


    public MetricsPlantBatchPackages getPatient(String id) {
        String url = metrcAuthorization.getHost() + "/plantbatches/v1/" + id;
        return SimpleRestUtil.get(url, MetricsPlantBatchPackages.class, metrcAuthorization.makeHeaders());
    }


    public MetricsPlantBatchPackagesList getActivePlantBatches(String licenseNumber) {
        String url = metrcAuthorization.getHost() + "/plantbatches/v1/active?licenseNumber=" + licenseNumber;
        return SimpleRestUtil.get(url, MetricsPlantBatchPackagesList.class, metrcAuthorization.makeHeaders());
    }


    public MetricsPlantBatchPackagesList getInActivePlantBatches(String licenseNumber) {
        String url = metrcAuthorization.getHost() + "/plantbatches/v1/inactive?licenseNumber=" + licenseNumber;
        return SimpleRestUtil.get(url, MetricsPlantBatchPackagesList.class, metrcAuthorization.makeHeaders());
    }


    public List<String> getPlantBatchesTypes() {
        String url = metrcAuthorization.getHost() + "/plantbatches/v1/types";
        return SimpleRestUtil.get(url, List.class, metrcAuthorization.makeHeaders());
    }


    public String createPlantBatchesPlantings(List<MetricsPlantBatchPlanting> metricsPlantBatchPlantings, String licenseNumber) throws InterruptedException, ExecutionException, IOException {
        String url = metrcAuthorization.getHost() + "/plantbatches/v1/createplantings?licenseNumber=" + licenseNumber;
        return SimpleRestUtil.post(url, metricsPlantBatchPlantings, String.class, MetricsErrorList.class, metrcAuthorization.makeHeaders());
    }


    public String createPlantBatchesPackages(List<MetricsPlantBatchPackage> metricsPlantBatchPackages, String licenseNumber) throws InterruptedException, ExecutionException, IOException {
        String url = metrcAuthorization.getHost() + "/plantbatches/v1/createpackages?licenseNumber=" + licenseNumber;
        return SimpleRestUtil.post(url, metricsPlantBatchPackages, String.class, MetricsErrorList.class, metrcAuthorization.makeHeaders());
    }


    public String changePlantBatchesGrowthPhase(List<MetricsPlantBatchGrowthPhase> metricsPlantBatchGrowthPhases, String licenseNumber) throws InterruptedException, ExecutionException, IOException {
        String url = metrcAuthorization.getHost() + "/plantbatches/v1/changegrowthphase?licenseNumber=" + licenseNumber;
        return SimpleRestUtil.post(url, metricsPlantBatchGrowthPhases, String.class, MetricsErrorList.class, metrcAuthorization.makeHeaders());
    }


    public String destroyPlantBatch(List<MetricsPlantBatchDestroy> metricsPlantBatchDestroys, String licenseNumber) throws InterruptedException, ExecutionException, IOException {
        String url = metrcAuthorization.getHost() + "/plantbatches/v1/destroy?licenseNumber=" + licenseNumber;
        return SimpleRestUtil.post(url, metricsPlantBatchDestroys, String.class, MetricsErrorList.class, metrcAuthorization.makeHeaders());
    }
}
