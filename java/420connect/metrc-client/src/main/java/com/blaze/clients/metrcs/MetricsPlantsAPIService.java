package com.blaze.clients.metrcs;

import com.blaze.clients.metrcs.models.MetricsErrorList;
import com.blaze.clients.metrcs.models.plants.*;
import com.mdo.pusher.SimpleRestUtil;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by mdo on 7/18/17.
 */
public class MetricsPlantsAPIService extends MetrcBaseService {

    public MetricsPlantsAPIService(MetrcAuthorization metrcAuthorization) {
        super(metrcAuthorization);
    }


    public MetricsPlant getPlants(String id) {
        String url = metrcAuthorization.getHost() + "/plants/v1/" + id;
        return SimpleRestUtil.get(url, MetricsPlant.class, metrcAuthorization.makeHeaders());
    }


    public MetricsPlant getPlantsByLabel(String label) {
        String url = metrcAuthorization.getHost() + "/plants/v1/" + label;
        return SimpleRestUtil.get(url, MetricsPlant.class, metrcAuthorization.makeHeaders());
    }


    public MetricsPlantList getVegetativePlants(String licenseNumber) {
        String url = metrcAuthorization.getHost() + "/plants/v1/vegetative?licenseNumber=" + licenseNumber;
        return SimpleRestUtil.get(url, MetricsPlantList.class, metrcAuthorization.makeHeaders());
    }


    public MetricsPlantList getFloweringPlants(String licenseNumber) {
        String url = metrcAuthorization.getHost() + "/plants/v1/flowering?licenseNumber=" + licenseNumber;
        return SimpleRestUtil.get(url, MetricsPlantList.class, metrcAuthorization.makeHeaders());
    }


    public MetricsPlantList getOnholdPlants(String licenseNumber) {
        String url = metrcAuthorization.getHost() + "/plants/v1/onhold?licenseNumber=" + licenseNumber;
        return SimpleRestUtil.get(url, MetricsPlantList.class, metrcAuthorization.makeHeaders());
    }


    public MetricsPlantList getInActivePlants(String licenseNumber) {
        String url = metrcAuthorization.getHost() + "/plants/v1/inactive?licenseNumber=" + licenseNumber;
        return SimpleRestUtil.get(url, MetricsPlantList.class, metrcAuthorization.makeHeaders());
    }


    public String movePlants(List<MetricsMovePlant> metricsMovePlants, String licenseNumber) throws InterruptedException, ExecutionException, IOException {
        String url = metrcAuthorization.getHost() + "/plants/v1/moveplants?licenseNumber=" + licenseNumber;
        return SimpleRestUtil.post(url, metricsMovePlants, String.class, MetricsErrorList.class, metrcAuthorization.makeHeaders());
    }


    public String changePlantGrowthPhases(List<MetricsPlantGrowthPhase> metricsPlantGrowthPhases, String licenseNumber) throws InterruptedException, ExecutionException, IOException {
        String url = metrcAuthorization.getHost() + "/plants/v1/changegrowthphases?licenseNumber=" + licenseNumber;
        return SimpleRestUtil.post(url, metricsPlantGrowthPhases, String.class, MetricsErrorList.class, metrcAuthorization.makeHeaders());
    }


    public String destroyPlants(List<MetricsPlantDestroy> metricsPlantDestroys, String licenseNumber) throws InterruptedException, ExecutionException, IOException {
        String url = metrcAuthorization.getHost() + "/plants/v1/destroyplants?licenseNumber=" + licenseNumber;
        return SimpleRestUtil.post(url, metricsPlantDestroys, String.class, MetricsErrorList.class, metrcAuthorization.makeHeaders());
    }


    public String createPlantPlantings(List<MetricsPlantPlanting> metricsPlantPlantings, String licenseNumber) throws InterruptedException, ExecutionException, IOException {
        String url = metrcAuthorization.getHost() + "/plants/v1/create/plantings?licenseNumber=" + licenseNumber;
        return SimpleRestUtil.post(url, metricsPlantPlantings, String.class, MetricsErrorList.class, metrcAuthorization.makeHeaders());
    }


    public String manicurePlants(List<MetricsManicurePlants> metricsManicurePlants, String licenseNumber) throws InterruptedException, ExecutionException, IOException {
        String url = metrcAuthorization.getHost() + "/plants/v1/manicureplants?licenseNumber=" + licenseNumber;
        return SimpleRestUtil.post(url, metricsManicurePlants, String.class, MetricsErrorList.class, metrcAuthorization.makeHeaders());
    }


    public String harvestPlants(List<MetricsManicurePlants> metricsManicurePlants, String licenseNumber) throws InterruptedException, ExecutionException, IOException {
        String url = metrcAuthorization.getHost() + "/plants/v1/harvestplants?licenseNumber=" + licenseNumber;
        return SimpleRestUtil.post(url, metricsManicurePlants, String.class, MetricsErrorList.class, metrcAuthorization.makeHeaders());
    }
}
