package com.blaze.clients.metrcs;

import com.blaze.clients.metrcs.models.MetricsErrorList;
import com.blaze.clients.metrcs.models.rooms.MetricsRoom;
import com.blaze.clients.metrcs.models.rooms.MetricsRoomList;
import com.mdo.pusher.SimpleRestUtil;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by mdo on 7/18/17.
 */
public class MetricsRoomsAPIService extends MetrcBaseService {

    public MetricsRoomsAPIService(MetrcAuthorization metrcAuthorization) {
        super(metrcAuthorization);
    }


    public MetricsRoom getRooms(String id) {
        String url = metrcAuthorization.getHost() + "/rooms/v1/" + id;
        return SimpleRestUtil.get(url, MetricsRoom.class, metrcAuthorization.makeHeaders());
    }


    public MetricsRoomList getActiveRooms(String licenseNumber) {
        String url = metrcAuthorization.getHost() + "/rooms/v1/active?licenseNumber=" + licenseNumber;
        return SimpleRestUtil.get(url, MetricsRoomList.class, metrcAuthorization.makeHeaders());
    }


    public String createRooms(List<MetricsRoom> metricsRooms, String licenseNumber) throws InterruptedException, ExecutionException, IOException {
        String url = metrcAuthorization.getHost() + "/rooms/v1/create?licenseNumber=" + licenseNumber;
        return SimpleRestUtil.post(url, metricsRooms, String.class, MetricsErrorList.class, metrcAuthorization.makeHeaders());
    }


    public String updateRooms(List<MetricsRoom> metricsRooms, String licenseNumber) throws InterruptedException, ExecutionException, IOException {
        String url = metrcAuthorization.getHost() + "/rooms/v1/update?licenseNumber=" + licenseNumber;
        return SimpleRestUtil.post(url, metricsRooms, String.class, MetricsErrorList.class, metrcAuthorization.makeHeaders());
    }


    public String deleteRoom(String id, String licenseNumber) {
        String url = metrcAuthorization.getHost() + "/rooms/v1/" + id + "?licenseNumber=" + licenseNumber;
        return SimpleRestUtil.delete(url, String.class, metrcAuthorization.makeHeaders());
    }
}
