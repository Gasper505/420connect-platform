package com.blaze.clients.metrcs;

import com.blaze.clients.metrcs.models.MetricsErrorList;
import com.blaze.clients.metrcs.models.sales.*;
import com.mdo.pusher.SimpleRestUtil;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by mdo on 7/18/17.
 */
public class MetricsSalesAPIService extends MetrcBaseService {

    public MetricsSalesAPIService(MetrcAuthorization metrcAuthorization) {
        super(metrcAuthorization);
    }


    public List<String> getSaleCustomerTypes() {
        String url = metrcAuthorization.getHost() + "/sales/v1/customertypes";
        return SimpleRestUtil.get(url, List.class, String.class, metrcAuthorization.makeHeaders());
    }


    public MetricsSaleList getSaleDeliveries(String licenseNumber) {
        String url = metrcAuthorization.getHost() + "/sales/v1/deliveries?licenseNumber=" + licenseNumber;
        return SimpleRestUtil.get(url, MetricsSaleList.class, metrcAuthorization.makeHeaders());
    }


    public MetricsSale getSaleDelivery(String id) {
        String url = metrcAuthorization.getHost() + "/sales/v1/delivery/" + id;
        return SimpleRestUtil.get(url, MetricsSale.class, metrcAuthorization.makeHeaders());
    }


    public MetricsSaleReturnReasonList getSaleDeliveryReturnReasons() {
        String url = metrcAuthorization.getHost() + "/sales/v1/delivery/returnreasons";
        return SimpleRestUtil.get(url, MetricsSaleReturnReasonList.class, metrcAuthorization.makeHeaders());
    }


    public String createSaleDeliveries(List<MetricsSaleDelivery> metricsSaleDeliveries, String licenseNumber) throws InterruptedException, ExecutionException, IOException {
        String url = metrcAuthorization.getHost() + "/sales/v1/deliveries?licenseNumber=" + licenseNumber;
        return SimpleRestUtil.post(url, metricsSaleDeliveries, String.class, MetricsErrorList.class, metrcAuthorization.makeHeaders());
    }


    public String updateSaleDeliveries(List<MetricsSaleDelivery> metricsSaleDeliveries, String licenseNumber) throws InterruptedException, ExecutionException, IOException {
        String url = metrcAuthorization.getHost() + "/sales/v1/deliveries?licenseNumber=" + licenseNumber;
        return SimpleRestUtil.put(url, metricsSaleDeliveries, String.class, metrcAuthorization.makeHeaders());
    }


    public String completeDeliveries(List<MetricsDeliveryComplete> metricsDeliveryCompletes, String licenseNumber) throws InterruptedException, ExecutionException, IOException {
        String url = metrcAuthorization.getHost() + "/sales/v1/deliveries/complete?licenseNumber=" + licenseNumber;
        return SimpleRestUtil.put(url, metricsDeliveryCompletes, String.class, metrcAuthorization.makeHeaders());
    }


    public String deleteDelivery(String licenseNumber) {
        String url = metrcAuthorization.getHost() + "/sales/v1/delivery/1?licenseNumber=" + licenseNumber;
        return SimpleRestUtil.delete(url, String.class, metrcAuthorization.makeHeaders());
    }


    public MetricsReceiptList getSaleReceipts(String licenseNumber, String lastModifiedStartDate) {
        String url = metrcAuthorization.getHost() + "/sales/v1/receipts?licenseNumber=" + licenseNumber + "&lastModifiedStart=" + lastModifiedStartDate;
        return SimpleRestUtil.get(url, MetricsReceiptList.class, metrcAuthorization.makeHeaders());
    }


    public MetricsReceipt getSaleReceipt(String id) {
        String url = metrcAuthorization.getHost() + "/sales/v1/receipts/" + id;
        return SimpleRestUtil.get(url, MetricsReceipt.class, metrcAuthorization.makeHeaders());
    }


    public String createSaleReceipts(List<MetricsSaleReceipt> metricsSaleReceipts, String licenseNumber) throws InterruptedException, ExecutionException, IOException {
        String url = metrcAuthorization.getHost() + "/sales/v1/receipts?licenseNumber=" + licenseNumber;
        return SimpleRestUtil.post(url, metricsSaleReceipts, String.class, MetricsErrorList.class, metrcAuthorization.makeHeaders());
    }


    public String updateSaleReceipts(List<MetricsSaleReceipt> metricsSaleReceipts, String licenseNumber) throws InterruptedException, ExecutionException, IOException {
        String url = metrcAuthorization.getHost() + "/sales/v1/receipts?licenseNumber=" + licenseNumber;
        return SimpleRestUtil.put(url, metricsSaleReceipts, String.class, MetricsErrorList.class, metrcAuthorization.makeHeaders());
    }


    public String deleteSaleReceipt(String id, String licenseNumber) {
        String url = metrcAuthorization.getHost() + "/sales/v1/receipts/" + id + "?licenseNumber=" + licenseNumber;
        return SimpleRestUtil.delete(url, String.class, metrcAuthorization.makeHeaders());
    }


    public MetricsSaleTransactionList getSaleTransactions(String licenseNumber) {
        String url = metrcAuthorization.getHost() + "/sales/v1/transactions?licenseNumber=" + licenseNumber;
        return SimpleRestUtil.get(url, MetricsSaleTransactionList.class, metrcAuthorization.makeHeaders());
    }


    public MetricsSaleTransactionList getSaleTransactionsByDate(String date, String licenseNumber) {
        String url = metrcAuthorization.getHost() + "/sales/v1/transactions/" + date + "?licenseNumber=" + licenseNumber;
        return SimpleRestUtil.get(url, MetricsSaleTransactionList.class, metrcAuthorization.makeHeaders());
    }


    public String createSaleTransactionsByDate(List<MetricsSaleTransaction> metricsSaleTransactions, String date, String licenseNumber) throws InterruptedException, ExecutionException, IOException {
        String url = metrcAuthorization.getHost() + "/sales/v1/transactions/" + date + "?licenseNumber=" + licenseNumber;
        return SimpleRestUtil.post(url, metricsSaleTransactions, String.class, String.class, metrcAuthorization.makeHeaders());
    }


    public String updateSaleTransactionsByDate(List<MetricsSaleTransaction> metricsSaleTransactions, String date, String licenseNumber) throws InterruptedException, ExecutionException, IOException {
        String url = metrcAuthorization.getHost() + "/sales/v1/transactions/" + date + "?licenseNumber=" + licenseNumber;
        return SimpleRestUtil.put(url, metricsSaleTransactions, String.class, metrcAuthorization.makeHeaders());
    }
}
