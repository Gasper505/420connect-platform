package com.blaze.clients.metrcs;

import com.blaze.clients.metrcs.models.MetricsErrorList;
import com.blaze.clients.metrcs.models.strains.MetricsStrain;
import com.blaze.clients.metrcs.models.strains.MetricsStrainList;
import com.google.gson.JsonSerializer;
import com.mdo.pusher.SimpleRestUtil;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by mdo on 7/18/17.
 */
public class MetricsStrainsAPIService extends MetrcBaseService {

    public MetricsStrainsAPIService(MetrcAuthorization metrcAuthorization) {
        super(metrcAuthorization);
    }


    public MetricsStrain getStrain(String id) {
        String url = metrcAuthorization.getHost() + "/strains/v1/" + id;
        return SimpleRestUtil.get(url, MetricsStrain.class, metrcAuthorization.makeHeaders());
    }


    public MetricsStrainList getActiveStrains(String licenseNumber) {
        String url = metrcAuthorization.getHost() + "/strains/v1/active?licenseNumber=" + licenseNumber;
        return SimpleRestUtil.get(url, MetricsStrainList.class, metrcAuthorization.makeHeaders());
    }


    public String createStrains(List<MetricsStrain> metricsStrains, String licenseNumber) throws InterruptedException, ExecutionException, IOException {
        String url = metrcAuthorization.getHost() + "/strains/v1/create?licenseNumber=" + licenseNumber;
        return SimpleRestUtil.post(url, metricsStrains, String.class, MetricsErrorList.class, metrcAuthorization.makeHeaders());
    }


    public String updateStrains(List<MetricsStrain> metricsStrains, String licenseNumber) throws InterruptedException, ExecutionException, IOException {
        String url = metrcAuthorization.getHost() + "/strains/v1/update?licenseNumber=" + licenseNumber;
        return SimpleRestUtil.post(url, metricsStrains, String.class, MetricsErrorList.class, metrcAuthorization.makeHeaders());
    }


    public String deleteStrain(String id, String licenseNumber) {
        String url = metrcAuthorization.getHost() + "/strains/v1/1?licenseNumber=" + licenseNumber;
        return SimpleRestUtil.delete(url, String.class, metrcAuthorization.makeHeaders());
    }
}
