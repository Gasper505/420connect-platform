package com.blaze.clients.metrcs;

import com.blaze.clients.metrcs.models.MetricsErrorList;
import com.blaze.clients.metrcs.models.transfers.ManifestMetricsTransferTemplate;
import com.blaze.clients.metrcs.models.transfers.MetricsTransferList;
import com.blaze.clients.metrcs.models.transfers.MetricsTransferPackageList;
import com.blaze.clients.metrcs.models.transfers.MetricsTransferTemplate;
import com.mdo.pusher.SimpleRestUtil;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by mdo on 7/18/17.
 */
public class MetricsTransferAPIService extends MetrcBaseService {

    public MetricsTransferAPIService(MetrcAuthorization metrcAuthorization) {
        super(metrcAuthorization);
    }


    public MetricsTransferList getIncomingTransfers(String licenseNumber) {
        String url = metrcAuthorization.getHost() + "/transfers/v1/incoming?licenseNumber=" + licenseNumber;
        return SimpleRestUtil.get(url, MetricsTransferList.class, metrcAuthorization.makeHeaders());
    }


    public MetricsTransferList getOutgoingTransfers(String licenseNumber) {
        String url = metrcAuthorization.getHost() + "/transfers/v1/outgoing?licenseNumber=" + licenseNumber;
        return SimpleRestUtil.get(url, MetricsTransferList.class, metrcAuthorization.makeHeaders());
    }


    public MetricsTransferList getRejectTransfers(String licenseNumber) {
        String url = metrcAuthorization.getHost() + "/transfers/v1/rejected?licenseNumber=" + licenseNumber;
        return SimpleRestUtil.get(url, MetricsTransferList.class, metrcAuthorization.makeHeaders());
    }

    public MetricsTransferList getTransfersByStatus(String licenseNumber, String status) {
        String url = String.format("%s/transfers/v1/%s?licenseNumber",metrcAuthorization.getHost(),status.toLowerCase(),licenseNumber);
        return SimpleRestUtil.get(url, MetricsTransferList.class, metrcAuthorization.makeHeaders());
    }




    public MetricsTransferPackageList getTransferDeliveriesById(String id) {
        String url = metrcAuthorization.getHost() + "/transfers/v1/" + id + "/deliveries";
        return SimpleRestUtil.get(url, MetricsTransferPackageList.class, metrcAuthorization.makeHeaders());
    }


    public MetricsTransferPackageList getTransferDeliveriesPackagesById(String id) {
        String url = metrcAuthorization.getHost() + "/transfers/v1/delivery/" + id + "/packages";
        return SimpleRestUtil.get(url, MetricsTransferPackageList.class, metrcAuthorization.makeHeaders());
    }


    public List<String> getTransferDeliveriesPackagesStates() {
        String url = metrcAuthorization.getHost() + "/transfers/v1/delivery/packages/states";
        return SimpleRestUtil.get(url, List.class, metrcAuthorization.makeHeaders());
    }

    public String createTransferTemplate(List<MetricsTransferTemplate> transferTemplates) throws InterruptedException, ExecutionException, IOException {
        String url = metrcAuthorization.getHost() + "/transfers/v1/templates";
        return SimpleRestUtil.put(url, transferTemplates, String.class, MetricsErrorList.class, metrcAuthorization.makeHeaders());
    }

    public String createManifestTransferTemplate(List<ManifestMetricsTransferTemplate> transferTemplates, String licenseNumber) throws InterruptedException, ExecutionException, IOException {
        String url = String.format("%s/transfers/v1/templates?licenseNumber=%s",metrcAuthorization.getHost(), licenseNumber);
        return SimpleRestUtil.post(url, transferTemplates, String.class, String.class, metrcAuthorization.makeHeaders());
    }

    public String getManifestTransferTemplatesByDate(String lastModifiedStart, String lastModifiedEnd) {
        String url = String.format("%s/transfers/v1/templates?licenseNumber=%s&lastModifiedStart=%s&lastModifiedEnd=%s",metrcAuthorization.getHost(), metrcAuthorization.getFacilityLicense(), lastModifiedStart, lastModifiedEnd);
        return SimpleRestUtil.get(url, String.class, MetricsErrorList.class, metrcAuthorization.makeHeaders());
    }

    public String updateManifestTransferTemplate(List<ManifestMetricsTransferTemplate> transferTemplates) throws InterruptedException, ExecutionException, IOException {
        String url = String.format("%s/transfers/v1/templates?licenseNumber=%s",metrcAuthorization.getHost(), metrcAuthorization.getFacilityLicense());
        return SimpleRestUtil.put(url, transferTemplates, String.class, MetricsErrorList.class, metrcAuthorization.makeHeaders());
    }

}
