package com.blaze.clients.metrcs.configs;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;

/**
 * Created by mdo on 8/24/17.
 * <p>
 * <p>
 * private String host = "https://sandbox-api-or.metrc.com";
 * <p>
 * private String vendorApiKey = "uRMSKuL0P1NaPkaYjykU5q0u1rcp7UG6yu02EqKoXgfptABW";
 * private String userApiKey = "4ABk0xpkhATaOhFgB81dses94sRCBA2jqV5xrAwX9fpUPZO6";
 */
public class MetrcAPIConfig {
    public static final String TEST_USER_API_KEY = "4ABk0xpkhATaOhFgB81dses94sRCBA2jqV5xrAwX9fpUPZO6";
    public static final String TEST_VENDOR_API_KEY = "uRMSKuL0P1NaPkaYjykU5q0u1rcp7UG6yu02EqKoXgfptABW";

    public enum MetrcState {
        ALASKA("AK"),
        COLORADO("CO"),
        CALIFORNIA("CA"),
        OREGON("OR"),
        NEVADA("NV"),
        MICHIGAN("MI"),
        MARYLAND("MD"),
        MONTANA("MT");


        MetrcState(String stateCode) {
            this.stateCode = stateCode;
        }

        public String stateCode;

    }

    @Valid
    @NotEmpty
    private String apiURL = "https://sandbox-api-or.metrc.com";
    @Valid
    @NotEmpty
    private String apiKey = "uRMSKuL0P1NaPkaYjykU5q0u1rcp7UG6yu02EqKoXgfptABW";
    @Valid
    @NotEmpty
    private MetrcState state = MetrcState.OREGON;


    public String getApiURL() {
        return apiURL;
    }

    public void setApiURL(String apiURL) {
        this.apiURL = apiURL;
    }

    public MetrcState getState() {
        return state;
    }

    public void setState(MetrcState state) {
        this.state = state;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }
}
