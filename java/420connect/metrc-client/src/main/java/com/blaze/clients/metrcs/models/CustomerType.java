package com.blaze.clients.metrcs.models;

/**
 * Created by mdo on 10/9/17.
 *
 * [
 *   "Consumer",
 *   "Patient",
 *   "Caregiver",
 *   "ExternalPatient"
 * ]
 *
 */
public enum CustomerType {
    Consumer("Consumer"),
    Patient("Patient"),
    Caregiver("Caregiver"),
    ExternalPatient("ExternalPatient");

    CustomerType(String metrcValue) {
        this.metrcValue = metrcValue;
    }

    private String metrcValue;

    public String getMetrcValue() {
        return metrcValue;
    }

    public void setMetrcValue(String metrcValue) {
        this.metrcValue = metrcValue;
    }
}
