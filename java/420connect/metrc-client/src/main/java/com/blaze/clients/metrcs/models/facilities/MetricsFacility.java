package com.blaze.clients.metrcs.models.facilities;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by Gaurav Saini on 23/8/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY,
        getterVisibility = JsonAutoDetect.Visibility.NONE,
        setterVisibility = JsonAutoDetect.Visibility.NONE)
public class MetricsFacility {
    private String HireDate;
    private String HomePage;
    @JsonProperty("IsOwner")
    private boolean IsOwner;
    @JsonProperty("IsManager")
    private boolean IsManager;
    private List Occupations;
    private String Name;
    private String Alias;
    private String DisplayName;
    private String SupportActivationDate;
    private String SupportExpirationDate;
    private String SupportLastPaidDate;
    private MetricsLicense License;

    public String getHireDate() {
        return HireDate;
    }

    public void setHireDate(String hireDate) {
        HireDate = hireDate;
    }

    public String getHomePage() {
        return HomePage;
    }

    public void setHomePage(String homePage) {
        HomePage = homePage;
    }

    @JsonIgnore
    public boolean isOwner() {
        return IsOwner;
    }

    public void setOwner(boolean owner) {
        IsOwner = owner;
    }

    public boolean isManager() {
        return IsManager;
    }

    @JsonIgnore
    public void setManager(boolean manager) {
        IsManager = manager;
    }

    public List getOccupations() {
        return Occupations;
    }

    public void setOccupations(List occupations) {
        Occupations = occupations;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getAlias() {
        return Alias;
    }

    public void setAlias(String alias) {
        Alias = alias;
    }

    public String getDisplayName() {
        return DisplayName;
    }

    public void setDisplayName(String displayName) {
        DisplayName = displayName;
    }

    public String getSupportActivationDate() {
        return SupportActivationDate;
    }

    public void setSupportActivationDate(String supportActivationDate) {
        SupportActivationDate = supportActivationDate;
    }

    public String getSupportExpirationDate() {
        return SupportExpirationDate;
    }

    public void setSupportExpirationDate(String supportExpirationDate) {
        SupportExpirationDate = supportExpirationDate;
    }

    public String getSupportLastPaidDate() {
        return SupportLastPaidDate;
    }

    public void setSupportLastPaidDate(String supportLastPaidDate) {
        SupportLastPaidDate = supportLastPaidDate;
    }

    public MetricsLicense getLicense() {
        return License;
    }

    public void setLicense(MetricsLicense license) {
        License = license;
    }

}
