package com.blaze.clients.metrcs.models.facilities;

import com.blaze.clients.metrcs.serializers.MetrcsLicenseDeserializer;
import com.blaze.clients.metrcs.serializers.MetrcsLicenseSerializer;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Created by Gaurav Saini on 23/8/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY,
        getterVisibility = JsonAutoDetect.Visibility.NONE,
        setterVisibility = JsonAutoDetect.Visibility.NONE)
public class MetricsLicense {
    public enum FacilityLicenseType {
        Unknown("Unknown"),
        RecreationalWholesaler("Recreational Wholesaler"),
        RecreationalProducer("Recreational Producer"),
        RecreationalRetailer("Recreational Retailer"),
        RecreationalProcessor("Recreational Processor"),
        Laboratory("Laboratory"),
        ResearchCertificate("Research Certificate");

        FacilityLicenseType(String displayName) {
            this.displayName = displayName;
        }

        public static FacilityLicenseType toLicenseType(String licenseValue) {
            FacilityLicenseType[] types = FacilityLicenseType.values();
            for (FacilityLicenseType type : types) {
                if (type.displayName.equalsIgnoreCase(licenseValue)) {
                    return type;
                }
            }
            FacilityLicenseType license = FacilityLicenseType.Unknown;
            license.displayName = licenseValue;
            return license;
        }
        public String displayName;
    }

    private String Number;
    private String StartDate;
    private String EndDate;
    //@JsonDeserialize(using = MetrcsLicenseDeserializer.class)
    //@JsonSerialize(using = MetrcsLicenseSerializer.class)
    private String LicenseType = "Unknown";

    public String getNumber() {
        return Number;
    }

    public void setNumber(String number) {
        Number = number;
    }

    public String getStartDate() {
        return StartDate;
    }

    public void setStartDate(String startDate) {
        StartDate = startDate;
    }

    public String getEndDate() {
        return EndDate;
    }

    public void setEndDate(String endDate) {
        EndDate = endDate;
    }

    public String getLicenseType() {
        return LicenseType;
    }

    public void setLicenseType(String licenseType) {
        LicenseType = licenseType;
    }
}
