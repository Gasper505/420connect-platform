package com.blaze.clients.metrcs.models.harvests;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

/**
 * Created by mdo on 7/18/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY,
        getterVisibility = JsonAutoDetect.Visibility.NONE,
        setterVisibility = JsonAutoDetect.Visibility.NONE)
public class MetricsHarvest {

    private int Id;
    private String Name;
    private String HarvestType;
    private int DryingRoomId;
    private String DryingRoomName;
    private double CurrentWeight;
    private double TotalWasteWeight;
    private int PlantCount;
    private double TotalWetWeight;
    private int PackageCount;
    private double TotalPackagedWeight;
    private String UnitOfWeightName;
    private String LabTestingState;
    private String LabTestingStateDate;
    private boolean IsOnHold;
    private String HarvestStartDate;
    private String FinishedDate;
    private String ArchivedDate;
    private String LastModified;
    private List<String> Strains;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getHarvestType() {
        return HarvestType;
    }

    public void setHarvestType(String harvestType) {
        HarvestType = harvestType;
    }

    public int getDryingRoomId() {
        return DryingRoomId;
    }

    public void setDryingRoomId(int dryingRoomId) {
        DryingRoomId = dryingRoomId;
    }

    public String getDryingRoomName() {
        return DryingRoomName;
    }

    public void setDryingRoomName(String dryingRoomName) {
        DryingRoomName = dryingRoomName;
    }

    public double getCurrentWeight() {
        return CurrentWeight;
    }

    public void setCurrentWeight(double currentWeight) {
        CurrentWeight = currentWeight;
    }

    public double getTotalWasteWeight() {
        return TotalWasteWeight;
    }

    public void setTotalWasteWeight(double totalWasteWeight) {
        TotalWasteWeight = totalWasteWeight;
    }

    public int getPlantCount() {
        return PlantCount;
    }

    public void setPlantCount(int plantCount) {
        PlantCount = plantCount;
    }

    public double getTotalWetWeight() {
        return TotalWetWeight;
    }

    public void setTotalWetWeight(double totalWetWeight) {
        TotalWetWeight = totalWetWeight;
    }

    public int getPackageCount() {
        return PackageCount;
    }

    public void setPackageCount(int packageCount) {
        PackageCount = packageCount;
    }

    public double getTotalPackagedWeight() {
        return TotalPackagedWeight;
    }

    public void setTotalPackagedWeight(double totalPackagedWeight) {
        TotalPackagedWeight = totalPackagedWeight;
    }

    public String getUnitOfWeightName() {
        return UnitOfWeightName;
    }

    public void setUnitOfWeightName(String unitOfWeightName) {
        UnitOfWeightName = unitOfWeightName;
    }

    public String getLabTestingState() {
        return LabTestingState;
    }

    public void setLabTestingState(String labTestingState) {
        LabTestingState = labTestingState;
    }

    public String getLabTestingStateDate() {
        return LabTestingStateDate;
    }

    public void setLabTestingStateDate(String labTestingStateDate) {
        LabTestingStateDate = labTestingStateDate;
    }

    public boolean isOnHold() {
        return IsOnHold;
    }

    public void setOnHold(boolean onHold) {
        IsOnHold = onHold;
    }

    public String getHarvestStartDate() {
        return HarvestStartDate;
    }

    public void setHarvestStartDate(String harvestStartDate) {
        HarvestStartDate = harvestStartDate;
    }

    public String getFinishedDate() {
        return FinishedDate;
    }

    public void setFinishedDate(String finishedDate) {
        FinishedDate = finishedDate;
    }

    public String getArchivedDate() {
        return ArchivedDate;
    }

    public void setArchivedDate(String archivedDate) {
        ArchivedDate = archivedDate;
    }

    public String getLastModified() {
        return LastModified;
    }

    public void setLastModified(String lastModified) {
        LastModified = lastModified;
    }

    public List<String> getStrains() {
        return Strains;
    }

    public void setStrains(List<String> strains) {
        Strains = strains;
    }
}
