package com.blaze.clients.metrcs.models.harvests;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Gaurav Saini on 19/7/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY,
        getterVisibility = JsonAutoDetect.Visibility.NONE,
        setterVisibility = JsonAutoDetect.Visibility.NONE)
public class MetricsHarvestPackage {

    private int Harvest;
    private String Item;
    private double Weight;
    private String UnitOfWeight;
    private String Tag;
    private boolean IsProductionBatch;
    private String ProductionBatchNumber;
    private boolean ProductRequiresRemediation;
    private String ActualDate;

    public int getHarvest() {
        return Harvest;
    }

    public void setHarvest(int harvest) {
        Harvest = harvest;
    }

    public String getItem() {
        return Item;
    }

    public void setItem(String item) {
        Item = item;
    }

    public double getWeight() {
        return Weight;
    }

    public void setWeight(double weight) {
        Weight = weight;
    }

    public String getUnitOfWeight() {
        return UnitOfWeight;
    }

    public void setUnitOfWeight(String unitOfWeight) {
        UnitOfWeight = unitOfWeight;
    }

    public String getTag() {
        return Tag;
    }

    public void setTag(String tag) {
        Tag = tag;
    }

    public boolean isProductionBatch() {
        return IsProductionBatch;
    }

    public void setProductionBatch(boolean productionBatch) {
        IsProductionBatch = productionBatch;
    }

    public String getProductionBatchNumber() {
        return ProductionBatchNumber;
    }

    public void setProductionBatchNumber(String productionBatchNumber) {
        ProductionBatchNumber = productionBatchNumber;
    }

    public boolean isProductRequiresRemediation() {
        return ProductRequiresRemediation;
    }

    public void setProductRequiresRemediation(boolean productRequiresRemediation) {
        ProductRequiresRemediation = productRequiresRemediation;
    }

    public String getActualDate() {
        return ActualDate;
    }

    public void setActualDate(String actualDate) {
        ActualDate = actualDate;
    }
}
