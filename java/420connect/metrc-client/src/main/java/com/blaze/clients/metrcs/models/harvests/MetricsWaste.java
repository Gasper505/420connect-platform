package com.blaze.clients.metrcs.models.harvests;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Gaurav Saini on 19/7/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY,
        getterVisibility = JsonAutoDetect.Visibility.NONE,
        setterVisibility = JsonAutoDetect.Visibility.NONE)
public class MetricsWaste {

    private int Id;
    private String UnitOfWeight;
    private double WasteWeight;
    private String ActualDate;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getUnitOfWeight() {
        return UnitOfWeight;
    }

    public void setUnitOfWeight(String unitOfWeight) {
        UnitOfWeight = unitOfWeight;
    }

    public double getWasteWeight() {
        return WasteWeight;
    }

    public void setWasteWeight(double wasteWeight) {
        WasteWeight = wasteWeight;
    }

    public String getActualDate() {
        return ActualDate;
    }

    public void setActualDate(String actualDate) {
        ActualDate = actualDate;
    }
}
