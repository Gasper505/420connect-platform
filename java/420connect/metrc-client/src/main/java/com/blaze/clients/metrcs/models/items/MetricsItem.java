package com.blaze.clients.metrcs.models.items;

import com.blaze.clients.metrcs.serializers.BigDecimalQuantitySerializer;
import com.blaze.common.data.compliance.ComplianceItemInfo;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Created by Gaurav Saini on 19/7/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY,
        getterVisibility = JsonAutoDetect.Visibility.NONE,
        setterVisibility = JsonAutoDetect.Visibility.NONE)
public class MetricsItem extends ComplianceItemInfo {

    private int Id;
    private String ItemCategory;
    private String Name;
    private String UnitOfMeasure;
    private String Strain;
    private String ItemBrand;
    private String DefaultLabTestingState;
    private boolean IsUsed = false;
    private String ApprovalStatus;

    private String AdministrationMethod;

    @JsonSerialize(using = BigDecimalQuantitySerializer.class)
    private BigDecimal UnitQuantity;
    private String UnitQuantityUnitOfMeasure;

    @JsonSerialize(using = BigDecimalQuantitySerializer.class)
    private BigDecimal UnitCbdPercent;
    @JsonSerialize(using = BigDecimalQuantitySerializer.class)
    private BigDecimal UnitCbdContent;
    private String UnitCbdContentUnitOfMeasure;


    @JsonSerialize(using = BigDecimalQuantitySerializer.class)
    private BigDecimal UnitThcPercent;
    @JsonSerialize(using = BigDecimalQuantitySerializer.class)
    private BigDecimal UnitThcContent;
    private String UnitThcContentUnitOfMeasure;

    @JsonSerialize(using = BigDecimalQuantitySerializer.class)
    private BigDecimal UnitVolume;
    private String UnitVolumeUnitOfMeasure;

    @JsonSerialize(using = BigDecimalQuantitySerializer.class)
    private BigDecimal UnitWeight;
    private String UnitWeightUnitOfMeasure;



    private String ServingSize;
    private String SupplyDurationDays;
    private String Ingredients;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MetricsItem that = (MetricsItem) o;
        return Name.equals(that.Name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(Name);
    }

    public String getUnitQuantityUnitOfMeasure() {
        return UnitQuantityUnitOfMeasure;
    }

    public void setUnitQuantityUnitOfMeasure(String unitQuantityUnitOfMeasure) {
        UnitQuantityUnitOfMeasure = unitQuantityUnitOfMeasure;
    }

    public String getDefaultLabTestingState() {
        return DefaultLabTestingState;
    }

    public void setDefaultLabTestingState(String defaultLabTestingState) {
        DefaultLabTestingState = defaultLabTestingState;
    }

    public boolean isUsed() {
        return IsUsed;
    }

    public void setUsed(boolean used) {
        IsUsed = used;
    }

    public String getApprovalStatus() {
        return ApprovalStatus;
    }

    public void setApprovalStatus(String approvalStatus) {
        ApprovalStatus = approvalStatus;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public BigDecimal getUnitQuantity() {
        return UnitQuantity;
    }

    public void setUnitQuantity(BigDecimal unitQuantity) {
        UnitQuantity = unitQuantity;
    }

    // and then "other" stuff:
    protected Map<String,Object> other = new HashMap<String,Object>();

    // "any getter" needed for serialization
    @JsonAnyGetter
    public Map<String,Object> any() {
        return other;
    }

    @JsonAnySetter
    public void set(String name, Object value) {
        other.put(name, value);
    }

    public String getItemCategory() {
        return ItemCategory;
    }

    public void setItemCategory(String itemCategory) {
        ItemCategory = itemCategory;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getUnitOfMeasure() {
        return UnitOfMeasure;
    }

    public void setUnitOfMeasure(String unitOfMeasure) {
        UnitOfMeasure = unitOfMeasure;
    }

    public String getStrain() {
        return Strain;
    }

    public void setStrain(String strain) {
        Strain = strain;
    }

    public String getItemBrand() {
        return ItemBrand;
    }

    public void setItemBrand(String itemBrand) {
        ItemBrand = itemBrand;
    }

    public String getAdministrationMethod() {
        return AdministrationMethod;
    }

    public void setAdministrationMethod(String administrationMethod) {
        AdministrationMethod = administrationMethod;
    }

    public BigDecimal getUnitCbdPercent() {
        return UnitCbdPercent;
    }

    public void setUnitCbdPercent(BigDecimal unitCbdPercent) {
        UnitCbdPercent = unitCbdPercent;
    }

    public BigDecimal getUnitCbdContent() {
        return UnitCbdContent;
    }

    public void setUnitCbdContent(BigDecimal unitCbdContent) {
        UnitCbdContent = unitCbdContent;
    }

    public String getUnitCbdContentUnitOfMeasure() {
        return UnitCbdContentUnitOfMeasure;
    }

    public void setUnitCbdContentUnitOfMeasure(String unitCbdContentUnitOfMeasure) {
        UnitCbdContentUnitOfMeasure = unitCbdContentUnitOfMeasure;
    }

    public BigDecimal getUnitThcPercent() {
        return UnitThcPercent;
    }

    public void setUnitThcPercent(BigDecimal unitThcPercent) {
        UnitThcPercent = unitThcPercent;
    }

    public BigDecimal getUnitThcContent() {
        return UnitThcContent;
    }

    public void setUnitThcContent(BigDecimal unitThcContent) {
        UnitThcContent = unitThcContent;
    }

    public String getUnitThcContentUnitOfMeasure() {
        return UnitThcContentUnitOfMeasure;
    }

    public void setUnitThcContentUnitOfMeasure(String unitThcContentUnitOfMeasure) {
        UnitThcContentUnitOfMeasure = unitThcContentUnitOfMeasure;
    }

    public BigDecimal getUnitVolume() {
        return UnitVolume;
    }

    public void setUnitVolume(BigDecimal unitVolume) {
        UnitVolume = unitVolume;
    }

    public String getUnitVolumeUnitOfMeasure() {
        return UnitVolumeUnitOfMeasure;
    }

    public void setUnitVolumeUnitOfMeasure(String unitVolumeUnitOfMeasure) {
        UnitVolumeUnitOfMeasure = unitVolumeUnitOfMeasure;
    }

    public BigDecimal getUnitWeight() {
        return UnitWeight;
    }

    public void setUnitWeight(BigDecimal unitWeight) {
        UnitWeight = unitWeight;
    }

    public String getUnitWeightUnitOfMeasure() {
        return UnitWeightUnitOfMeasure;
    }

    public void setUnitWeightUnitOfMeasure(String unitWeightUnitOfMeasure) {
        UnitWeightUnitOfMeasure = unitWeightUnitOfMeasure;
    }

    public String getServingSize() {
        return ServingSize;
    }

    public void setServingSize(String servingSize) {
        ServingSize = servingSize;
    }

    public String getSupplyDurationDays() {
        return SupplyDurationDays;
    }

    public void setSupplyDurationDays(String supplyDurationDays) {
        SupplyDurationDays = supplyDurationDays;
    }

    public String getIngredients() {
        return Ingredients;
    }

    public void setIngredients(String ingredients) {
        Ingredients = ingredients;
    }

    public Map<String, Object> getOther() {
        return other;
    }

    public void setOther(Map<String, Object> other) {
        this.other = other;
    }
}
