package com.blaze.clients.metrcs.models.items;

import com.blaze.common.data.compliance.ComplianceCategoryInfo;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Gaurav Saini on 22/8/17.
 * "RequiresStrain": false,
 *     "RequiresItemBrand": false,
 *     "RequiresAdministrationMethod": false,
 *     "RequiresUnitCbdPercent": false,
 *     "RequiresUnitCbdContent": false,
 *     "RequiresUnitThcPercent": false,
 *     "RequiresUnitThcContent": true,
 *     "RequiresUnitVolume": false,
 *     "RequiresUnitWeight": true,
 *     "RequiresServingSize": false,
 *     "RequiresSupplyDurationDays": false,
 *     "RequiresIngredients": false,
 *     "RequiresProductPhoto": false,
 *     "CanContainSeeds": false,
 *     "CanBeRemediated": true
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY,
        getterVisibility = JsonAutoDetect.Visibility.NONE,
        setterVisibility = JsonAutoDetect.Visibility.NONE)
public class MetricsItemCategory extends ComplianceCategoryInfo {

    private String Name;
    private String ProductCategoryType;
    private String QuantityType;
    private boolean RequiresStrain;
    private boolean RequiresItemBrand;
    private boolean RequiresAdministrationMethod;
    private boolean RequiresUnitCbdPercent;
    private boolean RequiresUnitCbdContent;
    private boolean RequiresUnitThcPercent;
    private boolean RequiresUnitThcContent;
    private boolean RequiresUnitVolume;
    private boolean RequiresUnitWeight;
    private boolean RequiresServingSize;
    private boolean RequiresSupplyDurationDays;
    private boolean RequiresIngredients;
    private boolean RequiresProductPhoto;
    private boolean CanContainSeeds;
    private boolean CanBeRemediated;


    public boolean isRequiresItemBrand() {
        return RequiresItemBrand;
    }

    public void setRequiresItemBrand(boolean requiresItemBrand) {
        RequiresItemBrand = requiresItemBrand;
    }

    public boolean isRequiresAdministrationMethod() {
        return RequiresAdministrationMethod;
    }

    public void setRequiresAdministrationMethod(boolean requiresAdministrationMethod) {
        RequiresAdministrationMethod = requiresAdministrationMethod;
    }

    public boolean isRequiresUnitCbdPercent() {
        return RequiresUnitCbdPercent;
    }

    public void setRequiresUnitCbdPercent(boolean requiresUnitCbdPercent) {
        RequiresUnitCbdPercent = requiresUnitCbdPercent;
    }

    public boolean isRequiresUnitCbdContent() {
        return RequiresUnitCbdContent;
    }

    public void setRequiresUnitCbdContent(boolean requiresUnitCbdContent) {
        RequiresUnitCbdContent = requiresUnitCbdContent;
    }

    public boolean isRequiresUnitThcPercent() {
        return RequiresUnitThcPercent;
    }

    public void setRequiresUnitThcPercent(boolean requiresUnitThcPercent) {
        RequiresUnitThcPercent = requiresUnitThcPercent;
    }

    public boolean isRequiresUnitVolume() {
        return RequiresUnitVolume;
    }

    public void setRequiresUnitVolume(boolean requiresUnitVolume) {
        RequiresUnitVolume = requiresUnitVolume;
    }

    public boolean isRequiresServingSize() {
        return RequiresServingSize;
    }

    public void setRequiresServingSize(boolean requiresServingSize) {
        RequiresServingSize = requiresServingSize;
    }

    public boolean isRequiresSupplyDurationDays() {
        return RequiresSupplyDurationDays;
    }

    public void setRequiresSupplyDurationDays(boolean requiresSupplyDurationDays) {
        RequiresSupplyDurationDays = requiresSupplyDurationDays;
    }

    public boolean isRequiresIngredients() {
        return RequiresIngredients;
    }

    public void setRequiresIngredients(boolean requiresIngredients) {
        RequiresIngredients = requiresIngredients;
    }

    public boolean isRequiresProductPhoto() {
        return RequiresProductPhoto;
    }

    public void setRequiresProductPhoto(boolean requiresProductPhoto) {
        RequiresProductPhoto = requiresProductPhoto;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getProductCategoryType() {
        return ProductCategoryType;
    }

    public void setProductCategoryType(String productCategoryType) {
        ProductCategoryType = productCategoryType;
    }

    public String getQuantityType() {
        return QuantityType;
    }

    public void setQuantityType(String quantityType) {
        QuantityType = quantityType;
    }

    public boolean isRequiresStrain() {
        return RequiresStrain;
    }

    public void setRequiresStrain(boolean requiresStrain) {
        RequiresStrain = requiresStrain;
    }

    public boolean isRequiresUnitThcContent() {
        return RequiresUnitThcContent;
    }

    public void setRequiresUnitThcContent(boolean requiresUnitThcContent) {
        RequiresUnitThcContent = requiresUnitThcContent;
    }

    public boolean isRequiresUnitWeight() {
        return RequiresUnitWeight;
    }

    public void setRequiresUnitWeight(boolean requiresUnitWeight) {
        RequiresUnitWeight = requiresUnitWeight;
    }

    public boolean isCanContainSeeds() {
        return CanContainSeeds;
    }

    public void setCanContainSeeds(boolean canContainSeeds) {
        CanContainSeeds = canContainSeeds;
    }

    public boolean isCanBeRemediated() {
        return CanBeRemediated;
    }

    public void setCanBeRemediated(boolean canBeRemediated) {
        CanBeRemediated = canBeRemediated;
    }
}
