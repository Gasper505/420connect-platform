package com.blaze.clients.metrcs.models.labtests;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Gaurav Saini on 19/7/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY,
        getterVisibility = JsonAutoDetect.Visibility.NONE,
        setterVisibility = JsonAutoDetect.Visibility.NONE)
public class MetricsLabTestResults {
    private String LabTestTypeName;
    private double Quantity;
    private boolean Passed;
    private String Notes;

    public String getLabTestTypeName() {
        return LabTestTypeName;
    }

    public void setLabTestTypeName(String labTestTypeName) {
        LabTestTypeName = labTestTypeName;
    }

    public double getQuantity() {
        return Quantity;
    }

    public void setQuantity(double quantity) {
        Quantity = quantity;
    }

    public boolean isPassed() {
        return Passed;
    }

    public void setPassed(boolean passed) {
        Passed = passed;
    }

    public String getNotes() {
        return Notes;
    }

    public void setNotes(String notes) {
        Notes = notes;
    }
}
