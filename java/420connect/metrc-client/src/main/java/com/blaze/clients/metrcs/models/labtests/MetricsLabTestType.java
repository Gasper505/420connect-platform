package com.blaze.clients.metrcs.models.labtests;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Gaurav Saini on 22/8/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY,
        getterVisibility = JsonAutoDetect.Visibility.NONE,
        setterVisibility = JsonAutoDetect.Visibility.NONE)
public class MetricsLabTestType {

    private int Id;
    private String Name;
    private boolean RequiresTestResult;
    private boolean AlwaysPasses;
    private int DependencyMode;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public boolean isRequiresTestResult() {
        return RequiresTestResult;
    }

    public void setRequiresTestResult(boolean requiresTestResult) {
        RequiresTestResult = requiresTestResult;
    }

    public boolean isAlwaysPasses() {
        return AlwaysPasses;
    }

    public void setAlwaysPasses(boolean alwaysPasses) {
        AlwaysPasses = alwaysPasses;
    }

    public int getDependencyMode() {
        return DependencyMode;
    }

    public void setDependencyMode(int dependencyMode) {
        DependencyMode = dependencyMode;
    }
}
