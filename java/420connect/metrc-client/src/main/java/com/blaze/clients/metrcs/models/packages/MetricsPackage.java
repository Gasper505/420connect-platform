package com.blaze.clients.metrcs.models.packages;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

/**
 * Created by Gaurav Saini on 20/7/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY,
        getterVisibility = JsonAutoDetect.Visibility.NONE,
        setterVisibility = JsonAutoDetect.Visibility.NONE)
public class MetricsPackage {

    private String Tag;
    private String Item;
    private double Quantity;
    private String UnitOfMeasure;
    private boolean IsProductionBatch;
    private String ProductionBatchNumber;
    private boolean ProductRequiresRemediation;
    private String PatientLicenseNumber;
    private String ActualDate;
    private List<PackageIngredients> Ingredients;


    public String getPatientLicenseNumber() {
        return PatientLicenseNumber;
    }

    public void setPatientLicenseNumber(String patientLicenseNumber) {
        PatientLicenseNumber = patientLicenseNumber;
    }

    public String getTag() {
        return Tag;
    }

    public void setTag(String tag) {
        Tag = tag;
    }

    public String getItem() {
        return Item;
    }

    public void setItem(String item) {
        Item = item;
    }

    public double getQuantity() {
        return Quantity;
    }

    public void setQuantity(double quantity) {
        Quantity = quantity;
    }

    public String getUnitOfMeasure() {
        return UnitOfMeasure;
    }

    public void setUnitOfMeasure(String unitOfMeasure) {
        UnitOfMeasure = unitOfMeasure;
    }

    public boolean isProductionBatch() {
        return IsProductionBatch;
    }

    public void setProductionBatch(boolean productionBatch) {
        IsProductionBatch = productionBatch;
    }

    public String getProductionBatchNumber() {
        return ProductionBatchNumber;
    }

    public void setProductionBatchNumber(String productionBatchNumber) {
        ProductionBatchNumber = productionBatchNumber;
    }

    public boolean isProductRequiresRemediation() {
        return ProductRequiresRemediation;
    }

    public void setProductRequiresRemediation(boolean productRequiresRemediation) {
        ProductRequiresRemediation = productRequiresRemediation;
    }

    public String getActualDate() {
        return ActualDate;
    }

    public void setActualDate(String actualDate) {
        ActualDate = actualDate;
    }

    public List<PackageIngredients> getIngredients() {
        return Ingredients;
    }

    public void setIngredients(List<PackageIngredients> ingredients) {
        Ingredients = ingredients;
    }
}
