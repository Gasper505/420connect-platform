package com.blaze.clients.metrcs.models.packages;

import com.blaze.clients.metrcs.serializers.BigDecimalQuantityDeserializer;
import com.blaze.clients.metrcs.serializers.BigDecimalQuantitySerializer;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.math.BigDecimal;

/**
 * Created by Gaurav Saini on 20/7/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY,
        getterVisibility = JsonAutoDetect.Visibility.NONE,
        setterVisibility = JsonAutoDetect.Visibility.NONE)
public class MetricsPackageAdjustment {

    private String Label;
    @JsonSerialize(using = BigDecimalQuantitySerializer.class)
    @JsonDeserialize(using = BigDecimalQuantityDeserializer.class)
    private BigDecimal Quantity;
    private String UnitOfMeasure;
    private String AdjustmentReason;
    private String AdjustmentDate;
    private String ReasonNote;

    public String getLabel() {
        return Label;
    }

    public void setLabel(String label) {
        Label = label;
    }

    public BigDecimal getQuantity() {
        return Quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        Quantity = quantity;
    }

    public String getUnitOfMeasure() {
        return UnitOfMeasure;
    }

    public void setUnitOfMeasure(String unitOfMeasure) {
        UnitOfMeasure = unitOfMeasure;
    }

    public String getAdjustmentReason() {
        return AdjustmentReason;
    }

    public void setAdjustmentReason(String adjustmentReason) {
        AdjustmentReason = adjustmentReason;
    }

    public String getAdjustmentDate() {
        return AdjustmentDate;
    }

    public void setAdjustmentDate(String adjustmentDate) {
        AdjustmentDate = adjustmentDate;
    }

    public String getReasonNote() {
        return ReasonNote;
    }

    public void setReasonNote(String reasonNote) {
        ReasonNote = reasonNote;
    }
}
