package com.blaze.clients.metrcs.models.packages;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Gaurav Saini on 20/7/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY,
        getterVisibility = JsonAutoDetect.Visibility.NONE,
        setterVisibility = JsonAutoDetect.Visibility.NONE)
public class MetricsPackagePlanting {

    private String PackageLabel;
    private double PackageAdjustmentAmount;
    private String PackageAdjustmentUnitOfMeasureName;
    private String PlantBatchName;
    private String PlantBatchType;
    private double PlantCount;
    private String StrainName;
    private String PlantedDate;

    public String getPackageLabel() {
        return PackageLabel;
    }

    public void setPackageLabel(String packageLabel) {
        PackageLabel = packageLabel;
    }

    public double getPackageAdjustmentAmount() {
        return PackageAdjustmentAmount;
    }

    public void setPackageAdjustmentAmount(double packageAdjustmentAmount) {
        PackageAdjustmentAmount = packageAdjustmentAmount;
    }

    public String getPackageAdjustmentUnitOfMeasureName() {
        return PackageAdjustmentUnitOfMeasureName;
    }

    public void setPackageAdjustmentUnitOfMeasureName(String packageAdjustmentUnitOfMeasureName) {
        PackageAdjustmentUnitOfMeasureName = packageAdjustmentUnitOfMeasureName;
    }

    public String getPlantBatchName() {
        return PlantBatchName;
    }

    public void setPlantBatchName(String plantBatchName) {
        PlantBatchName = plantBatchName;
    }

    public String getPlantBatchType() {
        return PlantBatchType;
    }

    public void setPlantBatchType(String plantBatchType) {
        PlantBatchType = plantBatchType;
    }

    public double getPlantCount() {
        return PlantCount;
    }

    public void setPlantCount(double plantCount) {
        PlantCount = plantCount;
    }

    public String getStrainName() {
        return StrainName;
    }

    public void setStrainName(String strainName) {
        StrainName = strainName;
    }

    public String getPlantedDate() {
        return PlantedDate;
    }

    public void setPlantedDate(String plantedDate) {
        PlantedDate = plantedDate;
    }
}
