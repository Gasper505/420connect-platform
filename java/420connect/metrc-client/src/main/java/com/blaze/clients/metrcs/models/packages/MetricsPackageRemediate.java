package com.blaze.clients.metrcs.models.packages;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Gaurav Saini on 20/7/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY,
        getterVisibility = JsonAutoDetect.Visibility.NONE,
        setterVisibility = JsonAutoDetect.Visibility.NONE)
public class MetricsPackageRemediate {

    private String PackageLabel;
    private String RemediationMethodName;
    private String RemediationDate;
    private String RemediationSteps;

    public String getPackageLabel() {
        return PackageLabel;
    }

    public void setPackageLabel(String packageLabel) {
        PackageLabel = packageLabel;
    }

    public String getRemediationMethodName() {
        return RemediationMethodName;
    }

    public void setRemediationMethodName(String remediationMethodName) {
        RemediationMethodName = remediationMethodName;
    }

    public String getRemediationDate() {
        return RemediationDate;
    }

    public void setRemediationDate(String remediationDate) {
        RemediationDate = remediationDate;
    }

    public String getRemediationSteps() {
        return RemediationSteps;
    }

    public void setRemediationSteps(String remediationSteps) {
        RemediationSteps = remediationSteps;
    }
}
