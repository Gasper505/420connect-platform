package com.blaze.clients.metrcs.models.packages;

import com.blaze.clients.metrcs.serializers.BigDecimalQuantityDeserializer;
import com.blaze.clients.metrcs.serializers.BigDecimalQuantitySerializer;
import com.blaze.common.data.compliance.CompliancePackageInfo;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * Created by Gaurav Saini on 22/8/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY,
        getterVisibility = JsonAutoDetect.Visibility.NONE,
        setterVisibility = JsonAutoDetect.Visibility.NONE)
public class MetricsPackages extends CompliancePackageInfo {

    private int Id;
    private String Label;
    private String PackageType;
    private String SourceHarvestNames;

    @JsonSerialize(using = BigDecimalQuantitySerializer.class)
    @JsonDeserialize(using = BigDecimalQuantityDeserializer.class)
    private BigDecimal Quantity;
    private String UnitOfMeasureName;
    private String UnitOfMeasureAbbreviation;
    private int ProductId;
    private String ProductName;
    private String ProductCategoryName;
    private String PackagedDate;
    private String InitialLabTestingState;
    private String LabTestingState;
    private String LabTestingStateName;
    private String LabTestingStateDate;
    private boolean IsProductionBatch;
    private String ProductionBatchNumber;
    private boolean IsTestingSample;
    private boolean IsProcessValidationTestingSample;
    private boolean ProductRequiresRemediation;
    private boolean ContainsRemediatedProduct;
    private String RemediationDate;
    private String ReceivedFromManifestNumber;
    private String ReceivedFromFacilityLicenseNumber;
    private String ReceivedFromFacilityName;
    private String ReceivedDateTime;
    private boolean IsOnHold;
    private String ArchivedDate;
    private String FinishedDate;
    private String LastModified;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getLabel() {
        return Label;
    }

    public void setLabel(String label) {
        Label = label;
    }

    public String getPackageType() {
        return PackageType;
    }

    public void setPackageType(String packageType) {
        PackageType = packageType;
    }

    public String getSourceHarvestNames() {
        return SourceHarvestNames;
    }

    public void setSourceHarvestNames(String sourceHarvestNames) {
        SourceHarvestNames = sourceHarvestNames;
    }

    public BigDecimal getQuantity() {
        return Quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        Quantity = quantity;
    }

    public String getUnitOfMeasureName() {
        return UnitOfMeasureName;
    }

    public void setUnitOfMeasureName(String unitOfMeasureName) {
        UnitOfMeasureName = unitOfMeasureName;
    }

    public String getUnitOfMeasureAbbreviation() {
        return UnitOfMeasureAbbreviation;
    }

    public void setUnitOfMeasureAbbreviation(String unitOfMeasureAbbreviation) {
        UnitOfMeasureAbbreviation = unitOfMeasureAbbreviation;
    }

    public int getProductId() {
        return ProductId;
    }

    public void setProductId(int productId) {
        ProductId = productId;
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }

    public String getProductCategoryName() {
        return ProductCategoryName;
    }

    public void setProductCategoryName(String productCategoryName) {
        ProductCategoryName = productCategoryName;
    }

    public String getPackagedDate() {
        return PackagedDate;
    }

    public void setPackagedDate(String packagedDate) {
        PackagedDate = packagedDate;
    }

    public String getInitialLabTestingState() {
        return InitialLabTestingState;
    }

    public void setInitialLabTestingState(String initialLabTestingState) {
        InitialLabTestingState = initialLabTestingState;
    }

    public String getLabTestingState() {
        return LabTestingState;
    }

    public void setLabTestingState(String labTestingState) {
        LabTestingState = labTestingState;
    }

    public String getLabTestingStateName() {
        return LabTestingStateName;
    }

    public void setLabTestingStateName(String labTestingStateName) {
        LabTestingStateName = labTestingStateName;
    }

    public String getLabTestingStateDate() {
        return LabTestingStateDate;
    }

    public void setLabTestingStateDate(String labTestingStateDate) {
        LabTestingStateDate = labTestingStateDate;
    }

    public boolean isProductionBatch() {
        return IsProductionBatch;
    }

    public void setProductionBatch(boolean productionBatch) {
        IsProductionBatch = productionBatch;
    }

    public String getProductionBatchNumber() {
        return ProductionBatchNumber;
    }

    public void setProductionBatchNumber(String productionBatchNumber) {
        ProductionBatchNumber = productionBatchNumber;
    }

    public boolean isTestingSample() {
        return IsTestingSample;
    }

    public void setTestingSample(boolean testingSample) {
        IsTestingSample = testingSample;
    }

    public boolean isProcessValidationTestingSample() {
        return IsProcessValidationTestingSample;
    }

    public void setProcessValidationTestingSample(boolean processValidationTestingSample) {
        IsProcessValidationTestingSample = processValidationTestingSample;
    }

    public boolean isProductRequiresRemediation() {
        return ProductRequiresRemediation;
    }

    public void setProductRequiresRemediation(boolean productRequiresRemediation) {
        ProductRequiresRemediation = productRequiresRemediation;
    }

    public boolean isContainsRemediatedProduct() {
        return ContainsRemediatedProduct;
    }

    public void setContainsRemediatedProduct(boolean containsRemediatedProduct) {
        ContainsRemediatedProduct = containsRemediatedProduct;
    }

    public String getRemediationDate() {
        return RemediationDate;
    }

    public void setRemediationDate(String remediationDate) {
        RemediationDate = remediationDate;
    }

    public String getReceivedFromManifestNumber() {
        return ReceivedFromManifestNumber;
    }

    public void setReceivedFromManifestNumber(String receivedFromManifestNumber) {
        ReceivedFromManifestNumber = receivedFromManifestNumber;
    }

    public String getReceivedFromFacilityLicenseNumber() {
        return ReceivedFromFacilityLicenseNumber;
    }

    public void setReceivedFromFacilityLicenseNumber(String receivedFromFacilityLicenseNumber) {
        ReceivedFromFacilityLicenseNumber = receivedFromFacilityLicenseNumber;
    }

    public String getReceivedFromFacilityName() {
        return ReceivedFromFacilityName;
    }

    public void setReceivedFromFacilityName(String receivedFromFacilityName) {
        ReceivedFromFacilityName = receivedFromFacilityName;
    }

    public String getReceivedDateTime() {
        return ReceivedDateTime;
    }

    public void setReceivedDateTime(String receivedDateTime) {
        ReceivedDateTime = receivedDateTime;
    }

    public boolean isOnHold() {
        return IsOnHold;
    }

    public void setOnHold(boolean onHold) {
        IsOnHold = onHold;
    }

    public String getArchivedDate() {
        return ArchivedDate;
    }

    public void setArchivedDate(String archivedDate) {
        ArchivedDate = archivedDate;
    }

    public String getFinishedDate() {
        return FinishedDate;
    }

    public void setFinishedDate(String finishedDate) {
        FinishedDate = finishedDate;
    }

    public String getLastModified() {
        return LastModified;
    }

    public void setLastModified(String lastModified) {
        LastModified = lastModified;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MetricsPackages that = (MetricsPackages) o;
        return Objects.equals(Label, that.Label);
    }

    @Override
    public int hashCode() {
        return Objects.hash(Label);
    }
}
