package com.blaze.clients.metrcs.models.packages;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Gaurav Saini on 18/8/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY,
        getterVisibility = JsonAutoDetect.Visibility.NONE,
        setterVisibility = JsonAutoDetect.Visibility.NONE)
public class MetricsReturnedPackages {

    private String Label;
    private double ReturnQuantityVerified;
    private String ReturnUnitOfMeasure;
    private String ReturnReason;
    private String ReturnReasonNote;

    public String getLabel() {
        return Label;
    }

    public void setLabel(String label) {
        Label = label;
    }

    public double getReturnQuantityVerified() {
        return ReturnQuantityVerified;
    }

    public void setReturnQuantityVerified(double returnQuantityVerified) {
        ReturnQuantityVerified = returnQuantityVerified;
    }

    public String getReturnUnitOfMeasure() {
        return ReturnUnitOfMeasure;
    }

    public void setReturnUnitOfMeasure(String returnUnitOfMeasure) {
        ReturnUnitOfMeasure = returnUnitOfMeasure;
    }

    public String getReturnReason() {
        return ReturnReason;
    }

    public void setReturnReason(String returnReason) {
        ReturnReason = returnReason;
    }

    public String getReturnReasonNote() {
        return ReturnReasonNote;
    }

    public void setReturnReasonNote(String returnReasonNote) {
        ReturnReasonNote = returnReasonNote;
    }
}
