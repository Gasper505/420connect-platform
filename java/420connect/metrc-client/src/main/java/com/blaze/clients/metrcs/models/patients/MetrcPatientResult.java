package com.blaze.clients.metrcs.models.patients;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY,
        getterVisibility = JsonAutoDetect.Visibility.NONE,
        setterVisibility = JsonAutoDetect.Visibility.NONE)
public class MetrcPatientResult {
    private String PatientLicenseNumber;
    private double FlowerOuncesAvailable;
    private double ThcOuncesAvailable;

    public String getPatientLicenseNumber() {
        return PatientLicenseNumber;
    }

    public void setPatientLicenseNumber(String patientLicenseNumber) {
        PatientLicenseNumber = patientLicenseNumber;
    }

    public double getFlowerOuncesAvailable() {
        return FlowerOuncesAvailable;
    }

    public void setFlowerOuncesAvailable(double flowerOuncesAvailable) {
        FlowerOuncesAvailable = flowerOuncesAvailable;
    }

    public double getThcOuncesAvailable() {
        return ThcOuncesAvailable;
    }

    public void setThcOuncesAvailable(double thcOuncesAvailable) {
        ThcOuncesAvailable = thcOuncesAvailable;
    }
}
