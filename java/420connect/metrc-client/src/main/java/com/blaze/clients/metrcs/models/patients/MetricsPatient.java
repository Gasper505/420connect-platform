package com.blaze.clients.metrcs.models.patients;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Gaurav Saini on 20/7/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY,
        getterVisibility = JsonAutoDetect.Visibility.NONE,
        setterVisibility = JsonAutoDetect.Visibility.NONE)
public class MetricsPatient {

    private String LicenseNumber;
    private String NewLicenseNumber;
    private String LicenseEffectiveStartDate;
    private String LicenseEffectiveEndDate;
    private double RecommendedPlants;
    private double RecommendedSmokableQuantity;
    private String ActualDate;

    public String getLicenseNumber() {
        return LicenseNumber;
    }

    public void setLicenseNumber(String licenseNumber) {
        LicenseNumber = licenseNumber;
    }

    public String getLicenseEffectiveStartDate() {
        return LicenseEffectiveStartDate;
    }

    public void setLicenseEffectiveStartDate(String licenseEffectiveStartDate) {
        LicenseEffectiveStartDate = licenseEffectiveStartDate;
    }

    public String getLicenseEffectiveEndDate() {
        return LicenseEffectiveEndDate;
    }

    public void setLicenseEffectiveEndDate(String licenseEffectiveEndDate) {
        LicenseEffectiveEndDate = licenseEffectiveEndDate;
    }

    public double getRecommendedPlants() {
        return RecommendedPlants;
    }

    public void setRecommendedPlants(double recommendedPlants) {
        RecommendedPlants = recommendedPlants;
    }

    public double getRecommendedSmokableQuantity() {
        return RecommendedSmokableQuantity;
    }

    public void setRecommendedSmokableQuantity(double recommendedSmokableQuantity) {
        RecommendedSmokableQuantity = recommendedSmokableQuantity;
    }

    public String getActualDate() {
        return ActualDate;
    }

    public void setActualDate(String actualDate) {
        ActualDate = actualDate;
    }

    public String getNewLicenseNumber() {
        return NewLicenseNumber;
    }

    public void setNewLicenseNumber(String newLicenseNumber) {
        NewLicenseNumber = newLicenseNumber;
    }
}
