package com.blaze.clients.metrcs.models.plants;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Gaurav Saini on 20/7/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY,
        getterVisibility = JsonAutoDetect.Visibility.NONE,
        setterVisibility = JsonAutoDetect.Visibility.NONE)
public class MetricsManicurePlants {

    private String Plant;
    private String UnitOfWeight;
    private double Weight;
    private String DryingRoom;
    private String HarvestName;
    private String ActualDate;

    public String getPlant() {
        return Plant;
    }

    public void setPlant(String plant) {
        Plant = plant;
    }

    public String getUnitOfWeight() {
        return UnitOfWeight;
    }

    public void setUnitOfWeight(String unitOfWeight) {
        UnitOfWeight = unitOfWeight;
    }

    public double getWeight() {
        return Weight;
    }

    public void setWeight(double weight) {
        Weight = weight;
    }

    public String getDryingRoom() {
        return DryingRoom;
    }

    public void setDryingRoom(String dryingRoom) {
        DryingRoom = dryingRoom;
    }

    public String getHarvestName() {
        return HarvestName;
    }

    public void setHarvestName(String harvestName) {
        HarvestName = harvestName;
    }

    public String getActualDate() {
        return ActualDate;
    }

    public void setActualDate(String actualDate) {
        ActualDate = actualDate;
    }
}
