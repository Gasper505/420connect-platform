package com.blaze.clients.metrcs.models.plants;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Gaurav Saini on 22/8/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY,
        getterVisibility = JsonAutoDetect.Visibility.NONE,
        setterVisibility = JsonAutoDetect.Visibility.NONE)
public class MetricsPlant {
    public enum PlantGrowthPhase {
        Vegetative,
        Flowering
    }

    public enum PlantState {
        Harvested,
        Tracked
    }

    private int Id;
    private String Label;
    private PlantState State = PlantState.Tracked;
    private PlantGrowthPhase GrowthPhase = PlantGrowthPhase.Flowering;
    private int PlantBatchId;
    private String PlantBatchName;
    private String PlantBatchTypeName;
    private int StrainId;
    private String StrainName;
    private int RoomId;
    private String RoomName;
    private int HarvestId;
    private String HarvestedUnitOfWeightName;
    private String HarvestedUnitOfWeightAbbreviation;
    private String HarvestedWetWeight;
    private String HarvestCount;
    private boolean IsOnHold;
    private String PlantedDate;
    private String VegetativeDate;
    private String FloweringDate;
    private String HarvestedDate;
    private String DestroyedDate;
    private String DestroyedNote;
    private String DestroyedByUserName;
    private String LastModified;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getLabel() {
        return Label;
    }

    public void setLabel(String label) {
        Label = label;
    }

    public PlantGrowthPhase getGrowthPhase() {
        return GrowthPhase;
    }

    public void setGrowthPhase(PlantGrowthPhase growthPhase) {
        GrowthPhase = growthPhase;
    }

    public void setIsOnHold(boolean isOnHold) {
        IsOnHold = isOnHold;
    }

    public PlantState getState() {
        return State;
    }

    public void setState(PlantState state) {
        State = state;
    }

    public int getPlantBatchId() {
        return PlantBatchId;
    }

    public void setPlantBatchId(int plantBatchId) {
        PlantBatchId = plantBatchId;
    }

    public String getPlantBatchName() {
        return PlantBatchName;
    }

    public void setPlantBatchName(String plantBatchName) {
        PlantBatchName = plantBatchName;
    }

    public String getPlantBatchTypeName() {
        return PlantBatchTypeName;
    }

    public void setPlantBatchTypeName(String plantBatchTypeName) {
        PlantBatchTypeName = plantBatchTypeName;
    }

    public int getStrainId() {
        return StrainId;
    }

    public void setStrainId(int strainId) {
        StrainId = strainId;
    }

    public String getStrainName() {
        return StrainName;
    }

    public void setStrainName(String strainName) {
        StrainName = strainName;
    }

    public int getRoomId() {
        return RoomId;
    }

    public void setRoomId(int roomId) {
        RoomId = roomId;
    }

    public String getRoomName() {
        return RoomName;
    }

    public void setRoomName(String roomName) {
        RoomName = roomName;
    }

    public int getHarvestId() {
        return HarvestId;
    }

    public void setHarvestId(int harvestId) {
        HarvestId = harvestId;
    }

    public String getHarvestedUnitOfWeightName() {
        return HarvestedUnitOfWeightName;
    }

    public void setHarvestedUnitOfWeightName(String harvestedUnitOfWeightName) {
        HarvestedUnitOfWeightName = harvestedUnitOfWeightName;
    }

    public String getHarvestedUnitOfWeightAbbreviation() {
        return HarvestedUnitOfWeightAbbreviation;
    }

    public void setHarvestedUnitOfWeightAbbreviation(String harvestedUnitOfWeightAbbreviation) {
        HarvestedUnitOfWeightAbbreviation = harvestedUnitOfWeightAbbreviation;
    }

    public String getHarvestedWetWeight() {
        return HarvestedWetWeight;
    }

    public void setHarvestedWetWeight(String harvestedWetWeight) {
        HarvestedWetWeight = harvestedWetWeight;
    }

    public String getHarvestCount() {
        return HarvestCount;
    }

    public void setHarvestCount(String harvestCount) {
        HarvestCount = harvestCount;
    }

    public boolean isOnHold() {
        return IsOnHold;
    }

    public void setOnHold(boolean onHold) {
        IsOnHold = onHold;
    }

    public String getPlantedDate() {
        return PlantedDate;
    }

    public void setPlantedDate(String plantedDate) {
        PlantedDate = plantedDate;
    }

    public String getVegetativeDate() {
        return VegetativeDate;
    }

    public void setVegetativeDate(String vegetativeDate) {
        VegetativeDate = vegetativeDate;
    }

    public String getFloweringDate() {
        return FloweringDate;
    }

    public void setFloweringDate(String floweringDate) {
        FloweringDate = floweringDate;
    }

    public String getHarvestedDate() {
        return HarvestedDate;
    }

    public void setHarvestedDate(String harvestedDate) {
        HarvestedDate = harvestedDate;
    }

    public String getDestroyedDate() {
        return DestroyedDate;
    }

    public void setDestroyedDate(String destroyedDate) {
        DestroyedDate = destroyedDate;
    }

    public String getDestroyedNote() {
        return DestroyedNote;
    }

    public void setDestroyedNote(String destroyedNote) {
        DestroyedNote = destroyedNote;
    }

    public String getDestroyedByUserName() {
        return DestroyedByUserName;
    }

    public void setDestroyedByUserName(String destroyedByUserName) {
        DestroyedByUserName = destroyedByUserName;
    }

    public String getLastModified() {
        return LastModified;
    }

    public void setLastModified(String lastModified) {
        LastModified = lastModified;
    }
}
