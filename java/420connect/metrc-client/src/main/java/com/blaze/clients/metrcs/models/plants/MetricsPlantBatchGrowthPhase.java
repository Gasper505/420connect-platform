package com.blaze.clients.metrcs.models.plants;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Gaurav Saini on 20/7/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY,
        getterVisibility = JsonAutoDetect.Visibility.NONE,
        setterVisibility = JsonAutoDetect.Visibility.NONE)
public class MetricsPlantBatchGrowthPhase {

    private int Id;
    private int Count;
    private String StartingTag;
    private String GrowthPhase;
    private String NewRoom;
    private String GrowthDate;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public int getCount() {
        return Count;
    }

    public void setCount(int count) {
        Count = count;
    }

    public String getStartingTag() {
        return StartingTag;
    }

    public void setStartingTag(String startingTag) {
        StartingTag = startingTag;
    }

    public String getGrowthPhase() {
        return GrowthPhase;
    }

    public void setGrowthPhase(String growthPhase) {
        GrowthPhase = growthPhase;
    }

    public String getNewRoom() {
        return NewRoom;
    }

    public void setNewRoom(String newRoom) {
        NewRoom = newRoom;
    }

    public String getGrowthDate() {
        return GrowthDate;
    }

    public void setGrowthDate(String growthDate) {
        GrowthDate = growthDate;
    }
}
