package com.blaze.clients.metrcs.models.plants;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Gaurav Saini on 22/8/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY,
        getterVisibility = JsonAutoDetect.Visibility.NONE,
        setterVisibility = JsonAutoDetect.Visibility.NONE)
public class MetricsPlantBatchPackages {

    private int Id;
    private String Name;
    private String Type;
    private int StrainId;
    private String StrainName;
    private int Count;
    private int LiveCount;
    private int PackagedCount;
    private int HarvestedCount;
    private int DestroyedCount;
    private int SourcePackageId;
    private String SourcePackageLabel;
    private int SourcePlantId;
    private String SourcePlantLabel;
    private String PlantedDate;
    private String LastModified;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public int getStrainId() {
        return StrainId;
    }

    public void setStrainId(int strainId) {
        StrainId = strainId;
    }

    public String getStrainName() {
        return StrainName;
    }

    public void setStrainName(String strainName) {
        StrainName = strainName;
    }

    public int getCount() {
        return Count;
    }

    public void setCount(int count) {
        Count = count;
    }

    public int getLiveCount() {
        return LiveCount;
    }

    public void setLiveCount(int liveCount) {
        LiveCount = liveCount;
    }

    public int getPackagedCount() {
        return PackagedCount;
    }

    public void setPackagedCount(int packagedCount) {
        PackagedCount = packagedCount;
    }

    public int getHarvestedCount() {
        return HarvestedCount;
    }

    public void setHarvestedCount(int harvestedCount) {
        HarvestedCount = harvestedCount;
    }

    public int getDestroyedCount() {
        return DestroyedCount;
    }

    public void setDestroyedCount(int destroyedCount) {
        DestroyedCount = destroyedCount;
    }

    public int getSourcePackageId() {
        return SourcePackageId;
    }

    public void setSourcePackageId(int sourcePackageId) {
        SourcePackageId = sourcePackageId;
    }

    public String getSourcePackageLabel() {
        return SourcePackageLabel;
    }

    public void setSourcePackageLabel(String sourcePackageLabel) {
        SourcePackageLabel = sourcePackageLabel;
    }

    public int getSourcePlantId() {
        return SourcePlantId;
    }

    public void setSourcePlantId(int sourcePlantId) {
        SourcePlantId = sourcePlantId;
    }

    public String getSourcePlantLabel() {
        return SourcePlantLabel;
    }

    public void setSourcePlantLabel(String sourcePlantLabel) {
        SourcePlantLabel = sourcePlantLabel;
    }

    public String getPlantedDate() {
        return PlantedDate;
    }

    public void setPlantedDate(String plantedDate) {
        PlantedDate = plantedDate;
    }

    public String getLastModified() {
        return LastModified;
    }

    public void setLastModified(String lastModified) {
        LastModified = lastModified;
    }
}
