package com.blaze.clients.metrcs.models.plants;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Gaurav Saini on 20/7/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY,
        getterVisibility = JsonAutoDetect.Visibility.NONE,
        setterVisibility = JsonAutoDetect.Visibility.NONE)
public class MetricsPlantGrowthPhase {

    private int Id;
    private String Label;
    private String NewTag;
    private String GrowthPhase;
    private String NewRoom;
    private String GrowthDate;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getLabel() {
        return Label;
    }

    public void setLabel(String label) {
        Label = label;
    }

    public String getNewTag() {
        return NewTag;
    }

    public void setNewTag(String newTag) {
        NewTag = newTag;
    }

    public String getGrowthPhase() {
        return GrowthPhase;
    }

    public void setGrowthPhase(String growthPhase) {
        GrowthPhase = growthPhase;
    }

    public String getNewRoom() {
        return NewRoom;
    }

    public void setNewRoom(String newRoom) {
        NewRoom = newRoom;
    }

    public String getGrowthDate() {
        return GrowthDate;
    }

    public void setGrowthDate(String growthDate) {
        GrowthDate = growthDate;
    }
}
