package com.blaze.clients.metrcs.models.plants;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Gaurav Saini on 20/7/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY,
        getterVisibility = JsonAutoDetect.Visibility.NONE,
        setterVisibility = JsonAutoDetect.Visibility.NONE)
public class MetricsPlantPlanting {

    private String PlantLabel;
    private String PlantBatchName;
    private String PlantBatchType;
    private int PlantCount;
    private String StrainName;
    private String ActualDate;

    public String getPlantLabel() {
        return PlantLabel;
    }

    public void setPlantLabel(String plantLabel) {
        PlantLabel = plantLabel;
    }

    public String getPlantBatchName() {
        return PlantBatchName;
    }

    public void setPlantBatchName(String plantBatchName) {
        PlantBatchName = plantBatchName;
    }

    public String getPlantBatchType() {
        return PlantBatchType;
    }

    public void setPlantBatchType(String plantBatchType) {
        PlantBatchType = plantBatchType;
    }

    public int getPlantCount() {
        return PlantCount;
    }

    public void setPlantCount(int plantCount) {
        PlantCount = plantCount;
    }

    public String getStrainName() {
        return StrainName;
    }

    public void setStrainName(String strainName) {
        StrainName = strainName;
    }

    public String getActualDate() {
        return ActualDate;
    }

    public void setActualDate(String actualDate) {
        ActualDate = actualDate;
    }
}
