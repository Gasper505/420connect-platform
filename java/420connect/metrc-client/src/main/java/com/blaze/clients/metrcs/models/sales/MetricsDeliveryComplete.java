package com.blaze.clients.metrcs.models.sales;

import com.blaze.clients.metrcs.models.packages.MetricsReturnedPackages;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

/**
 * Created by Gaurav Saini on 18/8/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY,
        getterVisibility = JsonAutoDetect.Visibility.NONE,
        setterVisibility = JsonAutoDetect.Visibility.NONE)
public class MetricsDeliveryComplete {

    private String Id;
    private String ActualArrivalDateTime;
    private List<String> AcceptedPackages;
    private String DriverName;
    private List<MetricsReturnedPackages> ReturnedPackages;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getActualArrivalDateTime() {
        return ActualArrivalDateTime;
    }

    public void setActualArrivalDateTime(String actualArrivalDateTime) {
        ActualArrivalDateTime = actualArrivalDateTime;
    }

    public List<String> getAcceptedPackages() {
        return AcceptedPackages;
    }

    public void setAcceptedPackages(List<String> acceptedPackages) {
        AcceptedPackages = acceptedPackages;
    }

    public String getDriverName() {
        return DriverName;
    }

    public void setDriverName(String driverName) {
        DriverName = driverName;
    }

    public List<MetricsReturnedPackages> getReturnedPackages() {
        return ReturnedPackages;
    }

    public void setReturnedPackages(List<MetricsReturnedPackages> returnedPackages) {
        ReturnedPackages = returnedPackages;
    }
}
