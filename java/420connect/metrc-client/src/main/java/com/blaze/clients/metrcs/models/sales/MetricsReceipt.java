package com.blaze.clients.metrcs.models.sales;

import com.blaze.common.data.compliance.ComplianceSaleReceiptInfo;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;;

import java.util.List;

/**
 * Created by Gaurav Saini on 22/8/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY,
        getterVisibility = JsonAutoDetect.Visibility.NONE,
        setterVisibility = JsonAutoDetect.Visibility.NONE)
public class MetricsReceipt extends ComplianceSaleReceiptInfo {

    private int Id;
    private String ReceiptNumber;
    private String SalesDateTime;
    private String SalesCustomerType;
    private String PatientLicenseNumber;
    private int TotalPackages;
    private double TotalPrice;
    private List<MetricsTransaction> Transactions;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getReceiptNumber() {
        return ReceiptNumber;
    }

    public void setReceiptNumber(String receiptNumber) {
        ReceiptNumber = receiptNumber;
    }

    public String getSalesDateTime() {
        return SalesDateTime;
    }

    public void setSalesDateTime(String salesDateTime) {
        SalesDateTime = salesDateTime;
    }

    public String getSalesCustomerType() {
        return SalesCustomerType;
    }

    public void setSalesCustomerType(String salesCustomerType) {
        SalesCustomerType = salesCustomerType;
    }

    public String getPatientLicenseNumber() {
        return PatientLicenseNumber;
    }

    public void setPatientLicenseNumber(String patientLicenseNumber) {
        PatientLicenseNumber = patientLicenseNumber;
    }

    public int getTotalPackages() {
        return TotalPackages;
    }

    public void setTotalPackages(int totalPackages) {
        TotalPackages = totalPackages;
    }

    public double getTotalPrice() {
        return TotalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        TotalPrice = totalPrice;
    }

    public List<MetricsTransaction> getTransactions() {
        return Transactions;
    }

    public void setTransactions(List<MetricsTransaction> transactions) {
        Transactions = transactions;
    }
}
