package com.blaze.clients.metrcs.models.sales;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

/**
 * Created by Gaurav Saini on 22/8/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY,
        getterVisibility = JsonAutoDetect.Visibility.NONE,
        setterVisibility = JsonAutoDetect.Visibility.NONE)
public class MetricsSale {

    private int Id;
    private String DeliveryNumber;
    private String SalesDateTime;
    private String SalesCustomerType;
    private String PatientLicenseNumber;
    private String DriverName;
    private String DriverOccupationalLicenseNumber;
    private String DriverVehicleLicenseNumber;
    private String VehicleMake;
    private String VehicleModel;
    private String VehicleLicensePlateNumber;
    private String PlannedRoute;
    private String EstimatedDepartureDateTime;
    private String EstimatedArrivalDateTime;
    private String ActualArrivalDateTime;
    private int TotalPackages;
    private double TotalPrice;
    private List<MetricsTransaction> Transactions;
    private String CreatedDateTime;
    private String CreatedByUserName;
    private String CompletedDateTime;
    private String SalesDeliveryState;
    private String VoidedDate;
    private String LastModified;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getDeliveryNumber() {
        return DeliveryNumber;
    }

    public void setDeliveryNumber(String deliveryNumber) {
        DeliveryNumber = deliveryNumber;
    }

    public String getSalesDateTime() {
        return SalesDateTime;
    }

    public void setSalesDateTime(String salesDateTime) {
        SalesDateTime = salesDateTime;
    }

    public String getSalesCustomerType() {
        return SalesCustomerType;
    }

    public void setSalesCustomerType(String salesCustomerType) {
        SalesCustomerType = salesCustomerType;
    }

    public String getPatientLicenseNumber() {
        return PatientLicenseNumber;
    }

    public void setPatientLicenseNumber(String patientLicenseNumber) {
        PatientLicenseNumber = patientLicenseNumber;
    }

    public String getDriverName() {
        return DriverName;
    }

    public void setDriverName(String driverName) {
        DriverName = driverName;
    }

    public String getDriverOccupationalLicenseNumber() {
        return DriverOccupationalLicenseNumber;
    }

    public void setDriverOccupationalLicenseNumber(String driverOccupationalLicenseNumber) {
        DriverOccupationalLicenseNumber = driverOccupationalLicenseNumber;
    }

    public String getDriverVehicleLicenseNumber() {
        return DriverVehicleLicenseNumber;
    }

    public void setDriverVehicleLicenseNumber(String driverVehicleLicenseNumber) {
        DriverVehicleLicenseNumber = driverVehicleLicenseNumber;
    }

    public String getVehicleMake() {
        return VehicleMake;
    }

    public void setVehicleMake(String vehicleMake) {
        VehicleMake = vehicleMake;
    }

    public String getVehicleModel() {
        return VehicleModel;
    }

    public void setVehicleModel(String vehicleModel) {
        VehicleModel = vehicleModel;
    }

    public String getVehicleLicensePlateNumber() {
        return VehicleLicensePlateNumber;
    }

    public void setVehicleLicensePlateNumber(String vehicleLicensePlateNumber) {
        VehicleLicensePlateNumber = vehicleLicensePlateNumber;
    }

    public String getPlannedRoute() {
        return PlannedRoute;
    }

    public void setPlannedRoute(String plannedRoute) {
        PlannedRoute = plannedRoute;
    }

    public String getEstimatedDepartureDateTime() {
        return EstimatedDepartureDateTime;
    }

    public void setEstimatedDepartureDateTime(String estimatedDepartureDateTime) {
        EstimatedDepartureDateTime = estimatedDepartureDateTime;
    }

    public String getEstimatedArrivalDateTime() {
        return EstimatedArrivalDateTime;
    }

    public void setEstimatedArrivalDateTime(String estimatedArrivalDateTime) {
        EstimatedArrivalDateTime = estimatedArrivalDateTime;
    }

    public String getActualArrivalDateTime() {
        return ActualArrivalDateTime;
    }

    public void setActualArrivalDateTime(String actualArrivalDateTime) {
        ActualArrivalDateTime = actualArrivalDateTime;
    }

    public int getTotalPackages() {
        return TotalPackages;
    }

    public void setTotalPackages(int totalPackages) {
        TotalPackages = totalPackages;
    }

    public double getTotalPrice() {
        return TotalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        TotalPrice = totalPrice;
    }

    public List<MetricsTransaction> getTransactions() {
        return Transactions;
    }

    public void setTransactions(List<MetricsTransaction> transactions) {
        Transactions = transactions;
    }

    public String getCreatedDateTime() {
        return CreatedDateTime;
    }

    public void setCreatedDateTime(String createdDateTime) {
        CreatedDateTime = createdDateTime;
    }

    public String getCreatedByUserName() {
        return CreatedByUserName;
    }

    public void setCreatedByUserName(String createdByUserName) {
        CreatedByUserName = createdByUserName;
    }

    public String getCompletedDateTime() {
        return CompletedDateTime;
    }

    public void setCompletedDateTime(String completedDateTime) {
        CompletedDateTime = completedDateTime;
    }

    public String getSalesDeliveryState() {
        return SalesDeliveryState;
    }

    public void setSalesDeliveryState(String salesDeliveryState) {
        SalesDeliveryState = salesDeliveryState;
    }

    public String getVoidedDate() {
        return VoidedDate;
    }

    public void setVoidedDate(String voidedDate) {
        VoidedDate = voidedDate;
    }

    public String getLastModified() {
        return LastModified;
    }

    public void setLastModified(String lastModified) {
        LastModified = lastModified;
    }
}
