package com.blaze.clients.metrcs.models.sales;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

/**
 * Created by Gaurav Saini on 18/8/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY,
        getterVisibility = JsonAutoDetect.Visibility.NONE,
        setterVisibility = JsonAutoDetect.Visibility.NONE)
public class MetricsSaleDelivery {

    private int Id;
    private String SalesDateTime;
    private String SalesCustomerType;
    private String PatientLicenseNumber;
    private String DriverName;
    private String DriverOccupationalLicenseNumber;
    private String DriverVehicleLicenseNumber;
    private String PhoneNumberForQuestions;
    private String VehicleMake;
    private String VehicleModel;
    private String VehicleLicensePlateNumber;
    private String RecipientAddressStreet1;
    private String RecipientAddressStreet2;
    private String RecipientAddressCity;
    private String RecipientAddressState;
    private String RecipientAddressPostalCode;
    private String PlannedRoute;
    private String EstimatedDepartureDateTime;
    private String EstimatedArrivalDateTime;
    private List<MetricsSaleTransaction> Transactions;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getSalesDateTime() {
        return SalesDateTime;
    }

    public void setSalesDateTime(String salesDateTime) {
        SalesDateTime = salesDateTime;
    }

    public String getSalesCustomerType() {
        return SalesCustomerType;
    }

    public void setSalesCustomerType(String salesCustomerType) {
        SalesCustomerType = salesCustomerType;
    }

    public String getPatientLicenseNumber() {
        return PatientLicenseNumber;
    }

    public void setPatientLicenseNumber(String patientLicenseNumber) {
        PatientLicenseNumber = patientLicenseNumber;
    }

    public String getDriverName() {
        return DriverName;
    }

    public void setDriverName(String driverName) {
        DriverName = driverName;
    }

    public String getDriverOccupationalLicenseNumber() {
        return DriverOccupationalLicenseNumber;
    }

    public void setDriverOccupationalLicenseNumber(String driverOccupationalLicenseNumber) {
        DriverOccupationalLicenseNumber = driverOccupationalLicenseNumber;
    }

    public String getDriverVehicleLicenseNumber() {
        return DriverVehicleLicenseNumber;
    }

    public void setDriverVehicleLicenseNumber(String driverVehicleLicenseNumber) {
        DriverVehicleLicenseNumber = driverVehicleLicenseNumber;
    }

    public String getPhoneNumberForQuestions() {
        return PhoneNumberForQuestions;
    }

    public void setPhoneNumberForQuestions(String phoneNumberForQuestions) {
        PhoneNumberForQuestions = phoneNumberForQuestions;
    }

    public String getVehicleMake() {
        return VehicleMake;
    }

    public void setVehicleMake(String vehicleMake) {
        VehicleMake = vehicleMake;
    }

    public String getVehicleModel() {
        return VehicleModel;
    }

    public void setVehicleModel(String vehicleModel) {
        VehicleModel = vehicleModel;
    }

    public String getVehicleLicensePlateNumber() {
        return VehicleLicensePlateNumber;
    }

    public void setVehicleLicensePlateNumber(String vehicleLicensePlateNumber) {
        VehicleLicensePlateNumber = vehicleLicensePlateNumber;
    }

    public String getRecipientAddressStreet1() {
        return RecipientAddressStreet1;
    }

    public void setRecipientAddressStreet1(String recipientAddressStreet1) {
        RecipientAddressStreet1 = recipientAddressStreet1;
    }

    public String getRecipientAddressStreet2() {
        return RecipientAddressStreet2;
    }

    public void setRecipientAddressStreet2(String recipientAddressStreet2) {
        RecipientAddressStreet2 = recipientAddressStreet2;
    }

    public String getRecipientAddressCity() {
        return RecipientAddressCity;
    }

    public void setRecipientAddressCity(String recipientAddressCity) {
        RecipientAddressCity = recipientAddressCity;
    }

    public String getRecipientAddressState() {
        return RecipientAddressState;
    }

    public void setRecipientAddressState(String recipientAddressState) {
        RecipientAddressState = recipientAddressState;
    }

    public String getRecipientAddressPostalCode() {
        return RecipientAddressPostalCode;
    }

    public void setRecipientAddressPostalCode(String recipientAddressPostalCode) {
        RecipientAddressPostalCode = recipientAddressPostalCode;
    }

    public String getPlannedRoute() {
        return PlannedRoute;
    }

    public void setPlannedRoute(String plannedRoute) {
        PlannedRoute = plannedRoute;
    }

    public String getEstimatedDepartureDateTime() {
        return EstimatedDepartureDateTime;
    }

    public void setEstimatedDepartureDateTime(String estimatedDepartureDateTime) {
        EstimatedDepartureDateTime = estimatedDepartureDateTime;
    }

    public String getEstimatedArrivalDateTime() {
        return EstimatedArrivalDateTime;
    }

    public void setEstimatedArrivalDateTime(String estimatedArrivalDateTime) {
        EstimatedArrivalDateTime = estimatedArrivalDateTime;
    }

    public List<MetricsSaleTransaction> getTransactions() {
        return Transactions;
    }

    public void setTransactions(List<MetricsSaleTransaction> transactions) {
        Transactions = transactions;
    }
}
