package com.blaze.clients.metrcs.models.sales;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;

/**
 * Created by Gaurav Saini on 22/8/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class MetricsSaleList extends ArrayList<MetricsSale> {
}
