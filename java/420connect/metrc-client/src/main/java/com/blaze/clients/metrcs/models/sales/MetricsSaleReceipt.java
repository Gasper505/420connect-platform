package com.blaze.clients.metrcs.models.sales;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

/**
 * Created by Gaurav Saini on 20/7/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY,
        getterVisibility = JsonAutoDetect.Visibility.NONE,
        setterVisibility = JsonAutoDetect.Visibility.NONE)
public class MetricsSaleReceipt {

    private long Id;
    private String SalesDateTime;
    private String SalesCustomerType;
    private String PatientLicenseNumber;
    private List<MetricsSaleTransaction> Transactions;

    public long getId() {
        return Id;
    }

    public void setId(long id) {
        Id = id;
    }

    public String getSalesDateTime() {
        return SalesDateTime;
    }

    public void setSalesDateTime(String salesDateTime) {
        SalesDateTime = salesDateTime;
    }

    public String getSalesCustomerType() {
        return SalesCustomerType;
    }

    public void setSalesCustomerType(String salesCustomerType) {
        SalesCustomerType = salesCustomerType;
    }

    public String getPatientLicenseNumber() {
        return PatientLicenseNumber;
    }

    public void setPatientLicenseNumber(String patientLicenseNumber) {
        PatientLicenseNumber = patientLicenseNumber;
    }

    public List<MetricsSaleTransaction> getTransactions() {
        return Transactions;
    }

    public void setTransactions(List<MetricsSaleTransaction> transactions) {
        Transactions = transactions;
    }
}
