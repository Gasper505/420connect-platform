package com.blaze.clients.metrcs.models.sales;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MetricsSaleReturnReasonList extends ArrayList<MetricsSaleReturnReason> {
}
