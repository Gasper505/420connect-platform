package com.blaze.clients.metrcs.models.sales;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Gaurav Saini on 22/8/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY,
        getterVisibility = JsonAutoDetect.Visibility.NONE,
        setterVisibility = JsonAutoDetect.Visibility.NONE)
public class MetricsTransaction {

    private Integer PackageId;
    private String PackageLabel;
    private String ProductName;
    private double QuantitySold;
    private String UnitOfMeasureName;
    private String UnitOfMeasureAbbreviation;
    private double TotalPrice;
    private String SalesDeliveryState;
    private String ArchivedDate;
    private String LastModified;


    public String getArchivedDate() {
        return ArchivedDate;
    }

    public void setArchivedDate(String archivedDate) {
        ArchivedDate = archivedDate;
    }

    public String getLastModified() {
        return LastModified;
    }

    public void setLastModified(String lastModified) {
        LastModified = lastModified;
    }

    public Integer getPackageId() {
        return PackageId;
    }

    public void setPackageId(Integer packageId) {
        PackageId = packageId;
    }

    public String getPackageLabel() {
        return PackageLabel;
    }

    public void setPackageLabel(String packageLabel) {
        PackageLabel = packageLabel;
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }

    public double getQuantitySold() {
        return QuantitySold;
    }

    public void setQuantitySold(double quantitySold) {
        QuantitySold = quantitySold;
    }

    public String getSalesDeliveryState() {
        return SalesDeliveryState;
    }

    public void setSalesDeliveryState(String salesDeliveryState) {
        SalesDeliveryState = salesDeliveryState;
    }

    public double getTotalPrice() {
        return TotalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        TotalPrice = totalPrice;
    }

    public String getUnitOfMeasureAbbreviation() {
        return UnitOfMeasureAbbreviation;
    }

    public void setUnitOfMeasureAbbreviation(String unitOfMeasureAbbreviation) {
        UnitOfMeasureAbbreviation = unitOfMeasureAbbreviation;
    }

    public String getUnitOfMeasureName() {
        return UnitOfMeasureName;
    }

    public void setUnitOfMeasureName(String unitOfMeasureName) {
        UnitOfMeasureName = unitOfMeasureName;
    }
}
