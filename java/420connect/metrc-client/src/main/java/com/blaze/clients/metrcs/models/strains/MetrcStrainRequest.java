package com.blaze.clients.metrcs.models.strains;

import com.blaze.common.data.compliance.ComplianceStrainInfo;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Gaurav Saini on 20/7/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY,
        getterVisibility = JsonAutoDetect.Visibility.NONE,
        setterVisibility = JsonAutoDetect.Visibility.NONE)
public class MetrcStrainRequest extends ComplianceStrainInfo {

    private String name;
    private String testingStatus;
    private double thcLevel;
    private double cbdLevel;
    private double indicaPercentage;
    private double sativaPercentage;
    private String genetics;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTestingStatus() {
        return testingStatus;
    }

    public void setTestingStatus(String testingStatus) {
        this.testingStatus = testingStatus;
    }

    public double getThcLevel() {
        return thcLevel;
    }

    public void setThcLevel(double thcLevel) {
        this.thcLevel = thcLevel;
    }

    public double getCbdLevel() {
        return cbdLevel;
    }

    public void setCbdLevel(double cbdLevel) {
        this.cbdLevel = cbdLevel;
    }

    public double getIndicaPercentage() {
        return indicaPercentage;
    }

    public void setIndicaPercentage(double indicaPercentage) {
        this.indicaPercentage = indicaPercentage;
    }

    public double getSativaPercentage() {
        return sativaPercentage;
    }

    public void setSativaPercentage(double sativaPercentage) {
        this.sativaPercentage = sativaPercentage;
    }

    public String getGenetics() {
        return genetics;
    }

    public void setGenetics(String genetics) {
        this.genetics = genetics;
    }
}
