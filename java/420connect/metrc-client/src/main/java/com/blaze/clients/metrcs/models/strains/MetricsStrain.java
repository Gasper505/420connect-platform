package com.blaze.clients.metrcs.models.strains;

import com.blaze.common.data.compliance.ComplianceStrainInfo;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Gaurav Saini on 20/7/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY,
        getterVisibility = JsonAutoDetect.Visibility.NONE,
        setterVisibility = JsonAutoDetect.Visibility.NONE)
public class MetricsStrain extends ComplianceStrainInfo {

    private int Id;
    private String Name;
    private String TestingStatus;
    private double ThcLevel;
    private double CbdLevel;
    private double IndicaPercentage;
    private double SativaPercentage;
    private String Genetics;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getTestingStatus() {
        return TestingStatus;
    }

    public void setTestingStatus(String testingStatus) {
        TestingStatus = testingStatus;
    }

    public double getThcLevel() {
        return ThcLevel;
    }

    public void setThcLevel(double thcLevel) {
        ThcLevel = thcLevel;
    }

    public double getCbdLevel() {
        return CbdLevel;
    }

    public void setCbdLevel(double cbdLevel) {
        CbdLevel = cbdLevel;
    }

    public double getIndicaPercentage() {
        return IndicaPercentage;
    }

    public void setIndicaPercentage(double indicaPercentage) {
        IndicaPercentage = indicaPercentage;
    }

    public double getSativaPercentage() {
        return SativaPercentage;
    }

    public void setSativaPercentage(double sativaPercentage) {
        SativaPercentage = sativaPercentage;
    }

    public String getGenetics() {
        return Genetics;
    }

    public void setGenetics(String genetics) {
        Genetics = genetics;
    }
}
