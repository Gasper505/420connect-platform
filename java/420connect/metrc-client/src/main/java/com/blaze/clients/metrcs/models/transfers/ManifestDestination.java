package com.blaze.clients.metrcs.models.transfers;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ManifestDestination {
    @JsonProperty("RecipientLicenseNumber")
    private String recipientLicenseNumber;
    @JsonProperty("TransferTypeName")
    private String transferTypeName;
    @JsonProperty("PlannedRoute")
    private String plannedRoute;
    @JsonProperty("EstimatedDepartureDateTime")
    private String estimatedDepartureDateTime;
    @JsonProperty("EstimatedArrivalDateTime")
    private String estimatedArrivalDateTime;
    @JsonProperty("Transporters")
    private List<ManifestTransporter> transporters = new ArrayList<>();
    @JsonProperty("Packages")
    private List<ManifestPackages> packages = new ArrayList<>();

    public String getRecipientLicenseNumber() {
        return recipientLicenseNumber;
    }

    public void setRecipientLicenseNumber(String recipientLicenseNumber) {
        this.recipientLicenseNumber = recipientLicenseNumber;
    }

    public String getTransferTypeName() {
        return transferTypeName;
    }

    public void setTransferTypeName(String transferTypeName) {
        this.transferTypeName = transferTypeName;
    }

    public String getPlannedRoute() {
        return plannedRoute;
    }

    public void setPlannedRoute(String plannedRoute) {
        this.plannedRoute = plannedRoute;
    }

    public String getEstimatedDepartureDateTime() {
        return estimatedDepartureDateTime;
    }

    public void setEstimatedDepartureDateTime(String estimatedDepartureDateTime) {
        this.estimatedDepartureDateTime = estimatedDepartureDateTime;
    }

    public String getEstimatedArrivalDateTime() {
        return estimatedArrivalDateTime;
    }

    public void setEstimatedArrivalDateTime(String estimatedArrivalDateTime) {
        this.estimatedArrivalDateTime = estimatedArrivalDateTime;
    }

    public List<ManifestTransporter> getTransporters() {
        return transporters;
    }

    public void setTransporters(List<ManifestTransporter> transporters) {
        this.transporters = transporters;
    }

    public List<ManifestPackages> getPackages() {
        return packages;
    }

    public void setPackages(List<ManifestPackages> packages) {
        this.packages = packages;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class ManifestPackages {
        @JsonProperty("PackageLabel")
        private String packageLabel;
        @JsonProperty("WholesalePrice")
        private Double wholesalePrice;

        public String getPackageLabel() {
            return packageLabel;
        }

        public void setPackageLabel(String packageLabel) {
            this.packageLabel = packageLabel;
        }

        public Double getWholesalePrice() {
            return wholesalePrice;
        }

        public void setWholesalePrice(Double wholesalePrice) {
            this.wholesalePrice = wholesalePrice;
        }
    }
}
