package com.blaze.clients.metrcs.models.transfers;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ManifestMetricsTransferTemplate  {
    @JsonProperty("Name")
    private String name;
    @JsonProperty("TransporterFacilityLicenseNumber")
    private String transporterFacilityLicenseNumber;
    @JsonProperty("DriverOccupationalLicenseNumber")
    private String driverOccupationalLicenseNumber;
    @JsonProperty("DriverName")
    private String driverName;
    @JsonProperty("DriverLicenseNumber")
    private String driverLicenseNumber;
    @JsonProperty("PhoneNumberForQuestions")
    private String phoneNumberForQuestions;
    @JsonProperty("VehicleMake")
    private String vehicleMake;
    @JsonProperty("VehicleModel")
    private String vehicleModel;
    @JsonProperty("VehicleLicensePlateNumber")
    private String vehicleLicensePlateNumber;
    @JsonProperty("Destinations")
    private List<ManifestDestination> destinations = new ArrayList<>();
    @JsonProperty("TransferTemplateId")
    private String transferTemplateId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTransporterFacilityLicenseNumber() {
        return transporterFacilityLicenseNumber;
    }

    public void setTransporterFacilityLicenseNumber(String transporterFacilityLicenseNumber) {
        this.transporterFacilityLicenseNumber = transporterFacilityLicenseNumber;
    }

    public String getDriverOccupationalLicenseNumber() {
        return driverOccupationalLicenseNumber;
    }

    public void setDriverOccupationalLicenseNumber(String driverOccupationalLicenseNumber) {
        this.driverOccupationalLicenseNumber = driverOccupationalLicenseNumber;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getDriverLicenseNumber() {
        return driverLicenseNumber;
    }

    public void setDriverLicenseNumber(String driverLicenseNumber) {
        this.driverLicenseNumber = driverLicenseNumber;
    }

    public String getPhoneNumberForQuestions() {
        return phoneNumberForQuestions;
    }

    public void setPhoneNumberForQuestions(String phoneNumberForQuestions) {
        this.phoneNumberForQuestions = phoneNumberForQuestions;
    }

    public String getVehicleMake() {
        return vehicleMake;
    }

    public void setVehicleMake(String vehicleMake) {
        this.vehicleMake = vehicleMake;
    }

    public String getVehicleModel() {
        return vehicleModel;
    }

    public void setVehicleModel(String vehicleModel) {
        this.vehicleModel = vehicleModel;
    }

    public String getVehicleLicensePlateNumber() {
        return vehicleLicensePlateNumber;
    }

    public void setVehicleLicensePlateNumber(String vehicleLicensePlateNumber) {
        this.vehicleLicensePlateNumber = vehicleLicensePlateNumber;
    }

    public List<ManifestDestination> getDestinations() {
        return destinations;
    }

    public void setDestinations(List<ManifestDestination> destinations) {
        this.destinations = destinations;
    }

    public String getTransferTemplateId() {
        return transferTemplateId;
    }

    public void setTransferTemplateId(String transferTemplateId) {
        this.transferTemplateId = transferTemplateId;
    }
}
