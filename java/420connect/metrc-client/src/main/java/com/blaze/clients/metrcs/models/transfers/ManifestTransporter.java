package com.blaze.clients.metrcs.models.transfers;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ManifestTransporter {
    @JsonProperty("TransporterFacilityLicenseNumber")
    private String transporterFacilityLicenseNumber;
    @JsonProperty("DriverOccupationalLicenseNumber")
    private String driverOccupationalLicenseNumber;
    @JsonProperty("DriverName")
    private String driverName;
    @JsonProperty("DriverLicenseNumber")
    private String driverLicenseNumber;
    @JsonProperty("PhoneNumberForQuestions")
    private String phoneNumberForQuestions;
    @JsonProperty("VehicleMake")
    private String vehicleMake;
    @JsonProperty("VehicleModel")
    private String vehicleModel;
    @JsonProperty("VehicleLicensePlateNumber")
    private String vehicleLicensePlateNumber;
    @JsonProperty("IsLayover")
    private boolean layover;
    @JsonProperty("EstimatedDepartureDateTime")
    private String estimatedDepartureDateTime;
    @JsonProperty("EstimatedArrivalDateTime")
    private String estimatedArrivalDateTime;

    public String getTransporterFacilityLicenseNumber() {
        return transporterFacilityLicenseNumber;
    }

    public void setTransporterFacilityLicenseNumber(String transporterFacilityLicenseNumber) {
        this.transporterFacilityLicenseNumber = transporterFacilityLicenseNumber;
    }

    public String getDriverOccupationalLicenseNumber() {
        return driverOccupationalLicenseNumber;
    }

    public void setDriverOccupationalLicenseNumber(String driverOccupationalLicenseNumber) {
        this.driverOccupationalLicenseNumber = driverOccupationalLicenseNumber;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getDriverLicenseNumber() {
        return driverLicenseNumber;
    }

    public void setDriverLicenseNumber(String driverLicenseNumber) {
        this.driverLicenseNumber = driverLicenseNumber;
    }

    public String getPhoneNumberForQuestions() {
        return phoneNumberForQuestions;
    }

    public void setPhoneNumberForQuestions(String phoneNumberForQuestions) {
        this.phoneNumberForQuestions = phoneNumberForQuestions;
    }

    public String getVehicleMake() {
        return vehicleMake;
    }

    public void setVehicleMake(String vehicleMake) {
        this.vehicleMake = vehicleMake;
    }

    public String getVehicleModel() {
        return vehicleModel;
    }

    public void setVehicleModel(String vehicleModel) {
        this.vehicleModel = vehicleModel;
    }

    public String getVehicleLicensePlateNumber() {
        return vehicleLicensePlateNumber;
    }

    public void setVehicleLicensePlateNumber(String vehicleLicensePlateNumber) {
        this.vehicleLicensePlateNumber = vehicleLicensePlateNumber;
    }

    public boolean getLayover() {
        return layover;
    }

    public void isLayover(boolean layover) {
        this.layover = layover;
    }

    public String getEstimatedDepartureDateTime() {
        return estimatedDepartureDateTime;
    }

    public void setEstimatedDepartureDateTime(String estimatedDepartureDateTime) {
        this.estimatedDepartureDateTime = estimatedDepartureDateTime;
    }

    public String getEstimatedArrivalDateTime() {
        return estimatedArrivalDateTime;
    }

    public void setEstimatedArrivalDateTime(String estimatedArrivalDateTime) {
        this.estimatedArrivalDateTime = estimatedArrivalDateTime;
    }
}
