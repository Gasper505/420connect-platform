package com.blaze.clients.metrcs.models.transfers;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MetrcsTransferTemplateResult {
    @JsonProperty("Id")
    private String id;

    @JsonProperty("ManifestNumber")
    private String manifestNumber;

    @JsonProperty("ShipmentLicenseType")
    private String shippingLicenseType;

    @JsonProperty("ShipperFacilityLicenseNumber")
    private String shippingFacilityLicenseNumber;

    @JsonProperty("ShipperFacilityName")
    private String shippingFacilityName;

    @JsonProperty("Name")
    private String name;

    @JsonProperty("TransporterFacilityLicenseNumber")
    private String transporterFacilityLicenseNumber;

    @JsonProperty("TransporterFacilityName")
    private String transporterFacilityName;

    @JsonProperty("DriverName")
    private String driverName;

    @JsonProperty("DriverOccupationalLicenseNumber")
    private String driverOccupationalLicenseNumber;

    @JsonProperty("DriverVehicleLicenseNumber")
    private String driverVehicleLicenseNumber;

    @JsonProperty("VehicleMake")
    private String vehicleMake;

    @JsonProperty("VehicleModel")
    private String vehicleModel;

    @JsonProperty("VehicleLicensePlateNumber")
    private String vehicleLicensePlateNumber;

    @JsonProperty("DeliveryCount")
    private long deliveryCount;

    @JsonProperty("ReceivedDeliveryCount")
    private long receivedDeliveryCount;

    @JsonProperty("PackageCount")
    private long packageCount;

    @JsonProperty("ReceivedPackageCount")
    private long receivedPackageCount;

    @JsonProperty("ContainsPlantPackage")
    private boolean containsPlantPackage;

    @JsonProperty("ContainsProductPackage")
    private boolean containsProductPackage;

    @JsonProperty("ContainsTestingSample")
    private boolean containsTestingSample;

    @JsonProperty("ContainsProductRequiresRemediation")
    private boolean containsProductRequiresRemediation;

    @JsonProperty("ContainsRemediatedProductPackage")
    private boolean containsRemediatedProductPackage;

    @JsonProperty("CreatedDateTime")
    private String createdDateTime;

    @JsonProperty("CreatedByUserName")
    private String createdByUserName;

    @JsonProperty("LastModified")
    private String lastModified;

    @JsonProperty("DeliveryId")
    private long deliveryId;

    @JsonProperty("RecipientFacilityLicenseNumber")
    private String recipientFacilityLicenseNumber;

    @JsonProperty("RecipientFacilityName")
    private String recipientFacilityName;

    @JsonProperty("ShipmentTypeName")
    private String shipmentTypeName;

    @JsonProperty("ShipmentTransactionType")
    private long shipmentTransactionType;

    @JsonProperty("EstimatedDepartureDateTime")
    private String estimatedDepartureDateTime;

    @JsonProperty("ActualDepartureDateTime")
    private String actualDepartureDateTime;

    @JsonProperty("EstimatedArrivalDateTime")
    private String estimatedArrivalDateTime;

    @JsonProperty("ActualArrivalDateTime")
    private String actualArrivalDateTime;

    @JsonProperty("DeliveryPackageCount")
    private long deliveryPackageCount;

    @JsonProperty("DeliveryReceivedPackageCount")
    private long deliveryReceivedPackageCount;

    @JsonProperty("ReceivedDateTime")
    private String receivedDateTime;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getManifestNumber() {
        return manifestNumber;
    }

    public void setManifestNumber(String manifestNumber) {
        this.manifestNumber = manifestNumber;
    }

    public String getShippingLicenseType() {
        return shippingLicenseType;
    }

    public void setShippingLicenseType(String shippingLicenseType) {
        this.shippingLicenseType = shippingLicenseType;
    }

    public String getShippingFacilityLicenseNumber() {
        return shippingFacilityLicenseNumber;
    }

    public void setShippingFacilityLicenseNumber(String shippingFacilityLicenseNumber) {
        this.shippingFacilityLicenseNumber = shippingFacilityLicenseNumber;
    }

    public String getShippingFacilityName() {
        return shippingFacilityName;
    }

    public void setShippingFacilityName(String shippingFacilityName) {
        this.shippingFacilityName = shippingFacilityName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTransporterFacilityLicenseNumber() {
        return transporterFacilityLicenseNumber;
    }

    public void setTransporterFacilityLicenseNumber(String transporterFacilityLicenseNumber) {
        this.transporterFacilityLicenseNumber = transporterFacilityLicenseNumber;
    }

    public String getTransporterFacilityName() {
        return transporterFacilityName;
    }

    public void setTransporterFacilityName(String transporterFacilityName) {
        this.transporterFacilityName = transporterFacilityName;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getDriverOccupationalLicenseNumber() {
        return driverOccupationalLicenseNumber;
    }

    public void setDriverOccupationalLicenseNumber(String driverOccupationalLicenseNumber) {
        this.driverOccupationalLicenseNumber = driverOccupationalLicenseNumber;
    }

    public String getDriverVehicleLicenseNumber() {
        return driverVehicleLicenseNumber;
    }

    public void setDriverVehicleLicenseNumber(String driverVehicleLicenseNumber) {
        this.driverVehicleLicenseNumber = driverVehicleLicenseNumber;
    }

    public String getVehicleMake() {
        return vehicleMake;
    }

    public void setVehicleMake(String vehicleMake) {
        this.vehicleMake = vehicleMake;
    }

    public String getVehicleModel() {
        return vehicleModel;
    }

    public void setVehicleModel(String vehicleModel) {
        this.vehicleModel = vehicleModel;
    }

    public String getVehicleLicensePlateNumber() {
        return vehicleLicensePlateNumber;
    }

    public void setVehicleLicensePlateNumber(String vehicleLicensePlateNumber) {
        this.vehicleLicensePlateNumber = vehicleLicensePlateNumber;
    }

    public long getDeliveryCount() {
        return deliveryCount;
    }

    public void setDeliveryCount(long deliveryCount) {
        this.deliveryCount = deliveryCount;
    }

    public long getReceivedDeliveryCount() {
        return receivedDeliveryCount;
    }

    public void setReceivedDeliveryCount(long receivedDeliveryCount) {
        this.receivedDeliveryCount = receivedDeliveryCount;
    }

    public long getPackageCount() {
        return packageCount;
    }

    public void setPackageCount(long packageCount) {
        this.packageCount = packageCount;
    }

    public long getReceivedPackageCount() {
        return receivedPackageCount;
    }

    public void setReceivedPackageCount(long receivedPackageCount) {
        this.receivedPackageCount = receivedPackageCount;
    }

    public boolean isContainsPlantPackage() {
        return containsPlantPackage;
    }

    public void setContainsPlantPackage(boolean containsPlantPackage) {
        this.containsPlantPackage = containsPlantPackage;
    }

    public boolean isContainsProductPackage() {
        return containsProductPackage;
    }

    public void setContainsProductPackage(boolean containsProductPackage) {
        this.containsProductPackage = containsProductPackage;
    }

    public boolean isContainsTestingSample() {
        return containsTestingSample;
    }

    public void setContainsTestingSample(boolean containsTestingSample) {
        this.containsTestingSample = containsTestingSample;
    }

    public boolean isContainsProductRequiresRemediation() {
        return containsProductRequiresRemediation;
    }

    public void setContainsProductRequiresRemediation(boolean containsProductRequiresRemediation) {
        this.containsProductRequiresRemediation = containsProductRequiresRemediation;
    }

    public boolean isContainsRemediatedProductPackage() {
        return containsRemediatedProductPackage;
    }

    public void setContainsRemediatedProductPackage(boolean containsRemediatedProductPackage) {
        this.containsRemediatedProductPackage = containsRemediatedProductPackage;
    }

    public String getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(String createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public String getCreatedByUserName() {
        return createdByUserName;
    }

    public void setCreatedByUserName(String createdByUserName) {
        this.createdByUserName = createdByUserName;
    }

    public String getLastModified() {
        return lastModified;
    }

    public void setLastModified(String lastModified) {
        this.lastModified = lastModified;
    }

    public long getDeliveryId() {
        return deliveryId;
    }

    public void setDeliveryId(long deliveryId) {
        this.deliveryId = deliveryId;
    }

    public String getRecipientFacilityLicenseNumber() {
        return recipientFacilityLicenseNumber;
    }

    public void setRecipientFacilityLicenseNumber(String recipientFacilityLicenseNumber) {
        this.recipientFacilityLicenseNumber = recipientFacilityLicenseNumber;
    }

    public String getRecipientFacilityName() {
        return recipientFacilityName;
    }

    public void setRecipientFacilityName(String recipientFacilityName) {
        this.recipientFacilityName = recipientFacilityName;
    }

    public String getShipmentTypeName() {
        return shipmentTypeName;
    }

    public void setShipmentTypeName(String shipmentTypeName) {
        this.shipmentTypeName = shipmentTypeName;
    }

    public long getShipmentTransactionType() {
        return shipmentTransactionType;
    }

    public void setShipmentTransactionType(long shipmentTransactionType) {
        this.shipmentTransactionType = shipmentTransactionType;
    }

    public String getEstimatedDepartureDateTime() {
        return estimatedDepartureDateTime;
    }

    public void setEstimatedDepartureDateTime(String estimatedDepartureDateTime) {
        this.estimatedDepartureDateTime = estimatedDepartureDateTime;
    }

    public String getActualDepartureDateTime() {
        return actualDepartureDateTime;
    }

    public void setActualDepartureDateTime(String actualDepartureDateTime) {
        this.actualDepartureDateTime = actualDepartureDateTime;
    }

    public String getEstimatedArrivalDateTime() {
        return estimatedArrivalDateTime;
    }

    public void setEstimatedArrivalDateTime(String estimatedArrivalDateTime) {
        this.estimatedArrivalDateTime = estimatedArrivalDateTime;
    }

    public String getActualArrivalDateTime() {
        return actualArrivalDateTime;
    }

    public void setActualArrivalDateTime(String actualArrivalDateTime) {
        this.actualArrivalDateTime = actualArrivalDateTime;
    }

    public long getDeliveryPackageCount() {
        return deliveryPackageCount;
    }

    public void setDeliveryPackageCount(long deliveryPackageCount) {
        this.deliveryPackageCount = deliveryPackageCount;
    }

    public long getDeliveryReceivedPackageCount() {
        return deliveryReceivedPackageCount;
    }

    public void setDeliveryReceivedPackageCount(long deliveryReceivedPackageCount) {
        this.deliveryReceivedPackageCount = deliveryReceivedPackageCount;
    }

    public String getReceivedDateTime() {
        return receivedDateTime;
    }

    public void setReceivedDateTime(String receivedDateTime) {
        this.receivedDateTime = receivedDateTime;
    }
}
