package com.blaze.clients.metrcs.models.transfers;

import com.blaze.common.data.compliance.ComplianceTransferInfo;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Gaurav Saini on 22/8/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY,
        getterVisibility = JsonAutoDetect.Visibility.NONE,
        setterVisibility = JsonAutoDetect.Visibility.NONE)
public class MetricsTransfer extends ComplianceTransferInfo {

    private int Id;
    private String ManifestNumber;
    private String ShipperFacilityLicenseNumber;
    private String ShipperFacilityName;
    private String TransporterFacilityLicenseNumber;
    private String TransporterFacilityName;
    private String DriverName;
    private String DriverOccupationalLicenseNumber;
    private String DriverVehicleLicenseNumber;
    private String VehicleMake;
    private String VehicleModel;
    private String VehicleLicensePlateNumber;
    private int DeliveryCount;
    private int ReceivedDeliveryCount;
    private int PackageCount;
    private int ReceivedPackageCount;
    private String CreatedDateTime;
    private String CreatedByUserName;
    private String LastModified;
    private int DeliveryId;
    private String RecipientFacilityLicenseNumber;
    private String RecipientFacilityName;
    private String ShipmentType;
    private String EstimatedDepartureDateTime;
    private String EstimatedArrivalDateTime;
    private int DeliveryPackageCount;
    private int DeliveryReceivedPackageCount;
    private String ReceivedDateTime;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getManifestNumber() {
        return ManifestNumber;
    }

    public void setManifestNumber(String manifestNumber) {
        ManifestNumber = manifestNumber;
    }

    public String getShipperFacilityLicenseNumber() {
        return ShipperFacilityLicenseNumber;
    }

    public void setShipperFacilityLicenseNumber(String shipperFacilityLicenseNumber) {
        ShipperFacilityLicenseNumber = shipperFacilityLicenseNumber;
    }

    public String getShipperFacilityName() {
        return ShipperFacilityName;
    }

    public void setShipperFacilityName(String shipperFacilityName) {
        ShipperFacilityName = shipperFacilityName;
    }

    public String getTransporterFacilityLicenseNumber() {
        return TransporterFacilityLicenseNumber;
    }

    public void setTransporterFacilityLicenseNumber(String transporterFacilityLicenseNumber) {
        TransporterFacilityLicenseNumber = transporterFacilityLicenseNumber;
    }

    public String getTransporterFacilityName() {
        return TransporterFacilityName;
    }

    public void setTransporterFacilityName(String transporterFacilityName) {
        TransporterFacilityName = transporterFacilityName;
    }

    public String getDriverName() {
        return DriverName;
    }

    public void setDriverName(String driverName) {
        DriverName = driverName;
    }

    public String getDriverOccupationalLicenseNumber() {
        return DriverOccupationalLicenseNumber;
    }

    public void setDriverOccupationalLicenseNumber(String driverOccupationalLicenseNumber) {
        DriverOccupationalLicenseNumber = driverOccupationalLicenseNumber;
    }

    public String getDriverVehicleLicenseNumber() {
        return DriverVehicleLicenseNumber;
    }

    public void setDriverVehicleLicenseNumber(String driverVehicleLicenseNumber) {
        DriverVehicleLicenseNumber = driverVehicleLicenseNumber;
    }

    public String getVehicleMake() {
        return VehicleMake;
    }

    public void setVehicleMake(String vehicleMake) {
        VehicleMake = vehicleMake;
    }

    public String getVehicleModel() {
        return VehicleModel;
    }

    public void setVehicleModel(String vehicleModel) {
        VehicleModel = vehicleModel;
    }

    public String getVehicleLicensePlateNumber() {
        return VehicleLicensePlateNumber;
    }

    public void setVehicleLicensePlateNumber(String vehicleLicensePlateNumber) {
        VehicleLicensePlateNumber = vehicleLicensePlateNumber;
    }

    public int getDeliveryCount() {
        return DeliveryCount;
    }

    public void setDeliveryCount(int deliveryCount) {
        DeliveryCount = deliveryCount;
    }

    public int getReceivedDeliveryCount() {
        return ReceivedDeliveryCount;
    }

    public void setReceivedDeliveryCount(int receivedDeliveryCount) {
        ReceivedDeliveryCount = receivedDeliveryCount;
    }

    public int getPackageCount() {
        return PackageCount;
    }

    public void setPackageCount(int packageCount) {
        PackageCount = packageCount;
    }

    public int getReceivedPackageCount() {
        return ReceivedPackageCount;
    }

    public void setReceivedPackageCount(int receivedPackageCount) {
        ReceivedPackageCount = receivedPackageCount;
    }

    public String getCreatedDateTime() {
        return CreatedDateTime;
    }

    public void setCreatedDateTime(String createdDateTime) {
        CreatedDateTime = createdDateTime;
    }

    public String getCreatedByUserName() {
        return CreatedByUserName;
    }

    public void setCreatedByUserName(String createdByUserName) {
        CreatedByUserName = createdByUserName;
    }

    public String getLastModified() {
        return LastModified;
    }

    public void setLastModified(String lastModified) {
        LastModified = lastModified;
    }

    public int getDeliveryId() {
        return DeliveryId;
    }

    public void setDeliveryId(int deliveryId) {
        DeliveryId = deliveryId;
    }

    public String getRecipientFacilityLicenseNumber() {
        return RecipientFacilityLicenseNumber;
    }

    public void setRecipientFacilityLicenseNumber(String recipientFacilityLicenseNumber) {
        RecipientFacilityLicenseNumber = recipientFacilityLicenseNumber;
    }

    public String getRecipientFacilityName() {
        return RecipientFacilityName;
    }

    public void setRecipientFacilityName(String recipientFacilityName) {
        RecipientFacilityName = recipientFacilityName;
    }

    public String getShipmentType() {
        return ShipmentType;
    }

    public void setShipmentType(String shipmentType) {
        ShipmentType = shipmentType;
    }

    public String getEstimatedDepartureDateTime() {
        return EstimatedDepartureDateTime;
    }

    public void setEstimatedDepartureDateTime(String estimatedDepartureDateTime) {
        EstimatedDepartureDateTime = estimatedDepartureDateTime;
    }

    public String getEstimatedArrivalDateTime() {
        return EstimatedArrivalDateTime;
    }

    public void setEstimatedArrivalDateTime(String estimatedArrivalDateTime) {
        EstimatedArrivalDateTime = estimatedArrivalDateTime;
    }

    public int getDeliveryPackageCount() {
        return DeliveryPackageCount;
    }

    public void setDeliveryPackageCount(int deliveryPackageCount) {
        DeliveryPackageCount = deliveryPackageCount;
    }

    public int getDeliveryReceivedPackageCount() {
        return DeliveryReceivedPackageCount;
    }

    public void setDeliveryReceivedPackageCount(int deliveryReceivedPackageCount) {
        DeliveryReceivedPackageCount = deliveryReceivedPackageCount;
    }

    public String getReceivedDateTime() {
        return ReceivedDateTime;
    }

    public void setReceivedDateTime(String receivedDateTime) {
        ReceivedDateTime = receivedDateTime;
    }
}
