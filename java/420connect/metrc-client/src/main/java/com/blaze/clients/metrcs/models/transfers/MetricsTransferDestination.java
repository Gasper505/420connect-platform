package com.blaze.clients.metrcs.models.transfers;

import java.util.List;

public class MetricsTransferDestination {
    private String TransporterFacilityLicenseNumber;
    private String DriverOccupationalLicenseNumber;
    private String DriverName;
    private String DriverLicenseNumber;
    private String PhoneNumberForQuestions;
    private String VehicleMake;
    private String VehicleModel;
    private String VehicleLicensePlateNumber;
    private Boolean IsLayover;
    private String EstimatedDepartureDateTime;
    private String EstimatedArrivalDateTime;
    private List<MetricsTransferDestinationPackage> Packages;

    public String getTransporterFacilityLicenseNumber() {
        return TransporterFacilityLicenseNumber;
    }

    public void setTransporterFacilityLicenseNumber(String transporterFacilityLicenseNumber) {
        TransporterFacilityLicenseNumber = transporterFacilityLicenseNumber;
    }

    public String getDriverOccupationalLicenseNumber() {
        return DriverOccupationalLicenseNumber;
    }

    public void setDriverOccupationalLicenseNumber(String driverOccupationalLicenseNumber) {
        DriverOccupationalLicenseNumber = driverOccupationalLicenseNumber;
    }

    public String getDriverName() {
        return DriverName;
    }

    public void setDriverName(String driverName) {
        DriverName = driverName;
    }

    public String getDriverLicenseNumber() {
        return DriverLicenseNumber;
    }

    public void setDriverLicenseNumber(String driverLicenseNumber) {
        DriverLicenseNumber = driverLicenseNumber;
    }

    public String getPhoneNumberForQuestions() {
        return PhoneNumberForQuestions;
    }

    public void setPhoneNumberForQuestions(String phoneNumberForQuestions) {
        PhoneNumberForQuestions = phoneNumberForQuestions;
    }

    public String getVehicleMake() {
        return VehicleMake;
    }

    public void setVehicleMake(String vehicleMake) {
        VehicleMake = vehicleMake;
    }

    public String getVehicleModel() {
        return VehicleModel;
    }

    public void setVehicleModel(String vehicleModel) {
        VehicleModel = vehicleModel;
    }

    public String getVehicleLicensePlateNumber() {
        return VehicleLicensePlateNumber;
    }

    public void setVehicleLicensePlateNumber(String vehicleLicensePlateNumber) {
        VehicleLicensePlateNumber = vehicleLicensePlateNumber;
    }

    public Boolean getLayover() {
        return IsLayover;
    }

    public void setLayover(Boolean layover) {
        IsLayover = layover;
    }

    public String getEstimatedDepartureDateTime() {
        return EstimatedDepartureDateTime;
    }

    public void setEstimatedDepartureDateTime(String estimatedDepartureDateTime) {
        EstimatedDepartureDateTime = estimatedDepartureDateTime;
    }

    public String getEstimatedArrivalDateTime() {
        return EstimatedArrivalDateTime;
    }

    public void setEstimatedArrivalDateTime(String estimatedArrivalDateTime) {
        EstimatedArrivalDateTime = estimatedArrivalDateTime;
    }

    public List<MetricsTransferDestinationPackage> getPackages() {
        return Packages;
    }

    public void setPackages(List<MetricsTransferDestinationPackage> packages) {
        Packages = packages;
    }
}
