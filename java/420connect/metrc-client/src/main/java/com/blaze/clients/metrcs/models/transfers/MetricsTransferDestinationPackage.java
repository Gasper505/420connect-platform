package com.blaze.clients.metrcs.models.transfers;

public class MetricsTransferDestinationPackage {
private String PackageLabel;
    private String ItemName;
    private double Quantity;
    private String UnitOfMeasureName;
    private String PackagedDate;
    private String GrossWeight;
    private String GrossUnitOfWeightName;
    private double WholesalePrice;

    public String getPackageLabel() {
        return PackageLabel;
    }

    public void setPackageLabel(String packageLabel) {
        PackageLabel = packageLabel;
    }

    public String getItemName() {
        return ItemName;
    }

    public void setItemName(String itemName) {
        ItemName = itemName;
    }

    public double getQuantity() {
        return Quantity;
    }

    public void setQuantity(double quantity) {
        Quantity = quantity;
    }

    public String getUnitOfMeasureName() {
        return UnitOfMeasureName;
    }

    public void setUnitOfMeasureName(String unitOfMeasureName) {
        UnitOfMeasureName = unitOfMeasureName;
    }

    public String getPackagedDate() {
        return PackagedDate;
    }

    public void setPackagedDate(String packagedDate) {
        PackagedDate = packagedDate;
    }

    public String getGrossWeight() {
        return GrossWeight;
    }

    public void setGrossWeight(String grossWeight) {
        GrossWeight = grossWeight;
    }

    public String getGrossUnitOfWeightName() {
        return GrossUnitOfWeightName;
    }

    public void setGrossUnitOfWeightName(String grossUnitOfWeightName) {
        GrossUnitOfWeightName = grossUnitOfWeightName;
    }

    public double getWholesalePrice() {
        return WholesalePrice;
    }

    public void setWholesalePrice(double wholesalePrice) {
        WholesalePrice = wholesalePrice;
    }
}
