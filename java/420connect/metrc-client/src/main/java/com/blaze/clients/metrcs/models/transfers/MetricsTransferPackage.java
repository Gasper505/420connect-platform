package com.blaze.clients.metrcs.models.transfers;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Gaurav Saini on 22/8/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY,
        getterVisibility = JsonAutoDetect.Visibility.NONE,
        setterVisibility = JsonAutoDetect.Visibility.NONE)
public class MetricsTransferPackage {

    private int PackageId;
    private String PackageLabel;
    private String SourceHarvestNames;
    private String ProductName;
    private String ProductCategoryName;
    private String ShipmentPackageState;
    private double ShippedQuantity;
    private String ShippedUnitOfMeasureName;
    private double ReceivedQuantity;
    private String ReceivedUnitOfMeasureName;
    private String ReceivedDateTime;

    public int getPackageId() {
        return PackageId;
    }

    public void setPackageId(int packageId) {
        PackageId = packageId;
    }

    public String getPackageLabel() {
        return PackageLabel;
    }

    public void setPackageLabel(String packageLabel) {
        PackageLabel = packageLabel;
    }

    public String getSourceHarvestNames() {
        return SourceHarvestNames;
    }

    public void setSourceHarvestNames(String sourceHarvestNames) {
        SourceHarvestNames = sourceHarvestNames;
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }

    public String getProductCategoryName() {
        return ProductCategoryName;
    }

    public void setProductCategoryName(String productCategoryName) {
        ProductCategoryName = productCategoryName;
    }

    public String getShipmentPackageState() {
        return ShipmentPackageState;
    }

    public void setShipmentPackageState(String shipmentPackageState) {
        ShipmentPackageState = shipmentPackageState;
    }

    public double getShippedQuantity() {
        return ShippedQuantity;
    }

    public void setShippedQuantity(double shippedQuantity) {
        ShippedQuantity = shippedQuantity;
    }

    public String getShippedUnitOfMeasureName() {
        return ShippedUnitOfMeasureName;
    }

    public void setShippedUnitOfMeasureName(String shippedUnitOfMeasureName) {
        ShippedUnitOfMeasureName = shippedUnitOfMeasureName;
    }

    public double getReceivedQuantity() {
        return ReceivedQuantity;
    }

    public void setReceivedQuantity(double receivedQuantity) {
        ReceivedQuantity = receivedQuantity;
    }

    public String getReceivedUnitOfMeasureName() {
        return ReceivedUnitOfMeasureName;
    }

    public void setReceivedUnitOfMeasureName(String receivedUnitOfMeasureName) {
        ReceivedUnitOfMeasureName = receivedUnitOfMeasureName;
    }

    public String getReceivedDateTime() {
        return ReceivedDateTime;
    }

    public void setReceivedDateTime(String receivedDateTime) {
        ReceivedDateTime = receivedDateTime;
    }
}
