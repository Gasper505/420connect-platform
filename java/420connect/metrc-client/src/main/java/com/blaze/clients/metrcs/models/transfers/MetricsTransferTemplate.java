package com.blaze.clients.metrcs.models.transfers;

import java.util.List;

public class MetricsTransferTemplate {
    private int TransferTemplateId;
    private String ManifestNumber;
    private String ShipperLicenseNumber;
    private String Name;
    private String TransporterFacilityLicenseNumber;
    private String DriverOccupationalLicenseNumber;
    private String DriverName;
    private String DriverLicenseNumber;
    private String PhoneNumberForQuestions;
    private String VehicleMake;
    private String VehicleModel;
    private String VehicleLicensePlateNumber;
    private List<MetricsTransferDestination> Destinations;

    public int getTransferTemplateId() {
        return TransferTemplateId;
    }

    public void setTransferTemplateId(int transferTemplateId) {
        TransferTemplateId = transferTemplateId;
    }

    public String getManifestNumber() {
        return ManifestNumber;
    }

    public void setManifestNumber(String manifestNumber) {
        ManifestNumber = manifestNumber;
    }

    public String getShipperLicenseNumber() {
        return ShipperLicenseNumber;
    }

    public void setShipperLicenseNumber(String shipperLicenseNumber) {
        ShipperLicenseNumber = shipperLicenseNumber;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getTransporterFacilityLicenseNumber() {
        return TransporterFacilityLicenseNumber;
    }

    public void setTransporterFacilityLicenseNumber(String transporterFacilityLicenseNumber) {
        TransporterFacilityLicenseNumber = transporterFacilityLicenseNumber;
    }

    public String getDriverOccupationalLicenseNumber() {
        return DriverOccupationalLicenseNumber;
    }

    public void setDriverOccupationalLicenseNumber(String driverOccupationalLicenseNumber) {
        DriverOccupationalLicenseNumber = driverOccupationalLicenseNumber;
    }

    public String getDriverName() {
        return DriverName;
    }

    public void setDriverName(String driverName) {
        DriverName = driverName;
    }

    public String getDriverLicenseNumber() {
        return DriverLicenseNumber;
    }

    public void setDriverLicenseNumber(String driverLicenseNumber) {
        DriverLicenseNumber = driverLicenseNumber;
    }

    public String getPhoneNumberForQuestions() {
        return PhoneNumberForQuestions;
    }

    public void setPhoneNumberForQuestions(String phoneNumberForQuestions) {
        PhoneNumberForQuestions = phoneNumberForQuestions;
    }

    public String getVehicleMake() {
        return VehicleMake;
    }

    public void setVehicleMake(String vehicleMake) {
        VehicleMake = vehicleMake;
    }

    public String getVehicleModel() {
        return VehicleModel;
    }

    public void setVehicleModel(String vehicleModel) {
        VehicleModel = vehicleModel;
    }

    public String getVehicleLicensePlateNumber() {
        return VehicleLicensePlateNumber;
    }

    public void setVehicleLicensePlateNumber(String vehicleLicensePlateNumber) {
        VehicleLicensePlateNumber = vehicleLicensePlateNumber;
    }

    public List<MetricsTransferDestination> getDestinations() {
        return Destinations;
    }

    public void setDestinations(List<MetricsTransferDestination> destinations) {
        Destinations = destinations;
    }
}
