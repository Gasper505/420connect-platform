package com.blaze.clients.metrcs.serializers;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;
import java.math.BigDecimal;

/**
 * Created by mdo on 1/12/18.
 */
public class BigDecimalQuantityDeserializer extends JsonDeserializer<BigDecimal> {

    @Override
    public BigDecimal deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        double doubleValue = p.getValueAsDouble();
        return BigDecimal.valueOf(doubleValue).setScale(4, BigDecimal.ROUND_HALF_EVEN);
    }
}
