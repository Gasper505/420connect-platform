package com.blaze.clients.metrcs.serializers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.math.BigDecimal;

/**
 * Created by mdo on 10/3/17.
 */
public class BigDecimalQuantitySerializer extends JsonSerializer<BigDecimal> {

    @Override
    public void serialize(BigDecimal value, JsonGenerator gen, SerializerProvider serializers) throws IOException, JsonProcessingException {

        double dvalue = round(value, 4);
        gen.writeNumber(dvalue);
        //gen.writeNumber(value);
    }

    private static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        double divisor = Math.pow(10.0, places);
        double newValue = Math.round(value * divisor) / divisor;
        return newValue;
        //BigDecimal bd = new BigDecimal(value);
        //bd = bd.setScale(places, RoundingMode.HALF_UP);
        //return bd.doubleValue();
    }

    private static double round(BigDecimal value, int places) {
        if (places < 0) throw new IllegalArgumentException();
        /*
        value = value.setScale(places, RoundingMode.HALF_UP);
        return value.doubleValue();*/
        return round(value.doubleValue(), places);
    }
}