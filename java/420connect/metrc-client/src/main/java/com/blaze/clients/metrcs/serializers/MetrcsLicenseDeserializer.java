package com.blaze.clients.metrcs.serializers;

import com.blaze.clients.metrcs.models.facilities.MetricsLicense;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;

/**
 * Created by mdo on 8/24/17.
 */
public class MetrcsLicenseDeserializer extends JsonDeserializer<MetricsLicense.FacilityLicenseType> {

    @Override
    public MetricsLicense.FacilityLicenseType deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        String value = p.getValueAsString();
        if (value != null) {
            return MetricsLicense.FacilityLicenseType.toLicenseType(value);
        }
        return MetricsLicense.FacilityLicenseType.Unknown;
    }
}
