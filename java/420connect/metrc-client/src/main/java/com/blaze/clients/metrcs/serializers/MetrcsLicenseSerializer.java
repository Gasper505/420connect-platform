package com.blaze.clients.metrcs.serializers;

import com.blaze.clients.metrcs.models.facilities.MetricsLicense;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;

/**
 * Created by mdo on 8/24/17.
 */
public class MetrcsLicenseSerializer extends JsonSerializer<MetricsLicense.FacilityLicenseType> {

    @Override
    public void serialize(MetricsLicense.FacilityLicenseType value, JsonGenerator gen, SerializerProvider serializers) throws IOException, JsonProcessingException {
        gen.writeString(value.displayName);
    }
}
